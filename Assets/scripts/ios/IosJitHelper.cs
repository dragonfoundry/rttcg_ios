using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

namespace DragonFoundry.Ios
{
	public class IosJitHelper
	{
		public static readonly IEqualityComparer<Guid> GuidEqualityComparer = new GuidEqualityComparerType();
		
		public static void DummyJitFunction() {
			Dictionary<Guid, String> dictionaryGuidString = new Dictionary<Guid, string>(GuidEqualityComparer);
			dictionaryGuidString.Add(Guid.NewGuid(), "Test-1");
			dictionaryGuidString.Add(Guid.Empty, "Test-2");

			JObject state = new JObject();

			state.Value<bool>("Dummy");
			state.Value<bool?> ("Dummy"); 
		}
		
		private class GuidEqualityComparerType : IEqualityComparer<Guid> {
			public bool Equals(Guid x, Guid y) {
				return x.Equals(y);
			}
			
			public int GetHashCode(Guid obj) {
				return obj.GetHashCode();
			}
		}
	}
}

