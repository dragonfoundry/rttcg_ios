﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;

public class StartUpNovaAudio : View
 {
	public NovaAudioPlayer AudioToPlay;

    public NovaAudio NovaAudio;
	// Use this for initialization
	new protected void Start () 
	{
		base.Start();
		if(this.AudioToPlay != null)
		{
			StartCoroutine(PlayAudio());
		}

	}
	
	IEnumerator PlayAudio()
	{
		yield return new WaitForSecondsRealtime(0.2f);
		while(this.NovaAudio == null || this.NovaAudio.IsInitialized == false)
		{
			yield return null;	
		}
		Debug.Log("Playing MainMenu Music",this);
		this.AudioToPlay.Play();
	}
}
