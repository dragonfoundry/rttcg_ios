﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

 #if UNITY_EDITOR
 using UnityEditor;
 #endif

[ExecuteInEditMode]
public class NovaAudioCrossfader : MonoBehaviour 
{
	public AudioMixerGroup AudioMixerGroupOne;
	public AudioMixerGroup AudioMixerGroupTwo;
	public AudioMixerSnapshot AudioSnapshotOne;
	public AudioMixerSnapshot AudioSnapshotTwo;
	private AudioMixerGroup currentMixerGroup = null;
	private NovaAudioGroup lastAudioPlayed = null;
	
	private IEnumerator stopAudioCoRoutine;
	private NovaAudioGroup audioGroupToBeStopped;
	
	public NovaAudioGroup PreviousAudio
	{
		get { return audioGroupToBeStopped; }
	}
	
	public NovaAudioGroup CurrentAudio
	{
		get { return this.lastAudioPlayed;}
	}
	
	void OnEnable()
	{
		this.lastAudioPlayed = null;
		this.currentMixerGroup = null;
	}
	
	void Awake()
	{
	}
	
	public void Crossfade(NovaAudioGroup audioGroup, float seconds)
	{
        if (audioGroup == null || audioGroup.AudioItems.Count == 0)
        {
            Debug.LogFormat("Failed to crossfade to {0} ({1} items)", audioGroup.Label, audioGroup == null ? -1 : audioGroup.AudioItems.Count);
            return;
        }
        Debug.LogFormat("CROSSFADING TO : {0} ({1} items) --------------------------------------",audioGroup.Label, audioGroup.AudioItems.Count);
		if(this.CurrentAudio == null)
		{
			// If nothings playing, just play the requested audiogroup with no crossfade
			this.AudioSnapshotOne.TransitionTo(0.01f);
			this.currentMixerGroup = this.AudioMixerGroupOne;
			this.lastAudioPlayed = audioGroup;
			this.lastAudioPlayed.AudioMixerGroup = this.currentMixerGroup;
		}
		else if(audioGroup != this.CurrentAudio)
		{
			// If there's already an audio being faded out...
			if(this.PreviousAudio != null)
			{
				// Put the Current Audio to the pervious mixergroup
				this.CurrentAudio.AudioMixerGroup = this.PreviousAudio.AudioMixerGroup;
				
				// Stop the previous audioGroup
				this.PreviousAudio.Stop();

				// Make the current audio the one being stopped by the stopAucioCoRoutine
				this.CurrentAudio.AudioMixerGroup = this.PreviousAudio.AudioMixerGroup;
				this.audioGroupToBeStopped = this.CurrentAudio;
				
				// Make the current audio the one being crossfaded too
				this.lastAudioPlayed = audioGroup;
				this.lastAudioPlayed.AudioMixerGroup = this.currentMixerGroup;
				AudioMixerSnapshot currentSnapshot = this.currentMixerGroup == this.AudioMixerGroupOne ? this.AudioSnapshotOne : this.AudioSnapshotTwo; 
				currentSnapshot.TransitionTo(seconds);
			}
			else
			{
				// Swap mixergroups
				this.currentMixerGroup = this.currentMixerGroup == this.AudioMixerGroupOne ? this.AudioMixerGroupTwo : this.AudioMixerGroupOne;
				this.audioGroupToBeStopped = this.CurrentAudio;
				
				// Start a coRoutine to stop the backgrounded audiogroup
				this.stopAudioCoRoutine = this.StopFadedAudioGroup(seconds);
				this.StartCoroutine(this.stopAudioCoRoutine);
				
				// Make the current audioGroup the one being crossfaded to
				this.lastAudioPlayed = audioGroup;
				this.lastAudioPlayed.AudioMixerGroup = this.currentMixerGroup;
				
				// Determine the current snapshot based on the current AudioMixerGroup and transition to it
				AudioMixerSnapshot currentSnapshot = this.currentMixerGroup == this.AudioMixerGroupOne ? this.AudioSnapshotOne : this.AudioSnapshotTwo; 
				currentSnapshot.TransitionTo(seconds);
			}
		}
	}
	
	private IEnumerator StopFadedAudioGroup(float seconds)
	{
		yield return new WaitForSecondsRealtime(seconds);
		{
			audioGroupToBeStopped.Stop();
			this.stopAudioCoRoutine = null;
			this.audioGroupToBeStopped = null;
		}
	}
}
