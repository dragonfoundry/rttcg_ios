﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NovaAudioButton : MonoBehaviour 
{
	public NovaAudioPlayer ButtonClickAudio;
	
	void Awake()
	{
		Button button = this.GetComponent<Button>();
		if(button != null)
		{
			button.onClick.RemoveListener(OnButtonClicked);
			button.onClick.AddListener(OnButtonClicked);
		}
		else
		{
			Toggle toggle = this.GetComponent<Toggle>();
			if(toggle != null)
			{
				toggle.onValueChanged.RemoveListener(OnToggleChanged);
				toggle.onValueChanged.AddListener(OnToggleChanged);
			}
		}
		
	}
	
	public void OnButtonClicked()
	{
		this.ButtonClickAudio.Play();
	}
	
	public void OnToggleChanged(bool isOn)
	{
		if(isOn)
		{
			this.ButtonClickAudio.Play();
		}
	}
}
