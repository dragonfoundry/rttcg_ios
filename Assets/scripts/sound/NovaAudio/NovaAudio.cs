﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using NovaBlitz.UI;

[ExecuteInEditMode]
public class NovaAudio : MonoBehaviour
{
    public static NovaAudio Instance;
    public AssetBundleManager AssetBundleManager { get; set; }
	private static int AUDIO_SOURCE_POOL_SIZE = 1;
	void OnEnable()
	{
		NovaAudio [] existingNovaAudio = GameObject.FindObjectsOfType<NovaAudio>();
		if(existingNovaAudio.Length > 1)
		{
			Debug.LogError("Cannot create NovaAudio component, another already exists in the scene");
			GameObject.DestroyImmediate(this);
			return;
		}
		this.gameObject.name = "NovaAudio";
		NovaAudio.Instance = this;
		
		// Destroy any children of the NovaAudio GO that may exist
		List<GameObject> children = new List<GameObject>();
		foreach(Transform child in transform)
		{
			children.Add(child.gameObject);
		}
		
		this.transform.DetachChildren();
		for(int i=0;i<children.Count;i++)
		{
			GameObject.DestroyImmediate(children[i]);
		}
		
		children.Clear();
		this.AudioSourcesRoot = null;
		this.AudioSources.Clear();
		this.AvailableAudioSources.Clear();
	}
	public bool IsInitialized {get; set;}
	//public AssetBundle AssetBundle {get; set;}
	public AudioMixer AudioMixer;
	public List<NovaAudioGroup> AudioGroups = new List<NovaAudioGroup>();
	private Transform AudioSourcesRoot = null;
	private List<AudioSource> AudioSources = new List<AudioSource>();
	private Queue<AudioSource> AvailableAudioSources = new Queue<AudioSource>();
	private int soundFxVolume;
	private IEnumerator soundFxSetterCoRoutine;

    public NovaAudioGroup MusicTheme { get; set; }
    public NovaAudioGroup MusicMenu { get; set; }

    public int SoundFxVolume
	{
		get { return this.soundFxVolume; }
		set 
		{
			this.soundFxVolume = value;
			this.soundFxSetterCoRoutine = this.SetSoundParameterValue(NovaConfig.KEY_SFXVOL, this.soundFxVolume);
			this.StartCoroutine(this.soundFxSetterCoRoutine);	
		}
	}
	private int musicVolume;
	private IEnumerator musicSetterCoRoutine;
	public int MusicVolume
	{
		get {return this.musicVolume;}
		set
		{
			this.musicVolume = value;
			this.musicSetterCoRoutine = this.SetSoundParameterValue(NovaConfig.KEY_MUSICVOL, this.musicVolume);
			this.StartCoroutine(this.musicSetterCoRoutine);	
		}
	}
	
	IEnumerator SetSoundParameterValue(string paramName, int value)
	{
		while(this.AudioMixer == null) { yield return null; }
		yield return null;
		Debug.LogFormat("#Settings# Setting {0}: {1}; {2}", paramName, value, this);
		this.AudioMixer.SetFloat(paramName, EngineUtils.LinearToDecibel(value*0.01f));	
	}
	
	
	void Awake()
	{
		this.IsInitialized = false;
	}
	
	void Start()
	{
		this.SoundFxVolume = PlayerPrefs.GetInt(NovaConfig.KEY_SFXVOL, NovaConfig.DEF_VOLUME);
		this.MusicVolume = PlayerPrefs.GetInt(NovaConfig.KEY_MUSICVOL, NovaConfig.DEF_VOLUME);
		Debug.LogFormat("#Settings# Setting default audio volumes SoundFxVolume:{0} MusicVolume:{1}", this.SoundFxVolume, this.MusicVolume);

        foreach (NovaAudioGroup audioGroup in this.AudioGroups)
        {
            if (audioGroup.Label == "music_theme")
            {
                MusicTheme = audioGroup;
            }
            if (audioGroup.Label == "music_menu")
            {
                MusicMenu = audioGroup;
            }
        }
    }
	
	public AudioSource GetAudioSource()
	{
		AudioSource audioSource = null;
		
		if(this.AudioSourcesRoot == null)
		{
			this.AudioSourcesRoot = new GameObject("AudioSources").transform;
			this.AudioSourcesRoot.transform.SetParent(this.transform);
			for(int i=0;i<AUDIO_SOURCE_POOL_SIZE;i++)
			{
				AudioSource newAudioSource = this.AudioSourcesRoot.gameObject.AddComponent<AudioSource>();
				this.AudioSources.Add(newAudioSource);
				this.AvailableAudioSources.Enqueue(newAudioSource);
			}
		}
		
		if(this.AvailableAudioSources.Count == 0)
		{
			audioSource = this.AudioSourcesRoot.gameObject.AddComponent<AudioSource>();
			this.AudioSources.Add(audioSource);
		}
		else
		{
			audioSource = this.AvailableAudioSources.Dequeue();	
		}
		audioSource.playOnAwake = false;
		return audioSource;
	}
	
	public void ProvideAudioSource(NovaAudioItem audioItem, System.Action onLoadAction, System.Action<float>onProgressAction = null)
	{
		AudioSource audioSource = this.GetAudioSource();
        //Debug.Log("ProvideAudioSource() is loading {0} asynchronously, audioSource is:{1}", audioItem.AssetPath, audioSource);	
        this.StartCoroutine(LoadAudioClipAsync(audioItem, audioSource, onLoadAction, onProgressAction));
	}
	
	IEnumerator LoadAudioClipAsync(NovaAudioItem audioItem, AudioSource audioSource, System.Action onloadAction, System.Action<float> onProgressAction)
	{
		yield return null;
	//#if UNITY_EDITOR
		//audioSource.clip = UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(audioItem.AssetPath);
//#else
		AssetBundleRequest assetRequest = AssetBundleManager.LoadAssetAsync<AudioClip>(audioItem.Label);// this.AssetBundle.LoadAssetAsync(audioItem.AssetPath.ToLower(), typeof(AudioClip));
        while(assetRequest.progress < 1.0f) 
		{
			if(onProgressAction != null) { onProgressAction.Invoke(assetRequest.progress); }
			yield return null;
		}
		audioSource.clip = (AudioClip)assetRequest.asset;
//#endif

        // Let the progress action know the AudioClip is fully loaded into the AudioSource
        if (onProgressAction != null) { onProgressAction.Invoke(1.0f); }
		
		yield return null;

        //Debug.LogFormat("LoadAudioClipAsync() is finished {0} audioSource is:{1}", audioItem.AssetPath, audioSource);	

        // Initialize the AudioItem
        audioItem.AudioSource = audioSource;
		audioSource.volume = audioItem.Volume;
		audioSource.outputAudioMixerGroup = this.AudioGroups.Where(ag=>ag.ID == audioItem.AudioGroupID).First().AudioMixerGroup;
		audioSource.loop = audioItem.IsLooping;
		audioItem.AudioSource = audioSource;
		if(onloadAction != null)
		{
			onloadAction.Invoke();	
		}
	}
	
	public IEnumerator PreloadAudioSource(NovaAudioItem audioItem)
	{
		AudioSource audioSource = this.GetAudioSource();
/*#if UNITY_EDITOR
        if (Application.isEditor)
        {
            audioSource.clip = UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(audioItem.AssetPath);
            yield break;
        }
#endif*/
        var assetLoadRequest = AssetBundleManager.LoadAssetAsync<AudioClip>(audioItem.Label); 
        //var assetLoadRequest = this.AssetBundle.LoadAssetAsync<AudioClip>(audioItem.AssetPath.ToLower());
		yield return assetLoadRequest;
		audioSource.clip = (AudioClip)assetLoadRequest.asset;
		if(audioSource.clip == null)
		{
			Debug.LogError("Could not find " + audioItem.AssetPath.ToLower() + " in the AssetBundle");
		}
		audioSource.volume = audioItem.Volume;
		audioSource.outputAudioMixerGroup = this.AudioGroups.Where(ag=>ag.ID == audioItem.AudioGroupID).First().AudioMixerGroup;
		audioSource.loop = audioItem.IsLooping;
		audioItem.AudioSource = audioSource;
	}
	
	public void RecycleAudioSource(AudioSource audioSource)
	{
		if(this.AudioSources.Contains(audioSource))
		{
			audioSource.Stop();
			audioSource.clip = null;
			this.AvailableAudioSources.Enqueue(audioSource);
		}
	}
	
	public void RecycleAudioSource(NovaAudioItem audioItem)
	{
		this.RecycleAudioSource(audioItem.AudioSource);
		audioItem.AudioSource = null;
	}

    public void CrossfadeToMusicMenu()
    {
        if (MusicMenu == null || MusicMenu.IsPlaying)
        {
            Debug.LogFormat("Don't crossfade. Music Menu: {0}", MusicMenu == null ? "null" : "isPlaying");
            return;
        }
        Debug.LogFormat("Crossfade to Music Menu:");
        MusicMenu.Play();
    }
    public void CrossfadeToMusicTheme()
    {
        if (MusicTheme == null || MusicTheme.IsPlaying)
        {
            Debug.LogFormat("Don't crossfade. Music Theme: {0}", MusicTheme == null ? "null" : "isPlaying");
            return;
        }
        Debug.LogFormat("Crossfade to Music Theme:");
        MusicTheme.Play();
    }
}

public enum PlayType
{
	Single,
	Sequential,
	Random,
	RandomWithReplacement,
}

[System.Serializable]
public class NovaAudioGroup
{
	public string ID;
	public string Label;
	public AudioMixerGroup AudioMixerGroup;
	public List<NovaAudioItem> AudioItems = new List<NovaAudioItem>();
	public int Instances;
	public bool IsLooping = false;
	public bool IsPreloaded;
	public PlayType PlayType;
	public bool IsPlaying
	{
		get 
		{
			if(this.lastPlayedsound != null && this.lastPlayedsound.AudioSource != null)
			{
				return this.lastPlayedsound.AudioSource.isPlaying;
			}	
			return false;
		}
	}
	public NovaAudioCrossfader Crossfader;
	public float CrossfadeDuration;
	private List<NovaAudioItem> sounds = new List<NovaAudioItem>();
	private List<NovaAudioItem> playedSounds = new List<NovaAudioItem>();
	private NovaAudioItem lastPlayedsound;
	private int index = 0;
	private IEnumerator loopCoRoutine = null;
	private System.DateTime? lastPlayedTime;
	
	public void Play()
	{
		int soundIndex = 0;
		this.Play(ref soundIndex);
	}
	
	public void Stop()
	{
		if(this.loopCoRoutine != null)
		{
			NovaAudio.Instance.StopCoroutine(this.loopCoRoutine);	
		}
		
		if(this.lastPlayedsound != null)
		{
			if(lastPlayedsound.AudioSource != null)
			{
				this.lastPlayedsound.AudioSource.Stop();
				if(this.IsPreloaded == false)
				{
                    Debug.Log("recycling audio source");
					NovaAudio.Instance.RecycleAudioSource(this.lastPlayedsound);
				}
			}
		}
	}
	
	public void Play(ref int soundIndex )
	{
		if(this.AudioItems.Count == 0)
		{
			Debug.LogFormat("Cannot play AudioGroup:{0} because it has no AudioSources", this.Label);
			return;
		}
		
		if(this.Crossfader != null)
		{
			// Only crossfade if you are not the current audio
			if(this.Crossfader.CurrentAudio != this)
			{
				// If this group isn't the previous or the current audiogroup, crossfade!
				this.Crossfader.Crossfade(this, this.CrossfadeDuration);
			}
		}
		
		switch(this.PlayType)
		{
			case PlayType.Single:
				soundIndex = 0;
				this.lastPlayedsound = this.AudioItems[soundIndex];
			break;
			case PlayType.Sequential:
				if(index >= this.AudioItems.Count) { index = 0;}
				soundIndex = this.index;
				this.lastPlayedsound = this.AudioItems[soundIndex];
				this.index++;
			break;
			case PlayType.Random:
				if(this.sounds.Count == 0)
				{
					this.sounds = this.AudioItems.Select(a => a).ToList();
					this.playedSounds.Clear();
				}
				
				if(this.sounds.Count > 0)
				{
					int rndIndex = Random.Range(0,this.sounds.Count);
					NovaAudioItem itemToPlay = this.sounds[rndIndex];
					if(itemToPlay == this.lastPlayedsound)
					{
						rndIndex++;
						if(rndIndex >= this.sounds.Count) { rndIndex = 0;}
						itemToPlay = this.sounds[rndIndex];
					}
					this.lastPlayedsound = itemToPlay;
					this.playedSounds.Add(itemToPlay);
					this.sounds.RemoveAt(rndIndex);
					soundIndex = this.AudioItems.IndexOf(this.lastPlayedsound);
				}
                break;
			case PlayType.RandomWithReplacement:
                {
                    soundIndex = Random.Range(0,this.AudioItems.Count);
					this.lastPlayedsound = this.AudioItems[soundIndex];
				}
			break;
		}
        //Debug.Logformat("SoundIndex = {0}", soundIndex);
		if(this.lastPlayedsound != null)
		{
			if(lastPlayedsound.AudioSource == null)
			{
				//NovaAudioItem localAudioItem = this.lastPlayedsound;
				// If the NovaAudioItem doesn't have an audioSoruce, load it asynchnously
				//NovaAudio.Instance.ProvideAudioSource(this.lastPlayedsound,()=>{localAudioItem.AudioSource.Play();});
                NovaAudio.Instance.ProvideAudioSource(this.lastPlayedsound, () => { PlayLastPlayedSound(); });
                //Debug.LogFormat("Provided audio source {0}", lastPlayedsound.Label);
			}
			else
			{
                //Debug.LogFormat("Playing last played audio source {0}", lastPlayedsound.Label);
                // Otherwise just play the sound
                if (this.lastPlayedsound.AudioSource.clip == null)
				{
					Debug.Log("Clip is null");	
				}
				this.PlayLastPlayedSound();	
			}
		}
	}

    private void PlayLastPlayedSound()
    {

        if (lastPlayedsound == null)
        {
            Debug.Log("LastPlayedSound is null on play");
            return;
        }
        else if (lastPlayedsound.AudioSource == null)
        { 
            Debug.LogFormat("LastPlatedSound Audio Source null for {0}", lastPlayedsound.Label);
            return;
        }
        else if (lastPlayedsound.AudioSource.clip == null)
        { 
            Debug.LogFormat("LastPlatedSound Audio Source Clip null for {0}", lastPlayedsound.Label);
            return;
        }
        //else
        //    Debug.LogFormat("Playing {0} AudioSourceIs:{1}", lastPlayedsound.AssetPath, lastPlayedsound.AudioSource );
        
        if (this.lastPlayedTime == null || (System.DateTime.UtcNow - this.lastPlayedTime.Value).TotalMilliseconds > 50)
        {
            this.lastPlayedsound.AudioSource.outputAudioMixerGroup = this.AudioMixerGroup;
            this.lastPlayedsound.AudioSource.Play();
            this.lastPlayedTime = System.DateTime.UtcNow;

            if (this.IsLooping && Application.isPlaying)
            {
                if (this.loopCoRoutine != null)
                {
                    NovaAudio.Instance.StopCoroutine(this.loopCoRoutine);
                }
                this.loopCoRoutine = PlayAfterWaiting(this.lastPlayedsound.AudioSource.clip.length);
                NovaAudio.Instance.StartCoroutine(this.loopCoRoutine);
            }
        }
        else
        {
            Debug.LogFormat("Skipping {0} sounds because it was recently played", lastPlayedsound.AssetPath);
        }
    
	}
	
	// Handles Looping
	private IEnumerator PlayAfterWaiting(float seconds)
    {
        Debug.LogFormat("Audio Loop instigated for {0}s",seconds);
        yield return new WaitForSeconds(seconds);
		Debug.LogFormat("Playing after waiting {0}s", seconds);
		this.Play();
	}
}

[System.Serializable]
public class NovaAudioItem
{
	public string Label;
	public AudioSource AudioSource = null;
	public float Weight = 1.0f;
	public string AssetPath;
	public bool IsLooping;
	public float Volume = 1.0f;
	public string AudioGroupID;
}

[System.Serializable]
public class NovaAudioPlayer
{
	public string AudioGroupID;
	private NovaAudioGroup novaAudioGroup;
	static private NovaAudio novaAudio = null;
	
	public System.Action PlayButtonClicked;

    public NovaAudioPlayer() { }
    public NovaAudioPlayer(string audioGroupId)
    {
        AudioGroupID = audioGroupId;
    }

	public void Play()
	{
		if(novaAudio == null)
		{
			var instance = GameObject.FindObjectOfType<NovaAudio>();
			if(instance)
			{
				novaAudio = instance;
			}
		}
		
		if(novaAudioGroup == null || novaAudioGroup.ID != this.AudioGroupID)
		{
			var audioGroup = novaAudio.AudioGroups.Where(ag => ag.ID == this.AudioGroupID).FirstOrDefault();
			this.novaAudioGroup = audioGroup;
		}

        if (this.novaAudioGroup != null)
        {
            this.novaAudioGroup.Play();
        }
	}
	
	public void Stop()
	{
		if(novaAudio == null)
		{
			var instance = GameObject.FindObjectOfType<NovaAudio>();
			if(instance)
			{
				novaAudio = instance;
			}
		}
		
		if(novaAudioGroup == null || novaAudioGroup.ID != this.AudioGroupID)
		{
			var audioGroup = novaAudio.AudioGroups.Where(ag => ag.ID == this.AudioGroupID).FirstOrDefault();
			this.novaAudioGroup = audioGroup;
		}
		
		if(this.novaAudioGroup != null && this.novaAudioGroup.IsPlaying)
		{
			this.novaAudioGroup.Stop();
		}
	}
}