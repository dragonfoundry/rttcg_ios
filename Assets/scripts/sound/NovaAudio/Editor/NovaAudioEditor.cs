﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;  
using UnityEditorInternal;
using UnityEngine.Audio;
using LinqTools;

[CustomEditor(typeof(NovaAudio))]
public class NovaAudioEditor : Editor 
{
	private Texture2D novaBurst;
	private GUIStyle burstStyle;
	private GUIStyle burstBackground;
	private GUIStyle headerFontStyle;
	private GUIStyle audioHeaderStyle;
	private bool reloadStyles = true;
	private ReorderableList audioGroupList;
	private ReorderableList audioItemList;
	private GUISkin inspectorSkin;
	private SerializedProperty selectedAudioGroup = null;
	private int selectedIndex;
	private Texture2D playTexture;
	private Texture2D stopTexture;
	private enum AudioSourceToggle
	{
		Volume = 0,
		Weight = 1,
		Loop = 2,
		Path = 3,
	}
	
	private bool isPrefab = false;
	private AudioSourceToggle selectedToggle;
	private NovaAudio novaAudioInstance;
	
	/// <summary>
	/// When the NovaAudio starts initialize it
	/// </summary>
	private void OnEnable()
	{
		this.isPrefab = PrefabUtility.GetPrefabParent(serializedObject.targetObject) == null && PrefabUtility.GetPrefabObject(serializedObject.targetObject) != null; // Is a prefab
		
		this.selectedIndex = -1;
		this.reloadStyles = true;
		this.inspectorSkin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
		
		if(serializedObject == null) return;
		
        this.audioGroupList = new ReorderableList(serializedObject, 
                this.serializedObject.FindProperty("AudioGroups"), 
                true, true, false, true);
		this.audioGroupList.drawHeaderCallback = (Rect rect) => {  EditorGUI.LabelField(rect, "Audio Groups"); };	
		this.audioGroupList.drawElementCallback = this.DrawAudioGroupElement;	
		this.audioGroupList.onRemoveCallback = this.RemoveAudioGroup;
	}
	
	/// <summary>
	/// Removes the AudioGroup object at the currently selected list index
	/// </summary>	
	public void RemoveAudioGroup(ReorderableList list)
	{
		ReorderableList.defaultBehaviours.DoRemoveButton(list);
	}
	
	/// <summary>
	/// Main GUI of the editor
	/// </summary>
	public override void OnInspectorGUI()
    {
		this.LoadStyles();
		
		// Cool Header Thing
		EditorGUILayout.BeginHorizontal(this.burstBackground, GUILayout.Height(55));
			EditorGUILayout.LabelField("", this.burstStyle ?? EditorStyles.helpBox, GUILayout.Width(40), GUILayout.Height(40));
			EditorGUILayout.LabelField("NovaAudio", this.headerFontStyle, GUILayout.Height(40) );
		EditorGUILayout.EndHorizontal();	
		
		if(this.novaAudioInstance == null)
		{
			this.novaAudioInstance = GameObject.FindObjectOfType<NovaAudio>();
		}
		
		if(this.selectedAudioGroup == null)
		{
			this.DrawNovaAudio();
		}
		else
		{
			this.DrawNovaAudioGroup();
		}
	}	
	
	///<summary>
	/// Draw the NovaAudioGroup editor after an audio group has been selected from the list
	///</summary>
	private void DrawNovaAudioGroup()
	{
		string label = this.selectedAudioGroup.FindPropertyRelative("Label").stringValue;
		EditorGUILayout.BeginHorizontal(this.inspectorSkin.GetStyle("LargeButtonMid"));
			// Audio Group label
			EditorGUILayout.LabelField(label);
			Texture2D bg = this.playTexture;
			if(this.novaAudioInstance.AudioGroups[this.audioGroupList.index].IsPlaying)
			{
				bg = this.stopTexture;
			}
			
			// Collapse "minus" button
			if(GUILayout.Button(this.inspectorSkin.GetStyle("OL Minus").normal.background,GUILayout.Width(22),GUILayout.Height(EditorGUIUtility.singleLineHeight)))
			{
				this.selectedAudioGroup = null;
				return;
			}
			
			// Play Button
			if(GUILayout.Button(bg,GUILayout.Width(22), GUILayout.Height(EditorGUIUtility.singleLineHeight)))
			{
				NovaAudioGroup currentAudioGroup = this.novaAudioInstance.AudioGroups[this.audioGroupList.index];
				if(currentAudioGroup.IsPlaying)
				{
					currentAudioGroup.Stop();
				}
				else
				{
					int soundIndex = 0;
					currentAudioGroup.Play(ref soundIndex); 
					this.audioItemList.index = soundIndex;
				}
			}
		EditorGUILayout.EndHorizontal();
		
		SerializedProperty instancesProperty = this.selectedAudioGroup.FindPropertyRelative("Instances");
		SerializedProperty isLoopingProperty = this.selectedAudioGroup.FindPropertyRelative("IsLooping");
		SerializedProperty isPreloadedProperty = this.selectedAudioGroup.FindPropertyRelative("IsPreloaded");
		SerializedProperty crossfaderProperty = this.selectedAudioGroup.FindPropertyRelative("Crossfader");
		SerializedProperty crossfadeDurationProperty = this.selectedAudioGroup.FindPropertyRelative("CrossfadeDuration");
		SerializedProperty playTypeProperty = this.selectedAudioGroup.FindPropertyRelative("PlayType");
		
		// Instances
		instancesProperty.intValue = EditorGUILayout.IntSlider("Instances", instancesProperty.intValue,1,10);
		
		// Loop
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PropertyField(isLoopingProperty, new GUIContent("Loop"));
		EditorGUILayout.LabelField("(only works in Play mode)");
		EditorGUILayout.EndHorizontal();
		
		// IsPreloaded
		isPreloadedProperty.boolValue = EditorGUILayout.Toggle("IsPreloaded", isPreloadedProperty.boolValue);
		
		// Crossfader
		NovaAudioCrossfader crossfader = (NovaAudioCrossfader)EditorGUILayout.ObjectField("Crossfader", crossfaderProperty.objectReferenceValue, typeof(NovaAudioCrossfader), true);
		crossfaderProperty.objectReferenceValue = crossfader;
		
		// Crossfade Duration
		if(crossfader != null)
		{
			EditorGUI.indentLevel += 1;
			float val = EditorGUILayout.Slider("Duration", crossfadeDurationProperty.floatValue, 0f, 5f);
			val = (float)Mathf.RoundToInt(val * 4) * 0.25f;
			crossfadeDurationProperty.floatValue = val;
			EditorGUI.indentLevel -= 1;
		}
		
		// Play Type
		EditorGUILayout.PropertyField(playTypeProperty);
		if(GUI.changed)
		{
			this.selectedAudioGroup.serializedObject.ApplyModifiedProperties();
		}
		
		// AudioSources
		serializedObject.Update();
		EditorGUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);
	        this.audioItemList.DoLayoutList();
		EditorGUILayout.EndVertical();
		Rect listRect = GUILayoutUtility.GetLastRect();
		serializedObject.ApplyModifiedProperties();
		this.DropAreaGUI(listRect);
	}
	
	/// <summary>
	/// Draw the default NovaAudioList view
	/// </summary>
	private void DrawNovaAudio()
	{
		serializedObject.Update();
		this.serializedObject.FindProperty("AudioMixer").objectReferenceValue =EditorGUILayout.ObjectField("Audio Mixer", this.serializedObject.FindProperty("AudioMixer").objectReferenceValue, typeof(AudioMixer), false);
		
		if(GUI.changed)
		{
			this.serializedObject.ApplyModifiedProperties();
		}
		// Audo Group List

		EditorGUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);
	        audioGroupList.DoLayoutList();
		EditorGUILayout.EndVertical();
		Rect listRect = GUILayoutUtility.GetLastRect();
        serializedObject.ApplyModifiedProperties();
		this.DropAreaGUI(listRect);
	}
	
	/// <summary>
	/// Lets this editor strech to the edge of the inspector window
	/// </summary>
	public override bool UseDefaultMargins()
	{
		return false;
	}
	
	///<summary>
	/// Draws and AudioGroup element in the Reorderable list
	///</summary>
	private void DrawAudioGroupElement(Rect rect, int index, bool isActive, bool isFocused)
	{
		if(this.isPrefab ) return;
		if(isFocused)
		{
			this.selectedIndex = index;
		}
		
	    var element = audioGroupList.serializedProperty.GetArrayElementAtIndex(index);
		rect.y += 2;
		var labelRect = new Rect(rect.x, rect.y, EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight);
		var mixerRect = new Rect(labelRect.xMax, rect.y, Mathf.Min(172, rect.width - labelRect.width - 55) , labelRect.height);
		var buttonRect = new Rect(rect.width, rect.y,22, labelRect.height);
		var editButtonRect = new Rect(buttonRect);
		editButtonRect.x -= buttonRect.width + 2;
		if(index == this.selectedIndex)
		{
			string stringBefore = element.FindPropertyRelative("Label").stringValue;
			string stringAfter = EditorGUI.TextField(labelRect,stringBefore);
			if(GUI.changed)
			{
				GUI.SetNextControlName("NameLabel");
				element.FindPropertyRelative("Label").stringValue = stringAfter;
			}
		}
		else
		{
			EditorGUI.LabelField(labelRect, element.FindPropertyRelative("Label").stringValue);
		}
		
		if(element.FindPropertyRelative("Crossfader").objectReferenceValue == null)
		{	
			SerializedProperty audioMixerGroupProperty = element.FindPropertyRelative("AudioMixerGroup");
			AudioMixerGroup audioMixerGroup = (AudioMixerGroup)EditorGUI.ObjectField(mixerRect, audioMixerGroupProperty.objectReferenceValue, typeof(AudioMixerGroup),false);
			if(GUI.changed)
			{
				audioMixerGroupProperty.objectReferenceValue = audioMixerGroup;
				this.serializedObject.ApplyModifiedProperties();
			}
		}
		else
		{
			SerializedProperty crossFaderProp = element.FindPropertyRelative("Crossfader");
			if(GUI.Button(mixerRect, crossFaderProp.objectReferenceValue.name))
			{
				EditorGUIUtility.PingObject(crossFaderProp.objectReferenceValue);
			}
		}
		
		if(GUI.Button(editButtonRect, this.inspectorSkin.GetStyle("OL Plus").normal.background))
		{
			this.selectedAudioGroup = element;
			this.audioGroupList.index = index;
			SerializedProperty audioItems = selectedAudioGroup.FindPropertyRelative("AudioItems");

			// Initialize the audioItemsList
			this.audioItemList = new ReorderableList(audioItems.serializedObject,
			    audioItems,
				true,true,false,true);
			this.audioItemList.drawHeaderCallback = this.DrawAudioItemsListHeader;
			this.audioItemList.drawElementCallback = this.DrawAudioItemElement;
			this.audioItemList.onRemoveCallback = this.RemoveAudioItem;
		}
		
		
		Texture2D bg = this.playTexture;
		if(this.novaAudioInstance.AudioGroups[index].IsPlaying)
		{
			bg = this.stopTexture;
		}
		if(GUI.Button(buttonRect, bg))
		{
			if(this.novaAudioInstance.AudioGroups[index].IsPlaying)
			{
				this.novaAudioInstance.AudioGroups[index].Stop();
			}
			else
			{
				Debug.Log("Playing " + this.novaAudioInstance.AudioGroups[index].Label + " AudioGroup");
				this.novaAudioInstance.AudioGroups[index].Play();
			}
		}
	}
	
	/// <summary>
	/// Removes a NovaAudioItem from a NovaAudioGroup
	/// </summary>
	private void RemoveAudioItem(ReorderableList list)
	{
		var element = list.serializedProperty.GetArrayElementAtIndex(list.index);
		AudioSource audioSource = element.FindPropertyRelative("AudioSource").objectReferenceValue as AudioSource;
		if(audioSource != null)
		{
			GameObject.DestroyImmediate(audioSource.gameObject);
		}
		ReorderableList.defaultBehaviours.DoRemoveButton(list);	
	}
	
	/// <summary>
	/// Draws the header fro the NovaAudioItems view
	/// </summary>
	private void DrawAudioItemsListHeader(Rect rect)
	{
		Rect labelRect = new Rect(rect.x,rect.y,100,rect.height);
		EditorGUI.LabelField(labelRect, "Audio Sources");
		Rect toolbarRect = new Rect(rect.width - 176, rect.y + 1, 185,rect.height -1);
		this.selectedToggle = (AudioSourceToggle)GUI.Toolbar(toolbarRect, (int)this.selectedToggle, new string [] {"Volume", "Weight", "Loop", "Path"}, EditorStyles.miniButton);
	}
	
	///<summary>
	/// Draws audio source elements in the AudioGroup's AudioSource list
	///</summary>
	private void DrawAudioItemElement(Rect rect, int index, bool isActive, bool isFocused)
	{
		var element = audioItemList.serializedProperty.GetArrayElementAtIndex(index);
		rect.y += 2;
		var labelRect = new Rect(rect.x, rect.y, EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight);
		var propRect = new Rect(labelRect.xMax, rect.y, Mathf.Max(172, rect.width - labelRect.width - 28) , labelRect.height);
		var buttonRect = new Rect(rect.width, rect.y,22, labelRect.height);
		
		
		EditorGUI.LabelField(labelRect, element.FindPropertyRelative("Label").stringValue);
		
		// Get areference to the current NovaAudioItem serializedProperty
		var audioItemProperty = this.audioItemList.serializedProperty.GetArrayElementAtIndex(index);
		
		SerializedProperty volumeProperty = audioItemProperty.FindPropertyRelative("Volume");
		SerializedProperty weightProperty = audioItemProperty.FindPropertyRelative("Weight");
		SerializedProperty pathProperty = audioItemProperty.FindPropertyRelative("AssetPath");
		SerializedProperty loopProperty = audioItemProperty.FindPropertyRelative("IsLooping");
		
		AudioSource audioSource = (AudioSource)audioItemProperty.FindPropertyRelative("AudioSource").objectReferenceValue;
		
		switch(this.selectedToggle)
		{
			case AudioSourceToggle.Volume:
				float volumeAfter = EditorGUI.Slider(propRect,  volumeProperty.floatValue, 0.0f, 1.0f);
				if(volumeAfter != volumeProperty.floatValue)
				{
					// if there's been a change in the volume slider.. and shift is pressed.. apply it to the other audio sources
					Event e = Event.current;
					if(e.shift)
					{
						int numAudioSources = audioItemList.serializedProperty.arraySize;
						for(int i=0;i<numAudioSources;i++)
						{
							var novaItem = audioItemList.serializedProperty.GetArrayElementAtIndex(i);
							novaItem.FindPropertyRelative("Volume").floatValue = volumeAfter;	
						}
					}
					else
					{
						volumeProperty.floatValue = volumeAfter;
					}
				}
			break;
			case AudioSourceToggle.Weight:
				weightProperty.floatValue =EditorGUI.Slider(propRect, weightProperty.floatValue, 0f, 1f);
			break;
			case AudioSourceToggle.Loop:
				propRect.x += propRect.width * 0.5f -5;
				propRect.width = 25;
				loopProperty.boolValue = EditorGUI.Toggle(propRect,loopProperty.boolValue);
			break;
			case AudioSourceToggle.Path:
				if(GUI.Button(propRect, pathProperty.stringValue))
				{
					AudioClip clip = (AudioClip)AssetDatabase.LoadAssetAtPath<AudioClip>(pathProperty.stringValue);
					EditorGUIUtility.PingObject(clip);
				}
			break;
		}
		
		this.serializedObject.ApplyModifiedProperties();
		
		Texture2D bg = this.playTexture;
		if(audioSource != null && audioSource.isPlaying)
		{
			bg = this.stopTexture;
		}
		
		if(GUI.Button(buttonRect, bg))
		{	
			if(audioSource == null)
			{
				audioSource = this.novaAudioInstance.GetAudioSource();
				audioItemProperty.FindPropertyRelative("AudioSource").objectReferenceValue = audioSource;
				Debug.Log("Loading AudioClip at " + pathProperty.stringValue);
				audioSource.clip = UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(pathProperty.stringValue);
			}
			
			audioSource.volume = volumeProperty.floatValue;
			audioSource.outputAudioMixerGroup = (AudioMixerGroup)this.selectedAudioGroup.FindPropertyRelative("AudioMixerGroup").objectReferenceValue;
			audioSource.loop = loopProperty.boolValue;
			
			if(audioSource.isPlaying)
			{
				audioSource.Stop();
				audioItemProperty.FindPropertyRelative("AudioSource").objectReferenceValue = null;
				this.novaAudioInstance.RecycleAudioSource(audioSource);
			}
			else
			{
				audioSource.Play();
			}
		}
	}
	
	/// <summary>
	/// Handles Drag and drop events on a list (using the lists drop area rectange)
	/// </summary>
    public void DropAreaGUI (Rect drop_area)
    {
        Event evt = Event.current;
        //GUI.Box (drop_area, "Add Trigger");
     
        switch (evt.type) {
        case EventType.DragUpdated:
        case EventType.DragPerform:
            if (!drop_area.Contains (evt.mousePosition))
                return;
             
            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
         
            if (evt.type == EventType.DragPerform) {
                DragAndDrop.AcceptDrag ();
				
				List<Object> draggedObjects = new List<Object>(DragAndDrop.objectReferences);
				draggedObjects = draggedObjects.OrderBy(o => o.name).ToList();
             
                foreach (Object dragged_object in draggedObjects) 
				{
					// Skip any dragged objects that are not AudioClips
					if(dragged_object is AudioClip == false) continue;
                    
					// Do On Drag Stuff here
					Debug.Log(((AudioClip)dragged_object).name);
					if(this.selectedAudioGroup == null)
					{
						this.AddAudioGroup((AudioClip)dragged_object);
					}
					else
					{
						this.AddAudioItem((AudioClip)dragged_object, this.selectedAudioGroup);
					}
                }
            }
            break;
        }
    }
	
	/// <summary>
	/// Adds and audio group (and its first audioItem)
	/// </summary>
	private void AddAudioGroup(AudioClip audioClip)
	{
		var index = this.audioGroupList.serializedProperty.arraySize;
		this.audioGroupList.serializedProperty.arraySize++;
		this.audioGroupList.index = index;
		this.selectedIndex = index;
		var element = audioGroupList.serializedProperty.GetArrayElementAtIndex(index);
		
		// Assign the Properties ot the NovaAudioGroup
		element.FindPropertyRelative("Label").stringValue = audioClip.name;
		element.FindPropertyRelative("Instances").intValue = 1;
		element.FindPropertyRelative("ID").stringValue = System.Guid.NewGuid().ToString();
		element.FindPropertyRelative("Crossfader").objectReferenceValue = null;
		element.FindPropertyRelative("CrossfadeDuration").floatValue = 0.5f;
		element.FindPropertyRelative("PlayType").enumValueIndex = (int)PlayType.Single;
		element.FindPropertyRelative("IsLooping").boolValue = false;
		element.FindPropertyRelative("IsPreloaded").boolValue = false;
		element.FindPropertyRelative("AudioItems").arraySize = 0;
		
		serializedObject.ApplyModifiedProperties();
		Debug.Log("AudioGroupList.index: " + index);
		
		// Make the audio clip the first audioItem in the AudioGroup
		this.AddAudioItem(audioClip, element);
	}	
	
	/// <summary>
	/// Adds an audioItem to an audio group 
	/// </summary>
	private void AddAudioItem(AudioClip audioClip, SerializedProperty serializedAudioGroup)
	{
		Debug.Log("Adding AudioClip " + audioClip.name);
			
		// Find the AudioItems list inside the AudioGroup serilizedOBject
		SerializedProperty audioItemsProperty = serializedAudioGroup.FindPropertyRelative("AudioItems");
		
		// Initalize the AaudioItem inside the AudioGroup
		int index2 = audioItemsProperty.arraySize;
		audioItemsProperty.arraySize++;
		var audioItemProperty = audioItemsProperty.GetArrayElementAtIndex(index2);
		audioItemProperty.FindPropertyRelative("Label").stringValue = audioClip.name;
		audioItemProperty.FindPropertyRelative("Weight").floatValue = 1.0f;
		audioItemProperty.FindPropertyRelative("Volume").floatValue = 1.0f;
		audioItemProperty.FindPropertyRelative("IsLooping").boolValue = false;
		audioItemProperty.FindPropertyRelative("AssetPath").stringValue = AssetDatabase.GetAssetPath(audioClip).ToLower();
		audioItemProperty.FindPropertyRelative("AudioGroupID").stringValue = serializedAudioGroup.FindPropertyRelative("ID").stringValue;
		//audioItemProperty.FindPropertyRelative("AudioSource").objectReferenceValue = audioSource;
			
		serializedObject.ApplyModifiedProperties();
	}
	
	/// <summary>
	/// Helper function to load header / font styles
	/// </summary>
	private void LoadStyles()
	{
		if(this.reloadStyles)
		{
			this.reloadStyles = false;
			this.novaBurst = Resources.Load ("NovaBurst_Small") as Texture2D;
			this.burstBackground = new GUIStyle(EditorStyles.miniButtonMid);
			this.burstBackground.padding.top = 5;
			this.headerFontStyle = new GUIStyle();
			this.headerFontStyle.alignment = TextAnchor.MiddleLeft;
			this.headerFontStyle.fontSize = 28;
			this.audioHeaderStyle = new GUIStyle();
			this.audioHeaderStyle.normal.background = this.inspectorSkin.GetStyle("OL Titlemid").normal.background;
			this.playTexture = this.inspectorSkin.GetStyle("ChannelStripDuckingMarker").normal.background;
			this.stopTexture = this.inspectorSkin.GetStyle("OL Toggle").active.background;
		}
		
		if(this.burstStyle == null && this.novaBurst != null)
		{
			this.burstStyle = new GUIStyle();
			this.burstStyle.normal.background = this.novaBurst;
		}
	}
}
