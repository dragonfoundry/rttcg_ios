﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using LinqTools;

[CustomPropertyDrawer(typeof(NovaAudioPlayer))]
public class NovaAudioPlayerDrawer : PropertyDrawer 
{
	private int index = -1;
	static private NovaAudio novaAudio = null;

	public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent label)
	{	
		if(novaAudio == null)
		{
			var instance = GameObject.FindObjectOfType<NovaAudio>();
			if(instance)
			{
				novaAudio = instance;
			}
			else
			{
                EditorGUI.LabelField(rect, "NovaAudioPlayer", "**Missing NovaAudio**");
				return;
			}
		}
		
		// Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
		EditorGUI.BeginProperty (rect, label, prop);
		
		Rect propertyRect = new Rect (rect.x, rect.y, rect.width - 33, EditorGUIUtility.singleLineHeight);
		Rect buttonRect = new Rect(propertyRect.xMax +2, rect.y, 30, EditorGUIUtility.singleLineHeight);
		string name = prop.name;
		
		List<string> audioGroupList = novaAudio.AudioGroups.Select(ag=>ag.Label).ToList<string>();
		audioGroupList.Insert(0," ");
		string [] audioGroupNames = audioGroupList.ToArray();
		
		string audioGroupID = prop.FindPropertyRelative("AudioGroupID").stringValue;
		NovaAudioGroup currentAudioGroup = null;
		if(audioGroupID != null)
		{
			currentAudioGroup = novaAudio.AudioGroups.Where(ag => ag.ID == audioGroupID).FirstOrDefault();
			this.index = novaAudio.AudioGroups.IndexOf(currentAudioGroup) + 1;
		}
		
		this.index = EditorGUI.Popup(propertyRect,ObjectNames.NicifyVariableName(name), this.index, audioGroupNames);
		
		if(currentAudioGroup != null)
		{
			Texture2D bg = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).GetStyle("ChannelStripDuckingMarker").normal.background;
			if(currentAudioGroup.IsPlaying)
			{
				bg = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).GetStyle("OL Toggle").active.background;
			}
			
			if(GUI.Button(buttonRect,bg ))
			{
				if(currentAudioGroup.IsPlaying)
				{
					currentAudioGroup.Stop();
				}
				else
				{
					currentAudioGroup.Play();
				}
			}
		}
		
		if(GUI.changed)
		{
			if(this.index > novaAudio.AudioGroups.Count || novaAudio.AudioGroups.Count == 0 || this.index <= 0)
			{
				Debug.Log("Erasing instance ID");
				this.index = 0;
				prop.FindPropertyRelative("AudioGroupID").stringValue = null;
			}
			else
			{
				var audioGroup = novaAudio.AudioGroups[index-1];
				prop.FindPropertyRelative("AudioGroupID").stringValue = audioGroup.ID;
			}
		}
		
		EditorGUI.EndProperty ();
	}
}
