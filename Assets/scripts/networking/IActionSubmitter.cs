using UnityEngine;
using System.Collections.Generic;

using Messages;

using DragonFoundry.GameState;

namespace DragonFoundry.Networking
{

	/// <summary>
	/// Provides an interface that allows an action to be submitted to the server.
	/// </summary>
	public interface IActionSubmitter
	{

		/// <summary>
		/// Submit an action to the server.
		/// </summary>
		/// <param name="action">The action to submit.</param>
		void SubmitAction(GameAction action);

		/// <summary>
		/// Concede the current game
		/// </summary>
		void SubmitConcedeGameAction();
	}
}
