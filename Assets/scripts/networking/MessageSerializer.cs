using System;
using System.IO;

//using LostPolygon.System.Net.Sockets;
using System.Net.Sockets;

using Messages;
using DragonFoundry.Common.MessageSocket;

namespace DragonFoundry.Networking
{
	public class MessageSerializer
	{
		private static MySerializer serializer;
		
		private static void InitSerializer()
		{
			if (serializer == null)
			{
				serializer = new MySerializer();
			}
		}
		
		public static int SerializeToClientMessageSocket(ClientMessageSocket socket, MessageBase message)
		{
			InitSerializer();
			
			byte[] allBytes = Serialize(message);
			socket.SendMessage(allBytes);
			
			return allBytes.Length;
		}
		
		public static byte[] Serialize(MemoryStream stream, object message)
		{
			if (message is MessageBase)
			{ // When serializing a message, also write the header
				WriteHeader(stream, message as MessageBase);
			}
			serializer.Serialize(stream, message);
			var toSend = new ArraySegment<Byte>(stream.GetBuffer(), 0, (int)stream.Position);
			return ArraySegmentToArray(toSend);
		}
		
		public static byte[] Serialize(object message)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				return Serialize(stream, message);
			}
		}
		
		public static object DeserializeFromBytes(byte[] message)
		{
            InitSerializer();
            MemoryStream requestStream = new MemoryStream(message);
			return DeserializeFromStream(requestStream);
		}
		
		public static object DeserializeFromStream(Stream stream)
		{
            // Read the version byte; we ignore it at the moment.
            //int version = stream.ReadByte();
            stream.ReadByte();
            //if (version != MessageConstants.MESSAGES_VERSION)
            //	return null;

            int id = stream.ReadByte();

            //Need to wrap this in a try-get.
            //            object result = Activator.CreateInstance(All.Messages[id]);
            //            return serializer.Deserialize(stream, result, All.Messages[id]);
            if (id <= 0)
                return null;
            try {
                Type type;
                MessageConstants.ByteLookup.TryGetValue((byte)id, out type);
                if (type == default(Type))
                    return null;
                object result = Activator.CreateInstance(type);//All.Messages[id]);
                return serializer.Deserialize(stream, result, type);//All.Messages[id]);
            }
            catch (Exception Except)
            {
                throw Except;
            }
		}
		
		// TODO: Convert this to a extension method for ArraySegment<byte>
		private static byte[] ArraySegmentToArray(ArraySegment<byte> segment)
		{
			byte[] result = new byte[segment.Count];
			
			for (int i = 0; i < segment.Count; i++)
			{
				result[i] = segment.Array[segment.Offset + i];
			}
			
			return result;
		}
		
		private static void WriteHeader(MemoryStream stream, MessageBase message)
		{
			// Version
			stream.WriteByte(MessageConstants.MESSAGES_VERSION);
			
			// Message ID
			stream.WriteByte(message.Id);
		}
	}
}