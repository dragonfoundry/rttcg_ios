﻿using UnityEngine;
using System.Collections;
using Messages;
using LinqTools;
using DragonFoundry.GameState;
using System.Collections.Generic;
using DragonFoundry.Controllers;

namespace DragonFoundry.Networking
{
	/// <summary>
	/// An implementation of IGameStateAdapter that will adapt a DiffGameStateResponse.
	/// </summary>
	public class DiffGameStateAdapter : IGameStateAdapter<DiffGameStateMessage>
	{
		#region IGameStateAdapter implementation

		public void AdaptGameState (GameStateContainer gameState, DiffGameStateMessage diffMessage)
		{
			//==================
			// Nova States
			//==================

			// Adapt NovaState to you... if there is one
			if(diffMessage.YouState != null)
			{
				if(gameState.SelfId == 0) { gameState.SelfId = diffMessage.YouState.Id; }
				INovaState selfNova = gameState.GetOrCreatePlayer(diffMessage.YouState.Id);
				this.ApplyNovaState(selfNova, diffMessage.YouState);
			}

			// Adapt NovaState to the opponent... if there is one
			if(diffMessage.OpponentState != null)
            {
                if (gameState.OpponentId == 0) { gameState.OpponentId = diffMessage.OpponentState.Id; }
                INovaState opponentNova = gameState.GetOrCreatePlayer(diffMessage.OpponentState.Id);
				this.ApplyNovaState(opponentNova, diffMessage.OpponentState);
			}

			//==================
			// Turn State
			//==================

			this.AdaptTurn(gameState, diffMessage);
            
			//==================
			// Legal Abilities
			//==================

			if(diffMessage.LegalAbilities != null)
            {
                //Debug.LogFormat("Legal Abilities: {0}/{1} cast abilities/legal", diffMessage.LegalAbilities.Count(a => a.AbilityType == AbilityType.Cast), diffMessage.LegalAbilities.Count(a => a.AbilityType == AbilityType.Cast && a.LegalToPlayAbility));
                // Loop though each legal ability adapting them into the gamestate
                List<int> abilitiesToRemove = new List<int>();
				foreach(LegalAbility legalAbility in diffMessage.LegalAbilities)
				{
					this.AdaptLegalAbility(gameState, legalAbility);

					// If the legal ability is not legal to play, mark it for removal
					if(legalAbility.LegalToPlayAbility == false) 
					{
						abilitiesToRemove.Add(legalAbility.AbilityId);
					}
				}

				// Remove any LegalAbility's that are no longer LegalToPlay
				for(int i=0; i<abilitiesToRemove.Count; i++)
				{
					gameState.LegalAbilities.RemoveLegalAbility(abilitiesToRemove[i]);
				}
			}

			//==================
			// Card States
			//==================
			if(diffMessage.CardStates != null)
			{
				foreach(ICardState card in diffMessage.CardStates)
				{
                    gameState.CurrentStates[card.Id] = card;
                    gameState.AdaptCard(card);
				}
			}

            if(diffMessage.LastKnownStates != null)
            {
                foreach (var state in diffMessage.LastKnownStates)
                {
                    gameState.LastKnownStates[state.Id] = state;
                    if (!gameState.CurrentStates.ContainsKey(state.Id))
                    {
                        gameState.CurrentStates[state.Id] = state;
                        Debug.LogWarningFormat("Card state not found in KnownCardStates: {0}", state.CardId);
                    }
                    if (!gameState.CardExistsInCards(state.Id))
                    { 
                        gameState.AdaptCard(state);
                        Debug.LogWarningFormat("Card state not found in cards: {0}", state.CardId);
                    }
                }
            }

			//==================
			// Combat States
			//==================
			if(diffMessage.CombatStates != null)
			{
				foreach(CombatState combatState in diffMessage.CombatStates)
				{
					this.AdaptCombatState(gameState, combatState);
				}
			}
		}

		#endregion

		/// <summary>
		/// Adapts the game event messages from a DiffGameStateMessage, dispatching them to the scene hierarchy
		/// </summary>
		public void AdaptGameEventMessages(GameObject messageParent,  DiffGameStateMessage diffMessage)
		{
			//==================
			// EventMessages
			//==================
			if(diffMessage.GameEvents != null)
			{
				foreach(EventMessage message in diffMessage.GameEvents)
				{
					// TODO: Eventually move away from unity's broadcast message and have events
					// and event listeners that subscribe to them -DMac
					switch(message.EventType)
					{
					case EventTypes.ZoneChangeEvent:
						messageParent.BroadcastMessage("EventZoneChanged", message);
						break;
					case EventTypes.AttackEvent:
						messageParent.BroadcastMessage("EventAttackEvent", message);
						break;
					case EventTypes.DamageEvent:
						messageParent.BroadcastMessage("EventDamageTaken", message);
						break;
                    case EventTypes.HealthCostEvent:
                        messageParent.BroadcastMessage("EventHealthCost", message);
                        break;
                    case EventTypes.BecomesEvent:
                        messageParent.BroadcastMessage("BecomesNewCard", message);
                        break;
                    case EventTypes.ControlChangeEvent:
						messageParent.BroadcastMessage("EventControlChanged", message);
						break;
					case EventTypes.HealEvent:
						messageParent.BroadcastMessage("EventHeal", message);
						break;
                    case EventTypes.EvadeEvent:
                        messageParent.BroadcastMessage("EventEvade", message);
                        break;
                    case EventTypes.PurifyEvent:
						messageParent.BroadcastMessage("EventPurify", message);
						break;
					case EventTypes.FxEvent:
						messageParent.BroadcastMessage("EventFxEvent", message);
						break;
					case EventTypes.CancelEvent:
						messageParent.BroadcastMessage("EventCancel", message);
						break;
					case EventTypes.PlayerActionFailure:
						messageParent.BroadcastMessage("EventActionFailure", message);
						break;
					case EventTypes.PlayerActionSuccess:
						messageParent.BroadcastMessage("EventActionSuccess", message);
						break;
					}
				}
			}
		}

		/// <summary>
		/// Applies the state of the nova (player) to the existing state instance
		/// </summary>
		private void ApplyNovaState(INovaState existingNovaState, INovaState stateUpdate)
		{
			MatchableState editableNovaState = (MatchableState)existingNovaState;
			editableNovaState.PlayerName = stateUpdate.PlayerName;
			editableNovaState.AvatarName = stateUpdate.AvatarName;
			editableNovaState.Id = stateUpdate.Id;
			editableNovaState.HasCombatAdvantage = stateUpdate.HasCombatAdvantage;
			editableNovaState.Health = stateUpdate.Health;
			editableNovaState.Damage = stateUpdate.Damage;
			editableNovaState.CurrentEnergy = stateUpdate.CurrentEnergy;
			editableNovaState.MaxEnergy = stateUpdate.MaxEnergy;
			editableNovaState.HasWon = stateUpdate.HasWon;
			editableNovaState.HasLost = stateUpdate.HasLost;
			editableNovaState.ArcaneThreshold = stateUpdate.ArcaneThreshold;
			editableNovaState.TechThreshold = stateUpdate.TechThreshold;
			editableNovaState.DivineThreshold = stateUpdate.DivineThreshold;
			editableNovaState.NatureThreshold = stateUpdate.NatureThreshold;
			editableNovaState.ChaosThreshold = stateUpdate.ChaosThreshold;
			editableNovaState.HandSize = stateUpdate.HandSize;
			editableNovaState.StartingHealth = stateUpdate.StartingHealth;
			editableNovaState.DeckSize = stateUpdate.DeckSize;
		}

		/// <summary>
		/// Adapts the turn state into the turndata attached to the GameState
		/// </summary>
		private void AdaptTurn(GameStateContainer gameState, DiffGameStateMessage stateUpdate)
		{
			TurnState turn = gameState.Turn;
			turn.SetState(stateUpdate);
		}

		/// <summary>
		/// Adapts the legal abilities into the GameState
		/// </summary>
		private void AdaptLegalAbility(GameStateContainer gameState, LegalAbility stateUpdate)
		{
			LegalAbility legalAbility = gameState.GetOrCreateLegalAbility(stateUpdate.AbilityId);
			legalAbility.AbilityId = stateUpdate.AbilityId;
			legalAbility.AbilityTypeId = stateUpdate.AbilityTypeId;
			legalAbility.ActivationCost = stateUpdate.ActivationCost;
			legalAbility.LegalToPlayAbility = stateUpdate.LegalToPlayAbility;
			if(stateUpdate.LegalTargetIds != null)
			{
				legalAbility.LegalTargetIds = new List<int>(stateUpdate.LegalTargetIds);
			}
			legalAbility.MaxTargets = stateUpdate.MaxTargets;
			legalAbility.MinTargets = stateUpdate.MinTargets;
			legalAbility.Source = stateUpdate.Source;
		}


		/// <summary>
		/// Adapts a combatstate into its corresponding ICardState
		/// </summary>s
		private void AdaptCombatState (GameStateContainer gameState, CombatState combatState)
		{
			ICardState cardState = gameState.GetCard(combatState.CardId);
			if(cardState == null)
            {
                Debug.LogErrorFormat("Received a combatStat for a card that doesn't exist. {0}", combatState.CardId);
            }

			cardState.AttackTargetId = combatState.AttackTargetId > 0 ? combatState.AttackTargetId : cardState.AttackTargetId;
			cardState.BlockedById = combatState.BlockedById;
			cardState.BlockTargets = combatState.BlockTargets.Count > 0 ? combatState.BlockTargets : cardState.BlockTargets;
			cardState.IsAttacking = combatState.AttackTargetId > 0 ? true : cardState.IsAttacking;
			cardState.IsBlocking = combatState.BlockTargets.Count > 0 ? true : cardState.IsBlocking;
		}
	}
}
