﻿using UnityEngine;
using System.Collections;

public class BackgroundSelector : MonoBehaviour 
{
    
	// Use this for initialization
	void Start () 
    {
        int index = Random.Range(0, transform.childCount);
        GameObject background = transform.GetChild(index).gameObject;
        background.SetActive(true);
	}
	
}
