﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using NovaBlitz.UI;

public class CardArtPositioningToolWindow : EditorWindow 
{
    [MenuItem("Window/Card Art Positioning Window")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		CardArtPositioningToolWindow window = (CardArtPositioningToolWindow)EditorWindow.GetWindow(typeof(CardArtPositioningToolWindow));
		window.name = "Card Art Positioning";
        window.cardArtDictionary = null;
        window.searchText = string.Empty;
        
        window.labelStyle = new GUIStyle(EditorStyles.boldLabel);
        window.labelStyle.normal.textColor = Color.white;
        window.labelStyle.active.textColor = Color.white;
        window.labelStyle.hover.textColor = Color.white;
        window.labelStyle.normal.background = window.MakeTex(1,1,new Color(0,0,0,0.5f));
	}
    
    private GUIStyle labelStyle;
    private string searchText;
    private float rightColumnWidth = 225;
    private Dictionary<string,Texture> cardArtDictionary;
	private Vector2 scrollPosition = Vector2.zero;
    private FrameType selectedFrame = FrameType.CardListItem;
    
    private enum FrameType
    {
        None,
        CardListItem,
        PlayerPortrait,
        Achievement
    }
    
	void OnGUI()
	{
        // Left Column
        Rect rect = new Rect(0,0, position.width - rightColumnWidth, position.height);
        GUILayout.BeginArea(rect, this.labelStyle);
            Rect rect2 = new Rect(rect);
            rect2.width = 115;
            rect2.x += 6;
            rect2.y += 6;
            rect2.height =80;
            GUILayout.BeginArea(rect2, GUI.skin.GetStyle("HelpBox"));
            GUILayout.BeginVertical();
                GUILayout.Label("Frame:");
                if(GUILayout.Toggle(this.selectedFrame == FrameType.CardListItem, "CardListItem")) {this.selectedFrame = FrameType.CardListItem;}
                if(GUILayout.Toggle(this.selectedFrame == FrameType.PlayerPortrait, "PlayerPortrait")) {this.selectedFrame = FrameType.PlayerPortrait;}
                if(GUILayout.Toggle(this.selectedFrame == FrameType.Achievement, "Achievement")) {this.selectedFrame = FrameType.Achievement;}
            GUILayout.EndVertical();
            GUILayout.EndArea();
        GUILayout.EndArea();
        
        // Right Column
        GUILayout.BeginArea(new Rect(position.width - rightColumnWidth, 0, rightColumnWidth, position.height));
            GUILayout.BeginVertical(GUILayout.Width(rightColumnWidth));
                GUILayout.Label("Filter:");
                searchText = GUILayout.TextField(searchText, 25, GUILayout.Width(rightColumnWidth - 20));
                this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition);
                
                this.LayoutCardArtToggles(searchText);
		        GUILayout.EndScrollView();
            GUILayout.EndVertical();
        GUILayout.EndArea();

    }
    
    
    private void LayoutCardArtToggles(string searchText)
    {
        if(this.cardArtDictionary == null)
        {
            this.cardArtDictionary = new Dictionary<string,Texture>();
            
            // get the system file paths of all the files in the asset folder
            string dataPath  = Application.dataPath;
            string folderPath = dataPath + Path.DirectorySeparatorChar + "Card Art";          
            string[] filePathsArray = Directory.GetFiles(folderPath);
            
            // Loop through all the card art in the resources folder
            for(int i=0;i<filePathsArray.Length;i++)
            {
                // Only try to load non .meta files
                if(filePathsArray[i].EndsWith(".meta") == false)
                {
                    // Extract the file name
                    int index = filePathsArray[i].LastIndexOf(Path.DirectorySeparatorChar)+1;
                    string cardArtName = filePathsArray[i].Substring(index, filePathsArray[i].Length - index-4);
                    
                    // Load the file into a texture
                    Texture texture = AssetBundleManager.Instance.LoadAsset<Texture>(cardArtName); //Resources.Load(cardArtName) as Texture;
                    
                    if(texture != null)
                    {
                        // Use it as a string key in the cardArtDictionary
                        this.cardArtDictionary[cardArtName] = texture; 
                    }
                    else
                    {
                        Debug.Log("Unable to load texture: Assets" + Path.DirectorySeparatorChar + "Card Art" + Path.DirectorySeparatorChar + "Resources" +Path.DirectorySeparatorChar + cardArtName );
                    }
                }
            }
        }
        
        int currentIndex = 0;
        bool isFiltered = string.IsNullOrEmpty(searchText) == false;
        
        
        foreach(string id in this.cardArtDictionary.Keys)
        {
            if( isFiltered && id.StartsWith(searchText, true, System.Globalization.CultureInfo.InvariantCulture) == false)
            {
                continue;
            }
            
            if(currentIndex % 2 == 0)
            {
                GUILayout.BeginHorizontal();
            }
            Texture texture = this.cardArtDictionary[id];
            float toggleWidth = 99f;
            float toggleHeight = texture.height * (toggleWidth/texture.width);
            if(GUILayout.Button(texture, GUILayout.Width(toggleWidth), GUILayout.Height(toggleHeight)))
            {
                Debug.Log("Texture:" + texture.name);
            }
            if(isFiltered)
            {
                Rect rect = GUILayoutUtility.GetLastRect();
                rect.x +=6;
                rect.width -= 12;
                rect.y +=6;
                rect.height = labelStyle.CalcHeight(new GUIContent(id), rect.width);
                GUI.Label(rect, id, this.labelStyle);
            }
            
            currentIndex++;
            if(currentIndex % 2 == 0)
            {
                GUILayout.EndHorizontal();
            }
        }
        
    }
    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width*height];
 
        for(int i = 0; i < pix.Length; i++)
            pix[i] = col;
 
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
 
        return result;
    }
 
}
