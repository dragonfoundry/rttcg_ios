﻿using System.IO;

using UnityEngine;
using UnityEditor;
using System.Collections;

namespace DragonFoundry.Editor {

	public static class CopyLibraries {

		[MenuItem("Nova Blitz/Copy Debug Libraries %\\")]
		public static void CopyDebugLibraries()
		{
            // Messages dll
			File.Copy("../messages/bin/Debug/Messages.dll", "Assets/Plugins/Messages.dll", true);

            // Netmq dll if available
            if (Directory.Exists("../../netmq/src/NetMQ/bin/Debug/"))
            {
                File.Copy("../../netmq/src/NetMQ/bin/Debug/netmq-v35.dll", "Assets/Plugins/netmq-v35.dll", true);
            }
			Debug.Log("Copied libraries.");
		}
	}
}