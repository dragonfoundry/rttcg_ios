﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using LinqTools;

public class MemoryWatchWindow : EditorWindow 
{
	private List<GameObjectData> objectsSnapshot = new List<GameObjectData>();
	private Dictionary<int,GameObjectData> objectsDictionary = new Dictionary<int,GameObjectData>();
	private Vector2 scrollPosition = Vector2.zero;
	private GUIStyle objectLabelStyle = null;
	private GUIStyle prefabLabelStyle = null;
	private GUIStyle persistedLabelStyle = null;
	
	protected class GameObjectData 
	{
		public int InstanceId;
		public string ObjectName;
		public GameObject Object;
		public PrefabType PrefabType;
		public string Label;
		public bool IsPersisted = false;
		public bool IsOrigional = false;
		
		public GameObjectData(GameObject obj, bool isOrigional = false)
		{
			this.Object = obj;
			this.ObjectName = obj.name;
			this.PrefabType = PrefabUtility.GetPrefabType(obj);
			this.InstanceId = obj.GetInstanceID();
			this.IsOrigional =  isOrigional;
			this.Label = this.InstanceId.ToString() + " > " + this.ObjectName;
		}
	}
	
	[MenuItem("Window/Memory Watch Window")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		MemoryWatchWindow window = (MemoryWatchWindow)EditorWindow.GetWindow(typeof(MemoryWatchWindow));
		window.name = "Memory Watch";
	}
	
	void OnGUI()
	{
		if(this.objectLabelStyle == null) {this.InitStyles();}
		
		GUILayout.BeginHorizontal(GUILayout.Height(30f));
		if(GUILayout.Button("Scan")) {this.PerformBaseScan();}
		if(GUILayout.Button("Record Transient Objects")) { this.RecordTransients();}
		if(GUILayout.Button("Find Persistant Objects")) {this.PerformCompare();}
		if(GUILayout.Button("Reset")) {this.objectsSnapshot.Clear(); this.objectsDictionary.Clear();}
		GUILayout.EndHorizontal();
		
		this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition);
		if(this.objectsSnapshot.Count > 0)
		{
			for(int i=0;i<this.objectsSnapshot.Count; i++)
			{
				GameObjectData objData = this.objectsSnapshot[i];
				
				if(objData.IsPersisted)
				{
					if(GUILayout.Button(objData.Label + ":" + objData.Object.name, this.persistedLabelStyle))
					{
						EditorGUIUtility.PingObject(objData.Object);
					}
				}
				else
				{
					switch(objData.PrefabType)
					{
						case PrefabType.Prefab:
							if(GUILayout.Button(objData.Label, this.prefabLabelStyle))
							{
								EditorGUIUtility.PingObject(objData.Object);
							}
							break;
						default:
							if(GUILayout.Button(objData.Label, this.objectLabelStyle))
							{
								EditorGUIUtility.PingObject(objData.Object);
							}
							break;	
					}
				}
			}
		}
		GUILayout.EndScrollView();
		
	}
	
	private void PerformBaseScan()
	{
		Object[] snapshot = Resources.FindObjectsOfTypeAll(typeof(GameObject));
		this.objectsSnapshot.Clear();
		this.objectsDictionary.Clear();
		for(int i=0;i<snapshot.Length;i++)
		{
			Object obj = snapshot[i];
			if(obj == null) continue;
			this.objectsSnapshot.Add(new GameObjectData((GameObject)obj, true));	
		}
		
		for(int i=0;i<this.objectsSnapshot.Count; i++)
		{
			GameObjectData objData = this.objectsSnapshot[i];
			this.objectsDictionary[objData.InstanceId] = objData;
		}
	}
	
	private void RecordTransients()
	{
		Object[] snapshot = Resources.FindObjectsOfTypeAll(typeof(GameObject));
		for(int i=0;i<snapshot.Length;i++)
		{
			GameObject obj = (GameObject)snapshot[i];
			if(this.objectsDictionary.ContainsKey(obj.GetInstanceID()) == false)
			{
				GameObjectData objData = new GameObjectData((GameObject)obj);
				this.objectsSnapshot.Add(objData);
				this.objectsDictionary[objData.InstanceId] = objData;
			}
		}
	}
	
	private void PerformCompare()
	{
		Object[] snapshot = Resources.FindObjectsOfTypeAll(typeof(GameObject));
		for(int i=0;i<snapshot.Length;i++)
		{
			GameObject obj = (GameObject)snapshot[i];
			GameObjectData objData = null;
			if(this.objectsDictionary.TryGetValue(obj.GetInstanceID(), out objData))
			{
				if(objData.IsOrigional == false)
				{
					objData.IsPersisted = true;
				}
			}
		}
		
		this.objectsSnapshot = this.objectsSnapshot.Where(obj=>obj.IsPersisted == true).ToList();
		this.objectsDictionary.Clear();
	}
	
	private void InitStyles()
	{
		this.objectLabelStyle = new GUIStyle(EditorStyles.label);
		this.prefabLabelStyle = new GUIStyle(this.objectLabelStyle);
		this.persistedLabelStyle = new GUIStyle(this.objectLabelStyle);
		this.prefabLabelStyle.normal.textColor = new Color(38/255f,90/255f,151/255f);
		this.persistedLabelStyle.normal.textColor = new Color(1f,0,0);
	}
	
	int GetBitsPerPixel(TextureFormat format)
	{
		switch (format)
		{
			case TextureFormat.Alpha8: //	 Alpha-only texture format.
				return 8;
			case TextureFormat.ARGB4444: //	 A 16 bits/pixel texture format. Texture stores color with an alpha channel.
				return 16;
			case TextureFormat.RGBA4444: //	 A 16 bits/pixel texture format.
				return 16;
			case TextureFormat.RGB24:	// A color texture format.
				return 24;
			case TextureFormat.RGBA32:	//Color with an alpha channel texture format.
				return 32;
			case TextureFormat.ARGB32:	//Color with an alpha channel texture format.
				return 32;
			case TextureFormat.RGB565:	//	 A 16 bit color texture format.
				return 16;
			case TextureFormat.DXT1:	// Compressed color texture format.
				return 4;
			case TextureFormat.DXT5:	// Compressed color with alpha channel texture format.
				return 8;
			/*
			case TextureFormat.WiiI4:	// Wii texture format.
			case TextureFormat.WiiI8:	// Wii texture format. Intensity 8 bit.
			case TextureFormat.WiiIA4:	// Wii texture format. Intensity + Alpha 8 bit (4 + 4).
			case TextureFormat.WiiIA8:	// Wii texture format. Intensity + Alpha 16 bit (8 + 8).
			case TextureFormat.WiiRGB565:	// Wii texture format. RGB 16 bit (565).
			case TextureFormat.WiiRGB5A3:	// Wii texture format. RGBA 16 bit (4443).
			case TextureFormat.WiiRGBA8:	// Wii texture format. RGBA 32 bit (8888).
			case TextureFormat.WiiCMPR:	//	 Compressed Wii texture format. 4 bits/texel, ~RGB8A1 (Outline alpha is not currently supported).
				return 0;  //Not supported yet
			*/
			case TextureFormat.PVRTC_RGB2://	 PowerVR (iOS) 2 bits/pixel compressed color texture format.
				return 2;
			case TextureFormat.PVRTC_RGBA2://	 PowerVR (iOS) 2 bits/pixel compressed with alpha channel texture format
				return 2;
			case TextureFormat.PVRTC_RGB4://	 PowerVR (iOS) 4 bits/pixel compressed color texture format.
				return 4;
			case TextureFormat.PVRTC_RGBA4://	 PowerVR (iOS) 4 bits/pixel compressed with alpha channel texture format
				return 4;
			case TextureFormat.ETC_RGB4://	 ETC (GLES2.0) 4 bits/pixel compressed RGB texture format.
				return 4;
			case TextureFormat.ATC_RGB4://	 ATC (ATITC) 4 bits/pixel compressed RGB texture format.
				return 4;
			case TextureFormat.ATC_RGBA8://	 ATC (ATITC) 8 bits/pixel compressed RGB texture format.
				return 8;
			case TextureFormat.BGRA32://	 Format returned by iPhone camera
				return 32;
		}
		return 0;
	}
	
	int CalculateTextureSizeBytes(Texture tTexture)
	{
		int tWidth=tTexture.width;
		int tHeight=tTexture.height;
		if (tTexture is Texture2D)
		{
			Texture2D tTex2D=tTexture as Texture2D;
		 	int bitsPerPixel=this.GetBitsPerPixel(tTex2D.format);
			int mipMapCount=tTex2D.mipmapCount;
			int mipLevel=1;
			int tSize=0;
			while (mipLevel<=mipMapCount)
			{
				tSize+=tWidth*tHeight*bitsPerPixel/8;
				tWidth=tWidth/2;
				tHeight=tHeight/2;
				mipLevel++;
			}
			return tSize;
		}
		
		if (tTexture is Cubemap)
		{
			Cubemap tCubemap=tTexture as Cubemap;
		 	int bitsPerPixel=GetBitsPerPixel(tCubemap.format);
			return tWidth*tHeight*6*bitsPerPixel/8;
		}
		return 0;
	}
	
}
