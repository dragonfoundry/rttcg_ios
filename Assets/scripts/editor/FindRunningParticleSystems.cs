﻿using UnityEngine;
using UnityEditor;

using System;
using System.Collections.Generic;
using LinqTools;
using System.Text;

namespace DragonFoundry.Editor {

    public static class FindRunningParticleSystems
    {

        [MenuItem("Nova Blitz/Find Running Particles")]
        public static void FindRunningParticles()
        {
            var particles = UnityEngine.Object.FindObjectsOfType<ParticleSystem>();

            foreach (var particle in particles) {
                if (particle.isPlaying) { 
                    EditorGUIUtility.PingObject(particle);
                }
            }
            
        }
    }
}
