﻿using System;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
//using UnityEditor.Build;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using NovaBlitz.UI;
using LinqTools;
using Messages;

public class CreateAssetBundles
{
    public static string BuildTarget { get { return EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android ? "Android" : EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.iOS ? "iOS" : "Standalone"; } }
    public static string FolderTarget { get { return string.Format("AssetBundles/{0}/StreamingAssets", BuildTarget); } }
    public static string HashTarget { get { return string.Format("Assets/AssetBundles/{0}", BuildTarget); } }
    public static string BundleTarget { get { return string.Format("Assets/AssetBundles/{0}/StreamingAssets", BuildTarget); } }
    public const string HASHFILE = "MasterAssetBundleList.json";

    [MenuItem ("Assets/Build LZ4 Compressed AssetBundles")]
    static void BuildAllAssetBundles ()
    {
        // Load the bundle build data from file
        string buildListString = File.ReadAllText("Assets/AssetBundles/BundleBuildList.json");
        List<AssetBundleBuildData> buildData = JsonConvert.DeserializeObject <List<AssetBundleBuildData>>(buildListString).OrderBy(d => d.Priority).ToList();
        List<AssetBundleBuild> assetBundleBuilds = new List<AssetBundleBuild>();
        List<string> IncludedWithBuild = new List<string>();
        for(int i = 0; i< buildData.Count; i++)
        {
            var data = buildData[i];
            /*if (data.Folder.StartsWith("CardArt"))
            {
                Debug.LogFormat("Skipping {0}", data.Name);
                continue;
            }*/
            if (data.Priority == 0 || (AssetBundleManager.PLATFORM == "Standalone" && data.ReleaseDate < DateTime.UtcNow))
            {
                IncludedWithBuild.Add(data.Name);
            }
            data.FilePaths = new List<string>();
            foreach(var file in data.FileContents)
            {
                data.FilePaths.Add("Assets/" + data.Folder + file);
            }
            if(data.Name == "novaaudio.unity3d")
            {
                var vfxpath = Path.Combine("sounds", "VFX");
                var uipath = Path.Combine("sounds", "UI");
                IEnumerable<string> vfx = Directory.GetFiles(Path.Combine(Application.dataPath, vfxpath));
                IEnumerable<string> ui = Directory.GetFiles(Path.Combine(Application.dataPath, uipath));
                foreach(var filepath in vfx)
                {
                    if(filepath.EndsWith(".wav"))
                    {
                        var filename = filepath.Split(new char[] { '/','\\' }, StringSplitOptions.None).Last();
                        data.FileContents.Add(filename);
                        data.FilePaths.Add("Assets/sounds/VFX/"+ filename);
                    }
                }
                foreach (var filepath in ui)
                {
                    if (filepath.EndsWith(".wav"))
                    {
                        string filename = filepath.Split(new char[] { '/', '\\' }, StringSplitOptions.None).Last();
                        data.FileContents.Add(filename);
                        data.FilePaths.Add("Assets/sounds/UI/" + filename);
                    }
                }
            }
            assetBundleBuilds.Add(new AssetBundleBuild { addressableNames = data.FileContents.Select(n => n.Split(new char[] { '.' })[0]).ToArray(), assetBundleName = data.Name, assetNames = data.FilePaths.ToArray() });
        }
        Debug.LogFormat("building {0} assetbundles", assetBundleBuilds.Count);
        //BuildPipeline.BuildAssetBundles ("Assets/StreamingAssets", BuildAssetBundleOptions.UncompressedAssetBundle, EditorUserBuildSettings.activeBuildTarget);
        Directory.CreateDirectory(Path.Combine(Application.dataPath, FolderTarget));
        AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles(BundleTarget, assetBundleBuilds.ToArray(), BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
        var bundleList = manifest.GetAllAssetBundles();
        
        Dictionary<string, AssetBundleData> bundleData = new Dictionary<string, AssetBundleData>();
        foreach (var bundleName in bundleList)
        {
            var data = buildData.FirstOrDefault(d => d.Name == bundleName);
            var hash128 = manifest.GetAssetBundleHash(bundleName).ToString();
            var filePath = Path.Combine(BundleTarget, bundleName);
            uint crc;
            BuildPipeline.GetCRCForAssetBundle(filePath, out crc);
            FileInfo f = new FileInfo(filePath);
            bundleData[bundleName] = new AssetBundleData(bundleName, hash128, crc, (int)f.Length / 1024, data.Priority, data.ReleaseDate);
            Debug.LogFormat("Asset bundle evaluated. Name:{0} Hash:{1} CRC:{2} Size:{3}kb", bundleName, hash128, crc, (int)f.Length / 1024);
        }
        string hashSerialized = JsonConvert.SerializeObject(bundleData);
        Debug.LogFormat("{0} Asset Bundles Built: [{1}]",bundleList.Length, hashSerialized);
        File.WriteAllText(Path.Combine(HashTarget, HASHFILE), hashSerialized);

        // Delete all files in the StreamingAssets folder

        // Replace them with the created bundles.
        //var included = File.ReadAllLines("Assets/AssetBundles/IncludedFiles.txt");
        List<AssetBundleData> IncludedBundleData = new List<AssetBundleData>();
        foreach(var fileName in IncludedWithBuild)
        {
            File.Copy(Path.Combine(BundleTarget, fileName), Path.Combine(Application.streamingAssetsPath, fileName), true);
            IncludedBundleData.Add(bundleData[fileName]);
        }

        // do something with IncludedBuildData, so we can load & cache those files.
        File.WriteAllText(Path.Combine(BundleTarget, AssetBundleManager.INCLUDED_BUNDLES), JsonConvert.SerializeObject(IncludedBundleData, Formatting.None));
        File.Copy(Path.Combine(BundleTarget, AssetBundleManager.INCLUDED_BUNDLES), Path.Combine(Application.streamingAssetsPath, AssetBundleManager.INCLUDED_BUNDLES), true);
    }
}

/*class MyCustomBuildProcessor : IPreprocessBuild
{
    public int CallbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        Debug.Log("MyCustomBuildProcessor.OnPreprocessBuild for target " + target + " at path " + path);
    }
}*/

/// <summary>
/// goal here is to copy build bundles from the right directory into the streaming assets for the build.
/// </summary>
[PostProcessBuild]
public class PostProcessAssetBundles
{
    // get the asset bundle manifest from the right folder
    // parse it
    // Push the right bundles to the right places (into the build, or up to S3)
}
#endif

namespace NovaBlitz.UI
{
    public class AssetBundleBuildData
    {
        public string Name;
        public DateTime ReleaseDate;
        public int Priority;
        public string Folder;
        public int ContentsCount;
        public List<string> FileContents;
        public List<string> FilePaths;
    }

}
