using UnityEngine;
using System.Collections;

namespace DragonFoundry.Fx
{
    public class ParticleEffectsTrigger : EffectsTrigger
    {
        public ParticleSystem particles;
        public NovaAudioPlayer Audio;
        private bool isStarting = false;

        protected void Start()
        {
            particles.Stop();
            particles.Clear();
        }
        
        void OnDisable()
        {
            this.EndEffect();
        }

        public override void StartEffect()
        {
			if (!this.gameObject.activeSelf)
				this.gameObject.SetActive (true);
            this.StartCoroutine(DelayStart());
        }

        protected IEnumerator DelayStart()
        {
            isStarting = true;
            yield return null;
            particles.Stop(true);
            particles.Play(true);
            if(this.Audio != null)
            {
                this.Audio.Play();
            }
            isStarting = false;
        }

        public override void EndEffect()
        {
            if (isStarting)
                this.StopCoroutine(DelayStart());
			// PB HACK: Turning off ending sound effects when the effect ends
			// (may want to only do this if the effect isn't a looping effect)
			if(this.Audio != null /*&& this.Audio.novaAudioGroup != null && this.Audio.novaAudioGroup.IsLooping*/)
            {
                //this.Audio.Stop(); 
            }
            particles.Stop(true);
            particles.Clear();

			//if (this.gameObject.activeSelf)
				this.gameObject.SetActive (false);
            isStarting = false;
        }
    }
}