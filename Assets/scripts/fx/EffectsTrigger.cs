﻿using UnityEngine;
using System.Collections;

namespace DragonFoundry.Fx { 

    public class EffectsTrigger : MonoBehaviour {

        public string startName;
        public string endName;

        public void TriggerEffect(string effectName)
        {
            if (startName == effectName)
            {
                StartEffect();
            }

            if (endName == effectName)
            {
                EndEffect();
            }
        }

        public virtual void StartEffect() { }

		public virtual void EndEffect() { }

    }
}