﻿using UnityEngine;
using System;

namespace DragonFoundry.Fx
{
    public class ContinuousEffect
    {
        private bool wasOn = false;
        private string startTrigger;
        private string stopTrigger;

		private String hi;

        public ContinuousEffect(string startTrigger, string stopTrigger)
        {
            this.startTrigger = startTrigger;
            this.stopTrigger = stopTrigger;
        }

        public void IsOn(bool isOn, MonoBehaviour eventRoot)
        {
            if (isOn && !wasOn)
            {
                eventRoot.BroadcastMessage("TriggerEffect", startTrigger, SendMessageOptions.DontRequireReceiver);
            }

            if (!isOn && wasOn)
            {
                eventRoot.BroadcastMessage("TriggerEffect", stopTrigger, SendMessageOptions.DontRequireReceiver);
            }

            wasOn = isOn;
        }
    }
}
