﻿using UnityEngine;
using ProtoBuf;
using System.Collections.Generic;
using LinqTools;

using Messages;

namespace DragonFoundry.DebugTools {

	[ProtoContract]
	public class MessageLog
	{
		private const int DEFAULT_MAX_LOG_SIZE = 1800;

		private int maximumLogSize;
		private List<MessageLogEntry> recycledEntries;

		[ProtoMember(1)]
		public List<MessageLogEntry> Log { get; set; }

		public MessageLog() : this(DEFAULT_MAX_LOG_SIZE) { }

		public MessageLog(int maximumLogSize)
		{
			this.maximumLogSize = maximumLogSize;
			Log = new List<MessageLogEntry>(this.maximumLogSize > 0 ? this.maximumLogSize : DEFAULT_MAX_LOG_SIZE);
			recycledEntries = new List<MessageLogEntry> ();
		}

		public MessageLog(IEnumerable<MessageLogEntry> logEntries)
		{
			this.maximumLogSize = DEFAULT_MAX_LOG_SIZE;
			Log = new List<MessageLogEntry>(logEntries);
			recycledEntries = new List<MessageLogEntry>();
		}

		public void AddLogMessage(MessageBase message)
		{
			AddLogMessage(message, Time.time);
		}

		public MessageLog Slice(float startTime, float endTime)
		{
			return new MessageLog(from e in Log where e.timestamp > startTime && e.timestamp < endTime select new MessageLogEntry (e.timestamp - startTime, e.message));
		}

		public void AddLogMessage(MessageBase message, float timestamp)
		{
			if (maximumLogSize > 0 && Log.Count >= maximumLogSize) {
				MessageLogEntry oldEntry = Log[0];
				Log.RemoveAt(0);
				RecycleLogEntry(oldEntry);
			}
			
			MessageLogEntry logEntry = NewLogEntry();
			logEntry.timestamp = timestamp;
			logEntry.message = message;
			Log.Add(logEntry);
		}

		public override string ToString ()
		{
			string logString = "Log with " + Log.Count + " entries\n";
			foreach (MessageLogEntry entry in Log)
			{
				logString += entry.ToString() + "\n";
			}

			return logString;
		}

		public void SerializeToFile(string filename)
		{
			using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.OpenOrCreate)) {
//				Serializer.Serialize(fs, this);
			}
		}

		public static MessageLog DeserializeFromFile(string filename)
		{
			using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Open)) {
//				MessageLog log = Serializer.Deserialize<MessageLog>(fs);
//				log.maximumLogSize = -1;
//				return log;
				return null;
			}
		}

		/// <summary>
		/// Fetches a log entry from the object pool. Temporarily just constructs a new object.
		/// </summary>
		/// <returns>A new log entry.</returns>
		private MessageLogEntry NewLogEntry()
		{
			if (recycledEntries.Count > 0) {
				MessageLogEntry entry = recycledEntries[0];
				recycledEntries.RemoveAt(0);

				entry.ClearAll();
				return entry;
			}

			return new MessageLogEntry();
		}

		private void RecycleLogEntry(MessageLogEntry logEntry)
		{
			recycledEntries.Add(logEntry);
		}

		[ProtoContract]
		public class MessageLogEntry {

			[ProtoMember(1)]
			public float timestamp;

			[ProtoMember(2)]
			public MessageBase message;

			public MessageLogEntry() {}
			public MessageLogEntry(float timestamp, MessageBase message)
			{
				this.timestamp = timestamp;
				this.message = message;
			}

			public void ClearAll()
			{
				timestamp = 0;
				message = null;
			}

			public override string ToString ()
			{
				return string.Format("[{0}:{1}] {2}", timestamp / 60, timestamp % 60, message.ToString());
			}
		}
	}
}	