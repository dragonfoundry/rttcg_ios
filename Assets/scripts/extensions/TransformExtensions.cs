﻿using UnityEngine;
using System.Collections;

public static class TransformExtensions 
{

	/// <summary>
	/// Helper function that can set the transform's position to be at the specified world position.
	/// Ideal usage: positioning a UI element to be directly over a 3D point in space.
	/// </summary>
	/// <param name="worldPos">World position, visible by the worldCam</param>
	/// <param name="worldCam">Camera that is able to see the worldPos</param>
	/// <param name="myCam">Camera that is able to see the transform this function is called on</param>
	static public void OverlayPosition (this Transform trans, Vector3 worldPos, Camera worldCam, Camera myCam)
	{
		worldPos = worldCam.WorldToViewportPoint(worldPos);
		worldPos = myCam.ViewportToWorldPoint(worldPos);
		Transform parent = trans.parent;
		trans.localPosition = (parent != null) ? parent.InverseTransformPoint(worldPos) : worldPos;
	}

	/// <summary>
	/// Helper function that can set the transform's position to be at the specified world position.
	/// Ideal usage: positioning a UI element to be directly over a 3D point in space.
	/// </summary>
	/// <param name="worldPos">World position, visible by the worldCam</param>
	/// <param name="worldCam">Camera that is able to see the worldPos</param>
	static public void OverlayPosition (this Transform trans, Vector3 worldPos, Camera worldCam)
	{
		Camera myCam = FindCameraForLayer(trans.gameObject.layer);
		if (myCam != null) trans.OverlayPosition(worldPos, worldCam, myCam);
	}

	/// <summary>
	/// Helper function that can set the transform's position to be over the specified target transform.
	/// Ideal usage: positioning a UI element to be directly over a 3D object in space.
	/// </summary>
	/// <param name="target">Target over which the transform should be positioned</param>
	static public void OverlayPosition (this Transform trans, Transform target)
	{
		Camera myCam = FindCameraForLayer(trans.gameObject.layer);
		Camera worldCam = FindCameraForLayer(target.gameObject.layer);
		if (myCam != null && worldCam != null) trans.OverlayPosition(target.position, worldCam, myCam);
	}

    /// <summary>
    /// Find the camera responsible for drawing the objects on the specified layer.
    /// </summary>

    static public Camera FindCameraForLayer(int layer)
    {
        int layerMask = 1 << layer;

        Camera cam;
        cam = Camera.main;
        if (cam && (cam.cullingMask & layerMask) != 0) return cam;

#if UNITY_4_3 || UNITY_FLASH
		Camera[] cameras = NGUITools.FindActive<Camera>();
		for (int i = 0, imax = cameras.Length; i < imax; ++i)
#else
        Camera[] cameras = new Camera[Camera.allCamerasCount];
        int camerasFound = Camera.GetAllCameras(cameras);
        for (int i = 0; i < camerasFound; ++i)
#endif
        {
            cam = cameras[i];
            if (cam && cam.enabled && (cam.cullingMask & layerMask) != 0)
                return cam;
        }
        return null;
    }

}
