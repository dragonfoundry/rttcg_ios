﻿using UnityEngine;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Messages;
using NovaBlitz.UI;
using DragonFoundry.GameState;
using DragonFoundry.Controllers.Display;

public static class ICardStateExtensions 
{
	public static bool IsDiscarded(this ICardState input)
	{
		return input.Zone == CardZone.Trash || input.Zone == CardZone.Void;
	}

    public static string GetNameLocId(this ICardState input)
    {
        if (input == null)
            return "null CardState";
        CardData card = null;
        if (GameData.CardDictionary.TryGetValue(input.CardId, out card))
        {
            return card.NameLocId;
        }
        else
        {
            Debug.Log("Could not find CardCatalog entery for CardID:" + input.CardId);
            return string.Empty;
        }
    }

    public static string GetName(this ICardState input)
	{
        if (input == null)
            return "null CardState";
		CardData card = null;
		if(GameData.CardDictionary.TryGetValue(input.CardId, out card))
		{
            return card.Name;
        }
		else
		{
			Debug.Log ("Could not find CardCatalog entery for CardID:" + input.CardId);
			return string.Empty;
		}
	}

	public static string GetSubtype(this ICardState input)
	{
        if (input.Subtype == 0)
            return string.Empty;

        string subtype;
        if (GameData.SubtypesById.TryGetValue(input.Subtype, out subtype))
        {
            var locSubtype = I2.Loc.ScriptLocalization.Get(GameData.SUBTYPE_DATA + subtype);
            if (string.IsNullOrEmpty(locSubtype))
                return subtype;
            else
                return locSubtype;
        }
        CardData card = null;
        if (GameData.CardDictionary.TryGetValue(input.CardId, out card))
        {
            return card.Subtype;
        }
        else
        {
            Debug.LogFormat("Could not find CardCatalog entery for CardID:{0}", input.CardId);
            return string.Empty;
        }
    }

	public static CardAspect GetAspect(this ICardState input)
	{
        if (input.IsArcane)
            return CardAspect.Arcane;
        else if (input.IsTech)
            return CardAspect.Tech;
        else if (input.IsDivine)
            return CardAspect.Divine;
        else if (input.IsNature)
            return CardAspect.Nature;
        else if (input.IsChaos)
            return CardAspect.Chaos;
        else
            return GameData.CardDictionary[input.CardId].Aspect;
	}

	public static string GetArtId(this ICardState input)
	{
		return GameData.CardDictionary[input.CardId].ArtID;
	}

    public const string ACTIVATE_REGEX = "[0-9]+: ";

    public static string GetEffectText(this ICardState input, bool isPurified)
	{
		CardData baseCard = GameData.CardDictionary[input.CardId];
		// Calculate the text based on original AbilityText and keywords

		var builder = new StringBuilder ();

		var keywordBuilders = new List<StringBuilder> ();
        // Make added/changed keywords blue
        foreach(var keyword in input.Keywords)
        {
            double val = 0;
            baseCard.NewHotnessKeywords.TryGetValue(keyword.Key, out val);
            KeywordData data;
            if(GameData.KeywordsById.TryGetValue(keyword.Key, out data) && (data.Type == KeywordType.Skill || data.Type == KeywordType.Condition) && (keyword.Value != 0 || val !=0))
            {
                keywordBuilders.Add(GetKeywordString(new StringBuilder(), data, keyword.Value, (int)val));
            }
        }

		for (int i = 0; i < keywordBuilders.Count; i++) 
		{
			builder.Append(keywordBuilders[i]);
            if (i < keywordBuilders.Count - 1)
            {
                builder.Append(',');
                builder.Append(' ');
            }
            else
                builder.Append('\n');
		}

        double oneShot;
        int currentOneShot;
        if(baseCard.NewHotnessKeywords.TryGetValue(GameData.OneShotId, out oneShot) && oneShot != 0 && (!input.Keywords.TryGetValue(GameData.OneShotId, out currentOneShot) || currentOneShot == 0))
        {
            isPurified = true;
        }

		if(isPurified)
			builder.Append(CardData.STRIKETHROUGH_OPEN).Append(CardData.GREY_OPEN);

        // modify effect text to show actual, current, activation cost
        var baseText = GameData.CardDictionary[input.CardId].EffectText;
        if(Regex.IsMatch(baseText, ACTIVATE_REGEX))
        {
            int oldActivationCost = baseCard.ActivationCost.HasValue ? baseCard.ActivationCost.Value : 0;
            int activationCost = input.ActivationCost;
            baseText = Regex.Replace(baseText, ACTIVATE_REGEX, string.Empty);
            builder.Append(activationCost > oldActivationCost ? CardData.RED_OPEN : activationCost < oldActivationCost ? CardData.BLUE_OPEN : string.Empty);
            builder.Append(activationCost).Append(':').Append(' ');
            builder.Append(activationCost != oldActivationCost ? CardData.COLOUR_CLOSE : string.Empty);
        }

        builder.Append (baseText);
		//string abilityString = CardCatalog.Cards [input.CardId].AbilityText;
		if(isPurified)
			builder.Append(CardData.STRIKETHROUGH_CLOSE).Append(CardData.COLOUR_CLOSE);

        if (builder.Length > 90)
        {
            int textlength = 0;
            bool inBrackets = false;
            for (int i = 0; i < builder.Length; i++)
            {
                if (builder[i] == '<')
                {
                    inBrackets = true;
                    continue;
                }
                else if (builder[i] == '>')
                {
                    inBrackets = false;
                    continue;
                }
                else if (!inBrackets)
                {
                    textlength++;
                }
            }
            if (textlength < 90)
            {
                builder.Insert(0, CardData.SHORT_PARAGRAPH);
            }
            else
            {
                inBrackets = false;
                for (int i = builder.Length - 1; i > 0; i--)
                {
                    if (builder[i] == '>')
                    {
                        inBrackets = true;
                        continue;
                    }
                    else if (builder[i] == '<')
                    {
                        inBrackets = false;
                        continue;
                    }
                    if (!inBrackets)
                    {
                        builder.Insert(i, CardData.SHORT_LAST_LINE);
                        break;
                    }
                }
            }
        }
        else if (builder.Length > 0)
        {
            builder.Insert(0, CardData.SHORT_PARAGRAPH);
        }

        return builder.ToString();
    }

    private static StringBuilder GetKeywordString(StringBuilder keywordBuilder, KeywordData data, int currentValue, int baseValue)
    {
        //StringBuilder keywordBuilder = new StringBuilder();
        //keywordBuilder.Append("<b>");
        // We know keywordValue isn't 0; if it's not equal to the original, make the text blue.

        if (currentValue == 0 && baseValue == 0)
            return keywordBuilder;
        if (currentValue == 0)
        {
            keywordBuilder.Append(CardData.GREY_OPEN).Append(CardData.STRIKETHROUGH_OPEN);
        }
        else if (data.Type == KeywordType.Condition)
        {
            keywordBuilder.Append(CardData.RED_OPEN);
        }
        else if (currentValue != baseValue)
        {
            keywordBuilder.Append(CardData.GREEN_OPEN);
        }
        else
            keywordBuilder.Append(CardData.BLUE_OPEN);

        string keywordName = I2.Loc.ScriptLocalization.Get(GameData.KEYWORD_DATA + data.Name);

        // Add the keyword text
        keywordBuilder.Append(keywordName);
        // Add the number if it's above 0
        if (data.ShowNumber && currentValue > 0)
        {
            keywordBuilder.Append(' ');
            keywordBuilder.Append(currentValue);
        }
            
        keywordBuilder.Append(CardData.COLOUR_CLOSE);

        if (currentValue == 0 && baseValue != 0)
        {
            keywordBuilder.Append(CardData.STRIKETHROUGH_CLOSE);
        }
        //keywordBuilder.Append("</b>");
        return keywordBuilder;
    }
    
	public static string GetFxScript(this ICardState input)
	{
        try
        {

            string fxScript = GameData.CardDictionary[input.CardId].FxScript;
            return string.IsNullOrEmpty(fxScript) ? string.Empty : fxScript.ToLower();
        }
        catch
        {
            Debug.LogError("FX script exception: " + input != null ? input.CardId.ToString() : "input is null");
            return string.Empty;
        }
	}

	public static int GetCost(this ICardState input)
	{
		return GameData.CardDictionary[input.CardId].Cost;
	}
	
	public static int GetOriginalEnergyCost(this ICardState input)
	{
		CardData cardData = GameData.CardDictionary[input.CardId];
		return cardData.Cost;
	}

	public static CardRarity GetRarity(this ICardState input)
	{
		CardData cardData = GameData.CardDictionary[input.CardId];
		return cardData.Rarity;
	}

	public static int GetOriginalHealth(this ICardState input)
	{
        if(input == null)
        {
            //Debug.LogException(new DataMisalignedException("Get Original Health failed"));
            return 0;
        }
		CardData cardData = GameData.CardDictionary[input.CardId];
		if(cardData != null && cardData.CardType == CardType.Unit)
		{
			UnitCard unitCard = (UnitCard)cardData;
			return unitCard == null ? 0 : unitCard.Health;
		}
		else
		{
			return 0;
		}
	}

    public static int GetStat(this ICardState input, KeywordEnum keywordEnum)
    {
        KeywordData keywordData;
        if (GameData.KeywordsByEnum.TryGetValue(keywordEnum, out keywordData))
        {
            int value;
            input.Keywords.TryGetValue(keywordData.Id, out value);
            return value;
        }
        return 0;
    }

public static int GetOriginalAtk(this ICardState input)
	{
		CardData cardData = GameData.CardDictionary[input.CardId];
		if(cardData.CardType == CardType.Unit)
		{
			UnitCard unitCard = (UnitCard)cardData;
			return unitCard.AttackPower;
		}
		else
		{
			throw new System.ArgumentException("Attepting to get AttackPower of a card that is not a Unit");
		}
	}

	public static int GetCurrentHealth(this ICardState input)
	{
		return input.Health - input.Damage;
	}
}
