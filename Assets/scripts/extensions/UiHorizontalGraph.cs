﻿using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/UiHorizontalGraph")]
    public class UiHorizontalGraph : UiLineRenderer
    {
        public int BaseHeight = -280;
        
        protected override UIVertex[] CreateLineSegment(Vector2 start, Vector2 end, SegmentType type)
        {
            //List<UIVertex> points = new List<UIVertex>();

            var uvs = middleUvs;
            if (type == SegmentType.Start)
                uvs = startUvs;
            else if (type == SegmentType.End)
                uvs = endUvs;

            Vector2 offset = new Vector2(start.y - end.y, end.x - start.x).normalized * LineThickness / 2;
            var v1 = new Vector2(start.x, BaseHeight);// start - offset;
            var v2 = start + offset;
            var v3 = end + offset;
            var v4 = new Vector2(end.x, BaseHeight);// end - offset;
            return SetVbo(new[] { v1, v2, v3, v4 }, uvs);
        }
    }
}