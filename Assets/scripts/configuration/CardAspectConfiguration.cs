﻿using UnityEngine;
using System.Collections;

namespace DragonFoundry.Configuration
{
    public class CardAspectConfiguration : MonoBehaviour
    {
        public string aspectTag;

		public Sprite unitTexture;
        public Sprite powerTexture;
        public Sprite itemTexture;
		public Sprite titleHolder;
        public Sprite abilityCostFrame;
		public Texture2D powerTop;
		public Texture2D powerBottom;
		public Texture2D unitTop;
		public Texture2D unitBottom;
    }
}