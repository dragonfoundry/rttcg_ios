﻿using UnityEngine;
using Messages;

namespace DragonFoundry.GameState
{
    public class TurnState
    {
		private int serverTicksRemaining;
		private float lastUpdateTimestamp;
		public float ticksPerSecond = 10f;
		private Phase phaseName;
		private int turnNumber;
		private int blitzTicksRemaining;
        public bool IsPaused;
        public bool IsTimerOff;

        public Phase Phase {get { return this.phaseName;}}
		public int TurnNumber { get { return this.turnNumber; } }
		public float BlitzTicksRemaining
        {
            get
            {
                // Returns 5 ticks less than the actual bonus ticks remaining.
                float blitzTicksRemaining = this.blitzTicksRemaining - (Time.time - this.lastUpdateTimestamp) * this.ticksPerSecond - 5;
                return blitzTicksRemaining;
            }
        }
		//public String PhaseName { get { return this.phaseName.ToString(); } }
		public bool HasStarted { get { return this.lastUpdateTimestamp > 0f; } }
		public float TicksRemaining
		{
			get 
			{
				if(this.IsPaused || this.IsTimerOff)
				{
					 return this.serverTicksRemaining;
				}
				else
				{
					float ticksRemaining = this.serverTicksRemaining - (Time.time - this.lastUpdateTimestamp) * this.ticksPerSecond;
					return ticksRemaining;
				}
			}
		}

		public void SetState(DiffGameStateMessage stateUpdate)
		{
			this.phaseName = stateUpdate.PhaseName;
			this.serverTicksRemaining = stateUpdate.TicksRemaining;
			this.blitzTicksRemaining = stateUpdate.BonusTicksRemaining;
			this.ticksPerSecond = (float) stateUpdate.TickConstant;
			this.lastUpdateTimestamp = Time.time;
			this.turnNumber = stateUpdate.GameTurnNumber;
            this.IsPaused = stateUpdate.IsPaused;
            this.IsTimerOff = stateUpdate.IsTimerOff == true;
		}
    }
}
