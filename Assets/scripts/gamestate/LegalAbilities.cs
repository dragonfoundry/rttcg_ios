﻿using UnityEngine;
using System.Collections;
using Messages;
using System.Collections.Generic;
using LinqTools;
using DragonFoundry.GameState;

public class LegalAbilities 
{
	private Dictionary<int, LegalAbility> legalAbilities = new Dictionary<int, LegalAbility>();

	/// <summary>
	/// Gets the number legal abilities in the cache
	/// </summary>
	public int NumLegalAbilities
	{
		get { return this.legalAbilities.Count; }
	}

	/// <summary>
	/// Gets the or create legal ability.
	/// </summary>
	public LegalAbility GetOrCreateLegalAbility(int abilityId)
	{
		if(this.legalAbilities.ContainsKey(abilityId) == false)
		{
			LegalAbility legalAbility = new LegalAbility();
			legalAbility.AbilityId = abilityId;
			this.legalAbilities[abilityId] = legalAbility;
		}
		
		return this.legalAbilities[abilityId];
	}

	/// <summary>
	/// Removes a legal ability from the local cache by abilityId
	/// </summary>
	public void RemoveLegalAbility(int abilityId)
	{
		if(this.legalAbilities.ContainsKey(abilityId))
		{
            LegalAbility legalAbility = this.legalAbilities[abilityId];
			this.legalAbilities.Remove(abilityId);
		}
	}

    /// <summary>
    /// Determins if the sourceId's "Cast" ability requires 1 or more targets
    /// </summary>
    public bool ActivateRequiresTarget(int sourceId)
    {
        LegalAbility activateAbility = this.GetActivateAbility(sourceId);
        if (activateAbility != null)
        {
            return activateAbility.MinTargets > 0;
        }

        return false;
    }

    /// <summary>
    /// Determins if the sourceId's "Cast" ability requires 1 or more targets
    /// </summary>
    public bool CastRequiresTarget(int sourceId)
	{
		LegalAbility castAbility = this.GetCastAbility(sourceId);
		if(castAbility != null)
		{
			return castAbility.MinTargets > 0;
		}

		return false;
    }

    /// <summary>
    /// Gets a list of legal targetIds for the sourceId's Activate action.
    /// </summary>
    public List<int> GetActivateTargets(int sourceId, out bool isTargeted)
    {
        LegalAbility activatedAbility = this.legalAbilities.Values.Where(la => la.Source == sourceId && la.AbilityType == AbilityType.Activated).FirstOrDefault();
        isTargeted = false;
        if (activatedAbility != null)
        {
            isTargeted = activatedAbility.MaxTargets > 0;
            return activatedAbility.LegalTargetIds;
        }

        return null;
    }

    /// <summary>
    /// Gets a list of legal targetIds for the sourceId's Block action.
    /// </summary>
    public List<int> GetBlockTargets(int sourceId)
	{
		LegalAbility blockAbility = this.legalAbilities.Values.Where(la=>la.Source == sourceId && la.AbilityType == AbilityType.Block).FirstOrDefault();

		if( blockAbility != null)
		{
			return blockAbility.LegalTargetIds;
		}

		return null;
	}

	/// <summary>
	/// Gets a list of legal targetIds for the sourceId's cast action.
	/// </summary>
	public List<int> GetCastTargets(int sourceId, out bool isTargeted)
	{
		LegalAbility castAbility = this.legalAbilities.Values.Where(la=>la.Source == sourceId && la.AbilityType == AbilityType.Cast).FirstOrDefault();
        isTargeted = false;
		if( castAbility != null)
		{
            isTargeted = castAbility.MaxTargets > 0;
			return castAbility.LegalTargetIds;
		}
		
		return null;
	}

    /// <summary>
    /// Used in the tutorial, to determine whether we should be showing the "got it" button and the scrim, or just letting the player's game action dismiss the tutorial.
    /// </summary>
    /// <returns></returns>
    public bool CanTakeUsefulGameAction()
    {
        return this.legalAbilities.Values.Any(a => a.LegalToPlayAbility && a.AbilityType != AbilityType.DoneWithPhase && a.AbilityType != AbilityType.PauseUnpause);
    }

	/// <summary>
	/// Determines if there is a legal attack ability between the sourceId and targetId
	/// </summary>
	public bool CanAttack(int sourceId, int? targetId = null) 
	{
		return this.GetAttackAbility(sourceId, targetId) != null;
	}

	/// <summary>
	/// Determines if there is a legal Block ability between the sourceId and targetId
	/// </summary>
	public bool CanBlock(int sourceId, int? targetId = null)
	{
		return this.GetBlockAbility(sourceId, targetId) != null;
	}

	/// <summary>
	/// Determines if the sourceId has an available Cast ability.
	/// </summary>
	public bool CanCast(int sourceId)
	{
		return this.GetCastAbility(sourceId) != null;
	}

	/// <summary>
	/// Determines if the sourceId can be activated
	/// </summary>
	public bool CanActivate(int sourceId)
	{
		return this.GetActivateAbility(sourceId) != null;
	}

	/// <summary>
	/// Determines if the playerId can be done with their phase
	/// </summary>
	public bool CanBeDoneWithPhase(int playerId)
	{
		bool canBeDone = this.GetDoneWithPhaseAbility(playerId) != null;
		return canBeDone;
	}
    
    /// <summary>
    /// Determines if the playerId can pause or unpause
    /// </summary>
    public bool CanPauseUnpause(int playerId)
    {
        bool canBeDone = this.GetPauseUnpauseAbility(playerId) != null;
        return canBeDone;
    }

    /// <summary>
    /// Attemps to find a legal Attack ability matching the parameters
    /// </summary>
    protected LegalAbility GetAttackAbility(int sourceId, int? targetId)
	{
		return this.legalAbilities.Values.Where(la=>la.Source == sourceId 
		                                        && la.AbilityType == AbilityType.Attack
												&& ( targetId.HasValue == false || la.LegalTargetIds.Contains(targetId.Value))).FirstOrDefault();
	}

	/// <summary>
	/// Attempts to find a legal Cast ability for the sourceId
	/// </summary>
	protected LegalAbility GetCastAbility(int sourceId)
	{
		return this.legalAbilities.Values.Where(la=>la.Source == sourceId
                                                && la.LegalToPlayAbility
                                                && la.AbilityType == AbilityType.Cast).FirstOrDefault();
	}

	/// <summary>
	/// Attempts to find a Legal Block ability matching the parameters
	/// </summary>
	protected LegalAbility GetBlockAbility(int sourceId, int? targetId)
	{
		return this.legalAbilities.Values.Where(la=>la.Source == sourceId 
                                                && la.LegalToPlayAbility
		                                        && la.AbilityType == AbilityType.Block 
		                                        && ( targetId.HasValue == false || la.LegalTargetIds.Contains(targetId.Value) )).FirstOrDefault();
	}

	/// <summary>
	/// Attempts to find a Legal Activate ability matching the sourceID
	/// </summary>
	protected LegalAbility GetActivateAbility(int sourceId)
	{
		return this.legalAbilities.Values.Where(la=>la.Source == sourceId
                                                && la.LegalToPlayAbility
                                                && la.AbilityType == AbilityType.Activated).FirstOrDefault();
    }

    /// <summary>
    /// Attempts to find a Legal Unpause ability matching the playerId
    /// </summary>
    protected LegalAbility GetUnpauseAbility(int playerId)
    {
        return this.legalAbilities.Values.Where(la => la.Source == playerId
                                                && la.AbilityType == AbilityType.PauseUnpause).FirstOrDefault();
    }

    /// <summary>
    /// Attempts to find a Legal DoneWithPhase ability matching the playerId
    /// </summary>
    protected LegalAbility GetDoneWithPhaseAbility(int playerId)
	{
		return this.legalAbilities.Values.Where (la=>la.Source == playerId 
		                                         && la.AbilityType == AbilityType.DoneWithPhase).FirstOrDefault();
	}
    
    /// <summary>
    /// Attempts to find a Legal DoneWithPhase ability matching the playerId
    /// </summary>
    protected LegalAbility GetPauseUnpauseAbility(int playerId)
    {
        return this.legalAbilities.Values.Where(la => la.Source == playerId
                                                && la.AbilityType == AbilityType.PauseUnpause).FirstOrDefault();
    }

    protected LegalAbility GetMulliganAbility(int playerId)
	{
		return this.legalAbilities.Values.Where(la=>la.Source == playerId
												&& la.AbilityType == AbilityType.Mulligan).FirstOrDefault();
	}

	/// <summary>
	/// Creates a "Block" GameAction between the sourceId and targetId
	/// </summary>
	public GameAction GetBlockAction(int sourceId, int targetId)
	{
		LegalAbility blockAbility = this.GetBlockAbility(sourceId, targetId);
		return this.CreateGameActionForAbility(blockAbility, targetId);
	}

	/// <summary>
	/// Creates an "Attack" GameAction between the sourceId and targetId
	/// </summary>
	public GameAction GetAttackAction(int sourceId, int targetId)
	{
		LegalAbility attackAbility = this.GetAttackAbility(sourceId, targetId);
		GameAction action = null;
        if(attackAbility != null)
        {
			try
			{
				action = this.CreateGameActionForAbility(attackAbility, targetId);
			}
			catch(System.ArgumentException ex)
			{
				Debug.LogWarningFormat("Trapping Exception: {0}", ex.Message);
			}
        }
        return action;
	}

	/// <summary>
	/// Creates a "Cast" GameAction for the sourceId's cast. Accepts an optional targetId
	/// </summary>
	public GameAction GetCastAction(int sourceId, int? targetId = null)
	{
		LegalAbility castAbility = this.GetCastAbility(sourceId);
		GameAction action = null;
		
		try
		{
			if(targetId.HasValue)
			{
				action = this.CreateGameActionForAbility(castAbility, targetId.Value);
			}
			else
            {
                if (castAbility != null)
				{
					action = this.CreateGameActionForAbility(castAbility);	
				}
				else
				{
					throw new System.ArgumentException("castAbility is NULL!");
				}
			}
		}
		catch(System.ArgumentException ex)
		{
			Debug.LogWarningFormat("Trapping Exception: {0}", ex.Message);
		}

		return action;
	}

	/// <summary>
	/// Creates a "Activate" GameAction for the sourceId's cast. Accepts an optional targetId
	/// </summary>
	public GameAction GetActivateAction(int sourceId, int? targetId = null)
	{
		LegalAbility activateAbility = this.GetActivateAbility(sourceId);
		
		if(targetId.HasValue)
		{
			return this.CreateGameActionForAbility(activateAbility, targetId.Value);
		}
		
		return this.CreateGameActionForAbility(activateAbility);
    }

    /// <summary>
    /// Creates a "Unpause" action for the given playerId
    /// </summary>
    public GameAction GetUnpauseAction(int playerId)
    {
        LegalAbility unpauseAbility = this.GetUnpauseAbility(playerId);
        if (unpauseAbility == null)
            return null;
        return this.CreateGameActionForAbility(unpauseAbility);
    }

    /// <summary>
    /// Creates a "DoneWithPhase" action for the given playerId
    /// </summary>
    public GameAction GetDoneWithPhaseAction(int playerId)
	{
		LegalAbility doneWithPhaseAbility = this.GetDoneWithPhaseAbility(playerId);
        if (doneWithPhaseAbility == null)
            return null;
		return this.CreateGameActionForAbility(doneWithPhaseAbility);
	}
    
    /// <summary>
    /// Creates a "Pause" action for the given playerId
    /// </summary>
    public GameAction GetPauseUnpauseAction(int playerId)
    {
        LegalAbility pauseUnpauseAbility = this.GetPauseUnpauseAbility(playerId);
        if (pauseUnpauseAbility == null)
            return null;
        return this.CreateGameActionForAbility(pauseUnpauseAbility);
    }

    /// <summary>
    /// Creates a "Mulligan" action for the given playerId
    /// </summary>
    public GameAction GetMulliganAction(int playerId)
	{
		LegalAbility mulliganAbility = this.GetMulliganAbility(playerId);
        if (mulliganAbility == null)
            return null;
        return this.CreateGameActionForAbility(mulliganAbility);
	}
	
	/// <summary>
	/// Given a legalAbility reference and optional list of targets, this method creats a submittable
	/// GameAction for that ability.
	/// </summary>
	protected GameAction CreateGameActionForAbility(LegalAbility legalAbility, params int[] targetIds)
	{
        if (legalAbility == null)
            return null;
		// Were enough targetIds provided?        
		if (legalAbility.MinTargets > 0 && (targetIds == null || targetIds.Length < legalAbility.MinTargets))
		{
			throw new System.ArgumentException(legalAbility.AbilityType.ToString() + " GameAction requires at least " + legalAbility.MinTargets + " target(s)");
		}

		// Create a new GameAction
		GameAction gameAction = new GameAction();

        // Set source id
        gameAction.LegalAbility = legalAbility;
		gameAction.SourceId = legalAbility.Source;
        gameAction.IsLegal = legalAbility.LegalToPlayAbility;
		
		// Initialize the TargetIdList TargetIds "list" property
		TargetIdList targetList = new TargetIdList();
		targetList.TargetIds = new List<int>();

		// Loop each targetId to check for legality and add it to the TargetIdList
		int numTargets = Mathf.Min(targetIds.Length, legalAbility.MaxTargets);
		for(int i=0; i<numTargets; i++)
		{
			// Check targetId legality
			int targetId = targetIds[i];
			if(legalAbility.LegalTargetIds.Contains(targetId))
			{
				// Add targetId to targetList
				targetList.TargetIds.Add(targetId);
			}
		}

		// Were the minimum number of legal targets found?
		if(legalAbility.MinTargets > targetList.TargetIds.Count)
		{
			throw new System.ArgumentException(legalAbility.AbilityType.ToString() + " GameAction requires " + legalAbility.MinTargets + " LEGAL target(s)");
		}
		
		// Set TargetIdLists in gameAction
		gameAction.TargetIdLists = new List<TargetIdList>();
		gameAction.TargetIdLists.Add(targetList);
		
		// Set ability identifier
		gameAction.AbilityId = legalAbility.AbilityId;

		// Done!
		return gameAction;
	}

    public void MakeAllIllegal()
    {
        foreach(var legalAbility in legalAbilities.Values)
        {
            legalAbility.LegalToPlayAbility = false;
        }
    }
}
