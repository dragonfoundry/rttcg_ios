﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Messages;

namespace DragonFoundry.GameState
{
	public class GameAction
	{
		public int SourceId { get; set; }

		public int AbilityId { get; set; }

        public bool IsLegal { get; set; }

		public List<TargetIdList> TargetIdLists { get; set; }

        public LegalAbility LegalAbility { get; set; }
	}
}
