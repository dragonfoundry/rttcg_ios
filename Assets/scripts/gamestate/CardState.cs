﻿using System;
using UnityEngine;
using Messages;

using Newtonsoft.Json.Linq;

namespace DragonFoundry.GameState
{
	/// <summary>
	/// This represents the state of a single card within the game.
	/// </summary>
	public class CardState
	{
		private JObject state;

        public JObject State { get { return state; } }

        private Actions cardActions = new Actions();
		private CardTargets cardTargets = new CardTargets();

		public Actions CardActions { get { return this.cardActions; }}
		public CardTargets CardTargets { get { return this.cardTargets; }}

		public string Aspect { get; private set; }
		public bool IsUnit { get; private set; }
		public bool IsEnergy { get; private set; }
		public bool IsPower { get; private set; }
		public string ArtId { get; private set; }
		public int Atk { get; private set; }
		public int OriginalPower { get; private set; }
		public int Health { get; private set; }
		public int OriginalHealth { get; private set; }
		public int DamageTaken { get; private set; }
		public int Cost { get; private set; }
		public int OriginalCost { get; private set; }
		public int Speed { get; private set; }
		public int Id { get; private set; }
		public int Controller { get; private set; }
		public string Zone { get; private set; }
		public string Name { get; private set; }
		public string EffectText { get; private set; }
		public int CountdownTicksRemaining { get; private set; }
		public int Cooldown { get; private set; }
		public float CountdownTimeElapsedAsRatio { get; private set; }
		public int GuardOrder { get; private set; }
		public int ZoneChangeTimestamp { get; private set; }
		public int ArcaneIcons { get; private set; }
		public int TechIcons { get; private set; }
		public int DivineIcons { get; private set; }
		public int NatureIcons { get; private set; }
		public int ChaosIcons { get; private set; }
		public string FxScript { get; private set; }

		/// <summary>
		/// Sets the internal state of this card.
		/// </summary>
		/// <param name="updatedState">The state of the given card.</param>
		public void SetState(JObject updatedState)
		{
			string wasZone = String.IsNullOrEmpty(this.Zone) ? string.Empty : (string)this.Zone; 
			state = updatedState;
            cardActions.SetActions(state["LegalActions"] as JArray, Id);
			cardTargets.SetTargets(state["Targets"] as JArray, Id);

			JToken test;
			this.Aspect= Val<string>("Aspect");
			this.IsUnit= state["IsUnit"] != null && Val<bool>("IsUnit");
			this.IsEnergy= state["IsEnergy"] != null && Val<bool>("IsEnergy");
			this.IsPower= state["IsPower"] != null && Val<bool>("IsPower");
			this.ArtId= Val<string>("ArtId");
			this.Atk= Val<int>("Atk");
			this.OriginalPower= Val<int>("OriginalPower");
			this.Health= Val<int>("Health");
			if(state.TryGetValue("OrigionalHealth", out test)) { if(this.IsNullOrEmpty(test)== false) this.OriginalHealth= test.Value<int>();}
			this.DamageTaken= Val<int>("Damage");
			this.Cost= Val<int>("Cost");
			this.OriginalCost= Val<int>("OriginalCost");
			//if(state.TryGetValue("Speed", out test)) { if(this.IsNullOrEmpty(test)== false) this.OriginalHealth= test.Value<int>();}
			this.Id= Val<int> ("Id");
			this.Controller= Val<int>("Controller"); 
			this.Zone= Val<string>("Zone");
			this.Name= Val<string>("Name");
			this.EffectText= Val<string>("EffectText") ?? "";
			this.CountdownTicksRemaining= Val<int> ("CountdownTicksRemaining");
			this.Cooldown= Val<int>("Cooldown"); 
			this.CountdownTimeElapsedAsRatio= Mathf.Clamp01(1 - (CountdownTicksRemaining / 400.0f));
			this.GuardOrder= Val<int>("GuardOrder");
			this.ZoneChangeTimestamp= Val<int>("ZoneChangeTimestamp");
			this.ArcaneIcons= Val<int>("ArcaneIcons");
			this.TechIcons= Val<int>("TechIcons");
			this.DivineIcons= Val<int>("DivineIcons");
			this.NatureIcons= Val<int>("NatureIcons");
			this.ChaosIcons= Val<int>("ChaosIcons");
			this.FxScript= Val<string>("FxScript");
			
			if(wasZone != this.Zone)
			{
				//Debug.Log ("SetState " + this.Name + " FromZone:" + wasZone + " toZone:" + this.Zone);
				if(this.IsEnergy && wasZone == "Hand" && this.Zone == "Stack")
				{
					this.Zone = "Trash";
				}
			}

			// HackHack: Sigh another hack to prevent the server from moving cards to the trash.. and instead flagging them for discard -DMac
			if(this.Zone == "Trash")
			{
				if(this.IsDiscarded == false)
				{
					this.IsDiscarded = true;
					this.TimeDiscarded = Time.time;
				}
				Debug.LogError("Overriding trash zone to: " + wasZone);
				this.Zone = wasZone;
			}
		
			// HackHack: A continuation of the hack in FullGameStateAdapter to prevent cards from being removed from the Hand immediately -DMac
			if(this.Zone == "Hand") { this.IsDiscarded = false; }

		}

		public bool IsDiscarded = false;
		public float TimeDiscarded;
		public bool IsAttackingOpponent = false;
		public bool IsBlocking = false;
		public int? BlockedCardId = null;
	
		public void SetZone(string zone)
		{
			this.Zone = zone;
		}

		public void Log()
		{
			Debug.Log(state);
		}

		public bool IsNullOrEmpty(JToken token)
		{
			return (token == null) ||
				(token.Type == JTokenType.Array && !token.HasValues) ||
					(token.Type == JTokenType.Object && !token.HasValues) ||
					(token.Type == JTokenType.String && token.ToString() == String.Empty) ||
					(token.Type == JTokenType.Null);
		}

		private T Val<T>(string key)
		{
			return state.Value<T>(key);
		}
	}
}
