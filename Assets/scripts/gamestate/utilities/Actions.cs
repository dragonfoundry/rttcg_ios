﻿using System;
using LinqTools;
using System.Collections.Generic;
using UnityEngine;
using Messages;

using Newtonsoft.Json.Linq;

namespace DragonFoundry.GameState
{
    public class Actions
    {
        private JArray legalActions;
        //private int entityId;

        public void SetActions(JArray legalActions, int entityId)
        {
            this.legalActions = legalActions;
            //this.entityId = entityId;
        }

        public bool HasActions { get { return legalActions != null && legalActions.Count > 0; } }

        //// Can do action
		public bool CanAttack { get { return LegalAttackTokens.Count() > 0; } }
		public bool CanBlock { get { return LegalBlockTokens.Count() > 0; } }
        public bool CanGuard { get { return LegalGuardTokens.Count() > 0; } }
        public bool CanCast { get { return LegalCastTokens.Count() > 0; } }
        public bool CanActivate { get { return LegalActivateTokens.Count() > 0; } }
        public bool CanBeDone { get { return LegalDoneTokens.Count() > 0; } }

        //// Targets lists
		public IEnumerable<int> AttackTargets { get { return GetTargetsFromActionTokens(LegalAttackTokens); } }
		public IEnumerable<int> BlockTargets { get { return GetTargetsFromActionTokens(LegalBlockTokens); } }
        public IEnumerable<int> GuardTargets { get { return GetTargetsFromActionTokens(LegalGuardTokens); } }
        public IEnumerable<int> CastTargets { get { return GetTargetsFromActionTokens(LegalCastTokens); } }
        public IEnumerable<int> ActivateTargets { get { return GetTargetsFromActionTokens(LegalActivateTokens); } }
        

        public bool CastRequiresTarget
        {
            get
            {
                if (LegalCastTokens.Count() > 0)
                {
                    JArray specs = (JArray)LegalCastTokens.First()["TargetSpecs"];
                    if (specs.Count > 0 && specs[0]["MinTargets"] != null)
                    {
                        return specs[0].Value<int>("MinTargets") > 0;
                    }
                }

                return false;
            }
        }

        public bool ActivateRequiresTarget
        {
            get
            {
                if (LegalActivateTokens.Count() > 0)
                {
                    JArray specs = (JArray)LegalActivateTokens.First()["TargetSpecs"];
                    if (specs.Count > 0 && specs[0]["MinTargets"] != null)
                    {
                        return specs[0].Value<int>("MinTargets") > 0;
                    }
                }

                return false;
            }
        }
        
        private IEnumerable<JToken> LegalAttackTokens
        {
            get { return GetActionTokensForActionName("Attack"); }
        }

		private IEnumerable<JToken> LegalBlockTokens
		{
			get { return GetActionTokensForActionName("Block"); }
		}

        private IEnumerable<JToken> LegalGuardTokens
        {
            get { return GetActionTokensForActionName("Guard"); }
        }

        private IEnumerable<JToken> LegalCastTokens
        {
            get { return GetActionTokensForActionName("Cast"); }
        }

        private IEnumerable<JToken> LegalActivateTokens
        {
            get { return GetActionTokensForActionName("ActivatedAbility"); }
        }

        private IEnumerable<JToken> LegalDoneTokens
        {
            get { return GetActionTokensForActionName("Done"); }
        }

        private IEnumerable<JToken> GetActionTokensForActionName(string actionName)
        {
            if (legalActions == null) { return Enumerable.Empty<JToken>(); }
            return from token in legalActions where token.Value<string>("ActionName").Contains(actionName) select token;
        }

        private IEnumerable<int> GetTargetsFromActionTokens(IEnumerable<JToken> tokens)
        {
            var targetSpecs = from token in tokens select token["TargetSpecs"];

            List<JToken> legalTargetIds = new List<JToken>();
            foreach (var targetSpec in targetSpecs)
            {
                legalTargetIds.AddRange(from token in targetSpec select token["LegalTargetIds"]);
            }

            List<int> ids = new List<int>();
            foreach (var targetId in legalTargetIds)
            {
                ids.AddRange(from token in targetId select (int)token);
            }

            return ids;
        }
    }
}
