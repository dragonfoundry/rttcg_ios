﻿using UnityEngine;
using System.Collections;

using System;
using LinqTools;
using System.Collections.Generic;
using Messages;

using Newtonsoft.Json.Linq;

namespace DragonFoundry.GameState
{
	public class CardTargets
	{
		private JArray targets;
		
		public void SetTargets(JArray targets, int cardId)
		{
			this.targets = targets;
		}

		public IEnumerable<int> AttackTargets { get { return TargetsForAction("Attack"); } }
		public IEnumerable<int> BlockTargets { get { return TargetsForAction("Block"); } }
        public IEnumerable<int> CastTargets { get { return TargetsForAction("Cast"); } }
		public IEnumerable<int> GuardTargets { get { return TargetsForAction("Guard"); } }

		public int? Guarding
		{
			get
			{
				if (GuardTargets.Count() == 0) {
					return null;
				}

				int guarding = GuardTargets.First();
				if (guarding < 0)
				{
					return null;
				}

				return guarding;
			}
		}

		private IEnumerable<int> TargetsForAction(String actionName)
		{
            if (!IsArrayGood()) {
                if (actionName == "Cast")
                {
                    return targets.Select(val => val.Value<int>());
                }

                return new int[0];
            }

            JToken actionToken = TargetTokenForAction(actionName);
            if (actionToken == null) { return new int[0]; }

            JArray targetArray = actionToken["targets"] as JArray;
            if (targetArray == null) { return new int[0]; }

            return targetArray.Select(val => (int)val);
		}

        private bool IsArrayGood()
        {
            if (!(targets is JArray)) { return false; }

            JArray targetsArray = targets as JArray;

            if (targetsArray.Count == 0) { return false; }

            JToken firstToken = targetsArray[0];

            if (!(firstToken is JObject)) { return false; }

            return true;
        }

        private JToken TargetTokenForAction(String actionName)
        {
            if (targets == null) { return null; }

            return (from target in targets
                   where actionName == target.Value<string>("type")
                   select target).FirstOrDefault();
        }


	}
}