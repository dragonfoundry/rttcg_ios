using UnityEngine;
using LinqTools;
using System.Collections.Generic;
using Messages;
using NovaBlitz.UI;

namespace DragonFoundry.GameState
{
	/// <summary>
	/// A read-only version of the game state
	/// </summary>
	public interface GameStateReadOnly
	{
		// TODO: This interface should return ReadOnly versions of PlayerState and CardState.

		/// <summary>
		/// The Id of the player that is local to this client.
		/// </summary>
		int SelfId { get; }

		LegalAbilities LegalAbilities { get; }

        /// <summary>
        /// The state of the player that is not local to this client.
        /// </summary>
        INovaState Self { get; }

		/// <summary>
		/// The state of the player that is local to this client.
		/// </summary>
		INovaState Opponent { get; }

        /// <summary>
        /// The state of the current turn.
        /// </summary>
        TurnState Turn { get; }

		/// <summary>
		/// A collection of all players in the game.
		/// </summary>
		IEnumerable<INovaState> Players { get; }

		/// <summary>
		/// A collection of all cards in the game.
		/// </summary>
		IEnumerable<ICardState> Cards { get; }

        /// <summary>
        /// Get the cards in the stack, in the order of the stack.
        /// </summary>
        IEnumerable<ICardState> Stack { get; }

        /// <summary>
        /// Gets the last known 
        /// </summary>
        ICardState LastKnownStateForCardId(int id);

        /// <summary>
        /// Gets the last known 
        /// </summary>
        ICardState CurrentStateForCardId(int id);

        /// <summary>
        /// Gets a collection of all cards in the game that are owned by a particular player.
        /// </summary>
        /// <returns>The cards owned by a particular player.</returns>
        /// <param name="id">The identifier of the player.</param>
        IEnumerable<ICardState> CardsForPlayer(int id);

		/// <summary>
		/// Gets a collection of all cards in the game that are owned by a particular player, and
		/// are currently in their hand.
		/// </summary>
		/// <returns>The cards in the hand of a particular player.</returns>
		/// <param name="id">The identifier of the player.</param>
		IEnumerable<ICardState> HandForPlayer(int id);

		/// <summary>
		/// Gets a collection of all cards in the game that are owned by a particular player, and
		/// are currently in play.
		/// </summary>
		/// <returns>The cards that are in play for a particular player.</returns>
		/// <param name="id">The identifier of the player.</param>
		IEnumerable<ICardState> PermanentsForPlayer(int id);

        

		IEnumerable<ICardState> OtherCardsForPlayer(int id);

		/// <summary>
		/// Get a particular card based on its identifier.
		/// </summary>
		/// <returns>The card, or null if there is no such card.</returns>
		/// <param name="id">The identifier of the card.</param>
		ICardState GetCard(int id);

		/// <summary>
		/// Get a particular player based on their identifier.
		/// </summary>
		/// <returns>The player, or null if there is no such player.</returns>
		/// <param name="id">The identifier of the player.</param>
		INovaState GetPlayer(int id);
	}

	/// <summary>
	/// This class stores the game state for an entire game. Primarily it stores the CardState and
	/// PlayerState, as well as providing various accessors to those entities.
	/// </summary>
	public class GameStateContainer : GameStateReadOnly {

        private TurnState turnState = new TurnState();
		private Dictionary<int, ICardState> cards = new Dictionary<int, ICardState>();
		private Dictionary<int, INovaState> players = new Dictionary<int, INovaState>();
		private LegalAbilities legalAbilities = new LegalAbilities();

		public int SelfId { get; set; }
        public int OpponentId { get; set; }

        public Dictionary<int, ICardState> LastKnownStates = new Dictionary<int, ICardState>();

        public Dictionary<int, ICardState> CurrentStates = new Dictionary<int, ICardState>();

		public LegalAbilities LegalAbilities { get { return this.legalAbilities; } }
        
        public NovaBlitz.Game.GameLog GameLog { get; set; }
        /// <summary>
        /// Get the current turn state for the game.
        /// </summary>
        /// <returns>The current turn state for the game.</returns>
        public TurnState Turn { get { return turnState; } }

		/// <summary>
		/// Get a CardState object with the given id. This will create a new CardState object
		/// if none currently exists.
		/// </summary>
		/// <returns>The CardState object.</returns>
		/// <param name="id">The id of the card.</param>
		public ICardState GetOrCreateCard(int id)
		{
			if (!cards.ContainsKey(id))
			{
				ICardState created = (ICardState)new MatchableState();
				cards[id] = created;
			}

			return GetCard(id);
		}

        public bool CardExistsInCards(int id)
        {
            return cards.ContainsKey(id);
        }

		/// <summary>
		/// Gets the or create legal ability in the gamestate
		/// </summary>
		public LegalAbility GetOrCreateLegalAbility(int abilityId)
		{
			return this.legalAbilities.GetOrCreateLegalAbility(abilityId);
		}


		public INovaState GetOrCreatePlayer(int id)
		{
			if(this.players.ContainsKey(id) == false)
			{
				INovaState newNova = (INovaState) new MatchableState();
				this.players[id] = newNova;
			}

			return this.players[id];
		}


        /// <summary>
        /// Adapts the cardState into the existing cardstate for that card
        /// </summary>
        public void AdaptCard(ICardState stateUpdate)
        {
            // Ignore the update if the card's in the trash (for now)

            MatchableState card = (MatchableState)GetOrCreateCard(stateUpdate.Id);


            card.Keywords = stateUpdate.Keywords;
            // Note - this is a DICTIONARY.
            // It's ok to point directly to it, as it was newly-created for this diffgamestate, and will never be used again.

            card.ArcaneIcons = stateUpdate.ArcaneIcons;
            card.Atk = stateUpdate.Atk;
            card.CardId = stateUpdate.CardId;
            card.ChaosIcons = stateUpdate.ChaosIcons;

            //Debug.LogFormat("#GameState#Adapting CardState for ID: {0}, Control:{1} Zone={2}, Damage={3}", stateUpdate.Id, stateUpdate.ControllerId, stateUpdate.Zone, stateUpdate.Damage);
            card.ControllerId = stateUpdate.ControllerId;
            card.Damage = stateUpdate.Damage;
            card.DivineIcons = stateUpdate.DivineIcons;
            card.EnergyCost = stateUpdate.EnergyCost;
            card.ActivationCost = stateUpdate.ActivationCost;
            card.Health = stateUpdate.Health;
            card.Id = stateUpdate.Id;
            card.IsActivating = stateUpdate.IsActivating;
            card.IsAttacking = stateUpdate.IsAttacking;
            card.IsBlocking = stateUpdate.IsBlocking;
            card.IsItem = stateUpdate.IsItem;
            card.IsPower = stateUpdate.IsPower;
            card.IsResource = stateUpdate.IsResource;
            card.IsTestTrue = stateUpdate.IsTestTrue;
            card.IsUnit = stateUpdate.IsUnit;
            card.NatureIcons = stateUpdate.NatureIcons;
            card.Subtype = stateUpdate.Subtype;
            card.TargetId = stateUpdate.TargetId;
            card.TechIcons = stateUpdate.TechIcons;
            card.ZoneChangeTimestamp = stateUpdate.ZoneChangeTimestamp;
            card.ZoneId = stateUpdate.ZoneId;

            card.IsArcane = stateUpdate.IsArcane;
            card.IsTech = stateUpdate.IsTech;
            card.IsDivine = stateUpdate.IsDivine;
            card.IsNature = stateUpdate.IsNature;
            card.IsChaos = stateUpdate.IsChaos;

            card.IsUpdated = true;
        }

        #region GameStateReadOnly

        public INovaState Self { get { return GetPlayer (SelfId); } }

		public INovaState Opponent
		{
			get
			{
				foreach (INovaState player in Players)
				{
					if (player.Id != this.SelfId)
					{
						return player;
					}
				}

				return null;
			}
		}
		
		public IEnumerable<INovaState> Players { get { return players.Values; } }
		public IEnumerable<ICardState> Cards { get { return cards.Values; } }

        public IEnumerable<ICardState> Stack
        {
            get
            {
                return from card in Cards where card.Zone == CardZone.Stack orderby card.ZoneChangeTimestamp ascending select card;
            }
        }
		
		public IEnumerable<ICardState> CardsForPlayer(int id)
		{
			return from card in Cards where card.ControllerId == id select card; 
		}

        public ICardState LastKnownStateForCardId(int id)
        {
            ICardState state;
            LastKnownStates.TryGetValue(id, out state);
            Debug.LogFormat("Getting Last known state for card {0}, atk = {1}, health = {2}, damage = {3}", state.Id, state.Atk, state.Health, state.Damage);
            return state;
        }

        public ICardState CurrentStateForCardId(int id)
        {
            ICardState state;
            CurrentStates.TryGetValue(id, out state);
            return state;
        }


        public IEnumerable<ICardState> HandForPlayer(int id)
		{
            var energyCards = from card in Cards where card.ControllerId == id && card.Zone == CardZone.Hand && card.IsResource orderby card.GetAspect().ToString() descending select card;
			var powerCards = from card in Cards where card.ControllerId == id && card.Zone == CardZone.Hand && card.IsPower select card;
            var otherCards = from card in Cards where card.ControllerId == id && card.Zone == CardZone.Hand && !card.IsResource && !card.IsPower orderby card.EnergyCost descending select card;
			powerCards = powerCards.Reverse();
			return powerCards.Concat(otherCards).Concat(energyCards);
		}
		
		public IEnumerable<ICardState> PermanentsForPlayer(int id)
		{
			return from card in Cards where card.ControllerId == id && card.Zone == CardZone.Arena && !card.IsResource orderby card.ZoneChangeTimestamp select card; 
		}

		public IEnumerable<ICardState> OtherCardsForPlayer(int id)
		{
			return from card in Cards where card.ControllerId == id && card.Zone == CardZone.NoZone select card; 
		}
        
        public IEnumerable<ICardState> TrashForPlayer(int id)
        {
            return from card in Cards where card.ControllerId == id && card.Zone == CardZone.Trash orderby card.ZoneChangeTimestamp select card;
        }

        public ICardState GetCard(int id)
		{
			if (cards.ContainsKey(id))
			{
				return cards[id];
			}
			
			return null;
		}
		
		public INovaState GetPlayer(int id)
		{
			if (this.players.ContainsKey(id))
			{
				return players[id];
			}
			
			return null;
		}
		
		#endregion
	}
}
