
using Newtonsoft.Json.Linq;

namespace DragonFoundry.GameState
{
	/// <summary>
	/// This represents the state of a single player within the game.
	/// </summary>
	public class PlayerState {

		private JObject state;

        private Actions playerActions = new Actions();
		public Actions PlayerActions { get { return playerActions; } }
		public int HandSize { get; private set; }
		public int Health {  get; private set;}
		public int DamageTaken { get; private set;}
		public int Energy {  get; private set;}
		public int MaxEnergy {  get; private set;}
		public int Id {  get; private set; }
		public bool HasWon { get; private set;}
		public bool HasLost {  get; private set;}
		public int DeckSize {  get; private set;} //Temporary
		public int DiscardSize {  get; private set; } //Temporary
		public int ArcaneThreshold { get; private set; }
		public int TechThreshold { get; private set;}
		public int DivineThreshold { get; private set;}
		public int NatureThreshold { get; private set; }
		public int ChaosThreshold { get; private set;}

		/// <summary>
		/// Sets the internal state of this player.
		/// </summary>
		/// <param name="updatedState">The state of the given player.</param>
		public void SetState(JObject updatedState)
		{
			state = updatedState;
            playerActions.SetActions(state["LegalActions"] as JArray, Id);

			this.HandSize=Val<int> ("HandSize");
			this.Health = Val<int>("Health");
			this.DamageTaken =Val<int>("Damage"); 
			this.Energy = Val<int>("Energy"); 
			this.MaxEnergy =Val<int>("EnergyMax"); 
			this.Id =Val<int> ("Id"); 
			this.HasWon =Val<bool?>("HasWon") ?? false; 
			this.HasLost =Val<bool?>("HasLost") ?? false; 
			this.DeckSize = 27; //Temporary
			this.DiscardSize =0; //Temporary
			this.ArcaneThreshold =Val<int>("ArcaneThreshold");
			this.TechThreshold =Val<int>("TechThreshold"); 
			this.DivineThreshold =Val<int>("DivineThreshold"); 
			this.NatureThreshold = Val<int>("NatureThreshold"); 
			this.ChaosThreshold = Val<int>("ChaosThreshold"); 
		}

		private T Val<T>(string key)
		{
			return state.Value<T>(key);
		}
	}
}