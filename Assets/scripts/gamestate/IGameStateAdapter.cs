namespace DragonFoundry.GameState {

	/// <summary>
	/// An interface for adapting an arbitrary view modification messages into
	/// an updated GameState.
	/// </summary>
	public interface IGameStateAdapter<T> {

		/// <summary>
		/// Adapt the given viewModification into the gameState.
		/// </summary>
		/// <param name="gameState">The current state of the game. This should be mutated to account
		/// for the viewModification.</param>
		/// <param name="viewModification">The change to the game state.</param>
		void AdaptGameState(GameStateContainer gameState, T viewModification);
	}
}
