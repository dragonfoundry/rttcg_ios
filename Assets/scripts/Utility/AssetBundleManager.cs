﻿using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Messages;

namespace NovaBlitz.UI
{
    public class AssetBundleManager : MonoBehaviour
    {
        public string FolderTarget { get { return Path.Combine(Application.dataPath, string.Format("AssetBundles/{0}/StreamingAssets", PLATFORM)); } }
        private string _BaseCloudFrontCache = "https://d59cihibqxhhp.cloudfront.net";
        public static AssetBundleManager Instance;

        [Inject] public NovaAudio NovaAudio { get; set; }
        
        public Dictionary<string, AssetBundle> AssetBundles = new Dictionary<string, AssetBundle>(StringComparer.OrdinalIgnoreCase);
        public Dictionary<string, AssetBundleData> AssetBundleData = new Dictionary<string, AssetBundleData>();
        public Dictionary<string, string> BundleNameByAssetName = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        
        private List<AssetBundleData> AssetBundlesToDownload = new List<AssetBundleData>();
        private List<AssetBundleData> FailedDownloads = new List<AssetBundleData>();
        private bool _IsCachePreloaded = false;
        public bool IsAllBundlesLoaded { get; private set; }
        private bool _IsPriotityZeroBundlesLoaded;
        public bool IsPriotityZeroBundlesLoaded { get { return _IsPriotityZeroBundlesLoaded; } private set { if(value == true) Debug.Log("Priority Zero bundles loaded"); _IsPriotityZeroBundlesLoaded = value; } }

        private UnityWebRequest _PendingRequest;
        private AssetBundleData _PendingBundle;
        public float RequestProgress { get { return _PendingRequest == null ? -2 : _PendingRequest.downloadProgress; } }
        public int RequestSize { get { return _PendingBundle == null ? -2 : _PendingBundle.Size; } }
        public string RequestName { get { return _PendingBundle == null ? String.Empty : _PendingBundle.Name; } }
        public int TotalDownloads { get; private set; }
        public int DownloadsCompleted { get { return TotalDownloads - FailedDownloads.Count - AssetBundlesToDownload.Count; } }
        public string ProgressString { get { return RequestProgress >= 0 && RequestProgress < 1 
                    ? string.Format ("{0} {1}/{2}: {3}: {4}/{5}kb", I2.Loc.ScriptLocalization.Downloading_Assets, DownloadsCompleted, TotalDownloads, RequestName, (int)(RequestProgress * RequestSize), RequestSize)
					: string.Empty; } }// string.Format("{0} {1}/{2}", I2.Loc.ScriptLocalization.Downloading_Assets, DownloadsCompleted, TotalDownloads); } }

#if UNITY_IOS
        public const string PLATFORM = "iOS";
#elif UNITY_ANDROID
        public const string PLATFORM = "Android";
#else
        public const string PLATFORM = "Standalone";
#endif
        public const string INCLUDED_BUNDLES = "included_bundles.txt";

        public void Awake()
        {
            IsAllBundlesLoaded = false;
            Instance = this;
        }

        public void PreloadIncludedAssetBundles()
        {
            Timer.Instance.StartCoroutine(PreloadEnumerator());
        }

        public IEnumerator PreloadEnumerator()
        {
            IsAllBundlesLoaded = false;
            _IsCachePreloaded = false;
            IsPriotityZeroBundlesLoaded = false;
            List<AssetBundleData> included = new List<AssetBundleData>();
            //string filePath = Path.Combine(Application.streamingAssetsPath, INCLUDED_BUNDLES);
            string filePath = Application.streamingAssetsPath + "/" + INCLUDED_BUNDLES;
            Debug.Log("File Path is : " + filePath);
            string result = string.Empty;
            if (filePath.Contains("://"))
            {
                UnityWebRequest www = UnityWebRequest.Get(filePath);
                yield return www.Send();
                if(!www.isHttpError && !www.isNetworkError)
                    result = www.downloadHandler.text;
            }
            else
                result = File.ReadAllText(filePath);
                
            try
            {
                included = JsonConvert.DeserializeObject<List<AssetBundleData>>(result).OrderBy(i => i.Priority - (i.Name.StartsWith("novaaudio") ? 1000 : 0)).ToList();
            }
            catch(Exception ex)
            {
                Debug.LogFormat("#AssetBundleManager# Failed to preload asset bundles: {0}", ex.ToString());
            }

            foreach (var bundleData in included)
            {
                Debug.Log("bundle Data + " + bundleData.Name);
                //Debug.Log(FolderTarget);
                if (!IsPriotityZeroBundlesLoaded && bundleData.Priority > 0)
                {
                    IsPriotityZeroBundlesLoaded = true;
                }
                //string path = Application.isEditor ? Path.Combine (FolderTarget, bundleData.Name) : Path.Combine (Application.streamingAssetsPath, bundleData.Name);
                string path = Application.streamingAssetsPath + "/" + bundleData.Name;
                Debug.Log(path);
                if (/*!Application.isEditor && */!path.Contains("file://"))
				{
					path = "file://" + path;
				}
				//Debug.LogFormat ("Preloading asset bundle from {0}", path);
                yield return LoadAssetBundle(bundleData, path);
            }
            if (FailedDownloads.Count > 0)
                Debug.LogErrorFormat("#AssetBundleManager# Asset Bundle preload failed. This is a CRITICAL error");
            FailedDownloads.Clear();
            _IsCachePreloaded = true;
            if (!IsPriotityZeroBundlesLoaded)
                IsPriotityZeroBundlesLoaded = true;
        }

        public IEnumerator LoadAssetBundle(AssetBundleData bundleData, string path)
        {           
            var hash128 = bundleData.Hash128;
            var crc = bundleData.CRC;
            // See if we have the current bundle loaded. If so, skip.
            AssetBundle loadedBundle;
            AssetBundleData loadedData;
            if(AssetBundleData.TryGetValue(bundleData.Name, out loadedData) && AssetBundles.TryGetValue(bundleData.Name, out loadedBundle))
            {
                if (loadedData.Hash128 == hash128)
                {
                    //Debug.LogFormat("#AssetBundleManager# Skipping bundle as it's already loaded {0} from {1}. {2}-{3}", bundleData.Name, path, hash128, crc);
                    yield break;
                }
                else
                {
                    Debug.LogFormat("#AssetBundleManager# Updated asset bundle found for {0}. Was {1}-{2} is now {3}-{4}. Path: {5}", bundleData.Name, loadedData.Hash128, loadedData.CRC, hash128, crc, path);
                    // We don't remove the files from bundle by name, as the reload would just re-add them anyway.
                    AssetBundleData.Remove(bundleData.Name);
                    AssetBundles.Remove(bundleData.Name);
                    loadedBundle.Unload(false); // this will grow memory, which isn't ideal, but it shouldn't happen often.
												// It will happen all the time if we update a Priority 0 bundle, though (until we update the game version).
                }
            }

            using (var www = UnityWebRequest.GetAssetBundle(path, Hash128.Parse(hash128), crc))
            {
                _PendingBundle = bundleData;
                _PendingRequest = www;
                yield return www.Send();
                if (www.isHttpError || www.isNetworkError)
                {
                    FailedDownloads.Add(bundleData);
						Debug.LogFormat("#AssetBundleManager# Failed to download {0} from {1}. {2}-{3}. Error: {4}",
							bundleData.Name, www.url, hash128, crc, www.error);
                }
                else
                {
                    AssetBundle bundle = ((DownloadHandlerAssetBundle)www.downloadHandler).assetBundle;
                    if (bundle != null)
                    {
                        yield return IntegrateAssetBundle(bundle, bundleData, www.url);
                    }
                    else
                    {
                        Debug.LogErrorFormat("#AssetBundleManager# Bundle is null for {0} from {1}. {2}-{3}", bundleData.Name, www.url, hash128, crc);
                    }
                }
                _PendingBundle = null;
                _PendingRequest = null;
            }
        }

        /// <summary>
        /// download one or more asset bundles. Can be called mid-game to ensure we have the latest ones.
        /// </summary>
        /// <param name="assetBundleList"></param>
        public void UpdateAssetBundles(List<AssetBundleData> assetBundles)
        {
            AssetBundlesToDownload.Clear();
            AssetBundlesToDownload.AddRange(assetBundles.OrderBy(a => a.Priority));

            if (_LoadAllAssetBundles != null)
            {
                Timer.Instance.StopCoroutine(_LoadAllAssetBundles);
            }
            IsAllBundlesLoaded = false;
            _LoadAllAssetBundles = LoadAllAssetBundles();
            Timer.Instance.StartCoroutine(_LoadAllAssetBundles);
        }
        private IEnumerator _LoadAllAssetBundles = null;

        private IEnumerator LoadAllAssetBundles()
        {            
            while(!_IsCachePreloaded)
            {
                yield return null;
            }

            TotalDownloads = AssetBundlesToDownload.Count;
            FailedDownloads.Clear();
            int downloadTries = 0;
            while (AssetBundlesToDownload.Count > 0 || FailedDownloads.Count > 0)
            {
                if(AssetBundlesToDownload.Count == 0)
                {
                    downloadTries++;
                    AssetBundlesToDownload.AddRange(FailedDownloads);
                    FailedDownloads.Clear();
                    yield return new WaitForSeconds(downloadTries);
                }
                
                var bundleToDownload = AssetBundlesToDownload.First();
                AssetBundlesToDownload.Remove(bundleToDownload);
                yield return LoadAssetBundle(bundleToDownload, Application.isEditor ? "file://" + Path.Combine(FolderTarget, bundleToDownload.Name) : string.Format("{0}/{1}/{2}/{3}", _BaseCloudFrontCache, PLATFORM, bundleToDownload.Hash128, bundleToDownload.Name));
				// If we've successfully downloaded all critical bundles, allow the window to close (basically ignore low-pri stuff like the music)
				if (!IsAllBundlesLoaded && FailedDownloads.Count == 0 && !AssetBundlesToDownload.Any (ab => ab.Priority < 50))
				{
					IsAllBundlesLoaded = true;
				}
            }
            Debug.LogFormat("#AssetBundleManager# Asset Bundle Load Complete");
            _LoadAllAssetBundles = null;
            IsAllBundlesLoaded = true;
        }
        
        
        private IEnumerator IntegrateAssetBundle(AssetBundle bundle, AssetBundleData data, string source)
        {
            //Debug.LogFormat("#AssetBundleManager# Integrating asset bundle {0} from {1}", data.Name, source);
            AssetBundles[data.Name] = bundle;
            AssetBundleData[data.Name] = data;

            var assetArray = bundle.GetAllAssetNames();
            List<string> assetNames = new List<string>();
            for (int i = 0; i < assetArray.Length; i++)
            {
                // May need a better storage solution here, if we do have duplicates. Ideally, there are none.
                string assetName = assetArray[i].Split(new char[] { '/', '\\' }, StringSplitOptions.None).Last().Split(new char[] { '.' }, StringSplitOptions.None).First();
                assetNames.Add(assetName);
                //if (BundleNameByAssetName.ContainsKey(assetName))
                //    Debug.LogFormat("#AssetBundleManager# Duplicate bundle name found for {0} (was {1} is {2})", assetName, BundleNameByAssetName[assetName], data.Name);
                BundleNameByAssetName[assetName] = data.Name;
            }

            // perform actions based on each file type - loading in music, enabling card backs, enabling languages/fonts, etc
            if(data.Name.StartsWith("novaaudio"))
            {
                foreach (NovaAudioGroup audioGroup in this.NovaAudio.AudioGroups)
                {
                    if (audioGroup.IsPreloaded)
                    {
                        foreach (NovaAudioItem audioItem in audioGroup.AudioItems)
                        {
                            yield return NovaAudio.PreloadAudioSource(audioItem);
                        }
                    }
                }
                Debug.Log("#AssetBundleManager# Nova Audio Initialized");
                this.NovaAudio.IsInitialized = true;

                if(this.NovaAudio.MusicTheme == null || !this.NovaAudio.MusicTheme.IsPlaying)
                    this.NovaAudio.CrossfadeToMusicMenu();
            }
            else if (data.Name.StartsWith("music_"))
            {
                var groupId = NovaAudio.AudioGroups.First(a => a.Label == "music_theme").ID;
                // add these to music_theme in NovaAudio (uploading an empty assetBundle will remove that track from the game, but deleting the bundle will not)
                for ( int i = 0; i< assetArray.Length; i++)
                {
                    if (!NovaAudio.MusicTheme.AudioItems.Any(a => a.AssetPath == assetArray[i]))
                        NovaAudio.MusicTheme.AudioItems.Add(new NovaAudioItem { AssetPath = assetArray[i], AudioGroupID = groupId, AudioSource = null, IsLooping = false, Label = assetNames[i], Volume = 0.7f, Weight = 1 });
                }
            }
            else if (data.Name.StartsWith("audiofx_"))
            {
                // add these to nova audio as new audio groups (need to set data programatically)
                for (int i = 0; i < assetArray.Length; i++)
                {
                    if (!NovaAudio.AudioGroups.Any(a => a.ID == assetNames[i]))
                    {
                        var group = new NovaAudioGroup
                        {
                            AudioMixerGroup = null,
                            ID = assetArray[i],
                            IsLooping = false,
                            IsPreloaded = true,
                            Label = assetNames[i],
                            PlayType = PlayType.Single,
                            Instances = 1,
                            CrossfadeDuration = 0,
                            Crossfader = null,
                            AudioItems = new List<NovaAudioItem> { new NovaAudioItem { AssetPath = assetArray[i], AudioGroupID = assetArray[i], AudioSource = null, IsLooping = false, Label = assetNames[i], Volume = 0.7f, Weight = 1 } }
                        };
                        NovaAudio.AudioGroups.Add(group);
                        yield return NovaAudio.PreloadAudioSource(group.AudioItems.First());
                    }
                }
            }
            else if (data.Name.StartsWith("font_"))
            {

            }
            else if (data.Name.StartsWith("cardback_"))
            {

            }
            else if (data.Name.StartsWith("cardart_"))
            {

            }
        }
        
        public T LoadAsset<T>(string assetName) where T : UnityEngine.Object
        {
            string bundleName;
            AssetBundle assetBundle;
            
            if (BundleNameByAssetName.TryGetValue(assetName, out bundleName) && AssetBundles.TryGetValue(bundleName, out assetBundle) && assetBundle != null)
            {
                try
                {
                    return assetBundle.LoadAsset<T>(assetName);
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("#AssetBundleManager# Load {0} from asset bundle {1} exception {2}", assetName, bundleName, ex.ToString());
                    return default(T);
                }
            }
            else
            {
                Debug.LogErrorFormat("#AssetBundleManager# Load {0} from asset bundle {1}: bundle not found", assetName, bundleName);
                return default(T);
            }
        }
        // Read 

        public AssetBundleRequest LoadAssetAsync<T>(string assetName) where T : UnityEngine.Object
        {
            string bundleName;
            AssetBundle assetBundle;
            if (BundleNameByAssetName.TryGetValue(assetName, out bundleName) && AssetBundles.TryGetValue(bundleName, out assetBundle) && assetBundle != null)
            {
                try
                {
                    return assetBundle.LoadAssetAsync<T>(assetName);
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("#AssetBundleManager# Load {0} from asset bundle {1} exception {2}", assetName, bundleName, ex.ToString());
                    return default(AssetBundleRequest);
                }
            }
            else
            {
                Debug.LogErrorFormat("#AssetBundleManager# Load {0} from asset bundle {1}: bundle not found", assetName, bundleName);
                return default(AssetBundleRequest);
            }
        }
    }
}
