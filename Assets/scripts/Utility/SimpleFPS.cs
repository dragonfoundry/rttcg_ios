﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFPS : MonoBehaviour {

    private float _deltaTime = 0.0f;
    private string _windowText = "";
    private float _boxWidth = 100f;
    private float _boxHeight = 50f;
    public GUIStyle _debugBoxStyle;

	// Update is called once per frame
	private void Update () {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
    }

    private void OnGUI()
    {
        _windowText = (1.0f / _deltaTime).ToString("0.00");
        GUI.BeginGroup(new Rect(0, 0.0f, _boxWidth, _boxHeight));
        GUI.Box(new Rect(0, 0.0f, _boxWidth, _boxHeight), _windowText, _debugBoxStyle);
        GUI.EndGroup();
    }
}