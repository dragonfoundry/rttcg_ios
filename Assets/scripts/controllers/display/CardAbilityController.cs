﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DragonFoundry.Controllers.Display;
using DragonFoundry.Controllers;
using Messages;
using NovaBlitz.UI;
using TMPro;
using System.Collections.Generic;

public class CardAbilityController : MonoBehaviour 
{
	public GridLayoutGroup AbilityGrid;
	public GameObject FirstAbility;
    
    void Start()
	{
		this.FirstAbility.SetActive(false);
	}
	
	public void HideAbilities()
	{
		int childCount = this.AbilityGrid.transform.childCount;
		for(int i=0; i<childCount; i++)
		{
			this.AbilityGrid.transform.GetChild(i).gameObject.SetActive(false);
		}
	}
	
	public void Initialize(Dictionary<int, double> keywords)
	{
		//Debug.LogFormat("Initialize Keywords :{0}", keywords.Count);
		int childCount = this.AbilityGrid.transform.childCount;
		for(int i=0;i<childCount;i++)
		{
			this.AbilityGrid.transform.GetChild(i).gameObject.SetActive(false);
		}
		int index = 0;
		if(keywords != null)
		{	
			foreach(var keyword in keywords)
			{
				if(keywords[keyword.Key] != 0f)
				{
					this.AddKewordBubble(keyword.Key, ref index, keyword.Value);	
				}
			}
			
			for(int i=index; i<childCount; i++)
			{
				this.AbilityGrid.transform.GetChild(i).gameObject.SetActive(false);
			}
		}
	}
	
	public void Initialize(ICardState cardState, Vector3 worldPosition)
	{
		
		int childCount = this.AbilityGrid.transform.childCount;
		for(int i=0;i<childCount;i++)
		{
			this.AbilityGrid.transform.GetChild(i).gameObject.SetActive(false);
		}
		
		int index = 0;
        foreach(var kvp in cardState.Keywords)
        {
            if (kvp.Value != 0)
                this.AddKewordBubble(kvp.Key, ref index, kvp.Value);
        }

        for (int i=index; i<childCount; i++)
		{
			this.AbilityGrid.transform.GetChild(i).gameObject.SetActive(false);
		}
	}

    private void AddKewordBubble(int keywordId, ref int currentIndex, double value)
    {
        // check for the key
        KeywordData data;
        if (!GameData.KeywordsById.TryGetValue(keywordId, out data))
        {
            Debug.LogFormat("Keyword not found: {0}", keywordId);
            return;
        }
        else if (data.Type != KeywordType.Skill && data.Type != KeywordType.Condition && data.Type != KeywordType.Hidden)
        {
            return;
        }
            // These are never used? -DMac
            //string description = GameData.KeywordDictionary[keyword].EN_text;
            //string keywordName = keyword.ToString();
            int childCount = this.AbilityGrid.transform.childCount;

        Transform child = null;
        if (currentIndex >= childCount)
        {
            child = ((GameObject)GameObject.Instantiate(this.FirstAbility, FirstAbility.transform.position, Quaternion.identity)).transform;
            child.SetParent(this.AbilityGrid.transform, true);
            child.SetAsLastSibling();
            child.localScale = Vector3.one;
        }
        else
        {
            child = this.AbilityGrid.transform.GetChild(currentIndex);
        }
        child.gameObject.SetActive(true);

        //KeywordData keywoardData = GameData.KeywordDictionary[keyword];

        TextMeshProUGUI[] labels = child.GetComponentsInChildren<TextMeshProUGUI>();
        string keywordName = I2.Loc.ScriptLocalization.Get(GameData.KEYWORD_DATA + data.Name);
        string keywordReminder = string.Format(I2.Loc.ScriptLocalization.Get(GameData.REMINDER_DATA + data.Name), value, 0 ,0); // always have 3 values here, for safety!
        if (labels.Length == 1)
        {
            labels[0].text = string.Format("<size=+1><b>{0}</b><size=-1>\n{1}", keywordName, keywordReminder);
        }
        else if(labels.Length > 1)
        {
            labels[0].text = keywordName;
            labels[1].text = keywordReminder;
        }
        currentIndex++;
    }
}
