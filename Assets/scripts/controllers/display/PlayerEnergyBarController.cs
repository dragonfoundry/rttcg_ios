﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace DragonFoundry.Controllers.Display
{

	/// <summary>
	/// Controls visualization of capacity and energy in the energybar
	/// </summary>
	public class PlayerEnergyBarController : MonoBehaviour 
	{
		public TextMeshProUGUI EnergyLabel;
		public VerticalLayoutGroup Pips;
		public int Capacity {get; private set;}
		public int Energy {get; private set;}
		public Color FullPipColor;
		public Color EmptyPipColor;
		public Image TopGlowBlob;
		public Image BottomGlowBlob;

		private GameObject pipPrefab;


		//====================
		// Unity Methods
		//====================
		void Start()
		{
			// Grab the pip that exists at design time...
			this.pipPrefab = this.Pips.transform.GetChild(0).gameObject;
			
			// Reparent it and make it inactive (so we can use it to instantiate more pips)
			this.pipPrefab.transform.AttachToParent(pipPrefab.transform.parent.parent, this.transform.childCount);
			this.pipPrefab.SetActive(false);

			// make the pip image color alph 0;
			Image pipImage = this.pipPrefab.GetComponentInChildren<Image>(); 
			Color color = pipImage.color;
			color.a = 0;
			pipImage.color = color;


			// Update the energy label
			this.UpdateEnergyLabel();
		}

		//====================
		// Public Methods
		//====================
		public void SetEnergyAndCapacity(int capacity, int energy)
		{
			this.Energy = energy;
			this.Capacity = capacity;
			this.UpdateEnergyLabel();

			// Determine the number of pips that should appear in this bar.
			// We want a minimum of 5 pips so that the first few energy appear reasonably sized as they fill up the bar.
			int numPips = Mathf.Max(new int[] {5,capacity,energy});

			if(this.Pips.transform.childCount > numPips)
			{
				// Keep discarding pips from the top of the energy bar until we reach our target "numPips"
				while(this.Pips.transform.childCount > numPips)
				{
					Transform child = this.Pips.transform.GetChild(this.Pips.transform.childCount - 1);
					child.SetParent(null);
					GameObject.DestroyImmediate(child.gameObject);
				}
				LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)this.Pips.transform);
			}
			else if(numPips > this.Pips.transform.childCount)
			{
				// Keep adding pips until we arrive at "numPips"
				while(this.Pips.transform.childCount < numPips)
				{
					int index = this.Pips.transform.childCount;
					bool isPipFilled = index < energy;
					this.AddPipAtIndex(index, isPipFilled);
				}
				LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)this.Pips.transform);
			}

            Pips.spacing = numPips > 10 ? 0 : 6;

			// Energy specific calcuations

			// Energy ratio stores the percentage of capacity filled by energy
			// an energyRatio of 1 means capacity is entirely filled, 0 means entirely empty
			float energyRatio = energy / (float)Mathf.Max(1,this.Capacity);

			if(energy == 0)
			{
				// Hide both glows
				this.TopGlowBlob.DOFade(0,0.4f);
				this.BottomGlowBlob.DOFade(0,0.4f);
			}
			else
			{
				// Show the bottom glow
				this.BottomGlowBlob.DOFade(0.75f,0.4f);

				if(energy > 4 && energyRatio > 0.85)
				{
					this.TopGlowBlob.DOFade(0.75f, 0.4f);
				}
				else
				{
					this.TopGlowBlob.DOFade(0.0f, 0.4f);
				}
            }

			// Because we loop the pip children, energy will never accidentally be set more than the number of children
            for (int i=0;i<this.Pips.transform.childCount; i++)
			{
				Image pipImage = this.Pips.transform.GetChild(i).GetComponentInChildren<Image>();
				
				if(i < energy)
				{
					if(DOTween.IsTweening(pipImage) == false)
					{
						pipImage.DOColor(this.FullPipColor, 0.4f);
						pipImage.enabled = true;
					}
				}
				else
				{
					pipImage.DOKill();
					pipImage.color = this.EmptyPipColor;
                    if (i >= this.Capacity)
                    {
                        pipImage.enabled = false;
                    }
                    else
                    {
                        pipImage.enabled = true;
                    }
                }
			}
		}

		protected Image AddPipAtIndex(int i, bool isFull)
		{
			GameObject newPip = GameObject.Instantiate(this.pipPrefab,Vector3.zero, Quaternion.identity) as GameObject;
			newPip.transform.AttachToParent(this.Pips.transform, this.Pips.transform.childCount);
			newPip.transform.localPosition = Vector3.zero;
			newPip.gameObject.SetActive(true);
			Image pipImage = newPip.GetComponentInChildren<Image>();

			if(isFull)
			{
				pipImage.DOColor(this.FullPipColor, 0.4f);
			}
			else
			{
				pipImage.DOColor(this.EmptyPipColor, 0.2f);
			}

			return pipImage;
		}

		/// <summary>
		/// Updates the energy label based on the current capacity and energy in the bar
		/// </summary>
		protected void UpdateEnergyLabel()
		{
			if(this.Capacity == 0)
			{
				this.EnergyLabel.text = "0";
			}
			else
			{
				this.EnergyLabel.text = string.Format("{0}/{1}", this.Energy, this.Capacity);
				//this.EnergyLabel.text = string.Format("{0}", this.Energy);
			}
		}

		#region Context Menu test fuctions
		[ContextMenu ("Add capacity...")]
		protected void AddCapacity()
		{
			this.SetEnergyAndCapacity(this.Capacity + 1, this.Energy);
		}

		[ContextMenu ("Fill Capacity...")]
		protected void FillCapacity()
		{
			this.SetEnergyAndCapacity(this.Capacity, this.Capacity);
		}

		
		[ContextMenu ("Use Energy...")]
		protected void Spend3Energy()
		{
			this.SetEnergyAndCapacity(this.Capacity, this.Energy - 3);
		}
		#endregion

	}
}
