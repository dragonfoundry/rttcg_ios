﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TMPro;
using DG.Tweening;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Enables initialization, value setting, and value tweening of the healthbar.
	/// </summary>
	public class PlayerHealthBarController : MonoBehaviour 
	{
		public Image HealthBarBG;
		public Image HealthBarCracks;
		public Image HealthBar;
		public TextMeshProUGUI HealthLabel;
		public int MaxHealth = 25;
		public int HealthBarSize;
		public bool isUsingImageFill = false;
		public Color DefaultColor;
		public Color DamageColor;
		public Image TopBlob;
		public Image BottomBlob;

		private int currentHealth;
		private int targetHealth;
		private float timeRemaining;
		private float healthPerSecond;
		private float pixelsPerHealth;
		private Transform healthBarTransform;
		private float accumulatedHealth;

		//====================
		// Unity Methods
		//====================
		void Awake()
		{
			this.MaxHealth = 40;
			this.HealthLabel.text = this.MaxHealth.ToString();
			this.currentHealth = this.MaxHealth;
			this.healthBarTransform = this.HealthBar.transform;
		}

		void Start()
		{
			this.InitializeHealthBarSize(this.MaxHealth);
			this.HealthBarCracks.DOFade(0,0);
		}
		
		public void InitializeHealthBarSize(int maxHealth)
		{
			this.MaxHealth = maxHealth;
			Debug.LogFormat("Initializing MaxHealth to:{0}", maxHealth);
			this.pixelsPerHealth = this.HealthBarSize / (float)this.MaxHealth;
			this.targetHealth = this.currentHealth;
			this.accumulatedHealth = 0;
			this.PositionHealthbarAtAmount(this.currentHealth);
			this.HealthBar.color = this.DefaultColor;
		}
	
#region Context Menu test fuctions
		[ContextMenu ("Apply 5 Damage...")]
		public void Apply5Damage()
		{
			this.ApplyDelta(-5, 1.0f);
		}

		[ContextMenu ("Set 30 Health...")]
		public void SetHealth22()
		{
			this.SetHealth(30, 0.5f);
		}
#endregion

		/// <summary>
		/// Sets a specific health value, will tween to that location or if duration is 0f, 
		/// change immediately to that value.
		/// </summary>
		public void SetHealth(int value, float duration)
		{
			this.targetHealth = value;
			this.timeRemaining = duration;
		}

		/// <summary>
		/// Applys a relative health amount either damage or healing to the healthbar
		/// and tweens to that value within the specified duration.
		/// </summary>
		public void ApplyDelta(int amount, float duration)
		{
			this.targetHealth += amount;
			this.timeRemaining += duration;
		}
	
		/// <summary>
		/// Positions the healthbar GameObject in worldspace at a position
		/// that represents the specified amount of health
		/// </summary>
		private void PositionHealthbarAtAmount(float healthAmount)
		{
			if(this.isUsingImageFill == false)
			{
				float yPos = (healthAmount * this.pixelsPerHealth) - this.HealthBarSize;
				//Debug.Log("YPos:" + yPos + " healthAmouint:" + healthAmount + " pixelsPerHealth:" + pixelsPerHealth);
				Vector3 localPosition = this.healthBarTransform.localPosition;
				localPosition.y = yPos;
				this.healthBarTransform.localPosition = localPosition;
			}
			else
			{
				this.HealthBar.fillAmount = healthAmount / this.MaxHealth;
			}

			if(healthAmount != this.currentHealth && this.timeRemaining > 0)
			{
				this.HealthBar.DOColor(this.DamageColor, 0.15f);
			}
			else
			{
				this.HealthBar.DOColor(this.DefaultColor, 0.5f);
			}

			float healthRatio = healthAmount / (float)Mathf.Max(1,this.MaxHealth);

			if(healthRatio <= 1/5f)
			{
				// Hide both glows
				this.TopBlob.DOFade(0,0.4f);
				this.BottomBlob.DOFade(0,0.4f);
			}
			else
			{
				// Show the bottom glow
				this.BottomBlob.DOFade(0.75f,0.4f);

				if(healthRatio > 0.85f)
				{
					this.TopBlob.DOFade(0.75f, 0.4f);
				}
				else
				{
					this.TopBlob.DOFade(0.0f, 0.4f);
				}
			}
		}

		/// <summary>
		/// Called every frame, does our healthbar tweening
		/// </summary>
		void Update()
		{
			// Early out if the healthbar is wehre it should be
			if(this.currentHealth == this.targetHealth) 
			{
				return;
			}

			// If there's no time remaining but we aren't at the target health...
			if(this.timeRemaining <= 0f && this.targetHealth != this.currentHealth)
			{
				// Jump right to the targetHealth and make it current
				this.currentHealth = this.targetHealth;
				this.PositionHealthbarAtAmount(this.currentHealth);
				this.accumulatedHealth = 0;
			}
			else
			{
				// While moving.. use the damage color
				this.HealthBar.color = this.DamageColor;

				// Calculate the amount to change health this frame.
				// Adding a 0.5 point to current health makes the tween look better
				float deltaHealth = this.targetHealth - (this.currentHealth + 0.5f);
				float healthChanged = (Time.deltaTime / this.timeRemaining) * deltaHealth;

				// Accumlate the health changes until they are larger than one unit of health
				this.accumulatedHealth += healthChanged;
				if(Mathf.Abs(this.accumulatedHealth) > 1.0f)
				{
					// Apply the integer change to current health (preserving any remainder)
					int intPart = (int) this.accumulatedHealth;
					this.accumulatedHealth -= intPart;
					this.currentHealth += intPart;

					// If we're at our target health, zero out accumulatedHealth
					if(this.currentHealth == this.targetHealth) this.accumulatedHealth = 0;
				}

				// Position the healthbar including accumulatedHealth so the tween blends smoothly between healthpoitns
				this.PositionHealthbarAtAmount(this.currentHealth + this.accumulatedHealth);
			
				this.timeRemaining -= Time.deltaTime;
			}

			// Update the health orb's label
			this.HealthLabel.text = this.currentHealth.ToString();
		}
	}
}
