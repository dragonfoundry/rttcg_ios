﻿using UnityEngine;
using UnityEditor;
using LinqTools;


[CustomPropertyDrawer(typeof(CardFxTest))]
public class CardFxTestDrawer : PropertyDrawer
{
	public override void OnGUI(Rect rect, SerializedProperty prop, GUIContent label)
	{
		var fxTest = (FxTest)prop.serializedObject.targetObject;

		EditorGUI.BeginProperty (rect, label, prop);
		SerializedProperty cardProp = prop.FindPropertyRelative("Card");
		SerializedProperty fxNameProp = prop.FindPropertyRelative("FxName");

		//Rect propertyRect = new Rect (rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight*2);
		Rect cardRect = new Rect(rect.x,rect.y, rect.width-30, EditorGUIUtility.singleLineHeight);
		EditorGUI.PropertyField(cardRect, cardProp);

		Rect fxNameRect = new Rect(rect.x,rect.y + EditorGUIUtility.singleLineHeight, cardRect.width, EditorGUIUtility.singleLineHeight);
		if(FxTest.FxNames != null && FxTest.FxNames.Count != 0)
		{
			int index = FxTest.FxNames.IndexOf(fxNameProp.stringValue);
			int newIndex = EditorGUI.Popup(fxNameRect,"FxName",index, FxTest.FxNames.ToArray());
			if(newIndex != index && newIndex < FxTest.FxNames.Count)
			{
				fxNameProp.stringValue = FxTest.FxNames[newIndex];
			}
		}
		else
		{
			if(fxTest.PooledFxPrefab != null)
			{
				fxTest.LoadFxNames();
			}
		}

		if( Application.isPlaying == false || cardProp.objectReferenceValue == null)
		{
			GUI.enabled = false;
		}

		Rect buttonRect = new Rect(rect.x + cardRect.width + 3, rect.y, 27, rect.height-1);
		Texture2D bg = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).GetStyle("ChannelStripDuckingMarker").normal.background;
		if(GUI.Button(buttonRect,bg ))
		{
			CardFrontController cardFront = (CardFrontController)cardProp.objectReferenceValue;
            //cardFront.TriggerFxScript(fxNameProp.stringValue);
            throw new System.NotImplementedException("fx now played by the arena mediator - fix this!");
		}

		EditorGUI.EndProperty ();
	}

	override public float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUIUtility.singleLineHeight * 2;
	}
}