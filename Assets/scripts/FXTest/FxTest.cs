﻿using UnityEngine;
using System.Collections.Generic;
using DragonFoundry.Fx;
using System;

public class FxTest : MonoBehaviour 
{
	public FxTrigger PooledFxPrefab;
	public ArenaLayout [] ArenaLayouts;
	public CardFxTest [] Cards;

	public static List<string> FxNames = new List<string>();

	// Use this for initialization
	void Start () 
	{
		foreach(ArenaLayout layout in this.ArenaLayouts)
		{
			layout.UpdateCardTweens();
		}

		if(this.PooledFxPrefab != null)
		{
			this.LoadFxNames();
		}
	}

	public void LoadFxNames()
	{
		FxNames.Clear();
		ParticleEffectsTrigger[] particleFx = this.PooledFxPrefab.GetComponentsInChildren<ParticleEffectsTrigger>(true);

		for (int i = 0; i < particleFx.Length; i++)
		{
			FxNames.Add(particleFx[i].gameObject.name.ToLower());
		}
	}
}

[Serializable]
public class CardFxTest
{
	public CardFrontController Card;
	public string FxName;
}
