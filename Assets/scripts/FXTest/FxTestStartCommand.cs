﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

public class FxTestStartCommand : Command 
{

	public override void Execute ()
	{
		FxTest fxTest = GameObject.FindObjectOfType(typeof(FxTest)) as FxTest;
		FxTrigger pooledFxPrefab = fxTest.PooledFxPrefab; 
		ObjectPool.CreatePool<FxTrigger>(pooledFxPrefab);
		ObjectPool.SetPoolSize(pooledFxPrefab, 6);
	}
}
