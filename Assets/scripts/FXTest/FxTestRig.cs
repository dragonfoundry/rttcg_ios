﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DragonFoundry.Fx;
using TMPro;

public class FxTestRig : MonoBehaviour 
{
	public Transform ParticleEffectsPrefab;
	public Image[] FXAnchors;
	public bool IsAllSame = true;
	public int NumberOfRepeats = 2;
    public TextMeshProUGUI FX_Name;
	private ParticleEffectsTrigger[] FxTriggers0;
	private ParticleEffectsTrigger[] FxTriggers1;
	private ParticleEffectsTrigger[] FxTriggers2;
	private ParticleEffectsTrigger[] FxTriggers3;
	private ParticleEffectsTrigger[] FxTriggers4;
	private ParticleEffectsTrigger[] FxTriggers5;

	int zero = 0;
	int one = 3;
	int two = 5;
	int three = 7;
	int four = 11;
	int five = 33;
	// Use this for initialization
	void Start () 
	{
		_FxPlayerCoroutine = FxPlayerCoroutine ();
		Timer.Instance.StartCoroutine(_FxPlayerCoroutine);
	}

	public void OnDestroy()
	{
		Timer.Instance.StopCoroutine (_FxPlayerCoroutine);
		StopTriggers(IsAllSame);
	}

	public List<ParticleEffectsTrigger> GetTriggers(bool isAllSame)
	{
		if (isAllSame)
		{
			return new List<ParticleEffectsTrigger> {
				FxTriggers0 [zero],
				FxTriggers1 [zero],
				FxTriggers2 [zero],
				FxTriggers3 [zero],
				FxTriggers4 [zero],
				FxTriggers5 [zero],
			};
		}
		else
		{
			return new List<ParticleEffectsTrigger> {
				FxTriggers0 [zero],
				FxTriggers1 [one],
				FxTriggers2 [two],
				FxTriggers3 [three],
				FxTriggers4 [four],
				FxTriggers5 [five],
			};
		}
	}

	public void StopTriggers(bool isAllSame)
	{
		var triggers = GetTriggers (isAllSame);
		foreach(var trigger in triggers)
		{
			trigger.EndEffect ();
		}
	}

	public IEnumerator _FxPlayerCoroutine { get; set;}
	public IEnumerator FxPlayerCoroutine()
	{
		yield return new WaitForSeconds (1.0f);

		for(int i = 0; i < FXAnchors.Length; i++)
		{
			Transform fxTrigger = (Transform)GameObject.Instantiate (ParticleEffectsPrefab);
			fxTrigger.AttachToParent (FXAnchors [i].transform,0);
			fxTrigger.localScale = Vector3.one * 8;
			fxTrigger.localPosition = Vector2.zero;
			//yield return new WaitForSeconds (1.0f);
		}
		FxTriggers0 = FXAnchors [0].GetComponentsInChildren<ParticleEffectsTrigger> (true);
		FxTriggers1 = FXAnchors [1].GetComponentsInChildren<ParticleEffectsTrigger> (true);
		FxTriggers2 = FXAnchors [2].GetComponentsInChildren<ParticleEffectsTrigger> (true);
		FxTriggers3 = FXAnchors [3].GetComponentsInChildren<ParticleEffectsTrigger> (true);
		FxTriggers4 = FXAnchors [4].GetComponentsInChildren<ParticleEffectsTrigger> (true);
		FxTriggers5 = FXAnchors [5].GetComponentsInChildren<ParticleEffectsTrigger> (true);
		foreach(var trigger in FxTriggers0){trigger.EndEffect ();}
		foreach(var trigger in FxTriggers1){trigger.EndEffect ();}
		foreach(var trigger in FxTriggers2){trigger.EndEffect ();}
		foreach(var trigger in FxTriggers3){trigger.EndEffect ();}
		foreach(var trigger in FxTriggers4){trigger.EndEffect ();}
		foreach(var trigger in FxTriggers5){trigger.EndEffect ();}
		zero = 0;
		one = 3;
		two = 5;
		three = 7;
		four = 11;
		five = 13;
		yield return new WaitForSeconds (1.0f);

		// Play all the FX in a loop
		while(true)
		{
			for(int i = 0; i<=NumberOfRepeats; i++)
			{
				bool isAllSame = IsAllSame;
				var nextTriggers = GetTriggers(isAllSame);
				foreach(var trigger in nextTriggers)
				{
					//yield return new WaitForSeconds(0.2f);
					trigger.StartEffect ();
                    FX_Name.text = trigger.name;
				}

				yield return new WaitForSeconds (3.0f);
				StopTriggers (isAllSame);
				yield return new WaitForSeconds(0.2f);

			}

			zero++;
			one++;
			two++;
			three++;
			four++;
			five++;
			if (zero >= FxTriggers0.Length)
				zero = 0;
			if (one >= FxTriggers1.Length)
				one = 0;
			if (two >= FxTriggers2.Length)
				two = 0;
			if (three >= FxTriggers3.Length)
				three = 0;
			if (four >= FxTriggers4.Length)
				four = 0;
			if (five >= FxTriggers5.Length)
				five = 0;
		}
	}
}