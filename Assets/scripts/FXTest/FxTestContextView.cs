﻿using UnityEngine;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.command.api;
using strange.extensions.command.impl;


public class FxTestContextView : ContextView 
{
	void Awake()
	{
		context = new FxTestContext(this, true);
		context.Start();
		GameObject.DontDestroyOnLoad(this.gameObject);
	}
}


public class FxTestContext : MVCSContext
{

	public FxTestContext() : base()
	{
	}
	
	public FxTestContext(MonoBehaviour view, bool autoStartup) : base(view, autoStartup)
	{
	}

	// Unbind the default EventCommandBinder and rebind the SignalCommandBinder
	protected override void addCoreComponents()
	{
		base.addCoreComponents();
		injectionBinder.Unbind<ICommandBinder>();
		injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
	}
	
	override public void Launch()
	{
		base.Launch();

		Debug.LogFormat("Start: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
		// Make sure you've mapped this to a StartCommand!
		StartSignal startSignal= (StartSignal)injectionBinder.GetInstance<StartSignal>();
		startSignal.Dispatch();
	}

	protected override void mapBindings()
	{
		commandBinder.Bind<StartSignal>()
				.To<FxTestStartCommand>();
	}
	
}