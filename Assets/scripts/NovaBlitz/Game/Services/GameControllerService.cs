﻿using UnityEngine;
using System.Collections;
using DragonFoundry.GameState;
using System.Collections.Generic;
using Messages;

public class GameControllerService
{
    public GameStateReadOnly GameState;
    
    [Inject] public SubmitGameActionSignal SubmitGameActionSignal {get;set;}
	[Inject] public ConcedeGameSignal ConcedeGameSignal {get;set;}

    public ICardState GetCardState(int cardId)
    {
        return this.GameState.GetCard(cardId);
    }

    public void ConcedeGame()
    {
        this.ConcedeGameSignal.Dispatch();
    }
    
    public void SubmitGameAction(GameAction gameAction)
    {
        this.SubmitGameActionSignal.Dispatch(gameAction);
    }

    public void SubmitConcedeGameAction()
    {
        this.ConcedeGame();
    }

    public void SubmitAttackAction(int sourceId, int? targetId=null)
    {
        if(targetId.HasValue == false)
        {
            targetId = this.GameState.Opponent.Id;
        }

        GameAction attackAction = this.GameState.LegalAbilities.GetAttackAction(sourceId, targetId.Value);
        if (attackAction != null)
        {
            this.SubmitGameAction(attackAction);
        }
    }

    public void SubmitActivateAction(int sourceId, int? targetId)
    {
        GameAction activateAction = this.GameState.LegalAbilities.GetActivateAction(sourceId, targetId);
        if (activateAction != null)
        {
            this.SubmitGameAction(activateAction);
        }
    }

    public int SubmitCastAction(int sourceId, int? targetId)
    {
        
        GameAction castAction = this.GameState.LegalAbilities.GetCastAction(sourceId, targetId);
        if (castAction != null)
        {
            this.SubmitGameAction(castAction);
            return castAction.AbilityId;
        }
        else
        {
            Debug.LogError("Attempting to submit cast action for sourceId "  + sourceId +  " but action is null!");
        }

        return 0;
    }

    public void SubmitBlockAction(int sourceId, int targetId)
    {
        GameAction blockAction = this.GameState.LegalAbilities.GetBlockAction(sourceId, targetId);
        if (blockAction != null)
        {
            this.SubmitGameAction(blockAction);
        }
    }

    public void SubmitUnpauseAction()
    {
        GameAction unpause = this.GameState.LegalAbilities.GetUnpauseAction(this.GameState.SelfId);
        if (unpause != null)
        {
            this.SubmitGameAction(unpause);
        }
    }

    public void SubmitDoneWithPhaseAction()
    {
        GameAction doneWitihPhase = this.GameState.LegalAbilities.GetDoneWithPhaseAction(this.GameState.SelfId);
        if (doneWitihPhase != null)
        {
            this.SubmitGameAction(doneWitihPhase);
            this.GameState.LegalAbilities.RemoveLegalAbility(doneWitihPhase.AbilityId);
        }
    }
    
    public void SubmitPauseUnpauseAction()
    {
        GameAction pauseUnpause = this.GameState.LegalAbilities.GetPauseUnpauseAction(this.GameState.SelfId);
        if (pauseUnpause != null)
        {
            this.SubmitGameAction(pauseUnpause);
            this.GameState.LegalAbilities.RemoveLegalAbility(pauseUnpause.AbilityId);
        }
    }

    public void SubmitMulliganAction()
    {
        GameAction mulligan = this.GameState.LegalAbilities.GetMulliganAction(this.GameState.SelfId);
        if (mulligan != null)
        {
            this.SubmitGameAction(mulligan);
        }
    }
    
    public List<int> GetBlockTargets(int sourceId)
    {
        return this.GameState.LegalAbilities.GetBlockTargets(sourceId);
    }
    
    public List<int>GetActivateTargets(int sourceId, out bool isTargeted)
    {
        return this.GameState.LegalAbilities.GetActivateTargets(sourceId, out isTargeted);
    }
    
    public List<int> GetCastTargets(int sourceId, out bool isTargeted)
    {
        return this.GameState.LegalAbilities.GetCastTargets(sourceId, out isTargeted);
    }
}
