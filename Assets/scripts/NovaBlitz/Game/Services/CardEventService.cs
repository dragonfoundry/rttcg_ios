﻿using UnityEngine;
using System.Collections;
using Messages;
using DragonFoundry.GameState;
using strange.extensions.signal.impl;
using System;

namespace NovaBlitz.Game
{
    public class DealCardToStartingHandSignal : Signal<ICardState, GameStateReadOnly> { }
    public class DiscardCardFromStartingHandSignal : Signal<ICardState, GameStateReadOnly> { }
    public class DealCardToOpponentStartingHandSignal : Signal<int> { } // affected cardID
    public class DiscardCardFromOpponentStartingHandSignal : Signal<int> { } // affected cardID
    public class DealCardToOpponentHandSignal : Signal<int> { } // affected cardID
    public class DealCardToHandSignal : Signal<ICardState, GameStateReadOnly> { }
    public class OpponentCastsCardSignal : Signal<ICardState, int, GameStateReadOnly> { }
    public class OpponentResolvesUnitSignal : Signal<ICardState, GameStateReadOnly> { }
    public class OpponentResolvesPowerSignal : Signal<ICardState, int, GameStateReadOnly> { } // CardState, targetId, gamestate
    public class PlayerResolvesUnitSignal : Signal<ICardState, GameStateReadOnly> { }
    public class PlayerResolvesPowerSignal : Signal<ICardState, int, GameStateReadOnly> { } // CardState, targetId, gamestate
    public class TrashPlayerCardSignal : Signal<int, CardZone, GameStateReadOnly> { }
    public class TrashOpponentCardSignal : Signal<int, CardZone, GameStateReadOnly> { }
    public class BanishPlayerCardSignal : Signal<int, GameStateReadOnly> { }
    public class BanishOpponentCardSignal : Signal<int, GameStateReadOnly> { }
    public class SpawnAndBanishPlayerCardSignal : Signal<ICardState, int, GameStateReadOnly> { } //source CardState, spawn cardID
    public class SpawnAndBanishOpponentCardSignal : Signal<ICardState, int, GameStateReadOnly> { } //source CardState, spawn cardID
    public class DiscardPlayerCardSignal : Signal<int, GameStateReadOnly> { }
    public class DiscardOpponentCardSignal : Signal<int, GameStateReadOnly> { }
    public class SpawnPlayerCardInArenaSignal : Signal<ICardState, int, CardZone, GameStateReadOnly> { } // new CardState, source cardId, fromZone
    public class SpawnOpponentCardInArenaSignal : Signal<ICardState, int, CardZone, GameStateReadOnly> { } // new CardState, source cardId, fromZone
    public class SummonPlayerCardToArenaSignal : Signal<ICardState, CardZone, GameStateReadOnly> { } // new CardState, fromZone
    public class SummonOpponentCardToArenaSignal : Signal<ICardState, CardZone, GameStateReadOnly> { } // new CardState, fromZone
    public class PlayerCardAttacksSignal : Signal<ICardState, int, GameStateReadOnly> { }
    public class OpponentCardAttacksSignal : Signal<ICardState, int, GameStateReadOnly> { }
    public class CardDamageTakenSignal : Signal<ICardState, int> { } // card, amount of damage
    public class CardHealedSignal : Signal<ICardState, int> { } // card, amount of healing
    public class PlayerDamageTakenSignal : Signal<int> { } // amount of damage
    public class OpponentDamageTakenSignal : Signal<int> { } // amount of damage
    public class PlayerHealedSignal : Signal<int> { } // Amount of healing
    public class OpponentHealedSignal : Signal<int> { } // amount of healing
    public class LoseControlOfCardSignal : Signal<ICardState> { }
    public class GainControlOfCardSignal : Signal<ICardState> { }
    public class InitiativeChangeSignal : Signal<int,ICardState> { } // player ID, ICardState source Card
    public class PlayCardFailedSignal : Signal<ICardState> { }
    public class PlayCardSucceededSignal : Signal<ICardState> { }
    public class ModifyOrSetStatEventSignal : Signal<int, float, ICardState> { } // statId, amount, affectedCardState
    public class OpponentActivateAbilitySignal : Signal<ICardState, int> { } // The card activating the ability, the target cardID (0 if there is no target) 


    public class CardEventService
    {
        public static Vector3 CardOrigin = new Vector3(10000, 10000, 0);
        public GameStateReadOnly GameState;
        
        [Inject] public ArenaCards ArenaCards {get;set;}
        
        [Inject] public DealCardToStartingHandSignal DealCardToStartingHandSignal {get;set;}
        [Inject] public DiscardCardFromStartingHandSignal DiscardCardFromStartingHandSignal {get;set;}
        [Inject] public DealCardToOpponentStartingHandSignal DealCardToOpponentStartingHandSignal {get;set;}
        [Inject] public DiscardCardFromOpponentStartingHandSignal DiscardCardFromOpponentStartingHandSignal {get;set;}
        [Inject] public OpponentCastsCardSignal OpponentCastsCardSignal {get;set;}
        [Inject] public OpponentResolvesUnitSignal OpponentResolvesUnitSignal {get;set;}
        [Inject] public OpponentResolvesPowerSignal OpponentResolvesPowerSignal {get;set;}
        [Inject] public PlayerResolvesUnitSignal PlayerResolvesUnitSignal {get;set;}
        [Inject] public PlayerResolvesPowerSignal PlayerResolvesPowerSignal {get;set;}
        [Inject] public DealCardToOpponentHandSignal DealCardToOpponentHandSignal {get;set;}
        [Inject] public DealCardToHandSignal DealCardToHandSignal {get;set;}
        [Inject] public TrashPlayerCardSignal TrashPlayerCardSignal {get;set;}
        [Inject] public TrashOpponentCardSignal TrashOpponentCardSignal {get;set;}
        [Inject] public BanishPlayerCardSignal BanishPlayerCardSignal {get;set;}
        [Inject] public BanishOpponentCardSignal BanishOpponentCardSignal {get;set;}
        [Inject] public SpawnAndBanishPlayerCardSignal SpawnAndBanishPlayerCardSignal {get;set;}
        [Inject] public SpawnAndBanishOpponentCardSignal SpawnAndBanishOpponentCardSignal {get;set;}
        [Inject] public DiscardPlayerCardSignal DiscardPlayerCardSignal {get;set;}
        [Inject] public DiscardOpponentCardSignal DiscardOpponentCardSignal {get;set;}
        [Inject] public SpawnPlayerCardInArenaSignal SpawnPlayerCardInArenaSignal {get;set;}
        [Inject] public SpawnOpponentCardInArenaSignal SpawnOpponentCardInArenaSignal {get;set;}
        [Inject] public SummonPlayerCardToArenaSignal SummonPlayerCardToArenaSignal {get;set;}
        [Inject] public SummonOpponentCardToArenaSignal SummonOpponentCardToArenaSignal {get;set;}
        [Inject] public PlayerCardAttacksSignal PlayerCardAttacksSignal {get;set;}
        [Inject] public OpponentCardAttacksSignal OpponentCardAttacksSignal {get;set;} 
        [Inject] public CardDamageTakenSignal CardDamageTakenSignal {get;set;}
        [Inject] public PlayerDamageTakenSignal PlayerDamageTakenSignal {get;set;}
        [Inject] public OpponentDamageTakenSignal OpponentDamageTakenSignal {get;set;}
        [Inject] public LoseControlOfCardSignal LoseControlOfCardSignal { get; set; }
        [Inject] public GainControlOfCardSignal GainControlOfCardSignal { get; set; }
        [Inject] public PlayCardFailedSignal PlayCardFailedSignal {get;set;}
        [Inject] public PlayCardSucceededSignal PlayCardSucceededSignal {get;set;}
        [Inject] public CardHealedSignal CardHealedSignal {get;set;}
        [Inject] public PlayerHealedSignal PlayerHealedSignal {get;set;}
        [Inject] public OpponentHealedSignal OpponentHealedSignal {get;set;}
        [Inject] public ModifyOrSetStatEventSignal ModifyOrSetStatEventSignal {get;set;}
        [Inject] public OpponentActivateAbilitySignal OpponentActivateAbilitySignal {get;set;}
        [Inject] public InitiativeChangeSignal InitiativeChangeSignal {get;set;}
        
        
        private CardFrontController GetOrCreateCardForState(ICardState cardState)
        {
            try
            {
                CardController cardFront = this.ArenaCards.GetCard(cardState.Id);

                if(cardFront != null)
                {
                    return (CardFrontController)cardFront;
                }
                else
                {
                    cardFront = (CardFrontController)this.ArenaCards.CreateCard(cardState);
                    Debug.LogFormat("#CardEventService#Creating offscreen cardFront {0}", cardFront.CardIdAndName);

                    this.ArenaCards.AddCard(cardFront);
                    cardFront.transform.position = CardOrigin; // this vector AKA way the hell offscreen -DMac
                    return (CardFrontController)cardFront;
                }
            }
            catch (Exception except)
            {
                throw except;
            }
        }
        
        internal void OnPhaseChanged(Phase phase, GameStateReadOnly gameState)
        {
            
        }

        /// <summary>
        /// Some CardEvents / Gameplay Events are driven by ZoneChangeEvent messages
        /// received by the server. This Serivce takes those messages and infers a specific
        /// gameplay event that it then dispatchds a StrangeIOC signal for.
        /// </summary>
        public void OnZoneChangeEvent(EventMessage eventMessage)
        {
            // Early out if we havn't initialized the GameState
            if(this.GameState == null)
            {
                Debug.LogWarningFormat("#CardEventService#Tried to process ZoneChangeEvent of type {0} but GameState was null", eventMessage.EventType);
                return;    
            }
            
            ICardState cardState = this.GameState.GetCard(eventMessage.Affected);
            
            Debug.LogFormat("#CardEventService#From {0} to {1} in {2} Controller:{3} CardId:{4}",
                ((CardZone)eventMessage.FromZone),
                ((CardZone)eventMessage.ToZone),
                this.GameState.Turn.Phase,
                (cardState != null ? cardState.ControllerId.ToString() : "?" ),
                (cardState != null ? cardState.Id.ToString() : "?") );
            
            bool isPlayersCard = cardState != null && cardState.ControllerId == this.GameState.SelfId;
            
            CardZone toZone = (CardZone)eventMessage.ToZone;
            CardZone fromZone = (CardZone)eventMessage.FromZone;
            
            if(toZone == CardZone.Hand || toZone == CardZone.Arena )
            {
                if(cardState != null)
                {
                    // Make sure we have an arena card for whatever the affected card is
                    this.GetOrCreateCardForState(cardState);
                }
            }
            
            // ===================
			// Mulligan Phase
			// ===================
            if(this.GameState.Turn.Phase == Phase.Mulligan)
            {
                cardState = this.GameState.GetCard(eventMessage.Affected);
                 
                switch(toZone)
                {
                    // DealCardToStartingHand
                    case CardZone.Hand:
                        if(isPlayersCard)
                        {
                            // Deal Player Card
                            // ----------------------------------------
                            this.DealCardToStartingHandSignal.Dispatch(cardState, this.GameState);
                        }
                        else
                        {
                            // Deal Opponent Card
                            // ----------------------------------------
                            this.DealCardToOpponentStartingHandSignal.Dispatch(eventMessage.Affected);
                        }
                    break;
                    // DiscardCardFromStartingHand
                    case CardZone.Void:
                        if(isPlayersCard)
                        {
                            // Dicard Player Card
                            // ----------------------------------------
                            this.DiscardCardFromStartingHandSignal.Dispatch(cardState, this.GameState);
                        }
                        else
                        {
                            // Discard Opponent card
                            // ----------------------------------------
                            this.DiscardCardFromOpponentStartingHandSignal.Dispatch(eventMessage.Affected);
                        }
                    break;
                }
                        
            }
            // ===================
			// Turn Phases
			// ===================
            else
            {

                /*
                                      _________________________________________________________________________________________________________________________
                     V  FROM ZONE     |       Deck      |      Hand       |     Stack      |     Arena      |     Trash      |     Void       |  None/Other    | < TO ZONE
                    __________________|_________________|_________________|________________|________________|________________|________________|________________|
                    |Deck             |        X        |     Draw        |     Cast*      |    Summon*     |    Mill        |    Exile*      |    Exile*      |
                    |                 |                 |                 |                |                |                |                |                |
                    |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|----------------|
                    |Hand             |    Discard**    |        X        |     Cast       |    Summon*     |    Discard     |    Exile*      |    Exile*      |
                    |                 |                 |                 |                |                |                |                |                |
                    |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|----------------|
                    |Stack            |    ToDeck*      |    Unsummon*    |       X        |    Summon      | Resolve(power) |    Exile*      |    Exile*      |
                    |                 |                 |                 |                |                | Cancel(power)  |                |                |
                    |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|----------------|
                    |Arena            |    ToDeck       |    Unsummon     |       x        |       X        |    Destroy     |    Exile       |    Exile       |
                    |                 |                 |                 |                |                |                |                |                |
                    |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|----------------|
                    |Trash            |    ToDeck       |     Draw*       |     Cast*      |    Summon*     |       X        |    Exile*      |    Exile*      |
                    |                 |                 |                 |                |                |                |                |                |
                    |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|----------------|
                    |Void             |        X        |     Draw*       |     Cast*      |    Summon*     |    Mill*       |       X        |       x        |
                    |                 |                 |                 |                |                |                |                |                |
                    |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|----------------|
                    |NoZone/Other     |        X        |     Draw*       |     Cast*      |    Summon*     |    Mill*       |       x        |       X        |
                    |                 |                 |                 |                |                |                |                |                |
                    |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|----------------|
                
                // * This effect has a different start point than the base version
                // ** This effect has a different end 
                
                //==============
                Deck
                Hand
                Stack
                Arena
                Trash
                Void
                All others (NoZone, Memory, EventLog, AllZones, CardFile)
                */

                switch (toZone)
                {
                    case CardZone.Stack:
                        
                    break;
                    case CardZone.Arena:
                        ICardState sourceCard = this.GameState.GetCard(eventMessage.Source);
                        string sourceCardName = string.Empty;
                        if (sourceCard != null) { sourceCardName = sourceCard.GetName();}
                        Debug.Log("#CardEventService#" + sourceCardName + " Spawning Card: " + cardState.GetName() + " with ID:" + cardState.Id + " into " + (isPlayersCard ? "player's" : "opponent's") +" arena from zone: " + fromZone + " to zone: " + toZone);
                        switch(fromZone)
                        {
                            // hand to Arena is special
                            case CardZone.Stack:
                            case CardZone.Hand:
                                if (isPlayersCard)
                                {
                                    this.SummonPlayerCardToArenaSignal.Dispatch(cardState, fromZone, this.GameState);
                                }
                                else
                                {
                                    this.SummonOpponentCardToArenaSignal.Dispatch(cardState, fromZone, this.GameState);
                                }
                                break;
                            case CardZone.Deck:
                                // Some card might do this... we'll want it to just spawn in the arena if it does
                            case CardZone.Trash:
                                // Rise from the grave does this
                            case CardZone.Void:
                                // This is the "Safety net" option too - any other "odd" zone change to arena works here.
                            default:
                                if (isPlayersCard)
                                {
                                    // Spawn Card to Players Arena
                                    // ----------------------------------------
                                    this.SpawnPlayerCardInArenaSignal.Dispatch(cardState, eventMessage.Source, fromZone, this.GameState);
                                
                                }
                                else
                                {
                                    // Spawn Card to Opponents Arena
                                    // ----------------------------------------
                                    this.SpawnOpponentCardInArenaSignal.Dispatch(cardState, eventMessage.Source, fromZone, this.GameState);
                                }
                            break;
                        }
                    break;
                    case CardZone.Hand:
                        if(eventMessage.FromZone == (int)CardZone.Arena || eventMessage.FromZone == (int)CardZone.Stack)
                        {
                            if (isPlayersCard)
                            {
                                Debug.Log("#CardEventService#Banishing " + cardState.GetName() + " to players hand");

                                // Banish Card to Players Hand
                                // ----------------------------------------
                                this.BanishPlayerCardSignal.Dispatch(eventMessage.Affected, this.GameState);
                            }
                            else
                            {
                                Debug.Log("#CardEventService#Banishing " + cardState.GetName() + " to opponent's hand");
                                // Banish CardBack to Opponents hand
                                // ----------------------------------------
                                this.BanishOpponentCardSignal.Dispatch(eventMessage.Affected, this.GameState);
                            }
                        }
                        else if(eventMessage.FromZone == (int)CardZone.Deck)
                        {
                            if (isPlayersCard)
                            {
                                //Debug.Log("#CardEventService#Dealing card to players hand");

                                // Deal Card to Players Hand
                                // ----------------------------------------
                                this.DealCardToHandSignal.Dispatch(cardState, this.GameState);
                            }
                            else
                            {
                                //Debug.Log("#CardEventService#Dealing card to opponent's hand");
                                // Deal CardBack to Opponents hand
                                // ----------------------------------------
                                this.DealCardToOpponentHandSignal.Dispatch(eventMessage.Affected);
                            }
                        }
                        else
                        {
                            sourceCard = this.GameState.GetCard(eventMessage.Source);

                            // When cards go from Void->Hand and they have a source...
                            if (isPlayersCard)
                            {
                                this.SpawnAndBanishPlayerCardSignal.Dispatch(sourceCard, eventMessage.Affected, this.GameState);
                            }
                            else
                            {
                                this.SpawnAndBanishOpponentCardSignal.Dispatch(sourceCard, eventMessage.Affected, this.GameState);
                            }
                        }
                    break;
                    case CardZone.EventLog:
                    case CardZone.NoZone:
                    case CardZone.Memory:
                    case CardZone.Trash:
                    case CardZone.Void:
                    case CardZone.Deck:
                        if (eventMessage.FromZone == (int)CardZone.Arena || eventMessage.FromZone == (int)CardZone.Stack)
                        {
                            if (isPlayersCard)
                            {
                                // Trash a Players card
                                // ----------------------------------------
                                this.TrashPlayerCardSignal.Dispatch(eventMessage.Affected, (CardZone)eventMessage.FromZone, this.GameState);
                            }
                            else
                            {
                                // Trash an Opponents card
                                // ----------------------------------------
                                this.TrashOpponentCardSignal.Dispatch(eventMessage.Affected, (CardZone)eventMessage.FromZone, this.GameState);
                            }
                        }
                        else if(eventMessage.FromZone == (int)CardZone.Hand)
                        {
                            if (isPlayersCard)
                            {
                                // Trash a Players card
                                // ----------------------------------------
                                this.DiscardPlayerCardSignal.Dispatch(eventMessage.Affected, this.GameState);
                            }
                            else
                            {
                                // Trash an Opponents card
                                // ----------------------------------------
                                this.DiscardOpponentCardSignal.Dispatch(eventMessage.Affected, this.GameState);
                            }
                        }
                    break;
                    //case CardZone.Deck:
                    
                    //break;
                }  
            }
        }
        
        public void OnChangeControlEvent(EventMessage message)
        {
            ICardState cardState = GameState.GetCard(message.Affected);
            
            // Make sure we have an arena card for whatever the affected card is
            this.GetOrCreateCardForState(cardState);
            
            if (cardState != null)
            {
                if (cardState.ControllerId == GameState.SelfId)
                {
                    Debug.LogFormat("#CardEventService#EventControlChangedTo Self id:{0} name:{1}", cardState.Id, cardState.GetName());
                    this.GainControlOfCardSignal.Dispatch(cardState);
                }
                else
                {
                    Debug.LogFormat("#CardEventService#EventControlChangedTo Opponent id:{0} name:{1}", cardState.Id, cardState.GetName());
                    this.LoseControlOfCardSignal.Dispatch(cardState);
                }
            }
        }

        internal void OnCastAbilitySucceeded(EventMessage message)
        {
            CardController card = this.ArenaCards.GetPendingCard(message.AbilityId);
            if(card != null)
            {
                ICardState cardState = this.GameState.GetCard(card.CardId);
                this.ArenaCards.RemovePendingCardAbility(message.AbilityId);
                this.PlayCardSucceededSignal.Dispatch(cardState);
            }
        }

        internal void OnCardAbilityFailed(EventMessage message)
        {
            CardController card = this.ArenaCards.GetPendingCard(message.AbilityId);
            if(card != null)
            {
                ICardState cardState = this.GameState.GetCard(card.CardId);
                this.ArenaCards.RemovePendingCardAbility(message.AbilityId);
                this.PlayCardFailedSignal.Dispatch(cardState);
            }
        }

        internal void OnActivateEvent(EventMessage message)
        {
            Debug.Log("#CardEventService# ActivateEvent: Affected:" + message.Affected + " Source:" + message.Source + " SourceControllerId:" + message.SourceControllerId + " Target:" + message.Amount + " OpponentControllerID:" + this.GameState.Opponent.ControllerId + " PlayerControllerID:" + this.GameState.SelfId);
            if(message.SourceControllerId != this.GameState.SelfId)
            {
                Debug.Log("#CardEventService# Dispatching OpponentAcitvateAbilitySignal");
                int targetCardId = message.Amount;
                ICardState abilityCardState = this.GameState.GetCard(message.Source);
                this.OpponentActivateAbilitySignal.Dispatch(abilityCardState, targetCardId);
            }
        }

        /// <summary>
        /// Converts an attack event message into a  player or opponent CardAttackSignal
        /// </summary>
        public void OnAttackEvent(EventMessage eventMessage)
        {
            ICardState attackingCardState = this.GameState.GetCard(eventMessage.Source);
            
            // Make sure we have an arena card for whatever the affected card is
            this.GetOrCreateCardForState(attackingCardState);
            
            int defenderId = eventMessage.Affected;
        
            bool isPlayersCard = attackingCardState != null && attackingCardState.ControllerId == this.GameState.SelfId;
            
            if(isPlayersCard)
            {
                this.PlayerCardAttacksSignal.Dispatch(attackingCardState, defenderId, this.GameState);
            }
            else
            {
                this.OpponentCardAttacksSignal.Dispatch(attackingCardState, defenderId, this.GameState);
            }
        }

        public void OnModifyOrSetStatEvent(EventMessage message)
        {
            //message.Amount // The Amount of change
            // message.Stat // id of the changed stat
            // message.Affected
            // GameData.KeywordsById
            //Debug.Log("ModifyOrSetStatEvent");
            ICardState cardState = this.GameState.GetCard(message.Affected);

            if(cardState != null)
            {
                this.ModifyOrSetStatEventSignal.Dispatch(message.ModifiedStat, (float)message.Amount, cardState);
            }
            else
            {
                Debug.LogErrorFormat("#CardEventService# ModifyOrSetStatEvent: cardState is null id:{0} Stat:{1}", message.Affected, message.ModifiedStat );
            }
        }


        /// <summary>
        /// Listents to CastEvents coming from the server and infers weather they are a Unit or a Power
        /// card. If they are opponent cards, this method dispatches signals so that listeners can do the work
        /// requred to visualize these events in the arena
        /// </summary>
        public void OnCastEvent(EventMessage eventMessage)
        {
            ICardState cardState = this.GameState.GetCard(eventMessage.Affected);

            bool isPlayersCard = cardState != null && cardState.ControllerId == this.GameState.SelfId;
            
            if(isPlayersCard)
            {
                // We don't handl to PlayerCast events because this client has already "cast"
                // the card, that's how the server knows about it to send us this event.
            }
            else
            {
                Debug.Log("#CardEventService# Opponent Dragged Card out of Hand id: " + eventMessage.Affected);
                
                // Ensure there's a CardBack for this operation
                this.ArenaCards.GetOrCreateCardBack(eventMessage.Affected);

                // Make sure we have an arena card for whatever the affected card is
                this.GetOrCreateCardForState(cardState);

                // Tell the opponent's hand to Drag a card out of the hand
                this.OpponentCastsCardSignal.Dispatch(cardState, eventMessage.Amount, this.GameState);
            }
        }

        public void OnCancelEvent(EventMessage eventMessage)
        {
            ICardState cardState = this.GameState.GetCard(eventMessage.Affected);

            bool isPlayersCard = cardState != null && cardState.ControllerId == this.GameState.SelfId;


            if (isPlayersCard)
            {
                Debug.Log("#CardEventService#This is a Cancel effect for the PLAYER. It isn't properly supported - the card should play a 'cancel' FX, and animate to the trash");
                this.PlayCardFailedSignal.Dispatch(cardState);
                this.DiscardPlayerCardSignal.Dispatch(eventMessage.Affected, this.GameState);
            }
            else
            {
                Debug.Log("#CardEventService#This is a Cancel effect for the OPPONENT. It isn't properly supported - the card should play a 'cancel' FX, and animate to the trash");
                // Make sure we have an arena card for whatever the affected card is
                this.GetOrCreateCardForState(cardState);

                Debug.Log("#CardEventService#Opponent Cast:" + (cardState.IsPower ? "Power" : "Unit - ") + cardState.GetName());
                if (cardState.IsPower)
                {
                    // Opponent Plays Power Card
                    // ----------------------------------------
                    int targetId = eventMessage.Amount;
                    this.OpponentResolvesPowerSignal.Dispatch(cardState, targetId, this.GameState);
                }
                else
                {
                    // Make sure there's a cardBack for it
                    this.ArenaCards.GetOrCreateCardBack(cardState.Id);

                    this.DiscardOpponentCardSignal.Dispatch(eventMessage.Affected, this.GameState);
                }
            }
        }

        public void OnResolveEvent(EventMessage eventMessage)
        {
            ICardState cardState = this.GameState.GetCard(eventMessage.Affected);

            Debug.Log("#CardEventService#ResolveEvent: sourceID:" + eventMessage.Source + " affectedID:" + eventMessage.Affected + " sourceControllerId:" + eventMessage.SourceControllerId + " affectedControllerId:" + eventMessage.AffectedControllerId + " GameState.SelfId:" + this.GameState.SelfId +  " targetId: " + eventMessage.Amount);
            bool isPlayersCard = eventMessage.SourceControllerId == this.GameState.SelfId;
             
            if(isPlayersCard)
            {
                Debug.Log("#CardEventService#Player Cast:" + (cardState.IsPower ? "Power" : "Unit - ") + cardState.GetName() );
                if(cardState.IsPower)
                {
                    // Opponent Plays Power Card
                    // ----------------------------------------
                    int targetId = eventMessage.Amount;
                    this.PlayerResolvesPowerSignal.Dispatch(cardState, targetId, this.GameState);
                }
                else
                {   
                    // Opponent Plays Unit Card
                    // ----------------------------------------
                    this.PlayerResolvesUnitSignal.Dispatch(cardState, this.GameState);
                }
            }
            else
            {
                // Make sure we have an arena card for whatever the affected card is
                this.GetOrCreateCardForState(cardState);
            
                Debug.Log("#CardEventService#Opponent Cast:" + (cardState.IsPower ? "Power" : "Unit - ") + cardState.GetName() );
                if(cardState.IsPower)
                {
                    // Opponent Plays Power Card
                    // ----------------------------------------
                    int targetId = eventMessage.Amount;
                    this.OpponentResolvesPowerSignal.Dispatch(cardState, targetId, this.GameState);
                }
                else
                {
                    // Opponent Plays Unit Card
                    // ----------------------------------------
                    this.OpponentResolvesUnitSignal.Dispatch(cardState, this.GameState);
                }
            }
        }

        /// <summary>
        /// Listens to initiative change events
        /// </summary>
        internal void OnGainInitiative(EventMessage message)
        {
            int sourceCardId = message.Source;
            int playerId = message.Affected;
            ICardState cardState = this.GameState.GetCard(sourceCardId);
            //Debug.LogFormat("#CardEventService# Player {0} gains initiative", playerId);
            this.InitiativeChangeSignal.Dispatch(playerId, cardState);
        }


        /// <summary>
        /// Listents to DamageTakenEvents coming from the server and infers weather they are a for a card
        /// or one of the players. Dispatches a signal for Card/Player/Opponent taking damage.
        /// </summary>
        public void OnDamageTaken(EventMessage eventMessage)
        {
            // The target taking the damage
            int targetId = eventMessage.Affected;
            
            ICardState cardState = this.GameState.GetCard(targetId);
            
            if(cardState != null)
            {
                // A Card Takes Damage
                // ----------------------------------------
                this.CardDamageTakenSignal.Dispatch(cardState, eventMessage.Amount);
            }
            else
            {
                if(targetId == this.GameState.SelfId)
                {
                    // Player Takes Damage
                    // ----------------------------------------
                    this.PlayerDamageTakenSignal.Dispatch(eventMessage.Amount);
                }
                else if(targetId == this.GameState.Opponent.Id)
                {
                    // Opponent Takes Damage
                    // ----------------------------------------
                    this.OpponentDamageTakenSignal.Dispatch(eventMessage.Amount);
                }
                else
                {
                    Debug.Log("#CardEventService#Damage taken for an unkown target - " + targetId);
                }
            }
        }
        
        /// <summary>
        /// Listents to HealingEvents coming from the server and infers weather they are a for a card
        /// or one of the players. Dispatches a signal for Card/Player/Opponent receiving damage.
        /// </summary>
        public void OnHealing(EventMessage eventMessage)
        {
             // The target taking the damage
            int targetId = eventMessage.Affected;
            
            ICardState cardState = this.GameState.GetCard(targetId);
            
            if(cardState != null)
            {
                // A Card is Healed
                // ----------------------------------------
                this.CardHealedSignal.Dispatch(cardState, eventMessage.Amount);
            }
            else
            {
                if(targetId == this.GameState.SelfId)
                {
                    // Player is Healed
                    // ----------------------------------------
                    this.PlayerHealedSignal.Dispatch(eventMessage.Amount);
                }
                else if(targetId == this.GameState.Opponent.Id)
                {
                    // Opponent is Healed
                    // ----------------------------------------
                    this.OpponentHealedSignal.Dispatch(eventMessage.Amount);
                }
                else
                {
                    Debug.LogError("#CardEventService#Healing for an unkown target - " + targetId);
                }
            }
        }
    }
}
