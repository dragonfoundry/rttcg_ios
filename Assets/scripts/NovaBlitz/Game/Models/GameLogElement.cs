﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using LinqTools;
using TMPro;
using Messages;
using NovaBlitz.UI;

namespace NovaBlitz.Game
{
    public class GameLogElement : MonoBehaviour
    {
        public RectTransform Rect;
        public Image TimeLineL;
        public Image TimeLineR;
        public Image ActionPhase;
        public TextMeshProUGUI TurnNumber;
        public Image CombatPhase;
        public Image Arrow;
        public Image TargetingArrow;
        public LayoutElement LayoutElement;

        public RectTransform Opponent;
        public Image OppCard;
        public RawImage OppArt;
        public Image OppTrash;
        public Image OppAttackIcon;
        public Image OppBlockIcon;
        public Image OppTargetIcon;
        public Image OppDamage;
        public TextMeshProUGUI OppDamageAmount;

        public RectTransform Self;
        public Image SelfCard;
        public RawImage SelfArt;
        public Image SelfTrash;
        public Image SelfAttackIcon;
        public Image SelfBlockIcon;
        public Image SelfTargetIcon;
        public Image SelfDamage;
        public TextMeshProUGUI SelfDamageAmount;
        public Image SelfEnd;
        public TextMeshProUGUI SelfEndLabel;
        public TextMeshProUGUI BelowLabel;

        private int SelfCardId;
        private int OppCardId;

        public bool IsLast { get; set; }
        private int _Width;
        public int Width { get { return _Width; } set { _Width = value; LayoutElement.preferredWidth = value; } }

        private static AssetManagerService assetManagerService;


        public Action<int,RectTransform> ShowTwoTouchAction;

        private Color CombatPhaseColor = new Color(1.0f, 0.6f, 0.0f);
        private Color ActionPhaseColor = new Color(0.0f, 0.5f, 0.8f);
        private Color AttackColor = new Color(1.0f, 0.0f, 0.0f);
        private Color AttackArrowColor = new Color(1.0f, 0.0f, 0.0f, 0.5f);
        private Color BlockColor = new Color(0.0f, 0.0f, 1.0f);
        private Color CastColor = new Color(0.0f, 1.0f, 1.0f);
        private Color CastArrowColor = new Color(0.0f, 1.0f, 1.0f, 0.5f);
        private Color UnitColor = new Color(0.7f, 0.7f, 0.7f);
        private Color ActivateColor = new Color(0.8f, 0.3f, 1.0f);
        private Color ActivateArrowColor = new Color(0.8f, 0.3f, 1.0f, 0.5f);
        private Color LoseColor = new Color(0.4f, 0.25f, 0.0f);
        private Color WinColor = new Color(0.1f, 0.85f, 0.9f);
        private Color LoseTextColor = new Color(1.0f, 0.6f, 0.0f);
        private Color WinTextColor = new Color(0.1f, 0.3f, 0.4f);
        private const int baseWidth = 150;
        private const int turnWidth = 50;
        private const int endwidth = 500;
        
        public GameLogData GameLogData { get; set; }

        public void SetAssets(GameLog log)
        {
            TurnOffObjects();
            bool isTarget = !string.IsNullOrEmpty(GameLogData.AffectedArtId) 
                || GameLogData.Affected == GameLogData.SelfId 
                || GameLogData.Affected == GameLogData.OppId;
            switch (GameLogData.EventType)
            {
                case EventTypes.ActivateEvent:
                    TargetingArrow.gameObject.SetActive(isTarget);
                    TargetingArrow.color = ActivateArrowColor;
                    LayoutElement.preferredWidth = baseWidth;
                    SetSource(GameLogData.SourceCardId, GameLogData.SourceArtId, EventTypes.ActivateEvent, GameLogData.IsSourceTrashed, GameLogData.SourceDamage, false);
                    SetAffected(GameLogData.AffectedCardId, GameLogData.AffectedArtId, isTarget ? EventTypes.TargetingEvent : EventTypes.NoEvent, GameLogData.IsAffectedTrashed, GameLogData.AffectedDamage, false);
                    break;
                case EventTypes.ResolveEvent:
                    TargetingArrow.gameObject.SetActive(isTarget);
                    TargetingArrow.color = CastArrowColor;
                    Width = baseWidth;
                    SetSource(GameLogData.SourceCardId, GameLogData.SourceArtId, EventTypes.ResolveEvent, GameLogData.IsSourceTrashed, GameLogData.SourceDamage, false);
                    SetAffected(GameLogData.AffectedCardId, GameLogData.AffectedArtId, isTarget ? EventTypes.TargetingEvent : EventTypes.NoEvent, GameLogData.IsAffectedTrashed, GameLogData.AffectedDamage, false);
                    break;
                case EventTypes.AttackingUnit:
                    Arrow.gameObject.SetActive(isTarget);
                    Arrow.color = AttackArrowColor;
                    Width = baseWidth;
                    SetSource(GameLogData.SourceCardId, GameLogData.SourceArtId, EventTypes.AttackingUnit, GameLogData.IsSourceTrashed, GameLogData.SourceDamage, false);
                    SetAffected(GameLogData.AffectedCardId, GameLogData.AffectedArtId, !string.IsNullOrEmpty(GameLogData.AffectedArtId) ? EventTypes.BlockEvent : EventTypes.NoEvent, GameLogData.IsAffectedTrashed, GameLogData.AffectedDamage, false);
                    break;
                case EventTypes.StartOfCombatEvent:
                    CombatPhase.gameObject.SetActive(true);
                    Width = turnWidth;
                    break;
                case EventTypes.StartOfTurnEvent:
                    ActionPhase.gameObject.SetActive(true);
                    TurnNumber.text = GameLogData.Turn.ToString();
                    Width = GameLogData.Turn == 1 ? baseWidth : turnWidth;
                    break;
                case EventTypes.LoseGameEvent:
                    if (GameLogData.Affected > 0)
                    {
                        Width = endwidth;
                        SelfEnd.gameObject.SetActive(true);
                        //OppEnd.gameObject.SetActive(true);
                        //OppEnd.color = GameLogData.OppId == GameLogData.Affected ? LoseColor : WinColor;
                        SelfEnd.color = GameLogData.SelfId == GameLogData.Affected ? LoseColor : WinColor;
                        //OppEndLabel.color = GameLogData.OppId == GameLogData.Affected ? LoseTextColor : WinTextColor;
                        //OppEndLabel.text = GameLogData.OppId == GameLogData.Affected ? I2.Loc.ScriptLocalization.Defeat : I2.Loc.ScriptLocalization.Victory;
                        SelfEndLabel.color = GameLogData.SelfId == GameLogData.Affected ? LoseTextColor : WinTextColor;
                        SelfEndLabel.text = GameLogData.SelfId == GameLogData.Affected ? I2.Loc.ScriptLocalization.Defeat : I2.Loc.ScriptLocalization.Victory;
                        BelowLabel.color = SelfEndLabel.color;
                        // above: Format,
                        var last = log.LogElements.LastOrDefault();
                            string format;

                        int seconds = (int)(last.LogTime - log.StartTime).Seconds;
                        TournamentUtils.TournamentFormatNames.TryGetValue(log.Format, out format);
                        BelowLabel.text = string.Format("{0}\n{1}: {2}\n{3}: {4}\n{5}\n{6}",
                            format,
                            I2.Loc.ScriptLocalization.Turns, last.Turn,
                            I2.Loc.ScriptLocalization.Length,
                            string.Format("{0}:{1}{2}", (int)(last.LogTime - log.StartTime).TotalMinutes, seconds < 10 ? "0" : string.Empty, seconds),
                            (log.StartTime.DayOfYear == DateTime.Now.DayOfYear && log.StartTime.Year == DateTime.Now.Year)
                                ? I2.Loc.ScriptLocalization.Today 
                                : (log.StartTime.AddDays(1).DayOfYear == DateTime.Now.DayOfYear && log.StartTime.AddDays(1).Year == DateTime.Now.Year)
                                    ? I2.Loc.ScriptLocalization.Yesterday 
                                    : log.StartTime.ToString("M"),
                            log.StartTime.ToString("t"));
                    }
                    else
                    {
                        Width = 0;
                    }
                    break;
                default:
                    break;
            }

            // ideally handle the case where it's your card targeting your card, or opp's card targeting opp's card (matters for targeted powers especially)
            SetTimeline();
            SetIcons();
        }

        public void SetTimeline()
        {
            TimeLineR.gameObject.SetActive(!IsLast);
            TimeLineL.color = GameLogData.Phase == Phase.Action 
                || GameLogData.Phase == Phase.PreAction 
                || GameLogData.Phase == Phase.Mulligan
                ? ActionPhaseColor
                : CombatPhaseColor;
            TimeLineR.color = GameLogData.EventType == EventTypes.StartOfTurnEvent 
                ? ActionPhaseColor 
                : (GameLogData.EventType == EventTypes.StartOfCombatEvent
                    ? CombatPhaseColor 
                    : TimeLineL.color);
        }

        private void SetIcons()
        {
            SelfBlockIcon.gameObject.SetActive(GameLogData.EventType == EventTypes.AttackingUnit
                && !string.IsNullOrEmpty(GameLogData.AffectedArtId)
                && GameLogData.AffectedControllerId == GameLogData.SelfId);
            OppBlockIcon.gameObject.SetActive(GameLogData.EventType == EventTypes.AttackingUnit
                && !string.IsNullOrEmpty(GameLogData.AffectedArtId) 
                && GameLogData.AffectedControllerId == GameLogData.OppId);

            SelfTargetIcon.gameObject.SetActive((GameLogData.EventType == EventTypes.ResolveEvent || GameLogData.EventType == EventTypes.ActivateEvent)
                && !string.IsNullOrEmpty(GameLogData.AffectedArtId)
                && GameLogData.SourceControllerId == GameLogData.SelfId);
            OppTargetIcon.gameObject.SetActive((GameLogData.EventType == EventTypes.ResolveEvent || GameLogData.EventType == EventTypes.ActivateEvent)
                && !string.IsNullOrEmpty(GameLogData.AffectedArtId)
                && GameLogData.SourceControllerId == GameLogData.OppId);
            SelfTargetIcon.gameObject.SetActive(false);
            OppTargetIcon.gameObject.SetActive(false);
            SelfAttackIcon.gameObject.SetActive(GameLogData.EventType == EventTypes.AttackingUnit
                && GameLogData.SourceControllerId == GameLogData.SelfId);
            OppAttackIcon.gameObject.SetActive(GameLogData.EventType == EventTypes.AttackingUnit
                && GameLogData.SourceControllerId == GameLogData.OppId);


        }

        private void SetSource(int cardId, string artId, EventTypes eventSubtype, bool isTrashed, int damage, bool hasInitiative)
        {
            if (GameLogData.SourceControllerId == 0)
            {

            }
            else if (GameLogData.SourceControllerId == GameLogData.SelfId)
                SetSelf(cardId, artId, eventSubtype, isTrashed, damage, hasInitiative, true);
            else
                SetOpponent(cardId, artId, eventSubtype, isTrashed, damage, hasInitiative, true);
        }

        private void SetAffected(int cardId, string artId, EventTypes eventSubtype, bool isTrashed, int damage, bool hasInitiative)
        {
            // affected has no controller
            if (GameLogData.AffectedControllerId == 0)
            {

            }
            // source and affected have the same controller
            else if (GameLogData.AffectedControllerId == GameLogData.SourceControllerId)
            {
                // need to do some cleverness here if both are controlled by the same player - pick the right one, and reposition the other. 
                if (GameLogData.AffectedControllerId == GameLogData.SelfId)
                {
                    SetOpponent(cardId, artId, eventSubtype, isTrashed, damage, hasInitiative, false);
                    Opponent.anchoredPosition = new Vector2(0, 60);
                    if (damage > 0)
                    {
                        OppDamage.rectTransform.anchoredPosition = new Vector2(0, -370);
                        TargetingArrow.gameObject.SetActive(false);
                    }
                }
                else
                {
                    SetSelf(cardId, artId, eventSubtype, isTrashed, damage, hasInitiative, false);
                    Self.anchoredPosition = new Vector2(0, -60);
                    if (damage > 0)
                    {
                        SelfDamage.rectTransform.anchoredPosition = new Vector2(0, 370);
                        TargetingArrow.gameObject.SetActive(false);
                    }
                }
            }
            // affected is controlled by you, source is not
            else if (GameLogData.AffectedControllerId == GameLogData.SelfId)
            {
                SetSelf(cardId, artId, eventSubtype, isTrashed, damage, hasInitiative, false);
                if (string.IsNullOrEmpty(artId))
                {
                    SelfDamage.rectTransform.anchoredPosition = new Vector2(0, -160);
                    Arrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 400f);
                    TargetingArrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 400f);
                    Arrow.rectTransform.anchoredPosition = new Vector2(0, -75);
                    TargetingArrow.rectTransform.anchoredPosition = new Vector2(0, -75);
                }
                else
                {
                    TargetingArrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 150f);
                    TargetingArrow.rectTransform.anchoredPosition = new Vector2(0, 0);
                }
            }
            // affected is controlled by opponent, source is not
            else
            {
                SetOpponent(cardId, artId, eventSubtype, isTrashed, damage, hasInitiative, false);
                if(string.IsNullOrEmpty(artId))
                {
                    OppDamage.rectTransform.anchoredPosition = new Vector2(0, 160);
                    Arrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 400f);
                    TargetingArrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 400f);
                    Arrow.rectTransform.anchoredPosition = new Vector2(0, 75);
                    TargetingArrow.rectTransform.anchoredPosition = new Vector2(0, 75);
                }
                else
                {
                    TargetingArrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 150f);
                    TargetingArrow.rectTransform.anchoredPosition = new Vector2(0, 0);
                }
            }
        }

        private void SetSelf(int cardId, string artId, EventTypes eventSubtype, bool isTrashed, int damage, bool hasInitiative, bool isSource)
        {

            SelfDamage.gameObject.SetActive(damage > 0 ||
                (GameLogData.EventType == EventTypes.AttackingUnit && GameLogData.AffectedControllerId == GameLogData.SelfId));
            SelfDamageAmount.text = damage.ToString();

            SelfCardId = cardId;
            UI.CardType cardType = UI.CardType.Undefined;
            UI.CardData cardData;
            if (UI.GameData.CardDictionary.TryGetValue(cardId, out cardData))
                cardType = cardData.CardType;

            SelfCard.gameObject.SetActive(!string.IsNullOrEmpty(artId));
            SelfCard.transform.localScale = Vector3.one * ((isSource || eventSubtype == EventTypes.BlockEvent) ? 1.0f : 0.85f);
            switch (eventSubtype)
            {
                case EventTypes.ActivateEvent:
                    SelfCard.color = ActivateColor;
                    break;
                case EventTypes.ResolveEvent:
                    SelfCard.color = cardType == UI.CardType.Power ? CastColor : UnitColor;
                    break;
                case EventTypes.TargetingEvent:
                    SelfCard.color = UnitColor;
                    break;
                case EventTypes.AttackingUnit:
                    SelfCard.color = AttackColor;
                    OppCard.color = BlockColor;
                    break;
            }
            if (!string.IsNullOrEmpty(artId))
            {
                if (assetManagerService == null)
                {
                    assetManagerService = GameObject.FindObjectOfType<AssetManagerService>();
                }
                assetManagerService.LoadAsset<Texture>(this.SelfArt.texture, artId, this.OnSelfTextureLoaded);
            }
            SelfTrash.gameObject.SetActive(isTrashed);
        }

        private void OnSelfTextureLoaded(Texture texture, string assetPath)
        {
            SelfArt.texture = texture;
        }
        private void OnOppTextureLoaded(Texture texture, string assetPath)
        {
            OppArt.texture = texture;
        }


        private void SetOpponent(int cardId, string artId, EventTypes eventSubtype, bool isTrashed, int damage, bool hasInitiative, bool isSource)
        {
            OppDamage.gameObject.SetActive(damage > 0 ||
                (GameLogData.EventType == EventTypes.AttackingUnit && GameLogData.AffectedControllerId == GameLogData.OppId));
            OppDamageAmount.text = damage.ToString();

            OppCardId = cardId;
            UI.CardType cardType = UI.CardType.Undefined;
            UI.CardData cardData;
            if (UI.GameData.CardDictionary.TryGetValue(cardId, out cardData))
                cardType = cardData.CardType;

            OppCard.gameObject.SetActive(!string.IsNullOrEmpty(artId));
            OppCard.transform.localScale = Vector3.one * ((isSource || eventSubtype == EventTypes.BlockEvent) ? 1.0f : 0.85f);
            switch (eventSubtype)
            {
                case EventTypes.ActivateEvent:
                    OppCard.color = ActivateColor;
                    break;
                case EventTypes.ResolveEvent:
                    OppCard.color = cardType == UI.CardType.Power ? CastColor : UnitColor;
                    break;
                case EventTypes.TargetingEvent:
                    OppCard.color = UnitColor;
                    break;
                case EventTypes.AttackingUnit:
                    OppCard.color = AttackColor;
                    SelfCard.color = BlockColor;
                    OppDamage.gameObject.SetActive(damage > 0 || GameLogData.AffectedControllerId == GameLogData.OppId);
                    break;
            }
            if (!string.IsNullOrEmpty(artId))
            {
                if (assetManagerService == null)
                {
                    assetManagerService = GameObject.FindObjectOfType<AssetManagerService>();
                }
                assetManagerService.LoadAsset<Texture>(this.OppArt.texture, artId, this.OnOppTextureLoaded);
                //OppCardArt.UpdateCardArt(artId, UI.CardAspect.NoAspect, assetManagerService);
            }
            OppTrash.gameObject.SetActive(isTrashed);
            //OppAttackIcon.gameObject.SetActive(eventSubtype == EventTypes.AttackEvent);
            //OppTargetIcon.gameObject.SetActive(eventSubtype == EventTypes.CastEvent);

            //OppInitiative.gameObject.SetActive(hasInitiative);
        }


        public void TurnOffObjects()
        {
            ActionPhase.gameObject.SetActive(false);
            CombatPhase.gameObject.SetActive(false);
            Arrow.gameObject.SetActive(false);
            TargetingArrow.gameObject.SetActive(false);
            SelfCard.gameObject.SetActive(false);
            SelfAttackIcon.gameObject.SetActive(false);
            SelfTargetIcon.gameObject.SetActive(false);
            SelfDamage.gameObject.SetActive(false);
            SelfEnd.gameObject.SetActive(false);
            OppCard.gameObject.SetActive(false);
            OppAttackIcon.gameObject.SetActive(false);
            OppTargetIcon.gameObject.SetActive(false);
            OppDamage.gameObject.SetActive(false);
            //OppEnd.gameObject.SetActive(false);
            SelfCard.transform.localScale = Vector3.one;
            OppCard.transform.localScale = Vector3.one;
            Opponent.anchoredPosition = new Vector2(0, 150);
            Self.anchoredPosition = new Vector2(0, -150);
            Arrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 150f);
            Arrow.rectTransform.localRotation = Quaternion.identity;
            Arrow.rectTransform.anchoredPosition = Vector2.zero;
            TargetingArrow.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 150f);
            TargetingArrow.rectTransform.localRotation = Quaternion.identity;
            TargetingArrow.rectTransform.anchoredPosition = Vector2.zero;
            if (GameLogData.SourceControllerId == GameLogData.OppId)
            {
                Arrow.rectTransform.localRotation *= Quaternion.Euler(180f, 0, 0);
                TargetingArrow.rectTransform.localRotation *= Quaternion.Euler(180f, 0, 0);
            }
            OppDamage.rectTransform.anchoredPosition = new Vector2(0,95);
            SelfDamage.rectTransform.anchoredPosition = new Vector2(0, -95);
        }


        public void OnClickSelfCard()
        {
            if(GameLogData != null)
                ShowTwoTouch(SelfCardId);
        }

        public void OnClickOpponentCard()
        {
            if (GameLogData != null)
                ShowTwoTouch(OppCardId);
        }

        public void ShowTwoTouch(int cardId)
        {
            if(ShowTwoTouchAction != null)
            {
                ShowTwoTouchAction.Invoke(cardId, (RectTransform)this.transform);
            }
        }

        protected void OnDestroy()
        {
            this.OppArt.texture = null;
            this.SelfArt.texture = null;
        }
    }

    public class GameLogData
    {
        public int SelfId { get; set; }
        public int OppId { get; set; }
        public int InitiativeId { get; set; }

        public EventTypes EventType { get; set; }
        public Phase Phase { get; set; }
        public int Turn { get; set; }
        public DateTime LogTime { get; set; }
        
        public int Source { get; set; }
        public int SourceCardId { get; set; }
        public string SourceArtId { get; set; }
        public int SourceControllerId { get; set; }
        public bool IsSourceTrashed { get; set; }
        public int SourceDamage = 0;

        public int Affected { get; set; }
        public int AffectedCardId { get; set; }
        public string AffectedArtId { get; set; }
        public int AffectedControllerId { get; set; }
        public bool IsAffectedTrashed { get; set; }
        public int AffectedDamage = 0;

        public int SelfHealth { get; set; }
        public int OppHealth { get; set; }

        public List<EventMessage> RelevantEvents = new List<EventMessage>();

        public GameLogData(EventTypes eventType, Phase phase, int turnNumber, int source, int affected, int selfId, int oppId, int selfHealth, int oppHealth)
        {
            LogTime = DateTime.Now;
            EventType = eventType;
            Phase = phase;
            Turn = turnNumber;
            Affected = affected;
            Source = source;
            SelfId = selfId;
            OppId = oppId;
            SelfHealth = selfHealth;
            OppHealth = oppHealth;
        }
    }

    public class GameLog
    {
        public GameFormat Format { get; set; }
        public DateTime StartTime { get; set; }
        public bool? IsPlayerWinner { get; set; }

        public PlayerData PlayerPlayerData { get; set; }
        public PlayerData OpponentPlayerData { get; set; }
        
        public List<GameLogData> LogElements = new List<GameLogData>();

        [JsonIgnore]
        public GameLogData CurrentLogData = null;

        public GameLog() { }

        public GameLog(PlayerData player, PlayerData opponent)
        {
            PlayerPlayerData = player;
            OpponentPlayerData = opponent;
            Format = player.format;
            IsPlayerWinner = null;
            StartTime = DateTime.Now;
        }
    }

    public enum ActionType
    {
        NoAction,
        Attack,
        Block,
        Target,
    }
}
