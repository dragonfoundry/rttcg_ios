﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Messages;
using LinqTools;
using strange.extensions.context.impl;
using DG.Tweening;
using NovaBlitz.UI;

namespace NovaBlitz.Game
{
    public class ArenaCards
    {
        [Inject] public UI.GameData GameData {get;set;}
        [Inject] public INovaContext ContextView {get;set; }
        public static Vector3 CardOrigin = new Vector3(10000, 10000, 0);

        public int NumPendingAbilities { get { return this.pendingCardAbilities.Count;}}
        public Dictionary<int, CardController> CardsWaitingToBeRecycled = new Dictionary<int, CardController>();

        private BiDictionary<int, CardController> arenaCardControllers = new BiDictionary<int, CardController>();
        private Dictionary<int, CardController> opponentCardBacks = new Dictionary<int, CardController>();
        private Camera uiCamera;
        private Dictionary<int,int> pendingCardAbilities = new Dictionary<int,int>(); // AbilityId, CardId

        public List<CardController> AllCardControllers { get { return arenaCardControllers.Values.ToList(); } }

        public void AddPendingCardAbility(int abilityId, int cardId)
        {
            this.pendingCardAbilities[abilityId] = cardId;
        }
        
        public void RemovePendingCardAbility(int abilityId)
        {
            this.pendingCardAbilities.Remove(abilityId);
        }
        
        public CardController GetPendingCard(int abilityId)
        {
            int cardId;
            if(this.pendingCardAbilities.TryGetValue(abilityId, out cardId))
            {
                return this.GetCard(cardId);
            }
            
            return null;
        }
        
        public void ClearPendingCardAbilities()
        {
            this.pendingCardAbilities.Clear();
        }

        public void ClearCardActionTrackers()
        {
            foreach(var card in AllCardControllers)
            {
                card.ResetTrackers();
            }
        }
        
        public CardFrontController CreateCard(ICardState cardState)
        {
            if (cardState == null)
            {
                Debug.Log("card state null on create card");
                return null;
            }
            cardState.IsUpdated = true; // This is neccessary so that rebinding of the view elements to data will happen
            CardFrontController arenaCard = this.GameData.ArenaCardPrefab.Spawn();
            arenaCard.CanvasGroup.alpha = 1.0f;
            arenaCard.CardId = cardState.Id;
            arenaCard.Initialize(cardState, cardState.ControllerId == GameData.CurrentGameData.PlayerId 
                ? GameData.CurrentGameData.PlayerCardBack 
                : GameData.CurrentGameData.OpponentCardBack, false);
            arenaCard.transform.AttachToParent(this.ContextView.MainCanvas.transform, 0);
            //Debug.LogFormat("Creating CardFrontController name: {0} id: {1} oldId:{2}", cardState.GetName(), cardState.Id, currentId);
            arenaCard.transform.localScale = Vector3.one;
            arenaCard.transform.localPosition = Vector3.zero;
            Debug.LogFormat("#ArenaCards#Created cardFront: {0} for {1}", arenaCard.CardIdAndName, cardState.ControllerId == GameData.CurrentGameData.PlayerId ? "player" : "opponent");
            return arenaCard;
        }
        
        public CardController CreateCardBack(int cardID)
        {
            CardController cardBack = this.GameData.CardBackPrefab.Spawn();
            cardBack.SetCardBack(GameData.CurrentGameData.OpponentCardBack);
            cardBack.CardId = cardID;
            cardBack.CanvasGroup.alpha = 1.0f;
            cardBack.transform.AttachToParent(this.ContextView.MainCanvas.transform, 0);
            AddCardBack(cardBack);
            Debug.LogFormat("#ArenaCards#Created cardBack: {0}", cardBack.CardIdAndName);
            return cardBack;
        }
        
        public void AddCard(CardController arenaCard)
        {
            this.arenaCardControllers[arenaCard.CardId] = arenaCard;
        }
        
        public void AddCardBack(CardController cardBack)
        {
            this.opponentCardBacks[cardBack.CardId] = cardBack;
        }

        internal void RecycleCardsAndBacks()
        {
            // Because this can occur during shutdown, it's possible some of these things have already been destroyed
            // so we null check them before recycling them.
            foreach(CardController card in this.opponentCardBacks.Values)
            {
                if(card != null)
                {
                    card.transform.DOKill();
                    card.Recycle();
                }
            }
            this.opponentCardBacks.Clear();
            
            foreach(CardController card in this.arenaCardControllers.Values)
            {
                if(card != null)
                {
                    card.transform.DOKill();
                    card.Recycle();
                }
            }

            var toRecycle = new List<PoolableMonoBehaviour>();
            foreach (var child in ContextView.MainCanvas.transform)
            {
                var poolable = child as PoolableMonoBehaviour;
                if (poolable != null)
                    toRecycle.Add(poolable);
            }
            foreach(var poolable in toRecycle)
            { 
                Debug.LogFormat("Recycling orphaned object: {0}", poolable.name);
                poolable.transform.DOKill();
                poolable.Recycle();
            }

            this.arenaCardControllers.Clear();
        }

        public CardController GetCard(int cardID)
        {
            CardController cardController = null;
            this.arenaCardControllers.TryGetValue(cardID, out cardController);
            return cardController;
        }

        public CardController GetCardWaitingForRecycle(int cardID)
        {
            CardController cardController = null;
            this.CardsWaitingToBeRecycled.TryGetValue(cardID, out cardController);
            return cardController;
        }

        /// <summary>
        /// It's possible that the bidictionary contains a different pairing of card & ID; in this case, DON'T remove the card.
        /// </summary>
        /// <param name="arenaCard"></param>
        /// <returns></returns>
        public CardController RemoveCard(CardController arenaCard)
        {
            int cardId;
            if(this.arenaCardControllers.TryGetValue(arenaCard, out cardId))
            {
                this.arenaCardControllers.Remove(arenaCard);
            }
            return arenaCard;
        }
        
        public CardController RemoveCard(int cardID)
        {
            CardController cardToRemove = null;
            if(this.arenaCardControllers.TryGetValue(cardID, out cardToRemove))
            {
                this.arenaCardControllers.Remove(cardToRemove);
            }
            return cardToRemove;
        }

        public CardController GetOrCreateCardBack(int cardID)
        {
            CardController cardBack = GetCardBack(cardID);
            if (cardBack != null)
            {
                return cardBack;
            }
            else
            {
                cardBack = this.CreateCardBack(cardID);
                cardBack.transform.position = CardOrigin; // this vector AKA way the hell offscreen
                return cardBack;
            }
        }
        
        public CardController GetCardBack(int cardID)
        {
            CardController cardBack = null;
            this.opponentCardBacks.TryGetValue(cardID, out cardBack);
            return cardBack;
        }
        
        public CardController RemoveCardBack(int cardID)
        {
            CardController cardBack = null;
            this.opponentCardBacks.TryGetValue(cardID, out cardBack);
            if(cardBack != null)
            {
                this.opponentCardBacks.Remove(cardID);
            }
            return cardBack;
        }
    }
}
