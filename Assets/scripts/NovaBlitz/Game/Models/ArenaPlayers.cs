﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArenaPlayers 
{
    public PlayerControllerView Player
    {
        get { return this.player; }
        set 
        { 
            if(value == null)
            {
                this.players.Remove(this.player.PlayerId);
            }
            else
            {
                this.players[value.PlayerId] = value;
            }
            this.player = value;
        }
    }
    
    public OpponentControllerView Opponent
    {
        get { return this.opponent; }
        set 
        { 
            if(value == null)
            {
                this.players.Remove(this.opponent.PlayerId);
            }
            else
            {
                this.players[value.PlayerId] = value;
            }
            this.opponent = value;
        }
    }
    
    public PlayerControllerBase GetPlayer(int playerId)
    {
        PlayerControllerBase player = null;
        this.players.TryGetValue(playerId, out player);
        
        if(player == null)
        {
            Debug.LogError("Could not find player with id: " + playerId);
        }
        
        return player;
    }
    
    public void Clear()
    {
        this.player = null;
        this.opponent = null;
        this.players.Clear();
    }
    
    
    private Dictionary<int, PlayerControllerBase> players = new Dictionary<int,PlayerControllerBase>();
    private PlayerControllerView player;
    private OpponentControllerView opponent;
	
}
