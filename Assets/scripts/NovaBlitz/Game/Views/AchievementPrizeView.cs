﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class AchievementPrizeView : AchievementView
	{
        /*
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public GameData GameData {get;set;}
		[Inject] public AssetManagerService AssetManagerService { get;set;}

		[SerializeField]
		protected PrizeView _PrizePrefab;
		[SerializeField]
		protected RectTransform _PrizeAnchor;
		protected IAnimatedUI _AnimWidget;

        protected override void Awake()
		{
			base.Awake();
			_AnimWidget = this.GetComponent<IAnimatedUI>();
			_AnimWidget.EventAnimOpen += OnAnimOpen;
		}

		protected void OnAnimOpen()
		{
            // set the frame; could easily do this later.
            int lvl;
            int prog;
            this.PlayerProfile.UserStatistics.TryGetValue("ach_" + Data.Name, out lvl);
            this.PlayerProfile.UserStatistics.TryGetValue(Data.Name, out prog);
            this.RefreshStats(lvl, prog);

            Debug.LogFormat("level {0}, lvl {1}, prog {2}", lvl, lvl, prog);

		}

		protected void OnLevelReached(int level)
        {

			if (null != this.Data.Rewards) {
				PrizeData[] rewardsThisLevel;
				if (this.Data.Rewards.TryGetValue(level, out rewardsThisLevel)) {
					foreach (var pdata in rewardsThisLevel) {
						var prizeView = UITools.SpawnRect(_PrizePrefab, _PrizeAnchor);
						var itemPrize = pdata as ItemPrizeData;
						if (null != itemPrize)
                        {
							ProductData prod; this.GameData.ProductCache.TryGetValue(itemPrize.ItemID, out prod);
							itemPrize.ItemData = prod;

                            AvatarDataContract avatar;
                            GameData.AvatarByItemIdDictionary.TryGetValue(itemPrize.ItemID, out avatar);
                            itemPrize.ArtId = avatar.ArtId;
                            itemPrize.AssetManagerService = this.AssetManagerService;
                        } //HACK Cheating with View injection. Syncing itemID with its catalog data should happen elsewhere.
						prizeView.Data = pdata;
						var animWidget = prizeView.GetComponent<IAnimatedUI>();
						if (null != animWidget) animWidget.IsOpen = true;
					}
				}
			}
		}*/
	}
}
