﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using DragonFoundry.GameState;
using Messages;
using System;

public class TurnControllerView : View
{
    public Image ActionBar;
    public Image OpponentActionBar;
    public Image CombatBar;
    public Image BarBackground;
    public Sprite ActionBackground;
    public Sprite CombatBackground;
    public Button EndTurnButton;
	public NovaAudioPlayer TimerSound;
    public NovaAudioPlayer CombatSound;
    public float TicksPerTurn = 160f; // Set at the Beginning of each turn in UpdateBars()
    public Phase TurnPhase {get; private set;}
    public Color ButtonLabelEnabled;
    public Color ButtonLabelDefault;
    public System.Action DoneButtonClicked;

    public NovaButton PauseButton;
    public TextMeshProUGUI PauseText;
    public System.Action PauseButtonClicked;

    private bool wasPaused = false;
    private bool isPaused = false;
    private bool isTimerOff = false;
    private float ticksPerSecond;
    const float DECIMAL_TO_PERCENT = 100f;     // Convert a number from the range of 0.0-1.0 to the range 0-100
    const float PERCENT_TO_DECIMAL = 1f/100f;  // Convert a number from the range of 0-100 to the range 0.0-1.0
    
	private List<float> TimerSoundTicks;

    private float ticksRemaining;
    private float percentRemaining;
    private float secondsRemaining;
    private float opponentTicksRemaining;
    private float opponentPercentRemaining;
    private float opponentSecondsRemaining;
    
    new void Start()
    {
        base.Start();
        this.TurnPhase = Phase.NoPhase;
        this.Initialize();
    }
    
    public void Initialize()
    {
        this.ActionBar.fillAmount = 0;
        this.OpponentActionBar.fillAmount = 0;
        this.CombatBar.fillAmount = 0;
		//this.EndTurnButtonText.text = "DONE";
		this.TimerSoundTicks = new List<float> { 49f, 39f, 30f, 22f, 15f };
        this.EndTurnButton.interactable = false;
        this.PauseButton.interactable = false;
        this.PauseText.text = I2.Loc.ScriptLocalization.Pause;
        this.GetComponentInChildren<Canvas>().worldCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
    }

    public void UpdateDoneButton(GameStateReadOnly gameState)
    {
        // Update the state of the Done button based on weather we can be done or not
        this.EndTurnButton.interactable = this.CanBeDoneWithPhase(gameState);
    }

    public void UpdatePauseButton(GameStateReadOnly gameState)
    {
        // Update the state of the Done button based on weather we can be done or not
        this.PauseButton.interactable = this.CanPauseUnpause(gameState);
    }

    private bool CanBeDoneWithPhase(GameStateReadOnly gameState)
    {
        return gameState.LegalAbilities.CanBeDoneWithPhase(gameState.SelfId)
                && (this.TurnPhase == Phase.Action || this.TurnPhase == Phase.Combat);
    }

    private bool CanPauseUnpause(GameStateReadOnly gameState)
    {
        return gameState.LegalAbilities.CanPauseUnpause(gameState.SelfId);
                //&& (this.TurnPhase == Phase.Action || this.TurnPhase == Phase.Combat);
    }

    /// <summary>
    /// click handler for the done button
    /// </summary>
    public void DoneWithPhaseButtonClicked()
    {
        this.EndTurnButton.interactable = false;
        if(this.DoneButtonClicked != null)
        {
            this.DoneButtonClicked.Invoke();
        }
    }

    /// <summary>
    /// click handler for the pause unpause button
    /// </summary>
    public void PauseGameButtonClicked()
    {
        this.PauseButton.interactable = false;
        if (this.PauseButtonClicked != null)
        {
            this.PauseButtonClicked.Invoke();
        }
    }

    /// <summary>
    /// Updates the bar position every frame
    /// <summary>
    void Update()
    {
        if(wasPaused != isPaused)
        {
            wasPaused = isPaused;
            this.PauseText.text = isPaused ? I2.Loc.ScriptLocalization.Resume : I2.Loc.ScriptLocalization.Pause;
        }
        // Early out if we are paused (so the bar doesn't keep moving at the end of game for example)
        if(this.isPaused || this.isTimerOff || this.TurnPhase == Phase.GameOver) { return; }
        
        if(this.TurnPhase == Phase.PreCombat)
        {
            this.CombatBar.fillAmount = 1.0f;
            return;
        }
        
        // Update the current state of percent remaining
        this.percentRemaining = (float) this.ActionBar.fillAmount * DECIMAL_TO_PERCENT;
        this.ticksRemaining -= this.ticksPerSecond * Time.unscaledDeltaTime;
        this.secondsRemaining = this.ticksRemaining / this.ticksPerSecond;
        this.opponentPercentRemaining = (float)this.OpponentActionBar.fillAmount * DECIMAL_TO_PERCENT;
        this.opponentTicksRemaining -= this.ticksPerSecond * Time.unscaledDeltaTime;
        this.opponentSecondsRemaining = this.opponentTicksRemaining / this.ticksPerSecond;

        // Calculate the percentage the bar should decrease every frame to finish when the ticks expire
        float barPercentPerSecond = secondsRemaining > 0f 
            ? percentRemaining / secondsRemaining 
            : 50f; // fallback amount (move the bar quickly!)
        float opponentBarPercentPerSecond = opponentSecondsRemaining > 0f
            ? opponentPercentRemaining / opponentSecondsRemaining
            : 50f;
            
        // Used Delta time to smoothly tween the bar down
        percentRemaining -= barPercentPerSecond * Time.unscaledDeltaTime;
        opponentPercentRemaining -= opponentBarPercentPerSecond * Time.unscaledDeltaTime;
        
        // Set the fill amount of the bar based on the calculated percentRemaining
        this.ActionBar.fillAmount = Mathf.Clamp01(percentRemaining * PERCENT_TO_DECIMAL);
        this.CombatBar.fillAmount = this.ActionBar.fillAmount;
        this.OpponentActionBar.fillAmount = Mathf.Clamp01(opponentPercentRemaining * PERCENT_TO_DECIMAL);
        
        /*if(this.NonBlitzActionBar.fillAmount > this.ActionBar.fillAmount)
        {
            // Below 0 blitz ticks; the nonBlitz bar and action bar should move together
            this.NonBlitzActionBar.fillAmount = ActionBar.fillAmount;
        }*/

		// Fire off audio when the timer's low
		//float displayedTicksRemaining = ProgressBar.fillAmount * 100f * timerScalingRatio;
		if (TimerSoundTicks != null && TimerSoundTicks.Count > 0 && secondsRemaining > 0 && secondsRemaining < TimerSoundTicks [0]) {
			for (int i = 0; i < TimerSoundTicks.Count; i++) 
			{
				bool playSound = false;
				if (ticksRemaining > 0 && ticksRemaining < TimerSoundTicks [i]) {
					playSound = true;
					TimerSoundTicks.RemoveAt (i);
				}
                if (playSound && this.EndTurnButton.interactable)
                {
                    TimerSound.Play();
                    //Debug.LogFormat("Ping:{0}", secondsRemaining);
                }
			}
		}
    }
    
    /// <summary>
    /// Only called when a diffGameState message arrives
    /// </summary>
    public void UpdateBars(TurnState turnState)
    {
        Phase previousPhase = this.TurnPhase;

        //Debug.Log("TurnState:" + turnState.Phase + " ticksRemaining:" + turnState.TicksRemaining + " time:" + Time.unscaledTime);
        
        // Initialize turn state variables
        this.TurnPhase = turnState.Phase;
        this.isPaused = turnState.IsPaused;
        this.isTimerOff = turnState.IsTimerOff;
        this.ticksPerSecond = turnState.ticksPerSecond;
        this.ticksRemaining = turnState.TicksRemaining;
        this.opponentTicksRemaining = turnState.BlitzTicksRemaining;
        
        // Update the current state of percent remaining bsaed on the bar amount
        this.percentRemaining = this.ActionBar.fillAmount * DECIMAL_TO_PERCENT;
        this.opponentPercentRemaining = this.OpponentActionBar.fillAmount * DECIMAL_TO_PERCENT;
        
        // Logic for when the turn phase changes
        if(previousPhase != this.TurnPhase)
        {
            // This is really important, set the number of ticks in this turn
            // when the turn starts so that all our percent calcuations work.
            this.TicksPerTurn = this.ticksRemaining;

			this.TimerSoundTicks = new List<float> { 49f, 39f, 30f, 22f, 15f };
            //this.TimerSoundTicks = new List<float> {3.6f,2.5f,1.6f,0.9f,0.4f,0.1f};
            switch (this.TurnPhase)
            {
                case Phase.Action:
                    this.ActionBar.enabled = true;
                    this.OpponentActionBar.enabled = true;
                    this.CombatBar.enabled = false;
                break;
                case Phase.PreCombat:
                    this.CombatSound.Play();
                    this.ActionBar.enabled = false;
                    this.OpponentActionBar.enabled = false;
                    this.CombatBar.enabled = true;
                    break;
                case Phase.Combat:
                    this.ActionBar.enabled = false;
                    this.OpponentActionBar.enabled = false;
                    this.CombatBar.enabled = true;
                break;
                default:
                    this.ActionBar.enabled = false;
                    this.OpponentActionBar.enabled = false;
                    this.CombatBar.enabled = false;
                break;
            }
            
            // Override turn state when phases change
            this.percentRemaining = (this.ticksRemaining / this.TicksPerTurn) * DECIMAL_TO_PERCENT;
            this.opponentPercentRemaining = (this.opponentTicksRemaining / this.TicksPerTurn) * DECIMAL_TO_PERCENT;
            this.ActionBar.fillAmount = this.percentRemaining * PERCENT_TO_DECIMAL;
            this.CombatBar.fillAmount = this.ActionBar.fillAmount;
            this.OpponentActionBar.fillAmount = this.opponentPercentRemaining * PERCENT_TO_DECIMAL;
        }
        
        // Override percentRemaining when ticksRemaining has suddenly jumped above what the bar is visualizing 
        // (by a half second worth ticks)
        if( (this.ticksRemaining / this.TicksPerTurn) * DECIMAL_TO_PERCENT > this.percentRemaining + (this.ticksPerSecond * 0.5f)
            || (this.opponentTicksRemaining / this.TicksPerTurn) * DECIMAL_TO_PERCENT > this.opponentPercentRemaining + (this.ticksPerSecond * 0.5f)
            || (this.ticksRemaining == 0.0f && this.opponentTicksRemaining == 0.0f))
        { 
            // If the server has added time.. make the bar percentage snap to it
            this.percentRemaining = (this.ticksRemaining / this.TicksPerTurn) * DECIMAL_TO_PERCENT;
            this.opponentPercentRemaining = (this.opponentTicksRemaining / this.TicksPerTurn) * DECIMAL_TO_PERCENT;
			if(this.ticksRemaining > 50f)
				this.TimerSoundTicks = new List<float> { 49f, 39f, 30f, 22f, 15f };

            // Apply percent remaining to the bar!
            if (this.ActionBar.enabled)
                this.ActionBar.fillAmount = this.percentRemaining * PERCENT_TO_DECIMAL;
            if(this.OpponentActionBar.enabled)
                this.OpponentActionBar.fillAmount = this.opponentPercentRemaining * PERCENT_TO_DECIMAL;
            if(this.CombatBar.enabled)
                this.ActionBar.fillAmount = this.percentRemaining * PERCENT_TO_DECIMAL;
        }
    }
}
