﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;
using Messages;
using DG.Tweening;
using System.Globalization;

namespace NovaBlitz.UI
{
    public class UpdatedPrizeView : PoolableMonoBehaviour
    {
        [SerializeField] protected TextMeshProUGUI _NameDisplay;
        [SerializeField] protected TextMeshProUGUI _QuantityDisplay;
        [SerializeField] protected Image _PrizeChestImage;
        [SerializeField] protected Image _GraphicDisplay;
        [SerializeField] protected Sprite _DefaultSprite;

        [SerializeField] protected Sprite _NaniteSprite;
        [SerializeField] protected Sprite _NaniteGlow;
        [SerializeField] protected Sprite _CreditsSprite;
        [SerializeField] protected Sprite _CreditsGlow;
        [SerializeField] protected Sprite _QPSprite;
        [SerializeField] protected Sprite _QPGlow;
        [SerializeField] protected Sprite _GemsSprite;
        [SerializeField] protected Sprite _GemsGlow;
        [SerializeField] protected Sprite _BasicPackSprite;
        [SerializeField] protected Sprite _SmartPackSprite;
        [SerializeField] protected Sprite _SmartPackGlow;
        [SerializeField] protected Image _SecondPackSprite;
        [SerializeField] protected Sprite _TwoSmartPackGlow;
        [SerializeField] protected Image _ThirdPackSprite;
        [SerializeField] protected Sprite _ThreeSmartPackGlow;
        [SerializeField] protected AvatarView AvatarWidget;
        [SerializeField] protected Sprite[] _AvatarGlows;
        [SerializeField] protected CardBackView CardBackWidget;
        [SerializeField] protected Sprite _CardBackGlow;
        [SerializeField] public AchievementView AchievementWidget;
        [SerializeField] public CanvasGroup CanvasGroup;
        [SerializeField] public CanvasGroup RarityGlowCanvasGroup;
        [SerializeField] public Image RarityGlow1;
        [SerializeField] public Image RarityGlow2;
        [SerializeField] public RarityGlowColors NanoBotColor;
        [SerializeField] public RarityGlowColors FreeEventColor;
        [SerializeField] public RarityGlowColors NovaGemColor;
        [SerializeField] public RarityGlowColors CreditsColor;
        [SerializeField] public RarityGlowColors ItemColor;
        [SerializeField] public Sprite UnitAlphaMask;

        public int NodeNumber { get; set; }
        public Action<int> CollectButtonClicked;
        public Sprite PrizeAlphaMask { get; set; }

        public RectTransform Rect { get; protected set; }
        
        public Prize Data { get; set; }

        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.UpdatedPrizeView; } }

        protected void Awake()
        {
            this.Rect = GetComponent<RectTransform>();
        }

        public void Initialize(Prize prizeData, GameData gameData, PlayerProfile playerProfile, GameOverMessage msg)
        {
            this.Data = prizeData;

            if (null == this.Data)
                return;

            if(prizeData.PrizeSource == PrizeSource.achievement)
            {
                // set the achievement view active (on the back), and animate its sliders
            }
            else if(prizeData.PrizeSource == PrizeSource.quest)
            {

            }

            int winsToday = 0;
            if (msg.AchievementTrackValue != null)
                msg.AchievementTrackValue.TryGetValue("daily_wins", out winsToday);

            _SecondPackSprite.gameObject.SetActive(false);
            _ThirdPackSprite.gameObject.SetActive(false);
            PrizeAlphaMask = null;
            if (null != _GraphicDisplay && null != _NameDisplay)
            {
                if (this.Data.PrizeType == PrizeType.currency)
                {
                    _GraphicDisplay.gameObject.SetActive(true);
                    switch (this.Data.CurrencyCode)
                    {
                        case CurrencyType.CR:
                            _GraphicDisplay.sprite = _CreditsSprite;
                            PrizeAlphaMask = _CreditsGlow;
                            switch (this.Data.PrizeSource)
                            {
                                case PrizeSource.annuity:
                                    _NameDisplay.SetText(playerProfile.CreditAnnuityExpiration.HasValue
                                        ? (playerProfile.CreditAnnuityExpiration.Value - DateTime.UtcNow).TotalDays <= 1
                                            ? I2.Loc.ScriptLocalization.AnnuityRewardCredit
                                            : string.Format(I2.Loc.ScriptLocalization.AnnuityRewardCredits, (int)(playerProfile.CreditAnnuityExpiration.Value - DateTime.UtcNow).TotalDays, 0, 0)
                                        : I2.Loc.ScriptLocalization.DailyRewardCredits);
                                    break;
                                case PrizeSource.dlc:
                                case PrizeSource.dailyReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.DlcRewardCredits);
                                    break;
                                case PrizeSource.season:
                                    _NameDisplay.SetText(string.Format(
                                        I2.Loc.ScriptLocalization.Season_Prize_Awarded,
                                        CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Server.Time.AddMonths(-1).Month), 0, 0));
                                    break;
                                case PrizeSource.kickstarterReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.KickstarterReward);
                                    break;
                                case PrizeSource.casual:
                                    if(winsToday > 0)
                                        _NameDisplay.SetText(string.Format(I2.Loc.ScriptLocalization.Wins_Today, winsToday, 0, 0));
                                    else
                                        _NameDisplay.SetText(I2.Loc.ScriptLocalization.Prize_Awarded);
                                    break;
                                default:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.Prize_Awarded);
                                    break;
                            }
                            break;
                        case CurrencyType.NG:
                            _GraphicDisplay.sprite = _GemsSprite;
                            PrizeAlphaMask = _GemsGlow;
                            switch(this.Data.PrizeSource)
                            {
                                case PrizeSource.annuity:
                                    _NameDisplay.SetText(playerProfile.GemAnnuityExpiration.HasValue
                                        ? (playerProfile.GemAnnuityExpiration.Value - DateTime.UtcNow).TotalDays <= 1
                                            ? I2.Loc.ScriptLocalization.AnnuityRewardGem
                                            : string.Format(I2.Loc.ScriptLocalization.AnnuityRewardGems, (int)(playerProfile.GemAnnuityExpiration.Value - DateTime.UtcNow).TotalDays, 0, 0)
                                        : I2.Loc.ScriptLocalization.DailyRewardGems);
                                    break;
                                case PrizeSource.dlc:
                                case PrizeSource.dailyReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.DlcRewardGems);
                                    break;
                                case PrizeSource.season:
                                    _NameDisplay.SetText(string.Format(
                                        I2.Loc.ScriptLocalization.Season_Prize_Awarded,
                                        CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Server.Time.AddMonths(-1).Month), 0, 0));
                                    break;
                                case PrizeSource.kickstarterReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.KickstarterReward);
                                    break;
                                case PrizeSource.casual:
                                    if (winsToday > 0)
                                        _NameDisplay.SetText(string.Format(I2.Loc.ScriptLocalization.Wins_Today, winsToday, 0, 0));
                                    else
                                        _NameDisplay.SetText(I2.Loc.ScriptLocalization.Prize_Awarded);
                                    break;
                                default:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.Prize_Awarded);
                                    break;
                            }
                            break;
                        case CurrencyType.SP:
                            _GraphicDisplay.sprite = _SmartPackSprite;
                            PrizeAlphaMask = _SmartPackGlow;
                            switch (this.Data.PrizeSource)
                            {
                                case PrizeSource.annuity:
                                    DateTime? expiration = playerProfile.SteamAnnuityExpiration.HasValue ? playerProfile.SteamAnnuityExpiration : playerProfile.SmartPackAnnuityExpiration;
                                    playerProfile.SteamAnnuityExpiration = null; // can null this out - we only need to use it once.
                                    _NameDisplay.SetText(expiration.HasValue
                                        ? (expiration.Value - DateTime.UtcNow).TotalDays <= 1
                                            ? I2.Loc.ScriptLocalization.AnnuityRewardSmartPack
                                            : string.Format(I2.Loc.ScriptLocalization.AnnuityRewardSmartPacks, (int)(expiration.Value - DateTime.UtcNow).TotalDays, 0, 0)
                                        : I2.Loc.ScriptLocalization.DailyRewardSmartPacks);
                                    break;
                                case PrizeSource.dlc:
                                case PrizeSource.dailyReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.DlcRewardSmartPacks);
                                    break;
                                case PrizeSource.season:
                                    _NameDisplay.SetText(string.Format(
                                        I2.Loc.ScriptLocalization.Season_Prize_Awarded,
                                        CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Server.Time.AddMonths(-1).Month), 0, 0));
                                    break;
                                case PrizeSource.kickstarterReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.KickstarterReward);
                                    break;
                                default:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.Prize_Awarded);
                                    break;
                            }
                            if (this.Data.Quantity > 1)
                            {
                                _SecondPackSprite.gameObject.SetActive(true);
                                PrizeAlphaMask = _TwoSmartPackGlow;
                            }
                            if (this.Data.Quantity > 2)
                            {
                                _ThirdPackSprite.gameObject.SetActive(true);
                                PrizeAlphaMask = _ThreeSmartPackGlow;
                            }
                            break;
                        case CurrencyType.QP:
                            _GraphicDisplay.sprite = _QPSprite;
                            PrizeAlphaMask = _QPGlow;
                            _NameDisplay.SetText(I2.Loc.ScriptLocalization.Qualifier_Points_Won);
                            break;
                        case CurrencyType.FD:
                            _GraphicDisplay.sprite = _BasicPackSprite;
                            PrizeAlphaMask = _SmartPackGlow;
                            _NameDisplay.SetText(I2.Loc.ScriptLocalization.Free_Draft_Earned);
                            break;
                        case CurrencyType.FC:
                            _GraphicDisplay.sprite = _BasicPackSprite;
                            PrizeAlphaMask = _SmartPackGlow;
                            _NameDisplay.SetText(I2.Loc.ScriptLocalization.Free_League_Earned);
                            break;
                        case CurrencyType.FT:
                            _GraphicDisplay.sprite = _BasicPackSprite;
                            PrizeAlphaMask = _SmartPackGlow;
                            _NameDisplay.SetText(I2.Loc.ScriptLocalization.Free_Tournament_Earned);
                            break;
                        case CurrencyType.NC:
                            _GraphicDisplay.sprite = _NaniteSprite;
                            PrizeAlphaMask = _NaniteGlow;
                            switch (this.Data.PrizeSource)
                            {
                                case PrizeSource.annuity:
                                    _NameDisplay.SetText(playerProfile.NanoBotAnnuityExpiration.HasValue
                                        ? (playerProfile.NanoBotAnnuityExpiration.Value - DateTime.UtcNow).TotalDays <= 1
                                            ? I2.Loc.ScriptLocalization.AnnuityRewardNanoBot
                                            : string.Format(I2.Loc.ScriptLocalization.AnnuityRewardNanoBots, (int)(playerProfile.NanoBotAnnuityExpiration.Value - DateTime.UtcNow).TotalDays, 0, 0)
                                        : I2.Loc.ScriptLocalization.DailyRewardNanoBots);
                                    break;
                                case PrizeSource.dlc:
                                case PrizeSource.dailyReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.DlcRewardNanoBots);
                                    break;
                                case PrizeSource.season:
                                    _NameDisplay.SetText(string.Format(
                                        I2.Loc.ScriptLocalization.Season_Prize_Awarded,
                                        CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Server.Time.AddMonths(-1).Month), 0, 0));
                                    break;
                                case PrizeSource.kickstarterReward:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.KickstarterReward);
                                    break;
                                default:
                                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.Prize_Awarded);
                                    break;
                            }
                            break;
                        default:
                            _GraphicDisplay.sprite = _DefaultSprite;
                            PrizeAlphaMask = _CardBackGlow;
                            break;
                    }
                    if (null != _QuantityDisplay)
                        _QuantityDisplay.text = string.Format("+{0} {1}", this.Data.Quantity, CurrencyUtils.GetName(this.Data.CurrencyCode, this.Data.Quantity));
                }
                else
                {
                    _GraphicDisplay.gameObject.SetActive(false);
                }

                AvatarDataContract avatar;
                if (this.Data.PrizeType == PrizeType.avatar && gameData.AvatarByItemIdDictionary.TryGetValue(this.Data.PrizeId, out avatar))
                {
                    _NameDisplay.text =
                        this.Data.PrizeSource == PrizeSource.season ? string.Format(
                            I2.Loc.ScriptLocalization.Season_Prize_Awarded,
                            CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Server.Time.AddMonths(-1).Month),
                            0,
                            0)
                        : I2.Loc.ScriptLocalization.Prize_Awarded;
                    _QuantityDisplay.text = I2.Loc.ScriptLocalization.Avatar_Awarded;
                    PrizeAlphaMask = _AvatarGlows[0];
                    this.AvatarWidget.gameObject.SetActive(false);
                    this.AvatarWidget.AvatarArt.UpdateCardArt(avatar.ArtId, CardAspect.NoAspect);
                    Timer.Instance.StartCoroutine(WaitToShowAvatarPrize());
                }
                else
                {
                    this.AvatarWidget.gameObject.SetActive(false);
                }
                string questAchievementArtId = null;

                CardBackDataContract cardBack;
                if (this.Data.PrizeType == PrizeType.cardBack && !string.IsNullOrEmpty(this.Data.PrizeId))
                {
                    if (gameData.CardBackByItemIdDictionary.TryGetValue(this.Data.PrizeId, out cardBack))
                    {
                        _NameDisplay.text =
                            this.Data.PrizeSource == PrizeSource.season ? string.Format(
                                I2.Loc.ScriptLocalization.Season_Prize_Awarded,
                                CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Server.Time.AddMonths(-1).Month),
                                0,
                                0)
                            : I2.Loc.ScriptLocalization.Prize_Awarded;
                        _QuantityDisplay.text = I2.Loc.ScriptLocalization.CardBackAwarded;
                        PrizeAlphaMask = _CardBackGlow;
                        this.CardBackWidget.gameObject.SetActive(true);
                        this.CardBackWidget.UpdateCardBackView(new CardBackData { cardBackArtId = cardBack.ArtId, cardBackId = cardBack.Id, isOwned = true, cardBackItemId = this.Data.PrizeId });
                    }
                    else
                    {
                        string artId = null;
                        switch (this.Data.PrizeId)
                        {
                            case "arcane_starter":
                                _NameDisplay.text = I2.Loc.ScriptLocalization.arcane_starter;
                                artId = "ArcaneCardBack";
                                questAchievementArtId = "DC0353";
                                break;
                            case "tech_starter":
                                _NameDisplay.text = I2.Loc.ScriptLocalization.tech_starter;
                                artId = "TechCardBack";
                                questAchievementArtId = "DC0351";
                                break;
                            case "divine_starter":
                                _NameDisplay.text = I2.Loc.ScriptLocalization.divine_starter;
                                artId = "DivineCardBack";
                                questAchievementArtId = "DC0352";
                                break;
                            case "nature_starter":
                                _NameDisplay.text = I2.Loc.ScriptLocalization.nature_starter;
                                artId = "NatureCardBack";
                                questAchievementArtId = "DC0354";
                                break;
                            case "chaos_starter":
                                _NameDisplay.text = I2.Loc.ScriptLocalization.chaos_ctarter;
                                artId = "ChaosCardBack";
                                questAchievementArtId = "DC0355";
                                break;
                            default:
                                break;
                        }
                        if(artId != null)
                        {
                            _QuantityDisplay.text = I2.Loc.ScriptLocalization.Starter_Deck_Unlocked;
                            PrizeAlphaMask = _CardBackGlow;
                            this.CardBackWidget.gameObject.SetActive(true);
                            this.CardBackWidget.UpdateCardBackView(new CardBackData { cardBackArtId = artId, cardBackId = 0, isOwned = true, cardBackItemId = this.Data.PrizeId });
                        }
                        else
                        {
                            this.CardBackWidget.gameObject.SetActive(false);
                        }
                        
                    }
                }
                else
                {
                    this.CardBackWidget.gameObject.SetActive(false);
                }


                AchievementData achievement;
                int oldLevel;
                int oldScore;
                int newScore;
                int newLevel;
                if (this.Data.PrizeSource == PrizeSource.achievement 
                    && gameData.AchievementDataCache.TryGetValue(this.Data.AchievementName, out achievement)
                    && msg.AchievementTrackValue != null && msg.AchievementTrackValue.TryGetValue(this.Data.AchievementName, out newScore)
                    && msg.OldAchievementTrackValue != null && msg.OldAchievementTrackValue.TryGetValue(this.Data.AchievementName, out oldScore)
                    && msg.OldAchievements != null && msg.OldAchievements.TryGetValue("ach_" + this.Data.AchievementName, out oldLevel)
                    && msg.Achievements != null && msg.Achievements.TryGetValue("ach_" + this.Data.AchievementName, out newLevel))
                {
                    UnitAlphaMask = _AvatarGlows[0];
                    //PrizeAlphaMask = _AvatarGlows[0];
                    achievement.oldScore = oldScore;
                    achievement.newScore = newScore;
                    achievement.oldLevel = oldLevel;
                    achievement.newLevel = newLevel;
                    this.AchievementWidget.Data = achievement;
                    var achievementName = string.Format("{0}:\n{1}", I2.Loc.ScriptLocalization.Achievement, this.AchievementWidget._NameDisplay.text);
					var prizename = string.Format("{0}:\n{1} {2}", I2.Loc.ScriptLocalization.Achievement, newScore, this.AchievementWidget._NameDisplay.text);
					this.AchievementWidget._NameDisplay.text = achievementName;
                    this._NameDisplay.text = prizename; // the "prize" side shows the number as well
                    this.AchievementWidget.gameObject.SetActive(true);
                    this._PrizeChestImage.gameObject.SetActive(false);
                }
                else if (this.Data.PrizeSource == PrizeSource.quest)
                {
                    UnitAlphaMask = _AvatarGlows[0];
                    // We had to overload PrizeId to pass the art Id in here
                    // a better system might have been to have an art Id in there that we could use for a variety of things
                    if(string.IsNullOrEmpty(questAchievementArtId))
                        questAchievementArtId = this.Data.PrizeId;
                    this.AchievementWidget.InitializeAsQuest(questAchievementArtId, this.Data.AchievementName, I2.Loc.ScriptLocalization.Quest_Completed);
                    this._NameDisplay.text = this.AchievementWidget._NameDisplay.text; // make the back & front texts the same

                    this.AchievementWidget.gameObject.SetActive(true);
                    this._PrizeChestImage.gameObject.SetActive(false);
                }
                else
                {
                    UnitAlphaMask = _CardBackGlow;
                    this.AchievementWidget.gameObject.SetActive(false);
                    this._PrizeChestImage.gameObject.SetActive(true);
                }
            }
        }


        public void SetTypeGlowColor(CurrencyType currency, bool isRevealed)
        {
            this.RarityGlowCanvasGroup.gameObject.SetActive(true);


            switch (currency)
            {
                case CurrencyType.NC:
                    this.RarityGlow1.color = this.NanoBotColor.GlowColor;
                    this.RarityGlow2.color = this.NanoBotColor.BurnColor;
                    this._QuantityDisplay.color = this.NanoBotColor.BurnColor;
                    this._NameDisplay.color = this.NanoBotColor.BurnColor;
                    break;
                case CurrencyType.SP:
                    this.RarityGlow1.color = this.NovaGemColor.GlowColor;
                    this.RarityGlow2.color = this.NovaGemColor.BurnColor;
                    this._QuantityDisplay.color = this.CreditsColor.BurnColor;
                    this._NameDisplay.color = this.CreditsColor.BurnColor;
                    break;
                case CurrencyType.NG:
                case CurrencyType.QP:
                    this.RarityGlow1.color = this.NovaGemColor.GlowColor;
                    this.RarityGlow2.color = this.NovaGemColor.BurnColor;
                    this._QuantityDisplay.color = this.NovaGemColor.BurnColor;
                    this._NameDisplay.color = this.NovaGemColor.BurnColor;
                    break;
                case CurrencyType.CR:
                    this.RarityGlow1.color = this.CreditsColor.GlowColor;
                    this.RarityGlow2.color = this.CreditsColor.BurnColor;
                    this._QuantityDisplay.color = this.CreditsColor.BurnColor;
                    this._NameDisplay.color = this.CreditsColor.BurnColor;
                    break;
                case CurrencyType.FD:
                case CurrencyType.FC:
                case CurrencyType.FT:
                    this.RarityGlow1.color = this.FreeEventColor.GlowColor;
                    this.RarityGlow2.color = this.FreeEventColor.BurnColor;
                    this._QuantityDisplay.color = this.CreditsColor.BurnColor;
                    this._NameDisplay.color = this.CreditsColor.BurnColor;
                    break;
                default:
                    this.RarityGlow1.color = this.ItemColor.GlowColor;
                    this.RarityGlow2.color = this.ItemColor.BurnColor;
                    this._QuantityDisplay.color = this.CreditsColor.BurnColor;
                    this._NameDisplay.color = this.CreditsColor.BurnColor;
                    break;
            }
            if (!isRevealed)
            {
                if (this.Data.PrizeSource == PrizeSource.achievement || this.Data.PrizeSource == PrizeSource.quest)
                {
                    this.RarityGlow1.color = this.CreditsColor.GlowColor;
                    this.RarityGlow2.color = this.CreditsColor.BurnColor;
                    this._QuantityDisplay.color = this.CreditsColor.BurnColor;
                    this._NameDisplay.color = this.CreditsColor.BurnColor;
                }
                else
                {
                    this.RarityGlow1.color = this.FreeEventColor.GlowColor;
                    this.RarityGlow2.color = this.FreeEventColor.BurnColor;
                    this._QuantityDisplay.color = this.CreditsColor.BurnColor;
                    this._NameDisplay.color = this.CreditsColor.BurnColor;
                }
            }
            
            this._NameDisplay.DOFade(0.0f, 0.0f);
            this._QuantityDisplay.DOFade(0.0f, 0.0f);
            this._NameDisplay.rectTransform.DOLocalMoveY(0.0f, 0.0f);
        }

        public void ScrollText()
        {
            Sequence seq = DOTween.Sequence();
            seq.Insert(0f, this._NameDisplay.rectTransform.DOLocalMoveY(240f, 2f, false));
            seq.Insert(0f, this._NameDisplay.DOFade(1.0f, 0.5f));
            seq.Insert(0.25f, this._QuantityDisplay.DOFade(1.0f, 1.0f));
            //seq.Append(this._NameDisplay.DOFade(0.0f, 1.0f));
        }

        private IEnumerator WaitToShowAvatarPrize()
        {
            while (!this.AvatarWidget.AvatarArt.IsArtLoaded)
            {
                yield return null;
            }
            this.AvatarWidget.gameObject.SetActive(true);
        }

        public void ClearTextures()
        {
            this.RarityGlowCanvasGroup.gameObject.SetActive(false);
            this.RarityGlowCanvasGroup.DOFade(0.0f, 0.0f);
            this.AvatarWidget.AvatarArt.ResetArt();
            this.CardBackWidget._CardBackDisplay.texture = null;
            this.AchievementWidget.AchievementArt.ResetArt();
        }

        public void OnCollectClick()
        {
            if(CollectButtonClicked != null)
            {
                CollectButtonClicked.Invoke(NodeNumber);
            }
        }

        public void OnDestroy()
        {
            CollectButtonClicked = null;
            ClearTextures();
        }

        public override void OnRecycle()
        {
            CollectButtonClicked = null;
            ClearTextures();
        }
    }
}
