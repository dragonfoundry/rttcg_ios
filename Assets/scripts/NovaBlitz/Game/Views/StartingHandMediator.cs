﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using System;
using Messages;
using DragonFoundry.Controllers.Display;
using DragonFoundry.GameState;
using DragonFoundry.Controllers;
using NovaBlitz.Game;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class StartingHandMediator : Mediator
	{
		[Inject] public StartingHandView View {get;set;}
        [Inject] public GameData GameData {get;set;}
        [Inject] public ArenaCards ArenaCards {get;set;}
        [Inject] public DealCardToStartingHandSignal DealCardToStartingHandSignal {get;set;}
        [Inject] public DiscardCardFromStartingHandSignal DiscardCardFromStartingHandSignal {get;set;}
        [Inject] public GameControllerService GameControllerService {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
        [Inject] public GameEventMessageSignal GameEventMessageSignal { get; set; }
		[Inject] public INovaContext NovaContext {get;set;}

        private LegalAbilities LegalAbilities
        {
            get 
            {
                return this.View.GameState.LegalAbilities;    
            }
        }
		
		
        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
		override public void OnRegister()
		{
			this.View.KeepHand += this.OnKeepHand;
			this.View.ReplaceHand += this.OnReplaceHand;
			this.View.Dismiss += this.OnDismiss;
            this.View.OnEarlyDeath += this.OnRemove;
            this.DealCardToStartingHandSignal.AddListener(this.OnCardDealtToStartingHand);
            this.DiscardCardFromStartingHandSignal.AddListener(this.OnCardDiscardedFromStartingHand);
            this.GameEventMessageSignal.AddListener(this.OnGameEventMessage);
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		override public void OnRemove()
		{
			this.View.KeepHand -= this.OnKeepHand;
			this.View.ReplaceHand -= this.OnReplaceHand;
			this.View.Dismiss -= this.OnDismiss;
            this.View.OnEarlyDeath -= this.OnRemove;
            this.DealCardToStartingHandSignal.RemoveListener(this.OnCardDealtToStartingHand);
            this.DiscardCardFromStartingHandSignal.RemoveListener(this.OnCardDiscardedFromStartingHand);
            this.GameEventMessageSignal.RemoveListener(this.OnGameEventMessage);
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);

            //Debug.LogWarning("Shutting down Starting hand");
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
            
            }
        }

        private void OnCardDiscardedFromStartingHand(ICardState cardState, GameStateReadOnly gameState)
        {
            if(cardState != null && cardState.ControllerId == gameState.SelfId)
            {
                Debug.Log("Card Discarded from StartingHand:" + cardState.Id);
                this.View.DiscardCard(cardState);
            }
        }

        private void OnCardDealtToStartingHand(ICardState cardState, GameStateReadOnly gameState)
        {
            // New way of creating cards from pool
            if(cardState != null && cardState.ControllerId == gameState.SelfId)
            {
                this.View.AddCardToHand(cardState);
            }
        }


        private void OnGameEventMessage(EventMessage message)
        {
            if(message.EventType == EventTypes.PlayerActionFailure)
            {
                this.View.OnMulliganFailed();
            }
        }

        private void OnDismiss()
        {
            this.Dismiss();
        }

        private void OnReplaceHand()
        {
            this.GameControllerService.SubmitMulliganAction();
        }

        private void OnKeepHand()
        {
            if (this.GameControllerService.GameState.LegalAbilities.CanBeDoneWithPhase(this.GameControllerService.GameState.SelfId))
            {
                this.GameControllerService.SubmitDoneWithPhaseAction();
            }
            else
            {
                this.GameControllerService.SubmitUnpauseAction();
            }
        }
		
		private void Dismiss()
		{
             //  Move the cards from the StartingHandDialog to the players hand
            PlayerHandController playerHandController = FindObjectOfType<PlayerHandController>();
            RectTransform handLayout = (RectTransform)playerHandController.HandLayout.transform;

            // Unregister from hand card events when we dismiss
            this.DealCardToStartingHandSignal.RemoveListener(this.OnCardDealtToStartingHand);
            this.DiscardCardFromStartingHandSignal.RemoveListener(this.OnCardDiscardedFromStartingHand);
            this.GameEventMessageSignal.RemoveListener(this.OnGameEventMessage);

            if (playerHandController != null)
            {
                // If the handController is using a zoomHand, instantly zoom it
                if(playerHandController.IsUsingZoomHand)
                {
                    playerHandController.ActivateZoomedHand(true);
                }

                CardController[] cards = this.View.HandRoot.GetComponentsInChildren<CardController>();
                for(int i=0;i<cards.Length;i++)
                {
                    CardController card = cards[i];
                    card.transform.SetParent(handLayout,true);
                    card.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
                    this.View.RecycleInsert(card);
                }
                
                // Tween in the starting hand
                playerHandController.HandLayout.TweenInStartingHand();
            }
            
            this.StartCoroutine(this.DelayHideView());
		}
        
        private IEnumerator DelayHideView()
        {
            yield return new WaitForSecondsRealtime(0.4f);
            
            this.CloseAnimatedViewSignal.Dispatch(typeof(StartingHandView));

             Camera uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
             PlayerHandController playerHandController = FindObjectOfType<PlayerHandController>();
             // Is the mouse over the PlayerHand RectTransform?
             bool isOverHand = RectTransformUtility.RectangleContainsScreenPoint((RectTransform)playerHandController.transform, Input.mousePosition, uiCamera);
      
             #if UNITY_IOS || UNITY_ANDROID
                isOverHand = isOverHand && Input.touchCount > 0;
             #endif

             if(playerHandController.IsUsingZoomHand && isOverHand == false)
             {
                 yield return new WaitForSecondsRealtime(0.4f);
				playerHandController.MustSetZoomHandActiveTo = false;
             }
        }
	}
}