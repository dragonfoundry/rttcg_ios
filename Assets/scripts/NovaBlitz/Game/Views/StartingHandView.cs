﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DragonFoundry.Controllers;
using DragonFoundry.Controllers.Display;
using Messages;
using TMPro;
using DragonFoundry.GameState;
using System.Collections.Generic;
using NovaBlitz.Game;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class StartingHandView : AnimatedView 
	{
        [Inject] public GameData GameData {get;set;}
        [Inject] public ArenaCards ArenaCards {get;set;}
		public Transform HandRoot;
		public Image ProgressBar;
		public Image LightGlow;
		public Image FrameGlow;
		public Image ProgressBarFrame;
        public GameStateReadOnly GameState;
        public CardDealer PlayerCardDealer;
		
		public Button KeepButton;
		public Button ReplaceButton;
        public TextMeshProUGUI KeepText;
        public GameObject MulliganHint;
        public RectTransform TrashDestination;

		public NovaAudioPlayer GameStartedSound;
		public NovaAudioPlayer TimerSound;
        public NovaAudioPlayer DealSound;
        public NovaAudioPlayer DiscardSound;

        public System.Action KeepHand;
		public System.Action ReplaceHand;
		public System.Action Dismiss;
        public System.Action OnEarlyDeath;

		private List<float> TimerSoundTicks;
		private const float timerScalingRatio = 1.2f; // This code is built on a 10-second timer. We're currently using 12s for mulligans.
		private bool wasInMulliganPhase = false;
        private bool hasClicked = false;
        private bool isDismissed = false;
        private RectTransform InsertTemplate;
        private Queue<RectTransform> insertTransforms = new Queue<RectTransform>();
        private BiDictionary<CardController, RectTransform> hoveredCardInserts = new BiDictionary<CardController, RectTransform>();
        private bool isWaitingForReplace = false;
		

		public void OnKeepHandClicked()
		{
			this.SetButtonState(false, false);
            this.hasClicked = true;
            if (this.KeepHand != null)
			{
				this.KeepHand.Invoke();
			}
		}
		
		public void OnReplaceHandClicked()
		{
			this.SetButtonState(false, false);
            this.hasClicked = true;
            this.isWaitingForReplace = true;
            if(this.dismissCoroutine != null)
            {
                this.StopCoroutine(dismissCoroutine);
                this.dismissCoroutine = null;
            }
            if (this.ReplaceHand != null)
			{
				this.ReplaceHand.Invoke();
			}
		}

        public void OnMulliganFailed()
        {
            this.isWaitingForReplace = false;
        }
		
		public void Initialize(GameStateReadOnly gameState)
		{
			this.GameState = gameState;
			this.ProgressBar.fillAmount = 0;
			this.TimerSoundTicks = new List<float> {1000f, 33,25,17f,9f,1f};
			this.wasInMulliganPhase = false;
			this.isDismissed = false;
			this.SetButtonState(false, false);
			//this.TimerSound.Play ();
		}
		
		public void SetButtonState(bool canMulligan, bool canKeep)
		{
			this.KeepButton.interactable = canKeep;
			this.ReplaceButton.interactable = canMulligan;
			this.KeepButton.gameObject.SetActive(canMulligan || canKeep);
			this.ReplaceButton.gameObject.SetActive(canMulligan);
		}
        
        public void DiscardCard(ICardState cardState)
        {
            CardController card = this.ArenaCards.GetCard(cardState.Id);
            this.ArenaCards.RemoveCard(card);
            RectTransform insert = this.hoveredCardInserts[card];
            card.transform.DOKill();
            
            card.transform.SetParent(this.TrashDestination,true);
            Sequence seq = DOTween.Sequence();
            seq.AppendCallback(() => this.DiscardSound.Play());
            seq.Join(card.CanvasGroup.DOFade(0.0f, 0.6f));
            seq.Join(card.transform.DOLocalMove(Vector3.zero, 1.0f).SetEase(Ease.OutQuart));
            seq.Join(card.transform.DOScale(Vector3.one * 0.5f, 0.6f).SetEase(Ease.InExpo));
            seq.AppendCallback(() => {
                card.Recycle();
                insert.SetParent(this.transform.GetChild(0));
                this.insertTransforms.Enqueue(insert);
            });
        }
        
        public void RecycleInsert(CardController card)
        {
             RectTransform insert = this.hoveredCardInserts[card];
             insert.SetParent(this.transform.GetChild(0));
             this.insertTransforms.Enqueue(insert);
        }

        public void AddCardToHand(ICardState cardState)
        {
            // Get the ArenaCard and hide it
            CardController arenaCard = this.ArenaCards.GetCard(cardState.Id);
            arenaCard.CanvasGroup.alpha = 0;
            arenaCard.transform.localScale = Vector3.one;
            
            // Add an insert immediately
            RectTransform insert = this.insertTransforms.Dequeue();
            insert.SetParent(this.HandRoot);
            insert.localPosition = Vector3.zero;
            
            // Attach the arena Card to the insert
            arenaCard.transform.SetParent(insert);
            arenaCard.transform.localPosition = Vector3.zero;
            arenaCard.Canvas.overrideSorting = false;
            ((RectTransform)arenaCard.transform).anchoredPosition = new Vector2(110, 150);
            
            // Link the insert and the card
            this.hoveredCardInserts[insert] = arenaCard;
            
            // Start a co-routine wtih a delay for animating in the card based on its position in the hand
            Timer.GameInstance.StartCoroutine(this.DealCardToStartingHand(arenaCard, 0.4f + (insert.GetSiblingIndex()+1) * 0.15f));
        }
        
        private IEnumerator DealCardToStartingHand(CardController activeCard, float delay)
        {
            float now = Time.timeSinceLevelLoad;
            while(!this.IsOpen && Time.timeSinceLevelLoad - now < 4.0f)
            {
                yield return null;
            }
            yield return new WaitForSecondsRealtime(delay);

            if (this == null)
                yield break;

            // Position the card at the Players CardDealer / Deck
            activeCard.transform.position = this.PlayerCardDealer.CardAnimationRoot.position;
            activeCard.transform.rotation = Quaternion.Euler(0f,180f,0f);
            
            // Do the tweens to the insert
            activeCard.CanvasGroup.DOFade(1f, 0.2f);
            this.DealSound.Play();
            activeCard.transform.DOLocalMove(Vector3.zero, 0.7f).SetEase(Ease.OutQuart);
            activeCard.transform.DOLocalRotate(Vector3.zero, 0.7f).SetEase(Ease.InExpo);
            activeCard.transform.DOScale(this.targetScale, 0.5f).SetEase(Ease.InExpo);
            this.isWaitingForReplace = false;
            this.hasClicked = false;
        }
        
        private Vector3 TargetScale()
        {
            return Vector3.one * ((Camera.main.aspect > 1.6f) ? 2f : 1.65f);
        }
        Vector3 targetScale;

        new void Start()
        {
            base.Start();
            this.InsertTemplate = (RectTransform)this.HandRoot.GetChild(0);
            this.InsertTemplate.SetParent(this.transform.GetChild(0));
            this.PlayerCardDealer = GameObject.FindObjectOfType<PlayerHandController>().CardDealer;
			this.FrameGlow.DOFade(0,0);

            targetScale = TargetScale();
            // Create a few insterts for use dealing cards
            for (int i=0;i<20;i++)
            {
                RectTransform insert = (RectTransform)(Instantiate(this.InsertTemplate.gameObject, Vector3.zero, Quaternion.identity) as GameObject).transform;
                insert.SetParent(this.InsertTemplate.parent);
                insert.localScale = Vector3.one;
				LayoutElement layoutElement = insert.GetComponent<LayoutElement>();
				layoutElement.preferredWidth *= targetScale.x * 0.9f;
				layoutElement.preferredHeight *= targetScale.y * 0.9f;
                this.insertTransforms.Enqueue(insert);
            }
        }
		
		new void Update()
		{
			base.Update();
            
			// No updates if we're dismissed
			if(this.isDismissed == true)
			{
				// Make sure the progress bar shows that it's finished
				this.ProgressBar.DOFillAmount(0f, 0.1f);
				return;
			}

            if (GameState == null)
                return;

            bool canMulligan = GameState.LegalAbilities != null && GameState.LegalAbilities.GetMulliganAction(this.GameState.SelfId) != null;
            bool canunpause = GameState.LegalAbilities.GetUnpauseAction(this.GameState.SelfId) != null;
            bool canKeep = GameState.LegalAbilities != null && (GameState.LegalAbilities.GetDoneWithPhaseAction(this.GameState.SelfId) != null);
            this.KeepText.text = canunpause ? I2.Loc.ScriptLocalization.Play : I2.Loc.ScriptLocalization.Keep;
            this.MulliganHint.gameObject.SetActive(GameData.CurrentGameData.GameFormat == GameFormat.Tutorial && !hasClicked && canMulligan && !canKeep);
            if (!hasClicked)
            {
                this.SetButtonState(canMulligan, (canKeep || canunpause));
            }

            if (this.GameState.Turn.IsPaused)
                return;

            float displayedTicksRemaining = ProgressBar.fillAmount * 100f * timerScalingRatio;
			if (displayedTicksRemaining > 0 && TimerSoundTicks.Count > 0 && displayedTicksRemaining < TimerSoundTicks [0]) {
				for (int i = 0; i < TimerSoundTicks.Count; i++) {
					bool playSound = false;
					if (displayedTicksRemaining < TimerSoundTicks [i]) {
						playSound = true;
						TimerSoundTicks.RemoveAt (i);
					}

					if (playSound && (this.KeepButton.interactable || this.ReplaceButton.interactable))
					{
						TimerSound.Play ();
						this.FrameGlow.DOFade(1f,0.3f).From();
					}
				}
			}

            			
			if(this.wasInMulliganPhase == false && this.GameState.Turn.Phase != Phase.NoPhase)
			{
				this.wasInMulliganPhase = true;
			}
			
			if(this.wasInMulliganPhase && GameState.Turn.TicksRemaining <= 0f && !isWaitingForReplace)
			{
				if(this.dismissCoroutine == null)
				{
                    if(hasClicked == false)
                    {
                        this.SetButtonState(false, false);
                        this.hasClicked = true;
                    }
					this.dismissCoroutine = this.WaitToDismiss(0.2f);
					Timer.GameInstance.StartCoroutine(this.dismissCoroutine);
					this.ProgressBarFrame.DOFade(0.25f, 0.2f);
				}
				return;
			}

            if (this.GameState.Turn.IsPaused)
                return;
			
			float ticksRemaining = GameState.Turn.TicksRemaining;
			float percentRemaining = (float) this.ProgressBar.fillAmount * 100f;
			float secondsRemaining = ticksRemaining / GameState.Turn.ticksPerSecond;
			
			// The following assumption is now no longer true. We're hacking in a change to 20s; ideally this code is refactored - PB
			// 		any time you see "/timerScalingRatio", that's the hack.
			// There are 100 ticks in a turn bar which represents 10 seconds. So the number of ticks remaining
			// is also the percentage of the bar remaining, when percent is expressed as a whole number between 0-100


			// This checks to see if the ticksRemaining has suddenly jumped above what the bar is visualizing (by a half second or 5 ticks)
			if(ticksRemaining / timerScalingRatio > percentRemaining + 5 || ticksRemaining == 0.0f) 
			{ 
				// If the server has added time.. make the bar percentage snap to it
				percentRemaining = ticksRemaining / timerScalingRatio; 
			}

			// Calculate the percentage the bar should decrease every frame to finish when the ticks expire
			float barPercentPerSecond = secondsRemaining > 0f ? percentRemaining / secondsRemaining : GameState.Turn.ticksPerSecond / timerScalingRatio;

			// Used Delta time to smoothly tween the bar down
			percentRemaining -= barPercentPerSecond * Time.deltaTime;

			// Set the fill amount of the bar based on the calculated percentRemaining
			this.ProgressBar.fillAmount = Mathf.Clamp01(percentRemaining / 100f);

			// Offset the bar to the left the same amount so we keep the pointy bit on the right of the bar
			/*Vector2 pos = this.ProgressBar.transform.localPosition;
			pos.x = -(this.ProgressBar.rectTransform.rect.width - (this.ProgressBar.rectTransform.rect.width * this.ProgressBar.fillAmount));
			this.ProgressBar.transform.localPosition = pos;*/
		}

		private IEnumerator dismissCoroutine = null;

		private IEnumerator WaitToDismiss(float seconds)
		{
			yield return seconds;

            if (this == null)
                yield break;
			if(this.wasInMulliganPhase && GameState.Turn.TicksRemaining <= 0f && !isWaitingForReplace)
			{
				if(this.isDismissed == false)
				{
					this.isDismissed = true;
					if(this.Dismiss != null)
					{
						this.Dismiss.Invoke();
					}
				}
			}
			else
			{
				// reset our dismissed state
				this.dismissCoroutine = null;
			}
		}

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}
