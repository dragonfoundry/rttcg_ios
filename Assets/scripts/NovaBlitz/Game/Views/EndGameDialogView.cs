﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using Messages;
using System.Collections.Generic;
using TMPro;
using NovaBlitz.UI;
using DragonFoundry.Fx;
using DragonFoundry.Controllers;
using DragonFoundry.Networking;
using DG.Tweening;

namespace NovaBlitz.Game
{
    public class EndGameDialogView : AnimatedView
	{
        public Action CloseDialogAction;
        public Action<ShareScreenShotParams> EventShareClicked;
        public Action EventLeaderboardClicked;
        public Action EventGameLogClicked;
        public Action EventReportBugClicked;

        public NovaAudioPlayer MenuTheme;
		
		public GameObject DefeatBanner;
        public TextMeshProUGUI DefeatLabel;
        public GameObject VictoryBanner;
        public TextMeshProUGUI VictoryLabel;
		public RectTransform DefeatBannerBar;
		public RectTransform VictoryBannerBar;
		public CanvasGroup VictoryBannerText;
                
        public bool IsShowPackOpener { get; set; }

        public GameObject PlayerCardParent;
        public PlayerCardView PlayerCard;

        public GameObject BottomRow;
        public NovaButton GameLogButton;
        public NovaButton ReportBugButton;
        public Button LeaderboardButton;
        public Button ShareButton;
        public NativeShare SharingPlugin;
        public NovaButton NextScrim;
        public Image NextButtonImage;
        public TextMeshProUGUI NextButtonText;

        public NovaAudioPlayer DefeatSound;
        public NovaAudioPlayer VictorySound;

        //public PlayerRankView RankDisplay;
        protected float _FallbackTimeout = 0.55f;

        public ShowMainMenuParams ShowMainMenuParams { get; set; }
        public GameOverMessage GameOverMessage { get; set; }

		public bool Victory 
		{
			set 
			{ 
				bool isVictorious = value;

                this.VictoryBanner.SetActive(false);
                this.DefeatBanner.SetActive(false);

                Sequence seq = DOTween.Sequence();
				seq.AppendInterval(0.5f);
				
				if(isVictorious)
				{
					// Victory 
					this.VictoryBannerBar.DOSizeDelta(new Vector2(0,this.VictoryBannerBar.sizeDelta.y),0).OnComplete( () => this.VictoryBanner.SetActive(true));
					this.VictoryBannerText.alpha = 0;
					
					seq.Append(this.VictoryBannerBar.DOSizeDelta(new Vector2(1920,this.VictoryBannerBar.sizeDelta.y),0.5f));
					//seq.Insert(1, this.RankRoot.transform.DOScale(0.25f,1f).From().SetEase(Ease.OutElastic));
					//seq.Insert(1, this.RankRoot.DOFade(0,0.25f).From());
					
					Sequence textSeq = DOTween.Sequence();
					textSeq.AppendInterval(0.4f);
                    textSeq.AppendCallback(() => this.VictorySound.Play());
					textSeq.Append(this.VictoryBannerText.DOFade(1.0f, 0.2f));
					textSeq.Append(((RectTransform)this.VictoryBannerText.transform).DOScale(3f, 0.25f).From());
					textSeq.Play();
				}
				else
				{
					// Defeat
					this.DefeatBannerBar.DOSizeDelta(new Vector2(0,this.VictoryBannerBar.sizeDelta.y),0).OnComplete(() => this.DefeatBanner.SetActive(true));
					seq.Append(this.DefeatBannerBar.DOSizeDelta(new Vector2(1920,this.VictoryBannerBar.sizeDelta.y),0.5f));
                    seq.AppendCallback(() => this.DefeatSound.Play());
                    //seq.Insert(1, this.RankRoot.DOFade(0,0.5f).From());
                }
				
				seq.Play();
                //this.VictoryBanner.SetActive(isVictorious);
                //this.DefeatBanner.SetActive(!isVictorious);
            }
		}


		public void Initialize(GameOverMessage msg, int winStreak, int packsOwned)
        {
            GameOverMessage = msg;
            
            if (!msg.IsJustRewards)
            {
                //MenuTheme.Play();
                Victory = msg.HasWon;
            }
            NextButtonText.text = msg.GrantedPrizes != null && msg.GrantedPrizes.Count > 0 ? I2.Loc.ScriptLocalization.CollectPrizes : I2.Loc.ScriptLocalization.Continue;

#if UNITY_IOS || UNITY_ANDROID
            this.ShareButton.gameObject.SetActive(true);
#else
            this.ShareButton.gameObject.SetActive(false);
#endif
            //this.ReportBugButton.gameObject.SetActive(false);
            this.GameLogButton.gameObject.SetActive(!msg.IsJustRewards);
            this.LeaderboardButton.gameObject.SetActive(!msg.IsJustRewards && msg.GameFormat != GameFormat.Tutorial);
            this.BottomRow.SetActive(this.LeaderboardButton.gameObject.activeSelf || this.ShareButton.gameObject.activeSelf || this.GameLogButton.gameObject.activeSelf);
            this.NextScrim.gameObject.SetActive(false);


            ShowMainMenuParams = new ShowMainMenuParams
            {
                IsStartup = false,
                EventWins = msg.WinCount,
                Format = msg.GameFormat,
                HasEndedEvent = msg.IsTournamentOver,
                HasLeveledUp = msg.CurrentMMR < msg.PrevMMR && msg.PrevMMR != 0,
                HasCompletedKeyQuest = msg.PreviousQuest != null && msg.PreviousQuest.ID == -5 && msg.PreviousQuest.CurrentScore >= msg.PreviousQuest.TargetScore,
                HasWon = msg.HasWon,
                Level = msg.CurrentMMR,
                WinStreak = winStreak,
                PacksToOpen = packsOwned,
            };

            IsShowPackOpener = (msg.GrantedCurrencies != null && msg.GrantedCurrencies.Any(c => c.Key == CurrencyType.SP || c.Key == CurrencyType.NG));
        }

		public void ClickNext() 
		{
            if (null != this.CloseDialogAction)
            {
                this.CloseDialogAction.Invoke();
            }
        }

        public void OnShareClick()
        {
            if(this.EventShareClicked != null)
            {
                this.EventShareClicked.Invoke(new ShareScreenShotParams(SharingPlugin, string.Empty, ShareButton, ShareButton.gameObject));
            }
        }

        public void OnReportBugClick()
        {
            if (this.EventReportBugClicked != null)
            {
                this.EventReportBugClicked.Invoke();
            }
        }

        public void OnLeaderboardClick()
        {
            if (this.EventLeaderboardClicked != null)
            {
                this.EventLeaderboardClicked.Invoke();
            }
        }

        public void OnGameLogClick()
        {
            if (this.EventGameLogClicked != null)
            {
                this.EventGameLogClicked.Invoke();
            }
        }

        public void OnPlayerCardAnimationComplete()
        {
            
            this.NextButtonImage.DOKill();
            this.NextButtonImage.gameObject.transform.DOScale(1.0f, 0.0f);
            this.NextButtonImage.DOFade(0.0f, 0.0f).OnComplete(() => {
                this.NextScrim.gameObject.SetActive(true);
                this.NextButtonImage.DOFade(1.0f, 0.3f);
                this.NextButtonImage.transform.DOPunchScale(Vector3.one * 0.15f, 2.0f, vibrato: 1, elasticity: 0.25f).SetLoops(-1);
            });
        }

        public void OnLocalize()
        {
            bool isBold = true;
            if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese")
                isBold = false;
            DefeatLabel.fontStyle = isBold ? FontStyles.Bold | FontStyles.SmallCaps : FontStyles.Normal | FontStyles.SmallCaps;
            VictoryLabel.fontStyle = isBold ? FontStyles.Bold | FontStyles.SmallCaps : FontStyles.Normal | FontStyles.SmallCaps;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            this.NextButtonImage.DOKill();
        }
    }
}
