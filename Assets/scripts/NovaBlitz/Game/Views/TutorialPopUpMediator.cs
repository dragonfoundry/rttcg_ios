﻿using UnityEngine;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using DragonFoundry.Controllers;
using DragonFoundry.Networking;
using DragonFoundry.GameState;
using Messages;
using System;

namespace NovaBlitz.Game
{
    public class TutorialPopUpMediator : Mediator
    {
        [Inject] public TutorialPopUpView View { get; set; }
        [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}
        [Inject] public ShowTutorialDialogSignal ShowTutorialDialogSignal { get; set; }
        [Inject] public SubmitGameActionSignal SubmitGameActionSignal {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get;set;}
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
		[Inject] public ViewReferences ViewReferences { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.DiffGameStateMessageSignal.AddListener(OnDiffGameStateMessage);
            this.ShowTutorialDialogSignal.AddListener(OnShowTutorialDialogSignal);
            this.View.DoneClicked += this.OnDoneClicked;
            this.View.MoveCanvas += this.MoveToCanvas;
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.SubmitGameActionSignal.AddListener(this.OnGameActionSubmitted);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.DiffGameStateMessageSignal.RemoveListener(OnDiffGameStateMessage);
            this.ShowTutorialDialogSignal.RemoveListener(OnShowTutorialDialogSignal);
            this.View.DoneClicked -= this.OnDoneClicked;
            this.View.MoveCanvas -= this.MoveToCanvas;
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.SubmitGameActionSignal.RemoveListener(this.OnGameActionSubmitted);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {

            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
               
            }
        }

        private void OnGameActionSubmitted(GameAction action)
        {
            this.View.SetButtonAndScrim();
        }

        private void OnDoneClicked()
        {
            if (this.View.GameState != null)
            {
                GameAction unpause = this.View.GameState.LegalAbilities.GetUnpauseAction(this.View.GameState.SelfId);
                if (unpause != null && unpause.IsLegal)
                {
                    this.SubmitGameActionSignal.RemoveListener(this.OnGameActionSubmitted);
                    this.SubmitGameActionSignal.Dispatch(unpause);
                    this.SubmitGameActionSignal.AddListener(this.OnGameActionSubmitted);
                }
            }
            this.CloseAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));
        }

        private void OnDiffGameStateMessage(DiffGameStateMessage diffMessage)
        {
            if(diffMessage.IsPaused)
            {
                if(string.IsNullOrEmpty(diffMessage.PauseHint) || diffMessage.PauseHint == "Mulligans" || diffMessage.PauseHint == "NoHint")
                {
                    // ignore this particular one
                }
                else
    				Timer.GameInstance.StartCoroutine (WaitToPopTutorialCoroutine(diffMessage));
            }
            if(this.View.IsOpen)
            {
                this.View.SetButtonAndScrim();
            }
        }

		private IEnumerator WaitToPopTutorialCoroutine(DiffGameStateMessage diffMessage)
		{
			var progress = (ProgressLoadingScreenView)this.ViewReferences.Get (typeof(ProgressLoadingScreenView));
			while (progress != null && progress.IsOpen) 
			{
				yield return null;
			}
            //this.CloseAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));

            if (!this.View.IsOpen)
            {
                ShowTutorialDialog(diffMessage.PauseHint, true);
            }

			Debug.LogFormat("PauseHint = {0}", diffMessage.PauseHint);
        }

        private void OnShowTutorialDialogSignal(string hint, bool isOnRight)
        {
			Debug.LogFormat("Hint = {0}", hint);
            //this.CloseAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));
            ShowTutorialDialog(hint, isOnRight);
        }

        private void ShowTutorialDialog(string hint, bool isOnRight)
        {
            if (this.View != null && this.View.IsOpen)
            {
                StartCoroutine(ShowTutorialDialogCoroutine(hint, isOnRight));
            }
            else
            {
                this.OpenAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));
                this.View.Initialize(hint, isOnRight);
            }
        }

        private IEnumerator ShowTutorialDialogCoroutine(string hint, bool isOnRight)
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));
            yield return new WaitForSecondsRealtime(1);
            this.OpenAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));
            this.View.Initialize(hint, isOnRight);
        }

        private void MoveToCanvas(bool isOnTop)
        {
            this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, isOnTop ? NovaCanvas.Overlay : NovaCanvas.Main, 1.0f);
        }

        //private IEnumerator
    }
}
