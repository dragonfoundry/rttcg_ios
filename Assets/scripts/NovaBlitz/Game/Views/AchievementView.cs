using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.Game;
using I2.Loc;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class AchievementView : PoolableMonoBehaviour
    {
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.AchievementView; } }
        [SerializeField] protected UpdatedPrizeView PrizeViewParent;

        [System.Serializable]
        public struct AchievementFramesClass
        {
            public Sprite Bronze;
            public Sprite Silver;
            public Sprite Gold;
            public Sprite Platinum;
            public Sprite Diamond;
            public Sprite Nova;
        }
        public AchievementFramesClass AchievementFrames = new AchievementFramesClass();
        public AchievementFramesClass AchievementGlows = new AchievementFramesClass();
        private Dictionary<int, Sprite> achievementFrames = new Dictionary<int, Sprite>();
        private Dictionary<int, Sprite> achievementGlows = new Dictionary<int, Sprite>();

        [SerializeField] public TextMeshProUGUI _NameDisplay;
        [SerializeField] public Image _FrameDisplay;
        public Image NewRankFrame;
        [SerializeField] protected Material GrayScaleMaterial;
        [SerializeField] protected Material UINoBackfaceMaterial;

        public Image RatingSliderStart;
        public Image RatingSlider;
        public TextMeshProUGUI RatingIncrease;
        public TextMeshProUGUI RatingText;
        private IntRange RatingBracket;
        public GameObject SliderArea;
        public TextMeshProUGUI AchievementUnlocked;
        public NovaButton CollectButton;

        protected AchievementData _Data;
		public AchievementData Data {
			get { return _Data; }
			set { _Data = value; RefreshData(); }
		}
        
        public int Level { get { return Data == null ? 0 : Data.newLevel; } }
        public int Rating { get { return Data == null ? 0 : Data.newScore; } }

        public CardArtImage AchievementArt;
        public Image ArtMask;
        
        protected void Awake()
        {
            achievementFrames[0] = AchievementFrames.Bronze;
            achievementFrames[1] = AchievementFrames.Bronze;
            achievementFrames[2] = AchievementFrames.Silver;
            achievementFrames[3] = AchievementFrames.Gold;
            achievementFrames[4] = AchievementFrames.Platinum;
            achievementFrames[5] = AchievementFrames.Diamond;
            achievementFrames[6] = AchievementFrames.Nova;

            achievementGlows[0] = AchievementGlows.Bronze;
            achievementGlows[1] = AchievementGlows.Bronze;
            achievementGlows[2] = AchievementGlows.Silver;
            achievementGlows[3] = AchievementGlows.Gold;
            achievementGlows[4] = AchievementGlows.Platinum;
            achievementGlows[5] = AchievementGlows.Diamond;
            achievementGlows[6] = AchievementGlows.Nova;
        }

        private void SetPivot(bool isDescending)
        {
        }

        public void InitializeAsQuest(string artId, string questName, string questUpText)
        {
            AchievementArt.UpdateCardArt(artId, CardAspect.NoAspect);
            ArtMask.material = UINoBackfaceMaterial;
            _FrameDisplay.material = UINoBackfaceMaterial;
            _FrameDisplay.sprite = AchievementFrames.Gold;
            _NameDisplay.text = questName;

            SliderArea.gameObject.SetActive(false);

            NewRankFrame.DOFade(0f, 0f);
            if (PrizeViewParent != null)
            {
                PrizeViewParent.CanvasGroup.interactable = false;
                PrizeViewParent.RarityGlowCanvasGroup.gameObject.SetActive(false);
                PrizeViewParent.RarityGlowCanvasGroup.transform.localPosition = new Vector3(0.0f, 40.0f, 0.0f);
                PrizeViewParent.RarityGlow1.sprite = AchievementGlows.Gold;
                PrizeViewParent.RarityGlow2.sprite = AchievementGlows.Gold;
            }

            this.RatingIncrease.alpha = 0;
            this.AchievementUnlocked.SetText(questUpText);
            this.AchievementUnlocked.gameObject.SetActive(true);
            this.AchievementUnlocked.color = IncreaseColor;
            this.AchievementUnlocked.transform.localScale = Vector3.one;
            this.AchievementUnlocked.transform.DOLocalMove(Vector3.zero, 0.0f);
            AchievementUnlocked.alpha = 0;

            if (seq != null)
            {
                seq.Kill();
            }
            seq = DOTween.Sequence();
            seq.Insert(0, DOTween.To(x => this.AchievementUnlocked.alpha = x, 0f, 1f, BONUS_HALFLIFE));
            seq.Insert(0, this.AchievementUnlocked.transform.DOBlendableLocalMoveBy(new Vector3(0, 150), BONUS_HALFLIFE * 4));
            seq.Insert(0, this.AchievementUnlocked.transform.DOScale(2.0f, BONUS_HALFLIFE * 4));
            seq.Insert(0 + BONUS_HALFLIFE * 3, DOTween.To(x => this.AchievementUnlocked.alpha = x, 1f, 0f, BONUS_HALFLIFE));

            // end the tweens; this is a one-and-done
            seq.AppendCallback(() => EndTweens());
        }

		public virtual void RefreshData()
		{
			if (null == this.Data)
				return;
            UpdateLabels();
            SetPivot(this.Data.IsDescending);
            
            if (Data.newLevel == 0)
            {
                AchievementArt.material = GrayScaleMaterial;
                ArtMask.material = null;
                _FrameDisplay.material = GrayScaleMaterial;
                _NameDisplay.gameObject.SetActive(false);
                SliderArea.gameObject.SetActive(false);
            }
            else
            {
                AchievementArt.material = null;
                ArtMask.material = UINoBackfaceMaterial;
                _FrameDisplay.material = UINoBackfaceMaterial;
                _NameDisplay.gameObject.SetActive(true);
                SliderArea.gameObject.SetActive(true);
            }

            AchievementArt.UpdateCardArt(this.Data.ArtId, CardAspect.NoAspect);

            if (seq != null)
            {
                seq.Kill();
            }


            if (Data.oldScore == 0)
                Data.oldScore = Data.newScore;

            this.AchievementUnlocked.gameObject.SetActive(false);
            this.RatingIncrease.gameObject.SetActive(false);
            // rank goes from 30 to -6, skipping 0
            SetFrame(Data.oldLevel, Data.oldLevel, null);

            this.isFirstRankUpComplete = false;

            if (CollectButton != null)
            {
                CollectButton.gameObject.SetActive(false);
            }
            if (Data.LevelBrackets != null)
            {
                PlayRatingSliderTweens(Data.oldScore, Data.newScore, Data.oldLevel, Data.newLevel);
            }
            else
            {
                SliderArea.gameObject.SetActive(false);
            }
        }
        
        public void UpdateLabels()
        {
            string name = I2.Loc.ScriptLocalization.Get("AchievementData/" + this.Data.Name);
            if (string.IsNullOrEmpty(name))
                name = this.Data.Name;
            _NameDisplay.text = name;
        }
                
        public float GetScale(int rating, IntRange bracket)
        {
            if (bracket.Min == -1)
                return 1;
            float scale = Data.IsDescending
                ? 0.05f + 0.95f * (Math.Max(0f, (bracket.Max - rating + 1)) / Math.Max(1.0f, (bracket.Max + 1 - bracket.Min)))
                : 0.05f + 0.95f * (Math.Max(0f, (rating - bracket.Min)) / Math.Max(1.0f, (bracket.Max + 1 - bracket.Min)));
            return scale;
        }
        private const string RATING_FORMAT = "{0}/{1}";
        private const string DESCENDING_FORMAT = "{0}({1})";

        public static Color IncreaseColor = new Color { r = 0.0f, g = 1.0f, b = 0.0f, a = 1 };

        private const float FADE_TIME = 0.2f;
        private const float INSERT_OFFSET = 0.25f;
        private const float FADE_HALFLIFE = 0.25f;
        private const float ACHIEVEMENT_HALFLIFE = 0.25f;
        private const float BONUS_HALFLIFE = 0.25f;
        private Sequence seq;
        public Sequence TweenSequence { get { return seq; } }
        private bool isFirstRankUpComplete = false;

        private string GetRatingText(IntRange? bracket, float rating)
        {
            return !bracket.HasValue || Data.IsDescending || Data.Name == "fastest_win"
                ? rating < 0 ? I2.Loc.ScriptLocalization.Nova : ((int)rating).ToString()
                : string.Format(RATING_FORMAT, (int)rating, Data.IsDescending ? bracket.Value.Min : bracket.Value.Max + 1);
        }

        public void PlayRatingSliderTweens(int oldRating, int newRating, int oldRank, int newRank)
        {
            oldRank = Math.Max(0, Math.Min(6, oldRank));
            newRank = Math.Max(0, Math.Min(6, newRank));
            if (PrizeViewParent != null)
            {
                //PrizeViewParent.CanvasGroup.interactable = false;
                //PrizeViewParent.RarityGlowCanvasGroup.gameObject.SetActive(false);
                //PrizeViewParent.RarityGlowCanvasGroup.DOFade(0.0f, 0.4f);
                PrizeViewParent.RarityGlowCanvasGroup.transform.localPosition = new Vector3(0.0f, 40.0f, 0.0f);
                PrizeViewParent.RarityGlow1.sprite = achievementGlows[oldRank];
                PrizeViewParent.RarityGlow2.sprite = achievementGlows[oldRank];
            }
            //Debug.LogFormat("{4} From {0} to {1}; from {2} to {3}", oldRating, newRating, oldRank, newRank, Data.Name);
            IntRange bracket;
            IntRange? bracketForText = null;
            if (!Data.LevelBrackets.TryGetValue(oldRank, out bracket))
            {
                if (oldRank >= Data.LevelBrackets.Count)
                {
                    bracket = new IntRange { Min = -1, Max = newRating };
                }
                else
                {
                    Debug.LogFormat("{0} bracket not found for {1}", Data.Name, oldRank);
                    EndTweens();
                    return;
                }
            }
            else
                bracketForText = bracket;
            //Debug.LogFormat("{0} bracket for {1}: {2}-{3}", Data.Name, oldRank, bracket.Min, bracket.Max);

            this.RatingSliderStart.transform.DOScaleX(GetScale(oldRating, bracket), 0f);
            this.RatingSlider.transform.DOScaleX(GetScale(oldRating, bracket), 0f);
            RatingText.text = GetRatingText(bracketForText, oldRating);
            
            // set everything to the start point

            if (oldRating == newRating)
            {
                EndTweens();
                return;
            }

            if(Data.Name.Contains("lifetime"))
            {

            }

            /*Timer.Instance.StartCoroutine(RatingSliderCoroutine(bracket, oldRating, newRating, oldRank, newRank));
        }

        private IEnumerator RatingSliderCoroutine(IntRange bracket, int oldRating, int newRating, int oldRank, int newRank)
        {
            yield return null;*/

            //Debug.LogFormat("{0} tweening bracket({2}-{3}) for {1}: {4} to {5}", Data.Name, oldRank, bracket.Min, bracket.Max, oldRating, newRating);
            int end = Data.IsDescending
                ? Math.Max(newRating, bracket.Min)
                : Math.Min(newRating, bracket.Max + 1);
            if (isFirstRankUpComplete && newRating != oldRating)
            {
                this.RatingIncrease.SetText("+{0}", Math.Abs(newRating - oldRating));
                this.RatingIncrease.gameObject.SetActive(true);
                this.RatingIncrease.color = IncreaseColor;
            }
            this.RatingIncrease.alpha = 0;
            this.RatingIncrease.transform.localScale = Vector3.one;
            this.RatingIncrease.transform.localPosition = Vector3.zero;
            Debug.LogFormat("{0}-{1} oldRating {2} end {3}", Data.Name, oldRank, oldRating, end);
            if (seq != null)
            {
                seq.Kill();
            }
            seq = DOTween.Sequence();
            float sliderTweenLength = ACHIEVEMENT_HALFLIFE * 3;// Math.Min(4, Math.Max(2, Math.Abs(end - oldRating) / 25f));
            if(Data.IsDescending)
                seq.Insert(0f, this.RatingSliderStart.transform.DOScaleX(GetScale(end, bracket), sliderTweenLength));
            else
                seq.Insert(0f, this.RatingSlider.transform.DOScaleX(GetScale(end, bracket), sliderTweenLength));
            seq.Insert(0f, DOTween.To(x =>
                { RatingText.text = GetRatingText(bracketForText, x); },
                oldRating, end, sliderTweenLength));
            seq.Insert(0f, DOTween.To(x => RatingIncrease.alpha = x, 0f, 1f, ACHIEVEMENT_HALFLIFE));
            seq.Insert(0f, this.RatingIncrease.transform.DOBlendableLocalMoveBy(new Vector3(0, 150), ACHIEVEMENT_HALFLIFE * 4));
            seq.Insert(0f, this.RatingIncrease.transform.DOScale(2.0f, ACHIEVEMENT_HALFLIFE * 3));
            seq.Insert(ACHIEVEMENT_HALFLIFE * 2, DOTween.To(x => RatingIncrease.alpha = x, 1f, 0f, ACHIEVEMENT_HALFLIFE * 2));
            seq.PrependInterval(1f); // gap before we start playing.

            seq.AppendCallback(() => PlayRankUpTweens(oldRating, newRating, oldRank));
        }

        public void EndTweens()
        {
            if (PrizeViewParent != null)
            {
                PrizeViewParent.CanvasGroup.interactable = true;
                PrizeViewParent.RarityGlowCanvasGroup.gameObject.SetActive(true);
                PrizeViewParent.RarityGlowCanvasGroup.DOFade(1.0f, 0.4f);
                PrizeViewParent.RarityGlowCanvasGroup.transform.DOScale(Vector3.one * 1.06f, 0.35f)
                    .OnComplete(() => PrizeViewParent.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.35f));
            }
            if(CollectButton != null)
            {
                CollectButton.gameObject.SetActive(true);
            }
        }

        public void PlayRankUpTweens(int oldRating, int newRating, int oldRank)
        {
            //int calcedOldRank = oldRank;// EngineUtils.FindBracket(oldRating, PlayerData.RankBrackets);
            if (Data.newLevel == oldRank)
            {
                EndTweens();
                return;
            }
            int newRank = Math.Min(6, oldRank + 1);
            
            IntRange bracket;
            if (!Data.LevelBrackets.TryGetValue(newRank, out bracket))
            {
                IntRange lastBracket;
                Data.LevelBrackets.TryGetValue(oldRank, out lastBracket);
                bracket = Data.IsDescending
                    ? new IntRange { Min = newRating, Max = lastBracket.Min - 1 }
                    : new IntRange { Min = lastBracket.Max + 1, Max = newRating };
            }

            if (seq != null)
            {
                seq.Kill();
            }
            seq = DOTween.Sequence();
            
            SetFrame(oldRank, newRank, seq);

            // Insert the "Rank Up/Nova Rank {X}/Rank Down" text
            string novaRankText;

            switch (newRank)
            {
                case 1:
                    novaRankText = "I";
                    break;
                case 2:
                    novaRankText = "II";
                    break;
                case 3:
                    novaRankText = "III";
                    break;
                case 4:
                    novaRankText = "IV";
                    break;
                case 5:
                    novaRankText = "V";
                    break;
                case 6:
                    novaRankText = "VI";
                    break;
                default:
                    novaRankText = string.Empty;
                    break;
            }
            this.AchievementUnlocked.SetText(string.Format("{0}: {1}", I2.Loc.ScriptLocalization.Achievement_Unlocked, novaRankText));
            this.AchievementUnlocked.gameObject.SetActive(true);
            this.AchievementUnlocked.color = IncreaseColor;
            this.AchievementUnlocked.transform.localScale = Vector3.one;
            this.AchievementUnlocked.transform.DOLocalMove(Vector3.zero, 0.0f);
            AchievementUnlocked.alpha = 0;
            seq.Insert(0, DOTween.To(x => this.AchievementUnlocked.alpha = x, 0f, 1f, BONUS_HALFLIFE));
            seq.Insert(0, this.AchievementUnlocked.transform.DOBlendableLocalMoveBy(new Vector3(0, 150), BONUS_HALFLIFE * 4));
            seq.Insert(0, this.AchievementUnlocked.transform.DOScale(2.0f, BONUS_HALFLIFE * 4));
            seq.Insert(0 + BONUS_HALFLIFE * 3, DOTween.To(x => this.AchievementUnlocked.alpha = x, 1f, 0f, BONUS_HALFLIFE));

            // check for more rank ups
            //if (newRating > oldRating)
            seq.AppendCallback(() => PlayRatingSliderTweens(Data.IsDescending ? bracket.Max : bracket.Min, newRating, newRank, Data.newLevel));
        }
        
        public void SetFrame(int oldFrame, int newFrame, Sequence seq)
        {
            Sprite frameSprite;
            if (!achievementFrames.TryGetValue(oldFrame, out frameSprite))
            {
                frameSprite = achievementFrames[0];
            }
            //if(seq != null || oldFrame == newFrame)
            _FrameDisplay.sprite = frameSprite;


            // set the regular frame to on, new frame to invisible
            _FrameDisplay.DOFade(1f, 0f).OnComplete(() =>
            {
                _FrameDisplay.gameObject.SetActive(true);
                NewRankFrame.gameObject.SetActive(false);
            });
            NewRankFrame.DOFade(0f, 0f);

            if (newFrame != oldFrame && seq != null) // NOTE - sequence is played elsewhere
            {
                // animate the frame change
                Sprite newframeSprite;
                if (!achievementFrames.TryGetValue(newFrame, out newframeSprite))
                {
                    newframeSprite = achievementFrames[0];
                }
                
                NewRankFrame.transform.DOScale(2f, 0f).OnComplete(() => NewRankFrame.sprite = newframeSprite);
                // zoom the new frame in; fade out the old frame
                seq.Insert(INSERT_OFFSET + FADE_HALFLIFE, _FrameDisplay.DOFade(0f, FADE_HALFLIFE).SetEase(Ease.InOutCubic).OnComplete(() =>_FrameDisplay.sprite = newframeSprite));
                seq.Insert(INSERT_OFFSET + FADE_HALFLIFE, NewRankFrame.transform.DOScale(1f, FADE_HALFLIFE * 2).SetEase(Ease.OutBounce));
                seq.Insert(INSERT_OFFSET, NewRankFrame.DOFade(1f, FADE_HALFLIFE * 2.0f).SetEase(Ease.InOutCubic));
                seq.InsertCallback(INSERT_OFFSET, () => NewRankFrame.gameObject.SetActive(true));
            }
        }

        public void OnLocalize()
        {
            RefreshData();
        }

        public override void OnRecycle()
        {
            Data = null;
            AchievementArt.ResetArt();
        }

        protected void OnDestroy()
        {
            Data = null;
            AchievementArt.ResetArt();
        }
    }

    [System.Serializable]
	public class MultiAchievementView : MultiRect<AchievementView, AchievementData>
	{
		protected override void Initialize(AchievementView instance, AchievementData data)
		{
			instance.Data = data;
		}
	}
}
