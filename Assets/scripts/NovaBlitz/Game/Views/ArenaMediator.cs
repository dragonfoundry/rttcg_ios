﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.UI;
using DragonFoundry.GameState;
using DragonFoundry.Fx;
using System;
using DG.Tweening;
using Messages;
using strange.extensions.context.impl;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using LinqTools;

namespace NovaBlitz.Game
{
    public class ArenaCardRecycledSignal : Signal<int, bool,Phase> {} // cardID, isPlayersCard, phase card was recycled in
    public class CardBackRecycledSignal : Signal<int> {} //CardID
    public class ArenaMediator : Mediator
    {
        [Inject] public ArenaView View {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get;set;}
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public GameStateInitializedSignal GameStateInitializedSignal {get;set;}
		[Inject] public GameEventMessageSignal GameEventMessageSignal {get;set;}
        [Inject] public INovaContext NovaContext {get;set;}
        [Inject] public CardEventService CardEventService {get;set;}
        [Inject] public GameControllerService GameControllerService {get;set;}
        [Inject] public LegalAbilitiesUpdatedSignal LegalAbilitiesUpdatedSignal {get;set;}
        [Inject] public ArenaCards ArenaCards {get;set;}
        [Inject] public ArenaPlayers ArenaPlayers {get;set;}
        [Inject] public TrashPlayerCardSignal TrashPlayerCardSignal {get;set;}
        [Inject] public TrashOpponentCardSignal TrashOpponentCardSignal {get;set;}
        [Inject] public DiscardPlayerCardSignal DiscardPlayerCardSignal {get;set;}
        [Inject] public DiscardOpponentCardSignal DiscardOpponentCardSignal {get;set;}
        [Inject] public PlayerCardAttacksSignal PlayerCardAttacksSignal {get;set;}
        [Inject] public OpponentCardAttacksSignal OpponentCardAttacksSignal {get;set;} 
        [Inject] public ArenaCardRecycledSignal ArenaCardRecycledSignal {get;set;}
        [Inject] public CardStateUpdatedSignal CardStateUpdatedSignal { get;set;}
        [Inject] public LoseControlOfCardSignal LoseControlOfCardSignal { get;set;}
        [Inject] public GainControlOfCardSignal GainControlOfCardSignal { get;set;}
	    [Inject] public CombatStateMessageSignal CombatStateMessageSignal { get;set;}
        [Inject] public PhaseChangedSignal PhaseChangedSignal {get;set;}
        [Inject] public CardDamageTakenSignal CardDamageTakenSignal {get;set;}
        [Inject] public CardHealedSignal CardHealedSignal {get;set;}
        [Inject] public PlayerHealedSignal PlayerHealedSignal { get; set; }
        [Inject] public OpponentHealedSignal OpponentHealedSignal { get; set; }
        [Inject] public CardBackRecycledSignal CardBackRecycledSignal {get;set;}
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
		[Inject] public ResizeArenaSignal ResizeArenaSignal { get; set;}
        [Inject] public ModifyOrSetStatEventSignal ModifyOrSetStatEventSignal {get;set;}
        [Inject] public GameData GameData {get;set;}
        [Inject] public GameOverSignal GameOverSignal { get; set; }
        [Inject] public PlayFxSignal PlayFxSignal { get; set; }
        [Inject] public LogEventSignal LogEventSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        
        private InputEventController InputEventController { get; set; }

        protected Camera uiCamera;

        private GameStateReadOnly GameState { get; set; }
        
        private Dictionary<int, Dictionary<int,TargetArrowInfo>> blockTargetArrows = new Dictionary<int, Dictionary<int,TargetArrowInfo>>(); 


		override public void OnRegister()
		{
            this.GameStateInitializedSignal.AddListener(this.OnGameStateInitialized);
			this.GameEventMessageSignal.AddListener(this.OnGameEventMessage);
            this.CardStateUpdatedSignal.AddListener(this.OnCardStateUpdated);
            this.TrashPlayerCardSignal.AddListener(this.OnTrashPlayerCard);
            this.TrashOpponentCardSignal.AddListener(this.OnTrashOpponentCard);
            this.LegalAbilitiesUpdatedSignal.AddListener(this.OnLegalAbilitiesUpdated);
            this.PlayerCardAttacksSignal.AddListener(this.OnPlayerCardAttacks);
            this.OpponentCardAttacksSignal.AddListener(this.OnOpponentCardAttacks);
            this.LoseControlOfCardSignal.AddListener(this.OnLoseControlOfCard);
            this.GainControlOfCardSignal.AddListener(this.OnGainControlOfCard);
            this.CombatStateMessageSignal.AddListener(this.OnCombatStateUpdated);
            this.PhaseChangedSignal.AddListener(this.OnPhaseChanged);
            this.CardDamageTakenSignal.AddListener(this.OnCardDamageTaken);
            this.CardHealedSignal.AddListener(this.OnCardHealed);
            this.PlayerHealedSignal.AddListener(this.OnSelfHealed);
            this.OpponentHealedSignal.AddListener(this.OnOpponentHealed);
            this.ModifyOrSetStatEventSignal.AddListener(this.OnModifyOrSetStat);
            this.GameOverSignal.AddListener(this.OnGameOver);

            this.View.ScreenResize += OnScreenResized;
            this.View.TrashClicked += OnTrashClicked;
            this.View.TrashHide += OnHideTrashCards;
            this.View.PlayerHandController.ShowTargets += OnShowTargets;
            this.View.PlayerArenaController.ShowTargets += OnShowTargets;
            this.View.PlayerHandController.HideTargets += OnHideTargets;
            this.View.PlayerArenaController.HideTargets += OnHideTargets;


            this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
        }

        override public void OnRemove()
        {
            this.GameStateInitializedSignal.RemoveListener(this.OnGameStateInitialized);
			this.GameEventMessageSignal.RemoveListener(this.OnGameEventMessage);
            this.CardStateUpdatedSignal.RemoveListener(this.OnCardStateUpdated);
            this.TrashPlayerCardSignal.RemoveListener(this.OnTrashPlayerCard);
            this.TrashOpponentCardSignal.RemoveListener(this.OnTrashOpponentCard);
            this.LegalAbilitiesUpdatedSignal.RemoveListener(this.OnLegalAbilitiesUpdated);
            this.PlayerCardAttacksSignal.RemoveListener(this.OnPlayerCardAttacks);
            this.OpponentCardAttacksSignal.RemoveListener(this.OnOpponentCardAttacks);
            this.LoseControlOfCardSignal.RemoveListener(this.OnLoseControlOfCard);
            this.GainControlOfCardSignal.RemoveListener(this.OnGainControlOfCard);
            this.CombatStateMessageSignal.RemoveListener(this.OnCombatStateUpdated);
            this.PhaseChangedSignal.RemoveListener(this.OnPhaseChanged);
            this.CardDamageTakenSignal.RemoveListener(this.OnCardDamageTaken);
			this.CardHealedSignal.RemoveListener(this.OnCardHealed);
            this.PlayerHealedSignal.RemoveListener(this.OnSelfHealed);
            this.OpponentHealedSignal.RemoveListener(this.OnOpponentHealed);
            this.ModifyOrSetStatEventSignal.RemoveListener(this.OnModifyOrSetStat);
            this.GameOverSignal.RemoveListener(this.OnGameOver);

            this.View.ScreenResize -= OnScreenResized;
            this.View.TrashClicked -= OnTrashClicked;
            this.View.TrashHide -= OnHideTrashCards;
            this.View.PlayerHandController.ShowTargets -= OnShowTargets;
            this.View.PlayerArenaController.ShowTargets -= OnShowTargets;
            this.View.PlayerHandController.HideTargets -= OnHideTargets;
            this.View.PlayerArenaController.HideTargets -= OnHideTargets;
        }

        /*
         * AZC needs to know "showing trash" or "showing targets" or "nothing"
         * Only listen to trash events if showing trash, and target changes if showing targets
         * Listens to "added to trash
         * Probably need to send a 
         * Ensure max 5 shown
         * Handle cards being recycled well
         */
        
        private void OnTrashClicked(bool isForPlayer)
        {
            View.isTrueShowingTrashFalseShowingTargets = true;
			View.isShowingTrashForPlayer = isForPlayer == true;
			View.isShowingTrashForOpponent = isForPlayer != true;
            ShowTrashCards(isForPlayer);
        }

        private void OnHideTrashCards()
        {
            if(View.isTrueShowingTrashFalseShowingTargets == true)
            {
                this.RecycleAllButListedTrashCards(null);
                View.isTrueShowingTrashFalseShowingTargets = null;
            }
        }

        private void OnShowTargets(IEnumerable<ICardState> cardStates, bool isTargeted)
        {
            if (_HideTargetCoroutine != null)
            {
                Timer.GameInstance.StopCoroutine(_HideTargetCoroutine);
                _HideTargetCoroutine = null;
            }
            View.isTrueShowingTrashFalseShowingTargets = false;
            this.ShowCardsFromTrash(cardStates, isTargeted);
        }
        private IEnumerator _HideTargetCoroutine;
        private void OnHideTargets()
        {
            if (View.isTrueShowingTrashFalseShowingTargets == false)
            {
                if(_HideTargetCoroutine != null)
                {
                    Timer.GameInstance.StopCoroutine(_HideTargetCoroutine);
                }
                _HideTargetCoroutine = HideTargetCoroutine();
                Timer.GameInstance.StartCoroutine(_HideTargetCoroutine);
                View.isTrueShowingTrashFalseShowingTargets = null;
            }
        }
        private IEnumerator HideTargetCoroutine()
        {
            yield return new WaitForSecondsRealtime(1.5f);
            this.RecycleAllButListedTrashCards(null);
        }

        private void ShowTrashCards(bool isForPlayer)
        {
            if (_HideTargetCoroutine != null)
            {
                Timer.GameInstance.StopCoroutine(_HideTargetCoroutine);
                _HideTargetCoroutine = null;
            }
            Debug.LogFormat("Getting card from player {0} trash", isForPlayer ? "player" : "opponent");
            var trashCards = ((GameStateContainer)this.GameState).TrashForPlayer(isForPlayer ? this.ArenaPlayers.Player.PlayerId : this.ArenaPlayers.Opponent.PlayerId);
            trashCards = trashCards.Skip(Math.Max(0, trashCards.Count() - 5));

            this.ShowCardsFromTrash(trashCards, null);
        }

        /*private void OnTargetListChanged(IEnumerable<ICardState> cardStates)
        {
            // wait 1s
            // add card to shown cards
            if (View.isTrueShowingTrashFalseShowingTargets == false)
				this.ShowCardsFromTrash(cardStates);
        }*/
        
        /// <summary>
        /// Provide the cardState and the parent transform for the trash card to appear from.
        /// Keeps track of and supports showing of multiple trash cards at once. All visible cards
        /// will be dismissed by calling RecycleTrashCards().
        /// </summary>
        public void ShowCardsFromTrash(IEnumerable<ICardState> cardStates, bool? isTargeted)
        {
			/*if (cardStates != null)
				Debug.LogFormat ("Showing {0} cards from trash. Ids: [{1}]", cardStates.Count(), string.Join(",", cardStates.Select(c => string.Format("{0}:{1}",c.CardId,c.GetName())).ToArray()));
			else
				Debug.LogFormat("Showing 0 cards from trash: null cardStates");*/
            // this enforces max 5 per player.
            var playerCards = cardStates.Where(c => c.ControllerId == this.ArenaPlayers.Player.PlayerId);
            var opponentCards = cardStates.Where(c => c.ControllerId == this.ArenaPlayers.Opponent.PlayerId);
            
            cardStates = playerCards.Take(5).Union(opponentCards.Take(5));

            // this will recycle everything if we call OnShowCardsFromTrash with cardStates null
            RecycleAllButListedTrashCards(cardStates);
            if (cardStates == null)
                return;

            //
            //CardFrontController[] playerCards = trashAnchor.GetComponentsInChildren<CardFrontController>();
            //CardFrontController[] oppCards = trashAnchor.GetComponentsInChildren<CardFrontController>();


            // get list of current cards for both players.
            // remove (recycle) cards that don't exist in this list
            // Add cards that do exist
            // re-layout cards based on childNumber
            foreach (var cardState in cardStates)
            {
                bool isForPlayer;
                if (cardState.ControllerId == this.ArenaPlayers.Player.PlayerId)
                {
                    isForPlayer = true;
                }
                else if (cardState.ControllerId == this.ArenaPlayers.Opponent.PlayerId)
                {
                    isForPlayer = false;
                }
                else
                {
                    Debug.LogFormat("CardState Controller({0}) is not Self({1}) or Opp({2})", cardState.ControllerId, this.ArenaPlayers.Player.PlayerId, this.ArenaPlayers.Opponent.PlayerId);
                    continue;
                }
				if (cardState.Zone == CardZone.Arena || cardState.Zone == CardZone.Stack || cardState.Zone == CardZone.Hand)
					continue; // Ignore this card because it should be elsewhere already.

                RectTransform trashAnchor = isForPlayer ? this.View.PlayerTrashCardHolder : this.View.OpponentTrashCardHolder;

                // Attempt to retrieve a version of the card from offscreen?
                CardController trashCard = this.ArenaCards.GetCard(cardState.Id);
                bool isNeedToAnimate = trashCard == null || trashCard.transform.parent != trashAnchor;
                
                // If no existing card, spawn a new one 
                if (trashCard == null)
                {
                    trashCard = this.ArenaCards.CreateCard(cardState);
                    this.ArenaCards.AddCard(trashCard);
                    Debug.LogFormat("added trash card {0}", trashCard.CardIdAndName);
                }

                // sets the card's tag to arenacard so it can be targeted, if we're showing targets. Otherwise, tagged to null
                trashCard.RaycastTag = View.isTrueShowingTrashFalseShowingTargets == false ? "arenaCard" : "Untagged";

                Debug.LogFormat("Showing Trash card {0}. Need to animate: {1}", trashCard.CardIdAndName, isNeedToAnimate);
                if (isNeedToAnimate)
                {
                    // Make the card transparent
                    trashCard.CanvasGroup.alpha = 0f;
                    trashCard.CanvasGroup.blocksRaycasts = true;

                    // Position it at the players trash
                    trashCard.transform.SetParent(trashAnchor);
                    trashCard.transform.localScale = Vector3.one * 0.8f;
                    trashCard.transform.localPosition = Vector3.zero;
                    trashCard.CardGlowAnimator.SetBool("IsPowerCardFloating", true);

                    // Tween it up from the trash icon to the right side of the screen
                    Sequence seq = DOTween.Sequence();
                    seq.Append(trashCard.transform.DOScale(Vector3.one * 1.3f, 0.2f));
                    seq.Join(trashCard.CanvasGroup.DOFade(1.0f, 0.1f));
                    //seq.Join(((RectTransform)trashCard.transform).DOAnchorPos(position, 0.2f));
                    //seq.Join(trashCard.transform.DORotate(new Vector3(0,65f,0), 0.3f).SetEase(Ease.OutCubic));
                    //seq.Join(trashCard.transform.DORotate(rotation, 0.3f).SetEase(Ease.OutCubic));
                }

                if(isTargeted.HasValue)
                {
                    View.PlayerHandController.SetTargetGlow(trashCard.TargetArrowTarget, isTargeted.Value, trashCard.CanvasGroup);
                }
            }
            RelayoutTrashCards(this.View.PlayerTrashCardHolder);
            RelayoutTrashCards(this.View.OpponentTrashCardHolder);
        }

        /// <summary>
        /// Used to set the layout of all the trash cards once they've been spawned, or reset layouts after a change.
        /// Call this once for each player's trash
        /// </summary>
        /// <param name="cards"></param>
        public void RelayoutTrashCards(RectTransform trashAnchor)
        {
            // Loop though all the hovering trash cards
            foreach (RectTransform card in trashAnchor)
            {
                var index = card.GetSiblingIndex();

                Vector2 position;
                Vector2 rotation;
                if (!trashPositions.TryGetValue(index, out position) || !trashRotations.TryGetValue(index, out rotation))
                {
                    Debug.LogErrorFormat("Trash slot not valid. Slot {0}", index);
                    return;
                }
                card.DOAnchorPos(position, 0.2f);
                card.DORotate(rotation, 0.2f).SetEase(Ease.OutCubic);
            }
                
        }

        private const float distanceBetweenCards = 150f;
        private Dictionary<int, Vector2> trashPositions = new Dictionary<int, Vector2>
        {
            { 0, new Vector2(-0* distanceBetweenCards, 0) },
            { 1, new Vector2(-1* distanceBetweenCards, 0) },
            { 2, new Vector2(-2* distanceBetweenCards, 0) },
            { 3, new Vector2(-3* distanceBetweenCards, 0) },
            { 4, new Vector2(-4* distanceBetweenCards, 0) },
        };
            private Dictionary<int, Vector2> trashRotations = new Dictionary<int, Vector2>
        {
            { 0, new Vector3(0, 65f, 0) },
            { 1, new Vector3(0, 57f, 0) },
            { 2, new Vector3(0, 50f, 0) },
            { 3, new Vector3(0, 44f, 0) },
            { 4, new Vector3(0, 39f, 0) },
        };

        public void RecycleAllButListedTrashCards(IEnumerable<ICardState> cardStates)
        {
            RecycleTrashCards(cardStates, this.View.PlayerTrashCardHolder);
            RecycleTrashCards(cardStates, this.View.OpponentTrashCardHolder);
            
        }

        /// <summary>
        /// Tweens away and recycles all currently hovering trash cards.
        /// </summary>
        public void RecycleTrashCards(IEnumerable<ICardState> cardStates, RectTransform trashAnchor)
        {
            List<CardFrontController> ToRemove = new List<CardFrontController>();
            // Loop though all the hovering trash cards
            foreach (Transform card in trashAnchor)
            {
                // Get a reference to their cardfront
                CardFrontController cardFront = card.GetComponent<CardFrontController>();
                if (cardFront == null)
                {
                    Debug.Log("Card Front null on recycle trash");
                    continue;
                }
                else if (cardStates != null && cardStates.Any(c => c.Id == cardFront.CardId))
                {
                    Debug.LogFormat("not recycling {0}", cardFront.CardIdAndName);
                    continue;
                }
                Debug.LogFormat("recycling {0}", cardFront.CardIdAndName);

                cardFront.DOKill();
                ToRemove.Add(cardFront);
                // Undo the animation state before recycling...
                cardFront.CardGlowAnimator.SetBool("IsPowerCardFloating", false);

                ArenaCards.RemoveCard(cardFront);
                // Tween the card down to the trashAnchor and recycle it
                Sequence seq = DOTween.Sequence();
                seq.Join(((RectTransform)cardFront.transform).DOAnchorPos(new Vector2(0f, 0f), 0.2f));
                seq.Join(cardFront.CanvasGroup.DOFade(0f, 0.3f));
                seq.Join(cardFront.transform.DORotate(Vector3.zero, 0.3f).SetEase(Ease.InCubic));
                seq.Join(cardFront.transform.DOScale(0.25f, 0.25f));
                seq.AppendCallback(() => {
                    cardFront.Recycle();
                });
            }
            foreach(var cardFront in ToRemove)
            {
                cardFront.transform.SetParent(cardFront.ControllerID == this.ArenaPlayers.Player.PlayerId
                    ? this.View.PlayerTrashIcon.transform
                    : this.View.OpponentTrashIcon.transform,
                    true);
            }
            LayoutRebuilder.ForceRebuildLayoutImmediate(trashAnchor);
        }

        private void OnGameOver(GameOverMessage msg)
        {
            this.View.ArenaCanvasGroup.blocksRaycasts = false;
            this.View.ArenaZoomController.InputEventController.gameObject.SetActive(false);
			Sequence seq = DOTween.Sequence ();
			seq.AppendInterval (4.0f);
			seq.Append(this.View.EndFadeScrim.DOFade(0.6f, 1.0f));
        }

        private Color32 Blue = new Color32(0, 136, 255, 255);
        private Color32 Red = new Color32(255, 136, 136, 255);
        private Color32 Green = new Color32(0, 255, 0, 255);
        private string ShowNumberPlus = "{0} +{1}";
        private string ShowNumberMinus = "{0} -{1}";
        private string PlusKeyword = "+{0}";
        private string MinusKeyword = "-{0}";

        private void OnModifyOrSetStat(int statId, float amount, ICardState affectedCardState)
        {
            if(affectedCardState.Zone == CardZone.Arena)
            {
                CardFrontController arenaCard = (CardFrontController)this.ArenaCards.GetCard(affectedCardState.Id);
                if(arenaCard != null)
                {
                    KeywordData keyword = null;
                    if(GameData.KeywordsById.TryGetValue(statId, out keyword))
                    {
                        int currentValue;
                        affectedCardState.Keywords.TryGetValue(statId, out currentValue);
                        bool isPositiveColor;
                        bool isPositiveText;
                        bool showNumber = keyword.ShowNumber;
                        switch (keyword.Type)
                        {
                            case KeywordType.Condition:
                                isPositiveColor = currentValue >= 0 && amount < 0;
                                isPositiveText = !isPositiveColor;
                                showNumber = keyword.ShowNumber && (currentValue > 0 || (amount < 0 && currentValue == 0));
                                break;
                            case KeywordType.Skill:
                                isPositiveText = isPositiveColor = amount > 0;
                                break;
                            case KeywordType.Notifier:
                                isPositiveText = isPositiveColor = amount > 0;
                                break;
                            default:
                                return;
                        }
                        string text = string.Format(
                            isPositiveText ? (showNumber ? ShowNumberPlus : PlusKeyword) : (showNumber ? ShowNumberMinus : MinusKeyword),
                            I2.Loc.ScriptLocalization.Get(GameData.KEYWORD_DATA + keyword.Name), 
                            Math.Abs((int)amount));
                        this.StartCoroutine(this.WaitForCardToArriveInArena(
                            arenaCard, 
                            text, 
                            isPositiveColor ? (keyword.Type == KeywordType.Notifier ? Green : Blue) : Red));
                    }
                }
            }
        }

        private IEnumerator WaitForCardToArriveInArena(CardFrontController arenaCard, string text, Color color)
        {
            int count = 0;
            while(arenaCard.gameObject.GetComponentInParent<PlayerArenaController>() == null && arenaCard.gameObject.GetComponentInParent<OpponentArenaController>() == null)
            {
                yield return new WaitForSecondsRealtime(0.2f);
                count++;
                if(count > 10) { break; }
            }

            if(count < 10) 
            {
                arenaCard.EnqueueScrollingText(text,color);
            }
        }

        private void OnScreenResized()
		{
			this.ResizeArenaSignal.Dispatch ();
		}

        private void OnSelfHealed(int amount)
        {
            if (amount > 0)
                PlayHealFx(this.GameState.SelfId, ArenaPlayers.Player.AvatarFrame, amount);
        }

        private void OnOpponentHealed(int amount)
        {
            if (amount > 0)
                PlayHealFx(this.GameState.Opponent.Id, ArenaPlayers.Opponent.AvatarFrame, amount);
        }


        private void OnCardHealed(ICardState cardState, int healAmount)
        {
             Debug.LogFormat("ArenaCard Healed - id:{0} {1} for {2}", cardState.Id, cardState.GetName(), healAmount);
            // Only update cards currently in the arena
            CardController card = this.ArenaCards.GetCard(cardState.Id);
            if(card != null && healAmount > 0)
            {
                if(typeof(CardFrontController).IsAssignableFrom(card.GetType()))
                {
                    CardFrontController cardFront = (CardFrontController)card;
                    var actualHeal = Math.Min(cardFront.CurrentDamage, healAmount);
                    cardFront.CurrentDamage -= actualHeal;
                    cardFront.CurrentDamage = Math.Max(0, cardFront.CurrentDamage);
                    cardFront.UpdateLabels();
                    PlayHealFx(cardState.Id, cardFront.Card.rectTransform, actualHeal);

                    this.StartCoroutine(this.WaitForCardToArriveInArena(
                        cardFront,
                        string.Format(ShowNumberPlus, I2.Loc.ScriptLocalization.Heal, actualHeal),
                        Green));
                }
            }
        }
        
        private void PlayHealFx(int playerId, RectTransform target, int healAmount)
        {
            string fxScript;
            if (healAmount <= 0)
            {
                fxScript = string.Empty;
            }
            else if (healAmount <= 4)
                fxScript= "Fx_Sparkle_heal_s";
            else if (healAmount <= 10)
                fxScript = "Fx_Sparkle_heal_m";
            else
                fxScript = "Fx_Sparkle_heal_l";

            if (healAmount > 0)
                this.PlayFxSignal.Dispatch(new PlayFxParams(playerId, playerId, null, target, null, fxScript, PlayFxPath.NoPath,0));
        }
      
        private void OnCardDamageTaken(ICardState cardState, int damage)
        {
            //Debug.LogWarning("ArenaCard took damage - " + cardState.GetName());
            // Only update cards currently in the arena
            CardController card = this.ArenaCards.GetCard(cardState.Id);
            if(card != null)
            {
                if(typeof(CardFrontController).IsAssignableFrom(card.GetType()))
                {
                    CardFrontController cardFront = (CardFrontController)card;
                    
                    cardFront.ShowDamageBurst(damage);
                   
                    cardFront.CurrentDamage += damage;
                    
                    cardFront.UpdateLabels();
                }
            }
        }
        
        private void OnPhaseChanged(Phase phase, int turnNumber, GameStateReadOnly gameState)
        {
            this.View.TargetArrowController.ReleaseDraggedArrow();
            this.View.TargetArrowController.ClearTargets();
            OnHideTargets();
            OnHideTrashCards();
           
            switch(phase)
            {
                case Phase.PreCombat:
                    this.View.FadeToColor(this.View.CombatPhaseColor);
                    this.View.ShowCombatPhase();
                    this.ArenaCards.ClearCardActionTrackers();
                    break;
                case Phase.PreAction:
                    this.View.FadeToColor(this.View.ActionPhaseColor);
                    this.View.ShowTurnPhase(turnNumber);
                    this.ArenaCards.ClearCardActionTrackers();
                    LogTutorialTurns(turnNumber, false);
                    break;
                case Phase.EndOfTurn:
                    if (this.ArenaCards.NumPendingAbilities > 0)
                        Debug.LogFormat("Turn End: numPendingCardAbilities before clear:{0}", this.ArenaCards.NumPendingAbilities);
                    this.ArenaCards.ClearPendingCardAbilities();
                    break;
                case Phase.GameOver:
                    this.ArenaCards.ClearPendingCardAbilities();
                    // Remove all the blockig arrows
                    foreach (var dict in this.blockTargetArrows.Values)
                    {
                        foreach (TargetArrowInfo arrow in dict.Values)
                        {
                            this.View.TargetArrowController.ReleaseTargetArrowInfo(arrow);
                        }
                        dict.Clear();
                    }
                    Timer.GameInstance.StartCoroutine(WaitAndHideTutorialPopupCoroutine());
                    LogTutorialTurns(turnNumber, true);
                    break;
            }
        }

        private void LogTutorialTurns(int turnNumber, bool isGameOver)
        {
            try
            {
                if (this.GameData.CurrentGameData != null && this.GameData.CurrentGameData.GameFormat == GameFormat.Tutorial)
                {
                    this.LogEventSignal.Dispatch(new LogEventParams
                    {
                        EventName = "TutorialTurns",
                        EventDetails = new Dictionary<string, string> {
                                { "Turn", (this.PlayerProfile.OnboardingProgress.CurrentTutorial * 100 + turnNumber).ToString() },
                                { "Health", this.GameState.Self == null ? "0" : this.GameState.Self.Health.ToString() },
                                { "OppHealth", this.GameState.Opponent == null ? "0" : this.GameState.Opponent.Health.ToString() },
                                { "IsGameOver", isGameOver.ToString() }
                            }
                    });
                }
            }
            catch (Exception e)
            {
                MetricsTrackingService.SavedException = string.Format("[CAUGHT] InitializeView - exception: {0}", e.ToString());
                Debug.LogFormat("Logtutorialturns: Self:{0} Opp:{1} LES:{2}",
                    GameState.Self == null ? "null" : "ok", 
                    GameState.Opponent == null ? "null" : "ok", 
                    LogEventSignal == null ? "null" : "ok");
            }
        }

        private IEnumerator WaitAndHideTutorialPopupCoroutine()
        {
            yield return null;
            this.CloseAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));
        }

        private void OnOpponentCardAttacks(ICardState attackingCardState, int targetId, GameStateReadOnly gameState)
        {
            CardController attackingCard = this.ArenaCards.GetCard(attackingCardState.Id);
            Timer.GameInstance.StartCoroutine(this.TweenCardToPositionAndBack(attackingCard,targetId, gameState.Turn.Phase));
        }

        private void OnPlayerCardAttacks(ICardState attackingCardState, int targetId, GameStateReadOnly gameState)
        {
            CardController attackingCard = this.ArenaCards.GetCard(attackingCardState.Id);
            Timer.GameInstance.StartCoroutine(this.TweenCardToPositionAndBack(attackingCard,targetId, gameState.Turn.Phase));

        }
        
        private IEnumerator TweenCardToPositionAndBack(CardController attackingCard, int targetId, Phase phaseAttacked)
        {

            CardFrontController defendingCard = (CardFrontController)this.ArenaCards.GetCard(targetId);

            int i = 0;
            while (attackingCard.RaycastTag != "arenaCard" || (defendingCard != null && defendingCard.RaycastTag != "arenaCard"))
            {
                i++;
                if(attackingCard.RaycastTag == "inCombat" || (defendingCard != null && defendingCard.RaycastTag == "inCombat"))
                    Debug.LogFormat("In combat: {0}. i={1}", attackingCard.RaycastTag == "inCombat" ? "attacker" : "defender", i);
                if (i >= 100)
                    break;

                yield return null;
            }


            // Destroy any zoomed cards involved in the attack animation
            if (this.View.ArenaZoomController.ZoomedCard != null
            && (this.View.ArenaZoomController.ZoomedCard.CardId == attackingCard.CardId
            || this.View.ArenaZoomController.ZoomedCard.CardId == targetId))
            {
                // Only destroy the zoomed card on IOS / Android
#if !UNITY_IOS || UNITY_ANDROID
                this.View.ArenaZoomController.RecycleZoomedCard();
#endif
            }

            if (attackingCard is CardFrontController)
                (attackingCard as CardFrontController).AttackAudio.Play();

            Vector3 defenderWorldPos = this.GetDefenderPosition(attackingCard, defendingCard, targetId, phaseAttacked);
            string attackerTag = attackingCard.RaycastTag;
            //Transform attackerParent = attackingCard.transform.parent;
            //int attackerSiblingIndex = attackingCard.transform.GetSiblingIndex();
            Vector3 attackerStartPos = attackingCard.transform.position;
            // make the start position match the parent


            Vector3 attackerStartPosCombatUICanvas = Vector3.zero;
            Vector2 screenPoint2 = RectTransformUtility.WorldToScreenPoint(this.uiCamera, attackerStartPos);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(this.View.CombatUICanvas, screenPoint2, this.uiCamera, out attackerStartPosCombatUICanvas);
            attackerStartPosCombatUICanvas.z = attackerStartPos.z;

            Vector3 attackerzoomDestination;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(this.ArenaPlayers.Player.BlockerLocation, screenPoint2, this.uiCamera, out attackerzoomDestination);

            Vector3 startingScale = attackingCard.transform.localScale;
            attackingCard.Canvas.overrideSorting = true;
            attackingCard.SortingOrder = 29;
            attackingCard.RaycastTag = "inCombat";
            //attackingCard.transform.AttachToParent(this.View.CombatUICanvas, 1, sortingOrder:80, overrideSorting:true);
            attackingCard.transform.localScale = startingScale;
            attackingCard.transform.position = attackerStartPosCombatUICanvas;
            
            // Attacking card rises up above the arena...
            attackingCard.transform.DOKill();
            Sequence seq = DOTween.Sequence();
            seq.Append(attackingCard.transform.DOMove(attackerzoomDestination, 0.2f));
            //seq.Append(attackingCard.transform.DOLocalMoveZ(-500f, 0.2f));
            seq.AppendInterval(0.1f);

            if(defendingCard != null)
            {
                string defenderTag = defendingCard.RaycastTag;
                //Transform defenderParent = defendingCard.transform.parent;
                //int defenderSiblingIndex = defendingCard.transform.GetSiblingIndex();
                Vector3 defenderScale = defendingCard.transform.localScale;
                Vector3 defenderStartPos = defendingCard.transform.position;

                defendingCard.Canvas.overrideSorting = true;
                defendingCard.SortingOrder = 28;
                defendingCard.RaycastTag = "inCombat";
                //defendingCard.transform.AttachToParent(this.View.CombatUICanvas, 1, sortingOrder:79, overrideSorting:true);
                defendingCard.transform.localScale = defenderScale;

                // Defending card rises above the arena
                //seq.Insert(0.1f,defendingCard.transform.DOLocalMoveZ(-407f, 0.2f));
				seq.Insert(0.1f,defendingCard.transform.DOMove(defenderWorldPos,0.3f).SetEase(Ease.OutQuint));
                 // Attacking card moves to attack location
				seq.Append(attackingCard.transform.DOMove(defenderWorldPos,0.4f).SetEase(Ease.InQuart));

                // Once it arrives.. play the attack sound
                if (attackingCard is CardFrontController)
                {
                    seq.AppendCallback(() => (attackingCard as CardFrontController).AttackImpactAudio.Play());
                }

                Vector2 attackVector = defenderWorldPos - attackingCard.transform.position;
                Transform cardTrans = defendingCard.transform;
                attackVector.Normalize();
                attackVector *= 100f;
                seq.AppendCallback(()=>{
                     /* Shake Target Card */
                     Sequence shakeSeq = DOTween.Sequence();
                    
                     if(cardTrans != null)
                     {
                        Vector3 startingPos = cardTrans.localPosition;
                        shakeSeq.Append(cardTrans.DOLocalMove(startingPos + (Vector3)attackVector, 0.1f).SetEase(Ease.OutElastic));
                        shakeSeq.Append(cardTrans.DOLocalMove(startingPos,0.2f));
                        shakeSeq.AppendInterval(0.8f);
                        shakeSeq.Append(cardTrans.DOMove(defenderStartPos,0.3f).SetEase(Ease.InOutQuart));
                        shakeSeq.AppendCallback(()=> {
                            defendingCard.Canvas.overrideSorting = false;
                            defendingCard.SortingOrder = 0;
                            defendingCard.RaycastTag = defenderTag;
                            //defendingCard.transform.AttachToParent(defenderParent, defenderSiblingIndex, overrideSorting: false);
                            defendingCard.transform.localScale = defenderScale;
                        });
                        shakeSeq.Play();
                     }
                     else
                     {
                         Debug.LogError("Couldn't find defender root to shake");
                     }
                });
            }
			else // defendingCard == null
            {
                // Attacking card moves to attack location
                seq.Append(attackingCard.transform.DOMove(defenderWorldPos,0.4f).SetEase(Ease.InQuart));

                // Once it arrives.. play the attack sound
                if (attackingCard is CardFrontController)
                {
                    seq.AppendCallback(() => (attackingCard as CardFrontController).AttackImpactAudio.Play());
                }

                Vector3 attackVector = defenderWorldPos - attackingCard.transform.position;
                attackVector.Normalize();
                attackVector *= 30f;
                
                seq.AppendCallback(()=>{
                    if (View.shakeScreen != null)
                        View.shakeScreen.Kill();
                    View.shakeScreen = DOTween.Sequence();
                    Vector3 startingPos = this.transform.localPosition;
                    View.shakeScreen.Append(this.transform.DOLocalMove(attackVector, 0.1f));
                    View.shakeScreen.Append(this.transform.DOLocalMove(startingPos,0.6f).SetEase(Ease.OutElastic));
                    View.shakeScreen.AppendCallback(() => View.shakeScreen = null);
                    //View.shakeScreen.Play();
                });
            }
            
            seq.Append(attackingCard.transform.DOMove(attackerzoomDestination, 0.4f).SetEase(Ease.OutQuart));
            seq.Append(attackingCard.transform.DOMove(attackerStartPosCombatUICanvas, 0.2f).SetEase(Ease.OutQuart));
            //seq.Append(attackingCard.transform.DOMove(attackerStartPos, 0.1f));
            seq.AppendCallback(()=> {
                attackingCard.Canvas.overrideSorting = false;
                attackingCard.SortingOrder = 0;
                attackingCard.RaycastTag = attackerTag;
                //attackingCard.transform.AttachToParent(attackerParent, attackerSiblingIndex, overrideSorting: false);
                attackingCard.transform.localScale = startingScale;
                attackingCard.Canvas.sortingLayerName = "Default";
                attackingCard.transform.position = attackerStartPos;
                if(phaseAttacked != Phase.Combat)
                {
                    this.View.PlayerArenaController.ArenaLayout.UpdateCardTweens();
                    this.View.OpponentArenaController.ArenaLayout.UpdateCardTweens();
                }
                //((CardFrontController)attackingCard).OverlayCanvas.sortingLayerName = "Default";
                Debug.LogFormat("#ArenaMediator#  AttackComplete: {0}", attackingCard.CardIdAndName);
            });
            //Debug.LogError("#ArenaMediator#  Setting attack sequence:" + attackingCard.TitleLabel.text  + " with ID:" + attackingCard.CardId);
            ((CardFrontController)attackingCard).AttackSequence = seq;
            //seq.Play();
        }
        
        private Vector3 GetDefenderPosition(CardController attackingCard, CardController defendingCard, int targetId, Phase phaseAttacked)
        {
            CardController arenaCard = this.ArenaCards.GetCard(targetId);
            if(arenaCard == null)
            {
                // The target is a player, return the position of the player portrait
                PlayerControllerBase player  = this.ArenaPlayers.GetPlayer(targetId);
                return player.TargetArrowTarget.BoundsRect.position;
            }
            else if (phaseAttacked != Phase.Combat)
            {
                return defendingCard.transform.position;
            }
            else
            {
                // The target is a defending unit, return a position right in front of the defending players portrait
                int controllerId = arenaCard.ControllerID;
                PlayerControllerBase player  = this.ArenaPlayers.GetPlayer(controllerId);
                return player.BlockerLocation.transform.position;
            }
        }
       
        private void OnTrashOpponentCard(int cardId, CardZone fromZone, GameStateReadOnly gameState)
        {
            // Only do this delayed trashing for opponent cards in the arena
            CardController card = this.ArenaCards.GetCard(cardId);
             //Debug.Log("#Arena#OnTrashOpponentCard '" + card + "' with ID:" + cardId);

            if(card == null)
            {
                Debug.Log("#Arena#Looking for opponent card card waiting to be trashed with ID:" + cardId);
                this.ArenaCards.CardsWaitingToBeRecycled.TryGetValue(cardId, out card);
            }

            if(card != null && (fromZone == CardZone.Arena || fromZone == CardZone.Stack))
            {
                //card = this.ArenaCards.RemoveCard(cardId);
                ICardState cardState = gameState.GetCard(cardId);
                Debug.LogFormat("#Arena#Trashing OpponentCard Card: '{0}' with ID: {1}", (cardState != null ? cardState.GetName() : cardId.ToString()), cardId);
                this.StartCoroutine(this.WaitToRecycleCard(card, gameState, false));
            } 
        }
        
        private void OnTrashPlayerCard(int cardId, CardZone fromZone, GameStateReadOnly gameState)
        {
            // only do this delayed trashing for player cards in the arena
            CardController card = this.ArenaCards.GetCard(cardId);
            if(card != null && fromZone == CardZone.Arena)
            {
                Debug.Log("#Arena#Trashing Players Card: " + gameState.GetCard(cardId).GetName() + " with ID: " + cardId);
                //card = this.ArenaCards.RemoveCard(cardId);
                this.StartCoroutine(this.WaitToRecycleCard(card, gameState, true)); 
            }
        }

        private IEnumerator WaitToRecycleCard(CardController card, GameStateReadOnly gameState, bool isPlayerCard)
        {
            int cardId = card.CardId;
            Phase phaseRecycled = gameState.Turn.Phase;

            this.ArenaCards.CardsWaitingToBeRecycled[card.CardId] = card;

            // Release all the arrows for this card
            List<int> toRemove = new List<int>();
            if (this.blockTargetArrows.ContainsKey(cardId))
            {
                foreach(TargetArrowInfo arrow in this.blockTargetArrows[cardId].Values)
                {
                    this.View.TargetArrowController.ReleaseTargetArrowInfo(arrow);
                }
                toRemove.Add(cardId);
                this.blockTargetArrows[cardId].Clear();
            }
            foreach (var arrowList in this.blockTargetArrows)
            {
                if (arrowList.Value != null && arrowList.Value.ContainsKey(cardId))
                {
                    this.View.TargetArrowController.ReleaseTargetArrowInfo(arrowList.Value[cardId]);
                    arrowList.Value.Remove(cardId);
                    // need to remove empty list items;
                }
                if (arrowList.Value.Count == 0 && arrowList.Key != cardId)
                    toRemove.Add(arrowList.Key);
            }

            // Remove the entery for this cardId from tohe bockTArgetArrows dictionary
            foreach (var id in toRemove)
            {
                this.blockTargetArrows.Remove(id);
            }

            // Fade in the death overlay (currently a skull image)
            if (card is CardFrontController)
            {
                CardFrontController cardFront = (CardFrontController)card;
                card.gameObject.name = "CardFront (Waiting to Trash)";
                cardFront.DeathOverlayCanvasGroup.DOKill();
                cardFront.DeathOverlayCanvasGroup.alpha =0;
                cardFront.DeathOverlayCanvasGroup.DOFade(1.0f,0.3f).SetDelay(1.5f);
            }
            var target = Time.realtimeSinceStartup + 3f; while(Time.realtimeSinceStartup < target) { yield return null; }
            yield return new WaitForSecondsRealtime(3f); // Waits a while letting players understand what's happened


            card.Canvas.overrideSorting = true;
			card.CanvasGroup.blocksRaycasts = false;
			card.SortingOrder = 20;
            card.RaycastTag = "Untagged"; // untagging here, so the card no longer gets laid out by the arena
            Sequence seq = DOTween.Sequence();
            //seq.Append(card.transform.DOMove( (isPlayerCard ? this.View.PlayerTrashIcon.transform.position : this.View.OpponentTrashIcon.transform.position), 0.8f).SetEase(Ease.OutQuint));
            //seq.Append(card.transform.DOJump((isPlayerCard ? this.View.PlayerTrashIcon.transform.position : this.View.OpponentTrashIcon.transform.position), -200.0f,1, 0.8f).SetEase(Ease.OutQuint));
            seq.Append(card.transform.DOPath((isPlayerCard 
                ? new Vector3[] { this.View.PlayerTrashCardHolder.transform.position, this.View.PlayerTrashIcon.transform.position } 
                : new Vector3[] { this.View.OpponentTrashCardHolder.transform.position, this.View.OpponentTrashIcon.transform.position }), 0.8f, pathType:PathType.CatmullRom).SetEase(Ease.OutQuint));

            seq.InsertCallback(0.2f, () => this.View.TrashCardSound.Play());
            seq.Insert(0.2f, card.CanvasGroup.DOFade(0.2f, 0.55f));
            seq.Insert(0.2f, card.transform.DOScale(0.25f, 0.5f));

            if(isPlayerCard)
            {
                // Open the players trash icon (and close it)
                this.View.PlayerTrashIcon.StopAllCoroutines();
                this.View.PlayerTrashIcon.sprite = this.View.TrashCanOpen;
                this.View.PlayerTrashIcon.StartCoroutine(WaitToCloseTrashcan(this.View.PlayerTrashIcon, 0.7f));
            }
            else
            {
                // Open the opponents trash icon (and close it)
                this.View.OpponentTrashIcon.StopAllCoroutines();
                this.View.OpponentTrashIcon.sprite = this.View.TrashCanOpen;
                this.View.OpponentTrashIcon.StartCoroutine(WaitToCloseTrashcan(this.View.OpponentTrashIcon, 0.7f));
            }

            yield return new WaitForSecondsRealtime(0.4f);


            // Update the card state of the trashed card
            if (gameState is GameStateContainer)
            {
                ICardState currentState = gameState.CurrentStateForCardId(cardId);
                var currentGameState = (GameStateContainer)gameState;
                //Debug.LogFormat("Adapting Dead card {0} to current info", currentState.Id);
                currentGameState.AdaptCard(currentState);
                OnCardStateUpdated(currentState, gameState);
            }



            if(card is CardBackController)
            {
                // Recycle a card Back
                CardBackController cardBack = (CardBackController)card;
                cardBack.gameObject.name = "CardBack (Waiting to Trash)";
				Debug.LogFormat("#Arena#Recycling CardBack with ID: {0}", cardId);
                this.ArenaCards.CardsWaitingToBeRecycled.Remove(card.CardId);
                cardBack.Recycle();

                yield return null; // wait until next frame
                this.CardBackRecycledSignal.Dispatch(cardId);
            }
            else
            {
                // Recycle a card CardFront
				Debug.LogFormat("#Arena#Recycling {0}", card.CardIdAndName);

                var cardFront = card as CardFrontController;
                if (cardFront != null)
                    cardFront.ResetCardFront();
                // TODO: Possibly make TargetArrowController listen to the trash event and remove
                // the target itself -DMac
                this.View.TargetArrowController.RemoveTarget(card.TargetArrowTarget);
                
                this.ArenaCards.RemoveCard(card);

                if(card != null)
                    card.transform.DOKill();
                this.ArenaCards.CardsWaitingToBeRecycled.Remove(card.CardId);
                card.Recycle();
                
                yield return null; // waita until next frame
                this.ArenaCardRecycledSignal.Dispatch(cardId, isPlayerCard, phaseRecycled);
            }
        }

        private IEnumerator WaitToCloseTrashcan(Image image, float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            image.sprite = this.View.TrashCanClosed;
        }

        private void OnLegalAbilitiesUpdated(GameStateReadOnly gameState)
        {
            // only update legal abilities if there's no selected card in the player's hand or arena
            if (this.View != null && this.View.PlayerHandController.SelectedCard == null && this.InputEventController.SelectedCard == null)
            {
                var cardControllers = ArenaCards.AllCardControllers;
                for (int i = 0; i < cardControllers.Count(); i++)
                {
                    cardControllers[i].DisplayLegalAbilities(gameState.LegalAbilities);
                }
            }
            /*
            IEnumerable<ICardState> localPlayersCards = gameState.CardsForPlayer(gameState.SelfId);
            foreach(ICardState card in localPlayersCards)
            {
                CardController cardController = this.ArenaCards.GetCard(card.Id);
                if(cardController != null)
                {
                    cardController.DisplayLegalAbilities(gameState.LegalAbilities);                   
                    //this.View.ArenaZoomController.CopyCardGlows(cardController);
                }
                else
                {
                    //Debug.LogWarning("Got legal abilites for " + (card.ControllerId == this.GameState.SelfId ? "a players" : "an opponents" )  + " card that didn't exist (" + card.GetName() + "): " + card.Id + " that was in zone " + card.Zone);
                }
            }*/
        }
        
        private void OnGainControlOfCard(ICardState cardState)
        {
            if (cardState == null)
            {
                Debug.LogError("Failed to gain control");
            }
            else if (cardState.Zone == CardZone.Arena)
            {
                Timer.GameInstance.StartCoroutine(WaitToGainControlCoroutine(cardState));
            }
        }

        private IEnumerator WaitToGainControlCoroutine(ICardState cardState)
        {
            CardController card = this.ArenaCards.GetCard(cardState.Id);
            Debug.LogFormat("Gaining control of {0}", card.CardIdAndName);
            float timeout = Time.realtimeSinceStartup + 3.0f;
            while(card.RaycastTag != "arenaCard")
            {
                yield return null;
                if (timeout < Time.realtimeSinceStartup)
                {
                    MetricsTrackingService.SavedException = "Gain Control timeout";
                    break;
                }
            }
            if (card == null)
            {
                Debug.LogFormat("Card is null for lose control of {0}", card.CardIdAndName);
                yield break;
            }
            Debug.LogFormat("Gained control of {0}", card.CardIdAndName);
            card.transform.AttachToParent(View.PlayerArenaController.transform, 0);
            this.View.PlayerArenaController.ArenaLayout.UpdateCardTweens();
            this.View.OpponentArenaController.ArenaLayout.UpdateCardTweens();
            card.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            if (card is CardFrontController)
            {
                var cardFront = (CardFrontController)card;
                cardFront.ResetGlows();

                cardFront.EnqueueScrollingText(I2.Loc.ScriptLocalization.Stolen, Blue);
            }
            this.View.PlayerHandController.ShowCastTargets();
            this.View.PlayerArenaController.RefreshTargets();
        }

        private void OnLoseControlOfCard(ICardState cardState)
        {
            if (cardState == null)
            {
                Debug.LogError("Failed to lose control");
            }
            else if (cardState.Zone == CardZone.Arena)
            {
                Timer.GameInstance.StartCoroutine(WaitToLoseControlCoroutine(cardState));
            }
        }

        private IEnumerator WaitToLoseControlCoroutine(ICardState cardState)
        {
            CardController card = this.ArenaCards.GetCard(cardState.Id);
            Debug.LogFormat("Losing control of {0}", card.CardIdAndName);
            float timeout = Time.realtimeSinceStartup + 3.0f;
            while (card.RaycastTag != "arenaCard")
            {
                yield return null;
                if (timeout < Time.realtimeSinceStartup)
                {
                    MetricsTrackingService.SavedException = "Lose Control timeout";
                    break;
                }
            }
            if(card == null)
            {
                Debug.LogFormat("Card is null for lose control of {0}", card.CardIdAndName);
                yield break;
            }
            Debug.LogFormat("Lost control of {0}", card.CardIdAndName);
            card.transform.AttachToParent(View.OpponentArenaController.transform, 0);
            this.View.PlayerArenaController.ArenaLayout.UpdateCardTweens();
            this.View.OpponentArenaController.ArenaLayout.UpdateCardTweens();

            card.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            if (card is CardFrontController)
            {
                var cardFront = (CardFrontController)card;
                cardFront.ResetGlows();

                cardFront.EnqueueScrollingText(I2.Loc.ScriptLocalization.Stolen, Blue);
            }
            this.View.PlayerHandController.ShowCastTargets();
            this.View.PlayerArenaController.RefreshTargets();
        }

        private void OnCardStateUpdated(ICardState cardState, GameStateReadOnly gameState)
        {
            this.View.isCardStatesUpdated = true;
            CardController card = this.ArenaCards.GetCard(cardState.Id);
            if (card != null && typeof(CardFrontController).IsAssignableFrom(card.GetType()))
            {
                CardFrontController cardFront = (CardFrontController)card;
                cardFront.UpdateCardState(gameState);
                
                CardFrontController draggedCard = (CardFrontController)this.View.PlayerArenaController.DraggedCard;
                
                // Is the player currently dragging a card?
                if(draggedCard != null)
                {
                    bool isTargeted;
                    // Get a list of targets for the currently dragged card
                    List<int> targets = gameState.LegalAbilities.GetCastTargets(draggedCard.CardId, out isTargeted);
                  
                    TargetArrowController targetArrowController = this.View.TargetArrowController;
                    
                    // Is the cardFront being updated a target of the dragged card?
                    if(targets != null && targets.Contains(cardState.Id))
                    {
                        if (isTargeted)
                        {
                            cardFront.TargetArrowTarget.TargetGlowAnimator.SetBool("IsCastTarget", true);
                            cardFront.TargetArrowTarget.TargetAction = TargetAction.Attack;
                        }
                        else
                        {
                            cardFront.TargetArrowTarget.TargetGlowAnimator.SetBool("IsAffected", true);
                            cardFront.TargetArrowTarget.TargetAction = TargetAction.Affected;
                        }
                        targetArrowController.AddTarget(cardFront.TargetArrowTarget);
                    }
                    else
                    {
                        cardFront.TargetArrowTarget.TargetGlowAnimator.SetBool("IsCastTarget", false);
                        cardFront.TargetArrowTarget.TargetGlowAnimator.SetBool("IsAffected", false);
                        // it is no longer a target of the dragged card, remove it from the target list
                        targetArrowController.RemoveTarget(cardFront.TargetArrowTarget);
                    }
                }
            }
        }
        
	 	/// <summary>
		/// Listens to the GameEventMessageSignal and re-broadcasts the message using unity messaging
		/// TODO: Eventually move away from unity's broadcast message and have events
		/// and event listeners that subscribe to them -DMac
		/// </summary>
	    private void OnGameEventMessage(EventMessage message)
        {
			//Debug.LogWarning("GameControllerMediator (" + this.GetInstanceID() + ") GameEventMessageSignal: " + message.EventType);
            switch(message.EventType)
			{
			case EventTypes.ZoneChangeEvent:
                 this.CardEventService.OnZoneChangeEvent(message);
				break;
            case EventTypes.ResolveEvent:
                 //Debug.Log("#GameEent#ResolveEvent for id:" + message.Affected);
                this.CardEventService.OnResolveEvent(message);
                break;
            case EventTypes.CastEvent:
                //Debug.Log("#GameEent#CastEvent for id:" + message.Affected);
                this.CardEventService.OnCastEvent(message);
                break;
            case EventTypes.AttackEvent:
				this.CardEventService.OnAttackEvent(message);
				break;
            case EventTypes.DamageEvent:
            case EventTypes.HealthCostEvent:
                this.CardEventService.OnDamageTaken(message);
				break;
            case EventTypes.BecomesEvent:
                this.BecomesNewCard(message);
                break;
            case EventTypes.ControlChangeEvent:
				this.CardEventService.OnChangeControlEvent(message);
				break;
			case EventTypes.HealEvent:
				this.CardEventService.OnHealing(message);
				break;
            case EventTypes.EvadeEvent:
                this.EventEvade(message);
                break;
            case EventTypes.PurifyEvent:
				this.EventPurify(message);
				break;
			case EventTypes.FxEvent:
                this.EventFxEvent(message);
				break;
			case EventTypes.CancelEvent:
				this.CardEventService.OnCancelEvent(message);
				break;
			case EventTypes.PlayerActionFailure:
				this.CardEventService.OnCardAbilityFailed(message);
				break;
			case EventTypes.PlayerActionSuccess:
				this.CardEventService.OnCastAbilitySucceeded(message);
				break;
            case EventTypes.ModifyOrSetStatEvent:
                this.CardEventService.OnModifyOrSetStatEvent(message);
                break;
            case EventTypes.GainInitiativeEvent:
                this.CardEventService.OnGainInitiative(message);
                break;
            case EventTypes.ActivateEvent:
                this.CardEventService.OnActivateEvent(message);
                break;
                /* TO HANDLE:
                LoseGameEvent
                WinGameEvent
                RefillEnergyPoolEvent
                AttackingUnit
                BlockingUnit
                ShuffleEvent
                */
                case EventTypes.AttackingUnit:
            case EventTypes.BlockingUnit:
                case EventTypes.CreateCardEvent:
                    // Ignore these events
                    break;
            default:
                Debug.LogErrorFormat("Unhandled event message: {0}", message.EventType.ToString());
                break;
			}
        }

        /// <summary>
        /// Listens for the CombatStateMessageSignal, and sets the appropriate object's combat identifiers.
        /// </summary>
        /// <param name="combatState"></param>
        private void OnCombatStateUpdated(CombatState combatState)
        {
            CardController card = this.ArenaCards.GetCard(combatState.CardId);
            if (card != null && typeof(CardFrontController).IsAssignableFrom(card.GetType()))
            {
                CardFrontController blockerCard = (CardFrontController)card;
                blockerCard.UpdateCombatState(combatState);
                
                // Release all the arrows for this card
                if(this.blockTargetArrows.ContainsKey(blockerCard.CardId))
                {
                    foreach(TargetArrowInfo arrow in this.blockTargetArrows[blockerCard.CardId].Values)
                    {
                        this.View.TargetArrowController.FadeOutAndReleaseTargetArrow(arrow);
                    }
                    this.blockTargetArrows[blockerCard.CardId].Clear();
                }
                
                // Attempt to add/re-add any new ones
                if(combatState.BlockTargets != null && combatState.BlockTargets.Count > 0)
                {
                    //combatState.BlockTargets - list of all the units it's blocking
                    for(int i=0;i <combatState.BlockTargets.Count;i++)
                    {
                        int targetId = combatState.BlockTargets[i];
                        CardFrontController targetCard = (CardFrontController)this.ArenaCards.GetCard(targetId);
                        if(targetCard != null)
                        {
                           this.AddBlockingArrow(blockerCard,targetCard);
                        }
                    }
                }
            }
        }
        
        private void AddBlockingArrow(CardFrontController blockerCard, CardFrontController targetCard)
        {
            TargetArrowInfo targetArrow = null;
            if(blockerCard.CardState.ControllerId == this.GameState.SelfId)
            {
                targetArrow = this.View.TargetArrowController.RequestTargetArrowInfo(blockerCard.BlockingArrowAnchor, targetCard.HealthLabel.transform, this.View.BlockerArrowsRoot);
            }
            else
            {
                targetArrow = this.View.TargetArrowController.RequestTargetArrowInfo(blockerCard.BlockingIndicator.transform, targetCard.BlockedByOpponentAnchor, this.View.BlockerArrowsRoot);
            }
            
            targetArrow.StartingColor = new Color32(40,170,255,255);
            
            // Ensure this blocker exists in the blockTargetArrows dictionary
            if(this.blockTargetArrows.ContainsKey(blockerCard.CardId) == false)
            {
                this.blockTargetArrows[blockerCard.CardId] = new Dictionary<int,TargetArrowInfo>();
            }
            
            this.blockTargetArrows[blockerCard.CardId][targetCard.CardId] = targetArrow;
        }
        
        /// <summary>
        /// Removes a targetArrowInfo from being managed in the blockTargetArrows state, this
        /// allows a caller to remove the arrow but keep a reference to it in case they want to 
        /// tween / fade it out in some way now that it's no longer indicating an active block state
        /// </summary>
        private TargetArrowInfo RemoveBlockingArrowRefernce(int blockerId, int targetId)
        {
            TargetArrowInfo targetArrow = null;
            if(this.blockTargetArrows.ContainsKey(blockerId))
            {
                if(this.blockTargetArrows[blockerId].TryGetValue(targetId, out targetArrow))
                {
                    this.blockTargetArrows[blockerId].Remove(targetId);
                }
            }
            
            return targetArrow;
        }
		
        private void OnGameStateInitialized(GameStateReadOnly gameState)
        {
            this.GameState = gameState;

			Debug.Log("#ArenaMediator# Adding gamestate to ArenaMediator");  
            this.CardEventService.GameState = gameState;
            
            // Give the InputEventController access to the gamestate
            InputEventController = GameObject.FindObjectOfType<InputEventController>();
            InputEventController.GameState = gameState;
            InputEventController.ArenaCardZoom.GameState = gameState;
            
			// Show the Starting Hand UI
            this.InitializeAnimatedViewSignal.Dispatch(typeof(StartingHandView),v=>((StartingHandView)v).Initialize(gameState));
            //this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(StartingHandView));
            this.CloseAnimatedViewSignal.Dispatch(typeof(StartingHandView));

            // Create a Tutorial Popup dialog but leave it hiden
            this.InitializeAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView),v=>((TutorialPopUpView)v).GameState = gameState);
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(StartGameDialogView));
        }

        public void EventEvade(EventMessage message)
        {
            CardController card = this.ArenaCards.GetCard(message.Affected);
            var cardFront = (CardFrontController)card;
            if (cardFront != null)
            {
                var affected = GetFxRect(message.Affected);
                this.PlayFxSignal.Dispatch(new PlayFxParams(message.Affected, message.Affected, null, affected, null, "FX_Holysymbol_shield", PlayFxPath.NoPath,0));

                this.StartCoroutine(this.WaitForCardToArriveInArena(
                    cardFront,
                    I2.Loc.ScriptLocalization.Evaded,
                    Green));
            }
        }
        
        public void BecomesNewCard(EventMessage message)
        {
            CardController card = this.ArenaCards.GetCard(message.Affected);
            if (card != null && typeof(CardFrontController).IsAssignableFrom(card.GetType()))
            {
                CardFrontController cardFront = (CardFrontController)card;
                cardFront.BecomeNewCard();

                this.StartCoroutine(this.WaitForCardToArriveInArena(
                    cardFront,
                    I2.Loc.ScriptLocalization.Transformed,
                    Blue));
            }
        }
        public void EventPurify(EventMessage message)
        {
            //ICardState cardState = GameState.GetCard(message.Source);
            CardController card = this.ArenaCards.GetCard(message.Affected);
            if (card != null && /*cardState != null && */typeof(CardFrontController).IsAssignableFrom(card.GetType()))
            {
                CardFrontController cardFront = (CardFrontController)card;
                cardFront.IsPurified = true;
                cardFront.CardState.IsUpdated = true;
                //cardState.IsUpdated = true;
                cardFront.UpdateLabels();

                this.StartCoroutine(this.WaitForCardToArriveInArena(
                    cardFront,
                    I2.Loc.ScriptLocalization.Purify,
                    Red));
            }
        }
        
        public void EventControlChanged(EventMessage message)
        {

        }
        /*
        public void EventCancel(EventMessage message)
        {
            // Make sure the message is about THIS card  being canceled
            if (this.cardState.Id != message.Affected) { return; }

            ICardState sourceCard = this.controller.GameState.GetCard(message.Source);

            //Debug.Log("FxEvent: " + sourceCard.GetName() + " applying '" + sourceCard.GetFxScript() + "' to " + this.cardState.GetName());

            if (sourceCard != null)
            {
                this.TriggerFxScript("fx_shadowburst");
                this.lastTarget = -1;
            }
        }*/

        private RectTransform GetFxRect(int cardId, string isToLocation = null)
        {
            if (cardId == ArenaPlayers.Player.PlayerId)
            {
                if (isToLocation == PlayFxParams.ENERGY_FX_TARGET)
                    return ArenaPlayers.Player.EnergyBarController.transform as RectTransform;
                else if (isToLocation == PlayFxParams.HEALTH_FX_TARGET)
                    return ArenaPlayers.Player.HealthBarController.transform as RectTransform;
                else
                    return ArenaPlayers.Player.AvatarFrame;
            }
            else if (cardId == ArenaPlayers.Opponent.PlayerId)
            {
                if (isToLocation == PlayFxParams.ENERGY_FX_TARGET)
                    return ArenaPlayers.Opponent.EnergyBarController.transform as RectTransform;
                else if (isToLocation == PlayFxParams.HEALTH_FX_TARGET)
                    return ArenaPlayers.Opponent.HealthBarController.transform as RectTransform;
                else
                    return ArenaPlayers.Opponent.AvatarFrame;
            }
            else if (cardId == 0)
                return View.GameFxAnchor;

            /* Priority order is:
             * 1. Card Backs
             * 2. Arena Cards
             * 3. Played powers
             * (because you can create a cardfront for a hand cardback, but that cardfront might not be visible; cardbacks are always visible if created)
            */

            CardController card = this.ArenaCards.GetCardBack(cardId);

            if(card == null)
                card = this.ArenaCards.GetCard(cardId);

            if(card == null)
            {
                this.ArenaCards.GetCardWaitingForRecycle(cardId);
            }

            // player played powers
            if (card != null)
            {
                //Debug.LogFormat("#TrailFX# Card exists {0}", cardId);
                CardController targetedPlayerCard = this.View.PlayerHandController.GetTargetedPowerCard(card);
                if (targetedPlayerCard != null)
                {
                    //Debug.LogFormat("#TrailFX# Targeted player card {0}", cardId);
                    card = targetedPlayerCard;
                }
            }

            // opponent played powers
            CardController targetedOpponentCard = this.View.OpponentHandController.GetPlayedPower(cardId);
            if (targetedOpponentCard != null)
            {
                //Debug.LogFormat("#TrailFX# Targeted opponent card {0}", cardId);
                card = targetedOpponentCard;
            }
            
            // opponent cardbacks
            /*if (card == null)
            {
                var cardBack = this.ArenaCards.GetCardBack(cardId);
                if(cardBack != null)
                {
                    card = cardBack;
                }
            }*/

            return card != null ? card.Card.rectTransform : null;
        }

        public void EventFxEvent(EventMessage message)
        {
            //message.PathFxToPlay = "whesting_e";
            //message.FxToPlay = "testing_h";
            string toLoc = null;
            if (toLoc == null && !string.IsNullOrEmpty(message.FxToPlay))
            {
                var potential = message.FxToPlay.Substring(message.FxToPlay.Length - 2);
                if (potential == PlayFxParams.ENERGY_FX_TARGET || potential == PlayFxParams.HEALTH_FX_TARGET)
                {
                    toLoc = potential;
                    message.FxToPlay = message.FxToPlay.Substring(0, message.FxToPlay.Length - 2);
                }
            }
            if (!string.IsNullOrEmpty(message.PathFxToPlay))
            {
                var potential = message.PathFxToPlay.Substring(message.PathFxToPlay.Length - 2);
                if (potential == PlayFxParams.ENERGY_FX_TARGET || potential == PlayFxParams.HEALTH_FX_TARGET)
                {
                    toLoc = potential;
                    message.PathFxToPlay = message.PathFxToPlay.Substring(0, message.PathFxToPlay.Length - 2);
                }
            }
            
            RectTransform source = GetFxRect(message.Source);
            RectTransform affected = GetFxRect(message.Affected, toLoc);
            if (source == null || affected == null)
            {
                Debug.LogErrorFormat("#FX# null {0} found playing {1}by{2}then{3}", source == null ? "source" : "affected", message.PathFxToPlay, message.PathType, message.FxToPlay);
                return;
            }
            //Debug.LogFormat("#FX# playing {0} by {1} then {2} from {3}", message.PathFxToPlay, message.PathType, message.FxToPlay, message.Source);

            //this.PlayFxSignal.Dispatch(new PlayFxParams(source, affected, null, fxScript, PlayFxPath.NoPath, message.Amount));
            this.PlayFxSignal.Dispatch(new PlayFxParams(message.Source, message.Affected, source, affected, message.PathFxToPlay, message.FxToPlay, message.Source != message.Affected ? message.PathType : PlayFxPath.NoPath, message.PathDurationMs));
        }

        private void StopPlayerFxScript(PlayerControllerBase player, string fxName)
        {
            ParticleEffectsTrigger particleFx = null;
            if (player.fxScripts.TryGetValue(fxName.ToLower(), out particleFx))
            {
                particleFx.EndEffect();
                if (player.currentParticleEffects == particleFx)
                {
                    player.currentParticleEffects = null;
                }
            }
        }
    }
}
