﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using NovaBlitz.UI;
using DragonFoundry.Fx;
using DragonFoundry.Controllers;
using DragonFoundry.Controllers.Display;
using LinqTools;
using DragonFoundry.Networking;
using Messages;

namespace NovaBlitz.Game
{
    public class TutorialPopUpView : AnimatedView
    {
        public TextMeshProUGUI TitleText;
        public TextMeshProUGUI Content;
        public NovaButton DoneButton;
        public GameObject Scrim;
        public RectTransform PlayableFx;
        public System.Action DoneClicked;
        public System.Action<bool> MoveCanvas;
        public DragonFoundry.GameState.GameStateReadOnly GameState;
        private bool isOnTop = false;
        private string Hint = string.Empty;

        /*protected override void Awake()
        {
            //this.IsOpen = false;
            base.Awake();
        }*/

        public void Initialize(string hint, bool isOnRight)
        {
            Hint = hint;

            if(isOnRight)
            {
                PlayableFx.pivot = new Vector2(1.0f, 0.5f);
                PlayableFx.anchorMin = new Vector2(1.0f, 0.5f);
                PlayableFx.anchorMax = new Vector2(1.0f, 0.5f);
                PlayableFx.anchoredPosition = new Vector2(0.0f, 110.0f);
            }
            else
            {
                PlayableFx.pivot = new Vector2(0.0f, 0.5f);
                PlayableFx.anchorMin = new Vector2(0.0f, 0.5f);
                PlayableFx.anchorMax = new Vector2(0.0f, 0.5f);
                PlayableFx.anchoredPosition = new Vector2(0.0f, -110.0f);
            }

            SetButtonAndScrim();
            Content.text = I2.Loc.ScriptLocalization.Get("TutorialHintText/" + hint);
            TitleText.text = I2.Loc.ScriptLocalization.Get("TutorialHintTitle/" + hint);
            isOnTop = hint == "Mulligans";
            if (MoveCanvas != null)
            {
                MoveCanvas.Invoke(isOnTop);
            }
        }

        /*protected override void Update()
        {
            base.Update();
            SetButtonAndScrim();
        }*/

        public void SetButtonAndScrim()
        {
            bool canAct = (GameState == null || GameState.LegalAbilities.CanTakeUsefulGameAction() && Hint != "Timer");
            DoneButton.gameObject.SetActive(!canAct);
            Scrim.gameObject.SetActive(!canAct);
        }
        /*
        public void Initialize(DiffGameStateMessage pausedMessage)
        {
            switch(pausedMessage.PauseReason)
            {
                case TutorialHint.NoPopUp:
                    TitleText.text = I2.Loc.ScriptLocalization.TutorialHint.NoPopUp;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.NoPopUpHint;
                    break;
                case TutorialHint.Activate:
                    TitleText.text = I2.Loc.ScriptLocalization.TutorialHint.Activate;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.ActivateHint;
                    break;
                case TutorialHint.BlitzTime:
                    TitleText.text = I2.Loc.ScriptLocalization.TutorialHint.BlitzTime;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.BlitzTimeHint;
                    break;
                case TutorialHint.EndOfActionPhase:
                    TitleText.text = I2.Loc.ScriptLocalization.TutorialHint.EndOfActionPhase;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.EndOfActionPhaseHint;
                    break;
                case TutorialHint.EndOfCombatPhase:
                    TitleText.text = I2.Loc.ScriptLocalization.TutorialHint.EndOfCombatPhase;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.EndOfCombatPhaseHint;
                    break;
                case TutorialHint.Initiative:
                    TitleText.text = I2.Loc.ScriptLocalization.Initiative;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.InitaitiveHint;
                    break;
                case TutorialHint.Keywords:
                    TitleText.text = I2.Loc.ScriptLocalization.Keywords;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.KeywordsHint;
                    break;
                case TutorialHint.Mulligan:
                    TitleText.text = I2.Loc.ScriptLocalization.Mulligan;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.MulliganHint;
                    break;
                case TutorialHint.Units:
                    TitleText.text = I2.Loc.ScriptLocalization.Units;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.UnitsHint;
                    break;
                case TutorialHint.Powers:
                    TitleText.text = I2.Loc.ScriptLocalization.Powers;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.PowersHint;
                    break;
                case TutorialHint.Attack:
                    TitleText.text = I2.Loc.ScriptLocalization.Attack;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.AttackHint;
                    break;
                case TutorialHint.Block:
                    TitleText.text = I2.Loc.ScriptLocalization.Block;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.BlockHint;
                    break;
                case TutorialHint.Play:
                    TitleText.text = I2.Loc.ScriptLocalization.TutorialHint.Play;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.PlayHint;
                    break;
                case TutorialHint.Flying:
                    TitleText.text = I2.Loc.ScriptLocalization.Flying;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.FlyingHint;
                    break;
                case TutorialHint.FirstStrike:
                    TitleText.text = I2.Loc.ScriptLocalization.First_Strike;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.FirstStrikeHint;
                    break;
                default:
                    TitleText.text = I2.Loc.ScriptLocalization.TutorialHint.NoPopUp;
                    Content.text = I2.Loc.ScriptLocalization.TutorialHint.NoPopUpHint;
                    break;
            }

            // TODO: use variables from the paused message to initialize text fields on the view
        }*/

        public void OnClickDone()
        {
            if(this.DoneClicked != null)
            {
                this.DoneClicked.Invoke();
            }
        }
    }
}
