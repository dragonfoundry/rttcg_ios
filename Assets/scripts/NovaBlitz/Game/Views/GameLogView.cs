﻿using UnityEngine;
using UnityEngine.UI;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine.UI.Extensions;
using NovaBlitz.UI;
using System;

namespace NovaBlitz.Game
{
    public class GameLogView : AnimatedView
    {
        public System.Action DialogDismissed;
        public System.Action<int> DeleteLog;
        public System.Action AddFriend;
        public System.Action<int,RectTransform> ShowTwoTouch;
        public Action LogIsInitialized;

        public PlayerCardView OpponentPlayerCard;
        public PlayerCardView SelfPlayerCard;
        public NovaButton AddFriendButton;
        public NovaButton DeleteLogButton;
        public GameLogElement GameLogElementPrefab;
        public ScrollRect ScrollRect;
        public HorizontalLayoutGroup HorizontalLayout;
        public UiHorizontalGraph OppHealth;
        public UiHorizontalGraph SelfHealth;
        public UiLineRenderer InitLine;

        public int LogNumber { get; set; }

        public void OnClickClose()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke();
            }
        }

        public void OnDeleteLog()
        {
            if (this.DeleteLog != null)
            {
                this.DeleteLog.Invoke(this.LogNumber);
            }
        }

        public void OnClickAddFriend()
        {
            if (this.AddFriend != null)
            {
                this.AddFriend.Invoke();
            }
        }

        public void OnShowTwoTouch(int cardId, RectTransform sourceTransform)
        {
            if(this.ShowTwoTouch != null)
            {
                this.ShowTwoTouch.Invoke(cardId, sourceTransform);
            }
        }

        public void Initialize(GameLog log, int logNumber, bool showDelete)
        {
            this.DeleteLogButton.gameObject.SetActive(showDelete);
            this.LogNumber = logNumber;
            this.AddFriendButton.interactable = true;
            this.AddFriendButton.gameObject.SetActive(false);
            this.SelfPlayerCard.Initialize(log.PlayerPlayerData);
            this.OpponentPlayerCard.Initialize(log.OpponentPlayerData);

            var elements = this.HorizontalLayout.GetComponentsInChildren<GameLogElement>();
            int existingElements = elements.Count();
            for (int i = log.LogElements.Count; i < existingElements; i++)
            {
                elements[i].gameObject.SetActive(false);
            }
            // need to populate the log!!
            for (int i = log.LogElements.Count -1 ; i >=0; i--)
            {
                GameLogElement element;
                if (i < existingElements)
                    element = elements[i];
                else
                    element = (GameLogElement)GameObject.Instantiate(this.GameLogElementPrefab);
                element.transform.AttachToParent(this.HorizontalLayout.transform, 0);
                element.transform.localPosition = Vector3.zero;
                element.IsLast = (i + 1 == log.LogElements.Count);
                element.GameLogData = log.LogElements[i];
                element.SetAssets(log);
                element.ShowTwoTouchAction -= OnShowTwoTouch;
                element.ShowTwoTouchAction += OnShowTwoTouch;

            }
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)HorizontalLayout.transform);

            int startHealthDistance = 50;
            int maxHealthDistance = 300;
            OppHealth.BaseHeight = maxHealthDistance;
            SelfHealth.BaseHeight = -maxHealthDistance;
            int healthMultiplier = 5; // total gap = 280 @ 40h
            int initHeight = 20;
            int transitionlength = 30;
            float xValue = 0;
            List<Vector2> oppHealthPoints = new List<Vector2>();
            List<Vector2> selfHealthPoints = new List<Vector2>();
            List<Vector2> initPoints = new List<Vector2>();
            int currentInit = 0;
            int currentOppHealth = maxHealthDistance;
            int currentSelfHealth = -maxHealthDistance;
            elements = this.HorizontalLayout.GetComponentsInChildren<GameLogElement>();
            Debug.Log("{0} Elements, {1} log items");
            for (int i = 0; i < elements.Count(); i++)
            {
                GameLogElement element = elements[i];
                xValue += element.Rect.sizeDelta.x / 2;
                int newInit = element.GameLogData.InitiativeId == element.GameLogData.SelfId ? -initHeight : element.GameLogData.InitiativeId == element.GameLogData.OppId ? initHeight : currentInit;
                if(element.GameLogData.EventType == Messages.EventTypes.LoseGameEvent)
                {
                    initPoints.Add(new Vector2(xValue, currentInit));
                    currentInit = newInit;
                }
                else if(newInit != currentInit)
                {
                    // make an init transition at the mid-point
                    initPoints.Add(new Vector2(xValue, currentInit));
                    initPoints.Add(new Vector2(xValue + transitionlength, newInit));
                    currentInit = newInit;
                }

                int newOppHealth = Math.Max(startHealthDistance, Math.Min(maxHealthDistance, maxHealthDistance - element.GameLogData.OppHealth * healthMultiplier));
                if(element.GameLogData.EventType == Messages.EventTypes.StartOfTurnEvent && element.GameLogData.Turn == 1)
                {
                    oppHealthPoints.Add(new Vector2(0, newOppHealth));
                    currentOppHealth = newOppHealth;
                }
                else if (element.GameLogData.EventType == Messages.EventTypes.LoseGameEvent)
                {
                    oppHealthPoints.Add(new Vector2(xValue, currentOppHealth));
                    currentOppHealth = newOppHealth;
                }
                else if (newOppHealth != currentOppHealth)
                {
                    oppHealthPoints.Add(new Vector2(xValue, currentOppHealth));
                    oppHealthPoints.Add(new Vector2(xValue + transitionlength, newOppHealth));
                    currentOppHealth = newOppHealth;
                }

                int newSelfHealth = Math.Min(-startHealthDistance, Math.Max(-maxHealthDistance, element.GameLogData.SelfHealth * healthMultiplier - maxHealthDistance));
                if (element.GameLogData.EventType == Messages.EventTypes.StartOfTurnEvent && element.GameLogData.Turn == 1)
                {
                    selfHealthPoints.Add(new Vector2(0, newSelfHealth));
                    currentSelfHealth = newSelfHealth;
                }
                else if (element.GameLogData.EventType == Messages.EventTypes.LoseGameEvent)
                {
                    selfHealthPoints.Add(new Vector2(xValue, currentSelfHealth));
                    currentSelfHealth = newSelfHealth;
                }
                else if (newSelfHealth != currentSelfHealth)
                {
                    selfHealthPoints.Add(new Vector2(xValue, currentSelfHealth));
                    selfHealthPoints.Add(new Vector2(xValue + transitionlength, newSelfHealth));
                    currentSelfHealth = newSelfHealth;
                }
                //oppHealthPoints.Add(new Vector2(xValue, Math.Max(startHealthDistance, Math.Min(maxHealthDistance, maxHealthDistance - element.GameLogData.OppHealth * healthMultiplier))));
                //selfHealthPoints.Add(new Vector2(xValue, Math.Min(-startHealthDistance, Math.Max(-maxHealthDistance, element.GameLogData.SelfHealth * healthMultiplier - maxHealthDistance))));
                xValue += element.Rect.sizeDelta.x / 2;
            }

            this.ScrollRect.horizontalNormalizedPosition = 1;

            SelfHealth.Points = selfHealthPoints.ToArray();
            OppHealth.Points = oppHealthPoints.ToArray();
            InitLine.Points = initPoints.ToArray();
            SelfHealth.transform.AttachToParent(this.HorizontalLayout.transform, 0);
            OppHealth.transform.AttachToParent(this.HorizontalLayout.transform, 0);
            InitLine.transform.AttachToParent(this.HorizontalLayout.transform, 0);

            Timer.Instance.StartCoroutine(InitializeComplete());
        }

        private IEnumerator InitializeComplete()
        {
            while (LogIsInitialized == null)
            {
                yield return null;
            }
            LogIsInitialized.Invoke();
        }
    }



    public class GameLogMediator : Mediator
    {
        [Inject] public GameLogView View { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public ViewReferences ViewReferences {get;set;}
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal{get;set;}
        [Inject] public ShowTwoTouchSignal ShowTwoTouchSignal{get;set;}
        [Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ExportGameLogSignal ExportGameLogSignal { get; set; }
		[Inject] public AddFriendSignal AddFriendSignal{get;set;}

        private TwoTouchOverlayView twoTouchOverlay;

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.DialogDismissed += OnDialogDismissed;
            this.View.DeleteLog += OnDeleteLog;
            this.View.AddFriend += OnAddFriend;
            this.View.ShowTwoTouch += OnShowTwoTouch;
            this.View.LogIsInitialized += OnLogInitialized;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.View.AddFriendButton.gameObject.SetActive(false);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.DialogDismissed -= OnDialogDismissed;
            this.View.DeleteLog -= OnDeleteLog;
            this.View.AddFriend -= OnAddFriend;
            this.View.ShowTwoTouch -= OnShowTwoTouch;
            this.View.LogIsInitialized -= OnLogInitialized;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
               
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
                // Unblur on close
                //this.NovaContext.BlurMainCanvas(false, this.View);
            }
        }

        private void OnLogInitialized()
        {
            this.View.AddFriendButton.gameObject.SetActive(this.View.OpponentPlayerCard.PlayerData != null
                && !string.IsNullOrEmpty(this.View.OpponentPlayerCard.PlayerData.playFabId)
                && !this.PlayerProfile.FriendListData.ContainsKey(this.View.OpponentPlayerCard.PlayerData.playFabId));
        }

        private void OnShowTwoTouch(int cardId, RectTransform sourceTransform)
        {
            if(this.twoTouchOverlay == null)
            {
                Debug.Log("#GameLogView# Instantiating TwoTouchOverlayView");
                this.twoTouchOverlay = (TwoTouchOverlayView) this.ViewReferences.Get(typeof(TwoTouchOverlayView));
            }
        
            this.ShowTwoTouchSignal.Dispatch(new TwoTouchParams() {CardId=cardId,DestCardScale=1.2f,SourceTransform=sourceTransform,IsEditable=false,IsFullScrim=true,IsBlurred=false });
        }

        private void OnDeleteLog(int logNumber)
        {
            this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(I2.Loc.ScriptLocalization.DeleteLog,
                I2.Loc.ScriptLocalization.DeleteLogCantBeUndone, OnConfirmDialogDismissed, false));
        }

        private void OnConfirmDialogDismissed(bool isConfirmed)
        {
            if(isConfirmed)
            {
                if (this.View.LogNumber > 0 && this.View.LogNumber < this.PlayerProfile.GameLogs.Count)
                    this.PlayerProfile.GameLogs.RemoveAt(this.View.LogNumber);
                this.ExportGameLogSignal.Dispatch();
                this.CloseAnimatedViewSignal.Dispatch(typeof(GameLogView));
            }
        }

        private void OnDialogDismissed()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(GameLogView));
        }
        
        private void OnAddFriend()
        {
            if(this.View.OpponentPlayerCard.PlayerData != null
                && !string.IsNullOrEmpty(this.View.OpponentPlayerCard.PlayerData.playFabId)
                && !this.PlayerProfile.FriendListData.ContainsKey(this.View.OpponentPlayerCard.PlayerData.playFabId))
            {
                this.AddFriendSignal.Dispatch(this.View.OpponentPlayerCard.PlayerData.playFabId, true);
                this.View.AddFriendButton.interactable = false;
            }
        }
    }
}
