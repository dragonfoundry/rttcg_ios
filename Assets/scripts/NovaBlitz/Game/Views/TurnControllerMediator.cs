﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using Messages;
using System;
using DragonFoundry.GameState;
using strange.extensions.signal.impl;

public class DoneWithTurnClickedSignal : Signal {}

public class TurnControllerMediator : Mediator
{
    [Inject] public TurnControllerView View {get;set;}
    [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}
    [Inject] public SubmitGameActionSignal SubmitGameActionSignal {get;set;}
    [Inject] public LegalAbilitiesUpdatedSignal LegalAbilitiesUpdatedSignal {get;set;}
    [Inject] public GameControllerService GameControllerService {get;set;}
    [Inject] public DoneWithTurnClickedSignal DoneWithTurnClickedSignal {get;set;}
	
    override public void OnRegister()
    {
        this.DiffGameStateMessageSignal.AddListener(this.OnStateUpdate);
        this.LegalAbilitiesUpdatedSignal.AddListener(this.OnLegalAbilitiesUpdated);
        this.View.DoneButtonClicked += this.OnDoneButtonClicked;
        this.View.PauseButtonClicked += this.OnPauseButtonClicked;
    }
    
    override public void OnRemove()
    {
        this.DiffGameStateMessageSignal.RemoveListener(this.OnStateUpdate);
        this.LegalAbilitiesUpdatedSignal.RemoveListener(this.OnLegalAbilitiesUpdated);
        this.View.DoneButtonClicked -= this.OnDoneButtonClicked;
        this.View.PauseButtonClicked -= this.OnPauseButtonClicked;
    }

    private void OnLegalAbilitiesUpdated(GameStateReadOnly gameState)
    {
        this.View.UpdateDoneButton(gameState);
        this.View.UpdatePauseButton(gameState);
    }

    private void OnDoneButtonClicked()
    {
        this.GameControllerService.SubmitDoneWithPhaseAction();
        this.DoneWithTurnClickedSignal.Dispatch();
    }

    private void OnPauseButtonClicked()
    {
        this.GameControllerService.SubmitPauseUnpauseAction();
    }

    private void OnStateUpdate(DiffGameStateMessage diffMessage)
    {
        if (diffMessage.SomethingHasHappened)
        {
            TurnState turn = new TurnState();
            turn.SetState(diffMessage);
            this.View.UpdateBars(turn);
        }
    }
}
