﻿using System;
using System.Collections;
using NovaBlitz.UI;
using DG.Tweening;
using strange.extensions.mediation.impl;
using Messages;
using TMPro;
using UnityEngine;

namespace NovaBlitz.Game
{
    public class StartGameDialogView : AnimatedView
    {
        public TextMeshProUGUI FormatText;
        public GameObject PlayerProfileParent;
        public GameObject OpponentProfileParent;
        public GameObject VersusParent;
        public PlayerCardView PlayerCardView;
        public PlayerCardView OpponentCardView;
        public NovaAudioPlayer StartGameSound;
    }

    public class StartGameDialogMediator : Mediator
    {
        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }

        [Inject] public StartGameDialogView View { get; set; }
        [Inject] public UIConfiguration UIConfiguration { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
        [Inject] public StartGameReadySignal StartGameReadySignal { get; set; }
        [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.StartGameReadySignal.AddListener(this.OnReadyToStartGame);
            this.DiffGameStateMessageSignal.AddListener(this.OnDiffGameStateMessage);
            InitializeView();
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.StartGameReadySignal.RemoveListener(this.OnReadyToStartGame);
            this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGameStateMessage);
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
                Debug.Log("StartGameDialogClosed");
            }
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
                InitializeView();
            }
            else if (viewType == typeof(StartingHandView))
            {
                // Make sure we go to the overlay layer with the TwoTouch
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
            }
        }

        public void InitializeView()
        {
            Timer.GameInstance.StartCoroutine(InitializeCoroutine());
        }

        public IEnumerator InitializeCoroutine()
        {
            int i = 0;
            while(View == null && i < 50)
            {
                yield return null;
            }

            try
            {
                this.View.StartGameSound.Play();
                var width = (this.View.transform as RectTransform).rect.width;
                //var overshoot = width * 0.05f;
                InitializePlayer(false);
                InitializePlayer(true);
                this.View.VersusParent.SetActive(false);
                this.View.PlayerProfileParent.SetActive(false);
                this.View.OpponentProfileParent.SetActive(false);
                Sequence seq = DOTween.Sequence();
                seq.AppendInterval(1.0f);
                seq.Insert(1.0f, this.View.PlayerProfileParent.transform.DOLocalMove(new Vector3(-width / 2, -100, 0), 0.4f).From().SetEase(Ease.OutBack));
                seq.InsertCallback(1.0f, () => { this.View.PlayerProfileParent.SetActive(true); });
                //seq.Insert(1.25f, this.View.PlayerProfileParent.transform.DOShakeRotation(1.0f, new Vector3(25, 0, 0), 1, 0, true));
                seq.InsertCallback(1.3f, () =>
                {
                    this.View.VersusParent.SetActive(true);
                    this.View.VersusParent.transform.DOPunchScale(Vector3.one * 0.4f, 0.4f, vibrato: 1, elasticity: 0.25f).From();
                });
                seq.Insert(1.0f, this.View.OpponentProfileParent.transform.DOLocalMove(new Vector3(width / 2, -100, 0), 0.4f).From().SetEase(Ease.OutBack));
                seq.InsertCallback(1.0f, () => { this.View.OpponentProfileParent.SetActive(true); });
                //seq.Insert(1.25f, this.View.OpponentProfileParent.transform.DOShakeRotation(1.0f, new Vector3(25, 0, 0), 1, 0, true));


                if (GameData.CurrentGameData.GameFormat == GameFormat.Tutorial)
                {
                    this.View.FormatText.text = I2.Loc.ScriptLocalization.Challenge;// GameData.CurrentGameData.OpponentName;
                }
                else
                {
                    string formatName;
                    TournamentUtils.TournamentFormatNames.TryGetValue(GameData.CurrentGameData.GameFormat, out formatName);
                    this.View.FormatText.text = formatName;
                }
            }
            catch (Exception e)
            {
                Debug.LogFormat("StartGameDialogView.InitializeView - View:{0} GameData:{1} GameData.CurrentGameData:{2} waitIterations:{3}", 
                    View == null ? "null" : "ok", GameData == null ? "null" : "ok", GameData == null || GameData.CurrentGameData == null ? "null" : "ok", i);
                MetricsTrackingService.SavedException = string.Format("[CAUGHT] InitializeView - exception: {0}", e.ToString());
            }
            this.StartGameReadySignal.RemoveListener(this.OnReadyToStartGame);
            this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGameStateMessage);
            this.StartGameReadySignal.AddListener(this.OnReadyToStartGame);
            this.DiffGameStateMessageSignal.AddListener(this.OnDiffGameStateMessage);
        }

        public void InitializePlayer(bool isOpponent)
        {
            CurrentGameData currentGameData = GameData.CurrentGameData;
            
            var playerCard = isOpponent ? this.View.OpponentCardView : this.View.PlayerCardView;
            if (playerCard == null)
            {
                playerCard = GameObject.Instantiate(this.UIConfiguration.PlayerCardPrefab);
                if (isOpponent)
                    this.View.OpponentCardView = playerCard;
                else
                    this.View.PlayerCardView = playerCard;
                playerCard.transform.SetParent(isOpponent ? this.View.OpponentProfileParent.transform : this.View.PlayerProfileParent.transform, false);
                playerCard.StatsPanel.gameObject.SetActive(!isOpponent || (currentGameData.GameFormat != GameFormat.Tutorial && currentGameData.GameFormat != GameFormat.Practice));
                playerCard.ButtonArea.gameObject.SetActive(false);
                playerCard.PlayStatusPanel.SetActive(false);
                playerCard.QuestPanel.SetActive(false);
                playerCard.VerticalStatusPanel.SetActive(false);
                // Move the card if it's the opponent
                if (isOpponent)
                {
                    playerCard.CardBack.rectTransform.Rotate(0, 0, -30);
                    playerCard.CardBack.rectTransform.anchoredPosition = new Vector2(-playerCard.CardBack.rectTransform.anchoredPosition.x, playerCard.CardBack.rectTransform.anchoredPosition.y);
                }
            }
            playerCard.Initialize(isOpponent ? currentGameData.OppPlayerData : currentGameData.PlayerPlayerData);
        }

        private void OnReadyToStartGame(ReadyToStartGameResponse request)
        {
            OpenStartGameView();
        }

        private void OnDiffGameStateMessage(DiffGameStateMessage message)
        {
            OpenStartGameView();
        }

        private void OpenStartGameView()
        {
            if (_OpenStartingHandCoroutine == null)
            {
                _OpenStartingHandCoroutine = OpenStartingHandCoroutine();
                Timer.GameInstance.StartCoroutine(_OpenStartingHandCoroutine);
            }
        }

        private System.Collections.IEnumerator _OpenStartingHandCoroutine;
        private System.Collections.IEnumerator OpenStartingHandCoroutine()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(StartGameDialogView));
            this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
            this.StartGameReadySignal.RemoveListener(this.OnReadyToStartGame);
            this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGameStateMessage);
            yield return new WaitForSecondsRealtime(0.5f);
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(StartingHandView));
            _OpenStartingHandCoroutine = null;
        }
    }
}
