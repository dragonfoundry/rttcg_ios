﻿using UnityEngine;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using DragonFoundry.Controllers;
using DragonFoundry.Networking;
using DragonFoundry.GameState;
using Messages;
using System;

namespace NovaBlitz.Game
{
    public class InGameMenuMediator : Mediator
    {
        [Inject] public InGameMenuView View { get; set; }
        [Inject] public ConcedeGameSignal ConcedeGameSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.DoneClicked += this.OnDoneClicked;
            this.View.ConcedeClicked += this.OnConcedeClicked;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.DoneClicked -= this.OnDoneClicked;
            this.View.ConcedeClicked -= this.OnConcedeClicked;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
            
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                // Were we closed?
            }
        }

        private void OnDoneClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(InGameMenuView));
        }

        private void OnConcedeClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(InGameMenuView));
            this.ConcedeGameSignal.Dispatch();
        }
    }
}
