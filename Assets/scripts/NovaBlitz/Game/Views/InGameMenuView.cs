﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using NovaBlitz.UI;
using DragonFoundry.Fx;
using DragonFoundry.Controllers;
using DragonFoundry.Controllers.Display;
using LinqTools;
using DragonFoundry.Networking;
using Messages;

namespace NovaBlitz.Game
{
    public class InGameMenuView : AnimatedView
    {
        public System.Action DoneClicked;
        public System.Action ConcedeClicked;
        

        public void OnClickConcede()
        {
            Debug.Log("Conceded from the game");
            if (this.ConcedeClicked != null)
            {
                this.ConcedeClicked.Invoke();
            }
        }

        public void OnClickDone()
        {
            Debug.Log("On Click done");
            if (this.DoneClicked != null)
            {
                this.DoneClicked.Invoke();
            }
        }
    }
}
