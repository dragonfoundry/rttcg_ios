﻿using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using System;
using System.Collections;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
    public enum ReportType
    {
        NoType,
        ReportBug,
        Feedback,
        RateGameFeedback,
        ReportPlayerGlobal,
        ReportPlayerPrivate,
        Purchasing,
        Exception,
    }

    public class ReportBugView : AnimatedView
    {
        public Action<bool> DialogDismissed;

        public TextMeshProUGUI PanelTitle;
        public CanvasGroup ViewCanvas;
        public Button Button;
        public TMP_InputField EmailInput;
        public TMP_InputField SubjectInput;
        public TMP_InputField DescriptionInput;
        public TextMeshProUGUI LengthHint;

        private int MinDescriptionLength = 1;
        public bool isSubmitting { get; set; }
        public bool _IsNoEmail { get; set; }

        public bool IsComplete { get { return IsSubjectComplete && IsDescriptionComplete && IsEmailComplete; } }
        private bool IsEmailComplete { get { return !_IsNoEmail || (this.EmailInput.text.Contains("@") && this.EmailInput.text.Contains(".")); } }
        private bool IsDescriptionComplete {  get { return !string.IsNullOrEmpty(DescriptionInput.text) && DescriptionInput.text.Length >= MinDescriptionLength; } }
        private bool IsSubjectComplete { get { return !string.IsNullOrEmpty(SubjectInput.text); } }
        
        public string Subject { get { return this.SubjectInput.text.Replace("\t", string.Empty).Trim(); } }
        public string Email { get { return this.EmailInput.text.Replace("\t", string.Empty).Trim(); } }
        public string Description { get { return this.DescriptionInput.text.Replace("\t", string.Empty).Trim(); } }
        
        public ReportType CurrentReportType { get; set; }
        public ChatMessage MessageToReport { get; set; }
        public List<ChatMessage> ChatList = new List<ChatMessage>();

        public void Initialize(ReportType type, ChatMessage message, List<ChatMessage> chatList)
        {
            CurrentReportType = type;
            MessageToReport = message;
            // could be a 

            this.ClearText();
            if(chatList != null)
                ChatList.AddRange(chatList);

            switch (type)
            {
				case ReportType.ReportPlayerGlobal:
				case ReportType.ReportPlayerPrivate:
					PanelTitle.text = I2.Loc.ScriptLocalization.Report_Player;
					SubjectInput.text = string.Format ("{0}: {1}", I2.Loc.ScriptLocalization.Report_Player, message.SenderDisplayName);
					LengthHint.text = I2.Loc.ScriptLocalization.Report_Player;
                    (DescriptionInput.placeholder as TextMeshProUGUI).text = I2.Loc.ScriptLocalization.Report_Player_Hint;
                    break;
                case ReportType.ReportBug:
                    PanelTitle.text = I2.Loc.ScriptLocalization.Report_Bug;
					(DescriptionInput.placeholder as TextMeshProUGUI).text = I2.Loc.ScriptLocalization.Enter_Feedback_Hint;
					LengthHint.text = I2.Loc.ScriptLocalization.Report_Bug;
                    break;
                case ReportType.Feedback:
                case ReportType.RateGameFeedback:
                    PanelTitle.text = I2.Loc.ScriptLocalization.Feedback;
                    (DescriptionInput.placeholder as TextMeshProUGUI).text = I2.Loc.ScriptLocalization.Enter_Feedback_Hint;
                    LengthHint.text = I2.Loc.ScriptLocalization.Send_Feedback;
                    break;
            }
            // Su

            // this should log a ZenDesk ticket, including the current chat history and the offending message. 
            // Should be a text entry panel to give the information, so people have to actually tell us what's up.
        }

        public void OnEndEditEmail()
        {
            if ((Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !EmailInput.wasCanceled)) && IsEmailComplete)
            {
                SubjectInput.ActivateInputField();
                SubjectInput.Select();
            }
        }
        
        public void OnEndEditSubject()
        {
            if ((Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !SubjectInput.wasCanceled)) && IsSubjectComplete)
            {
                DescriptionInput.ActivateInputField();
                DescriptionInput.Select();
            }
        }

        public void OnEndEditDescription()
        {
            if ((Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !SubjectInput.wasCanceled)) && IsComplete)
            {
                OnClickDone();
            }
        }

        public void OnClickClose()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(false);
            }
        }


        public void OnClickDone()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(true);
            }
        }

        void LateUpdate()
        {
            if(this.IsOpen && !isSubmitting)
            {
                this.Button.interactable = IsComplete;
            }
        }

        public void ClearText()
        {
            EmailInput.text = string.Empty;
            SubjectInput.text = string.Empty;
            DescriptionInput.text = string.Empty;
            this.Button.interactable = false;
            ChatList.Clear();
        }
    }


    public class ReportBugMediator : Mediator
    {
        [Inject] public ReportBugView View { get; set; }
        [Inject] public ZendeskTicketSignal ZendeskTicketSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.DialogDismissed += OnDialogDismissed;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            //this.View.ClearText();
            //OnOpenAnimatedView(this.View.GetType());
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.DialogDismissed -= OnDialogDismissed;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
                this.View.ViewCanvas.interactable = true;
                if (this.PlayerProfile == null 
                    || PlayerProfile.LoginInfoResult == null
                    || PlayerProfile.LoginInfoResult.AccountInfo == null
                    || PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo == null
                    || string.IsNullOrEmpty(this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo.Email))
                    this.View._IsNoEmail = true;
                this.View.EmailInput.gameObject.SetActive(this.View._IsNoEmail);
                Timer.Instance.StartCoroutine(DelaySelectEntryField());
            }
        }

        private IEnumerator DelaySelectEntryField()
        {
            yield return new WaitForSecondsRealtime(0.1f);
            if (this.View._IsNoEmail)
            {
                this.View.EmailInput.ActivateInputField();
                this.View.EmailInput.Select();
            }
            else if(this.View.CurrentReportType == ReportType.ReportBug || this.View.CurrentReportType == ReportType.Feedback || this.View.CurrentReportType == ReportType.RateGameFeedback)
            {
                this.View.SubjectInput.ActivateInputField();
                this.View.SubjectInput.Select();
            }
            else if (this.View.CurrentReportType == ReportType.ReportPlayerGlobal || this.View.CurrentReportType == ReportType.ReportPlayerPrivate)
            {
                this.View.DescriptionInput.ActivateInputField();
                this.View.DescriptionInput.Select();
            }
        }


        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
                this.View.ClearText();
            }
        }

        private void OnDialogDismissed(bool isSubmit)
        {
            if (isSubmit && this.View.IsComplete)
            {
                Timer.Instance.StartCoroutine(SubmitTicketCoroutine());
            }
            else
            {
                CloseView();
            }
        }

        private IEnumerator SubmitTicketCoroutine()
        {
            var ticket = new ZendeskTicket
            {
                ContactType = View.CurrentReportType == ReportType.ReportPlayerGlobal 
                    ? "globalchat" 
                    : View.CurrentReportType == ReportType.ReportPlayerPrivate ? "privatechat" : "bug",
                Description = (View.CurrentReportType == ReportType.ReportPlayerGlobal || View.CurrentReportType == ReportType.ReportPlayerPrivate) && this.View.MessageToReport != null
                    ? string.Format("{0}\n\nReported Message: {1}", this.View.MessageToReport.Message, this.View.Description) 
                    : this.View.Description,
                Subject = string.Format("{0} [From: {1} ({2})]", 
                    (View.CurrentReportType == ReportType.ReportPlayerGlobal || View.CurrentReportType == ReportType.ReportPlayerPrivate) && this.View.MessageToReport != null
                        ? string.Format("{0} ({1})", this.View.Subject, View.MessageToReport.SenderPlayFabId)
                        : this.View.Subject, 
                    this.PlayerProfile.MetaProfile.Name, 
                    this.PlayerProfile.MetaProfile.PlayFabID),
                Happiness = -1,
                EmailAddress = this.View._IsNoEmail ? this.View.Email : this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo.Email
            };
            ticket.Tags.Add(this.View.CurrentReportType.ToString());
            this.View.isSubmitting = true;
            yield return null;
            this.View.Button.interactable = false;
			this.View.ViewCanvas.interactable = false;
			//this.View.EmailInput.interactable = false;
			//this.View.SubjectInput.interactable = false;
			//this.View.DescriptionInput.interactable = false;
            this.View.LengthHint.text = I2.Loc.ScriptLocalization.Submitting_Bug;
            //this.View.LengthHint.gameObject.SetActive(true);
            yield return null;
            yield return null;
            //yield return new WaitForSecondsRealtime(10);
            this.ZendeskTicketSignal.Dispatch(ticket, this.View.CurrentReportType, this.View.ChatList);
			yield return new WaitForSecondsRealtime(2);
            CloseView();
        }

        private void CloseView()
        {
            this.View.DescriptionInput.text = " ";
            this.View.SubjectInput.text = " ";
            this.View.isSubmitting = false;
            this.View.ViewCanvas.interactable = true;
            this.CloseAnimatedViewSignal.Dispatch(typeof(ReportBugView));
        }
    }
}
