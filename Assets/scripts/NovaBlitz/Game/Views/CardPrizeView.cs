﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NovaBlitz.UI;
using Messages;


namespace NovaBlitz.Game
{
    public class CardPrizeView : MonoBehaviour
    {
        [SerializeField]
        protected TextMeshProUGUI _NameDisplay;

        protected PrizeData _Data;
        public PrizeData Data
        {
            get { return _Data; }
            set { _Data = value; Refresh(); }
        }

        public virtual void Refresh()
        {
            if (null == this.Data)
                return;

            if (null != _NameDisplay)
            {

                var cardprize = (this.Data as CardPrizeData);
                if (cardprize != null && cardprize.PrizeSource == PrizeSource.lastWeeksCard || cardprize.PrizeSource == PrizeSource.thisWeeksCard)
                {
                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.CardOfTheWeek);
                }
                else
                {

                    _NameDisplay.SetText(I2.Loc.ScriptLocalization.Card_Awarded);
                }
            }
        }
    }
}
