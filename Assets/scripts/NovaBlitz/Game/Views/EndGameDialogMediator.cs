﻿using UnityEngine;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using DragonFoundry.Controllers;
using DragonFoundry.Networking;
using System;

namespace NovaBlitz.Game
{
    public class EndGameDialogMediator : Mediator
    {
		[Inject] public EndGameDialogView View {get;set;}
        //[Inject] public ChillingoService ChilingoService {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public ShareScreenShotSignal ShareScreenShotSignal { get; set; }
        [Inject] public ShowLeaderboardSignal ShowLeaderboardSignal { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }
        [Inject] public OpenPrizeSignal OpenPrizeSignal { get; set; }
        [Inject] public ZendeskTicketSignal ZendeskTicketSignal { get; set; }

        protected float _FallbackTimeout = 0.55f;

		/// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
		{
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.View.EventShareClicked += OnShareClicked;
            this.View.EventLeaderboardClicked += OnLeaderboardClicked;
            this.View.EventGameLogClicked += OnGameLogClicked;
            this.View.CloseDialogAction += OnCloseDialog;
            this.View.EventReportBugClicked += OnReportBugClicked;
#if UNITY_IOS || UNITY_ANDROID
#else
            this.View.ShareButton.gameObject.SetActive(false);
#endif
            this.View.VictoryBanner.SetActive(false);
            this.View.DefeatBanner.SetActive(false);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
		{
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.View.EventShareClicked -= OnShareClicked;
            this.View.EventLeaderboardClicked -= OnLeaderboardClicked;
            this.View.EventGameLogClicked -= OnGameLogClicked;
            this.View.CloseDialogAction -= OnCloseDialog;
            this.View.EventReportBugClicked -= OnReportBugClicked;
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType() && this.View.IsOpen )
            {
/*#if UNITY_ANDROID || UNITY_IOS
                ChilingoService.OnExitFrontEnd();
#endif*/

                this.View.gameObject.SetActive(false);
                this.OpenPrizeSignal.Dispatch(View.GameOverMessage.GrantedPrizes, PrizeViewType.GameEnd, View.GameOverMessage, View.ShowMainMenuParams);
            }
        }

        private void OnCloseDialog()
        {
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
        }

        private void OnShareClicked(ShareScreenShotParams Params)
        {
            this.ShareScreenShotSignal.Dispatch(Params);
        }
        
        protected void OnLeaderboardClicked()
        {
            this.ShowLeaderboardSignal.Dispatch(this.PlayerProfile.RatingMonth, LeaderboardType.Ranking);
        }

        protected void OnGameLogClicked()
        {
            int logNumber = this.PlayerProfile.GameLogs.Count - 1;
            var log = this.PlayerProfile.GameLogs[logNumber];
            if (log != null)
            {
                this.InitializeAnimatedViewSignal.Dispatch(typeof(GameLogView),
                    (v) =>
                    {
                        ((GameLogView)v).Initialize(log, logNumber, false);
                    });
                this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(GameLogView));
            }
        }

        protected void OnReportBugClicked()
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(ReportBugView),
                    (v) => { ((ReportBugView)v).Initialize(ReportType.ReportBug, null, null); });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ReportBugView));
            //this.ZendeskTicketSignal.Dispatch(new ZendeskTicket { ContactType = "bug", Description = "this is a bug", Happiness = -1, Subject = "WTF is this bug?" });
        }
    }
}
