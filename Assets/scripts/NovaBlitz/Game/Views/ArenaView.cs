﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using NovaBlitz.UI;
using DragonFoundry.Fx;
using DragonFoundry.Controllers;
using strange.extensions.mediation.impl;
using DragonFoundry.GameState;
using System;
using DG.Tweening;
using UnityEngine.EventSystems;

public class ArenaView : View
{
    public CanvasGroup ArenaCanvasGroup;
    public RectTransform ArenaUICanvas;
    public RectTransform CombatUICanvas;
    public TargetArrowController TargetArrowController;
    public PlayerArenaController PlayerArenaController;
    public PlayerHandController PlayerHandController;
    public OpponentHandController OpponentHandController;
    public CardDealer PlayerCardDealer;
    public CardDealer OpponentCardDealer;
    public OpponentArenaController OpponentArenaController;
    public ArenaZoomController ArenaZoomController;
    public NovaAudioPlayer GamePlayAudio;
    public GameObject ArenaBackgroundGO;
    public Material HexBackgroundMaterial;
    public Color ActionPhaseColor;
    public Color CombatPhaseColor;
    public CanvasGroup TurnPhaseCallout;
    public CanvasGroup CombatPhaseCallout;
    public RectTransform CombatMask;
    public RectTransform RightCombatCrown;
    public RectTransform LeftCombatCrown;
    public TextMeshProUGUI CombatPhaseLabel;
    public TextMeshProUGUI TurnPhaseLabel;
    public RectTransform BlockerArrowsRoot;
    public RectTransform Blinders;
    public Image EndFadeScrim;
    public RectTransform GameFxAnchor;
	//public EventTrigger PlayerTrash;
	//public EventTrigger OpponentTrash;
    public RectTransform PlayerTrashCardHolder;
	public RectTransform OpponentTrashCardHolder;
    public Sprite TrashCanOpen;
    public Sprite TrashCanClosed;
    public Image OpponentTrashIcon;
    public Image PlayerTrashIcon;
    public NovaAudioPlayer TrashCardSound;

    public TurnControllerView TimeBladeController;

	public Action ScreenResize { get; set;}
	public int width { get; set;}
	public int height {get;set;}

    public Sequence shakeScreen;

    public Action<bool> TrashClicked;
    public Action TrashHide;

    public bool isCardStatesUpdated { get; set; }
	public bool isShowingTrashForPlayer { get; set; }
	public bool isShowingTrashForOpponent { get; set; }
	private bool? _isTrueShowingTrashFalseShowingTargets;
	public bool? isTrueShowingTrashFalseShowingTargets { get { return _isTrueShowingTrashFalseShowingTargets; } 
		set { _isTrueShowingTrashFalseShowingTargets = value;
			if (value != true)
				isShowingTrashForPlayer = false;
			isShowingTrashForOpponent = false;
		} }

    protected override void Start()
    {
        base.Start();
        //this.GamePlayAudio.Play();
        this.HexBackgroundMaterial.SetColor ("_TintColor", this.ActionPhaseColor);
		this.width = Screen.width;
		this.height = Screen.height;
        this.EndFadeScrim.DOFade(0.0f, 0.0f);
        this.PlayerHandController.ArenaView = this;
        this.OpponentHandController.ArenaView = this;
        /*PlayerTrash.OnPointerClick += OnPlayerTrashClicked();
		PlayerTrash.OnPointerExit += OnTrashExit();
		OpponentTrash.OnPointerClick += OnPlayerTrashClicked();
		OpponentTrash.OnPointerExit += OnTrashExit();*/

    }

	void Update()
	{
		if(Screen.width != width || Screen.height != height)
		{
			Debug.LogFormat("Resolution change: Screen {0}/{1} to {2}/{3}",width, height, Screen.width, Screen.height);

			this.width = Screen.width;
			this.height = Screen.height;

			if(ScreenResize != null)
			{
				ScreenResize.Invoke ();
			}
		}
        if(isCardStatesUpdated)
        {
            isCardStatesUpdated = false;
            if(isTrueShowingTrashFalseShowingTargets == true && this.TrashClicked != null)
                this.TrashClicked.Invoke(isShowingTrashForPlayer);
        }
	}

    override protected void OnDestroy()
    {
        base.OnDestroy();
        if(this.ArenaBackgroundGO != null)
        {
            GameObject.Destroy(this.ArenaBackgroundGO);
        }
    }

    public void OnPlayerTrashClicked(BaseEventData eventData)
    {
        Debug.Log("Trash clicked");
		if(this.TrashClicked != null && (isTrueShowingTrashFalseShowingTargets == null && isShowingTrashForPlayer == false))
        {
            this.TrashClicked.Invoke(true);
        }
    }

    public void OnOpponentTrashClicked(BaseEventData eventData)
    {
        Debug.Log("Opponent Trash clicked");
		if (this.TrashClicked != null && (isTrueShowingTrashFalseShowingTargets == null && isShowingTrashForOpponent == false))
        {
            this.TrashClicked.Invoke(false);
        }
    }

    public void OnTrashExit(BaseEventData eventData)
    {
        if (isTrueShowingTrashFalseShowingTargets == true && this.TrashHide != null)
        {
            TrashHide.Invoke();
        }
    }

    public void ShowTurnPhase(int turnCount)
    {
        this.TurnPhaseCallout.transform.localScale = Vector3.one;
        Sequence seq = DOTween.Sequence();
        seq.Append(this.TurnPhaseCallout.DOFade(1f, 0f));
        seq.Insert(0,this.TurnPhaseCallout.transform.DOScale(0.2f,0.25f).From().SetEase(Ease.OutBack));
        seq.AppendInterval(1.5f);
        seq.Append(this.TurnPhaseCallout.DOFade(0f, 0.3f));
        seq.Join(this.TurnPhaseCallout.transform.DOScale(0.2f,0.3f).From().SetEase(Ease.InBack));
        seq.Play();
        this.TurnPhaseLabel.SetText(I2.Loc.ScriptLocalization.Turn, turnCount);
        this.TimeBladeController.BarBackground.sprite = this.TimeBladeController.ActionBackground;
    }
    
    public void ShowCombatPhase()
    {
        Vector2 sizeDelta = this.CombatMask.sizeDelta;
        float initialWidth = sizeDelta.x;
        sizeDelta.x = 150f;
        float rightCrownStart = this.RightCombatCrown.anchoredPosition.x;
        float leftCrownStart = this.LeftCombatCrown.anchoredPosition.x;
         this.CombatPhaseLabel.transform.DOScale(Vector3.zero, 0f);

        this.CombatPhaseLabel.transform.DOScale(Vector3.one * 0.25f,0);
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.3f);
        seq.Append(this.CombatPhaseCallout.DOFade(1f,0.3f).SetEase(Ease.InBack));
        seq.Join(this.CombatPhaseCallout.transform.DOScale(0.2f,0.25f).From().SetEase(Ease.OutBack));
        seq.Insert(0.25f, this.CombatMask.DOSizeDelta(sizeDelta, 0.45f).From());
        seq.Insert(0.5f, this.RightCombatCrown.DOAnchorPosX(1200, 0.3f).From());
        seq.Insert(0.5f, this.LeftCombatCrown.DOAnchorPosX(-1200, 0.3f).From());
        seq.Insert(0.3f, this.CombatPhaseLabel.transform.DOScale(Vector3.one * 1.35f, 0.5f).SetEase(Ease.OutQuad));
        seq.Insert(0.8f,  this.CombatPhaseLabel.transform.DOScale(Vector3.one * 1.15f, 0.2f).SetEase(Ease.OutQuad));
        seq.AppendInterval(0.6f);
        seq.Append(this.CombatPhaseCallout.DOFade(0f, 0.3f));
        seq.Join(this.RightCombatCrown.DOAnchorPosX(1200, 0.3f));
        seq.Join(this.LeftCombatCrown.DOAnchorPosX(-1200, 0.3f));
        seq.Join(this.CombatMask.DOSizeDelta(sizeDelta, 0.3f));
        seq.OnComplete(()=>{
            sizeDelta.x = initialWidth;
            this.CombatMask.DOSizeDelta(sizeDelta, 0f);
            this.RightCombatCrown.DOAnchorPosX(rightCrownStart, 0f);
            this.LeftCombatCrown.DOAnchorPosX(leftCrownStart, 0f);
        });
        seq.Play();
        this.TimeBladeController.BarBackground.sprite = this.TimeBladeController.CombatBackground;
    }
    
    public void FadeToColor(Color color)
    {
        this.StartCoroutine(FadeToColor(this.HexBackgroundMaterial.GetColor("_TintColor"), color, 0.5f));
    }
    
    private IEnumerator FadeToColor(Color currentColor, Color targetColor, float duration)
    {
        float currentTime = duration;
        
        do
        {
            Color tintColor = Color.Lerp(targetColor, currentColor, currentTime / duration);
            this.HexBackgroundMaterial.SetColor ("_TintColor", tintColor);
            currentTime -= Time.deltaTime; 
            yield return null;
        } while (currentTime > 0f);
        
        this.HexBackgroundMaterial.SetColor ("_TintColor", targetColor);
    }
}
