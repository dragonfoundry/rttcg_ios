﻿using strange.extensions.signal.impl;
using Messages;

namespace NovaBlitz.Game
{
    public class SelfInputActionSignal : Signal<Messages.InputAction>
    {
    }
}