using Messages;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using UnityEngine;
using LinqTools;
using NovaBlitz.UI;

namespace NovaBlitz.Game
{
	public class GameOverSignal : Signal<GameOverMessage> {}
    public class CleanUpGameCommand : Command
    {
        [Inject] public INovaContext SessionContext { get; set; }

        public override void Execute()
        {

            Timer.GameInstance.StopAllCoroutines();
            var pmbs = SessionContext.MainCanvas.GetComponentsInChildren<PoolableMonoBehaviour>().Where(pmb => pmb.transform.parent == SessionContext.MainCanvas.transform).ToList();
            Debug.LogFormat("found {0} pmbs", pmbs.Count());
            foreach (var pmb in pmbs)
            {
                Debug.LogFormat("recycling pmb: {0}", pmb.PooledObjectType);
                pmb.Recycle();

            }
        }
    }
}
