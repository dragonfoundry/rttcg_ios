﻿using strange.extensions.signal.impl;
using Messages;

namespace NovaBlitz.UI
{
    public class CardTwoTouchOpenedSignal : Signal<CardData>
    {
    }
}
