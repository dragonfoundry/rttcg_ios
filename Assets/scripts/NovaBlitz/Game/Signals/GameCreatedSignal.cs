﻿using Messages;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
    public class GameCreatedSignal : Signal<CreateGameResponse>
    {
    }
}
