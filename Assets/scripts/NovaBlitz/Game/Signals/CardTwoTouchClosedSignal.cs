﻿using strange.extensions.signal.impl;
using Messages;

namespace NovaBlitz.UI
{
    public class CardTwoTouchClosedSignal : Signal<CardData>
    {
    }
}
