﻿using strange.extensions.signal.impl;
using Messages;

namespace NovaBlitz.Game
{
    public class OpponentInputActionSignal : Signal<Messages.InputAction>
    {
    }
}
