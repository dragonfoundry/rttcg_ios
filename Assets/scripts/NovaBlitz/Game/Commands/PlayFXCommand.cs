﻿using UnityEngine;
using System.IO;
using System;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI;
using DG.Tweening;
using Messages;

namespace NovaBlitz.Game
{

    public class PlayFxParams
    {
        public const string ENERGY_FX_TARGET = "_e";
        public const string HEALTH_FX_TARGET = "_h";

        public readonly RectTransform Source;
        public readonly RectTransform Destination;
        public readonly string PathFxName;
        public readonly string EndFxName;
        public PlayFxPath Path;
        public float DurationS;
        public int SourceId;
        public int DestId;

        public PlayFxParams(int sourceId, int destId, RectTransform source, RectTransform dest, string pathFxName, string endFxName, PlayFxPath path, int durationMs)
        {
            Source = source;
            SourceId = sourceId;
            Destination = dest;
            DestId = destId;
            PathFxName = pathFxName;
            EndFxName = endFxName;
            Path = path;
            DurationS = durationMs /1000f;

            // testing values = deleteme
            //Path = PlayFxPath.ArcWiggle;
            //DurationS = 600f/1000f;
            //PathFxName = "trail_fire";
            /*
            "trail_fire";
            "trail_vines";
            "trail_stars";
            "trail_sparks";
            "trail_smoke";
            "trail_energy";
            "trail_rings";
            "trail_lightning";
            "trail_holy";
            "trail_arcane";
            "trail_magic";
            "trail_goldorbs";
            "trail_evil";

            "trail_laser";
            */
        }
    }

    public class PlayFxSignal : Signal<PlayFxParams> { }
    public class PlayFxCommand : Command
    {
        [Inject] public PlayFxParams Params { get; set; }
        [Inject] public UIConfiguration UIConfiguration { get; set; }
        [Inject] public INovaContext ContextView { get; set; }

        public override void Execute()
        {
            this.Retain();
            Timer.GameInstance.StartCoroutine(TriggerFxCoroutine());
        }

        private IEnumerator TriggerFxCoroutine()
        {
            FxTrailTrigger fxTrailTrigger = null;
            // Play trail FX, if any (needs start, destination, path, and an FX name)
            if (Params.SourceId != Params.DestId && Params.Source != null && Params.Destination != null && Params.Path != PlayFxPath.NoPath && !string.IsNullOrEmpty(Params.PathFxName))
            {

                int j = 0;
                while (Params.Source != null && Params.Destination != null 
                    && (Params.Source.position.x > 5000 || Params.Destination.position.x > 5000) 
                    && j < 150)
                {
                    yield return null;
                    j++;
                }
                if (j > 20)
                    Debug.LogFormat("#TrailFX# Waited {0} frames", j);
                if(j == 150)
                {
                    MetricsTrackingService.SavedException = string.Format("Waited too long for FX {0} from {1}({2}) to {3}({4})", 
                        Params.PathFxName, 
                        Params.SourceId,
                        Params.Source == null ? "null" : Params.Source.position.ToString(),
                        Params.DestId,
                        Params.Destination == null ? "null" : Params.Destination.position.ToString());
                }

                if (Params.Source != null && Params.Destination != null)
                {
                    if (Params.PathFxName != "trail_none")
                    {
                        //Debug.LogFormat("#TrailFX# sequencing dur {0}s at {1}:{2}", Params.DurationS, DateTime.UtcNow.ToLongTimeString(), DateTime.UtcNow.Millisecond);
                        fxTrailTrigger = UIConfiguration.PooledTrailFxPrefab.Spawn();
                        fxTrailTrigger.gameObject.SetActive(true);
                        fxTrailTrigger.transform.AttachToParent(ContextView.MainCanvas.transform, 0);
                        fxTrailTrigger.Initialize(Params, ContextView);

                        // play 
                        fxTrailTrigger.Trigger(Params.PathFxName);
                    }

                    // wait for the duration
                    yield return new WaitForSecondsRealtime(Params.DurationS);
                }
            }

            // play end FX, if any (this will likely play slightly early, given additional waits/yields in the trail FX code; that's ok
            if (!string.IsNullOrEmpty(Params.EndFxName))
            {
                if (Params.Destination != null || !string.IsNullOrEmpty(Params.EndFxName) && Params.Destination != null)
                {
                    int j = 0;
                    while (Params.Destination != null
                        && (Params.Destination.position.x > 5000)
                        && j < 15)
                    {
                        yield return null;
                        j++;
                    }

                    //Debug.LogFormat("Triggering destination FX script {0}", Params.EndFxName);
                    FxTrigger fxTrigger = UIConfiguration.PooledFxPrefab.Spawn();
                    fxTrigger.targetTransform = Params.Destination;
                    fxTrigger.gameObject.SetActive(true);
                    fxTrigger.transform.AttachToParent(ContextView.MainCanvas.transform, 0);
                    fxTrigger.Initialize(ContextView);

                    yield return null;

                    fxTrigger.Trigger(Params.EndFxName);
                }
            }

            // clean up trail particles, if any, after the duration is over 
            // destination FX clean themselves up
            if (fxTrailTrigger != null)
            {
                yield return new WaitForSecondsRealtime(0.25f);
                if (fxTrailTrigger != null)
                {
                    fxTrailTrigger.RecycleFx();
                }
            }
            this.Release();
        }
    }
}
