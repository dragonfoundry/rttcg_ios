﻿using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI;
using Newtonsoft.Json;

namespace NovaBlitz.Game
{
    public class GameLogsExportedSignal : Signal { }
    public class ExportGameLogSignal : Signal { }
    public class ExportGameLogCommand : Command
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GameLogsExportedSignal GameLogsExportedSignal { get; set; }

        public override void Execute()
        {
            // Add to the player profile
            try
            {
                string logfile = JsonConvert.SerializeObject(this.PlayerProfile.GameLogs, 
                    Formatting.Indented, 
                    new JsonSerializerSettings {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        Converters = new List<JsonConverter> { new Newtonsoft.Json.Converters.StringEnumConverter() } });
                var directory = Path.Combine(Application.persistentDataPath, this.PlayerProfile.MetaProfile.PlayFabID);
                Directory.CreateDirectory(directory);
                File.WriteAllText(Path.Combine(directory, NovaConfig.GAME_LOG_FILE_NAME), logfile);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.ToString());
            }
            this.GameLogsExportedSignal.Dispatch();
            // format: Date/Time, Format
            // OR, export the full list from the profile?

            // to a PlayFabId/gamelogs file
            // in Json? Protobuf?

        }
    }
}
