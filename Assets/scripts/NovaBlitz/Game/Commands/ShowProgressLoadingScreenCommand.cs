﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using Messages;

namespace NovaBlitz.UI
{
    public class ShowProgressLoadingScreenCommand : Command
    {
        [Inject] public InitializeProgressScreenSignal InitializeProgressScreenSignal { get; set; }
        [Inject] public AwaitGameStartOrCancelSignal AwaitGameStartOrCancelSignal {get;set;}
        [Inject] public ViewStateStack ViewStateStack {get;set;}
        [Inject] public ClientService ClientService { get; set; }
       
        public override void Execute()
        {
            if(ClientService.IsInGame == true)
            {
                Debug.LogError("Already in game; stop moving into the game");
                this.Fail();
                return;
            }
            this.Retain();

            ClientService.IsInGame = true;
            Dictionary<AsyncProgress, string> progressLabels = new Dictionary<AsyncProgress, string>();
            progressLabels[AsyncProgress.NoProgress] = I2.Loc.ScriptLocalization.AsyncProgress.NoProgress;
            progressLabels[AsyncProgress.ObjectPool] = I2.Loc.ScriptLocalization.AsyncProgress.ObjectPool;
            progressLabels[AsyncProgress.CreateGameRequest] = I2.Loc.ScriptLocalization.AsyncProgress.CreateGameRequest;
            progressLabels[AsyncProgress.JoinGameRequest] = I2.Loc.ScriptLocalization.AsyncProgress.JoinGameRequest;
            progressLabels[AsyncProgress.ReadyToStartGameRequest] = I2.Loc.ScriptLocalization.AsyncProgress.ReadyToStartGameRequest;

            this.InitializeProgressScreenSignal.Dispatch(progressLabels);
            this.AwaitGameStartOrCancelSignal.Dispatch();
            Timer.Instance.StartCoroutine(this.WaitForOpenAnimation(0.5f));

            //
            this.ViewStateStack.Clear();
        }

        private IEnumerator WaitForOpenAnimation(float duration)
        {
            yield return new WaitForSecondsRealtime(duration);
            this.Release();
        } 
    }
}

