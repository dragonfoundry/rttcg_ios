using UnityEngine;
using System;
using System.Text;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using Messages;
using strange.extensions.command.impl;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;


namespace NovaBlitz.Game
{
	public class ShowGameOverUICommand : Command
	{
		[Inject] public GameOverMessage GameOverMessage {get;set;}
        
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        [Inject] public UIConfiguration UIConfiguration { get; set; }

        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        
        [Inject] public SelfEmoteSignal SelfEmoteSignal { get; set; }
        [Inject] public OpponentEmoteSignal OpponentEmoteSignal { get; set; }
        [Inject] public OpenPrizeSignal OpenPrizeSignal { get; set; }
        [Inject] public LogGameOverDataSignal LogGameOverDataSignal { get; set; }
		[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

        public IEnumerator WaitForPrizeDialog { get; set; }

        public override void Execute()
		{
            Debug.LogFormat("Game over message. Won?{0}:{1} prizes:{2}. Format:{3} Daily:{4} JustRewards:{5} GameGuid:{6}", 
                GameOverMessage.HasWon,
                GameOverMessage.GameEndReason,
                GameOverMessage.GrantedPrizes == null 
                    ? "none" 
                    : string.Format("{0}:[{1}]",
                        GameOverMessage.GrantedPrizes.Count, String.Join(",", GameOverMessage.GrantedPrizes.Select(p => string.Format("{0}:{1}", p.CurrencyCode != CurrencyType.xx ? p.CurrencyCode.ToString(): p.PrizeId, p.Quantity)).ToArray())), 
                GameOverMessage.GameFormat,
                GameOverMessage.IsDailyRewards,
                GameOverMessage.IsJustRewards,
                GameOverMessage.GameGuid);
            this.Retain();
            if (WaitForPrizeDialog != null)
            {
                Timer.Instance.StopCoroutine(WaitForPrizeDialog);
            }
            WaitForPrizeDialog = this.GameOverMessage.IsJustRewards 
                ? this.WaitForPrizeDialogCoroutine(GameOverMessage) 
                : this.WaitForGameEndAnimations(GameOverMessage);
            Timer.Instance.StartCoroutine(WaitForPrizeDialog);
            // hold off on the pack opener screen somehow
            // bring up the rewards right after the progress loading screen closes (if it's open).
            this.AsyncProgressSignal.Dispatch(AsyncProgress.Complete, 1.1f);
		}
        
        public IEnumerator WaitForGameEndAnimations(GameOverMessage msg)
        {
            //yield return new WaitForSecondsRealtime(5);
            //var arena = (ArenaView)this.ViewReferences.Get(typeof(ArenaView));
            var ArenaView = UnityEngine.Object.FindObjectOfType<ArenaView>();
            if (ArenaView != null && ArenaView.isActiveAndEnabled)
            {
                if (msg.HasWon)
                {
                    this.SelfEmoteSignal.Dispatch(-1);
                    yield return new WaitForSecondsRealtime(0.5f);
                    this.OpponentEmoteSignal.Dispatch(-2);
                }
                else
                {
                    this.OpponentEmoteSignal.Dispatch(-1);
                    yield return new WaitForSecondsRealtime(0.5f);
                    this.SelfEmoteSignal.Dispatch(-2);
                }

                yield return new WaitForSecondsRealtime(4.5f);
            }
            else
                yield return null;
            /*if (msg.GameEndReason == GameEndReasons.WinLose)
            {
                yield return new WaitForSecondsRealtime(2);
            }*/
            ShowDialog(msg);
            this.Release();
        }

        public IEnumerator WaitForPrizeDialogCoroutine(GameOverMessage msg)
        {
            while(!PlayerProfile.IsAllDataLoaded)
            {
                yield return null;
            }
            //ProgressLoadingScreenView progressloadingscreen = (ProgressLoadingScreenView)this.ViewReferences.Get(typeof(ProgressLoadingScreenView));
            
            /*if (progressloadingscreen != null && progressloadingscreen.IsOpen && !msg.IsJustRewards)
            {
                this.OpenAnimatedViewSignal.Dispatch(typeof(EndGameDialogView));
                var endgame = (EndGameDialogView)ViewReferences.Get(typeof(EndGameDialogView));
            }*/
            //while (progressloadingscreen != null && progressloadingscreen.IsOpen)
            {
                yield return null;
            }
            ShowDialog(msg);
            this.Release();
        }

        protected void ShowDialog(GameOverMessage msg)
        {
            this.LogGameOverDataSignal.Dispatch(msg);

            int oldQps;
            this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.QP, out oldQps);
            int oldWins;
            this.PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.GAMES_WON, out oldWins);
            int oldWinStreak;
            this.PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.CURRENT_WIN_STREAK, out oldWinStreak);
            int oldLeaderboardPosition;
            this.PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.LEADERBOARD, out oldLeaderboardPosition);

            var oldOnboardingProgreass = UpdatePlayerData(msg);
            UpdatePlayerInventoryAndWallet(msg);

            if (msg.IsJustRewards)
            {
                //if (GameOverMessage.GrantedPrizes != null && GameOverMessage.GrantedPrizes.Count > 0)
                this.OpenPrizeSignal.Dispatch(GameOverMessage.GrantedPrizes, GameOverMessage.IsDailyRewards ? PrizeViewType.DailyRewards : PrizeViewType.GameEnd, GameOverMessage, new ShowMainMenuParams());
                return;
            }

            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(EndGameDialogView));
            var endGameDialog = this.ViewReferences.Get<EndGameDialogView>();
            if (endGameDialog == null)
                return;

            if (msg.IsTournamentOver)
            {
                PlayerProfile.LastMenuScreenViewed = typeof(HomeScreen);
            }
            int winStreak;
            this.PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.CURRENT_WIN_STREAK, out winStreak);
            int packsOwned;
            if (this.PlayerProfile.UiHintData.OpenPacks > 0)
                this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.SP, out packsOwned);
            else
                packsOwned = 0;

            endGameDialog.Initialize(msg, oldWinStreak, packsOwned);

            InitializePlayerCard(msg, endGameDialog, oldLeaderboardPosition, oldQps, oldWins, oldWinStreak, oldOnboardingProgreass);
            
            // preload ads for end game;
        }

        private OnboardingProgress UpdatePlayerData(GameOverMessage msg)
        {
            // update some player data, for safety's sake
            switch (msg.GameFormat)
            {
                case GameFormat.Draft:
                    PlayerProfile.DraftData = msg.StatusData;
                    this.ProfileUpdatedSignal.Dispatch(ProfileSection.TournamentData);
                    break;
                case GameFormat.Constructed:
                    PlayerProfile.ConstructedData = msg.StatusData;
                    this.ProfileUpdatedSignal.Dispatch(ProfileSection.TournamentData);
                    break;
                case GameFormat.Tournament:
                case GameFormat.Annual:
                case GameFormat.Monthly:
                    PlayerProfile.TournamentData = msg.StatusData;

                    this.ProfileUpdatedSignal.Dispatch(ProfileSection.TournamentData);
                    // TODO  - Add tournament wins/losses here
                    break;
                case GameFormat.Tutorial:
                    if (GameOverMessage.HasWon)
                    {

                        PlayerProfile.OnboardingProgress.CurrentTutorial++;
                        if (PlayerProfile.OnboardingProgress.CurrentTutorial > this.GameData.TutorialList.Count)
                        {
                            PlayerProfile.OnboardingProgress.TutorialsCompleted = true;
                            // Once you've gone through the tutorial, re-load everything.
                            //LoadAccountInfoSignal.Dispatch(PlayerProfile.MetaProfile.PlayFabSessionToken);
                            PlayerProfile.OnboardingProgress.CurrentTutorial = 1;
                            if (PlayerProfile.CurrentDeck != null)
                            {
                                PlayerProfile.CurrentDeck.IsTutorialDeck = false;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            var oldOnboardingProgress = PlayerProfile.OnboardingProgress;

            if (msg.OnboardingProgress != null)
                PlayerProfile.OnboardingProgress = msg.OnboardingProgress;


            if (msg.IsTournamentOver)
            {
                switch (msg.GameFormat)
                {
                    case GameFormat.Draft:
                        this.PlayerProfile.DraftDeck = null;
                        this.PlayerProfile.DraftData = new TournamentData();
                        this.PlayerProfile.DraftProgress = new DraftProgress();
                        break;
                    case GameFormat.Constructed:
                        this.PlayerProfile.LeagueDeck = null;
                        this.PlayerProfile.ConstructedData = new TournamentData();
                        break;
                    case GameFormat.Tournament:
                    case GameFormat.Monthly:
                    case GameFormat.Annual:
                        this.PlayerProfile.EventDeck = null;
                        this.PlayerProfile.CurrentTournament = null;
                        this.PlayerProfile.TournamentData = new TournamentData();
                        break;
                }
                return oldOnboardingProgress;
            }

            // Update Rank and rating
            if (!msg.IsJustRewards)
            {
                if (msg.CurrentMMR != 0)
                    PlayerProfile.UserStatistics[NBEconomy.PLAYER_STATS_RANK] = msg.CurrentMMR;
                if (msg.CurrentRating > 0)
                    PlayerProfile.UserStatistics[NBEconomy.PLAYER_STATS_RATING] = msg.CurrentRating;
            }

            // Update Achievement data
            foreach (var ach in msg.AchievementTrackValue)
            {
                PlayerProfile.UserStatistics[ach.Key] = ach.Value;
            }
            foreach (var ach in msg.Achievements)
            {
                int current;
                if(PlayerProfile.UserStatistics.TryGetValue(ach.Key, out current) && current < ach.Value)
                {
                    this.MetricsTrackingService.TrackAchievement(ach.Key, ach.Value);
                }
                PlayerProfile.UserStatistics[ach.Key] = ach.Value;
            }
            this.ProfileUpdatedSignal.Dispatch(ProfileSection.Statistics);
            return oldOnboardingProgress;
        }

        private void UpdatePlayerInventoryAndWallet(GameOverMessage msg)
        {
            foreach (var curr in msg.GrantedCurrencies)
            {
                int balance;
                PlayerProfile.Wallet.Currencies.TryGetValue(curr.Key, out balance);
                int newBalance = balance + curr.Value;
                PlayerProfile.Wallet.Currencies[curr.Key] = newBalance;
                Debug.LogFormat("Game Over. Earned {0}{1}. Balance: {2} from {3}", curr.Value, curr.Key, newBalance, balance);
            }


            // Rewards
            var grantedProducts = msg.GrantedItems.Select(gi => { NovaCatalogItem p; this.GameData.ProductCache.TryGetValue(gi, out p); return p; }).OfType<NovaCatalogItem>();
            var cardProducts = grantedProducts.Where(gp => gp.ItemClass == NBEconomy.ITEMCLASS_CARD);
            //var nonCards = grantedProducts.Where(gp => gp.ItemClass != NBEconomy.ITEMCLASS_CARD);
            var avatarProducts = grantedProducts.Where(gp => gp.ItemClass == NBEconomy.ITEMCLASS_AVATAR);
            var cardBackProducts = grantedProducts.Where(gp => gp.ItemClass == NBEconomy.ITEMCLASS_CARDBACK);
            var playMatProducts = grantedProducts.Where(gp => gp.ItemClass == NBEconomy.ITEMCLASS_PLAYMAT);

            Dictionary<int, NovaCatalogItem> cardProductsById = new Dictionary<int, NovaCatalogItem>();
            // Add granted cards
            var cardPrizes = new List<CardPrizeData>();
            foreach (var card in cardProducts)
            {
                NovaInventoryItem item;
                if(PlayerProfile.Inventory.TryGetValue(card.ItemId, out item))
                {
                    item.RemainingUses++;
                }
                else
                {
                    item = new NovaInventoryItem { ItemClass = NBEconomy.ITEMCLASS_CARD, ItemId = card.ItemId, RemainingUses = 1 };
                    PlayerProfile.Inventory.Add(card.ItemId, item);
                }
                int cardId;
                int.TryParse(card.ItemId, out cardId);
                if (msg.GameFormat != GameFormat.Tutorial)
                {
                    PlayerProfile.AddCard(cardId);
                }
                cardProductsById[cardId] = card;
                CardData c;
                GameData.CardDictionary.TryGetValue(cardId, out c);
                var prize = msg.GrantedPrizes.FirstOrDefault(p => p.PrizeId == cardId.ToString());
                if (prize != null)
                    cardPrizes.Add(new CardPrizeData { CardData = c, PrizeSource = prize.PrizeSource, Quantity = prize.Quantity });
            }
            foreach (var avatar in avatarProducts)
            {
                NovaInventoryItem item;
                if (PlayerProfile.Inventory.TryGetValue(avatar.ItemId, out item))
                {
                    item.RemainingUses++;
                }
                else
                {
                    item = new NovaInventoryItem { ItemClass = NBEconomy.ITEMCLASS_AVATAR, ItemId = avatar.ItemId, RemainingUses = 1 };
                    PlayerProfile.Inventory.Add(avatar.ItemId, item);
                }
                if (PlayerProfile.OwnedAvatars == null)
                    PlayerProfile.OwnedAvatars = new HashSet<string>();
                PlayerProfile.OwnedAvatars.Add(avatar.ItemId);
            }
            foreach (var cardBack in cardBackProducts)
            {
                NovaInventoryItem item;
                if (PlayerProfile.Inventory.TryGetValue(cardBack.ItemId, out item))
                {
                    item.RemainingUses++;
                }
                else
                {
                    item = new NovaInventoryItem { ItemClass = NBEconomy.ITEMCLASS_CARDBACK, ItemId = cardBack.ItemId, RemainingUses = 1 };
                    PlayerProfile.Inventory.Add(cardBack.ItemId, item);
                }
                if (PlayerProfile.OwnedCardBacks == null)
                    PlayerProfile.OwnedCardBacks = new HashSet<string>();
                PlayerProfile.OwnedCardBacks.Add(cardBack.ItemId);
            }
            foreach (var playMat in playMatProducts)
            {
                NovaInventoryItem item;
                if (PlayerProfile.Inventory.TryGetValue(playMat.ItemId, out item))
                {
                    item.RemainingUses++;
                }
                else
                {
                    item = new NovaInventoryItem { ItemClass = NBEconomy.ITEMCLASS_PLAYMAT, ItemId = playMat.ItemId, RemainingUses = 1 };
                    PlayerProfile.Inventory.Add(playMat.ItemId, item);
                }
                if (PlayerProfile.OwnedPlaymats == null)
                    PlayerProfile.OwnedPlaymats = new HashSet<string>();
                PlayerProfile.OwnedPlaymats.Add(playMat.ItemId);
            }
            
            if (msg.GrantedCurrencies.Count > 0)
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);
            if (msg.GrantedItems.Count > 0)
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Inventory);
        }

        private void InitializePlayerCard(GameOverMessage msg, EndGameDialogView endGameDialog, int oldLeaderboardPosition, int oldQps, int oldWins, int oldWinStreak, OnboardingProgress oldOnboardingProgress)
        {

            string avatarArtId = string.Empty;
            string cardBackArtId = string.Empty;
            AvatarDataContract avContract;
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.AvatarID) && this.GameData.AvatarByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.AvatarID, out avContract))
            {
                avatarArtId = avContract.ArtId;
            }
            CardBackDataContract cbContract;
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.CardBackID) && this.GameData.CardBackByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.CardBackID, out cbContract))
            {
                cardBackArtId = cbContract.ArtId;
            }

            int qps;
            this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.QP, out qps);

            var tournamentData = new TournamentData();
            if(msg.StatusData != null)
            {
                tournamentData.event_status = msg.StatusData.event_status;
                tournamentData.event_wins = msg.WinCount + (msg.HasWon ? -1 : 0);
                tournamentData.event_losses = msg.LossCount + (msg.HasWon ? 0 : -1);
            }
            if (msg.IsTournamentOver)
            {
                tournamentData.event_status = TournamentStatus.playing;
            }

            int newQuestValue = 0;
            QuestData questToShow = null;
            if(msg.GameFormat != GameFormat.Casual && msg.GameFormat != GameFormat.Practice)// && msg.GameFormat != GameFormat.Challenge)
            {
                Debug.LogFormat("No Quest shown because format is {0}", msg.GameFormat);
                // no quest to show here!
            }
            // if there's a previous quest, we need to show that as the completion animation
            else if (msg.PreviousQuest != null && oldOnboardingProgress != null && oldOnboardingProgress.CurrentQuest != null && msg.PreviousQuest.ID == oldOnboardingProgress.CurrentQuest.ID)
            {
                Debug.LogFormat("Quest completed. {0} with progress {1} to {2}", oldOnboardingProgress.CurrentQuest.ID, oldOnboardingProgress.CurrentQuest.CurrentScore, msg.PreviousQuest.CurrentScore);
                this.PlayerProfile.MostRecentCompletedQuest = msg.PreviousQuest;
                // completed this quest; given a new one so show the old one
                questToShow = oldOnboardingProgress.CurrentQuest;
                newQuestValue = msg.PreviousQuest.CurrentScore;
            }
            else if (oldOnboardingProgress != null && oldOnboardingProgress.CurrentQuest != null && msg.OnboardingProgress != null && msg.OnboardingProgress.CurrentQuest != null)
            {
                Debug.LogFormat("Quest found. {0} with progress {1} to {2}", oldOnboardingProgress.CurrentQuest.ID, oldOnboardingProgress.CurrentQuest.CurrentScore, msg.OnboardingProgress.CurrentQuest.CurrentScore);
                // progress on the same quest
                questToShow = oldOnboardingProgress.CurrentQuest;
                newQuestValue = msg.OnboardingProgress.CurrentQuest.CurrentScore;
            }
            else
            {
                Debug.LogFormat("Quest not found. Current quest is {0} with progress {1}", this.PlayerProfile.OnboardingProgress.CurrentQuest.ID, this.PlayerProfile.OnboardingProgress.CurrentQuest.CurrentScore);
                questToShow = this.PlayerProfile.OnboardingProgress.CurrentQuest;
                newQuestValue = 0;
            }

            // Set up the data for the PlayerCard.
            var Data = new PlayerData(this.PlayerProfile.MetaProfile.PlayFabID, this.PlayerProfile.MetaProfile.Name, this.PlayerProfile.UserStatistics, this.GameData.RankBrackets,
                avatarArtId, cardBackArtId, qps,
                tournamentData, msg.GameFormat, msg.PrevRating, msg.PrevMMR, oldLeaderboardPosition, oldQps, oldWins, oldWinStreak, msg.HasWon, questToShow, newQuestValue);
            
            if (endGameDialog.PlayerCard == null)
            {
                endGameDialog.PlayerCard = GameObject.Instantiate(this.UIConfiguration.PlayerCardPrefab);
                endGameDialog.PlayerCard.transform.SetParent(endGameDialog.PlayerCardParent.transform, false);
                endGameDialog.PlayerCard.StatsPanel.gameObject.SetActive(true);
                endGameDialog.PlayerCard.QuestPanel.SetActive(false);
                endGameDialog.PlayerCard.ButtonArea.gameObject.SetActive(false);
                endGameDialog.PlayerCard.PlayStatusPanel.SetActive(false);
                endGameDialog.PlayerCard.VerticalStatusPanel.SetActive(false);
            }
            endGameDialog.PlayerCard.Initialize(Data);
            endGameDialog.PlayerCard.AnimationsComplete += endGameDialog.OnPlayerCardAnimationComplete;
        }
    }
}
