﻿using UnityEngine;
using System.Collections;
using strange.extensions.context.impl;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using DragonFoundry.Networking;
using DragonFoundry.Controllers;
using NovaBlitz.UI;
using UnityEngine.UI;

namespace NovaBlitz.Game
{
    public class PreInitializeArenaCommand : Command
    {
        [Inject] public CancelSignal CancelSignal { get; set; }
        [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal { get; set; }
        
        public override void Execute()
        {
            Debug.Log("PRE Initailize New Arena game controller");
            this.DiffGameStateMessageSignal.AddListener(OnDiffGameStateMessage);
            this.CancelSignal.AddListener(OnCancel);

            SessionContextView sessionContext = GameObject.FindObjectOfType<SessionContextView>();
            sessionContext.transform.position = Vector3.zero;

            Debug.Log("Initialized Session Context");
            GameObject backgroundPrefab = Resources.Load("Prefabs/ArenaBackground") as GameObject;
            GameObject newBackgroundGameObject = GameObject.Instantiate(backgroundPrefab);
            newBackgroundGameObject.transform.SetParent(sessionContext.MainCanvas.transform, false);
            Debug.Log("Initialized Arena Background");

            GameObject newArenaPrefab = Resources.Load("Prefabs/16by9Arena") as GameObject;
            Debug.Log("loaded arena prefab");
            GameObject newArenaGameObject = GameObject.Instantiate(newArenaPrefab);
            Debug.Log("instantiated arena prefab");
            newArenaGameObject.transform.SetParent(sessionContext.MainCanvas.transform, false);
            newArenaGameObject.transform.localPosition = newArenaPrefab.transform.position;
            Debug.Log("positioned arena prefab");
            ArenaView arenaView = newArenaGameObject.GetComponent<ArenaView>();
            arenaView.ArenaBackgroundGO = newBackgroundGameObject;
            Debug.Log("Initialized Arena");
        }

        private void OnCancel()
        {
            this.DiffGameStateMessageSignal.RemoveListener(OnDiffGameStateMessage);
            this.CancelSignal.RemoveListener(OnCancel);
            var arenaView = GameObject.FindObjectOfType<ArenaView>();
            if (arenaView != null && arenaView.ArenaBackgroundGO != null)
            {
                GameObject.Destroy(arenaView.ArenaBackgroundGO);
                Debug.Log("destroyed background");
            }
            if(arenaView != null && arenaView.gameObject != null)
            { 
                GameObject.Destroy(arenaView.gameObject);
                Debug.Log("destroyed arena");
            }
        }

        private void OnDiffGameStateMessage(Messages.DiffGameStateMessage message)
        {
            this.DiffGameStateMessageSignal.RemoveListener(OnDiffGameStateMessage);
            this.CancelSignal.RemoveListener(OnCancel);
        }
    }
    
	public class InitializeNewArenaGameControllerCommand : Command
	{
	    [Inject] public ViewReferences ViewReferences {get;set;}
        [Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
	    [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
		[Inject] public GameData GameData { get; set; }
		[Inject] public ResizeArenaSignal ResizeArenaSignal { get; set;}

        public override void Execute ()
		{
			Debug.Log ("Initailize New Arena game controller");

			/*SessionContextView sessionContext = GameObject.FindObjectOfType<SessionContextView> ();
			sessionContext.transform.position = Vector3.zero;
			NovaBlitz.UI.INovaContext novaContext = sessionContext;
			Camera uiCamera = GameObject.Find ("CameraForUI").GetComponent<Camera> ();

            Debug.Log("Initialized Session Context");
			GameObject backgroundPrefab = Resources.Load ("Prefabs/ArenaBackground") as GameObject;
			GameObject newBackgroundGameObject = GameObject.Instantiate (backgroundPrefab);
			newBackgroundGameObject.transform.SetParent (novaContext.MainCanvas.transform, false);
            Debug.Log("Initialized Arena Background");

            GameObject newArenaPrefab = Resources.Load ("Prefabs/16by9Arena") as GameObject;
			GameObject newArenaGameObject = GameObject.Instantiate (newArenaPrefab);
			newArenaGameObject.transform.SetParent (novaContext.MainCanvas.transform, false);
			newArenaGameObject.transform.localPosition = newArenaPrefab.transform.position;
			ArenaView arenaView = newArenaGameObject.GetComponent<ArenaView>();
			arenaView.ArenaBackgroundGO = newBackgroundGameObject;
            Debug.Log("Initialized Arena");*/

            ArenaView arenaView = GameObject.FindObjectOfType<ArenaView>();
            Camera uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();

            this.ResizeArenaSignal.Dispatch();

            arenaView.PlayerCardDealer.SetCardBack(GameData.CurrentGameData.PlayerCardBack);
			arenaView.OpponentCardDealer.SetCardBack(GameData.CurrentGameData.OpponentCardBack);
            arenaView.TimeBladeController.PauseButton.gameObject.SetActive(GameData.CurrentGameData.GameFormat == Messages.GameFormat.Practice);
            Debug.Log("Initialized Arena Components");

            Canvas[] canvases = arenaView.GetComponentsInChildren<Canvas>();
			foreach(Canvas canvas in canvases)
			{
				canvas.worldCamera = uiCamera;
			}
            Debug.LogFormat("Canvases:{0}", canvases.Length);

			this.ViewReferences.DestroyAllViews();
		}
    }
}
