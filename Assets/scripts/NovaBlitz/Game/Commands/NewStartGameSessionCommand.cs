﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using DragonFoundry.Networking;
using DragonFoundry.Controllers.Display;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using LinqTools;
using System;
using Messages;

namespace NovaBlitz.Game
{
    public class StartGameSessionSignal : Signal<StartGameSessionObject> { } // IsSinglePlayer
    public class StartGameSessionObject
    {
        //public bool IsSinglePlayer; 
        //public bool IsChallengeGame; 
        //public bool IsDraftGame; 
        //public bool IsConstructedLeagueGame;
        public Messages.GameFormat GameFormat;
        public bool IsAcceptingChallenge = false;
        public bool IsNewDraft = false;
        public bool IsResumeDraft = false;
        public string PlayFabIdToChallenge;
        public DeckData Deck;
        public Guid GameToRejoin;
    }

    public class NewStartGameSessionCommand : Command
	{
		// Signal Parameters
		[Inject] public StartGameSessionObject StartGameSessionData {get;set;}
		
		// Injected Dependencies
		[Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public DeclineFriendChallengeSignal DeclineFriendChallengeSignal {get;set;}
		[Inject] public GameData GameData {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}

		[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;} 
		[Inject] public RejoinGameSignal StartPracticeGameSignal {get;set;}
		[Inject] public StartGameSignal StartGameSignal {get;set;}
		[Inject] public StartChallengeGameSignal StartChallengeGameSignal {get;set;}
		[Inject] public CancelMatchmakingSignal CancelMatchmakingSignal {get;set; }
        [Inject] public CancelSignal CancelSignal { get; set; }
        [Inject] public CancelFriendChallengeSignal CancelFriendChallengeSignal {get;set;}
		[Inject] public LogEventSignal LogEventSignal {get;set; }
        [Inject] public NovaAudio NovaAudio { get; set; }
        [Inject] public ShowMainMenuSignal ShowMainMenuSignal { get; set; }

        private bool onObjectPoolFinished = false;
		private IEnumerator asyncRequest = null;

		public override void Execute ()
		{
			this.CancelMatchmakingSignal.AddListener(this.OnCanceled);
			
			Debug.LogFormat ("New Start game for {0}", StartGameSessionData.GameFormat);
			this.LogEventSignal.Dispatch (new LogEventParams {
				EventName = LogEventParams.EVENT_TIME_WAIT_FOR_GAME, 
				IsTimedEvent = true, 
				IsTimedEventOver = false,
				Format = StartGameSessionData.GameFormat
				});

            // Switch to theme music

            this.NovaAudio.CrossfadeToMusicTheme();

            //Initialize the size of the PooledFX
            ObjectPool.SetPoolSizeAsync(UIConfiguration.PooledFxPrefab,6,6, ()=>{this.onObjectPoolFinished=true;}, this.OnPoolProgress);

            this.PlayerProfile.FriendChallenges.Remove(StartGameSessionData.PlayFabIdToChallenge);
            // Send Decline challenges to all pending challenges
            foreach (var id in this.PlayerProfile.FriendChallenges.Keys.ToArray())
            {
                if(id != StartGameSessionData.PlayFabIdToChallenge)
                    this.DeclineFriendChallengeSignal.Dispatch(id); // This command also removes it from the PlayerProfile
            }
			
			// Load the card file, keywords, and matchmaking servers, so we have up-to-date versions for each game.
			// Ideally we want to check if the card file is up-to-date and only load if it isn't -PB
			this.Retain ();
            this.asyncRequest = WaitForObjectPoolAndServer();
            Timer.Instance.StartCoroutine(this.asyncRequest);
		}

        private void OnCanceled()
        {
            Debug.Log("NewStartGameSession OnCanceled");
            if (this.StartGameSessionData.GameFormat == GameFormat.Challenge && this.StartGameSessionData.IsAcceptingChallenge == false )
			{
				this.CancelFriendChallengeSignal.Dispatch(this.StartGameSessionData.PlayFabIdToChallenge);
			}
            if(this.asyncRequest != null)
    			Timer.Instance.StopCoroutine(this.asyncRequest);
			this.CancelMatchmakingSignal.RemoveListener(this.OnCanceled);
            this.NovaAudio.CrossfadeToMusicMenu();

            this.LogEventSignal.Dispatch (new LogEventParams {
				EventName = LogEventParams.EVENT_TIME_WAIT_FOR_GAME, 
				IsTimedEvent = true, 
				IsTimedEventOver = true,
				Format = StartGameSessionData.GameFormat,
				EventDetails = new Dictionary<string,string>{ {"StartCanceled", "true"}}
			});
            this.Fail();
            this.Release();
            this.CancelSignal.Dispatch();
            this.ShowMainMenuSignal.Dispatch(new ShowMainMenuParams { IsCancelMatchmaking = true });
        }

        private void OnPoolProgress(float progress)
		{
			this.AsyncProgressSignal.Dispatch(AsyncProgress.ObjectPool, progress);
		}

		private void OnTitleDataLoaded()
		{
			Debug.Log("StartGameSessionCommmand: OnTitleDataLoaded");

			// Start a coroutine that waits for the card pool to be resized down to a small 
			// number of cards
			this.asyncRequest = WaitForObjectPoolAndServer();
			Timer.Instance.StartCoroutine(this.asyncRequest);
		}
		
		/// <summary>
		/// Waits for the object pool to finish being resized asynchronously
		/// </summary>
		IEnumerator WaitForObjectPoolAndServer()
		{
			
			float seconds = 0;
			while(this.onObjectPoolFinished == false ) 
			{
				//Debug.Log("Waiting for object pool seconds:" + seconds);
				seconds += Time.deltaTime;
				if(seconds > 15) yield break;
				yield return null; 
			}
			
			yield return null;
			yield return null;
			yield return null;
			
			// Initialize a create game reqeust
			Messages.CreateGameRequest cgRequest = new Messages.CreateGameRequest { GameFormat = this.StartGameSessionData.GameFormat };

            if (StartGameSessionData.GameFormat == Messages.GameFormat.Challenge)
            {
                DeckData myDeck = this.StartGameSessionData.Deck ?? this.PlayerProfile.CurrentDeck;
                cgRequest.DeckList = myDeck.DeckList;
                Debug.Log("Starting Challenge game");
				cgRequest.PlayFabIdToChallenge = this.StartGameSessionData.PlayFabIdToChallenge;
				if(StartGameSessionData.IsAcceptingChallenge == false)
				{
					// Starting Challenge Game
					this.StartChallengeGameSignal.Dispatch(cgRequest);	
				}
				else
				{
					// Accepting Challenge game
					this.StartGameSignal.Dispatch(cgRequest);
				}
			}
			else if(StartGameSessionData.GameFormat == Messages.GameFormat.Practice)
            {
                DeckData myDeck = this.StartGameSessionData.Deck ?? this.PlayerProfile.CurrentDeck;
                cgRequest.DeckList = myDeck.DeckList;
                Debug.Log("Starting Practice game");
				this.StartGameSignal.Dispatch(cgRequest);		
			}
			else
			{
                if(StartGameSessionData.GameFormat == GameFormat.Casual)
                {
                    DeckData myDeck = this.StartGameSessionData.Deck ?? this.PlayerProfile.CurrentDeck;
                    cgRequest.DeckList = myDeck.DeckList;
                }
				// For matchamking regular and draft games
				Debug.Log("Starting Game Signal");
				this.StartGameSignal.Dispatch(cgRequest);
			}

            this.CancelMatchmakingSignal.RemoveListener(this.OnCanceled);
            this.Release ();
        }
    }
}
