﻿using UnityEngine;
using System;
using System.Text;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using Messages;
using strange.extensions.command.impl;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;
using strange.extensions.signal.impl;


namespace NovaBlitz.Game
{
    public class LogGameOverDataSignal : Signal<GameOverMessage> { }
    public class LogGameOverDataCommand : Command
    {
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }
        
		[Inject] public GameOverMessage Msg {get;set;}
		[Inject] public LogEventSignal LogEventSignal {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}

        public override void Execute()
        {
            try
            {
                // Log Fusepowered metrics
                this.MetricsTrackingService.TrackGameEnd(Msg);
                // Log the game end
                if (!Msg.IsJustRewards)
                {
                    this.LogEventSignal.Dispatch(new LogEventParams
                    {
                        EventName = LogEventParams.EVENT_TIME_IN_GAME,
                        IsTimedEvent = true,
                        IsTimedEventOver = true,
                        Format = Msg.GameFormat,
                        hasWon = Msg.HasWon,
                        EventValue = Msg.CurrentRating,
                    });

                    this.LogEventSignal.Dispatch(new LogEventParams
                    {
                        EventName = LogEventParams.EVENT_GAME_COMPLETED,
                        EventValue = Msg.CurrentRating,
                        hasWon = Msg.HasWon,
                        Format = Msg.GameFormat
                    });
                }
                // Log rating changes
                if (Msg.CurrentMMR < Msg.PrevMMR && Msg.CurrentMMR != 0)
                {
                    this.LogEventSignal.Dispatch(new LogEventParams
                    {
                        EventName = LogEventParams.EVENT_LEVEL_UP,
                        EventValue = Msg.CurrentMMR
                    });
                }
                // Log tournament completions
                if (Msg.IsTournamentOver)
                {
                    this.LogEventSignal.Dispatch(new LogEventParams
                    {
                        EventName = LogEventParams.EVENT_TOURNAMENT_COMPLETED,
                        EventValue = Msg.WinCount,
                        EventValue2 = Msg.LossCount,
                        Format = Msg.GameFormat
                    });
                }
                foreach(var curr in Msg.GrantedCurrencies)
                {
                    this.LogEventSignal.Dispatch(new LogEventParams
                    {
                        EventName = LogEventParams.EVENT_CURRENCY_CHANGE,
                        CurrencyType = curr.Key,
                        CurrencyBalance = PlayerProfile.Wallet.Currencies[curr.Key],
                        EventValue = curr.Value
                    });
                }
            }
            catch (Exception except)
            {
                Debug.LogError(except.ToString());
            }
        }
    }
}
