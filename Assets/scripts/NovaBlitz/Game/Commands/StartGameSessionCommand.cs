﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using DragonFoundry.Networking;
using DragonFoundry.Controllers.Display;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using LinqTools;
using System;
using Messages;

namespace NovaBlitz.Game
{
	/*
	public class StartGameSessionCommand : Command
	{
		// Signal Parameters
		[Inject] public StartGameSessionObject StartGameSessionData {get;set;}
		
		// Injected Dependencies
		[Inject] public ClientPrefs ClientPrefs { get; set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public DeclineFriendChallengeSignal DeclineFriendChallengeSignal {get;set;}
		[Inject] public TitleDataLoadedSignal TitleDataLoadedSignal { get; set; }
		[Inject] public GameData GameData {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}

		[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;} 
		[Inject] public ServerConnectedSignal ServerConnectedSignal {get;set;}
		[Inject] public StartPracticeGameSignal StartPracticeGameSignal {get;set;}
		[Inject] public StartGameSignal StartGameSignal {get;set;}
		[Inject] public StartChallengeGameSignal StartChallengeGameSignal {get;set;}
		[Inject] public StartGameReadySignal StartGameReadySignal {get;set;}
		[Inject] public CancelSignal CancelSignal {get;set;}
		[Inject] public CancelFriendChallengeSignal CancelFriendChallengeSignal {get;set;}
        [Inject] public LogCustomEventSignal LogCustomEventSignal { get; set; }

 
		private DemoClient client;
		private bool onObjectPoolFinished = false;
		private bool isServerConnected = false;
		private IEnumerator asyncRequest = null;

		public override void Execute ()
		{			
			this.CancelSignal.AddListener(this.OnCanceled);
			
			
			DeckData myDeck = this.StartGameSessionData.GameFormat == Messages.GameFormat.Draft
				? this.PlayerProfile.DraftDeck
				: this.PlayerProfile.CurrentDeck;
			
			if (this.StartGameSessionData.GameFormat == Messages.GameFormat.Draft)
				myDeck.LegalDeckSize = PlayerProfile.DRAFT_DECK_SIZE; //HACK: for drafting -DMAc
			
			if(this.StartGameSessionData.GameFormat != Messages.GameFormat.Tutorial && myDeck.DeckList.Count != myDeck.LegalDeckSize)
			{
				Debug.LogWarning("CurrentDeck '" + myDeck.Name + "' is not legal Decksize of " + myDeck.LegalDeckSize + " I is instead " + myDeck.Cards.Count);
				this.Fail();
				return;
			}

			// Listen for the server connection
			this.ServerConnectedSignal.AddOnce(this.OnServerConnected);

			Debug.Log ("New Start game");

			// Once the cards assets are in the ObjectPool, resize the object pool smaller
			ObjectPool.SetPoolSizeAsync(this.UIConfiguration.CardPrefab, this.UIConfiguration.CardPoolSize, 10, null, this.OnPoolProgress);
			ObjectPool.SetPoolSizeAsync(this.UIConfiguration.CardListItemPrefab, this.UIConfiguration.CardPoolSize, 10, null, this.OnPoolProgress);
			ObjectPool.SetPoolSizeAsync(this.UIConfiguration.PooledFxPrefab,6,2, ()=>{this.onObjectPoolFinished=true;}, this.OnPoolProgress);

			// Send Decline challenges to all pending challenges
			foreach (var id in this.PlayerProfile.FriendChallenges.Keys.ToArray())
				this.DeclineFriendChallengeSignal.Dispatch(id); // This command also removes it from the PlayerProfile
			
			// Load the card file, keywords, and matchmaking servers, so we have up-to-date versions for each game.
			// Ideally we want to check if the card file is up-to-date and only load if it isn't -PB
			this.Retain ();
            //this.ClientOutOfDateSignal.AddOnce(OnClientOutOfDate)
			//this.LoadTitleDataSignal.Dispatch (true);
            this.asyncRequest = WaitForObjectPoolAndServer();
            Timer.Instance.StartCoroutine(this.asyncRequest);

            //this.TitleDataLoadedSignal.AddOnce(OnTitleDataLoaded);
		}

        private void OnCanceled()
        {
            LogGameStartEvent(StartGameSessionData.GameFormat, true);
            this.ServerConnectedSignal.RemoveListener(this.OnServerConnected);
			this.TitleDataLoadedSignal.RemoveListener(this.OnTitleDataLoaded);
			if(this.StartGameSessionData.GameFormat == GameFormat.Challenge && this.StartGameSessionData.IsAcceptingChallenge == false )
			{
				this.CancelFriendChallengeSignal.Dispatch(this.StartGameSessionData.PlayFabIdToChallenge);
			}
			Timer.Instance.StopCoroutine(this.asyncRequest);
			this.CancelSignal.RemoveListener(this.OnCanceled);
        }

        private void OnPoolProgress(float progress)
		{
			this.AsyncProgressSignal.Dispatch(AsyncProgress.ObjectPool, progress);
		}

		private void OnTitleDataLoaded()
		{
			Debug.Log("StartGameSessionCommmand: OnTitleDataLoaded");

			// Start a coroutine that waits for the card pool to be resized down to a small 
			// number of cards
			this.asyncRequest = WaitForObjectPoolAndServer();
			Timer.Instance.StartCoroutine(this.asyncRequest);
		}
		
		/// <summary>
		/// Waits for the object pool to finish being resized asynchronously
		/// </summary>
		IEnumerator WaitForObjectPoolAndServer()
		{
			
			float seconds = 0;
			while(this.onObjectPoolFinished == false ) 
			{
				//Debug.Log("Waiting for object pool seconds:" + seconds);
				seconds += Time.deltaTime;
				if(seconds > 15) yield break;
				yield return null; 
			}
			
			seconds = 0;
			while(this.isServerConnected == false)
			{
				//Debug.Log("Waiting for server seconds:" + seconds);
				seconds += Time.deltaTime;
				if(seconds > 15) yield break;
				yield return null; 
			}
			
			yield return null;
			yield return null;
			yield return null;
			
			// Initialize a create game reqeust
			Messages.CreateGameRequest cgRequest = new Messages.CreateGameRequest();
			//cgRequest.MultiplayerGame = this.StartGameSessionData.IsSinglePlayer == false;
			//cgRequest.IsChallengeGame = 
			cgRequest.GameFormat = this.StartGameSessionData.GameFormat;

			if(StartGameSessionData.GameFormat == Messages.GameFormat.Challenge)
			{
				Debug.Log("Starting Challenge game");
				cgRequest.PlayFabIdToChallenge = this.StartGameSessionData.PlayFabIdToChallenge;
				if(StartGameSessionData.IsAcceptingChallenge == false)
				{
					// Starting Challenge Game
					this.StartChallengeGameSignal.Dispatch(cgRequest, new CurrentGameData());	
				}
				else
				{
					// Accepting Challenge game
					this.StartGameSignal.Dispatch(cgRequest, new CurrentGameData());
				}
			}
			else if(StartGameSessionData.GameFormat == Messages.GameFormat.Practice)
			{
				Debug.Log("Starting Practice game");
				this.StartPracticeGameSignal.Dispatch(cgRequest, new CurrentGameData());		
			}
			else
			{
				// For matchamking regular and draft games
				Debug.Log("Starting Game Signal");
				this.StartGameSignal.Dispatch(cgRequest, new CurrentGameData());
			}
			
			this.StartGameReadySignal.AddOnce(this.OnStartGameReady);

			this.Release ();
            LogGameStartEvent(StartGameSessionData.GameFormat, true);
        }

        private void OnServerConnected(bool isConnected, string reason)
		{
			Debug.Log("StartGameSessionCommand: OnServerConnected");
			this.isServerConnected = true;
		}

        private void OnStartGameReady(ReadyToStartGameResponse response)
        {
            // Destroy all UI Views
			this.ViewReferences.DestroyAllViews();
        }

        private void LogGameStartEvent(GameFormat format, bool isSuccessful)
        {
            // for logging game start events
            Dictionary<string, object> bodyParams = new Dictionary<string, object>();
            bodyParams["format"] = format.ToString();
            if (format == GameFormat.Tutorial)
            {
                bodyParams["tutorial_number"] = PlayerProfile.OnboardingProgress.CurrentTutorial;
            }
            else
            {
                foreach (var aspect in PlayerProfile.CurrentDeck.Aspects)
                {
                    if (aspect == CardAspect.NoAspect)
                        continue;
                    bodyParams[aspect.ToString()] = true;
                }
            }
            if(format == GameFormat.Challenge)
            {
                bodyParams["received_challenge"] = StartGameSessionData.IsAcceptingChallenge;
                bodyParams["challenger"] = StartGameSessionData.PlayFabIdToChallenge;
            }
            this.LogCustomEventSignal.Dispatch("PhotonChat_DC", bodyParams);
        }
    }*/
}
