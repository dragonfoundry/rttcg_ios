﻿using Messages;
using NovaBlitz.Game;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NovaBlitz.UI
{
    public class AwaitGameStartOrCancelSignal : Signal {}
    public class AwaitGameStartOrCancelCommand : Command
    {
		[Inject] public ShowCancelProgressButtonSignal ShowCancelProgressButtonSignal {get;set;}
        [Inject] public ClientService ClientService {get;set;}
        [Inject] public CancelMatchmakingResponseSignal CancelMatchmakingResponseSignal { get; set; }
		[Inject] public CancelMatchmakingSignal CancelMatchmakingSignal {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
        [Inject] public ServerErrorRaisedSignal ServerErrorRaisedSignal { get; set; }
		[Inject] public FriendChallengeDeclinedSignal FriendChallengeDeclinedSignal {get;set;}
        //[Inject] public GameStateInitializedSignal GameStateInitializedSignal {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public SetProgressScreenStatusLabelSignal SetProgressScreenStatusLabelSignal {get;set;}
        [Inject] public ViewReferences ViewReferences {get;set;}
        [Inject] public StartGameReadySignal StartGameReadySignal { get; set; }
        [Inject] public GameCreatedSignal GameCreatedSignal { get; set; }
        [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}

        private IEnumerator ShowCancelButtonCoroutine { get; set; }
        private bool IsStartGameReady = false;

        public override void Execute()
		{
            Debug.Log("AwaitGameStartOrCancelCommand");
			this.Retain();
			this.ServerErrorRaisedSignal.AddListener(this.OnServerError);
            this.GameCreatedSignal.AddListener(this.OnGameCreated);
            this.DiffGameStateMessageSignal.AddOnce(this.OnDiffGameState);

            this.FriendChallengeDeclinedSignal.AddListener(this.OnChallengeDeclined);
			//this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            //this.GameStateInitializedSignal.AddListener(this.OnGameStateInitialized);
            this.StartGameReadySignal.AddListener(OnGameReady);
            if (ShowCancelButtonCoroutine != null)
                Timer.GameInstance.StopCoroutine(ShowCancelButtonCoroutine);
            ShowCancelButtonCoroutine = this.WaitToShowCancelButton();
            Timer.GameInstance.StartCoroutine(ShowCancelButtonCoroutine);
		}

        private void OnGameCreated(CreateGameResponse response)
        {
            if (ShowCancelButtonCoroutine != null)
                Timer.GameInstance.StopCoroutine(ShowCancelButtonCoroutine);
        }

        private void OnGameReady(ReadyToStartGameResponse response)
        {
            Debug.LogFormat("Start Game Ready");
            IsStartGameReady = true;
            //this.ReleaseCommand();
        }

        private void OnDiffGameState(DiffGameStateMessage diffMessage)
        {
            if(diffMessage.IsForceStart)
            {
                if (!IsStartGameReady)
                {
                    MetricsTrackingService.SavedException = "Force Start for game. Compare with Loggly data";
                    Debug.Log("FORCE START");
                }
                else
                {
                    Debug.Log("Force start because opponent didn't show up");
                }
            }
             this.ReleaseCommand();
        }

        /*private void OnGameStateInitialized(GameStateReadOnly gameState)
        {
            this.ReleaseCommand();
        }*/

        private void OnCloseAnimatedView(Type viewType)
        {
           if(viewType == typeof(ProgressLoadingScreenView))
           {
               this.ReleaseCommand();
           }
        }

        private IEnumerator WaitToShowCancelButton()
        {
            yield return new WaitForSecondsRealtime(15f);
            this.ShowCancelProgressButtonSignal.Dispatch(this.OnCancelClicked);
        }
		
		private void OnServerError(ErrorCode error, MessageBase message)
		{
            CancelAndRelease();
        }
		
		private void OnChallengeDeclined(string playfabID)
		{
			if (playfabID == this.PlayerProfile.SelectedChallengeID)
			{
                OnCancelClicked();

                //this.CancelAndRelease();
            }
		}
		
		private IEnumerator hardStopCoroutine;
        private void OnCancelClicked()
        {
            this.SetProgressScreenStatusLabelSignal.Dispatch(I2.Loc.ScriptLocalization.AsyncProgress.Canceling);
            CancelMatchmakingRequest cancelRequest = new CancelMatchmakingRequest { };
            this.CancelMatchmakingResponseSignal.AddListener(this.OnCancelMatchmakingResponse);
            this.ClientService.SendMessage(cancelRequest);
			
            this.hardStopCoroutine = this.HardCancelAfterFiveSeconds();
            Timer.GameInstance.StartCoroutine(this.hardStopCoroutine);
            
            ProgressLoadingScreenView progressScreen = (ProgressLoadingScreenView)this.ViewReferences.Get(typeof(ProgressLoadingScreenView));
            Dictionary<AsyncProgress, string> progressLabels = new Dictionary<AsyncProgress, string>();
            progressLabels[AsyncProgress.CancelMatchmaking] = I2.Loc.ScriptLocalization.AsyncProgress.Canceling;
            
            progressScreen.InitializeProgressItems(progressLabels);
            this.AsyncProgressSignal.Dispatch(AsyncProgress.CancelMatchmaking, 1.0f);
        }
		
        public IEnumerator HardCancelAfterFiveSeconds()
        {
            yield return new WaitForSecondsRealtime(5f);
            this.AsyncProgressSignal.Dispatch(AsyncProgress.CancelMatchmaking, 1.0f);
            CancelAndRelease();
        }
        
        private void OnCancelMatchmakingResponse(CancelMatchmakingResponse response)
        {
            this.SetProgressScreenStatusLabelSignal.Dispatch(string.Empty);
            this.CancelMatchmakingResponseSignal.RemoveListener(OnCancelMatchmakingResponse);
            Timer.GameInstance.StopCoroutine(this.hardStopCoroutine);
            if (response.IsSuccess)
            {
				this.AsyncProgressSignal.Dispatch(AsyncProgress.CancelMatchmaking, 1.0f);
                this.CancelAndRelease();
            }
            else
            {
                this.ShowCancelProgressButtonSignal.Dispatch(this.OnCancelClicked);
            }
        }

        private void CancelAndRelease()
        {

            this.ClientService.IsInGame = false;
            this.CancelMatchmakingSignal.Dispatch();
            ReleaseCommand();
        }

        private void ReleaseCommand()
        {
            this.CancelMatchmakingResponseSignal.RemoveListener(this.OnCancelMatchmakingResponse);
			this.ServerErrorRaisedSignal.RemoveListener(this.OnServerError);
            this.GameCreatedSignal.RemoveListener(this.OnGameCreated);
            this.FriendChallengeDeclinedSignal.RemoveListener(this.OnChallengeDeclined);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.StartGameReadySignal.RemoveListener(this.OnGameReady);
            this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGameState);
            //this.GameStateInitializedSignal.RemoveListener(this.OnGameStateInitialized);

            if (this.ShowCancelButtonCoroutine != null)
            {
                Timer.GameInstance.StopCoroutine(this.ShowCancelButtonCoroutine);
            }
			if(this.hardStopCoroutine != null)
			{
				Timer.GameInstance.StopCoroutine(this.hardStopCoroutine);
			}
            this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
            Debug.Log("AwaitGameStartOrCancelCommand released!");
            this.SetProgressScreenStatusLabelSignal.Dispatch(string.Empty);
			this.Release();
        }
    }
}
