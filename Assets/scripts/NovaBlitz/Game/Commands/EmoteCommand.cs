﻿using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI;
using Newtonsoft.Json;

namespace NovaBlitz.Game
{
    public class EmoteSignal : Signal<int> { }
    public class EmoteCommand : Command
    {
        [Inject] public int emote { get; set; }
        [Inject] public ClientService ClientService { get; set; }

        public override void Execute()
        {
            this.ClientService.SendMessage(new Messages.EmoteMessage { Emote = emote, GameGuid = this.ClientService.SessionGuid });
        }
    }
}
