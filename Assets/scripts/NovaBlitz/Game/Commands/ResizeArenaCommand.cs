﻿using System;
using System.Collections;
using UnityEngine;
using NovaBlitz.UI;
using UnityEngine.UI;

using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.Game
{
	public class ResizeArenaSignal : StartSignal {}
	public class ResizeArenaCommand : Command
	{
		[Inject] public ClientPrefs ClientPrefs {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}


		public override void Execute()
		{
			Timer.GameInstance.StartCoroutine (WaitThenResize ());
		}
		private IEnumerator WaitThenResize()
		{
			yield return null;

			SessionContextView sessionContext = GameObject.FindObjectOfType<SessionContextView> ();
			ArenaView arenaView = (ArenaView)UnityEngine.Object.FindObjectOfType(typeof(ArenaView));
			UXConfig currentConfig = UXConfig.DesktopWideScreenSquareArena;
            if (sessionContext == null || arenaView == null)
                yield break;
			bool isMobile = false;

			#if UNITY_IOS || UNITY_ANDROID
			isMobile = true;
			#endif

			arenaView.PlayerHandController.IsUsingZoomHand = false;

			if(isMobile)
			{
				// Mobile Configs
				if(this.ClientPrefs.IsScreen16x9)
				{
					if(this.ClientPrefs.IsWideArena)
					{
						currentConfig = UXConfig.MobileWideScreenWideArena;
					}
					else
					{
						currentConfig = UXConfig.MobileWideScreenSquareArena;
					}
				}
				else
				{
					currentConfig = UXConfig.MobileSquareScreenSquareArena;
				}
			}
			else
			{
				// Desktop Configs
				if(this.ClientPrefs.IsScreen16x9)
				{
					if(this.ClientPrefs.IsWideArena)
					{
						currentConfig = UXConfig.DesktopWideScreenWideArena;
					}
					else
					{
						currentConfig = UXConfig.DesktopWideScreenSquareArena;
					}
				}
				else
				{
					currentConfig = UXConfig.DesktopSquareScreenSquareArena;
				}
			}

			CanvasScaler mainScaler = sessionContext.MainCanvas.GetComponent<CanvasScaler>();
			CanvasScaler overlayScaler = sessionContext.OverlayCanvas.GetComponent<CanvasScaler>();

			UXValues uxValues = this.UIConfiguration.GetUXValues(currentConfig);
			arenaView.PlayerHandController.UXValues = uxValues;
			arenaView.transform.localScale = Vector3.one * uxValues.ArenaScale;

			// OverlayScalar matchWidthOrHeight is always going to be 1 so it matces the MainContext scalar
			// because we often have views from both contexts open at the same time when moving between
			// UI and gameplay -Dmac
			switch(currentConfig)
			{
				case UXConfig.DesktopSquareScreenSquareArena:
					mainScaler.matchWidthOrHeight = 0;
					overlayScaler.matchWidthOrHeight = 1;
					break;
				case UXConfig.DesktopWideScreenSquareArena:
					mainScaler.matchWidthOrHeight = 1;
					overlayScaler.matchWidthOrHeight = 1;
					break;
				case UXConfig.DesktopWideScreenWideArena:
					mainScaler.matchWidthOrHeight = 0;
					overlayScaler.matchWidthOrHeight = 1;
					arenaView.PlayerHandController.IsUsingZoomHand = true;
					// if the aspect ratio is even wider than 16:9 switch back to scaling by height
					if( Camera.main.aspect > 1.78f )
					{
						mainScaler.matchWidthOrHeight = 1;
						overlayScaler.matchWidthOrHeight = 1;
					}
					break;
				case UXConfig.MobileSquareScreenSquareArena:
					mainScaler.matchWidthOrHeight = 0;
					overlayScaler.matchWidthOrHeight = 1;
					break;
				case UXConfig.MobileWideScreenSquareArena:
					mainScaler.matchWidthOrHeight = 1;
					overlayScaler.matchWidthOrHeight = 1;
					break;
				case UXConfig.MobileWideScreenWideArena:
					mainScaler.matchWidthOrHeight = 0;
					overlayScaler.matchWidthOrHeight = 1;
					arenaView.PlayerHandController.IsUsingZoomHand = true;
                    // if the aspect ratio is even wider than 16:9 switch back to scaling by height
                    if ( Camera.main.aspect > 1.78f )
					{
						mainScaler.matchWidthOrHeight = 1;
						overlayScaler.matchWidthOrHeight = 1;
					}
					break;
			}

			Debug.LogFormat("Current Config is :{0} ZoomHand:{1} Aspect:{2}", currentConfig, arenaView.PlayerHandController.IsUsingZoomHand, Camera.main.aspect);

			//Vector3 arenaScale =  Vector3.one * uxValues.ArenaScale;

			if(uxValues.ArenaScale == 1.0f)
			{
				if(this.ClientPrefs.IsScreen16x9) // widescreen wide arena
				{
					arenaView.ArenaUICanvas.sizeDelta = new Vector2(1920f,1080);
				}
				else // square screen square arena
				{
					arenaView.ArenaUICanvas.sizeDelta = new Vector2(1920f,1440);
				}
			}
			else // wide screen square arena
			{
				//arenaView.transform.localScale = Vector3.one * uxValues.ArenaScale;
				Vector2 size = ((RectTransform)arenaView.ArenaUICanvas.transform).sizeDelta;
				size.x = 2475;
				((RectTransform)arenaView.ArenaUICanvas.transform).sizeDelta = size;
				Debug.LogFormat("resizing to 0.75. x:{0}, y:{1}",size.x, size.y);
			}

			Debug.LogFormat ("Resizing with config: {0}, zoom to {1}, constrain {2}, aspect {3}", 
				currentConfig.ToString (), 
				uxValues.ArenaScale,
				mainScaler.matchWidthOrHeight == 0 ? "width" : "height",
				Camera.main.aspect);
		}
	}
}

