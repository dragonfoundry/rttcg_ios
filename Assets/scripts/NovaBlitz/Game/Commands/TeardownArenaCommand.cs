﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using DragonFoundry.Networking;
using NovaBlitz.UI;

namespace NovaBlitz.Game
{
	public class TeardownArenaSignal : Signal {}
    public class TeardownArenaCommand : Command
    {
		[Inject] public ViewReferences ViewReferences {get;set;}
        [Inject] public ArenaCards ArenaCards {get;set;}
        [Inject] public ViewStateStack ViewStateStack{get;set;}
        [Inject] public UnloadUnusedResourcesSignal UnloadUnusedResourcesSignal { get; set; }
        
		public override void Execute()
		{
            // Make sure we clear any references to blurred views in the ViewStateStack, that way when
            // we destroy them below their OnRemove() functions are propely called.  If there's a reference
            // in a C# dictionary to any view being destroyed below OnDestroy() won't be called on it.. and so StrangeIOC
            // won't call OnRemove() for the view. -DMac
            this.ViewStateStack.Clear();

            ArenaView arenaView = (ArenaView)Object.FindObjectOfType(typeof(ArenaView));
            if (arenaView != null)
            {
                GameObject.Destroy(arenaView.gameObject);
            }

            TutorialPopUpView tutorial = (TutorialPopUpView)Object.FindObjectOfType(typeof(TutorialPopUpView));
            if (tutorial != null)
            {
                GameObject.Destroy(tutorial.gameObject);
            }

            InGameMenuView inGameMenu = (InGameMenuView)Object.FindObjectOfType(typeof(InGameMenuView));
			if (inGameMenu != null)
            {
                GameObject.Destroy(inGameMenu.gameObject);
            }

            EndGameDialogView end = (EndGameDialogView)Object.FindObjectOfType(typeof(EndGameDialogView));
            if (end != null)
            {
                GameObject.Destroy(end.gameObject);
            }

            StartingHandView start = (StartingHandView)Object.FindObjectOfType(typeof(StartingHandView));
            if (start != null)
            {
                if (start.OnEarlyDeath != null)
                    start.OnEarlyDeath.Invoke();
                GameObject.Destroy(start.gameObject);
            }

            StartGameDialogView startGame = (StartGameDialogView)Object.FindObjectOfType(typeof(StartGameDialogView));
            if (startGame != null)
            {
                GameObject.Destroy(startGame.gameObject);
            }

            Debug.Log("Destroy all views");
            // DestroyAllViews is supposed to do what the above GameObject.Destroy() methods are... so it's duplicating here 
            // I don't know why the above Destroy tests were added so I'm hesitant to remove them without knowing what issue they were intended to address.
            // --DMac
            this.ArenaCards.RecycleCardsAndBacks();
            this.ViewReferences.DestroyAllViews();


            this.UnloadUnusedResourcesSignal.Dispatch();

        }

    }
}