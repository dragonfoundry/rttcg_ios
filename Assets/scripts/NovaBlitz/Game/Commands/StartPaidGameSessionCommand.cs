﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using DragonFoundry.Networking;
using DragonFoundry.Controllers.Display;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using LinqTools;
using System;
using Messages;
using NovaBlitz.Economy;

public class StartPaidGameSessionSignal : Signal<StartPaidGameSessionParams> {}

public class StartPaidGameSessionParams 
{ 
     public CurrencyType Currency;
     public int ExpectedPrice;
     public GameFormat Format;
     public DeckData DeckToPlay; // make sure to clone this when creating this params object!
     public TournamentEventData TournamentToPlay;
}

public class StartPaidGameSessionCommand : Command 
{
	// Signal Parameters
	[Inject] public StartPaidGameSessionParams StartPaidGameSessionParams {get;set;}
			
	public override void Execute ()
	{
	}
}
