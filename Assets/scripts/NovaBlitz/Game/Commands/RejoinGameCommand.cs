﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using LinqTools;
using NovaBlitz.Game;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
    public class RejoinGameSignal : Signal<CreateGameRequest> { }
    public class RejoinGameCommand : Command
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }


        public override void Execute()
        {
        }

    }

}
