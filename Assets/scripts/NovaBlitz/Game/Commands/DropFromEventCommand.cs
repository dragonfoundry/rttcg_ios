﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using NovaBlitz.Game;
using System;

namespace NovaBlitz.UI
{
	public class DropFromEventSignal : Signal<GameFormat> {}
	
	public class DropFromEventCommand : Command
	{
		// Signal Parameters
		[Inject] public GameFormat GameFormat {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ClientService ClientService {get;set;}
		[Inject] public CancelSignal CancelSignal {get;set;}
		[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}

        [Inject] public ViewReferences ViewReferences { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public InitializeProgressScreenSignal InitializeProgressScreenSignal { get;set;}
        [Inject] public HideCancelProgressButtonSignal HideCancelProgressButtonSignal {get;set;}


        [Inject]
        public GameOverSignal GameOverSignal { get; set; }
        		
		public override void Execute()
		{

            Debug.Log("Drop From Event Command:");
			this.CancelSignal.AddListener(this.OnCanceled);

            Dictionary<AsyncProgress, string> progressLabels = new Dictionary<AsyncProgress, string>();
            progressLabels[AsyncProgress.NoProgress] = "FOO...";
            progressLabels[AsyncProgress.DropFromEvent] = "BAR...";
            progressLabels[AsyncProgress.Authenticating] = "GOO...";

            this.InitializeProgressScreenSignal.Dispatch(progressLabels);
            this.Retain();
            
            //this.AsyncProgressSignal.Dispatch(AsyncProgress.NoProgress, 0.5f);

            DropFromEventRequest dropRequest = new DropFromEventRequest { GameFormat = this.GameFormat, PlayFabSessionToken = this.PlayerProfile.MetaProfile.PlayFabSessionToken };
            this.AsyncProgressSignal.Dispatch(AsyncProgress.DropFromEvent, 0.75f);
            Debug.Log("Drop from EventCommand: Sending DropRequest ");
            this.GameOverSignal.AddListener(OnDropFromEventResponse);
            this.ClientService.SendMessage(dropRequest);
        }

        private void OnCanceled()
        {
            this.GameOverSignal.RemoveListener(OnDropFromEventResponse);
            this.CancelSignal.RemoveListener(this.OnCanceled);
            
			this.Fail();
            this.Release();
            this.ClientService.IsInGame = false;
        }

		private void OnDropFromEventResponse(GameOverMessage response)
        {
            this.ClientService.IsInGame = false;
            this.GameOverSignal.RemoveListener(OnDropFromEventResponse);
            Debug.Log("Drop From Event Command: DropFromEventREsponse" );
            this.CancelSignal.RemoveListener(this.OnCanceled);
			this.AsyncProgressSignal.Dispatch(AsyncProgress.DropFromEvent, 1f);

            if (response.Error != ErrorCode.NoError)
            {
                this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
            }
            else
            {
                this.HideCancelProgressButtonSignal.Dispatch();
                this.AsyncProgressSignal.Dispatch(AsyncProgress.Authenticating, 0.0f);
            }
            this.Release();
        }
    }
}