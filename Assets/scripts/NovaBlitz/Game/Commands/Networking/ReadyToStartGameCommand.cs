﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using NovaBlitz.UI;

public class ReadyToStartGameSignal : Signal<CreateGameRequest> {}
public class StartGameReadySignal : Signal<ReadyToStartGameResponse> {}
public class ReadyToStartGameCommand : Command
{
	// Signal Parametrers
	[Inject] public CreateGameRequest CreateGameRequest {get;set;}
	[Inject] public PlayerProfile PlayerProfile {get; set;}
	
	// Injected Dependencies
    [Inject] public GameData GameData {get;set;}
	[Inject] public ClientService ClientService {get;set;}
	[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
	[Inject] public StartGameReadySignal StartGameReadySignal {get;set;}
	[Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}

	[Inject] public CancelSignal CancelSignal {get;set;}
	[Inject] public CancelFriendChallengeSignal CancelFriendChallengeSignal {get;set;}
	[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
	[Inject] public LogEventSignal LogEventSignal {get;set;}
	[Inject] public TeardownArenaSignal TeardownArenaSignal {get;set;}
    [Inject] public ShowMainMenuSignal ShowMainMenuSignal { get; set; }
        [Inject] public ProcessGameStateEventsSignal ProcessGameStateEventsSignal { get; set; }
	
	//private IEnumerator asyncRequest = null;
	override public void Execute()
    {
        Debug.Log("ReadyToStartGameCommand: Setting up listeners; waiting 5s to send");
		
		this.Retain();
		this.CancelSignal.AddListener(this.OnCanceled);
		this.AsyncProgressSignal.Dispatch(AsyncProgress.ReadyToStartGameRequest, 0.5f);
        //this.asyncRequest = this.ClientService.GetResponseAsync<ReadyToStartGameResponse>(this.OnReadyToStartResponse);
        this.StartGameReadySignal.AddListener(OnReadyToStartResponse);
		this.DiffGameStateMessageSignal.AddListener(this.OnDiffGameState);
        Timer.GameInstance.StartCoroutine(WaitToReadyGame(CreateGameRequest.GameToRejoin == Guid.Empty));
	}

    private IEnumerator WaitToReadyGame(bool isWait)
    {
        float targetTime = Time.realtimeSinceStartup + 0.1f;
        while (Time.realtimeSinceStartup < targetTime) { yield return null; }
        targetTime += isWait ? 5.0f : 0.5f;
        Debug.LogFormat("Process game state signal sending at {0}. Waited {1}. Last Frame {2}", Time.realtimeSinceStartup, Time.realtimeSinceStartup - targetTime - 0.1f, Time.deltaTime);
        ProcessGameStateEventsSignal.Dispatch();
        while (Time.realtimeSinceStartup < targetTime) { yield return null; }

        Debug.LogFormat("Sending Ready to start game request at {0}. Waited {1}. Last Frame {2}", Time.realtimeSinceStartup, Time.realtimeSinceStartup - targetTime - 5.1f, Time.deltaTime);
        ReadyToStartGameRequest readyToStartRequest = new ReadyToStartGameRequest()
        {
            GameGuid = GameData.CurrentGameData.GameGuid,
            PlayerPlayFabSessionToken = this.PlayerProfile.MetaProfile.PlayFabSessionToken
        };
        this.ClientService.SendMessage(readyToStartRequest);
    }

    private void OnReadyToStartResponse(ReadyToStartGameResponse response)
    {
        this.StartGameReadySignal.RemoveListener(OnReadyToStartResponse);
		//this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGameState);
		
        Debug.Log("OnReadyToStartResponse");
        if (response.Error != ErrorCode.NoError)
        {
            this.CancelSignal.Dispatch();
            return;
        }
        this.AsyncProgressSignal.Dispatch(AsyncProgress.ReadyToStartGameRequest, 1.0f);
		// this 200ms delay gives the loading screen time to show a full progress bar before it is stripped away as part of the 
		// MainContext to SessionContext transition.
		//Timer.Instance.StartCoroutine(DelayStartSession());
		//this.CancelSignal.RemoveListener(this.OnCanceled);
    }

	private void OnDiffGameState(DiffGameStateMessage diffMessage)
	{
        Debug.Log("OnDiffGameState - ReadyToStartCommand");
		//this.StartGameReadySignal.RemoveListener(OnReadyToStartResponse);
		this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGameState);

		this.AsyncProgressSignal.Dispatch(AsyncProgress.ReadyToStartGameRequest, 1.0f);
		// this 200ms delay gives the loading screen time to show a full progress bar before it is stripped away as part of the 
		// MainContext to SessionContext transition.
		Timer.GameInstance.StartCoroutine(DelayStartSession());
		this.CancelSignal.RemoveListener(this.OnCanceled);
	}

    private void OnCanceled()
    {
        Timer.GameInstance.StopCoroutine(WaitToReadyGame(false));
        Timer.GameInstance.StopCoroutine(DelayStartSession());
        this.StartGameReadySignal.RemoveListener(OnReadyToStartResponse);
        this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGameState);
        this.CancelSignal.RemoveListener(this.OnCanceled);

        Debug.Log("ReadyToStartGamecommand: OnCanceled");
		if(this.CreateGameRequest.GameFormat == GameFormat.Challenge)
		{
			this.CancelFriendChallengeSignal.Dispatch(this.CreateGameRequest.PlayFabIdToChallenge);
		}
        //this.ClientService.StopAsyncRequest(this.asyncRequest);
        this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
        //this.ClientService.Disconnect();

        this.ClientService.IsInGame = false;

        this.TeardownArenaSignal.Dispatch();
        
        //yield return null;
        //yield return null;
        var ShowMainMenuParams = new ShowMainMenuParams();
  
        if (this.PlayerProfile.UiHintData.OpenPacks > 0)
            this.PlayerProfile.Wallet.Currencies.TryGetValue(Messages.CurrencyType.SP, out ShowMainMenuParams.PacksToOpen);

        this.ShowMainMenuSignal.Dispatch(ShowMainMenuParams);


		this.Fail();

		this.LogEventSignal.Dispatch (new LogEventParams {
			EventName = LogEventParams.EVENT_TIME_WAIT_FOR_GAME, 
			IsTimedEvent = true, 
			IsTimedEventOver = true,
			Format = CreateGameRequest.GameFormat,
			EventDetails = new Dictionary<string,string>{ {"StartCanceled", "true"}}
		});
        this.Release();
    }

    private IEnumerator DelayStartSession()
    {
		yield return new WaitForSecondsRealtime(0.2f);
        Debug.Log("Delay start session");
        this.Release();

		this.LogEventSignal.Dispatch (new LogEventParams {
			EventName = LogEventParams.EVENT_TIME_WAIT_FOR_GAME, 
			IsTimedEvent = true, 
			IsTimedEventOver = true,
			Format = CreateGameRequest.GameFormat,
			EventDetails = new Dictionary<string,string>{ {"StartCanceled", "false"}}
		});

		this.LogEventSignal.Dispatch (new LogEventParams {
			EventName = LogEventParams.EVENT_TIME_IN_GAME, 
			IsTimedEvent = true, 
			IsTimedEventOver = false,
			Format = CreateGameRequest.GameFormat,
		});
    }
}
