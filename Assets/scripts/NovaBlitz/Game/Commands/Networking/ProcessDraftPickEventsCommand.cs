﻿using UnityEngine;
using System.Collections;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using DragonFoundry.GameState;
using DragonFoundry.Networking;


namespace NovaBlitz.UI
{

    public class DraftNextPackSignal : Signal<DraftNextPack> { }
    public class DraftOverSignal : Signal { } // Draft guid

    public class ProcessDraftPickEventsSignal : Signal { }
    public class ProcessDraftPickEventsCommand : Command
    {
        // Injected Dependencies
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }

        override public void Execute()
        {
            Debug.LogWarning("Processing Draft Events!");
            //this.ClientService.IsSessionReady = true;
			this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
        }
    }
}