﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using System.Collections.Generic;
using NovaBlitz.Game;
using System.Net;
using NovaBlitz.UI;
using System;

public class StartGameSignal : Signal<CreateGameRequest> {}
public class StartChallengeGameSignal : Signal<CreateGameRequest> {}

public class SendCreateGameCommand : Command
{
	// Signal Parametrers
	[Inject] public CreateGameRequest CreateGameRequest {get;set;}
	
	// Injected Dependencies
	[Inject] public ClientService ClientService {get;set;}
	[Inject] public PlayerProfile PlayerProfile {get;set;}
	[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
	
    override public void Execute()
    {
		Debug.Log("CreateGameCommand:");
		this.CreateGameRequest.PlayFabSessionToken = this.PlayerProfile.MetaProfile.PlayFabSessionToken;
		this.CreateGameRequest.AvatarId = QueryUtils.GetIdNumber(this.PlayerProfile.MetaProfile.AvatarID);
        this.CreateGameRequest.CardBackId = QueryUtils.GetIdNumber(this.PlayerProfile.MetaProfile.CardBackID);
        this.CreateGameRequest.PlayerName = this.PlayerProfile.MetaProfile.Name;
        this.CreateGameRequest.GameToRejoin = this.PlayerProfile.GameToRejoin;

		if (this.CreateGameRequest.DeckList == null) {
			if (null != this.PlayerProfile.CurrentDeck)
				this.CreateGameRequest.DeckList = this.PlayerProfile.CurrentDeck.DeckList;
		}
		
		this.ClientService.SendMessage(this.CreateGameRequest);
		this.AsyncProgressSignal.Dispatch(AsyncProgress.CreateGameRequest, 0.25f);
    }
    
}
