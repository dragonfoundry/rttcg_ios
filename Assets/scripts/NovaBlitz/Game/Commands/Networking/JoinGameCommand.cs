﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using NovaBlitz.UI;

public class JoinGameSingal : Signal<CreateGameRequest> {}
public class GameJoinedSignal :Signal<JoinGameResponse> {}
public class JoinGameCommand : Command
{
	// Signal Parametrers
	[Inject] public CreateGameRequest CreateGameRequest {get;set;}
	
	// Injected Dependencies
	[Inject] public PlayerProfile PlayerProfile {get; set;}
	[Inject] public ClientService ClientService {get;set;}
	[Inject] public GameJoinedSignal GameJoinedSignal{get;set;}
	[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
	[Inject] public CancelSignal CancelSignal {get;set;}
	[Inject] public CancelFriendChallengeSignal CancelFriendChallengeSignal {get;set;}
	[Inject] public LogEventSignal LogEventSignal {get;set;}
    [Inject] public GameData GameData { get; set; }
    [Inject] public ServerErrorRaisedSignal ServerErrorRaisedSignal { get; set; }
    [Inject] public ShowMainMenuSignal ShowMainMenuSignal { get; set; }

    override public void Execute()
    {
        Debug.Log("JoinGameCommand:");
		JoinGameRequest joinGameRequest = new JoinGameRequest() { GameGuid = GameData.CurrentGameData.GameGuid, PlayerPlayFabSessionToken = this.PlayerProfile.MetaProfile.PlayFabSessionToken };
		//Debug.Log(joinGameRequest.PlayerPlayFabSessionToken);

		this.Retain();
		this.CancelSignal.AddListener(this.OnCanceled);
		this.AsyncProgressSignal.Dispatch(AsyncProgress.JoinGameRequest, 0.35f);
        this.GameJoinedSignal.AddListener(OnJoinGameResponse);
        //this.asyncRequest = this.ClientService.GetResponseAsync<JoinGameResponse>(this.OnJoinGameResponse);
        this.ClientService.SendMessage(joinGameRequest);
        _JoinGameTimeoutCoroutine = JoinGameTimeoutCoroutine();
        Timer.GameInstance.StartCoroutine(_JoinGameTimeoutCoroutine);


    }

    private IEnumerator _JoinGameTimeoutCoroutine { get; set; }
    private IEnumerator JoinGameTimeoutCoroutine()
    {
        yield return new WaitForSecondsRealtime(5f);

        Debug.LogError("join game timeout");
        this.CancelSignal.Dispatch();
    }

    private void OnJoinGameResponse(JoinGameResponse response)
    {
        if(this._JoinGameTimeoutCoroutine != null)
        {
            Timer.GameInstance.StopCoroutine(_JoinGameTimeoutCoroutine);
            _JoinGameTimeoutCoroutine = null;
        }
        this.GameJoinedSignal.RemoveListener(OnJoinGameResponse);
        Debug.Log("OnJoinGameResponse");
        if (response.Error != ErrorCode.NoError)
        {
            this.CancelSignal.Dispatch();
            this.ServerErrorRaisedSignal.Dispatch(response.Error, response);
            return;
        }
		this.AsyncProgressSignal.Dispatch(AsyncProgress.JoinGameRequest, 1.0f);
		
		// Let the game know the details of the game we just joined
        //this.CurrentGameData.PlayerId = response.PlayerId;
		//this.CurrentGameData.PlayerGuid = response.PlayerGuid;
		//this.GameJoinedSignal.Dispatch(this.CurrentGameData);
		
		// Store the current GameGuid in the player Profile when we actually join the game
		this.ClientService.SessionGuid = GameData.CurrentGameData.GameGuid;
		this.CancelSignal.RemoveListener(this.OnCanceled);
		this.Release();
    }

    private void OnCanceled()
    {
        this.GameJoinedSignal.RemoveListener(OnJoinGameResponse);
        if (this._JoinGameTimeoutCoroutine != null)
        {
            Timer.GameInstance.StopCoroutine(_JoinGameTimeoutCoroutine);
            _JoinGameTimeoutCoroutine = null;
        }
        Debug.Log("JoinGameCommand: OnCancled");
		if(this.CreateGameRequest.GameFormat == GameFormat.Challenge)
		{
			this.CancelFriendChallengeSignal.Dispatch(this.CreateGameRequest.PlayFabIdToChallenge);
        }
        this.ShowMainMenuSignal.Dispatch(new ShowMainMenuParams { IsCancelMatchmaking = true });
        //this.ClientService.StopAsyncRequest(this.asyncRequest);
        this.CancelSignal.RemoveListener(this.OnCanceled);

        this.ClientService.IsInGame = false;
        //this.ClientService.Disconnect();
        this.Fail();
		this.LogEventSignal.Dispatch (new LogEventParams {
			EventName = LogEventParams.EVENT_TIME_WAIT_FOR_GAME, 
			IsTimedEvent = true, 
			IsTimedEventOver = true,
			Format = CreateGameRequest.GameFormat,
			EventDetails = new Dictionary<string,string>{ {"StartCanceled", "true"}}
		});
        this.Release();
    }
}
