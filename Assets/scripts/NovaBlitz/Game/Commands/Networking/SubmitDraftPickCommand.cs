﻿using UnityEngine;
using System.Collections;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using DragonFoundry.GameState;
using NovaBlitz.UI;

public class SubmitDraftPickSignal : Signal <DraftPick> {}
public class SubmitDraftPickCommand : Command
{
	// Signal Parameters
	[Inject] public DraftPick DraftPick {get;set;}
	
	// Injected Dependencies
	[Inject] public ClientService ClientService {get;set;}

    override public void Execute()
    {		
		this.ClientService.SendMessage(this.DraftPick);
    }
}
