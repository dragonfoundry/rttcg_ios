﻿using UnityEngine;
using System.Collections;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using NovaBlitz.UI;
using NovaBlitz.Economy;

public class PaidGameNewOrResumeCommand : Command 
{
	// Signal Parameters
	[Inject] public StartPaidGameSessionParams StartGameSessionParams {get;set;}

	// Injected Dependencies
	[Inject] public PlayerProfile PlayerProfile {get; set;}
	[Inject] public ClientService ClientService {get;set;}
	[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
	[Inject] public CancelSignal CancelSignal {get;set;}
	[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
	[Inject] public DraftSessionLoadedSignal DraftSessionLoadedSignal {get;set;}
    [Inject] public ProcessDraftPickEventsSignal ProcessDraftPickEventsSignal { get; set; }
    [Inject] public StartGameSessionSignal StartGameSessionSignal { get; set; }
    [Inject] public PaidGameNewOrResumedSignal PaidGameNewOrResumedSignal { get; set; }
    [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }

    [Inject] public PlayScreen PlayScreen { get;set;}
	[Inject] public ViewReferences ViewReferences { get;set;}
	[Inject] public LogEventSignal LogEventSignal {get;set;}
    [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }


    //private IEnumerator asyncRequest;

    override public void Execute()
    {
		Debug.LogFormat("PaidGameNewOrResumeCommand: {0}", StartGameSessionParams.Format.ToString());

        // Initialize a create game reqeust
        PaidGameNewOrResume paidGameRequest = new PaidGameNewOrResume()
        {
            PlayFabSessionToken = this.PlayerProfile.MetaProfile.PlayFabSessionToken,
            Format = StartGameSessionParams.Format,
            Currency = StartGameSessionParams.Currency,
            ExpectedPaymentAmount = StartGameSessionParams.ExpectedPrice
        };
        if (StartGameSessionParams.DeckToPlay != null)
        {
            paidGameRequest.DeckList = StartGameSessionParams.DeckToPlay.DeckList;
            paidGameRequest.DeckName = StartGameSessionParams.DeckToPlay.Name;
        }
        if(StartGameSessionParams.TournamentToPlay != null)
        {
            paidGameRequest.TournamentId = StartGameSessionParams.TournamentToPlay.ID;
            paidGameRequest.Leaderboard = StartGameSessionParams.TournamentToPlay.Leaderboard;
            paidGameRequest.Prizes = StartGameSessionParams.TournamentToPlay.Prizes;
            paidGameRequest.StartTime = StartGameSessionParams.TournamentToPlay.StartTimeUTC;
        }
			
		this.Retain();
		this.CancelSignal.AddListener(this.OnCanceled);
		this.AsyncProgressSignal.Dispatch(AsyncProgress.PaidGameNewOrResumeRequest, 0.5f);
        this.PaidGameNewOrResumedSignal.AddListener(OnPaidGameNewOrResumeResponse);
        this.ClientService.SendMessage(paidGameRequest);
	}

    private void OnCanceled()
    {
		Debug.Log("PaidGameNewOrResumeCommand: OnCanceled");
        this.PaidGameNewOrResumedSignal.RemoveListener(OnPaidGameNewOrResumeResponse);
		this.CancelSignal.RemoveListener(this.OnCanceled);
        this.Fail();
        this.Release();
        this.ClientService.IsInGame = false;
    }

    private void OnPaidGameNewOrResumeResponse(PaidGameNewOrResumeResponse response)
    {
        this.ClientService.IsInGame = false;
        this.PaidGameNewOrResumedSignal.RemoveListener(OnPaidGameNewOrResumeResponse);
        Debug.Log("OnPaidGameNewOrResumeResponse");
        if (response.Error == ErrorCode.NoError)
        {
            CurrencyType currency = CurrencyType.xx;
            int price = 0;
            this.AsyncProgressSignal.Dispatch(AsyncProgress.PaidGameNewOrResumeRequest, 1.0f);
            if (response.PaymentAmounts != null)
            {
                foreach (var kvp in response.PaymentAmounts)
                {
                    currency = kvp.Key;
                    price = kvp.Value;
                    if (PlayerProfile.Wallet.Currencies.ContainsKey(kvp.Key))
                    {
                        int prevBalance;
                        PlayerProfile.Wallet.Currencies.TryGetValue(kvp.Key, out prevBalance);
                        int newBalance = prevBalance - kvp.Value;
                        PlayerProfile.Wallet.Currencies[kvp.Key] = newBalance;
                        Debug.LogFormat("Paid Game: change by {0}{1} to {2} from {3}", -kvp.Value, kvp.Key, newBalance, prevBalance);

                        this.LogEventSignal.Dispatch(new LogEventParams{
							EventName = LogEventParams.EVENT_VIRTUAL_PURCHASE, 
							CurrencyType = kvp.Key, 
							CurrencyBalance = newBalance,
							EventValue = kvp.Value,
							ItemId = response.Format.ToString(), 
							ItemClass = "event_entry"});
                    }
                }
            }

            switch (response.Format)
            {
                // If we've successfully started a draft, start drafting
                case GameFormat.Draft:
                    this.PlayerProfile.DraftData = response.CurrentStatus;
                    this.ProcessDraftPickEventsSignal.Dispatch();
                    this.DraftSessionLoadedSignal.Dispatch();
                    break;
                // If we've successfully started a tournament, go to matchmaking
                case GameFormat.Constructed:
                    // TODO Update the player's constructed deck!!!
                    this.PlayerProfile.ConstructedData = response.CurrentStatus;
                    var startGameSessionData = new StartGameSessionObject { GameFormat = response.Format, PlayFabIdToChallenge = string.Empty };
                    this.StartGameSessionSignal.Dispatch(startGameSessionData);
                    this.PlayerProfile.LeagueDeck = StartGameSessionParams.DeckToPlay;
                    break;
                case GameFormat.Tournament:
                case GameFormat.Monthly:
                case GameFormat.Annual:
                    // TODO Update the player's event deck!!!
                    this.PlayerProfile.TournamentData = response.CurrentStatus;
                    var startEventGameSessionData = new StartGameSessionObject { GameFormat = response.Format, PlayFabIdToChallenge = string.Empty };
                    this.StartGameSessionSignal.Dispatch(startEventGameSessionData);
                    this.PlayerProfile.CurrentTournament = StartGameSessionParams.TournamentToPlay;
                    break;
                default:
                    break;
            }
            this.ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);

            var deckListView = (DeckListView)ViewReferences.Get(typeof(DeckListView));
            if (deckListView != null)
            {
                this.CloseAnimatedViewSignal.Dispatch(typeof(DeckListView));
                //deckListView.IsOpen = false;
            }

            this.MetricsTrackingService.TrackVirtualPurchase(currency, price, null, response.Format);
        }
        else
        {
            Debug.LogErrorFormat("Paid Game New Or Resume error: {0}", response.Error.ToString());
        }
        this.CancelSignal.RemoveListener(this.OnCanceled);
		this.Release();
    }
	
}
