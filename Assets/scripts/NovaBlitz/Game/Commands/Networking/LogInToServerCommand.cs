﻿using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using System;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using UnityEngine;
using NovaBlitz.Game;
using Messages;

namespace NovaBlitz.UI
{ 
    public class LogInToServerSignal : Signal { }

    public class LogInToServerCommand : Command
    {
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }

        public override void Execute()
        {
            this.Retain();
            if(string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.PlayFabSessionToken))
            {
                Debug.Log("Session token empty on login to server");
            }
            this.ClientService.SendMessage(new PlayerLogInRequest
            {
                PlayFabSessionToken = this.PlayerProfile.MetaProfile.PlayFabSessionToken,
                CurrentClientDataVersion = "goirejgoisfoishfoihgfoishfoishf",
                Platform = AssetBundleManager.PLATFORM,
            });
            this.Release();
        }
    }
}
