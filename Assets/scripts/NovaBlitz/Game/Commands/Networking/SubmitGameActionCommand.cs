﻿using UnityEngine;
using System.Collections;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using DragonFoundry.GameState;
using NovaBlitz.UI;

public class SubmitGameActionSignal : Signal<GameAction>{}
public class SubmitGameActionCommand : Command
{
	// Signal Parameters
	[Inject] public GameAction GameAction {get;set;}
	
	// Injected Dependencies
	[Inject] public ClientService ClientService {get;set;}
	
	override public void Execute()
    {
        /*if(GameAction.LegalAbility != null)
        {
            GameAction.LegalAbility.LegalToPlayAbility = false;
        }*/

		GameActionMessage actionMessage = new GameActionMessage();
		actionMessage.GameGuid = this.ClientService.SessionGuid;
		actionMessage.AbilityId = this.GameAction.AbilityId;
		actionMessage.SourceId = this.GameAction.SourceId;
		actionMessage.TargetIdLists = this.GameAction.TargetIdLists;
		
		this.ClientService.SendMessage(actionMessage);
	}
}