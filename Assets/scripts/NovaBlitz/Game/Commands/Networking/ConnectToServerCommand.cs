﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using System;
using System.Net;
using NovaBlitz.Game;
using NovaBlitz.UI;

public class ConnectToServerSignal : Signal<string,Int32> {} // Hostname , Port
public class ServerConnectedSignal : Signal<bool,string> {} // isConnected, reason

public class ConnectToServerCommand : Command
{
	// Signal Parameters
	[Inject] public string Hostname {get;set;}
	[Inject] public Int32 Port {get;set;}
	
	// Injected Dependences
	[Inject] public ClientService ClientService {get;set;}
	[Inject] public ServerConnectedSignal ServerConnectedSignal {get;set;}
    [Inject] public ServerErrorRaisedSignal ServerErrorRaisedSignal { get; set; }
    [Inject] public LogInToServerSignal LogInToServerSignal { get; set; }

    public override void Execute()
	{
		Debug.Log("Connecting to server " + this.Hostname + ":" + this.Port);	
		this.Retain();
		this.ClientService.ConnectToServerAsync(this.Hostname, this.Port, this.OnFinishedConnecting);
	}	
	
	private void OnFinishedConnecting(bool isConnected, string reason)
	{
		if(isConnected)
        {
            this.ClientService.IsSessionReady = true;
            this.LogInToServerSignal.Dispatch();
            this.Release();
		}
		else
        {
            //this.ServerErrorRaisedSignal.Dispatch(Messages.ErrorCode.ConnectionFailed, new Messages.EventMessage { ErrorCode = Messages.ErrorCode.ConnectionFailed });
            this.Fail();
            this.Release();
		}
		this.ServerConnectedSignal.Dispatch(isConnected,reason);
	}
}
