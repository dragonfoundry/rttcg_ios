﻿using UnityEngine;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using NovaBlitz.UI;

namespace NovaBlitz.Game
{
    public class PlayerLoggedInSignal : Signal<PlayerLogInResponse> { } // bool is "isAuthenticated"
    public class PlayerLoggedInCommand : Command
    {
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }
        [Inject] public PlayerLogInResponse response { get; set; }
        [Inject] public LoadTitleDataSignal LoadTitleDataSignal { get; set; }
        [Inject] public ServerErrorRaisedSignal ServerErrorRaisedSignal { get; set; }

        public override void Execute()
        {
            if (response == null || !response.LogInSuccess)
            {
                if (response == null || response.Error == ErrorCode.NoError)
                {
                    // something went wrong; most likely an out-of-date session token. So disconnect & log out the user.
                    Debug.LogError("Log in failure. Something went wrong; disconnect & log out the user.");
                    this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect); // we're not un-linking the device here.
                }
                else
                {
                    // actual error code - needs to be handled
                    Debug.LogErrorFormat("Log in failure. Error code {0}", response.Error);
                    this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.DontReconnect); // we're not un-linking the device here.
                    //ServerErrorRaisedSignal.Dispatch(response.Error, response);
                }
            }
            else if (!response.DataUpToDate)
            {
                this.LoadTitleDataSignal.Dispatch();
                // get new PlayFab data
                Debug.LogError("Client data is out of date");
            }
            else
                Debug.Log("Player Logged In To Edge Server.");
        }
    }
}
