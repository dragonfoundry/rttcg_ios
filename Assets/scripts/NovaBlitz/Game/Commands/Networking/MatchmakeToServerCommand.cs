﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using DragonFoundry.Networking;
using DragonFoundry.Controllers.Display;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using LinqTools;
using System;
using NovaBlitz.Game;

public class MatchmakeToServerSignal : Signal { }
public class MatchmakeToServerCommand : Command
{
	// Injected Dependencies
	[Inject] public ClientService ClientService {get;set;}
	[Inject] public ConnectToServerSignal ConnectToServerSignal{get;set;}
	[Inject] public ClientPrefs ClientPrefs { get; set;}
	[Inject] public CancelSignal CancelSignal {get;set;}
	[Inject] public ServerConnectedSignal ServerConnectedSignal { get;set;}
	[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
    [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }

    public override void Execute()
	{
		//Debug.LogFormat("Matchmake to server command:{0}", System.DateTime.Now.ToLongTimeString());
        // check to see if we're already connected - if so, we're good.
        if (ClientService.IsConnected)
        {
            Debug.Log("Already Connected");
            return;
        }

		this.Retain();
		this.CancelSignal.AddListener(this.OnCanceled);
        this.ServerConnectedSignal.AddListener(this.OnConnected);
        // Pick a random matchmaking server (for right now, there's only one; later there could be more) - PB
        DNSDataContract server;
		if(ClientPrefs.DNSoptions != null && ClientPrefs.DNSoptions.Count >= 1)
		{
			server = ClientPrefs.DNSoptions[ClientPrefs.random.Next(0, ClientPrefs.DNSoptions.Count)];
		}
		else
		{
			this.OnCanceled();
			return;
		}

        //this.CancelSignal.RemoveListener(this.OnCanceled);
        // We release here so that the next command in the sequence can fire
        //this.Release();

#if (UNITY_EDITOR && !UNITY_IOS && !UNITY_ANDROID)
        string local = System.Environment.GetEnvironmentVariable("NOVABLITZ_DEV_LOCALHOST");
        // No pints will now be owed; only defaults to local for Paul! And now only on his PC!
        if (Application.platform == RuntimePlatform.WindowsEditor && !string.IsNullOrEmpty(local))
			server.Hostname = "localhost";
#elif DEVELOPMENT_BUILD && !UNITY_IOS && !UNITY_ANDROID
        string localh = System.Environment.GetEnvironmentVariable("NOVABLITZ_DEV_LOCALHOST");
        if(!string.IsNullOrEmpty(localh))
            server.Hostname = "localhost";
        Debug.LogFormat("Test Build. connection to {0}",server.Hostname);
#endif
        //server.Hostname = "localhost";
        //server.Hostname = "calitest2-elbNBEdg-TWSUGK2T5YLW-2039330635.us-west-1.elb.amazonaws.com";
        //server.IP = "172.31.99.136";
        // Then we start the connection command, the a subsequent command in the sequence can learn
        // handle the ServerConnectedSignal when it finisihes
        this.ConnectToServerSignal.Dispatch(server.Hostname, Int32.Parse(server.Port));
	}

    private void OnCanceled()
    {
		Debug.Log("MatchmakeToSErverCommand: OnCanceled");
		this.CancelSignal.RemoveListener(this.OnCanceled);
        this.ServerConnectedSignal.RemoveListener(this.OnConnected);
        this.ClientService.Disconnect();
        this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect);
        //this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));

        // NEED TO LOG OUT PLAYER HERE
        //Debug.LogError("NEED TO LOG OUT PLAYER HERE, but not disassociate the player's device ID - it's not a real logout (could just close the app if it fails to connect)");
        this.Fail();
        this.Release();
    }

    private void OnConnected(bool isConnected, string reason)
    {
        if(!isConnected)
        {
            Debug.LogError("Matchmake to server. not connected.");
            OnCanceled();
            return;
        }
        else
        {

            Debug.Log("Matchmake to server. CONNECTED!");
        }
        this.ServerConnectedSignal.RemoveListener(this.OnConnected);
        this.CancelSignal.RemoveListener(this.OnCanceled);
        this.Release();
    }
}
