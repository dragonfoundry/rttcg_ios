﻿using UnityEngine;
using System.Collections;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using NovaBlitz.UI;

public class ConcedeGameSignal : Signal {}
public class ConcedeGameCommand : Command  
{
	// Injected Dependencies
	[Inject] public ClientService ClientService {get;set;}
	
	override public void Execute()
    {
		//Debug.Log("ConcedeGameCommand");
        Messages.ConcedeGameRequest request = new Messages.ConcedeGameRequest();
		request.GameGuid = this.ClientService.SessionGuid;
		this.ClientService.SendMessage(request);
    }
}
