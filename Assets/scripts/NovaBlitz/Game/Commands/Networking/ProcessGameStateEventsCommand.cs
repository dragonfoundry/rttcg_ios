﻿using UnityEngine;
using System.Collections;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using LinqTools;
using DragonFoundry.GameState;
using DragonFoundry.Networking;
using NovaBlitz.UI;

public class DiffGameStateMessageSignal : Signal<DiffGameStateMessage> { }
public class GameEventMessageSignal : Signal<EventMessage> { }
public class CombatStateMessageSignal : Signal<CombatState> { }
public class GameStateInitializedSignal : Signal<GameStateReadOnly> { }
public class LegalAbilitiesUpdatedSignal : Signal<GameStateReadOnly> { }
public class PlayerStateUpdatedSignal : Signal<INovaState> { }
public class OpponentStateUpdatedSignal : Signal<INovaState> { }
public class PhaseChangedSignal : Signal<Phase, int, GameStateReadOnly> { } // Phase, turn#, gamestate
public class CardStateUpdatedSignal : Signal<ICardState, GameStateReadOnly> { }

public class ProcessGameStateEventsSignal : Signal { }
public class ProcessGameStateEventsCommand : Command
{
    // Signal Parameters
    //[Inject] public CreateGameRequest CreateGameRequest {get;set;}
    //[Inject] public CurrentGameData CurrentGameData {get;set;}

    // Injected Dependencies
    [Inject] public GameStateInitializedSignal GameStateInitializedSignal { get; set; }
    [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal { get; set; }
    [Inject] public GameEventMessageSignal GameEventMessageSignal { get; set; }
    [Inject] public CombatStateMessageSignal CombatStateMessageSignal { get; set; }
    [Inject] public LegalAbilitiesUpdatedSignal LegalAbilitiesUpdatedSignal { get; set; }
    [Inject] public PlayerStateUpdatedSignal PlayerStateUpdatedSignal { get; set; }
    [Inject] public OpponentStateUpdatedSignal OpponentStateUpdatedSignal { get; set; }
    [Inject] public CardStateUpdatedSignal CardStateUpdatedSignal { get; set; }
    [Inject] public PhaseChangedSignal PhaseChangedSignal { get; set; }
    [Inject] public GameControllerService GameControllerService { get; set; }
    [Inject] public CancelSignal CancelSignal { get; set; }
    [Inject] public ShowMainMenuSignal ShowMainMenuSignal { get; set; }
    [Inject] public GameData GameData { get; set; }
    [Inject] public ExportGameLogSignal ExportGameLogSignal { get; set; }
    [Inject] public PlayerProfile PlayerProfile { get; set; }
    [Inject] public ClientService ClientService { get; set; }
    

    // =====================
    // Private Properties
    // =====================
    private GameStateContainer gameState;
    private DiffGameStateAdapter diffAdapter;
    private Phase previousPhase = Phase.NoPhase;

    override public void Execute()
    {
        this.gameState = new GameStateContainer();
        InitializeGameLog(gameState);

        this.diffAdapter = new DiffGameStateAdapter();
        Debug.LogFormat("Processing Game Events!");
        this.Retain();

        this.GameControllerService.GameState = this.gameState; // Give the service a refrence to the current gamestate
        this.GameStateInitializedSignal.Dispatch((GameStateReadOnly)this.gameState);
        this.DiffGameStateMessageSignal.AddListener(this.OnDiffGamestateMessage);
        this.CancelSignal.AddListener(OnGameCancelOrEnd);
        this.ShowMainMenuSignal.AddListener(OnShowMainMenu);

        // Initialize the players (avatar, cardback, name, starting health)
        INovaState opponentState = new MatchableState
        {
            PlayerName = GameData.CurrentGameData.OpponentName,
            Id = GameData.CurrentGameData.OpponentId,
            AvatarId = GameData.CurrentGameData.OpponentAvatar,
            CardBackId = GameData.CurrentGameData.OpponentCardBack,
            Health = GameData.CurrentGameData.OpponentStartingHealth,
            StartingHealth = GameData.CurrentGameData.OpponentStartingHealth,
        };
        INovaState selfState = new MatchableState
        {
            PlayerName = GameData.CurrentGameData.PlayerName,
            Id = GameData.CurrentGameData.PlayerId,
            CardBackId = GameData.CurrentGameData.PlayerCardBack,
            Health = GameData.CurrentGameData.PlayerStartingHealth,
            StartingHealth = GameData.CurrentGameData.PlayerStartingHealth,
        };


        this.PlayerStateUpdatedSignal.Dispatch(selfState);
        this.OpponentStateUpdatedSignal.Dispatch(opponentState);
    }

    private void InitializeGameLog(GameStateContainer gameState)
    {
        gameState.GameLog = new GameLog(GameData.CurrentGameData.PlayerPlayerData, GameData.CurrentGameData.OppPlayerData);
        this.PlayerProfile.GameLogs.Add(gameState.GameLog);
        // Trim list to last 50 games on this machine
        while (PlayerProfile.GameLogs.Count > 50)
        {
            this.PlayerProfile.GameLogs.RemoveAt(0);
        }
    }

    private void OnShowMainMenu(ShowMainMenuParams Params)
    {
        OnGameCancelOrEnd();
    }

    private void OnGameCancelOrEnd()
    {
        this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGamestateMessage);
        this.CancelSignal.RemoveListener(OnGameCancelOrEnd);
        this.ShowMainMenuSignal.RemoveListener(OnShowMainMenu);
        this.ClientService.IsInGame = false;
        this.Fail();
        this.Release();
    }

    private void OnDiffGamestateMessage(DiffGameStateMessage diffMessage)
    {
        if(diffMessage.SomethingHasHappened)
        { 
            // Diff GameState processing
            this.diffAdapter.AdaptGameState(this.gameState, diffMessage);

            // Did CardState change?
            if (diffMessage.CardStates != null)
            {
                foreach (ICardState cardState in diffMessage.CardStates)
                {
                    this.CardStateUpdatedSignal.Dispatch(cardState, this.gameState);
                }
            }

            // Did the localPlayer state change?
            if (diffMessage.YouState != null)
            {
                INovaState selfState = diffMessage.YouState;
                this.PlayerStateUpdatedSignal.Dispatch(selfState);
            }

            // Did the opponent state change?
            if (diffMessage.OpponentState != null)
            {
                INovaState opponentState = diffMessage.OpponentState;
                this.OpponentStateUpdatedSignal.Dispatch(opponentState);
            }

            isNewEventLog = false;
            // Event Messsage processing 
            if (diffMessage.GameEvents != null)
            {
                foreach (EventMessage message in diffMessage.GameEvents)
                {
                    this.GameEventMessageSignal.Dispatch(message);
                    ProcessEventLog(message);
                }
            }

            // Combat State processing 
            if (diffMessage.CombatStates != null)
            {
                foreach (CombatState combatState in diffMessage.CombatStates)
                {
                    this.CombatStateMessageSignal.Dispatch(combatState);
                }
            }

            // Did the Turn Phase Change?
            if (this.gameState.Turn.Phase != this.previousPhase)
            {
                if (this.previousPhase == Phase.Combat) // turn off all the attacking/blocking callouts
                {
                    foreach (var card in gameState.Cards)
                    {
                        card.IsAttacking = false;
                        card.IsBlocking = false;
                    }
                }
                this.PhaseChangedSignal.Dispatch(this.gameState.Turn.Phase, diffMessage.GameTurnNumber, this.gameState);
                this.previousPhase = this.gameState.Turn.Phase;
                if (this.gameState.Turn.Phase == Phase.Action)
                {
                    ProcessEventLog(new EventMessage { EventType = EventTypes.StartOfTurnEvent });
                }
                else if (this.gameState.Turn.Phase == Phase.Combat)
                {
                    ProcessEventLog(new EventMessage { EventType = EventTypes.StartOfCombatEvent });
                }
            }


            // Check for end of game
            if (this.gameState.Turn.Phase == Phase.GameOver)
            {
                this.gameState.LegalAbilities.MakeAllIllegal();
                this.LegalAbilitiesUpdatedSignal.Dispatch(this.gameState);
                this.CancelSignal.RemoveListener(OnGameCancelOrEnd);
                this.ShowMainMenuSignal.RemoveListener(OnShowMainMenu);
                this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGamestateMessage);
                Debug.LogWarning("Turn Phase is GameOver");
                this.Release();
                var you = gameState.GetPlayer(gameState.SelfId);
                var opp = gameState.GetPlayer(gameState.OpponentId);
                var loserId = you != null && you.HasWon ? gameState.OpponentId : (opp != null && opp.HasWon ? gameState.SelfId : 0);
                if (this.gameState != null && this.gameState.GameLog != null)
                {
                    this.ProcessEventLog(new EventMessage { EventType = EventTypes.LoseGameEvent, Affected = loserId });
                    gameState.GameLog.IsPlayerWinner = loserId == gameState.OpponentId;
                    this.ExportGameLogSignal.Dispatch();
                }
            }
        }


        // Did LegalAbilities change?
        if (diffMessage.LegalAbilities != null)
        {
            //Debug.LogFormat("Legal Abilities (shh): {0}/{1} cast abilities/legal", diffMessage.LegalAbilities.Count(a => a.AbilityType == AbilityType.Cast), diffMessage.LegalAbilities.Count(a => a.AbilityType == AbilityType.Cast && a.LegalToPlayAbility));
            this.LegalAbilitiesUpdatedSignal.Dispatch(this.gameState);
        }
    }

    Phase previousLogPhase = Phase.Action;
    bool isNewEventLog = false;
    int hangingHealthCost = 0;
    int hangingHealthCostPayer = 0;

    private void ProcessEventLog(EventMessage msg)
    {
        if(msg == null)
        {
            Debug.LogErrorFormat("Null message processed");
            return;
        }
        var logData = gameState.GameLog.CurrentLogData;
        try
        {
            switch (msg.EventType)
            {
                case EventTypes.LoseGameEvent:
                // Handle this one from the gameovermessage.
                // needs an image & to identify winner & loser.
                case EventTypes.StartOfTurnEvent:
                case EventTypes.StartOfCombatEvent:
                // Roll these two up under the other events. Do it here, because we always want to track these.
                // If one event would get both, create a second event to space them out.
                // make the only phases that come through "Action" or "Combat"
                // ensure we update player health at phase change
                // Need a "first event" with a start of turn.
                case EventTypes.ActivateEvent:
                case EventTypes.ResolveEvent:
                case EventTypes.AttackingUnit:
                    this.gameState.GameLog.CurrentLogData = new GameLogData(
                        msg.EventType,
                        previousLogPhase,
                        gameState.Turn == null ? 1 : gameState.Turn.TurnNumber,
                        msg.Source,
                        (msg.EventType == EventTypes.ActivateEvent || msg.EventType == EventTypes.ResolveEvent) ? msg.Amount : msg.Affected,
                        gameState.Self.Id,
                        gameState.Opponent == null ? (gameState.Self.Id == 1 ? 2 : 1) : gameState.Opponent.Id,
                        gameState.Self.Health - gameState.Self.Damage,
                        gameState.Opponent == null ? 0 : gameState.Opponent.Health - gameState.Opponent.Damage);
                    logData = this.gameState.GameLog.CurrentLogData;
                    isNewEventLog = true;
                    if (msg.EventType == EventTypes.ResolveEvent && hangingHealthCost > 0)
                    {
                        if (hangingHealthCostPayer == logData.SelfId)
                        {
                            logData.Affected = logData.SelfId;
                            logData.AffectedControllerId = logData.SelfId;
                            logData.AffectedDamage += hangingHealthCost;
                        }
                        else if (hangingHealthCostPayer == logData.OppId)
                        {
                            logData.Affected = logData.OppId;
                            logData.AffectedControllerId = logData.OppId;
                            logData.AffectedDamage += hangingHealthCost;
                        }
                    }
                    hangingHealthCost = 0;
                    hangingHealthCostPayer = 0;
                    this.gameState.GameLog.LogElements.Add(logData);
                    previousLogPhase = msg.EventType == EventTypes.StartOfTurnEvent ? Phase.Action : (msg.EventType == EventTypes.StartOfCombatEvent ? Phase.Combat : previousLogPhase);
                    if (logData.Affected <= 2)
                    {
                        logData.AffectedControllerId = logData.Affected;
                    }
                    else if (logData.Affected > 2)
                    {
                        // find the cards' cardIds and cardArtIds so we can pull card art
                        var card = gameState.GetCard(logData.Affected);
                        if (card != null)
                        {
                            logData.AffectedCardId = card.CardId;
                            logData.AffectedArtId = card.GetArtId();
                            logData.AffectedControllerId = card.ControllerId;
                        }
                    }
                    if (logData.Source <= 2)
                    {
                        logData.SourceControllerId = logData.Source;
                    }
                    else if (logData.Source > 2)
                    {
                        // find the cards' cardIds and cardArtIds so we can pull card art
                        var card = gameState.GetCard(logData.Source);
                        if (card != null)
                        {
                            logData.SourceCardId = card.CardId;
                            logData.SourceArtId = card.GetArtId();
                            logData.SourceControllerId = card.ControllerId;
                        }
                    }
                    logData.InitiativeId = gameState.Self.HasCombatAdvantage ? gameState.Self.Id : (gameState.Opponent != null && gameState.Opponent.HasCombatAdvantage ? gameState.Opponent.Id : 0);
                    break;
                default:
                    if (logData == null)
                        break;
                    else if (logData.EventType != EventTypes.AttackingUnit
                        && logData.EventType != EventTypes.ResolveEvent
                        && logData.EventType != EventTypes.ActivateEvent)
                    {
                        if (msg.EventType == EventTypes.CombatDamageEvent || msg.EventType == EventTypes.DamageEvent || msg.EventType == EventTypes.BlockingUnit)
                            Debug.LogErrorFormat("MORE IMPORTANT: {0} skipped. Log Event is {0}", msg.EventType, logData.EventType);
                        //else
                        //    Debug.LogWarningFormat("Less important: {0} skipped. Log Event is {0}", msg.EventType, logData.EventType);
                        break;
                    }
                    switch (msg.EventType)
                    {
                        case EventTypes.BlockingUnit:
                            // switch the attack affected to the blocker.
                            if (logData.EventType == EventTypes.AttackingUnit)
                            {
                                logData.Affected = msg.Source;
                                if (logData.Affected <= 2)
                                {
                                    logData.AffectedControllerId = logData.Affected;
                                    logData.AffectedArtId = null;
                                    logData.AffectedDamage = 0;
                                }
                                else if (logData.Affected > 2)
                                {
                                    // find the cards' Ids and cardArtIds so we can pull card art
                                    var card = gameState.GetCard(logData.Affected);
                                    if (card != null)
                                    {
                                        logData.AffectedCardId = card.CardId;
                                        logData.AffectedArtId = card.GetArtId();
                                        logData.AffectedControllerId = card.ControllerId;
                                        logData.AffectedDamage = 0;
                                    }
                                }
                            }
                            break;
                        case EventTypes.DamageEvent:
                        case EventTypes.CombatDamageEvent:
                            //logData.RelevantEvents.Add(msg);
                            if (msg.Affected == logData.Affected)
                            {
                                logData.AffectedDamage += msg.Amount;
                            }
                            else if (msg.Affected == logData.Source)
                            {
                                logData.SourceDamage += msg.Amount;
                            }
                            if (!isNewEventLog) // Only capture stray damage from events that come in in future event logs
                            {
                                if (msg.Affected == logData.SelfId)
                                    logData.SelfHealth -= msg.Amount;
                                else if (msg.Affected == logData.OppId)
                                    logData.OppHealth -= msg.Amount;
                            }
                            break;
                        case EventTypes.HealthCostEvent:
                            // memorize this to display on the NEXT card resolve event, since it will come in ahead of that
                            // assume we won't get multiple costs paid by different players without an intervening card resolution
                            hangingHealthCost += msg.Amount;
                            hangingHealthCostPayer = msg.Affected;
                            // health drop can occur immediately since it's displayed on a continuous time bar
                            if (msg.Affected == logData.SelfId)
                                logData.SelfHealth -= msg.Amount;
                            else if (msg.Affected == logData.OppId)
                                logData.OppHealth -= msg.Amount;
                            break;
                        case EventTypes.HealEvent:
                            //logData.RelevantEvents.Add(msg);
                            if (!isNewEventLog) // Only capture stray damage from events that come in in future event logs
                            {
                                if (msg.Affected == logData.SelfId)
                                    logData.SelfHealth += msg.Amount;
                                else if (msg.Affected == logData.OppId)
                                    logData.OppHealth += msg.Amount;
                            }
                            break;
                        case EventTypes.ZoneChangeEvent:
                            //logData.RelevantEvents.Add(msg);
                            switch (msg.ToZone)
                            {
                                case (int)CardZone.Trash:
                                case (int)CardZone.Void:
                                    if (msg.Affected == logData.Affected)
                                        logData.IsAffectedTrashed = true;
                                    else if (msg.Affected == logData.Source)
                                    {
                                        var card = gameState.GetCard(logData.Source);
                                        logData.IsSourceTrashed = card == null || !card.IsPower;
                                    }
                                    break;
                            }
                            break;
                        case EventTypes.ControlChangeEvent:
                            //logData.RelevantEvents.Add(msg);
                            break;
                    }
                    break;
            }
        }
        catch(System.Exception ex)
        {
            MetricsTrackingService.SavedException = string.Format("ProcessGameLog. eventtype:{0} gameState:{1} self:{2} opp:{3} Exception: {4}",
                msg.EventType,
                gameState == null ? "null" : "ok",
                gameState == null || gameState.Self == null ? "null" : "ok",
                gameState == null || gameState.Opponent == null ? "null" : "ok",
                ex.ToString());
            Debug.Log("Process game log error");
        }
    }
}
