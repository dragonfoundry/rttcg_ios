﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using System.Collections.Generic;
using NovaBlitz.Game;
using System.Net;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;
using LinqTools;

public class WaitForCreateGameCommand : Command
{
	// Signal Parametrers
	[Inject] public CreateGameRequest CreateGameRequest {get;set;}
	
	// Injected Dependencies
    [Inject] public GameData GameData { get; set; }
    [Inject] public PlayerProfile PlayerProfile { get; set; }
	[Inject] public ClientService ClientService {get;set;}
	[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
	[Inject] public CancelMatchmakingSignal CancelMatchmakingSignal {get;set; }
    [Inject] public CancelSignal CancelSignal { get; set; }
    [Inject] public CancelFriendChallengeSignal CancelFriendChallengeSignal {get;set;}
    [Inject] public GameCreatedSignal GameCreatedSignal { get; set; }
	[Inject] public HideCancelProgressButtonSignal HideCancelProgressButtonSignal {get;set;}
	[Inject] public SetProgressScreenStatusLabelSignal SetProgressScreenStatusLabelSignal {get;set;}
    [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
	[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
	[Inject] public LogEventSignal LogEventSignal {get;set;}
    [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }
    [Inject] public ServerErrorRaisedSignal ServerErrorRaisedSignal { get; set; }
    [Inject] public NovaAudio NovaAudio { get; set; }

    private IEnumerator _TickerCoroutine;

    override public void Execute()
    {
		Debug.Log("WaitForCreateGameResponse: ");
		this.Retain();
        this.GameCreatedSignal.AddListener(this.OnCreateGameResponse);
		this.CancelMatchmakingSignal.AddListener(this.OnCanceled);
		
		if(this.CreateGameRequest.GameFormat != GameFormat.Practice)
		{
			this.SetProgressScreenStatusLabelSignal.Dispatch(I2.Loc.ScriptLocalization.AsyncProgress.CreateGameRequest);
        }
        _TickerCoroutine = TickerCoroutine();
        Timer.Instance.StartCoroutine(_TickerCoroutine);
    }

    private IEnumerator TickerCoroutine()
    {
        int i = 0;
        while(true)
        {
            yield return new WaitForSecondsRealtime(1.0f);
            i++;
            Debug.LogFormat("Waited {0}s for create game", i);
        }
    }

    private void OnCanceled()
    {
        Debug.Log("WaitForCreateGameCommand: OnCanceled");
        if(_TickerCoroutine != null)
            Timer.Instance.StopCoroutine(_TickerCoroutine);
        this.ClientService.SendMessage(new CancelMatchmakingRequest());
		if(this.CreateGameRequest.GameFormat == GameFormat.Challenge)
		{
			this.CancelFriendChallengeSignal.Dispatch(this.CreateGameRequest.PlayFabIdToChallenge);
        }
        this.GameCreatedSignal.RemoveListener(this.OnCreateGameResponse);
        this.CancelMatchmakingSignal.RemoveListener(this.OnCanceled);
        this.NovaAudio.CrossfadeToMusicMenu();
        //this.ClientService.Disconnect();
        
        this.LogEventSignal.Dispatch (new LogEventParams {
			EventName = LogEventParams.EVENT_TIME_WAIT_FOR_GAME, 
			IsTimedEvent = true, 
			IsTimedEventOver = true,
			Format = CreateGameRequest.GameFormat,
			EventDetails = new Dictionary<string,string>{ {"StartCanceled", "true"}}
		});
        this.Fail();
        this.Release();
        this.CancelSignal.Dispatch();
    }

    private void OnCreateGameResponse(CreateGameResponse createGameResponse)
    {
        if (_TickerCoroutine != null)
            Timer.Instance.StopCoroutine(_TickerCoroutine);
        this.GameCreatedSignal.RemoveListener(this.OnCreateGameResponse);
        if (createGameResponse.Error != ErrorCode.NoError)
        {
            Debug.LogErrorFormat("Create Game {0} Error: {5}, Received: {1}, Matched: {2}, Sent: {3} Guid: {4}", createGameResponse.GameFormat, createGameResponse.RequestReceived.ToString("hh:mm:ss.fff"), createGameResponse.MatchMade.ToString("hh:mm:ss.fff"), createGameResponse.ResponseSent.ToString("hh:mm:ss.fff"), createGameResponse.CreatedGameGuid, createGameResponse.Error);

            if (createGameResponse.Error == ErrorCode.IllegalDeck)
            {
                var decklist = CreateGameRequest.DeckList == null ? "null" : string.Format("{0}c:[{1}]", CreateGameRequest.DeckList.Count, string.Join(",", CreateGameRequest.DeckList.Select(c => c.ToString()).ToArray()));
                var currentDeck = this.PlayerProfile.CurrentDeck == null || this.PlayerProfile.CurrentDeck.DeckList == null ? "null"
                    : this.PlayerProfile.CurrentDeck.ToString();
                var error = string.Format("Illegal deck error. Format:{0} Decklist:{1} CurrentDeck:{2}", CreateGameRequest.GameFormat, decklist, currentDeck);
                MetricsTrackingService.SavedException = error;
                Debug.Log(error);
            }
            this.CancelMatchmakingSignal.Dispatch();
            this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
            this.ServerErrorRaisedSignal.Dispatch(createGameResponse.Error, createGameResponse);
            return;
        }
        else
        {
            Debug.LogFormat("OnCreateGameResponse {0}, Received:{1}, Matched:{2}, Sent:{3} Guid:{4} Opp:{5}", createGameResponse.GameFormat, createGameResponse.RequestReceived.ToString("hh:mm:ss.fff"), 
                createGameResponse.MatchMade.ToString("hh:mm:ss.fff"), 
                createGameResponse.ResponseSent.ToString("hh:mm:ss.fff"), 
                createGameResponse.CreatedGameGuid, 
                string.IsNullOrEmpty(createGameResponse.OpponentPlayFabId) ? "AI" : createGameResponse.OpponentPlayFabId);
        }
        var currentGameData = new CurrentGameData
        {
            GameGuid = createGameResponse.CreatedGameGuid,
            OpponentName = createGameResponse.OpponentName,
            OpponentId = createGameResponse.OpponentId,
            OpponentAvatar = createGameResponse.OpponentAvatarId,
            OpponentCardBack = createGameResponse.OpponentCardBackId,
            PlayerName = createGameResponse.PlayerName,
            PlayerId = createGameResponse.PlayerId,
            PlayerAvatar = createGameResponse.PlayerAvatarId,
            PlayerCardBack = createGameResponse.PlayerCardBackId,
            OpponentStartingHealth = createGameResponse.OpponentStartingHealth,
            PlayerStartingHealth = createGameResponse.PlayerStartingHealth,
            PlayerStats = createGameResponse.PlayerStats,
            OpponentStats = createGameResponse.OpponentStats,
            OpponentPlayFabId = createGameResponse.OpponentPlayFabId,
            GameFormat = createGameResponse.GameFormat,
        };
        if(currentGameData.GameFormat == GameFormat.Tutorial)
        {
            currentGameData.TutorialAttackData = new Dictionary<int, List<TutorialCombatData>>();
            foreach(var kvp in this.GameData.TutorialCombatDictionary)
            {
                var list = new List<TutorialCombatData>();
                foreach(var tad in kvp.Value)
                {
                    list.Add(tad.Clone());
                }
                currentGameData.TutorialAttackData.Add(kvp.Key, list);
            }

            int tutorialNumber = this.PlayerProfile.OnboardingProgress.CurrentTutorial;
            TutorialDataContract contract;
            if (this.GameData.TutorialDictionary.TryGetValue(tutorialNumber, out contract))
            {
                var titleArray = new string[]
                {
                I2.Loc.ScriptLocalization.TutorialScreen.Title_1,
                I2.Loc.ScriptLocalization.TutorialScreen.Title_1,
                I2.Loc.ScriptLocalization.TutorialScreen.Title_2,
                I2.Loc.ScriptLocalization.TutorialScreen.Title_3,
                };
                if (tutorialNumber > 0 & tutorialNumber < titleArray.Length)
                {
                    currentGameData.OpponentName = titleArray[tutorialNumber];
                    currentGameData.OpponentAvatar = -tutorialNumber;
                }
                if (currentGameData.OpponentStats == null)
                {
                    currentGameData.OpponentStats = new Dictionary<string, int>();
                }
                currentGameData.OpponentStats[NBEconomy.PLAYER_STATS_MMR] = tutorialNumber + 6;
                currentGameData.OpponentStats[NBEconomy.PLAYER_STATS_RANK] = 31 - (tutorialNumber + 6);
            }
        }
        InitializePlayerData(currentGameData, false);
        InitializePlayerData(currentGameData, true);

        this.MetricsTrackingService.TrackGameStart(currentGameData.GameFormat);


        GameData.CurrentGameData = currentGameData;
        this.AsyncProgressSignal.Dispatch(AsyncProgress.CreateGameRequest, 1.2f);
		this.CancelMatchmakingSignal.RemoveListener(this.OnCanceled);
		this.HideCancelProgressButtonSignal.Dispatch();
		this.SetProgressScreenStatusLabelSignal.Dispatch(string.Empty);
        this.Release();
    }

    private void InitializePlayerData(CurrentGameData currentGameData, bool isOpponent)
    {

        AvatarDataContract avContract;
        string avatarArtId = string.Empty;
        string cardBackArtId = string.Empty;
        if (this.GameData.AvatarDictionary.TryGetValue(isOpponent ? currentGameData.OpponentAvatar : currentGameData.PlayerAvatar, out avContract))
        {
            avatarArtId = avContract.ArtId;
        }
        CardBackDataContract cbContract;
        if (GameData.CardBackDictionary.TryGetValue(isOpponent ? currentGameData.OpponentCardBack : currentGameData.PlayerCardBack, out cbContract))
        {
            cardBackArtId = cbContract.ArtId;
        }
        var tournamentData = new TournamentData();

        var stats = isOpponent ? currentGameData.OpponentStats : currentGameData.PlayerStats;
        if (stats == null)
            stats = new Dictionary<string, int>();

        int qps;
        stats.TryGetValue("qp", out qps);
        int event_wins;
        int event_losses;
        stats.TryGetValue("event_wins", out event_wins);
        stats.TryGetValue("event_losses", out event_losses);
        tournamentData.event_wins = event_wins;
        tournamentData.event_losses = event_losses;

        string playFabId = isOpponent ? currentGameData.OpponentPlayFabId : this.PlayerProfile.MetaProfile.PlayFabID;
        string displayName = isOpponent ? currentGameData.OpponentName : currentGameData.PlayerName;
        // Set up the data for the PlayerCard.
        var playerData = new PlayerData(playFabId, displayName, stats, null,
            avatarArtId, cardBackArtId, qps,
            tournamentData, currentGameData.GameFormat, 0, 0, 0, -1, 0, 0, false, null, 0);
        if (isOpponent)
        {
            currentGameData.OppPlayerData = playerData;
        }
        else
        {
            currentGameData.PlayerPlayerData = playerData;
        }

    }

}
