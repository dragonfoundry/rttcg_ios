﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class EmoteMenu : MonoBehaviour
{
    public Action<int> SendEmote;
    public VerticalLayoutGroup LayoutGroup;
    public CanvasGroup LayoutCanvas;
    public float TargetSpacing;

    private NovaButton[] NovaButtons;
    private float ShowMenuTime = 0.2f;
    private float HideMenuTime = 0.2f;

    public bool isOpen { get; set; }
    private Sequence seqence;

    public void Initialize()
    {
        NovaButtons = LayoutGroup.GetComponentsInChildren<NovaButton>();
        this.gameObject.SetActive(false);
    }

    public void ShowMenu()
    {
        if(isOpen)
            return;
        if(seqence != null)
            seqence.Kill();
        LayoutGroup.spacing = TargetSpacing;
        LayoutCanvas.alpha = 0;
        seqence = DOTween.Sequence();
        seqence.AppendCallback(() => this.gameObject.SetActive(true));
        seqence.AppendCallback(() => { foreach (var button in NovaButtons) { button.interactable = true; } });
        seqence.InsertCallback(0.1f, () => isOpen = true);
        seqence.Insert(0, DOTween.To(x => LayoutGroup.spacing = x, TargetSpacing, 0, ShowMenuTime).SetEase(Ease.OutExpo));
        seqence.Insert(0, LayoutCanvas.DOFade(1.0f, ShowMenuTime / 2));
    }
    
    public void OnClickGreetings() { OnClickElement(1); }
    public void OnClickThanks() { OnClickElement(2); }
    public void OnClickWellPlayed() { OnClickElement(3); }
    public void OnClickTaunt() { OnClickElement(4); }
    public void OnClickOops() { OnClickElement(5); }
    public void OnClickSorry() { OnClickElement(6); }

    public void OnClickElement(int element)
    {
        HideMenu();
        // send the message
        if(SendEmote != null)
        {
            SendEmote.Invoke(element);
        }
    }

    public void HideMenu()
    {
        if (seqence != null)
            seqence.Kill();
        isOpen = false;
        seqence = DOTween.Sequence();
        seqence.Insert(0, DOTween.To(x => LayoutGroup.spacing = x, LayoutGroup.spacing, TargetSpacing, HideMenuTime).SetEase(Ease.OutBack));
        seqence.Insert(0, LayoutCanvas.DOFade(0.0f, HideMenuTime / 2));
        seqence.AppendCallback(() => { foreach (var button in NovaButtons) { button.interactable = false; } });
        seqence.AppendCallback(() => this.gameObject.SetActive(false));
    }

    public void EnableMenu()
    {

    }
}

