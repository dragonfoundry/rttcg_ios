﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;
using NovaBlitz.UI;
using NovaBlitz.Game;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections;
using Messages;

public class OpponentControllerView : PlayerControllerBase 
{
    public CardDealer OpponentCardDealer;
    public NovaButton OpponentMenuButton;
    public NovaButton SquelchButton;
    public CanvasGroup SquelchCanvas;
    public TextMeshProUGUI SquelchText;
    public Image Squelched;
    public bool IsSquelchOpen { get; set; }
    protected override float EmoteAnimationDirection { get { return -PlayEmoteDistance; } }

    [Inject] public PlayFxSignal PlayFxSignal {get;set;}


    [ContextMenu("death...")]

	public IEnumerator PerformDeathSequence()
	{
		yield return new WaitForSecondsRealtime(0.5f);
        Debug.Log("Death kids");
        this.Scanlines.gameObject.SetActive(true);
        this.InitiativeFrame.GetComponent<Image>().sprite = this.SmashedFrame;
        this.InitiativeOn.DOFade(0f, 0.1f);
        this.InitiativeBlob.DOFade(0f,0.1f);
        this.InitiativeOrbFx.DOFade(0f, 0.2f);
        this.HealthLabel.DOColor(new Color32(175,33,33,255), 0.2f);
        
        this.PlayFxSignal.Dispatch(new PlayFxParams(0, 0, null, (RectTransform)this.AvatarFrame.transform, null, "fx_impact_crater_m", PlayFxPath.NoPath,0));
        this.PlayFxSignal.Dispatch(new PlayFxParams(0, 0, null, (RectTransform)this.AvatarFrame.transform, null, "fx_smokefizzle_s", PlayFxPath.NoPath,0));

        Sequence seq = DOTween.Sequence();
        seq.Append(this.AvatarFrame.DOLocalRotate(new Vector3(0,0,-15f), 0.15f));
        seq.Join(this.AvatarFrame.DOAnchorPos(new Vector2(-125,70),0.1f));
        seq.Append(this.AvatarFrame.DOLocalRotate(new Vector3(7.5f,-8f,15f), 0.15f));
        seq.Append(this.AvatarFrame.DOLocalRotate(new Vector3(7.5f,-8f,10f), 0.1f));
        seq.Insert(0.3f, this.AvatarFrame.DOAnchorPos(new Vector2(-108,15),0.25f));
        seq.Insert(0.2f, this.Scanlines.DOFade(0.3f,0.5f).SetEase(Ease.InBounce));
        seq.SetDelay(0.4f);
        seq.Play();

        Sequence seq2 = DOTween.Sequence();
         seq2.Append(this.InitiativeFrame.DOAnchorPos(new Vector2(55,-105f),0.25f));
        seq2.Join(this.InitiativeFrame.DOLocalRotate(new Vector3(0,0,20f),0.3f));
        seq2.Append(this.InitiativeFrame.DOAnchorPos(new Vector2(55,-90f),0.2f));
        seq2.Join(this.InitiativeFrame.DOLocalRotate(new Vector3(0,0,15f),0.25f));
        seq2.SetDelay(0.4f);
        seq2.Play();

        this.HealthBarController.HealthBarCracks.DOFade(1,0).SetDelay(0.4f);

        StartCoroutine(FlickerArt());
    }

    private IEnumerator FlickerArt()
    {
        while(true)
        {
            yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(0.02f, 0.8f));
            this.Avatar.DOFade(1f,0);
            yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(0.01f, 0.04f));
            this.Avatar.DOFade(0.75f,0);
        }
    }
    public void OnPointerEnterDeck()
    {
        //Debug.Log("Pointer enter deck");
        this.DeckCountRoot.DOKill();
        this.DeckCountRoot.DOAnchorPosY(-40f, 0.1f).SetEase(Ease.OutQuad).SetDelay(0.15f);
    }
    public void OnPointerExitDeck()
    {
        //Debug.Log("Pointer exit deck");
        this.DeckCountRoot.DOKill();
        this.DeckCountRoot.DOAnchorPosY(30f, 0.15f).SetEase(Ease.OutQuad);
    }

    public void Initialize()
    {
        this.SquelchButton.gameObject.SetActive(false);
    }

    private Sequence sequence;
    public void OpponentMenuButtonClick()
    {
        if (sequence != null)
            sequence.Kill();
        sequence = DOTween.Sequence();
        sequence.InsertCallback(0, () =>
        {
            SquelchButton.interactable = true;
            SquelchButton.gameObject.SetActive(true);
            SquelchCanvas.alpha = 0;
            OpponentMenuButton.gameObject.SetActive(false);
        });
        sequence.Insert(0.01f, SquelchCanvas.DOFade(1f,1f));
        sequence.InsertCallback(0.1f,() => IsSquelchOpen = true);
        sequence.Insert(3f, SquelchCanvas.DOFade(0f, 0.3f));
        sequence.AppendCallback(() => IsSquelchOpen = false);

    }

    public void HideSquelch()
    {
        if (sequence != null)
            sequence.Kill();
        IsSquelchOpen = false;
        sequence = DOTween.Sequence();
        sequence.Append(SquelchCanvas.DOFade(0f, 1.0f));
        sequence.AppendCallback(() => {
            SquelchButton.interactable = false;
            SquelchButton.gameObject.SetActive(false);
            OpponentMenuButton.gameObject.SetActive(true);
        });
    }

    public void OnSquelchClick()
    {
        this.isSquelched = !this.isSquelched;
        Squelched.gameObject.SetActive(isSquelched);
        SquelchButton.interactable = false;
        this.SquelchText.text = isSquelched ? "Unsquelch" : "Squelch";
        SquelchButton.interactable = false;
        HideSquelch();
    }

    private int numFramesActive = 0;
    override protected void Update()
    {
        base.Update();
        if(Input.GetMouseButtonUp(0) && IsSquelchOpen)
        {
            HideSquelch();
        }
        
        bool shouldBeActive = this.TargetArrowTarget.TargetAction == TargetAction.None;
        if (shouldBeActive)
        {
            this.numFramesActive += 1;
        }
        else
        {
            this.OpponentMenuButton.interactable = false;
            this.OpponentMenuButton.GetComponent<Image>().raycastTarget = false;
            this.numFramesActive = 0;
        }

        if (this.numFramesActive > 5)
        {
            this.OpponentMenuButton.interactable = true;
            this.OpponentMenuButton.GetComponent<Image>().raycastTarget = true;
        }

        //Debug.Log("Settings Button:" + this.SettingsPanelButton.interactable + " numFramesActive:" + this.numFramesActive);
    }
}
