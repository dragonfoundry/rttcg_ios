﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using System;
using Messages;
using DragonFoundry.Fx;
using DragonFoundry.Controllers.Display;
using NovaBlitz.UI;
using DG.Tweening;

public class PlayerControllerBase : View 
{

    public int PlayerId;
    public TextMeshProUGUI HealthLabel;
    //public TextMeshProUGUI EnergyLabel;
    public PlayerEnergyBarController EnergyBarController;
    public TextMeshProUGUI NameLabel;
    public TextMeshProUGUI DeckCountLabel;
    public RectTransform DeckCountRoot;
    public TargetArrowTarget TargetArrowTarget;
    public RectTransform AvatarFrame;
    public CardArtImage Avatar;
    public PlayerHealthBarController HealthBarController;
    public RectTransform BlockerLocation;
    public int TargetHealth;
    public Image InitiativeBlob;
    public Sprite SmashedFrame;
    public Image Scanlines;

    [HideInInspector] public INovaState PlayerState;
    private bool hasInitiative = false;
    private Vector2 initiativeDefaultAnchorPos;
    public Image EmoteDisplay;
    public CanvasGroup EmoteCanvas;
    public TextMeshProUGUI EmoteText;
    public NovaAudioPlayer EmoteAudio;
    [HideInInspector] public bool isSquelched;
    private Sequence PlayEmoteSequence = null;
    private ConcurrentQueue<Sequence> EmoteSequences = new ConcurrentQueue<Sequence>();
    protected float PlayEmoteDistance = 160f;
    private float EmoteInTime = 0.5f;
    private float EmoteDisplayTime = 2.0f;
    private float EmoteOutTime = 0.5f;
    protected virtual float EmoteAnimationDirection { get { return PlayEmoteDistance; } }

    // Accessory UI Elements
    public RawImage damageIcon;
    public TextMeshProUGUI damageText;
    public NovaAudioPlayer AttackImpactAudio;
    public CanvasGroup InitiativeOn;
    public RectTransform InitiativeFrame;
    public Sequence InitiativeSequence { get; set; }

    public Dictionary<string, ParticleEffectsTrigger> fxScripts = new Dictionary<string, ParticleEffectsTrigger>(StringComparer.OrdinalIgnoreCase);
    public ParticleEffectsTrigger currentParticleEffects = null;
    public Image InitiativeOrbFx;

    protected override void Start()
    {
        base.Start();
        // Initialize FX
        this.fxScripts.Clear();
        ParticleEffectsTrigger[] particleFx = this.GetComponentsInChildren<ParticleEffectsTrigger>();
        
        for (int i = 0; i < particleFx.Length; i++)
        {
            this.fxScripts.Add(particleFx[i].gameObject.name.ToLower(), particleFx[i]);
        }
        this.currentParticleEffects = null;
        this.InitiativeOn.gameObject.SetActive(true);
        this.InitiativeOn.DOFade(0f,0f);
        this.InitiativeBlob.DOFade(0f,0f);
        this.initiativeDefaultAnchorPos = this.InitiativeFrame.anchoredPosition;
    }

    private float deckSize = -1;
    protected virtual void Update()
    {
        if(this.PlayerState != null && this.PlayerState.DeckSize != this.deckSize)
        {
            this.DeckCountLabel.text = this.PlayerState.DeckSize.ToString();// + "/40";
             this.deckSize = this.PlayerState.DeckSize;
        }
    }


    public void PlayEmote(string emoteText, int emote)
    {
        // if the emote sequence is already playing, queue up the next one
        // play the emote sequence
        // animate & open the emote bubble
        // animate the text
        // animate out the emote bubble
        Color emoteColor;
        if(emote > 0)
        {
            emoteColor = Color.green;
        }
        else if(emote == -1)
        {
            emoteColor = Color.cyan;
        }
        else if(emote == -2)
        {
            emoteColor = new Color(1.0f, 0.6f, 0.0f);
        }
        else
        {
            emoteColor = Color.grey;
        }

        Sequence seq = DOTween.Sequence();
        seq.Pause();
        seq.Append(EmoteDisplay.rectTransform.DOAnchorPos(Vector2.zero, 0));
        seq.Append(EmoteCanvas.DOFade(0, 0));
        seq.Append(EmoteDisplay.rectTransform.DOScale(Vector3.one * 0.5f, 0));
        seq.InsertCallback(0.0f, () =>
        {
            EmoteText.text = emoteText;
            EmoteText.color = emoteColor;
            EmoteDisplay.color = emoteColor;
        });
        seq.InsertCallback(0.0f, () => this.EmoteAudio.Play());
        seq.Insert(0.0f, EmoteDisplay.rectTransform.DOAnchorPos(new Vector2(Math.Abs(EmoteAnimationDirection/3), EmoteAnimationDirection), EmoteInTime).SetEase(Ease.OutBack));
        seq.Insert(0.0f, EmoteCanvas.DOFade(1.0f, EmoteInTime));
        seq.Insert(0.0f, EmoteDisplay.rectTransform.DOScale(Vector3.one, EmoteInTime).SetEase(Ease.OutBack));
        seq.Insert(EmoteInTime + EmoteDisplayTime, EmoteDisplay.rectTransform.DOAnchorPos(Vector2.zero, EmoteOutTime));
        seq.Insert(EmoteInTime + EmoteDisplayTime, EmoteCanvas.DOFade(0.0f, EmoteOutTime /2));
        seq.AppendCallback(() => { PlayEmoteSequence = null; PlayNextEmote(); });
        // re-enable the menu if it's the player controller
        EmoteSequences.Enqueue(seq);
        PlayNextEmote();
    }

    private void PlayNextEmote()
    {
        Sequence toPlay;
        if (PlayEmoteSequence == null && EmoteSequences.TryDequeue(out toPlay))
        {
            PlayEmoteSequence = toPlay;
            toPlay.Play();
        }
        else
        {
            ReactivateEmoteMenu();
        }
    }

    protected virtual void ReactivateEmoteMenu() { }

    float lastDamageTime = 0;
    public void EventDamageTaken(int damage)
    {
        if (damageText == null)
            return;

        if (Time.time - this.lastDamageTime > 1)
        {
            damageText.text = damage.ToString();
            this.lastDamageTime = Time.time;
        }
        else
        {
            damageText.text = (int.Parse(damageText.text) + damage).ToString();
        }

        damageIcon.GetComponent<Animator>().SetTrigger("Show");
    }
    
    public void ApplyTargetHealth()
    {
        this.HealthBarController.SetHealth(this.TargetHealth, 0.5f);
    }

    public virtual void SetInitiative(bool hasInitiative, float blobOffset)
    {
        if(this.hasInitiative == hasInitiative) return;

        this.hasInitiative = hasInitiative;

        RectTransform blobTrans = (RectTransform)this.InitiativeBlob.transform;
        if(InitiativeSequence != null)
        {
            InitiativeSequence.Kill();
            this.InitiativeFrame.anchoredPosition = this.initiativeDefaultAnchorPos;
        }

        if(hasInitiative)
        {
            InitiativeSequence = DOTween.Sequence();
            InitiativeSequence.Append(blobTrans.DOAnchorPosY(blobOffset,0f));
            InitiativeSequence.Join(this.InitiativeBlob.DOFade(1f,0f));
            InitiativeSequence.AppendInterval(0.1f);
            InitiativeSequence.Append(blobTrans.DOAnchorPosY(0f,0.2f));
            InitiativeSequence.Append(this.InitiativeOn.DOFade(1f,0));
            InitiativeSequence.Join(this.InitiativeBlob.DOFade(0f,0));
            InitiativeSequence.Insert(0.25f,this.InitiativeFrame.DOPunchAnchorPos(new Vector3(0,-40f),0.2f,1));
            InitiativeSequence.Play();
        }
        else
        {
            InitiativeSequence = DOTween.Sequence();
            InitiativeSequence.Append(blobTrans.DOAnchorPosY(0f,0f));
            InitiativeSequence.Join(this.InitiativeBlob.DOFade(1f,0f));
            InitiativeSequence.Join(this.InitiativeOn.DOFade(0f, 0.0f));
            InitiativeSequence.Append(blobTrans.DOAnchorPosY(blobOffset,0.2f));
            InitiativeSequence.Append(this.InitiativeBlob.DOFade(0f,0));
            InitiativeSequence.Play();

            // Traveling Fx Orb
            RectTransform orbTransform = (RectTransform)this.InitiativeOrbFx.transform;
            orbTransform.localScale = Vector3.one;
            orbTransform.localPosition = new Vector3(orbTransform.localPosition.x, -77f, orbTransform.localPosition.z);
            this.InitiativeOrbFx.DOFade(1f,0f);
            orbTransform.DOAnchorPosY(900f, 0.35f);
            orbTransform.DOScaleY(5f,0.25f).OnComplete(
                ()=>{
                    orbTransform.DOScaleY(2f,0.1f);
                    this.InitiativeOrbFx.DOFade(0f,0.1f);
                });
        }
    }

    public void InitializeHealthBarSize(int maxHealth)
    {
        this.HealthBarController.InitializeHealthBarSize(maxHealth);
    }
}
