﻿using UnityEngine;
using System.Collections;
using TMPro;
using NovaBlitz.UI;
using NovaBlitz.Game;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;
using UnityEngine.UI;
using Messages;

public class PlayerControllerView : PlayerControllerBase
{
    public TextMeshProUGUI VersionLabel;
    public CardDealer PlayerCardDealer;
    public Button SettingsPanelButton;
    public Button EmoteMenuButton;
    public EmoteMenu EmoteMenu;
    public RectTransform TrickHolder;
    public RectTransform TrickResolver;

    [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
    [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
    [Inject] public ViewReferences ViewReferences {get;set;}
    [Inject] public PlayFxSignal PlayFxSignal {get;set;}

    public bool isEmoteMenuAllowed = true;

    public void OnClickProfileMenu()
    {
        Debug.Log("OnClickProfileMenu TargetArrowTarget:" + this.TargetArrowTarget.TargetAction);
        this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(InGameMenuView));
    }

    public void OnClickEmoteMenu()
    {
        if(this.isEmoteMenuAllowed)
        {
            this.EmoteAudio.Play();
            EmoteMenu.ShowMenu();
        }
    }

    public void ResetEmoteMenu()
    {
        EmoteMenu.HideMenu();
    }

    protected override void ReactivateEmoteMenu()
    {
        this.isEmoteMenuAllowed = true;
    }

    [ContextMenu("death...")]
    public IEnumerator PerformDeathSequence()
    {
		yield return new WaitForSecondsRealtime(0.5f);
        Debug.Log("Death kids");
        this.Scanlines.gameObject.SetActive(true);
        this.InitiativeFrame.GetComponent<Image>().sprite = this.SmashedFrame;
        this.InitiativeOn.DOFade(0f, 0.1f);
        this.InitiativeBlob.DOFade(0f,0.1f);
        this.InitiativeOrbFx.DOFade(0f, 0.2f);
        this.HealthLabel.DOColor(new Color32(175,33,33,255), 0.2f);
        

        this.PlayFxSignal.Dispatch(new PlayFxParams(1, 1, null, (RectTransform)this.AvatarFrame.transform, null, "fx_impact_crater_m", PlayFxPath.NoPath,0));
        this.PlayFxSignal.Dispatch(new PlayFxParams(1, 1, null, (RectTransform)this.AvatarFrame.transform, null, "fx_smokefizzle_s", PlayFxPath.NoPath,0));

        Sequence seq = DOTween.Sequence();
        seq.Append(this.AvatarFrame.DOLocalRotate(new Vector3(0,0,15f), 0.15f));
        seq.Join(this.AvatarFrame.DOAnchorPos(new Vector2(-125,-70),0.1f));
        seq.Append(this.AvatarFrame.DOLocalRotate(new Vector3(-7.5f,-8f,-15f), 0.15f));
        seq.Append(this.AvatarFrame.DOLocalRotate(new Vector3(-7.5f,-8f,-10f), 0.1f));
        seq.Insert(0.3f, this.AvatarFrame.DOAnchorPos(new Vector2(-108,5),0.25f));
        seq.Insert(0.2f, this.Scanlines.DOFade(0.3f,0.5f).SetEase(Ease.InBounce));
        seq.SetDelay(0.4f);
        seq.Play();

        Sequence seq2 = DOTween.Sequence();
        seq2.Append(this.InitiativeFrame.DOAnchorPos(new Vector2(55,-105f),0.25f));
        seq2.Join(this.InitiativeFrame.DOLocalRotate(new Vector3(0,0,20f),0.3f));
        seq2.Append(this.InitiativeFrame.DOAnchorPos(new Vector2(55,-90f),0.2f));
        seq2.Join(this.InitiativeFrame.DOLocalRotate(new Vector3(0,0,15f),0.25f));
        seq2.SetDelay(0.4f);
        seq2.Play();

        this.HealthBarController.HealthBarCracks.DOFade(1,0).SetDelay(0.4f);

        StartCoroutine(FlickerArt());
    }

    private IEnumerator FlickerArt()
    {
        while(true)
        {
            yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(0.02f, 0.8f));
            this.Avatar.DOFade(1f,0);
            yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(0.01f, 0.04f));
            this.Avatar.DOFade(0.75f,0);
        }
    }

    public void OnPointerEnterDeck()
    {
        //Debug.Log("Pointer enter deck");
        this.DeckCountRoot.DOKill();
        this.DeckCountRoot.DOAnchorPosY(100f, 0.1f).SetEase(Ease.OutQuad).SetDelay(0.15f);
    }

    public void OnPointerExitDeck()
    {
        //Debug.Log("Pointer exit deck");
        this.DeckCountRoot.DOKill();
        this.DeckCountRoot.DOAnchorPosY(30f, 0.15f).SetEase(Ease.OutQuad);
    }

    private int numFramesActive = 0;
    override protected void Update()
    {
        base.Update();

        if(Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Menu))
        {   
            InGameMenuView menu = (InGameMenuView)this.ViewReferences.Get(typeof(InGameMenuView));
            StartingHandView startingHand = (StartingHandView)this.ViewReferences.Get(typeof(StartingHandView));
            EndGameDialogView endGame = (EndGameDialogView)this.ViewReferences.Get(typeof(EndGameDialogView));
            OpenPrizeView prizeView = (OpenPrizeView)this.ViewReferences.Get(typeof(OpenPrizeView));
            ProgressLoadingScreenView loadingView = (ProgressLoadingScreenView)this.ViewReferences.Get(typeof(ProgressLoadingScreenView));
            if ((menu == null || menu.IsOpen == false) 
                && (startingHand == null || startingHand.IsOpen == false)
                && (endGame == null || endGame.IsOpen == false)
                && (prizeView == null || prizeView.IsOpen == false)
                && (loadingView == null || loadingView.IsOpen == false))
            {
                this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(InGameMenuView));
            }
            else if(Input.GetKeyUp(KeyCode.Escape))
            {
                var view = this.ViewReferences.ViewStateStack.TryPeekViewState();
                if (view is EndGameDialogView || view is OpenPrizeView || view is StartGameDialogView || view is StartingHandView || view is ProgressLoadingScreenView)
                {
                }
                else if(view != null)
                {
                    this.CloseAnimatedViewSignal.Dispatch(view.GetType());
                }
            }
        }

        if (Input.GetMouseButtonUp(0) && EmoteMenu.isOpen)
        {
            EmoteMenu.HideMenu();
        }

        bool shouldBeActive = this.TargetArrowTarget.TargetAction == TargetAction.None;
        if(shouldBeActive)
        {
            this.numFramesActive += 1;
        }
        else
        {
            this.EmoteMenuButton.interactable = false;
            this.EmoteMenuButton.GetComponent<Image>().raycastTarget = false;
            this.numFramesActive = 0;
        }

        if(this.numFramesActive > 5)
        {
            this.EmoteMenuButton.interactable = true; 
            this.EmoteMenuButton.GetComponent<Image>().raycastTarget = true;
        }
       
        //Debug.Log("Settings Button:" + this.SettingsPanelButton.interactable + " numFramesActive:" + this.numFramesActive);
    }
}
