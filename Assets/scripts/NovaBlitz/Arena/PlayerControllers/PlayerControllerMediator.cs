﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using DragonFoundry.GameState;
using System;
using NovaBlitz;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Game;
using Messages;

public class PlayerControllerMediator : Mediator
{
    [Inject] public PlayerControllerView View { get; set; }
    [Inject] public GameStateInitializedSignal GameStateInitializedSignal { get; set; }
    [Inject] public PlayerDamageTakenSignal PlayerDamageTakenSignal { get; set; }
    [Inject] public PlayerStateUpdatedSignal PlayerStateUpdatedSignal { get; set; }
    [Inject] public ArenaPlayers ArenaPlayers { get; set; }
    [Inject] public GameData GameData { get; set; }
    [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}
    [Inject] public SelfEmoteSignal SelfEmoteSignal { get; set; }
    [Inject] public EmoteSignal EmoteSignal { get; set; }
    [Inject] public InitiativeChangeSignal InitiativeChangeSignal {get;set;}
    [Inject] public GameOverSignal GameOverSignal {get;set;}

    private bool isInitialized = false;
    
    override public void OnRegister()
    {
        this.GameStateInitializedSignal.AddListener(this.OnGameStateInitialzied);
        this.PlayerStateUpdatedSignal.AddListener(this.OnPlayerStateUpdated);
        this.PlayerDamageTakenSignal.AddListener(this.View.EventDamageTaken);
        this.DiffGameStateMessageSignal.AddListener(this.OnDiffGamestateMessage);
        this.SelfEmoteSignal.AddListener(this.OnEmoteSignal);
        this.View.EmoteMenu.SendEmote += OnSendEmote;
        this.View.EmoteMenu.Initialize();
        this.InitiativeChangeSignal.AddListener(this.OnInitiativeChange);
    }
    
    override public void OnRemove()
    {
        this.GameStateInitializedSignal.RemoveListener(this.OnGameStateInitialzied);
        this.PlayerStateUpdatedSignal.RemoveListener(this.OnPlayerStateUpdated);
        this.PlayerDamageTakenSignal.RemoveListener(this.View.EventDamageTaken);
        this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGamestateMessage);
        this.SelfEmoteSignal.RemoveListener(this.OnEmoteSignal);
        this.View.EmoteMenu.SendEmote -= OnSendEmote;
        this.InitiativeChangeSignal.RemoveListener(this.OnInitiativeChange);
    }

    private void OnInitiativeChange(int playerId, ICardState sourceCardState)
    {
        bool hasInitiative = playerId == this.GameData.CurrentGameData.PlayerId;
        //Debug.Log("#PlayerControllerMediator# Player " + playerId +  " (" + this.GameData.CurrentGameData.PlayerId + ") hasInitiative:" + hasInitiative);
        this.View.SetInitiative(hasInitiative,100f);
    }

    private void OnSendEmote(int emote)
    {
        this.View.isEmoteMenuAllowed = false;
        this.View.EmoteMenu.HideMenu();
        //this.View.EmoteMenuButton.interactable = false;
        this.EmoteSignal.Dispatch(emote);
    }

    string EmoteAvatarName = "Emote/a{0}/{1}";
    string EmoteName = "Emote/{0}";
    private void OnEmoteSignal(int emote)
    {
        // forced emotes are -ve numbers
        if (emote > 0 && View.isSquelched)
            return;
        string emoteText = I2.Loc.ScriptLocalization.Get(string.Format(EmoteAvatarName, GameData.CurrentGameData.PlayerAvatar, emote));
        if (string.IsNullOrEmpty(emoteText))
            emoteText = I2.Loc.ScriptLocalization.Get(string.Format(EmoteName, emote));
        if (!string.IsNullOrEmpty(emoteText))
            this.View.PlayEmote(emoteText, emote);

		if(emote == -2)
        {
            // Important that this is performed on the main Timer, so stopping coroutines on the game timer doesn't affect it
            Timer.Instance.StartCoroutine(this.View.PerformDeathSequence());
		}
    }
    
    private void OnDiffGamestateMessage(DiffGameStateMessage diffMessage)
    {
        if(diffMessage.SomethingHasHappened && diffMessage.YouState != null)
        {
            int numCardsRemaining = diffMessage.YouState.DeckSize;
            //Debug.Log("Your DeckSize:" + numCardsRemaining);
            this.View.PlayerCardDealer.NumCardsRemaining = numCardsRemaining;
        }
    }

    private void OnPlayerStateUpdated(INovaState playerState)
    {
        this.View.NameLabel.text = playerState.PlayerName;
        AvatarDataContract avatar;
        if (GameData.AvatarDictionary.TryGetValue(playerState.AvatarId, out avatar))
        {
            this.View.Avatar.UpdateCardArt(avatar.ArtId, CardAspect.NoAspect);
        }

        if(this.View.EnergyBarController != null)
        {
            //this.View.EnergyBarController.SetCapacity(playerState.MaxEnergy);
            //this.View.EnergyBarController.SetEnergy(playerState.CurrentEnergy);
            this.View.EnergyBarController.SetEnergyAndCapacity(playerState.MaxEnergy, playerState.CurrentEnergy);
        }

        this.View.PlayerState = playerState;
        this.View.TargetArrowTarget.Id = playerState.Id;
        this.View.PlayerId = playerState.Id;
        this.ArenaPlayers.Player = this.View;

        if(this.isInitialized == false)
        {
            this.View.HealthBarController.InitializeHealthBarSize(playerState.StartingHealth);
            this.View.HealthBarController.SetHealth(playerState.Health, 0f);
            this.isInitialized = true;
        }
        else
        {
            int newTargetHealth = playerState.Health - playerState.Damage;
            
            if(newTargetHealth > this.View.TargetHealth)
            {
                // Appply target health immediately for heals
                this.View.TargetHealth = newTargetHealth;
                this.View.ApplyTargetHealth();
            }
            else
            {
                this.View.TargetHealth = newTargetHealth;
            }
        }
        
    }
    
    private void OnGameStateInitialzied(GameStateReadOnly gameState)
    {
        if (this.View is PlayerControllerView)
        {
            (this.View as PlayerControllerView).VersionLabel.text = this.GameData.VersionLabel;
        }

        this.View.HealthBarController.InitializeHealthBarSize(GameData.CurrentGameData.PlayerStartingHealth);
        this.isInitialized = true;
    }
}
