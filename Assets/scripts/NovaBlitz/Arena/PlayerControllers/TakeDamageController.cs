﻿using UnityEngine;
using System.Collections;

public class TakeDamageController : MonoBehaviour 
{
	public PlayerControllerBase PlayerControllerBase;
	
	public void OnDamageBurstEvent()
	{
		this.PlayerControllerBase.ApplyTargetHealth();
	}
}
