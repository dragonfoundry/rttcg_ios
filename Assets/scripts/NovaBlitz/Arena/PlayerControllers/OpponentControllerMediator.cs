﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using DragonFoundry.GameState;
using System;
using Messages;
using NovaBlitz;
using NovaBlitz.Game;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;

public class OpponentControllerMediator : Mediator
{
    [Inject] public OpponentControllerView View {get;set;}
    [Inject] public GameStateInitializedSignal GameStateInitializedSignal {get;set;}
    [Inject] public OpponentDamageTakenSignal OpponentDamageTakenSignal {get;set;}
    [Inject] public ArenaPlayers ArenaPlayers {get;set;}
    
    [Inject] public OpponentStateUpdatedSignal OpponentStateUpdatedSignal {get;set;}
    [Inject] public GameData GameData { get; set; }
    [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}
    [Inject] public OpponentEmoteSignal OpponentEmoteSignal { get; set; }
    [Inject] public InitiativeChangeSignal InitiativeChangeSignal {get;set;}
    [Inject] public GameOverSignal GameOverSignal {get;set;}

    private bool isInitialized = false;
    
    override public void OnRegister()
    {
        this.GameStateInitializedSignal.AddListener(this.OnGameStateInitialzied);
        this.OpponentStateUpdatedSignal.AddListener(this.OnOpponentStateUpdated);
        this.OpponentDamageTakenSignal.AddListener(this.View.EventDamageTaken);
        this.DiffGameStateMessageSignal.AddListener(this.OnDiffGamestateMessage);
        this.OpponentEmoteSignal.AddListener(this.OnEmoteSignal);
        this.View.Initialize();
        this.InitiativeChangeSignal.AddListener(this.OnInitiativeChange);
    }
    
    override public void OnRemove()
    {
        this.GameStateInitializedSignal.RemoveListener(this.OnGameStateInitialzied);
        this.OpponentStateUpdatedSignal.RemoveListener(this.OnOpponentStateUpdated);
        this.OpponentDamageTakenSignal.RemoveListener(this.View.EventDamageTaken);
        this.DiffGameStateMessageSignal.RemoveListener(this.OnDiffGamestateMessage);
        this.OpponentEmoteSignal.RemoveListener(this.OnEmoteSignal);
        this.InitiativeChangeSignal.RemoveListener(this.OnInitiativeChange);
    }

    private void OnInitiativeChange(int playerId, ICardState sourceCardState)
    {
        bool hasInitiative = playerId == this.GameData.CurrentGameData.OpponentId;
        //Debug.Log("#OpponentControllerMediator# Player " + playerId + " (" + this.GameData.CurrentGameData.OpponentId + ") hasInitiative:" + hasInitiative);
        this.View.SetInitiative(hasInitiative,-100f);
    }

    string EmoteAvatarName = "Emote/a{0}/{1}";
    string EmoteName = "Emote/{0}";
    private void OnEmoteSignal(int emote)
    {
        // forced emotes are -ve numbers
        if (emote > 0 && View.isSquelched)
            return;
        
        string emoteText = I2.Loc.ScriptLocalization.Get(string.Format(EmoteAvatarName, GameData.CurrentGameData.OpponentAvatar, emote));
        if(string.IsNullOrEmpty(emoteText))
            emoteText = I2.Loc.ScriptLocalization.Get(string.Format(EmoteName, emote));
        if (!string.IsNullOrEmpty(emoteText))
            this.View.PlayEmote(emoteText, emote);

		if(emote == -2)
		{
            // Important that this is performed on the main Timer, so stopping coroutines on the game timer doesn't affect it
			Timer.Instance.StartCoroutine(this.View.PerformDeathSequence());
		}
    }


    private void OnDiffGamestateMessage(DiffGameStateMessage diffMessage)
    {
        if(diffMessage.SomethingHasHappened && diffMessage.OpponentState != null)
        {
            int numCardsRemaining = diffMessage.OpponentState.DeckSize;
            //Debug.Log("Opponent DeckSize:" + numCardsRemaining);
            this.View.OpponentCardDealer.NumCardsRemaining = numCardsRemaining;
        }
    }


    private const string TUTORIAL_TITLE = "TutorialScreen/Title_";

    private void OnOpponentStateUpdated(INovaState opponentState)
    {
        AvatarDataContract avatar;
        if (GameData.AvatarDictionary.TryGetValue(opponentState.AvatarId, out avatar))
        {
            this.View.Avatar.UpdateCardArt(avatar.ArtId, CardAspect.NoAspect);
        }

        TutorialDataContract tutorial;
        if (opponentState.AvatarId < 0 && GameData.TutorialDictionary.TryGetValue(-opponentState.AvatarId, out tutorial))
        {
            string title = I2.Loc.ScriptLocalization.Get(TUTORIAL_TITLE + tutorial.TutorialId);
            this.View.NameLabel.text = string.IsNullOrEmpty(title) ? tutorial.Title : title;
        }
        else
            this.View.NameLabel.text = opponentState.PlayerName;

        if(this.View.EnergyBarController != null)
        {
            this.View.EnergyBarController.SetEnergyAndCapacity(opponentState.MaxEnergy, opponentState.CurrentEnergy);
        }
        else
        {
            //this.View.EnergyLabel.text = "<u>" + opponentState.CurrentEnergy + "</u>\n<size=-25>" + opponentState.MaxEnergy;
        }
        this.View.PlayerState = opponentState;
        this.View.TargetArrowTarget.Id = opponentState.Id;
        this.View.PlayerId = opponentState.Id;
        this.ArenaPlayers.Opponent = this.View;
        
        if(this.isInitialized == false)
        {
            this.View.HealthBarController.InitializeHealthBarSize(opponentState.StartingHealth);
            this.View.HealthBarController.SetHealth(opponentState.Health, 0f);
            this.isInitialized = true;
        }
        else
        {
            int newTargetHealth = opponentState.Health - opponentState.Damage;
            
            if(newTargetHealth > this.View.TargetHealth)
            {
                // Appply target health immediately for heals
                this.View.TargetHealth = newTargetHealth;
                this.View.ApplyTargetHealth();
            }
            else
            {
                this.View.TargetHealth = newTargetHealth;
            }
        }
    }
    
    private void OnGameStateInitialzied(GameStateReadOnly gameState)
    {
        this.isInitialized = true;
        this.View.HealthBarController.InitializeHealthBarSize(GameData.CurrentGameData.OpponentStartingHealth);
    }
}
