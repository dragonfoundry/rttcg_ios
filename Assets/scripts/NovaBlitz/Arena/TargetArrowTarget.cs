﻿using UnityEngine;
using System.Collections;

public enum TargetType
{
    Unit,
    Opponent,
    Self
}

public enum TargetAction
{
    None,
    Block,
    Attack,
    Activate,
    Affected,
}

public class TargetArrowTarget : MonoBehaviour 
{
    public TargetType TargetType;
    public TargetAction TargetAction = TargetAction.None;
    public RectTransform BoundsRect;
    public int Id;
    public Animator TargetGlowAnimator;
}
