
using UnityEngine;
using System.Collections;
using System;
using NovaBlitz.UI;
using System.Collections.Generic;
using DragonFoundry.Fx;

public class FxTrigger : FxBase
{
    //FX
    public RectTransform targetTransform { get; set; }
    private Vector3 _TargetPosition { get; set; }

    // Pool state
    public override PooledObjectType PooledObjectType { get { return PooledObjectType.FxTrigger; } }

    public void Initialize(INovaContext ContextView)
    {
        _ParticleBounds = ContextView.ParticleBounds;
        SetScaleAndPosition();
    }

    void Update()
    {
        if(this.currentParticleEffects != null)
        {
            if(this.wasStarted == false)
            {
                this.wasStarted = this.currentParticleEffects.particles.IsAlive();
            }
           
            if(this.wasStarted)
            {
                if (this.currentParticleEffects.particles.IsAlive() == false)
                {
                    //Debug.Log("Recycling fx " + this.currentParticleEffects.gameObject.name);
                    this.currentParticleEffects.gameObject.SetActive(false);
                    this.currentParticleEffects = null;
                    this.targetTransform = null;
                    this.Recycle();
                }
                else if(targetTransform != null && targetTransform.gameObject.activeInHierarchy)
                {
                    SetScaleAndPosition();
                    SetSortLayer(currentParticleEffects);
                }
            }
        }
    }
    
    public virtual void SetScaleAndPosition()
    {
        // ensure scale matches scale of target
        if (10 * targetTransform.lossyScale.x != fxTransform.lossyScale.x && fxTransform.parent != null)
        {
            float factor = 10 * targetTransform.lossyScale.x / fxTransform.parent.lossyScale.x;
            fxTransform.localScale = Vector3.one * factor;
        }

        // ensure location matches location of target
        //var worldThis = fxTransform.TransformPoint(fxTransform.rect.center);
        //var worldTarget = targetTransform.TransformPoint(targetTransform.rect.center);

        
        if (targetTransform.position != _TargetPosition)
        {
            //var localTarget = fxTransform.InverseTransformPoint(worldTarget);
            //fxTransform.localPosition = localTarget;
            var newTargetPosition = GetFlatPlaneXY(targetTransform.position);
            var screen2 = RectTransformUtility.WorldToScreenPoint(uiCamera, newTargetPosition);
            Vector3 worldPoint = fxTransform.position;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(targetTransform, screen2, uiCamera, out worldPoint);
            fxTransform.position = worldPoint;
            _TargetPosition = targetTransform.position;
        }
    }
    
    protected override IEnumerator WaitToStartFx(ParticleEffectsTrigger particleFx)
    {
        if (this.targetTransform != null)
        {
            _TargetCanvas = this.targetTransform.GetComponentInParent<Canvas>();
            _SortOrder = int.MinValue;
            _SortLayer = string.Empty;
            _OverrideSorting = false;
            SetSortLayer(particleFx);
        }
        yield return null;

        particleFx.gameObject.SetActive(true);
        particleFx.StartEffect();
        //Debug.LogWarning("StartedEffect from FxTrigger " + particleFx.gameObject.name);
        this.currentParticleEffects = particleFx;
    }

    protected void SetSortLayer(ParticleEffectsTrigger particleFx)
    {

        if (_TargetCanvas != null && (_SortOrder != _TargetCanvas.sortingOrder || _SortLayer != _TargetCanvas.sortingLayerName || _OverrideSorting != _TargetCanvas.overrideSorting))
        {
            _SortOrder = _TargetCanvas.sortingOrder;
            _SortLayer = _TargetCanvas.sortingLayerName;
            _OverrideSorting = _TargetCanvas.overrideSorting;
            int sortOrder = _SortOrder + 1;// Math.Max(20, _SortOrder + 1);
            var particles = particleFx.GetComponentsInChildren<ParticleSystem>();
            for (int i = 0; i < particles.Length; i++)
            {
                var renderer = particles[i].GetComponent<Renderer>();
                renderer.sortingLayerName = _OverrideSorting ? _SortLayer : "Default";
                renderer.sortingOrder = sortOrder;
            }
            var trails = particleFx.GetComponentsInChildren<TrailRenderer>();
            for (int i = 0; i < trails.Length; i++)
            {
                trails[i].sortingLayerName = _OverrideSorting ? _SortLayer : "Default";
                trails[i].sortingOrder = sortOrder;
            }
            //Debug.LogFormat("#FX# Sorting {0} in {1}", sortOrder, _TargetCanvas.sortingLayerName);
        }
    }

    public override void OnRecycle()
    {
        targetTransform = null;
        _TargetPosition = Vector3.zero;
        currentParticleEffects = null;
        wasStarted = false;
        _TargetCanvas = null;
    }
}