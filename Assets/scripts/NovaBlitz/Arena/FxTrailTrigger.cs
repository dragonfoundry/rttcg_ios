﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using NovaBlitz.Game;
using DragonFoundry.Fx;
using Messages;
using NovaBlitz.UI;

public class FxTrailTrigger : FxBase
{
    public RectTransform rotator;
    public RectTransform emitter;
    public ParticleEffectsTrigger TestTrail;
    public Canvas TrailCanvas;
    
    public void Initialize(PlayFxParams Params, INovaContext ContextView)
    {
        _ParticleBounds = ContextView.ParticleBounds;
        _SourceTransform = Params.Source;
        _TargetTransform = Params.Destination;
        _DurationS = Params.DurationS;
        _TimeLeftS = _DurationS;
        _Path = Params.Path;
        _IsPlaying = false;
        //_Params = Params;
    }

    private RectTransform _SourceTransform;
    private RectTransform _TargetTransform;
    //private PlayFxParams _Params;
    public bool _IsPlaying;
    private Vector3 _ExpectedTarget;
    private float _DurationS;
    private DateTime _StartTime;
    private float _TimeLeftS;
    private PlayFxPath _Path;
    private Sequence _Sequence;
    private Vector3 _LastPosition;

    private const float zHeight = 90f;

    // Pool state
    public override PooledObjectType PooledObjectType { get { return PooledObjectType.FxTrailTrigger; } }

    float DistanceToTarget { get {
            var sourcePos = _FxFlatPosition;
            var targetPos = _TargetFlatPosition;
            return new Vector3(targetPos.x - sourcePos.x, targetPos.y - sourcePos.y).magnitude;
        } }
    Vector3 PathToTarget { get
        {
            var fxPos = _FxFlatPosition;
            var targetPos = _TargetFlatPosition;
            return new Vector3(targetPos.x - fxPos.x, targetPos.y - fxPos.y); } }

    private Vector3 _FxFlatPosition { get { return GetFlatPlaneXY(fxTransform.position); } }
    private Vector3 _SourceFlatPosition {  get { return GetFlatPlaneXY(_SourceTransform.position); } }
    private Vector3 _TargetFlatPosition { get { return GetFlatPlaneXY(_TargetTransform.position); } }

    private void SetFxTransformStartPosition()
    {
        float xMod = UnityEngine.Random.Range(1f, 4f) * (UnityEngine.Random.Range(0, 2) == 0 ? -1 : 1);
        float yMod = UnityEngine.Random.Range(2f, 6f) * (UnityEngine.Random.Range(0, 2) == 0 ? -1 : 1);
        var sourceFlatPos = _SourceFlatPosition;
        fxTransform.position = new Vector3(sourceFlatPos.x + xMod, sourceFlatPos.y + yMod, zHeight);
    }

    protected override IEnumerator WaitToStartFx(ParticleEffectsTrigger particleFx)
    {
        var sourceFlatPos = _SourceFlatPosition;
        SetFxTransformStartPosition();
        particleFx.transform.localScale = Vector3.one;
        if (PathToTarget == Vector3.zero || particleFx == null)
        {
            Debug.LogFormat("#TrailFX# Path to target is zero");
            yield break;
        }


        if (this._TargetTransform != null)
        {
            var canvas = this._TargetTransform.GetComponentInParent<Canvas>();
            if (canvas != null)
            {
                int sortOrder = 500;// Math.Max(60,canvas.sortingOrder + 1);
                var particles = particleFx.GetComponentsInChildren<ParticleSystem>();
                for (int i = 0; i < particles.Length; i++)
                {
                    var renderer = particles[i].GetComponent<Renderer>();
                    renderer.sortingLayerName = canvas.sortingLayerName;
                    renderer.sortingOrder = sortOrder;
                }
                var trails = particleFx.GetComponentsInChildren<TrailRenderer>();
                for (int i = 0; i < trails.Length; i++)
                {
                    trails[i].sortingLayerName = canvas.sortingLayerName;
                    trails[i].sortingOrder = sortOrder;
                }
                TrailCanvas.sortingLayerName = canvas.sortingLayerName;
                TrailCanvas.sortingOrder = sortOrder;
            }
        }

        //yield return null;
        //particleFx.StartEffect();
        //Debug.LogWarning("StartedEffect from FxTrigger " + particleFx.gameObject.name);
        this.currentParticleEffects = particleFx;
        //yield return null;
        //yield return null;

        SetFxTransformStartPosition();
        particleFx.gameObject.SetActive(true);
        particleFx.StartEffect();
        yield return null;
        SetPathTweens(particleFx);
    }
    private void SetPathTweens(ParticleEffectsTrigger particleFx)
    {
        _LastPosition = fxTransform.position;
        var path = PathToTarget;
        OrientTransforms(path);
        
        // Make sure the floater's in the right position
        emitter.localPosition = Vector3.zero;
        emitter.localRotation = Quaternion.identity;

        // The main path, a straight line between source & target
        fxTransform.DOBlendableMoveBy(path, _DurationS);

        //Debug.LogFormat("#TrailFX# First path to target {0} {1} {2} for {3}", path.x, path.y, path.z, _DurationS);
        /*fxTransform.DOBlendableMoveBy(path, _DurationS).OnComplete(() =>
        {
            if (currentParticleEffects != null)
                currentParticleEffects.EndEffect();
        });*/
        _ExpectedTarget = _TargetFlatPosition;// new Vector3(_TargetTransform.position.x, _TargetTransform.position.y, zHeight);
        _IsPlaying = true;
        _StartTime = DateTime.UtcNow;

        switch (this._Path)
        {
            case PlayFxPath.Arc:
                PlayArcTweens();
                break;
            case PlayFxPath.ArcWiggle:
                PlayArcWiggleTweens();
                break;
            case PlayFxPath.Curved:
                PlayCurvedTweens();
                break;
            case PlayFxPath.CurvedWiggle:
                PlayCurvedWiggleTweens();
                break;
            case PlayFxPath.Drop:
                PlayDropTweens();
                break;
            case PlayFxPath.Spiral:
                PlaySpiralTweens(1.0f);
                break;
            case PlayFxPath.TightSpiral:
                PlaySpiralTweens(1.5f);
                break;
            case PlayFxPath.Wiggle:
                PlayWiggleTweens();
                break;
            case PlayFxPath.ZigZag:
                PlayZigZagTweens();
                break;
            case PlayFxPath.Straight:
            case PlayFxPath.NoPath:
            default:
                break;
        }
    }

    private void OrientTransforms(Vector3 path)
    {
        //var local = fxTransform.localRotation;
        if (path != Vector3.zero)
        {
            float rotationZ = Mathf.Atan2(path.y, path.x) * Mathf.Rad2Deg;
            fxTransform.localRotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);
            /*
            var direction = Quaternion.LookRotation(path, Vector3.up);
            if (float.IsNaN(direction.w))
            {
                Debug.LogFormat("#TrailFX# NaN on direction. path={0}, rot={1}", path, fxTransform.localRotation);
            }
            else
            {
                fxTransform.localRotation = direction;
            }*/
        }
        //var newLocal = fxTransform.localRotation;
        //Debug.LogFormat("#TrailFX# base old local: w{0} x{1} y{2} z{3} >> w{4} x{5} y{6} z{7}", local.w, local.x, local.y, local.z, newLocal.w, newLocal.x, newLocal.y, newLocal.z);
    }

    private Vector3 CreatePerpendicularVector(Vector3 sourceVector, float angle, float scale) // angle is -90 to +90; 0 is perp
    {
        // project the vector onto a the horizontal plane of the board
        var normal = Vector3.ProjectOnPlane(sourceVector, Vector3.forward).normalized;
        // rotate a vertical vector around the axis
        var rotated = Quaternion.AngleAxis(angle, normal) * Vector3.forward;
        return rotated;
    }
    
    private void PlayArcTweens()
    {
        // curves to the target; going high (0-20deg)
        float angle = UnityEngine.Random.Range(-10f, 10f);
        Vector3 move = new Vector3(0,0,UnityEngine.Random.Range(0.25f, 0.35f) * DistanceToTarget * -15);

        var local = rotator.localRotation;
        rotator.localRotation = Quaternion.Euler(angle, 0, 0);
        emitter.localRotation = Quaternion.Euler(angle, 0, 0);
        float topPoint = 0.4f;
        _Sequence = DOTween.Sequence();
        //_Sequence.AppendCallback(() => Debug.LogFormat("#TrailFX# ARC path start {0} {1} {2}", emitter.localPosition.x, emitter.localPosition.y, emitter.localPosition.z));
        _Sequence.Append(emitter.DOBlendableLocalMoveBy(move, _DurationS * topPoint).SetEase(Ease.OutSine));
        //_Sequence.AppendCallback(() => Debug.LogFormat("#TrailFX# ARC path midpoint {0} {1} {2}", emitter.localPosition.x, emitter.localPosition.y, emitter.localPosition.z));
        _Sequence.Append(emitter.DOBlendableLocalMoveBy(-move, _DurationS * (1 - topPoint)).SetEase(Ease.InSine));
        //_Sequence.AppendCallback(() => Debug.LogFormat("#TrailFX# ARC path end {0} {1} {2}", emitter.localPosition.x, emitter.localPosition.y, emitter.localPosition.z));
        //Debug.LogFormat("#TrailFX# Arc path set");
        _Sequence.Play();
    }

    private void PlayCurvedTweens()
    {
        // curves to the target; going low (90-30deg)

        float angle = UnityEngine.Random.Range(0, 2) == 0 ? UnityEngine.Random.Range(-90f, -45f) : UnityEngine.Random.Range(45f, 90f);
        Vector3 move = new Vector3(0, 0, UnityEngine.Random.Range(0.15f, 0.35f) * DistanceToTarget *-15);
        //Debug.LogFormat("#TrailFX# Curve {0}deg height {1}", angle, move.magnitude);

        //var local = rotator.localRotation;
        rotator.localRotation = Quaternion.Euler(angle, 0, 0);
        emitter.localRotation = Quaternion.Euler(-angle, 0, 0);
        //var newLocal = rotator.localRotation;
        //Debug.LogFormat("#TrailFX# emitter local: w{0} x{1} y{2} z{3} >> w{4} x{5} y{6} z{7}", local.w, local.x, local.y, local.z, newLocal.w, newLocal.x, newLocal.y, newLocal.z);
        float topPoint = 0.25f;
        _Sequence = DOTween.Sequence();
        //_Sequence.AppendCallback(() => Debug.LogFormat("#TrailFX# CURVE path start {0} {1} {2}", emitter.localPosition.x, emitter.localPosition.y, emitter.localPosition.z));
        _Sequence.Append(emitter.DOBlendableLocalMoveBy(move, _DurationS * topPoint).SetEase(Ease.OutQuad));
        //_Sequence.AppendCallback(() => Debug.LogFormat("#TrailFX# CURVE path midpoint {0} {1} {2}", emitter.localPosition.x, emitter.localPosition.y, emitter.localPosition.z));
        _Sequence.Append(emitter.DOBlendableLocalMoveBy(-move, _DurationS * (1 - topPoint)).SetEase(Ease.InOutQuad));
        //_Sequence.AppendCallback(() => Debug.LogFormat("#TrailFX# CURVE path end {0} {1} {2}", emitter.localPosition.x, emitter.localPosition.y, emitter.localPosition.z));
        _Sequence.Play();
    }

    private void PlayCurvedWiggleTweens()
    {
        // as curve, but vibration along the path
        PlayCurvedTweens();
        PlayWiggleTweens();
    }

    private void PlayArcWiggleTweens()
    {
        // as arc, but vibration along the path
        PlayArcTweens();
        PlayWiggleTweens();
    }

    private void PlayDropTweens()
    {
        fxTransform.DOKill();
        fxTransform.position = _TargetFlatPosition;
        fxTransform.DOBlendableMoveBy(new Vector3(0, 0, -400), _DurationS).From();
        // straight down from the sky
    }

    private void PlaySpiralTweens(float tightness)
    {
        // spirals along the line, narrowing in on each spiral
        Vector3 move = new Vector3(0, 150, 0);// new Vector3(0, UnityEngine.Random.Range(0.25f, 0.5f) * distanceToTarget * 20, 0);
        emitter.DOBlendableLocalMoveBy(move, 0);
        _Sequence = DOTween.Sequence();

        // 1x tightness = 1 spiral per 8; 
        // 3x tightness = 1 spiral per 2.5;
        var rotations = (DistanceToTarget * tightness / 15f );
        //Debug.LogFormat("Rotations: {0}", rotations);
        //float spiralsPerSecond = tightness * 3f;
        for (int i = 0; i < rotations; i++)
        {
            _Sequence.Append(rotator.DOLocalRotate(new Vector3(360, 0, 0), _DurationS / rotations, RotateMode.FastBeyond360).SetEase(Ease.Linear));
        }
        emitter.DOBlendableLocalMoveBy(move * -0.8f, _DurationS);
    }

    private void PlayWiggleTweens()
    {
        float distance = UnityEngine.Random.Range(40f, 80f) * (UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1);
        float modifier = 1.0f;
        Vector3 move = new Vector3(0, distance, 0);
        float period = UnityEngine.Random.Range(0.09f, 0.12f);
        Sequence seq = DOTween.Sequence();
        if (currentParticleEffects != null)
            seq.Append(currentParticleEffects.transform.DOBlendableLocalMoveBy(move * -0.5f, 0.0f));
        seq.AppendInterval(UnityEngine.Random.Range(0f, _DurationS / 12f));
        seq.AppendCallback(() => {
            modifier -= 0.1f;
            if (currentParticleEffects != null)
                currentParticleEffects.transform.DOBlendableLocalMoveBy(move * Math.Max(0.1f, modifier), period).SetEase(Ease.InOutQuad).SetLoops(10000, LoopType.Yoyo);
        });
        seq.Play();
        // does 6 wiggles to the target, narrowing each
    }

    private void PlayZigZagTweens()
    {
        int direction = UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1;
        var dist = DistanceToTarget; // screen width is 100 world space units.
        float distance = UnityEngine.Random.Range(DistanceToTarget *3, DistanceToTarget *4);

        
        // does 4 zigzags to the target, slightly narrowing each time
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(UnityEngine.Random.Range(0f, _DurationS / 12f));
        seq.Append(emitter.DOBlendableLocalMoveBy(new Vector3(0, distance * direction), _DurationS / 6f));
        seq.Append(emitter.DOBlendableLocalMoveBy(new Vector3(0, distance * -1.7f * direction), _DurationS / 6f));
        seq.Append(emitter.DOBlendableLocalMoveBy(new Vector3(0, distance * direction), _DurationS / 6f));
        seq.Append(emitter.DOBlendableLocalMoveBy(new Vector3(0, distance * -0.5f * direction), _DurationS / 6f));
        seq.Append(emitter.DOBlendableLocalMoveBy(new Vector3(0, distance * 0.2f * direction), _DurationS / 6f));
    }

    private void LateUpdate()
    {
        if (this == null || this._TargetTransform == null || !_IsPlaying)
            return;

        // re-orient the object to make it face along its path
        var currentDirection = (fxTransform.position - _LastPosition);
        if (currentDirection != Vector3.zero)
        {
            OrientTransforms(currentDirection);
            _LastPosition = fxTransform.position;
        }

        // Re-orient the move to send it towards the target.
        _TimeLeftS = _DurationS - (float)(DateTime.UtcNow - _StartTime).TotalSeconds;
        if (_TimeLeftS > 0.00f)
        {
            var targetFlatPos = _TargetFlatPosition;
            var diffX = targetFlatPos.x - _ExpectedTarget.x;
            var diffY = targetFlatPos.y - _ExpectedTarget.y;

            if (diffX > minRetarget || diffY > minRetarget || diffX < -minRetarget || diffY < -minRetarget)
            {
                var difference = new Vector3(diffX, diffY);
                //Debug.LogFormat("#TrailFX# Update adjustment {0} target {1}, expected {2}", difference, targetFlatPos, _ExpectedTarget);
                fxTransform.DOBlendableMoveBy(difference, _TimeLeftS);//.SetEase(Ease.InQuart);
                _ExpectedTarget = targetFlatPos;// new Vector3(_TargetTransform.position.x, _TargetTransform.position.y, zHeight);
            }
        }
    }

    private const float minRetarget = 2.0f;

    public override void RecycleFx()
    {
        //Debug.LogFormat("#TrailFX# Recycle FX starting?");
        CompleteRecycle();
    }
    

    private void CompleteRecycle()
    {
        if (this != null)
        {
            _SourceTransform = null;
            _TargetTransform = null;
            _DurationS = 0;
            _TimeLeftS = 0;
            _Path = PlayFxPath.NoPath;
            _IsPlaying = false;
            _Sequence = null;
            fxTransform.localRotation = Quaternion.identity;
            if (currentParticleEffects != null)
            {
                currentParticleEffects.transform.localPosition = Vector3.zero;
                currentParticleEffects.transform.localRotation = Quaternion.identity;
                currentParticleEffects.EndEffect();
                currentParticleEffects.gameObject.SetActive(false);
            }
            if (emitter != null)
            {
                emitter.localPosition = Vector3.zero;
                emitter.localRotation = Quaternion.identity;
                emitter.DOKill();
            }
            if (fxTransform != null)
            {
                fxTransform.DOKill();
            }
            if (rotator != null)
            {
                rotator.DOKill();
                rotator.localRotation = Quaternion.identity;
                rotator.localPosition = Vector3.zero;
            }
        }

        base.RecycleFx();
    }
}

