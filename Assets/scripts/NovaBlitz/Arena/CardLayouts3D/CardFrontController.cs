﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Messages;
using DragonFoundry.Configuration;
using DragonFoundry.GameState;
using NovaBlitz.UI;
using DG.Tweening;

[System.Serializable]
public class RarityGlowColors
{
    public Color GlowColor;
    public Color BurnColor;
}

[ExecuteInEditMode]
public class CardFrontController : CardController
{
    // Core UI Elements
    public RawImage cardArt;
    public RawImage scanlines;
    public Image cardBackground;
    public RectTransform RulesBlinder;
    public I2.Loc.Localize TitleLocalizer;
    public TextMeshProUGUI RulesLabel;
    public TextMeshProUGUI HealthLabel;
    public TextMeshProUGUI AttackLabel;
    public TextMeshProUGUI SubtypeLabel;
    public TextMeshProUGUI AbilityCostLabel;
    public GameObject FloatingTextPrefab;

    public Sprite PowerAlphaMask;
    public Sprite UnitAlphaMask;
    public Image CombatGlow1;
    public Image CombatGlow2;
    public Image ActivateGlow1;
    public Image ActivateGlow2;
    public Image PlayableGlow1;
    public Image PlayableGlow2;
    public Image CastGlow1;
    public Image CastGlow2;
    public Image RarityGlow1;
    public Image RarityGlow2;
    public CanvasGroup RarityGlowCanvasGroup;
    public CanvasGroup BlackGlowsCanvasGroup;
    public Image BlackGlowUnit;
    public Image BlackGlowPower;
    public Image OutlineUnit;
    public Image OutlinePower;
    public CanvasGroup OutlineCanvasGroup;
    public CanvasGroup AbilityFrameCanvasGroup;
    public CanvasGroup DeathOverlayCanvasGroup;
    public Image AbilityCostFrame;
    public Image AbilityCostWhiteMask;

    public RarityGlowColors CommonColor;
    public RarityGlowColors RareColor;
    public RarityGlowColors EpicColor;
    public RarityGlowColors LegendaryColor;

	// New Frame Elements
	//public Image topBackground;
	public Image titleHolder;
	//public Image bottomBackground;
	public Image artMask;
    
    public Sequence AttackSequence;
    
    public Animator damageIconAnimator;
    public TextMeshProUGUI DamageLabel;

    public NovaAudioPlayer AttackAudio;
    public NovaAudioPlayer AttackImpactAudio;
    public CardAspectConfiguration[] aspects;
	public Sprite[] rarities;
	public Sprite[] masks;
	public Image RarityIcon;
    public Canvas OverlayCanvas;

    private CombatState CombatState { get; set; }

    // UI State
    public ICardState CardState { get { return cardState; } }
    public CardData CardData {get { return cardData;} }
    public Prize PrizeData { get; set; }
    public bool IsPurified = false;
    public CanvasGroup AttackingIndicator;
    public TextMeshProUGUI AttackOrderIndicator;
    public CanvasGroup BlockingIndicator;
    public CanvasGroup NoAttackIndicator;
    public RectTransform BlockingArrowAnchor;
    public RectTransform AttackingArrowAnchor;
    public RectTransform BlockedByOpponentAnchor;

    public static Color _DamageColor = new Color { r = 1f, g = 0.5f, b = 0.5f, a = 1 };
    public static Color _BuffColor = new Color { r = 0.0f, g = 1.0f, b = 0.0f, a = 1 };
    public static Color _NerfColor = new Color { r = 0.65f, g = 0.65f, b = 0.65f, a = 1 };

    // Internal State
    private CardAspectConfiguration currentAspect = null;
    private Texture cardArtTexture;

    public override int SortingOrder { get { return Canvas.sortingOrder; } set { Canvas.sortingOrder = value; OverlayCanvas.sortingOrder = value + 2; } }
    // Pool state
    public override PooledObjectType PooledObjectType { get { return PooledObjectType.CardFrontController; } }
    public override string CardIdAndName
    {
        get
        {
            CardData baseData;
            return string.Format("id:{0}:{1}", CardId, GameData.CardDictionary.TryGetValue(CardId, out baseData) && baseData != null ? baseData.Name : TitleLabel.text);
        }
    }

    protected Camera uiCamera;
    protected ArenaView arenaView;

    void Start()
    {
        this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
        SetArenaView();
        //InitializeGlows();
    }

    //private float nextLogTime;
    private const float rulesblinderBase = -142.0f;
    private const float minScreenPos = -28f;
    private const float maxScreenPos = -66f;
    private const float ScreenPosFactor = 80f / (minScreenPos - maxScreenPos);

    private void Update()
    {
        //if (arenaView != null && (arenaView.shakeScreen == null || !arenaView.shakeScreen.IsPlaying()))
        //{
            var yvalue = (473.0f / Screen.height) * uiCamera.WorldToScreenPoint(RarityIcon.transform.position).y;

        // if shaking, only calc for cards not in hand.

            if (arenaView != null && ((yvalue < minScreenPos && arenaView.shakeScreen == null && this.transform.parent == arenaView.PlayerHandController.transform)
                || RulesBlinder.localPosition.y != rulesblinderBase && (arenaView.shakeScreen == null || this.transform.parent != arenaView.PlayerHandController.transform)))
            {
                RulesBlinder.localPosition = new Vector3(0, Math.Max(rulesblinderBase, Math.Min(0, ScreenPosFactor * (minScreenPos - yvalue) - 30 - RulesBlinder.rect.height)), 0);
            }

            /*if(Time.time > nextLogTime)
            {

                Debug.LogFormat("w:{0} h:{1} y:{2} newY:{3}", Screen.width, Screen.height, yvalue, RulesBlinder.localPosition.y);
                nextLogTime = Time.time + 2.0f;
            }*/
        //}
    }

    private void SetArenaView()
    {
        if (arenaView == null)
        {
            var arena = GameObject.Find("16by9Arena(Clone)");
            if (arena != null)
                this.arenaView = arena.GetComponent<ArenaView>();
        }
    }

    private void InitializeGlows()
    {
        if (this.AttackingIndicator != null) { this.AttackingIndicator.gameObject.SetActive(true); this.AttackingIndicator.DOFade(0.0f, 0.0f); }
        if (this.BlockingIndicator != null) { this.BlockingIndicator.gameObject.SetActive(true); this.BlockingIndicator.DOFade(0.0f, 0.0f); }
        if (this.NoAttackIndicator != null) { this.NoAttackIndicator.gameObject.SetActive(true); this.NoAttackIndicator.DOFade(0.0f, 0.0f); }
        this.AttackToggleButton.gameObject.SetActive(false);
        this.AbilityToggleButton.gameObject.SetActive(false);

        this.OutlinePower.gameObject.SetActive(false);
        this.OutlinePower.gameObject.SetActive(false);

        this.DamageLabel.text = "0";
        this.DeathOverlayCanvasGroup.alpha = 0;

        this.RarityGlowCanvasGroup.gameObject.SetActive(false);


        this.CanvasGroup.interactable = false;
        if (this.CardGlowAnimator.isInitialized)
        {
            this.CardGlowAnimator.SetBool("IsPlayable", false);
            this.CardGlowAnimator.SetBool("IsCombatActive", false);
            this.CardGlowAnimator.SetBool("IsActivatable", false);
            this.CardGlowAnimator.SetBool("IsActivateTarget", false);
            this.CardGlowAnimator.SetBool("IsCastTarget", false);
            this.CardGlowAnimator.SetBool("IsHovered", false);
            this.CardGlowAnimator.SetBool("IsSelected", false);
            this.CardGlowAnimator.SetBool("IsAffected", false);
            this.CardGlowAnimator.SetBool("IsAirborn", false);
        }
    }

    public void OnDamageBurstEvent()
    {
        Debug.LogWarning("Handle this some way -DMac: OnDamageBurstEvent");
    }

    public void SetRarityGlowColor(CardRarity rarity)
    {
        this.RarityGlowCanvasGroup.gameObject.SetActive(true);

        switch(rarity)
        {
            case CardRarity.Common:
                this.RarityGlow1.color = this.CommonColor.GlowColor;
                this.RarityGlow2.color = this.CommonColor.BurnColor;
            break;
            case CardRarity.Rare:
                this.RarityGlow1.color = this.RareColor.GlowColor;
                this.RarityGlow2.color = this.RareColor.BurnColor;
            break;
            case CardRarity.Epic:
                this.RarityGlow1.color = this.EpicColor.GlowColor;
                this.RarityGlow2.color = this.EpicColor.BurnColor;
            break;
            case CardRarity.Legendary:
                this.RarityGlow1.color = this.LegendaryColor.GlowColor;
                this.RarityGlow2.color = this.LegendaryColor.BurnColor;
            break;
            default:
                this.RarityGlowCanvasGroup.gameObject.SetActive(false);
            break;
        }
    }

    override public void ShowAspectFrame()
    {
        if(this.AbilityCostLabel.text != "-1")
        {
            this.AbilityCostWhiteMask.DOFade(1,0);
            this.AbilityFrameCanvasGroup.DOFade(1, 0.3f).SetDelay(0.2f);
            this.AbilityCostWhiteMask.DOFade(0,0.5f).SetDelay(0.5f);
        }
    }

    [ContextMenu("Scroll Text...")]
    void ScrollText()
    {
        this.EnqueueScrollingText("[Stunned 1]", Color.blue);
        this.EnqueueScrollingText("[Attack -3]", Color.red);
        this.EnqueueScrollingText("[Heal +3]", Color.green);
    }

    private float nextScrollingTextTime = 0;
    public void EnqueueScrollingText(string text, Color color)
    {
        GameObject floatingTextGO = GameObject.Instantiate(this.FloatingTextPrefab, Vector3.one, Quaternion.identity) as GameObject;
        Transform parent = this.OverlayCanvas.transform;//this.transform.GetChild(0);
        floatingTextGO.transform.AttachToParent(parent, parent.childCount);
        floatingTextGO.transform.localPosition = Vector3.zero;
        TextMeshProUGUI shadowLabel = floatingTextGO.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI floatingTextLabel = shadowLabel.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        CanvasGroup canvasGroup = floatingTextGO.GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0;
        shadowLabel.text = text;
        floatingTextLabel.text = text;
        floatingTextLabel.color = color;

        Sequence seq = DOTween.Sequence();
        float delayBetweenTexts = 1.3f;
        float delay = 0;

        if(Time.unscaledTime - nextScrollingTextTime < 0)
        {
            delay = nextScrollingTextTime - Time.unscaledTime;
            seq.SetDelay(delay);
            this.nextScrollingTextTime = this.nextScrollingTextTime + delayBetweenTexts;
        }
        else
        {
            this.nextScrollingTextTime = Time.unscaledTime + delayBetweenTexts;
        }
        seq.Append(canvasGroup.DOFade(1f,0f));
        seq.Append(shadowLabel.transform.DOLocalMoveY(60f, 3f));
        seq.Join(floatingTextLabel.transform.DOLocalMoveY(12, 2.8f).SetEase(Ease.InExpo));
        seq.Join(floatingTextLabel.transform.DOScale(Vector3.one * 1.2f, 2.4f).SetEase(Ease.OutSine));
        seq.Insert(delay+2.5f,canvasGroup.DOFade(0f,0.3f));
        seq.OnComplete(()=>{
            GameObject.Destroy(floatingTextGO);
        });
        seq.Play();

    }

    /// <summary>
    /// TODO: This is a huge piece of crap that should be replaced by the cardState.ActivationCost being set by the serer.
    /// </summary>
    private int GetActivationCost(ICardState cardState)
    {
        string rulesText = cardState.GetEffectText(false);

        if(rulesText.Contains("1:"))
        {
            return 1;
        }
        else if(rulesText.Contains("2:"))
        {
            return 2;
        }
        else if(rulesText.Contains("3:"))
        {
            return 3;
        }
        else if(rulesText.Contains("4:"))
        {
            return 4;
        }

        return -1;
    }

    override public void Initialize(ICardState cardState, int cardBackId, bool isPurified)
    {
        SetArenaView();
        this.InitializeGlows();
        this.cardData = null;
        base.Initialize(cardState, cardBackId, isPurified);
        this.CanvasGroup.interactable=false;
        this.DeathOverlayCanvasGroup.alpha=0;
        
        this.cardState = cardState;
        this.CardId = cardState.Id;
        this.IsPurified = isPurified;

        UpdateLabels();
        
        foreach (CardAspectConfiguration aspect in aspects)
        {
            if (aspect.aspectTag == cardState.GetAspect().ToString())
            {
                currentAspect = aspect;
                break;
            }
        }
       
        // TODO: Get the server to pass ActivationCost in the initial cardState for cards with ActivatedAbilities
        //Debug.LogFormat("ActivationCost: {0}" cardState.ActivationCost);
        this.AbilityCostLabel.text = this.GetActivationCost(cardState).ToString();

        this.AbilityFrameCanvasGroup.alpha = 0;
        LoadCardArt();
        UpdateArt();
        UpdateBackground();
        this.RarityGlowCanvasGroup.gameObject.SetActive(false);
    }
    
    /// <summary>
    /// Initializes a card's appearance from raw cardData instead of cardstate data
    /// </summary>
    public void Initialize(CardData cardData, string cardBack)
    {
        this.InitializeGlows();
        this.cardState = null;
        this.cardData = cardData;
        this.RulesLabel.richText = true;
        this.TitleLocalizer.Term = cardData.NameLocId;
        this.RulesLabel.text = cardData.GenerateCardText();
        this.CostLabel.text = cardData.Cost.ToString();
        this.SubtypeLabel.text = cardData.Subtype;
        this.AbilityFrameCanvasGroup.alpha =0;
        this.AbilityCostLabel.text = "-1";
        this.DeathOverlayCanvasGroup.alpha=0;

        /*if(cardData.NewHotnessKeywords.ContainsKey((int)KeywordEnum.ActivatedAbility))
        {
            Debug.LogFormat("ActivatoinCosts: {0}", cardData.NewHotnessKeywords[(int)KeywordEnum.ActivatedAbility] );
        }
        else
        {
            Debug.Log("ActivatoinCosts not found");
        }*/
        
        switch (cardData.CardType)
        {
        case CardType.Item:
        case CardType.Power:
            this.AttackLabel.text = string.Empty;
            this.HealthLabel.text = string.Empty;
            AttackLabel.gameObject.SetActive(false);
            HealthLabel.gameObject.SetActive(false);
            break;
        case CardType.Unit:
            UnitCard unit = (UnitCard)cardData;
            this.AttackLabel.text = unit.AttackPower.ToString();
            this.HealthLabel.text = unit.Health.ToString();
            AttackLabel.gameObject.SetActive(true);
            HealthLabel.gameObject.SetActive(true);
            break;
        }
        
        foreach (CardAspectConfiguration aspect in aspects)
        {
            if (aspect.aspectTag == cardData.Aspect.ToString())
            {
                currentAspect = aspect;
                break;
            }
        }
        
        this.LoadCardArt(cardData.ArtID);
        this.SetCardBack(QueryUtils.GetIdNumber(cardBack));
        
		if (cardData.CardType == CardType.Power) { cardBackground.sprite = currentAspect.itemTexture; artMask.sprite = masks [1]; }
		else if (cardData.CardType == CardType.Unit) { cardBackground.sprite = currentAspect.unitTexture; artMask.sprite = masks [0]; }
		else { cardBackground.sprite = currentAspect.powerTexture; artMask.sprite = masks [1]; }

		titleHolder.sprite = currentAspect.titleHolder;

		int rarity = (int)cardData.Rarity;
		if (rarity < rarities.Length && rarity >= 0)
			RarityIcon.sprite = rarities [rarity];

        this.BlackGlowPower.gameObject.SetActive(cardData.CardType == CardType.Power);
        this.BlackGlowUnit.gameObject.SetActive(cardData.CardType == CardType.Unit);
        this.RarityGlowCanvasGroup.gameObject.SetActive(false);
    }

    public void ResetCardFront()
    {
        IsPurified = false;
        CurrentDamage = 0;
        if(this.cardState != null)
            this.cardState.Damage = 0;
        UpdateLabels();
        InitializeGlows();
    }
        
    public void ResetGlows()
    {
        if(this.CardGlowAnimator.isInitialized) { this.CardGlowAnimator.Rebind(); }
        this.UpdateFlyingState();
    }
    
    public void UpdateFlyingState()
    {
        if(this.cardState != null && this.CardGlowAnimator.isInitialized)
        {
            this.CardGlowAnimator.SetBool("IsAirborn", this.cardState.GetStat(KeywordEnum.Flying) > 0 && this.cardState.Zone == CardZone.Arena);
        } 
    }
    
    public override void AttackTarget(TargetArrowTarget target, TargetArrowController arrowController)
    {
        this.arrowController = arrowController;
        this.target = target;
        this.targetArrowInfo = this.arrowController.RequestTargetArrowInfo(this.AttackingArrowAnchor, this.target.BoundsRect, this.OverlayCanvas.transform, 0);
        this.targetArrowInfo.StartingColor =  new Color(255/255f,40/255f, 40/255f,255/255f);
        this.targetArrowInfo.Source.z -= 1; 
        this.arrowController.FadeOutAndReleaseTargetArrow(this.targetArrowInfo,0.25f,0.5f);
        
        if(this.PlayerArenaController != null)
        {
            this.PlayerArenaController.AttackWithCard(this.cardState);
        }
        this.timesAttacked++;
    }
    
    public override void BlockTarget(TargetArrowTarget target, TargetArrowController arrowController)
    {
        if(this.PlayerArenaController != null)
        {
            this.PlayerArenaController.BlockWithCard(this.cardState, target.Id);
        }
        this.timesBlocked++;
    }
    
    public override void ActivateTarget(TargetArrowTarget target, TargetArrowController arrowController)
    {
        if (this.PlayerArenaController != null)
        {
            if(target != null)
            {
                Debug.LogFormat("Activating targeted ability:{0}, targetId:{1}", this.CardIdAndName, target.Id);
                this.PlayerArenaController.ActivateCard(this.cardState, target.Id);
            }
            else
            {
                 Debug.LogFormat("Activating ability:{0}", this.CardIdAndName);
                this.PlayerArenaController.ActivateCard(this.cardState, null);
            }
            this.timesActivated++;
        }
    }

    public void UpdateCombatState(CombatState combatState)
    {
        // Send a null combat state to wipe the combat state
        CombatState = combatState;
        if (CombatState == null)
        {
            BlockingIndicator.DOFade(0.0f, 0.5f);
            AttackingIndicator.DOFade(0.0f, 0.5f);
            NoAttackIndicator.DOFade(0.0f, 0.5f);
            // Turn off all the identifiers.
            return;
        }
        
        if (combatState.AttackTargetId > 0)
        {
            if (AttackingIndicator.alpha == 0)
                AttackingIndicator.transform.DOPunchScale(Vector3.one * 1.1f, 0.5f, 1, 0.25f);
            AttackingIndicator.DOFade(1.0f, 0.5f);
            AttackOrderIndicator.text = combatState.AttackOrder > 0 ? combatState.AttackOrder.ToString() : string.Empty;
        }
        else
        {
            AttackingIndicator.DOFade(0.0f, 0.5f);
        }
        
        BlockingIndicator.DOFade(combatState.BlockTargets.Count > 0 ? 1.0f : 0.0f, 0.5f);
        NoAttackIndicator.DOFade(combatState.DidNotAct ? 1.0f : 0.0f, 0.5f);

        // Process the combat state to show
        //combatState.AttackTargetId - the singular object (almost always one of the two players) that the card is attacking
        //combatState.BlockedById - This one's internal, so it comes across as always 0
    }

    public void UpdateCardState(GameStateReadOnly gameState)
    {
        if (this == null || cardState == null)
            return;
        if(cardState.Zone != CardZone.Trash)
        {
            UpdateLabels();
        }
        UpdateAspect(false);
        
        // When cardstate comes in, the card may have lost or gained flying state
        if(cardState.Zone == CardZone.Arena)
        {
             // If it's in the arena display that state on the card.
            UpdateFlyingState();
        }
        else
        {
            // Anywhere else (like the hand or trash) doesn't show the flying animations
            if(this.CardGlowAnimator.isInitialized)
            {
                this.CardGlowAnimator.SetBool("IsAirborn", false);
            }
        }
        
        UpdateArt();
        UpdateBackground();
    }
    
    public void BecomeNewCard()
    {
        CurrentDamage = cardState.Damage;
        UpdateLabels();
        UpdateAspect(true);
        LoadCardArt();
        UpdateArt();
        UpdateBackground();
    }


    private void UpdateAspect(bool forceUpdate)
    {
        if (currentAspect != null && !forceUpdate) { return; }

        foreach (CardAspectConfiguration aspect in aspects)
        {
            if (aspect.aspectTag == cardState.GetAspect().ToString())
            {
                currentAspect = aspect;
                return;
            }
        }

        if (currentAspect == null)
        {
            Debug.LogWarningFormat("No aspect was found matching the card's aspect: {0}. Using a default aspect.", cardState.GetAspect());
            currentAspect = aspects[0];
        }
    }
    
    float lastDamageTime = 0;
    public void ShowDamageBurst(int damage)
    {
        if (Time.time - this.lastDamageTime > 1)
        {
            this.DamageLabel.text = damage.ToString();
            this.lastDamageTime = Time.time;
        }
        else
        {
            // If the next damage arrives quickly.. add its total to the damage burst
            this.DamageLabel.text = (int.Parse(this.DamageLabel.text) + damage).ToString();
        }
        
        this.damageIconAnimator.SetTrigger("Show");
    }

    public void UpdateLabels()
    {
        if (CardState == null)
            return;
         // Set the health colour
        int originalHealth = this.CardState.GetOriginalHealth();
        if (this.CurrentDamage > 0)
            this.HealthLabel.color = _DamageColor;
        else if (this.CardState.Health < originalHealth)
            this.HealthLabel.color = _NerfColor;
        else if (this.CardState.Health > originalHealth)
            this.HealthLabel.color = _BuffColor;
        else
            this.HealthLabel.color = Color.white;
        
        // Update the health label     
        HealthLabel.text = (cardState.Health - this.CurrentDamage).ToString();
        //HealthLabel.text = cardState.GetCurrentHealth().ToString();
                
        if (!cardState.IsUpdated)
            return;
        else
            cardState.IsUpdated = false;

        // Set bolding for complex languages
        bool isBold = true;
        if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese")
            isBold = false;
        TitleLabel.fontStyle = isBold ? FontStyles.Bold | FontStyles.SmallCaps : FontStyles.Normal | FontStyles.SmallCaps;
        RulesLabel.fontStyle = isBold ? FontStyles.Bold : FontStyles.Normal;
        SubtypeLabel.fontStyle = isBold ? FontStyles.Bold | FontStyles.SmallCaps : FontStyles.Normal | FontStyles.SmallCaps;

        RulesLabel.richText = true;
        TitleLocalizer.Term = cardState.GetNameLocId();
        RulesLabel.text = cardState.GetEffectText(this.IsPurified);
        CostLabel.text = cardState.EnergyCost.ToString();
        //energyCostCard.text = "1";
        AttackLabel.text = cardState.Atk.ToString();

        // Set energy cost colour (grey is bad, green is good)
        int originalEnergyCost = this.CardState.GetOriginalEnergyCost();
        if (this.CardState.EnergyCost > originalEnergyCost)
            this.CostLabel.color = _NerfColor;
        else if (this.CardState.EnergyCost < originalEnergyCost)
            this.CostLabel.color = _BuffColor;
        else
            this.CostLabel.color = Color.white;


        // Set the subtype text
        SubtypeLabel.text = cardState.GetSubtype();
        // Set attack and health colours - red is damaged, grey is reduced, green is increased.
        if (this.CardState.IsUnit)
        {
            int originalAtk = this.CardState.GetOriginalAtk();
            if (this.CardState.Atk < originalAtk)
                this.AttackLabel.color = _NerfColor;
            else if (this.CardState.Atk > originalAtk)
                this.AttackLabel.color = _BuffColor;
            else
                this.AttackLabel.color = Color.white;
            //this.healthText.color = this.CardState.GetCurrentHealth() < this.CardState.GetOriginalHealth() ? Color.red : Color.white;
        }
        
        AttackLabel.gameObject.SetActive(cardState.IsUnit);
        HealthLabel.gameObject.SetActive(cardState.IsUnit);
        //energyCostCard.gameObject.SetActive(cardState.IsResource);
        //RulesLabel.gameObject.SetActive(!string.IsNullOrEmpty(RulesLabel.text));
    }


    private void UpdateBackground()
    {
		if (cardState.IsResource) 
        { cardBackground.sprite = currentAspect.itemTexture; artMask.sprite = masks [1];  }
		else if (cardState.IsUnit) { cardBackground.sprite = currentAspect.unitTexture; artMask.sprite = masks [0];  }
		else { cardBackground.sprite = currentAspect.powerTexture; artMask.sprite = masks [1];  }

        this.BlackGlowPower.gameObject.SetActive(cardState.IsPower);
        this.BlackGlowUnit.gameObject.SetActive(cardState.IsUnit);

        this.OutlinePower.gameObject.SetActive(cardState.IsPower);
        this.OutlineUnit.gameObject.SetActive(cardState.IsUnit);

		titleHolder.sprite = currentAspect.titleHolder;

		int rarity = (int)cardState.GetRarity ();
		if (rarity < rarities.Length && rarity >= 0)
			RarityIcon.sprite = rarities [rarity];
    }

    private void UpdateArt()
    {
        if (cardArtTexture != null)
        {
            cardArt.texture = cardArtTexture;
            scanlines.texture = cardArt.texture;
        }
        cardArt.gameObject.SetActive(true);
    }
    
    private void LoadCardArt(string artId =  null)
    {
		string cardArtId = artId;
        
        if(cardArtId == null && this.cardState != null)
        {
            cardArtId = this.cardState.GetArtId();
        }
        
		if(string.IsNullOrEmpty(cardArtId)==false)
        {
        	cardArtTexture = AssetBundleManager.Instance.LoadAsset<Texture>(cardArtId); //Resources.Load(cardArtId) as Texture;
        }
        
        if (cardArtTexture == null || string.IsNullOrEmpty(cardArtId))
        {
            CardAspect aspect = CardAspect.NoAspect;
            
            if(this.cardState != null)
            {
                aspect = this.cardState.GetAspect();
            }
            else if(this.cardData != null)
            {
                aspect = this.CardData.Aspect;
            }   
            
            switch(aspect)
            {
                case CardAspect.Arcane: cardArtId = "DC0353"; break;
                case CardAspect.Tech: cardArtId = "DC0351"; break;
                case CardAspect.Divine: cardArtId = "DC0352"; break;
                case CardAspect.Nature: cardArtId = "DC0354"; break;
                case CardAspect.Chaos: cardArtId = "DC0355"; break;
                default: cardArtId = "DC0348"; break;
            }

            cardArtTexture = AssetBundleManager.Instance.LoadAsset<Texture>(cardArtId);
        }
        
        if (cardArtTexture != null)
        {
            cardArt.texture = cardArtTexture;
            scanlines.texture = cardArt.texture;
        }
 
		if((this.cardState != null && this.cardState.IsPower) || (this.cardData != null && this.cardData.CardType == CardType.Power))
        {
            this.CombatGlow1.sprite = this.PowerAlphaMask;
            this.CombatGlow2.sprite = this.PowerAlphaMask;
            this.ActivateGlow1.sprite = this.PowerAlphaMask;
            this.ActivateGlow2.sprite = this.PowerAlphaMask;
            this.PlayableGlow1.sprite = this.PowerAlphaMask;
            this.PlayableGlow2.sprite = this.PowerAlphaMask;
            this.CastGlow1.sprite = this.PowerAlphaMask;
            this.CastGlow2.sprite = this.PowerAlphaMask;
        }
        else
        {
            this.CombatGlow1.sprite = this.UnitAlphaMask;
            this.CombatGlow2.sprite = this.UnitAlphaMask;
            this.ActivateGlow1.sprite = this.UnitAlphaMask;
            this.ActivateGlow2.sprite = this.UnitAlphaMask;
            this.PlayableGlow1.sprite = this.UnitAlphaMask;
            this.PlayableGlow2.sprite = this.UnitAlphaMask;
            this.CastGlow1.sprite = this.UnitAlphaMask;
            this.CastGlow2.sprite = this.UnitAlphaMask;
            this.AbilityCostFrame.sprite = this.currentAspect.abilityCostFrame;
        }
    }

    private void InitAspectIcon(Image aspectImage, int aspectThreshold)
    {
        aspectImage.gameObject.SetActive(true);

        // Get a reference to the Text Label...
        TextMeshProUGUI aspectLabel = aspectImage.GetComponentInChildren<TextMeshProUGUI>();

        // Set the Lable Text
        aspectLabel.text = aspectThreshold.ToString();

        // Hide or show the text lable based on weather it is > 1 or not
        aspectLabel.enabled = aspectThreshold > 1;

        // Show or hide the Aspect Icon
        aspectImage.gameObject.SetActive(aspectThreshold > 0);
    }

    public void OnLocalize()
    {
        UpdateLabels();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        cardArt.texture = null;
    }

    public override void OnRecycle()
    {
        base.OnRecycle();
        arenaView = null;
        cardArt.texture = null;
        CardBack.texture = null;
        Canvas.overrideSorting = true;
        InitializeGlows();
        CardGlowAnimator.Rebind();
        CanvasGroup.blocksRaycasts = false;
        CanvasGroup.interactable = false;
        CanvasGroup.alpha = 0;
        Canvas.sortingLayerName = "Default";
        SortingOrder = 100;
        cardState = null;
        cardData = null;
        IsPurified = false;
        PrizeData = null;
        cardData = null;
        cardState = null;
        legalAbilities = null;
        CombatState = null;
        cardArtTexture = null;
        currentAspect = null;
        RarityGlowCanvasGroup.gameObject.SetActive(false);
        RarityGlowCanvasGroup.DOFade(0.0f,0.0f);
        OutlineCanvasGroup.alpha = 0f;
        //cardFront.CardId = -1;
		RulesBlinder.localPosition = new Vector3(0, rulesblinderBase, 0);
        if (AttackSequence != null)
        {
            AttackSequence.Kill();
            AttackSequence = null;
        }
    }
}
