﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.UI;

public class ActivePowersLayout : MonoBehaviour 
{
	public CanvasGroup PlayerActivePowersTray;
	public RectTransform BoundsRect;
	public RectTransform PowerCardRoot;
	public RectTransform LayoutTweenerRoot;
	public bool IsDraggingTargetedPower = false;
	private RectTransform trayRectTransform;
	private RectTransform localRectTransform;
	private int trayChildCount = 0;

	private Camera uiCamera;
	private float defaultWidth = 328;
	private float incrementWidth = 150f;
	private float defaultAnchorPosX;
	private bool isMouseInRect = false;
	private const float TRAY_VISIBLE_X = -930f;
	private const float TRAY_HIDDEN_X = -1235f;
	private Transform powerInsertsRoot;
	private Queue<RectTransform> powerInsertTransforms = new Queue<RectTransform>();

	void Start()
	{
		this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
		this.trayRectTransform = (RectTransform)this.PlayerActivePowersTray.transform;
		this.localRectTransform = (RectTransform)this.transform;
		this.defaultAnchorPosX = this.localRectTransform.anchoredPosition.x;
		this.trayRectTransform.DOAnchorPosX(TRAY_HIDDEN_X,0f);
		this.trayRectTransform.DOSizeDelta(new Vector2(this.defaultWidth, this.trayRectTransform.sizeDelta.y),0);
		this.trayRectTransform = (RectTransform)this.PlayerActivePowersTray.transform;

		// Create a new root trasnform for powerInsertsRoot
        this.powerInsertsRoot = new GameObject().transform;
        this.powerInsertsRoot.name = "PowerInsertsForPowerLayout_" + this.name;
        this.powerInsertsRoot.SetParent(this.transform.parent, false);

		RectTransform powerInsertTransform = (RectTransform)this.PowerCardRoot.GetChild(0);
		RectTransform tweenerInsertTransform = (RectTransform)this.LayoutTweenerRoot.GetChild(0);

		// Rename the default tweener insert
		tweenerInsertTransform.name = "TweenerInsert 0";
		tweenerInsertTransform.gameObject.SetActive(false);

        // Attach the default powerInsert to the new root
        powerInsertTransform.gameObject.SetActive(false);
        powerInsertTransform.SetParent(this.powerInsertsRoot);
        this.powerInsertTransforms.Enqueue(powerInsertTransform);
        powerInsertTransform.name = "PowerInsert 0";
		

        // Create 9 copies of the default powerInsert and TweenerInserts
        for(int i=1;i<19;i++)
        {
            RectTransform trans = GameObject.Instantiate(powerInsertTransform);
            trans.SetParent(this.powerInsertsRoot);
            trans.localScale = Vector3.one;
            trans.localPosition = Vector3.zero;
            trans.name = "PowerInsert " + i;
            this.powerInsertTransforms.Enqueue(trans);

			// Tweener Insert
			trans = GameObject.Instantiate(tweenerInsertTransform);
			trans.SetParent(this.LayoutTweenerRoot);
			trans.localScale = Vector3.one;
            trans.localPosition = Vector3.zero;
            trans.name = "TweenerInsert " + i;
        }
	}

	public void AddPowerCard(CardController powerCard)
	{
		Vector3 originalPosition = powerCard.transform.position;
		RectTransform powerInsert = this.powerInsertTransforms.Dequeue();
		powerInsert.gameObject.SetActive(true);
		powerInsert.AttachToParent(this.PowerCardRoot, this.PowerCardRoot.childCount);
		Debug.LogFormat("#ActivePowerLayout# Attaching powerInsert: {0} to root for {1}", powerInsert.name, powerCard.CardIdAndName);
		RectTransform tweenerInsert = (RectTransform)this.LayoutTweenerRoot.GetChild(powerInsert.GetSiblingIndex());
		tweenerInsert.gameObject.SetActive(true);
		LayoutRebuilder.ForceRebuildLayoutImmediate(this.LayoutTweenerRoot);

		powerInsert.position = tweenerInsert.position;

		// Attach the card to the powerInsert
		powerCard.transform.AttachToParent(powerInsert,0, blocksRaycasts : false);
		powerCard.transform.localScale = Vector3.one;
		powerCard.transform.position = originalPosition;
		powerCard.CardGlowAnimator.SetBool("IsPending", true);
	}

	public CardController ReleasePowerCard(CardController powerCard)
	{
		if(powerCard.transform.parent != null && powerCard.transform.parent.parent != null && powerCard.transform.parent.parent == this.PowerCardRoot)
		{
			RectTransform powerInsert = (RectTransform)powerCard.transform.parent;
			RectTransform tweenerInsert = (RectTransform)this.LayoutTweenerRoot.GetChild(powerInsert.GetSiblingIndex());
			tweenerInsert.gameObject.SetActive(false);
			tweenerInsert.SetAsLastSibling();
			LayoutRebuilder.ForceRebuildLayoutImmediate(this.LayoutTweenerRoot);

			powerInsert.SetParent(this.powerInsertsRoot);
			this.powerInsertTransforms.Enqueue(powerInsert);
			powerInsert.gameObject.SetActive(false);
			return powerCard;
		}

		return powerCard;
	}

	public void ShowTray(int childCount)
	{
		if(this.trayChildCount == 0 && childCount > 0)
		{
			// Show
			this.PlayerActivePowersTray.DOFade(1f, 0.0f);
			this.trayRectTransform.DOAnchorPosX(TRAY_VISIBLE_X, 0.2f);
		}
		
		if(childCount == 0)
		{
			// Hide
			this.trayRectTransform.DOAnchorPosX(TRAY_HIDDEN_X,0.3f)
				.OnComplete(()=>this.PlayerActivePowersTray.DOFade(0f,0f));
		}
		else
		{
			if(childCount > this.trayChildCount)
			{
				// Increase size
				Vector2 sizeDelta = this.trayRectTransform.sizeDelta;
				sizeDelta.x = this.defaultWidth + (this.incrementWidth * (childCount-1));
				this.trayRectTransform.DOSizeDelta(sizeDelta,0.25f);
			}
			else if(childCount < this.trayChildCount)
			{
				// Decrease size
				Vector2 sizeDelta = this.trayRectTransform.sizeDelta;
				sizeDelta.x = this.defaultWidth + (this.incrementWidth * (childCount-1));
				this.trayRectTransform.DOSizeDelta(sizeDelta,0.25f);
			}
		}

		// Remember what size the tray has sized to
		this.trayChildCount = Mathf.Max(childCount,0);
	}

	void LateUpdate()
	{
		Vector2 sizeDelta = this.trayRectTransform.sizeDelta;
		this.BoundsRect.sizeDelta = sizeDelta;

		if(this.IsDraggingTargetedPower)
		{
			bool mouseInRect = RectTransformUtility.RectangleContainsScreenPoint(this.BoundsRect, Input.mousePosition, this.uiCamera);
			if(mouseInRect == false)
			{
				this.BoundsRect.anchoredPosition = this.trayRectTransform.anchoredPosition;
			}

			if(mouseInRect && DOTween.IsTweening(this.trayRectTransform) == false && this.trayChildCount > 0)
			{
				this.isMouseInRect = true;
				float targetTrayX = TRAY_HIDDEN_X + 100f;
				float currentTrayX = this.trayRectTransform.anchoredPosition.x;
				float deltaX = targetTrayX - currentTrayX;
				this.trayRectTransform.DOAnchorPosX(currentTrayX+deltaX,0.25f).SetEase(Ease.OutQuart);
				this.localRectTransform.DOAnchorPosX(this.localRectTransform.anchoredPosition.x + deltaX, 0.25f).SetEase(Ease.OutQuart);
			}
			else if(this.isMouseInRect == true && mouseInRect == false)
			{
				this.isMouseInRect = false;
				this.localRectTransform.DOKill();
				this.localRectTransform.DOAnchorPosX(this.defaultAnchorPosX, 0.25f).SetEase(Ease.OutQuart);
				
				this.trayRectTransform.DOKill();
				this.trayRectTransform.DOAnchorPosX(this.trayChildCount > 0 ? TRAY_VISIBLE_X : TRAY_HIDDEN_X, 0.25f).SetEase(Ease.OutQuart);
			}
		}
	}
}
