﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System;
using strange.extensions.mediation.impl;
using Messages;
using NovaBlitz.Game;
using DragonFoundry.GameState;
using NovaBlitz.UI;

public enum HandState
{
    DefaultState = 0,
    CardHovered = 1,
    CardSelected = 2,
    CardDragged = 3,
}

public class TargetedPowerCardInfo
{
    public CardController Card = null;
    public TargetArrowInfo Arrow = null;
    public IEnumerator FadeOutCoRoutine = null;
    public int? TargetId = null;
    public CardController HandCard;
    public float offset = 0;
}

public class PlayerHandController : View
{
    [Inject] public ArenaCards ArenaCards { get; set; }
    [Inject] public ArenaPlayers ArenaPlayers { get; set; }
    [Inject] public GameControllerService GameControllerService { get; set; }
    [Inject] public ClientPrefs ClientPrefs {get;set;}
    [Inject] public ArenaCardRecycledSignal ArenaCardRecycledSignal {get;set;}

    // =====================
    // Public Properties
    // =====================
    public UXValues UXValues;
    public CardDealer CardDealer;
    public ArenaZoomController ArenaCardZoom;
    public RectTransform DragArrowsRoot;
    public RectTransform PlayedPowerRoot;
    public RectTransform BoundsRect;
    public Canvas OverlayCanvas;
    public RectTransform VoidLocatoin;
    public NovaAudioPlayer DrawCardSound;
    public NovaAudioPlayer PlayCardSound;
    public InputEventController InputEventController;
    public TargetArrowController ArrowController;
    public ActivePowersLayout ActivePowersLayout;
    public PlayerControllerView PlayerController;
    public RectTransform ArenaFloor;
    public RectTransform InsertTransform;
    public RectTransform PowerInsertTransform;
    public HandLayout3D HandLayout;
    public PlayerArenaController ArenaController;
    public TextMeshProUGUI text;
    public float HOVER_DISTANCE_Y_16x9 = 500f;
    public float HOVER_DISTANCE_Y_16x9_IOS = 560;
    public float HOVER_DISTANCE_Y_4x3 = 375;
    public float HOVER_DISTANCE_Y_4x3_IOS = 480;
    public float HOVER_SCALE = 1.6f;
    public float HOVER_SCALE_IOS = 2f;
    public ArenaView ArenaView { get; set; }
    public int NumHoveredCards
    {
        get { return this.hoveredCardInserts.Count;}
    }

    public float HoverScale
    {
        get
        {
            return this.IsUsingZoomHand ? this.UXValues.ZoomHandHoverScale : this.UXValues.HoverScale;
        }
    }

    public float HoverDistanceY
    {
        get
        {
            float zoomHoverDistanceY = this.UXValues.ZoomHandHoverDistanceY + (this.canvasScaler.referenceResolution.y - ((RectTransform)this.sessionContextView.MainCanvas.transform).sizeDelta.y) * 0.5f;
            return this.IsUsingZoomHand ? zoomHoverDistanceY : this.UXValues.HoverDistanceY;
        }
    }

    public bool IsDraggingCard
    {
        get { return this.currentState == HandState.CardDragged || this.currentState == HandState.CardSelected; }
    }
    public bool IsHandZoomedIn {get; private set;}

    public bool IsZoomingIn {
        get {
            if(this.zoomSequence == null) return false;
            return this.zoomSequence.IsPlaying();
        }
    }

    public bool IsZoomingOut {
        get {
             if(this.returnSequence == null) return false;
            return this.returnSequence.IsPlaying();
        }
    }

    public int NumDeactivations { get; private set;}
    
    public bool IsUsingZoomHand;
	private bool? _MustSetZoomHandActiveTo;
	public bool? MustSetZoomHandActiveTo {get {return _MustSetZoomHandActiveTo;} set {
			Debug.LogFormat ("Must Set Zoom Active To {0}", value);
			_MustSetZoomHandActiveTo = value;
		}}

    public GameStateReadOnly GameState { get; set; }
    public CardController DraggedCard { get; private set; }
    private OpponentHandController opponentHandController;
    public Action<int, RectTransform, string> TriggerFxEvent;
    public Action<IEnumerable<ICardState>, bool> ShowTargets;
    public Action HideTargets;

    // =====================
    // Private Properties
    // =====================
    private Queue<RectTransform> insertTransforms = new Queue<RectTransform>();
    //private Queue<RectTransform> powerInsertTransforms = new Queue<RectTransform>();
    private BiDictionary<CardController, RectTransform> hoveredCardInserts = new BiDictionary<CardController, RectTransform>();
    private Transform insertsRoot;
    private float lastHoverTime;
    private Camera uiCamera;
    //private float arenaScale;
    private SessionContextView sessionContextView;
    private CanvasScaler canvasScaler;

    class Transition
    {
        public Transition(HandState fromState, InputEvent inputEvent, HandState toState, System.Action<CardController> transitionFunction = null)
        {
            this.FromState = fromState;
            this.ToState = toState;
            this.InputEvent = inputEvent;
            this.TransitionFunction = transitionFunction;
        }
        public HandState FromState;
        public HandState ToState;
        public InputEvent InputEvent;
        public System.Action<CardController> TransitionFunction;
    }

    // State transition data
    private bool _IsPlayingCard;
    public bool IsPlayingCard { get { return _IsPlayingCard; }
        set {
            //Debug.LogFormat("IsPlayingCard:{0}", value);
            if (value == false &&  _IsPlayingCard == true && this.HideTargets != null)
                HideTargets.Invoke();
            _IsPlayingCard = value;
        }
    }
    private List<Transition> transitions = new List<Transition>();
	private CardController _hoveredCard; // TODO: probably should have refactored this out and just relied on the hover state tracked in InputEventController
	public CardController HoveredCard { get { return _hoveredCard; } set {
			_hoveredCard = value;
			if (value == null) 
				this.ArenaCardZoom.HideKeywords();
		}
	}
    private CardController _selectedCard;
    public CardController SelectedCard
    {
        get { return _selectedCard; }
        private set
        {
            if (_selectedCard != null && value == null)
            {
                if(!IsPlayingCard && this.HideTargets != null)
                    HideTargets.Invoke();
                var cardControllers = ArenaCards.AllCardControllers;
                for (int i = 0; i < cardControllers.Count(); i++)
                {
                    cardControllers[i].RefreshLegalAbilities();
                }
            }
            else if (value != null)
            {
                var cardControllers = ArenaCards.AllCardControllers;
                for (int i = 0; i < cardControllers.Count(); i++)
                {
                    var cardController = cardControllers[i];
                    if (cardController != value)
                        cardControllers[i].ClearPlayAbilityStates();
                }
            }
            _selectedCard = value;

        }
    }
    private CardController scaledCard;
    private List<CardController> incommingCards = new List<CardController>();
    private Dictionary<int, TargetedPowerCardInfo> targetedPowerCards = new Dictionary<int, TargetedPowerCardInfo>();
    private HandState currentState = HandState.DefaultState;

    public HandState HandState { get { return this.currentState;}}

    // Input Event state data
    private Dictionary<InputEvent, bool> inputEvents = new Dictionary<InputEvent, bool>();
    private Dictionary<InputEvent, CardController> inputCards = new Dictionary<InputEvent, CardController>();
    private Vector2 mousePositionLastFrame;

    /// <summary>
    /// Utility method for the InputEventController so that when it raycasts a cardInsert in the hand
    /// it's able to determine the card being hovered from it
    /// </summary>
    public CardController GetCardController(RectTransform insert)
    {
        CardController hoveredCard = null;
        this.hoveredCardInserts.TryGetValue(insert, out hoveredCard);
        return hoveredCard;
    }

    public CardController GetTargetedPowerCard(CardController baseCard)
    {
        TargetedPowerCardInfo info;
        if (baseCard != null && targetedPowerCards.TryGetValue(baseCard.CardId, out info) && info.Card != null)
        {
            return info.Card;
        }
        return null;
    }

    void ConstrainCardToBounds(CardController draggedCard, RectTransform cardRect)
    {
        // Get the constraining Bounds rect
        Vector3[] corners = new Vector3[4];
        this.BoundsRect.GetWorldCorners(corners);
        Rect worldBounds = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[1].y - corners[0].y);
        Vector3 worldMin = new Vector3(worldBounds.xMin, worldBounds.yMin, this.BoundsRect.position.z);
        Vector3 worldMax = new Vector3(worldBounds.xMax, worldBounds.yMax, this.BoundsRect.position.z);
        Vector3 viewportMin = Camera.main.WorldToViewportPoint(worldMin);
        Vector3 viewportMax = Camera.main.WorldToViewportPoint(worldMax);
        Rect viewPortBounds = new Rect(viewportMin.x, viewportMin.y, viewportMax.x - viewportMin.x, viewportMax.y - viewportMin.y);

        // Get the draggedCard's bounds
        Vector3[] corners2 = new Vector3[4];
        cardRect.GetWorldCorners(corners2);
        Rect cardBounds = new Rect(corners2[0].x, corners2[0].y, corners2[2].x - corners2[0].x, corners2[1].y - corners2[0].y);
        Vector3 cardWorldMin = new Vector3(cardBounds.xMin, cardBounds.yMin, draggedCard.transform.position.z);
        Vector3 cardWorldMax = new Vector3(cardBounds.xMax, cardBounds.yMax, draggedCard.transform.position.z);
        Vector3 cardViewportMin = Camera.main.WorldToViewportPoint(cardWorldMin);
        Vector3 cardViewportMax = Camera.main.WorldToViewportPoint(cardWorldMax);
        Rect cardViewPortBounds = new Rect(cardViewportMin.x, cardViewportMin.y, cardViewportMax.x - cardViewportMin.x, cardViewportMax.y - cardViewportMin.y);

        Vector3 draggedCardViewport = Camera.main.WorldToViewportPoint(draggedCard.transform.position);

        if (cardViewPortBounds.xMin < viewPortBounds.xMin) { draggedCardViewport.x += viewPortBounds.xMin - cardViewPortBounds.xMin; }
        else if (cardViewPortBounds.xMax > viewPortBounds.xMax) { draggedCardViewport.x += viewPortBounds.xMax - cardViewPortBounds.xMax; }
        if (cardViewPortBounds.yMin < viewPortBounds.yMin) { draggedCardViewport.y += viewPortBounds.yMin - cardViewPortBounds.yMin; }
        else if (cardViewPortBounds.yMax > viewPortBounds.yMax) { draggedCardViewport.y += viewPortBounds.yMax - cardViewPortBounds.yMax; }

        Vector3 worldOffset = Camera.main.ViewportToWorldPoint(draggedCardViewport);
        draggedCard.transform.position = worldOffset;
    }

    /// <summary>
    /// Instantiates a bunch of insert transforms form the referenced one.
    /// Stores them in a queue for dealing several cards at once.
    /// </summary>
    protected override void Start()
    {
        base.Start();
        /*if(this.ClientPrefs.IsWideArena)
        {
            this.arenaScale = 1f;
        }
        else
        {
            this.arenaScale = 0.75f;
        }*/

        this.opponentHandController = GameObject.FindObjectOfType<OpponentHandController>();

        this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
        this.sessionContextView = this.GetComponentInParent<SessionContextView>();
        this.canvasScaler = this.sessionContextView.MainCanvas.GetComponent<CanvasScaler>();
        // TODO: Add a rightClick input that cancels dragging state

        /*        
        HandLayout3D State Transitions based on Input Events.
                          _______________________________________________________________________
                          |   DefaultState  |   CardHovered   |  CardSelected  |  CardDragged   | < STATES
        __________________|_________________|_________________|________________|________________|
        |<MouseOverHand>  |        X        |        X        |       X        |  CardSelected  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOutHand>   |        X        |   DefaultState  |   CardDragged  |       X        |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOutHandCard>        X        |   DefaultState  |       X        |       X        |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOverHandCard>  CardHovered   |   CardHovered   |       X        |       X        |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOverCard>  |   CardHovered   |   CardHovered   |       X        |       X        |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOutCard>   |        X        |   DefaultState  |       X        |       X        |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseUpHand>    |        X        |  *DefaultState  |  CardHovered   |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseUpCard>    |        X        |  *DefaultState  |  CardHovered   |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseUpArena>   |        X        |        X        |       X        |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseDown>      |        X        |        X        |  CardHovered   |       X        |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOut>       |        X        |  DefaultState   |  CardDragged   |       X        |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOver>      |        X        |        X        |       X        |  CardSelected  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseClick>     |        X        |  CardSelected   |  CardHovered   |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<MouseOutWindow> |        X        |  DefaultState   |  DefaultState  |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<CardDraggedUp>  |        X        |  CardSelected   |       X        |  CardSelected  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<LostCastAbility>|        X        |        X        |  DefaultState  |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        |<SummonFromHand> |  DefaultState   |  DefaultState   |  DefaultState  |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|
        
            ^ INPUTS
        */

        this.transitions.Clear();

        // From State.DefaultState
        this.transitions.Add(new Transition(HandState.DefaultState, InputEvent.MouseOverHandCard, HandState.CardHovered, this.OnCardHovered));
        this.transitions.Add(new Transition(HandState.DefaultState, InputEvent.SummonFromHand, HandState.DefaultState, this.OnCardSummoned));
        this.transitions.Add(new Transition(HandState.DefaultState, InputEvent.DiscardFromHand, HandState.DefaultState, this.OnCardDiscarded));

        // From State.CardHovered
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseOverHandCard, HandState.CardHovered, this.OnCardHovered));
        //this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseClick, HandState.CardSelected, this.SelectHoveredCard));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.CardDraggedUp, HandState.CardSelected, this.SelectHoveredCard));
        //this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseUpCard, HandState.CardSelected, this.SelectHoveredCard));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseUpHand, HandState.DefaultState, this.ReturnCardToHand));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.SummonFromHand, HandState.DefaultState, this.OnCardSummoned));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.DiscardFromHand, HandState.DefaultState, this.OnCardDiscarded));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseOutHandCard, HandState.DefaultState, this.ReturnCardToHand));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseOutWindow, HandState.DefaultState, this.ReturnCardToHand));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseOutHand, HandState.DefaultState, this.ReturnCardToHand));
        this.transitions.Add(new Transition(HandState.CardHovered, InputEvent.MouseUpArena, HandState.DefaultState, this.ReturnCardToHand));

        // From State.CardSelected
        this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.MouseOutHand, HandState.CardDragged, this.MoveCardOutOfHand));
#if UNITY_IOS || UNITY_ANDROID
        this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.MouseUpHand, HandState.DefaultState, this.ReturnSelectedCardToDefault));
#else
        this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.MouseUpHand, HandState.CardHovered, ReturnSelectedCardToHand));
#endif
        //this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.MouseUpCard, HandState.CardHovered, this.ReturnSelectedCardToHand));
        //this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.MouseClick, HandState.CardHovered, this.ReturnSelectedCardToHand));
        this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.MouseOutWindow, HandState.DefaultState, this.ReturnSelectedCardToDefault));
        this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.LostCastAbility, HandState.DefaultState, this.ReturnSelectedCardToDefault));
        this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.SummonFromHand, HandState.DefaultState, this.OnCardSummoned));
        this.transitions.Add(new Transition(HandState.CardSelected, InputEvent.DiscardFromHand, HandState.DefaultState, this.OnCardDiscarded));

        // From State.CardDragged
        this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.MouseOverHand, HandState.CardSelected, this.ReturnDraggedCardToHand));//HandState.DefaultState, this.CardOutOfWindow));//
        this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.MouseUpArena, HandState.DefaultState, this.PlayCard));
        //this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.MouseClick, HandState.DefaultState, this.PlayCard));
        this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.MouseUpCard, HandState.DefaultState, this.PlayCard));
		this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.MouseUpHand, HandState.DefaultState, this.ReturnDraggedCardToDefault));
        this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.MouseOutWindow, HandState.DefaultState, this.CardOutOfWindow));
        this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.LostCastAbility, HandState.DefaultState, this.CardOutOfWindow));
        this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.SummonFromHand, HandState.DefaultState, this.OnCardSummoned));
        this.transitions.Add(new Transition(HandState.CardDragged, InputEvent.DiscardFromHand, HandState.DefaultState, this.OnCardDiscarded));

        // Create a new root transform for ineserts
        this.insertsRoot = new GameObject().transform;
        this.insertsRoot.name = "InsertsFor_" + this.name;
        this.insertsRoot.SetParent(this.transform.parent, false);

        // Attach the default insert to the new root
        this.InsertTransform.gameObject.SetActive(false);
        this.InsertTransform.SetParent(this.insertsRoot);
        this.insertTransforms.Enqueue(this.InsertTransform);
        this.InsertTransform.name = "Insert 0";

        // Create 9 copies of the default insert
        for (int i = 1; i < 20; i++)
        {
            RectTransform trans = GameObject.Instantiate(this.InsertTransform);
            trans.SetParent(this.transform);
            trans.localScale = Vector3.one;
            trans.localPosition = Vector3.zero;
            trans.name = "Insert " + i;
            this.insertTransforms.Enqueue(trans);
            trans.SetParent(this.insertsRoot);
        }
    }

    internal void OnCardDiscarded(CardController card)
    {
        //CardController card = this.ArenaCards.GetCard(cardId);
        

        card.RaycastTag = "Untagged"; // make sure the discarded card isn't hovered again

         RectTransform cardInsert = null;
        if (this.hoveredCardInserts.TryGetValue(card, out cardInsert))
        {
            cardInsert.GetComponent<CanvasGroup>().alpha = 0.0f;
        }

        ICardState cardState = this.GameState.GetCard(card.CardId);
        if(cardState.ControllerId == this.GameState.SelfId)
        {
            Debug.LogFormat("#PlayerHand# Discarding player card: {0} (already unhovered)", card.CardIdAndName);
            this.StartCoroutine(DelayTrash(card));
        }
        else
        {
            Debug.LogFormat("#PlayerHand# unable to discard card:{0}", card.CardIdAndName);
        }
        
    }


    private IEnumerator DelayTrash(CardController card)
    {
        // Wait until end of frame incase a card is dealt and discarded in the same frame
        yield return null;
        yield return null;

        if (card is CardFrontController)
        {
            CardFrontController cardFront = (CardFrontController)card;
            card.gameObject.name = "CardFront (Waiting to Trash)";
            cardFront.DeathOverlayCanvasGroup.DOKill();
            cardFront.DeathOverlayCanvasGroup.alpha = 0;
            cardFront.DeathOverlayCanvasGroup.DOFade(1.0f, 0.3f);
        }

        if (this.CardDealer.IsCardBeingDealt(card))
        {
            while(this.CardDealer.IsCardBeingDealt(card))
            {
                //Debug.LogFormat("Discard waiting for card to be dealt ID: {0} {1}", card.CardId, card.TitleLabel.text);
                yield return new WaitForSecondsRealtime(0.1f);
            }
        }

        if(this.incommingCards.Contains(card))
        {
            //Debug.LogFormat("Discard waiting for incomming card ID: {0} {1}", card.CardId, card.TitleLabel.text);
            yield return new WaitForSecondsRealtime(0.1f);
        }

        yield return new WaitForSecondsRealtime(2.0f);

        // Kill any remaining tweens on the card, like for examlpe "ReturnToHand"
        card.transform.DOKill();

        RectTransform cardInsert = null;
        if (this.hoveredCardInserts.TryGetValue(card, out cardInsert))
        {
            Debug.LogFormat("#PlayerHand# Discarding player card {0} and removing its insert", card.CardIdAndName);
            cardInsert.AttachToParent(this.insertsRoot,0);
            this.insertTransforms.Enqueue(cardInsert);
            this.hoveredCardInserts.Remove(card);
        }

        // Find any orphaned inserts in the hand, the CardDealer doesn't add it's insert to hoveredInsert
        // so that the automatic positionCardOverInsertX logic doesn't interfere with it's deal tween
        foreach(Transform child in this.transform)
        {
            CardController tempCard = null;
            if(this.hoveredCardInserts.TryGetValue((RectTransform)child, out tempCard) == false)
            {
                if(child.gameObject.GetComponentInChildren<CardFrontController>() == null)
                {
                    child.AttachToParent(this.insertsRoot,0);
                    this.insertTransforms.Enqueue((RectTransform)child);
                    child.gameObject.SetActive(false);
                }
            }
        }

        // Look for any power infos that might be hanging around
        TargetedPowerCardInfo powerCardInfo = null;
        if(this.targetedPowerCards.TryGetValue(card.CardId, out powerCardInfo) && powerCardInfo != null)
        {
            Debug.LogError("Targeted power discarded");

            // Stop the coRoutine currently responsible for fading out the power card
            if(powerCardInfo.FadeOutCoRoutine != null)
            {
                this.StopCoroutine(powerCardInfo.FadeOutCoRoutine);
            }
            AnimateCardToTrash(card.CardId, powerCardInfo);

            // Now clean up the power and any associated arrows etc.

            //powerCardInfo.Card.CanvasGroup.DOFade(0, 0.0f);
            this.ActivePowersLayout.ReleasePowerCard(powerCardInfo.Card);
            this.ArrowController.ReleaseDraggedArrow(powerCardInfo.Arrow);

            //this.targetedPowerCards.Remove(card.CardId);
            this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);
            /*if(powerCardInfo.HandCard != null)
            {
                powerCardInfo.HandCard.transform.DOKill();
                powerCardInfo.HandCard.Recycle();
            }*/
        }
        card.transform.DOKill();


        int cardId = card.CardId;
        this.ArenaCards.RemoveCard(card);

        card.Recycle();

        yield return null; // waita until next frame

        this.ArenaCardRecycledSignal.Dispatch(cardId, true, this.GameState.Turn.Phase);

        yield return null; // wait for end of frame

        this.HandLayout.UpdateCardTweens();
    }

    private bool TryPlayCard(int cardId, int? targetId)
    {
        int abilityId = this.GameControllerService.SubmitCastAction(cardId, targetId);

        if (abilityId != 0)
        {
            this.ArenaCards.AddPendingCardAbility(abilityId, cardId);
            return true;
        }
        else
        {
			Debug.LogFormat(" Unable to play Card with ID = {0} and targetId:{1}", cardId, targetId);
            // Was unable to submit cast
            return false;
        }
    }

    public void OnCardSummoned(CardController summonedCard)
    {
        // Is the card Dragged?
        if (this.DraggedCard == summonedCard)
        {
            Debug.LogFormat("Dragged Card Summoned: {0}", summonedCard.CardIdAndName);
            this.ArenaController.OnPlayCard(summonedCard);
        }
        else
        {
            Debug.LogFormat("Card Summoned: {0}", summonedCard.CardIdAndName);
            // Try to get an existing insert for the card
            RectTransform cardInsert = null;
            if (this.hoveredCardInserts.TryGetValue(summonedCard, out cardInsert) == false)
            {
                // Get a new card insert for the card
                cardInsert = this.insertTransforms.Dequeue();
                cardInsert.gameObject.SetActive(true);
                this.hoveredCardInserts[summonedCard] = cardInsert;
            }

            // Hide the insert art
            cardInsert.GetChild(0).gameObject.SetActive(false);

            // Attach the insert to the Arena Floor
            cardInsert.AttachToParent(this.ArenaController.ArenaLayout.transform, 0, sortingOrder: 0);
            this.ArenaController.ArenaLayout.UpdateCardTweens();
            this.HandLayout.UpdateCardTweens();

            // Attach the card to the insert
            summonedCard.transform.AttachToParent(cardInsert, 1, sortingOrder: 100);
            summonedCard.RaycastTag = "Untagged";

            // Run a sequence to animate the card into the arena
            Vector3 cardPos = new Vector3(0f, 75, -200f);
            Sequence seq = DOTween.Sequence();
            seq.Append(summonedCard.transform.DOLocalMove(cardPos, 0.1f));
            seq.Insert(0, summonedCard.transform.DOScale(new Vector3(2f, 2f, 2f), 0.1f));
            seq.AppendInterval(0.05f);
            seq.Append(summonedCard.transform.DOLocalMove(Vector3.zero, 0.07f).SetEase(Ease.InExpo));
            seq.AppendCallback(() =>
                {
                    summonedCard.RaycastTag = "arenaCard";
                    summonedCard.transform.AttachToParent(this.ArenaController.ArenaLayout.transform, cardInsert.GetSiblingIndex(), overrideSorting: false, blocksRaycasts: true);
                    summonedCard.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                    summonedCard.name = "ArenaCard: CardCanvas" + this.transform.childCount;
                    summonedCard.PlayerArenaController = this.ArenaController;

                    // Return the insert to the inserts queue
                    cardInsert.gameObject.SetActive(false);
                    this.hoveredCardInserts.Remove(cardInsert);
                    cardInsert.AttachToParent(this.insertsRoot, 0);
                    this.insertTransforms.Enqueue(cardInsert);

                    this.ArenaController.ArenaLayout.UpdateCardTweens();
                    this.HandLayout.UpdateCardTweens();
                });
            seq.Play();
        }

        if (summonedCard == this.SelectedCard)
        {
            this.SelectedCard = null;
        }

        if (summonedCard == this.DraggedCard)
        {
            this.DraggedCard = null;
        }

        if (summonedCard == this.HoveredCard)
        {
            this.HoveredCard = null;
        }
    }

    internal void OnCardSpawnedAndBanished(ICardState sourceCardState, int spawnCardId, GameStateReadOnly gameState)
    {
        // Position them over the source card initially
        CardController sourceCardController = null;
        sourceCardController = sourceCardState == null ? null : this.ArenaCards.GetCard(sourceCardState.Id);
        CardController spawnCard = this.ArenaCards.GetCard(spawnCardId);
		Debug.LogFormat("#PlayerHand#SpawnedAndBanished Affected:{0} is ({1}) over source:{2} is ({3})", spawnCardId,
            (spawnCard != null ? spawnCard.name : "null"), sourceCardState != null ? sourceCardState.Id : -10000,
            (sourceCardController != null ? sourceCardController.name : "null"));

        /*bool spawnCardIsPower = false;
        if(spawnCard is CardFrontController)
        {
            CardFrontController cardFront = (CardFrontController)spawnCard;
            if(cardFront.IsPower && this.opponentHandController != null)
            {
                spawnCardIsPower = true;
            }
        }*/

        spawnCard.transform.AttachToParent(this.OverlayCanvas.transform, this.OverlayCanvas.transform.childCount, sortingOrder:90, blocksRaycasts:false);
        spawnCard.CanvasGroup.alpha = 0f;
        spawnCard.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        spawnCard.transform.DORotate(Vector3.zero, 0.2f);

        Sequence seq = DOTween.Sequence();

        seq.AppendInterval(0.2f);
        seq.Append(spawnCard.CanvasGroup.DOFade(1f, 0.1f));
        seq.AppendCallback(()=>{
            var viewPoint = uiCamera.WorldToViewportPoint(spawnCard.transform.position);
            if(viewPoint.x >= 0 && viewPoint.x <= 1 && viewPoint.y >= 0 && viewPoint.y <= 1 && viewPoint.z > 0)
            {
                // reset rotation etc; do nothing.
            }
            else if (sourceCardController != null)
            {
                spawnCard.transform.position = sourceCardController.transform.position;
                this.UpdateSpawnCardPositionIfNeeded(spawnCard, sourceCardState.Id);
                spawnCard.transform.localScale = sourceCardController.transform.localScale;
            }
            else
            {
                spawnCard.transform.position = this.VoidLocatoin.position;
                spawnCard.transform.localScale = Vector3.one;
            }
            Vector3 cameraDir = (this.uiCamera.transform.position - spawnCard.transform.position).normalized;
            spawnCard.transform.DOMove(spawnCard.transform.position + cameraDir * 5f, 0.3f);
        });
        seq.AppendInterval(0.3f);
        seq.AppendCallback(()=>{
            RectTransform cardInsert;
            if(this.hoveredCardInserts.TryGetValue(spawnCard, out cardInsert) == false)
            {
                cardInsert = this.insertTransforms.Dequeue();
                cardInsert.gameObject.SetActive(true);
                cardInsert.rotation = Quaternion.identity;
                this.hoveredCardInserts[spawnCard] = cardInsert;
            }

            this.incommingCards.Add(spawnCard);

            cardInsert.AttachToParent(this.HandLayout.transform, this.HandLayout.transform.childCount);
            cardInsert.localPosition = this.HandLayout.GetChildPosition(cardInsert);
            this.HandLayout.UpdateCardTweens();
            spawnCard.transform.DOMove(cardInsert.position, 0.4f);
            spawnCard.transform.DOScale(Vector3.one, 0.3f);
        });
        seq.AppendInterval(0.1f);
        seq.OnComplete(()=>{            
            RectTransform cardInsert = this.hoveredCardInserts[spawnCard];
            spawnCard.RaycastTag = "card";
            spawnCard.transform.AttachToParent(this.transform, cardInsert.GetSiblingIndex(), blocksRaycasts: true);
            cardInsert.AttachToParent(this.insertsRoot,0);
            this.insertTransforms.Enqueue(cardInsert);
            this.hoveredCardInserts.Remove(spawnCard);
            cardInsert.gameObject.SetActive(false);
            this.incommingCards.Remove(spawnCard);
            spawnCard.DisplayLegalAbilities(gameState.LegalAbilities);
            this.HandLayout.UpdateCardTweens();
            
        });
        seq.Play();
    }

    public void OnCardBanished(int cardId, GameStateReadOnly gameState)
    {
        // Get a reference to the arena card
        CardFrontController banishedCard = (CardFrontController)this.ArenaCards.GetCard(cardId);
        banishedCard.gameObject.SetActive(true);
        banishedCard.ResetCardFront();

        // Play a banish effect on it
        if (TriggerFxEvent != null)
            TriggerFxEvent.Invoke(banishedCard.CardId, banishedCard.Card.rectTransform, "fx_cardbanish");

        // scale it up
        banishedCard.transform.AttachToParent(this.OverlayCanvas.transform, 0, sortingOrder: 90, blocksRaycasts: false);
        banishedCard.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        banishedCard.transform.DORotate(Vector3.zero, 0.2f);
        banishedCard.transform.DOLocalMoveZ(banishedCard.transform.localPosition.z - 200, 0.4f)
        .OnComplete(() =>
        {
            // attach it to the hand
            banishedCard.transform.AttachToParent(this.transform, 0, blocksRaycasts: true);
            banishedCard.RaycastTag = "card";
            this.HandLayout.UpdateCardTweens();
        });
    }

    private void UpdateSpawnCardPositionIfNeeded(CardController spawnCard, int sourceCardId)
    {
         // Handle positining a spawncard when the card is an opponent power that's currently being played
        if(spawnCard is CardFrontController)
        {
            CardFrontController cardFront = (CardFrontController)spawnCard;
            if(cardFront.IsPower && this.opponentHandController != null)
            {
                CardController opponentPower = this.opponentHandController.GetPlayedPower(sourceCardId);
				Debug.LogFormat("#PlayerHand# SpawnCard is Power! sourceCardId is {0}", sourceCardId);
                if(opponentPower != null)
                {
					Debug.LogFormat("#PlayerHand# SourceCard is opponent played power! {0}", sourceCardId);
                    spawnCard.transform.position = opponentPower.transform.position;
                }
            }
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (insertsRoot != null)
        {
            Destroy(insertsRoot);
        }
    }

    internal void OnPlayCardSucceeded(ICardState cardState)
    {
        TargetedPowerCardInfo targetedCardInfo = null;
        
        //TargetedPowerCardInfo targetedCardInfo = this.targetedPowerCards.Where(kvp=>kvp.Key.CardId == cardState.Id).Select(o=>o.Value).FirstOrDefault();
        //Debug.LogFormat("#PCI# play card succeeded, TPCcount:{0}, cs:{1}, Card is {2}", this.targetedPowerCards.Count, cardState.Id, targetedCardInfo.Card == null ? "null" : targetedCardInfo.Card.CardId.ToString());
        if(this.targetedPowerCards.TryGetValue(cardState.Id, out targetedCardInfo) && targetedCardInfo != null)
        {
            //Debug.Log("Play Card Succeeded, power info has a value!");
            if(targetedCardInfo.Arrow != null)
            {
                targetedCardInfo.Arrow.StartingColor = new Color(255/255f,23/255f, 23/255f,255/255f);
            }
            targetedCardInfo.Card.CardGlowAnimator.SetBool("IsPending", false);
            IEnumerator fadeOutCardCoRoutine = this.FadeOutPowerCard(cardState.Id);
            targetedCardInfo.FadeOutCoRoutine = fadeOutCardCoRoutine;
            StartCoroutine(fadeOutCardCoRoutine);
        }
        else
        {
            // Try to show the aspectt frame (this ends up affecting player and opponent cards)
          
            //CardController card = this.ArenaCards.GetCard(cardState.Id);
            // card.ShowAspectFrame();
            
            Debug.Log("Play Card Succeeded, power info is null");
        }
        this.IsPlayingCard = false;
    }


    internal void OnPlayCardFailed(ICardState cardState)
    {
        Debug.LogError("#PlayerHand# PLAY CARD FAILED!!!");
        CardController card = this.ArenaCards.GetCard(cardState.Id);
        if(cardState.IsUnit)
        {
            this.ArenaController.OnPlayCardFailed(cardState);
            if (card != null)
            {
                this.StartCoroutine(this.WaitToReturnCardToHand(card));
            }
        }
        else
        {
            if(card != null)
            {
                card.gameObject.SetActive(true);
                card.CanvasGroup.alpha = 0;

                // Create an insert in the hand for the card again
                GetAndAttachInsert(card);
                /*RectTransform insert = this.insertTransforms.Dequeue();
                insert.gameObject.SetActive(true);
                int siblingIndex = 0;
                insert.AttachToParent(this.transform, siblingIndex, sortingOrder: this.HandLayout.BaseIndex + siblingIndex * 5, overrideSorting: true);
                Vector3 localPos = insert.localPosition; localPos.z = 0; insert.localPosition = localPos;
                insert.GetChild(0).gameObject.SetActive(true);

                insert.GetComponent<CanvasGroup>().alpha = 1.0f;
                this.hoveredCardInserts[card] = insert;
                insert.localRotation = Quaternion.identity;*/

                card.transform.DOKill();
                this.CardOutOfWindow(card);
                card.CanvasGroup.alpha = 1f;

                TargetedPowerCardInfo powerCardInfo = null;
                if(this.targetedPowerCards.TryGetValue(card.CardId, out powerCardInfo) && powerCardInfo != null)
                {
                    Debug.LogError("Targeted power canceled");

                    // Stop the coRoutine currently responsible for fading out the power card
                    if(powerCardInfo.FadeOutCoRoutine != null)
                    {
                        this.StopCoroutine(powerCardInfo.FadeOutCoRoutine);
                    }

                    // Now clean up the power and any associated arrows etc.
                    
                    powerCardInfo.Card.CanvasGroup.DOFade(0,0.0f);
                    if (powerCardInfo.TargetId.HasValue)
                    {
                        CardFrontController targetCardFront = (CardFrontController)this.ArenaCards.GetCard(powerCardInfo.TargetId.Value);
                        if(targetCardFront != null)
                        {
                            targetCardFront.CardGlowAnimator.SetBool("IsHovered", false);
                            targetCardFront.CardGlowAnimator.SetBool("IsSpinning", false);
                        }
                    }

                    this.ArrowController.ReleaseDraggedArrow(powerCardInfo.Arrow);

                    this.targetedPowerCards.Remove(card.CardId);
                    this.ActivePowersLayout.ReleasePowerCard(powerCardInfo.Card);
                    powerCardInfo.Card.transform.DOKill();
                    powerCardInfo.Card.Recycle();
                    this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);
                }

                this.ReturnHoveredCardToHand(card);
            }
            else
            {
                Debug.LogError("#PlayerHand# Power Card is Null :(");
            }
        }
        this.ActivePowersLayout.IsDraggingTargetedPower = false;
        this.IsPlayingCard = false;
    }

    private RectTransform GetAndAttachInsert(CardController card)
    {

        // Create an insert in the hand for the card again
        RectTransform insert = this.insertTransforms.Dequeue();
        insert.gameObject.SetActive(true);
        int siblingIndex = 0;
        insert.AttachToParent(this.transform, siblingIndex, sortingOrder: this.HandLayout.BaseIndex + siblingIndex * 3, overrideSorting: true);
        Vector3 localPos = insert.localPosition; localPos.z = 0; insert.localPosition = localPos;
        insert.GetChild(0).gameObject.SetActive(true);

        insert.GetComponent<CanvasGroup>().alpha = 1.0f;
        this.hoveredCardInserts[card] = insert;
        insert.localRotation = Quaternion.identity;
        return insert;
    }

    private IEnumerator WaitToReturnCardToHand(CardController card)
    {
        // I assume this waits for the play aniamtion to complete in the arena before yanking it back
        yield return new WaitForSecondsRealtime(1.0f);
        card.transform.DOKill();
        var playedCardInsert = card.transform.parent.transform as RectTransform;
        card.transform.AttachToParent(this.transform, this.transform.childCount, sortingOrder: this.transform.childCount + 90, overrideSorting: true, blocksRaycasts: true);
        card.CanvasGroup.alpha = 1.0f;
        card.RaycastTag = "card";
        card.Canvas.sortingLayerName="Default";
        card.CanvasGroup.interactable = true;
        //((CardFrontController)card).CardGlowAnimator.SetBool("IsPending", false);
        //((CardFrontController)card).CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        if (card == this.SelectedCard)
        {
            this.SelectedCard = null;
        }
        if (card == this.DraggedCard)
        {
            this.DraggedCard = null;
        }

        /*Debug.Log("Can't tell if the cast requires target");
        this.lastHoverTime = Time.unscaledTime;
        this.selectedCard = null;
        this.hoveredCard = null;
        this.DraggedCard = null;*/
        this.ReturnCardToHand(card);
        this.TryToReleasePowerCardInfo(card);
        this.ArrowController.ReleaseDraggedArrow();
        this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);

        this.ArenaController.ArenaLayout.UpdateCardTweens();
        this.HandLayout.UpdateCardTweens();


        // clean up the insert
        this.ArenaController.RecycleInsert(playedCardInsert);
    }

    public void OnLegalAbilitiesUpdated(GameStateReadOnly gameState)
    {
        if (this.DraggedCard != null)
        {
            if (gameState.LegalAbilities.CanCast(this.DraggedCard.CardId) == false)
            {
                this.TriggerInput(InputEvent.LostCastAbility, this.DraggedCard);
            }
            this.ShowCastTargets();
        }
    }

    /// <summary>
    /// Determins if the cardControllers "Cast" ability requires 1 or more targets
    /// </summary>
    public bool CastRequiresTarget(CardController activeCard)
    {
		if (activeCard == null)
			return false;
		
        if (this.GameState != null)
        {
            return this.GameState.LegalAbilities.CastRequiresTarget(activeCard.CardId);
        }
        else
        {
            Debug.LogError("#PlayerHand# Gamestate is null!");
        }

        return false;
    }
    /*
    public void TryAddCardToTargets(CardController playedCard)
    {
        if (this.IsDraggingCard)
        {
            bool isTargeted;
            List<int> targets = this.GameState.LegalAbilities.GetCastTargets(this.DraggedCard.CardId, out isTargeted);
            if (targets != null && targets.Contains(playedCard.CardId))
            {
                if(isTargeted)
                {
                    playedCard.TargetArrowTarget.TargetGlowAnimator.SetBool("IsCastTarget", true);
                    playedCard.TargetArrowTarget.TargetAction = TargetAction.Attack;
                }
                else
                {
                    playedCard.TargetArrowTarget.TargetGlowAnimator.SetBool("IsAffected", true);
                    playedCard.TargetArrowTarget.TargetAction = TargetAction.Affected;
                }
                this.InputEventController.TargetArrowController.AddTarget(playedCard.TargetArrowTarget);
            }
        }
	}
    */
    /// <summary>
    /// Primarily handles the state trasition where there's a mouseUp over the arena
    /// and we're currently dragging a card over it. Does some basic animation tweening
    /// to stub out a UX for playing a card.
    /// </summary>
    public void PlayCard(CardController activeCard)
    {
        CardController card = this.DraggedCard;

		Debug.LogFormat("#PlayerHand# PlayCard:{0}", (card != null ? card.CardIdAndName : "?"));
        IsPlayingCard = true;

        //this.HoveredCard = null;
        //this.DraggedCard = null;
        //this.SelectedCard = null;

        bool castRequiresTarget = this.CastRequiresTarget(card);
        TargetArrowTarget target = null;
        if (castRequiresTarget)
        {
            target = this.ArrowController.GetDraggedArrowTarget();
        }

        bool playCardSucceeded = false;

        if (castRequiresTarget)
        {
            if (target != null)
            {
                playCardSucceeded = this.TryPlayCard(card.CardId, target.Id);
            }
        }
        else
        {
            playCardSucceeded = this.TryPlayCard(card.CardId, null);
        }

        ClearTargets();

        if (this.CastRequiresTarget(card))
        {

			Debug.LogFormat("#PlayerHand#{0} requires a target and the success is:{1}", card.CardIdAndName, playCardSucceeded);

            if (playCardSucceeded)
            {
                TargetedPowerCardInfo targetedCardInfo = this.targetedPowerCards[card.CardId];
                ((CardFrontController)targetedCardInfo.Card).ResetGlows();
                targetedCardInfo.Card.CardGlowAnimator.SetBool("IsPending", true);
                targetedCardInfo.TargetId = target.Id;
                this.ArrowController.DraggedTargetArrowInfo.FollowDestination = target.transform;
                this.ArrowController.DraggedTargetArrowInfo.StartingColor = new Color32(255,23,23,80);
                this.StartCoroutine(this.ArrowController.FadeOutArrowHead(2.0f, 0.75f));
            
                if(target.BoundsRect != null)
                {
                    this.ArrowController.DraggedTargetArrowInfo.FollowDestination = target.BoundsRect;
                }

                // Hide the card, (A trash message will clean it up later)
                card.CanvasGroup.alpha = 0;
                card.CanvasGroup.interactable = false;

                 // This isn't dragged anymore
                this.ArrowController.DraggedTargetArrowInfo = null;

                // TODO: Remove the insert when the card leaves the hand
                // and re-add it when the card enters.

                // Return the insert temporairly attached to the arena floor, to the queue
                //      NOTE: the problem with this is that card dragging is done by moving an insert
                //      and having a card position above it... so the code that does that in Update() will have to be
                //      signifigantly updated so to speak.
                // Return the insert temporairly attached to the arena floor, to the queue
                RectTransform cardInsert = null;

                // If we're playing a card that was selected after a UnHoverAll() during a zoom hand deactivation, it may not have an insert
                if(this.hoveredCardInserts.TryGetValue(card, out cardInsert))
                {
                    this.hoveredCardInserts.Remove(cardInsert);
                    cardInsert.AttachToParent(this.insertsRoot, 0, overrideSorting: true);
                    cardInsert.gameObject.SetActive(false);
                    this.insertTransforms.Enqueue(cardInsert);
                }

                if (GameState != null && GameState.Turn.Phase == Phase.Combat)
                {
                    AttachToTricksHolder(targetedCardInfo.Card);
                }
            }
            else // !playCardSucceeded
            {
				Debug.LogFormat("#PlayerHand# Returning power to hand -{0}", card.CardIdAndName);
                card.CanvasGroup.alpha = 1f;
                this.lastHoverTime = Time.unscaledTime;
                //this.SelectedCard = null;
                //this.HoveredCard = null;
                //this.DraggedCard = null;
                this.ReturnCardToHand(card);
                this.TryToReleasePowerCardInfo(card);
                this.ArrowController.ReleaseDraggedArrow();
                this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);
                this.IsPlayingCard = false;
            }
        }
        else // untargeted
        {

            if (playCardSucceeded)
            {
                Debug.Log("Play card succeeded");
                // Return the insert temporairly attached to the arena floor, to the queue
                RectTransform cardInsert = null;

                // If we're playing a card that was selected after a UnHoverAll() during a zoom hand deactivation, it may not have an insert
                if(this.hoveredCardInserts.TryGetValue(card, out cardInsert))
                {
                    this.hoveredCardInserts.Remove(cardInsert);
                    cardInsert.AttachToParent(this.insertsRoot, 0, overrideSorting: true);
                    cardInsert.gameObject.SetActive(false);
                    this.insertTransforms.Enqueue(cardInsert);
                }

                this.HandLayout.UpdateCardTweens();

                switch (card.CardType)
                {
                    // If it's a Unit, play it to the arena
                    case CardType.Unit:
                        this.ArenaController.OnPlayCard(card);
                        break;
                    // If it's an untargeted power, consume it
                    case CardType.Power:
                        this.ArenaController.OnCardDragEnd();
						Debug.LogFormat("#PlayerHand# Player casts untargeted power of {0}", card.CardIdAndName);

                        // Spawn a copy of the dragged card over the hand
                        CardFrontController zoomGO = this.ArenaCards.CreateCard(((CardFrontController)card).CardState);
                        zoomGO.transform.position = activeCard.transform.position;

                        // Hide the existing card
                        card.gameObject.SetActive(false);

                        zoomGO.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
                        zoomGO.SortingOrder = 200;

                        // Tween it over to the player Portrait
                        //zoomGO.transform.DOLocalMove(Vector3.zero,1200f).SetEase(Ease.OutQuart).SetSpeedBased();
                        TargetedPowerCardInfo targetInfo = new TargetedPowerCardInfo { Card = zoomGO, HandCard = card };
                        zoomGO.CardGlowAnimator.SetBool("IsPowerCardFloating",true);
                        //this.ActivePowersLayout.AddPowerCard(targetInfo.Card);
                        //targetInfo.Insert = powerInsert;
                        this.targetedPowerCards[card.CardId] = targetInfo;
                        
                        if(GameState != null && GameState.Turn.Phase == Phase.Combat)
                        {
                            AttachToTricksHolder(zoomGO);
                        }

                        break;
                }
            }
            else // !playCardSucceeded
            {
                Debug.Log("Failed to play untargeted card");
                card.CanvasGroup.alpha = 1f;
                this.lastHoverTime = Time.unscaledTime;
                //this.SelectedCard = null;
                //this.HoveredCard = null;
                //this.DraggedCard = null;
                this.ReturnCardToHand(card);
                this.TryToReleasePowerCardInfo(card);
                this.ArrowController.ReleaseDraggedArrow();
                this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);
                this.IsPlayingCard = false;
            }
        }
        this.HoveredCard = null;
        this.DraggedCard = null;
        this.SelectedCard = null;
    }

    private void AttachToTricksHolder(CardController card)
    {

        if (GameState != null && GameState.Turn.Phase == Phase.Combat)
        {
            // tween the card to the tricks area & parent it to that area, positioning based on its current childcount

            card.transform.AttachToParent(PlayerController.TrickHolder, 0);
            foreach (RectTransform transform in PlayerController.TrickHolder)
            {
                var index = transform.GetSiblingIndex();
                transform.DOLocalMove(new Vector3(40 * index, 90 * index, -20 * index), 0.5f);
            }
            // tween size to the new size
            card.transform.DOScale(0.8f, 0.5f);
            card.SortingOrder = 199;
        }
    }

    private void TryToReleasePowerCardInfo(CardController card)
    {
        TargetedPowerCardInfo powerCardInfo = null;
        if (this.targetedPowerCards.TryGetValue(card.CardId, out powerCardInfo))
        {
			Debug.LogFormat("#PlayerHand# found targetedPowerCardInfo for -{0}", card.CardIdAndName);
            card.CanvasGroup.alpha = 0f;
            RectTransform cardInsert = null;
            Vector3 targetPos = card.transform.position;
            if (this.hoveredCardInserts.TryGetValue(card, out cardInsert))
            {
				Debug.LogFormat("#PlayerHand# Found Hovered Card Insert {0} {1}", card.CardIdAndName, cardInsert.gameObject);
                targetPos = cardInsert.transform.position;
            }
            //UnityEditor.EditorApplication.isPaused = true;
            powerCardInfo.Card.transform.DOKill();
            this.ActivePowersLayout.ReleasePowerCard(powerCardInfo.Card);
            powerCardInfo.Card.transform.AttachToParent(this.OverlayCanvas.transform, 0);
            powerCardInfo.Card.transform.DOMove(targetPos, 200f).SetEase(Ease.OutQuad).SetSpeedBased()
            .OnComplete(() =>
            {
                if (this.targetedPowerCards.TryGetValue(card.CardId, out powerCardInfo))
                {
                    powerCardInfo.Card.transform.DOKill();
                    powerCardInfo.Card.Recycle();
                    card.CanvasGroup.alpha = 1f;

                    this.targetedPowerCards.Remove(card.CardId);
                }
            });
            powerCardInfo.Card.transform.DOScale(Vector3.one, 0.1f);
            powerCardInfo.Card.CardGlowAnimator.SetBool("IsPlayable", card.CardGlowAnimator.GetBool("IsPlayable"));
        }
        else
        {
            Debug.Log("#PlayerHand# Didn't find existing TargetedPowerCard Info");
        }
    }

    public IEnumerator FadeOutPowerCard(int cardId)
    {
        TargetedPowerCardInfo targetedCardInfo = null;
        if (this.targetedPowerCards.TryGetValue(cardId, out targetedCardInfo)
            && targetedCardInfo != null
            && targetedCardInfo.Card != null
            && targetedCardInfo.Card.transform.parent == PlayerController.TrickHolder)
        {
            var card = targetedCardInfo.Card;
            // move the card back to position so it can be shown to resolve
            card.transform.AttachToParent(PlayerController.TrickResolver, 0);
            card.transform.DOLocalMove(Vector3.zero, 0.3f).OnComplete(() => card.SortingOrder = 200);
            // scale the card back up
            card.transform.DOScale(1.4f, 0.3f);

            
            // wait a little longer
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.3f);
        targetedCardInfo = null;
        if (this.targetedPowerCards.TryGetValue(cardId, out targetedCardInfo) && targetedCardInfo != null)
        {
            yield return new WaitForSecondsRealtime(1.0f);
            //targetedCardInfo.Card.CanvasGroup.DOFade(0, 1f).SetEase(Ease.InExpo);
            AnimateCardToTrash(cardId, targetedCardInfo);
            
        }
    }

    public void AnimateCardToTrash(int cardId, TargetedPowerCardInfo targetedCardInfo)
    {
        //yield return new WaitForSecondsRealtime(0.5f);
        if (this.targetedPowerCards.TryGetValue(cardId, out targetedCardInfo) && targetedCardInfo != null)
        {
            // FAde out this target arrow instead of just releasing it
            if (targetedCardInfo.Arrow != null)
            {
                this.ArrowController.FadeOutAndReleaseTargetArrow(targetedCardInfo.Arrow);
            }

            this.ActivePowersLayout.ReleasePowerCard(targetedCardInfo.Card);

        }
        Sequence seq = DOTween.Sequence();
        seq.Append(targetedCardInfo.Card.transform.DOMove(this.ArenaView.PlayerTrashIcon.transform.position, 0.8f).SetEase(Ease.OutQuint));
        //seq.Append(card.transform.DOJump((isPlayerCard ? this.View.PlayerTrashIcon.transform.position : this.View.OpponentTrashIcon.transform.position), -200.0f,1, 0.8f).SetEase(Ease.OutQuint));
        //seq.Append(card.transform.DOPath((isPlayerCard
        //    ? new Vector3[] { this.View.PlayerTrashCardHolder.transform.position, this.View.PlayerTrashIcon.transform.position }
        //    : new Vector3[] { this.View.OpponentTrashCardHolder.transform.position, this.View.OpponentTrashIcon.transform.position }), 0.8f, pathType: PathType.CatmullRom).SetEase(Ease.OutQuint));

        seq.InsertCallback(0.2f, () => this.ArenaView.TrashCardSound.Play());
        seq.Insert(0.2f, targetedCardInfo.Card.CanvasGroup.DOFade(0.2f, 0.55f));
        seq.Insert(0.2f, targetedCardInfo.Card.transform.DOScale(0.25f, 0.5f));
        seq.AppendCallback(() =>
        {
            this.targetedPowerCards.Remove(targetedCardInfo.Card.CardId);
            targetedCardInfo.Card.transform.DOKill();
            targetedCardInfo.Card.Recycle();

            if (targetedCardInfo.HandCard != null)
            {
                targetedCardInfo.HandCard.transform.DOKill();
                targetedCardInfo.HandCard.Recycle();
            }
            this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);
        });
    }

    /// <summary>
    /// When a card is dragged out of the hand, this method attaches the insert 
    /// to the arena floor and collapses the hand.
    /// </summary>
    public void  MoveCardOutOfHand(CardController activeCard)
    {
        CardController card = activeCard;

        if (card == null)
        {
            card = this.SelectedCard;
        }

        if (card != null)
        {

			//Debug.LogFormat("#PlayerHand# MoveCardOutOFHand:{0} ID:{1}", card.TitleLabel.text, card.CardId);

            RectTransform insert = null;
            this.hoveredCardInserts.TryGetValue(card, out insert);

            if (insert == null)
            {
                this.SelectHoveredCard(card);
            }


            // If we are moving a card out of the hand as it's been unhovered during a zoom hand deactivation, we might not have an insert
            // to clean up
            if(this.hoveredCardInserts.TryGetValue(this.SelectedCard, out insert))
            {
                insert.AttachToParent(this.ArenaFloor, 0, sortingOrder: 2, overrideSorting: true);
                insert.GetComponent<CanvasGroup>().alpha = 0.25f;
                insert.GetChild(0).gameObject.SetActive(false);
                insert.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                insert.localRotation = Quaternion.identity;
            }

            this.ArenaCardZoom.HideKeywords();

            if(card.IsPower)
            {
                this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount + 1);

                /*CardFrontController [] cardFronts = this.GetComponentsInChildren<CardFrontController>();
                for(int i=0;i<cardFronts.Length;i++)
                {
                    if(cardFronts[i] != card)
                        cardFronts[i].CardGlowAnimator.SetBool("IsPlayable", false);
                }*/
            }
            /*var cardControllers = ArenaCards.AllCardControllers;
            for (int i = 0; i < cardControllers.Count(); i++)
            {
                var cardController = cardControllers[i];
                if(cardController != card)
                    cardController.ClearPlayAbilityStates();
            }*/

            bool isTargeted = ShowCastTargets();
            // Targeting Powers
            if (isTargeted)
            {
                this.ActivePowersLayout.IsDraggingTargetedPower = true;

                card.CanvasGroup.alpha = 0;
                card.CanvasGroup.blocksRaycasts = false;

                // Spawn a copy of the dragged card over the hand
                CardFrontController zoomGO = this.ArenaCards.CreateCard(((CardFrontController)card).CardState);
                //GameObject zoomGO = zoomCard.gameObject;
                zoomGO.transform.DOKill();
                zoomGO.transform.localScale = new Vector3(this.HoverScale, this.HoverScale, 1);
                zoomGO.transform.position = card.transform.position;
                zoomGO.SortingOrder = 200;

                Debug.LogFormat("zoomGo scale == {0}", this.HoverScale);
                //this.ActivePowersLayout.AddPowerCard(zoomGO.GetComponent<CardFrontController>());

                // Tween it over to the player Portrait
                // zoomGO.transform.DOLocalMove(Vector3.zero, 1200).SetEase(Ease.OutQuart).SetSpeedBased();

                zoomGO.CardGlowAnimator.SetBool("IsPowerCardFloating", true);
                ((RectTransform)zoomGO.transform).DOAnchorPosY(-10f, 0.3f) //.SetEase(Ease.InCirc)
                    .OnComplete(() => {
                        ((RectTransform)zoomGO.transform).DOAnchorPosY(125f, 0.5f).SetEase(Ease.OutQuint);
                    });
                zoomGO.transform.DOScale(new Vector3(1.2f, 1.2f, 1), 0.4f);

                TargetedPowerCardInfo existingPowerCardInfo = null;
                if (this.targetedPowerCards.TryGetValue(card.CardId, out existingPowerCardInfo) && existingPowerCardInfo != null)
                {
                    existingPowerCardInfo.Card.transform.DOKill();
                    existingPowerCardInfo.Card.Recycle();
                    this.ActivePowersLayout.ReleasePowerCard(existingPowerCardInfo.Card);
                    this.ArrowController.ReleaseDraggedArrow(existingPowerCardInfo.Arrow);

                    this.targetedPowerCards.Remove(card.CardId);
                }
                else
                {
                    Debug.LogFormat("#PlayerHand#No existing powerCardInfo for {0}", card.CardIdAndName);
                }

                // Update the view of the active powers layout
                // this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);

                // Store a reference to it (so we can destroy it later)
                TargetedPowerCardInfo powerCardInfo = new TargetedPowerCardInfo { Card = zoomGO, HandCard = card };
                zoomGO.transform.localScale = new Vector3(1.4f,1.4f, 1);
                zoomGO.CanvasGroup.alpha = 1f;
                zoomGO.RaycastTag = "Untagged";
                zoomGO.CardGlowAnimator.SetBool("IsPlayable", true);

                // Create an Arrow to drag & Anchor its source to the copy card
                powerCardInfo.Arrow = this.ArrowController.DragArrow(powerCardInfo.Card.transform,this.DragArrowsRoot);
                this.targetedPowerCards[card.CardId] = powerCardInfo;

            }
            else
            {
                // Powers don't trigger arena dragging
                if (card.CardType != CardType.Power)
                {
                    this.ArenaController.OnCardDragBegin(card);
                }

                //Debug.LogFormat("zoomGo scale == {0}", this.HoverScale);

                card.CanvasGroup.blocksRaycasts = true;
                card.RaycastTag = "Untagged";
                card.Canvas.overrideSorting = true;
                card.SortingOrder = 200;
                card.transform.DOScale(new Vector3(1.4f,1.4f,1f), 0.15f);
            }
        }
        else
        {
            Debug.LogError("#PlayerHand# Attempted to move a nulll card out of the hand");
        }
	}

    private void ClearTargets()
    {
        this.ArrowController.ClearTargets();
    }

    public bool ShowCastTargets()
    {
        // only do this if we're not waiting to play a card (if we're waiting, then selectedCard will conveniently be null)
        if (this.SelectedCard == null)
        {
            //Debug.Log("selected card is null for show cast targets");
            return false;
        }

        ClearTargets();

        bool isTargeted;
        List<int> castTargets = this.GameControllerService.GetCastTargets(this.SelectedCard.CardId, out isTargeted);
        if (castTargets != null && castTargets.Count > 0)
        {
            List<ICardState> potentialTargetCardStates = null;
            // Targeted Abilities
            //CardController sourceCard = this.ArenaCards.GetCard(sourceId);
            for (int i = 0; i < castTargets.Count; i++)
            {
                var targetId = castTargets[i];
                CardController castTargetCard = this.ArenaCards.GetCard(targetId);
                if (castTargetCard != null && castTargetCard.CardZone == CardZone.Arena)
                {
                    SetTargetGlow(castTargetCard.TargetArrowTarget, isTargeted, castTargetCard.CanvasGroup);
                }
                else
                {
                    // Is the target the opponent
                    if (targetId == this.GameState.Opponent.Id)
                    {
                        SetTargetGlow(this.ArenaPlayers.Opponent.TargetArrowTarget, isTargeted, null);
                    }
                    // is the target the player
                    else if (targetId == this.GameState.Self.Id)
                    {
                        SetTargetGlow(this.ArenaPlayers.Player.TargetArrowTarget, isTargeted, null);
                    }
					else if (targetId != this.SelectedCard.CardId) // don't show the card itself as a target - it can't be.
                    {
                        var cardState = this.GameControllerService.GetCardState(targetId);
                        if(cardState != null)
                        {
                            if (potentialTargetCardStates == null)
                                potentialTargetCardStates = new List<ICardState>();
                            potentialTargetCardStates.Add(cardState);
                            /*var newCard = castTargetCard ?? this.ArenaCards.CreateCard(cardState);
							if (castTargetCard == null)
							{
								this.ArenaCards.AddCard (newCard);
							}
                            SetTargetGlow(newCard.TargetArrowTarget, isTargeted, newCard.CanvasGroup);*/
                        }
                        // create the card, add its card state to the list of targets to show
                    }
                }
            }
            if (potentialTargetCardStates != null)
            {
                if(ShowTargets != null)
                {
                    ShowTargets.Invoke(potentialTargetCardStates, isTargeted);
                }
                // Show trash cards; highlight them
                // make targets interactible
            }
        }
        return isTargeted;
    }

    public void SetTargetGlow(TargetArrowTarget target, bool isTargeted, CanvasGroup canvasGroup)
    {
        if(target.TargetGlowAnimator != null)
        {
            target.TargetGlowAnimator.SetBool(isTargeted ? "IsCastTarget" : "IsAffected", true);
            target.TargetAction = isTargeted ? TargetAction.Attack : TargetAction.Affected;
            this.InputEventController.TargetArrowController.AddTarget(target);
        }
        if (canvasGroup != null)
            canvasGroup.interactable = true;
    }


    public void ReturnDraggedCardToHand(CardController activeCard)
    {
        CardEnters(activeCard);
        //ReturnSelectedCardToHand(activeCard);
    }

    /// <summary>
    /// When a dragged card re-enters the Hand rect, it needs to transition its insert
    /// from the arenafloor, back to the handLayout and become a "SelectedCard" instead of 
    /// a DraggedCard
    /// </summary>
    public void CardEnters(CardController activeCard)
    {
        CardController card = activeCard;

        if (card != null)
        {
            this.ArrowController.ReleaseDraggedArrow();
            ClearTargets();
            //this.HoveredCard = card;

            RectTransform cardInsert = null;
            if(this.hoveredCardInserts.TryGetValue(card, out cardInsert) == false)
            {
                Debug.Log("#PlayerHand# CardEnters but no Insert. Removed by DeactivateZoom?");
                // If this was from a LostCastAbility this cards insert might already have been lost
                cardInsert = this.insertTransforms.Dequeue();
                cardInsert.gameObject.SetActive(true);
    
                cardInsert.AttachToParent(this.transform, 0, sortingOrder: this.HandLayout.BaseIndex, overrideSorting: true, blocksRaycasts: true);
                cardInsert.position = activeCard.transform.position;
                this.hoveredCardInserts[activeCard] = cardInsert;
                this.HandLayout.CalculateSiblingIndex(cardInsert);
            }            

            Vector3 localPos = cardInsert.transform.localPosition;
            cardInsert.transform.GetChild(0).gameObject.SetActive(true);
            localPos.z = 0;
            cardInsert.transform.localPosition = localPos;
            cardInsert.AttachToParent(this.transform, 0, sortingOrder: 150, overrideSorting: true);
            cardInsert.GetComponent<CanvasGroup>().alpha = 1.0f;

            // Position the card insert under the card
            Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(this.uiCamera, card.transform.position);
            Vector2 insertLocalPos = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)cardInsert.transform.parent, screenPos, this.uiCamera, out insertLocalPos);
            cardInsert.transform.localPosition = insertLocalPos;

            // Calculate and apply sibling index
            this.HandLayout.CalculateSiblingIndex(cardInsert);

            this.ActivePowersLayout.IsDraggingTargetedPower = false;

            // Position insert directly in the correct position
            cardInsert.localPosition = this.HandLayout.GetChildPosition(cardInsert);

            TargetedPowerCardInfo powerCardInfo = null;
           
            this.targetedPowerCards.TryGetValue(activeCard.CardId, out powerCardInfo);

            /*var cardFronts = ArenaCards.AllCardControllers;
            //CardFrontController [] cardFronts = this.GetComponentsInChildren<CardFrontController>();
            for(int i=0;i<cardFronts.Count;i++)
            {
                cardFronts[i].RefreshLegalAbilities();
            }*/

            //Debug.LogFormat("Card returned. IsZoomedIn: {0} IsZoomingIn: {1}, MustZoomIn: {2}", this.IsHandZoomedIn, this.IsZoomingIn,  this.MustSetZoomHandActiveTo);

            if (powerCardInfo != null)
            {
                powerCardInfo.Card.transform.DOKill();
                this.ActivePowersLayout.ReleasePowerCard(powerCardInfo.Card);
                powerCardInfo.Card.transform.AttachToParent(this.OverlayCanvas.transform,0);
                powerCardInfo.Card.transform.DOScale(this.IsHandZoomedIn || this.MustSetZoomHandActiveTo == true ? new Vector3(1.3f,1.3f,1f) : Vector3.one, 0.1f);

                // when a targeted power is dragged out of the window.. you want the power card to tween over to the hand
                Vector3 targetPos = cardInsert.transform.position;

                if(RectTransformUtility.RectangleContainsScreenPoint((RectTransform)this.transform, Input.mousePosition, this.uiCamera))
                {
                    // When dragging a targeted power back into the hand you want the power card to zoom over to the card
                    targetPos = card.transform.position;
                }

                powerCardInfo.Card.transform.DOMove(targetPos, 200f).SetEase(Ease.OutQuad).SetSpeedBased()
                .OnComplete(() =>
                {
                    if (this.targetedPowerCards.TryGetValue(card.CardId, out powerCardInfo) && powerCardInfo != null)
                    {
                        powerCardInfo.Card.transform.DOKill();
                        powerCardInfo.Card.Recycle();
                        card.CanvasGroup.alpha = 1f;
                        card.CanvasGroup.blocksRaycasts = true;
                        card.RaycastTag = "card";
                        this.targetedPowerCards.Remove(card.CardId);
                    }
                });
                powerCardInfo.Card.CardGlowAnimator.SetBool("IsPlayable", card.CardGlowAnimator.GetBool("IsPlayable"));
            }
            else
            {
				//Debug.LogFormat("#PlayerHand# setting card alpah to 1 {0}", card.TitleLabel.text);
                card.CanvasGroup.alpha = 1f;
            }

            if(card.IsPower)
            {
                card.transform.DOScale(this.IsHandZoomedIn || this.MustSetZoomHandActiveTo == true ? new Vector3(1.3f, 1.3f, 1f) : Vector3.one, 0.1f);
            }

            this.HandLayout.UpdateCardTweens();
            this.ArenaController.OnCardDragEnd();
        }
    }

    /// <summary>
    /// Makes the currently hovered card, selected
    /// </summary>
    public void SelectHoveredCard(CardController activeCard)
    {
        Debug.LogFormat("Selecting hovered card. Card is {0}", activeCard == null ? "null" : activeCard.CardIdAndName);
        this.SelectedCard = activeCard;
        this.ArenaCardZoom.HideKeywords();
        this.scaledCard = null;

        // If we are selecting a card as part of moving it out of the hand from a "Drag" operation, we might not have an insert
        // if the hand was recently UnhoveredAll by a zoom hand deactivation.
        RectTransform insert = null;
        if( this.hoveredCardInserts.TryGetValue(activeCard, out insert))
        {
            insert.localRotation = Quaternion.identity;
            insert.GetComponent<Canvas>().sortingOrder = 150;
        }

        ((CardFrontController)this.SelectedCard).OutlineCanvasGroup.alpha = 0f;

        // this.hoveredCard might have been zeroe'd out by UnHovereAll() so we use the card reference
        // passed in by the InputEventController
        activeCard.transform.DOKill();
        activeCard.transform.DOScale(this.IsHandZoomedIn ? new Vector3(1.3f, 1.3f, 1f) : Vector3.one, 0.2f).SetEase(Ease.OutQuint);

        this.DraggedCard = activeCard;

		/*if(this.IsUsingZoomHand)// && IsHandZoomedIn)
        {
            this.DeactivateZoomedHand();
        }*/
    }

    /// <summary>
    /// Workhorse method for taking a card that's been moused over, adding a cardInsert (indicator) at
    /// the cards position, and then popping up the card above the insert (and attached to an overlay Canvas)
    /// </summary>
    public void OnCardHovered(CardController activeCard)
    {
        if(activeCard == HoveredCard)
        {
            Debug.Log("Active and hovered cards are the same");
            //return;
        }
        bool wasHovered = this.HoveredCard != null;

        if (wasHovered)
        {
            // Return the previously hovered card to it's default state
            this.ReturnHoveredCardToHand(this.HoveredCard);

            float timeSinceHover = Time.unscaledTime - this.lastHoverTime;
            if (timeSinceHover < 0.02f || timeSinceHover > 0.5f)
            {
                wasHovered = false;
            }
        }

#if UNITY_IOS || UNITY_ANDROID

#elif true
        // show the keywords tooltips via the ZoomController (only for standalone (mouse) builds)
        Vector2 point = RectTransformUtility.PixelAdjustPoint(activeCard.transform.position,this.OverlayCanvas.transform, this.OverlayCanvas);
        bool isKeywordFloatedUp = (this.IsZoomingIn && this.IsUsingZoomHand && point.x > 16f) || point.x > 20f;
        //Debug.LogFormat("ThePoint: {0}, {1} IsZoomingIn:{2} isKeywordFloatedUp:{3}", point.x, point.y, this.IsZoomingIn, isKeywordFloatedUp);
        this.ArenaCardZoom.TriggerKeywordsPopup(((CardFrontController)activeCard).CardState,isKeywordFloatedUp);
#endif

		this.lastHoverTime = Time.unscaledTime;
		//Debug.LogFormat ("card hovered {0} at {1}", activeCard.name, this.lastHoverTime);

        activeCard.transform.DOKill();

        // Do we already have an insert for this card? Was it recently deslected?
        RectTransform insert = null;
        if(this.hoveredCardInserts.TryGetValue(activeCard, out insert) == false)
        {
            insert = this.insertTransforms.Dequeue();
            insert.gameObject.SetActive(true);
            int siblingIndex = activeCard.transform.GetSiblingIndex();
            insert.AttachToParent(this.transform, siblingIndex, sortingOrder: 140 /*this.HandLayout.BaseIndex + siblingIndex * 3*/, overrideSorting: true, blocksRaycasts: true);
            Vector3 localPos = insert.localPosition; localPos.z = 0; insert.localPosition = localPos;
            this.hoveredCardInserts[activeCard] = insert;
        }
        else
        {
            Debug.LogWarningFormat("Insert not found for {0}", activeCard.CardIdAndName);
        }
        
        insert.GetChild(0).gameObject.SetActive(true);
        insert.GetComponent<CanvasGroup>().alpha = 1.0f;
        insert.localRotation = Quaternion.identity;

		this.HoveredCard = activeCard;

        activeCard.RaycastTag = "Untagged";
        activeCard.transform.AttachToParent(this.OverlayCanvas.transform, 0, sortingOrder: 600, overrideSorting: true, blocksRaycasts: false);
        activeCard.transform.SetAsLastSibling();
        insert.localPosition = this.HandLayout.GetChildPosition(insert);
        insert.Rotate(0f, 0f, this.HandLayout.GetChildZRotation(insert));

		activeCard.transform.localRotation = Quaternion.identity;
        activeCard.transform.localScale = new Vector3(this.HoverScale, this.HoverScale, 1);


        // Card Transform
        RectTransform rectT = (RectTransform)activeCard.transform;
        /*Vector3 anchor = rectT.anchoredPosition3D;
        anchor.y = this.HoverDistanceY;
        rectT.anchoredPosition3D = anchor;*/

        // Card Tween
        rectT.DOAnchorPosY(this.HoverDistanceY + 5, 0.1f).SetEase(Ease.OutExpo);
		//if(this.IsUsingZoomHand)
		//	rectT.DOScale (rectT.localScale*1.7f, 0.2f);
		//UnityEditor.EditorApplication.isPaused = true;
    }

    /// <summary>
    /// Callback from CardDealer, an animation event on the DealCard animation
    /// triggers this callback, so we can control percisely in the animation timeline
    /// when the hand opens to accept the newly dealt card.
    /// </summary>
    private void OnAddInsert(RectTransform insertTransform)
    {
        // Move the insert to the hand
        insertTransform.gameObject.SetActive(true);
        insertTransform.GetChild(0).gameObject.SetActive(false); // Hide the insert art
        insertTransform.SetParent(this.transform);
        insertTransform.GetComponent<CanvasGroup>().alpha = 1.0f;
        insertTransform.localScale = Vector3.one;

        // New Cards Always come in on the right
        insertTransform.SetSiblingIndex(this.transform.childCount);

        // Position the insert in the hand
        insertTransform.localPosition = this.HandLayout.GetChildPosition(insertTransform);
        insertTransform.Rotate(0f, 0f, this.HandLayout.GetChildZRotation(insertTransform));

        // Make it active
        insertTransform.gameObject.SetActive(true);

        // If it has a canvas, set the sort order appropriately
        Canvas insertCanvas = insertTransform.GetComponent<Canvas>();
        if (insertCanvas != null)
        {
            insertCanvas.sortingOrder = insertTransform.GetSiblingIndex() + 1;
        }

        // Calculate card positions and tween to new positions if there are any
        this.HandLayout.UpdateCardTweens();
    }

    /// <summary>
    /// Callback from CardDealer, an animation event on the DealCard animation
    /// triggers this callback, handing us a reference to the spawned card
    /// and allowing us to set up a tween to take it to the hand.
    /// </summary>
    private void OnCardDealt(CardController cardController, RectTransform insertTransform)
    {
        // Handle a card that's already existing

        // Strip off any pending aniamtions (and possibly some disasterous OnComplete animation handlers)
        cardController.transform.DOKill();
        CardFrontController front = cardController as CardFrontController;
        if (front != null)
        {
            if (front.AttackSequence != null)
            {
                Debug.Log("#PlayerHand# Killing attack sequence");
                front.AttackSequence.Kill();
            }
            front.PlayerArenaController = null;
            front.RaycastTag = "card";
            front.CurrentDamage = 0;
            front.UpdateLabels();
        }

        // Make the card a child of its insert and make it sort above the other cards in the hand
        Vector3 localScale = cardController.transform.localScale;
        cardController.transform.AttachToParent(insertTransform, 0, sortingOrder: this.transform.childCount + 300);
        cardController.transform.localScale = localScale;
        cardController.gameObject.name = "CardCanvas " + this.transform.childCount;
        cardController.CanvasGroup.alpha = 1.0f;
        cardController.Canvas.sortingLayerName = "Default";
        cardController.CardGlowAnimator.SetBool("IsPowerCardFloating", false);

        // ...or the hand is currently "flat" with no overlapping cards
        if (this.transform.childCount >= this.HandLayout.MaxCardsInFlatHand)
        {
            cardController.transform.DORotateQuaternion(insertTransform.rotation, 0.5f).SetEase(Ease.OutSine);
        }

        // Make the card display its possible actions (IsPlayable?)
        cardController.DisplayLegalAbilities(this.GameState.LegalAbilities);

        // ============================
        // Deal Card to Hand Tween
        // ============================
        cardController.transform.DOLocalMove(Vector3.zero, 2000f).SetSpeedBased()
        .SetEase(Ease.InQuad)
        .OnComplete(
            () =>
            {
                // Make the card a child of Hand layout, have it block raycasts and have a proper sort order
                cardController.transform.AttachToParent(this.transform, insertTransform.GetSiblingIndex() + 1, sortingOrder: insertTransform.GetSiblingIndex(), blocksRaycasts: true);

                // Deactivate the insert transform and return it to the queue
                insertTransform.gameObject.SetActive(false);
                insertTransform.SetParent(this.insertsRoot);
                this.insertTransforms.Enqueue(insertTransform);
                this.HandLayout.UpdateCardTweens();
                this.incommingCards.Remove(cardController);

            });
        this.DrawCardSound.Play();
    }

    public void DealCardFromDeck(ICardState cardState)
    {
        RectTransform currentInsert = null;
        if (this.insertTransforms.Count == 0)
        {
            throw new System.Exception("Can't queue up any more cards to deal");
        }
        else
        {
            // Get an insert transform to use with this card deal aniamtion
            currentInsert = this.insertTransforms.Dequeue();
            CardController arenaCard = this.ArenaCards.GetCard(cardState.Id);
            arenaCard.CanvasGroup.alpha = 0;
            //Debug.Log("#PlayerHand# ticksRemaining on deal = " + this.GameState.Turn.TicksRemaining + " phase:" + this.GameState.Turn.Phase);
            // The first deal of a turn comes at 15 ticks remaining so we'll have a slow deal for anything > 14 ticks in the action Phase -DMac
            bool isFastDeal = this.GameState.Turn.Phase != Phase.PreAction;
            //Debug.Log("#PlayerHand# isFastDeal=" + isFastDeal );
            this.CardDealer.DealCard(() => OnAddInsert(currentInsert), (c) => OnCardDealt(c, currentInsert), arenaCard, isFastDeal);
            //Debug.Log("#PlayerHand# Requested deal from the card dealer");
            this.incommingCards.Add(arenaCard);
        }
    }

    Sequence zoomSequence = null;
    public void ActivateZoomedHand(bool isInstant=false)
    {
        // Early out if we're already zooming in
        if(this.IsZoomingIn) 
		{
			Debug.LogFormat("NOT Activating ZoomHand - Zooming In");
			return; 
		}

        // 165px is the amount of offset to have in the wide arena with the default Y resolution of 1080. 
        // with a free aspect screen it's possible to show more of the vertical space in the players hand so in that case we need to move the zoom hand up less.
        // The following calcuation determins how many extra pixels have been added to the players hand and subtracts that amount from the basic 165px movement
        // This keeps the zoom hand moving up so that the bottom of the cards are fairly flush with the bottom of the screen -Dmac
        float anchorOffset = 165f + (canvasScaler.referenceResolution.y - ((RectTransform)this.sessionContextView.MainCanvas.transform).sizeDelta.y) * 0.5f;
        
        this.IsHandZoomedIn = true;
        RectTransform rectTrans = (RectTransform)this.transform;

        if(isInstant == false)
        {
            this.zoomSequence = DOTween.Sequence();
            this.zoomSequence.Append(rectTrans.DOAnchorPosY(anchorOffset, 0.2f));
            this.zoomSequence.Insert(0f, rectTrans.DOScale(new Vector3(1.3f,1.3f,1f), 0.2f));
            this.zoomSequence.OnComplete(() => zoomSequence = null);
			//this.zoomSequence.AppendInterval (0.3f);
            //this.zoomSequence.Play();
        }
        else
        {
            Vector2 anchorPos =rectTrans.anchoredPosition;
            anchorPos.y = anchorOffset;
            rectTrans.localScale = new Vector3(1.3f, 1.3f,1f);
            rectTrans.anchoredPosition = anchorPos;
        }
		Debug.LogFormat("Activating ZoomHand - Offset:{0}. ZoomingOut:{1} IsZoomedIn:{2}", anchorOffset, this.IsZoomingOut, this.IsHandZoomedIn);
    }

    Sequence returnSequence = null;
    public void DeactivateZoomedHand()
    {
		if(this.IsZoomingOut/* || this.IsZoomingIn || !this.IsHandZoomedIn*/)
		{ 
			//Debug.LogFormat("NOT Deactivating ZoomHand - Zooming Out");
			return;
		}

		//Debug.LogFormat("DeActivating ZoomHand - NumDeactivations:{0}. ZoomingIn:{1} IsZoomedIn:{2}", this.NumDeactivations, this.IsZoomingIn, this.IsHandZoomedIn);
        this.IsHandZoomedIn = false;
        this.NumDeactivations++;

        if(this.IsZoomingIn && this.zoomSequence != null)
        {
            this.zoomSequence.Kill();
            this.zoomSequence = null;
        }

        RectTransform rectTrans = (RectTransform)this.transform;
        this.returnSequence = DOTween.Sequence();
        this.returnSequence.Append(rectTrans.DOAnchorPosY(-51.2f, 0.2f));
        this.returnSequence.Insert(0f, rectTrans.DOScale(Vector3.one, 0.2f));
		this.returnSequence.InsertCallback(0.01f, ()=>UnHoverAllCards());
        this.returnSequence.OnComplete(() => returnSequence = null);
        //this.zoomSequence.AppendInterval (0.3f);
        //this.returnSequence.Play();
    }

    List<CardController> cardsToUnhover = new List<CardController>();
    private void UnHoverAllCards()
    {
        foreach(CardController cardController in this.hoveredCardInserts.Keys)
        {
            if(cardController != this.DraggedCard )
            {
                this.cardsToUnhover.Add(cardController);
            }
        }
        for(int i=0;i<this.cardsToUnhover.Count;i++)
        {
            this.ReturnCardToHand(this.cardsToUnhover[i]);
        }
        this.cardsToUnhover.Clear();
        if(this.currentState == HandState.CardHovered)
        {
            this.currentState = HandState.DefaultState;
		}
		this.HoveredCard = null;
    }


    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (this.GameState != null && this.GameState.Turn != null && this.GameState.Turn.Phase == Phase.Action && this.GameState.Turn.TicksRemaining <= 0)
        {
            if (this.DraggedCard != null)
            {
                this.TriggerInput(InputEvent.LostCastAbility, this.DraggedCard);
                Debug.Log("#PlayerHand# lost cast ability due to running otu of time");
            }
        }

        // If there's a seletedInsert, position the hovered card over it
        if (this.SelectedCard != null)
        {
            RectTransform selectedInsert = null;

            if (this.hoveredCardInserts.TryGetValue(this.SelectedCard, out selectedInsert))
            {
                // Find out what position this insert should be at
                this.HandLayout.CalculateSiblingIndex(selectedInsert);
                this.HandLayout.UpdateCardTweens();
                CardController selectedCard = this.hoveredCardInserts[selectedInsert];

                Vector2 localPoint;
                RectTransform parent = (RectTransform)selectedInsert.parent;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(parent, Input.mousePosition, this.uiCamera, out localPoint);
                // Position the selected insert using the mouse pos
                selectedInsert.localPosition = localPoint;
                // Then calculate (and apply) its sibling index based on the mouse position
                this.HandLayout.CalculateSiblingIndex(selectedInsert);
                // Then Position the selected insert so it's in the exact correct position in the hand
                selectedInsert.localPosition = this.HandLayout.GetChildPosition(selectedInsert);

                // Position the selected card in the Overlay using the mouse pos
                // With an offset so it's more readable
                RectTransform overlayTransform = (RectTransform)this.OverlayCanvas.transform;
                Vector2 localCardPoint;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(overlayTransform, Input.mousePosition, this.uiCamera, out localCardPoint);
                localCardPoint.y += 321f * 0.5f; // (half card height)
                selectedCard.transform.localPosition = localCardPoint;

                this.ConstrainCardToBounds(selectedCard, (RectTransform)selectedCard.Card.transform);
            }
        }

#if UNITY_IOS || UNITY_ANDROID
        else
        {
            // ==============================
            // Zoom hovered hand cards
            // ==============================
			if(this._hoveredCard != null && Input.GetMouseButton(0))
            {
                ((CardFrontController)this._hoveredCard).OutlineCanvasGroup.alpha = 1.0f;

                if(this.scaledCard != this._hoveredCard && DOTween.IsTweening(this._hoveredCard.transform) == false )
                {
                    this.scaledCard = this._hoveredCard;
                    this._hoveredCard.transform.DOLocalMoveY(this._hoveredCard.transform.localPosition.y + 275f, 0.1f).SetEase(Ease.OutExpo);
                    this._hoveredCard.transform.DOScale(this._hoveredCard.transform.localScale*1.7f, 0.1f).SetEase(Ease.OutExpo)
					//this.hoveredCard.transform.DOLocalMoveY(0.0f,0.5f)
                        .OnComplete(()=>this.ArenaCardZoom.ShowCardKeywords((CardFrontController)this._hoveredCard));
                }
            }
        }
#endif

        // =============================
        // State Transition Pump
        // =============================
        if (this.inputEvents.Count > 0)
        {
            // Perform State Transitions
            foreach (InputEvent inputEvent in this.inputEvents.Keys)
            {
                bool isEventSet = false;
                if (this.inputEvents.TryGetValue(inputEvent, out isEventSet))
                {
                    if (isEventSet)
                    {
                        CardController activeCard = null;
                        this.inputCards.TryGetValue(inputEvent, out activeCard);
                        this.currentState = this.DoStateTransition(this.currentState, inputEvent, activeCard);
                    }
                }
				else{
					Debug.LogFormat ("Missing event for {0}. currentState: {1}", inputEvent, currentState);
				}
            }
            this.inputEvents.Clear();
        }

        // Make cards that are above inserts be exactly above them.
        for (int i = 0; i < this.HandLayout.transform.childCount; i++)
        {
            Transform child = this.HandLayout.transform.GetChild(i);
            RectTransform selectedInsert = null;

            if (this.SelectedCard != null)
            {
                this.hoveredCardInserts.TryGetValue(this.SelectedCard, out selectedInsert);
            }
            CardController cardController;
            if (this.hoveredCardInserts.TryGetValue((RectTransform)child, out cardController) && child != selectedInsert && this.incommingCards.Contains(cardController) == false)
            {
                // Only position cards above their inserts (like cards returning to the hand)
                // Dont position cards that ar being banished from the arena though, they have their own tweens (incommingCards)
                // when the card is NOT the currently selected/dragged card
                this.UpdateCardXToMatchInsertX(cardController, child);
            }
        }

		if(IsUsingZoomHand)
		{
			if(MustSetZoomHandActiveTo == true)
			{
				if(!IsZoomingIn && !IsHandZoomedIn)// && !IsZoomingOut)
					this.ActivateZoomedHand ();
				MustSetZoomHandActiveTo = null;
			}
			else if(MustSetZoomHandActiveTo == false)
			{
				if(!IsZoomingOut && IsHandZoomedIn)// && !IsZoomingIn)
					this.DeactivateZoomedHand ();
				MustSetZoomHandActiveTo = null;
			}
		}
    }

    /// <summary>
    /// Positions the card to align its center above the center of the insert. 
    /// Does not ajust the y position of the card.
    /// </summary>
    void UpdateCardXToMatchInsertX(CardController card, Transform insert)
    {
        if (insert.parent == this.transform && card.RaycastTag != "returning")
        {
            Vector2 localPoint;
            Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(this.uiCamera, insert.position);
            RectTransform overlayTransform = (RectTransform)this.OverlayCanvas.transform;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(overlayTransform, screenPoint, this.uiCamera, out localPoint);
            Vector3 localPos = card.transform.localPosition;
            localPos.x = localPoint.x;
            card.transform.localPosition = localPos;
        }
    }

    /// <summary>
    /// Utility function to queue up an InputEvent
    /// </summary>
    public void TriggerInput(InputEvent eventType, CardController cardController)
    {
        this.inputEvents[eventType] = true;
        this.inputCards[eventType] = cardController;
    }

    /// <summary>
    /// Handles moving from one state to another based on an InputEvent and a reference card.
    /// See the state transitions list set up in Start() for details
    /// </summary>
    private HandState DoStateTransition(HandState currentState, InputEvent inputEvent, CardController activeCard)
    {
        Transition currentTransition = this.transitions.Where(s => s.FromState == currentState && s.InputEvent == inputEvent).FirstOrDefault();
        if (currentTransition != null)
        {
            Debug.LogFormat("#HandTransition# Transitioning {0}->({1})->{2} ({3})", currentState, currentTransition.InputEvent, currentTransition.ToState, activeCard == null ? "[activeCard is null]" : activeCard.CardIdAndName);
            /*if (currentTransition.ToState == HandState.CardHovered || currentTransition.ToState == HandState.CardSelected)
            {
				Debug.LogFormat("#HandTransition# Transitioning {0}->({1})->{2} ({3})", currentState, currentTransition.InputEvent, currentTransition.ToState, activeCard.CardIdAndName);
            }
            else
            {
			    Debug.LogFormat("#HandTransition# Transitioning {0}->({1})->{2})", currentState, currentTransition.InputEvent, currentTransition.ToState);
            }*/
            if (currentTransition.TransitionFunction != null)
            {
                currentTransition.TransitionFunction.Invoke(activeCard);
            }
            currentState = currentTransition.ToState;
        }
        return currentState;
	}

	/// <summary>
	/// One of the more complex state transitions, performs slightly different
	/// operations based on weather the activeCard is the selected card or not.
	/// In general, this method tweens the activeCard back to it's insert in the hand
	/// </summary>
	public void ReturnCardToHand(CardController activeCard)
	{
		CardFrontController cf = (CardFrontController)activeCard;
        //Debug.LogFormat("#PlayerHand# ReturnCardToHand:{0}", (activeCard != null ? cf.TitleLabel.text : "null"));
        
        if (activeCard != null)
		{
			// Somtimes it's possible for us to go from a dragged state to an unselected state
			// (if for examlpe the MouseUpHand happens before the MouseEntersHand event)
			if (activeCard == this.DraggedCard)
			{
				this.ArenaController.OnCardDragEnd();
			}

			if(activeCard.IsPower && this.CastRequiresTarget(activeCard) && activeCard == this.SelectedCard)
			{
				this.CardEnters(activeCard);
			}
            
            ClearTargets();

            // Normal state
            this.ReturnHoveredCardToHand(activeCard);
            if (HoveredCard != null && HoveredCard != SelectedCard && HoveredCard != DraggedCard)
            {
                Debug.LogFormat("another card to return: {0}", HoveredCard.CardIdAndName);
                this.ReturnHoveredCardToHand(HoveredCard);
            }
        }
		else
		{
			cf = (CardFrontController)this.HoveredCard;
			this.ReturnHoveredCardToHand(this.HoveredCard);
        }
        this.HoveredCard = null;

        /*var cardControllers = ArenaCards.AllCardControllers;
        for (int i = 0; i < cardControllers.Count(); i++)
        {
            cardControllers[i].RefreshLegalAbilities();
        }*/
        /*
        CardFrontController[] cardFronts = this.GetComponentsInChildren<CardFrontController>();
        for (int i = 0; i < cardFronts.Length; i++)
        {
            cardFronts[i].RefreshLegalAbilities();
        }*/

        this.scaledCard = null;
	}

	/// <summary>
	/// Retun a dragged card to the hand when the mouse is dragged out of the window
	/// </summary>
	private void CardOutOfWindow(CardController activeCard)
	{
		CardController card = activeCard;
		if (this.DraggedCard != null) { card = this.DraggedCard; }
		TargetedPowerCardInfo powerCardInfo = null;
		if(this.targetedPowerCards.TryGetValue(card.CardId, out powerCardInfo))
		{
			card.CanvasGroup.alpha = 0;
		}
		else
		{
			card.CanvasGroup.alpha = 1;
		}
		this.CardEnters(card);
		this.ReturnSelectedCardToDefault(card);

		this.DraggedCard = null;
	}

	/// <summary>
	/// Returns the selected/dragged card to its position in the hand.
	/// </summary>
	public void ReturnSelectedCardToHand(CardController card)
	{
        CardController theCard = this.SelectedCard ?? card;

		// If we're returning a card to the hand, but the hand has recently been deactivated
		// causing the forced UnHoverAll() method, there might not be an insert for the selected card
		RectTransform insert = null;
		if(this.hoveredCardInserts.TryGetValue(theCard, out insert))
		{
			this.HandLayout.CalculateSiblingIndex(insert);
		}
		else
		{
            insert = GetAndAttachInsert(theCard);
            this.HandLayout.CalculateSiblingIndex(insert);
            Debug.LogFormat("#PlayerHand# ReturnSelectedCardTohand ({0}) coundn't find an insert ", theCard.CardIdAndName);
		}

		//Vector2 point = RectTransformUtility.PixelAdjustPoint(theCard.transform.position, this.OverlayCanvas.transform, this.OverlayCanvas);
		//bool isKeywordFloatedUp = (this.IsZoomingIn && this.IsUsingZoomHand && point.x > 16f) || point.x > 20f;
		this.HandLayout.UpdateCardTweens();

		this.DraggedCard = null;

		/*RectTransform rectT = (RectTransform)(theCard).transform;
		Vector3 anchor = rectT.anchoredPosition3D;
		anchor.y = this.HoverDistanceY;
		rectT.anchoredPosition = anchor;
		theCard.transform.localScale = new Vector3(this.HoverScale, this.HoverScale, 1f);*/
		theCard.RaycastTag = "card";

		this.SelectedCard = null;
	}

	public void ReturnSelectedCardToDefault(CardController card)
	{
		this.ReturnSelectedCardToHand(card);
        //this.ReturnCardToHand(card);
        this.ReturnHoveredCardToHand(card);
        //this.UnHoverAllCards ();
        this.HoveredCard = null;
		this.scaledCard = null;	
		//this.HandLayout.UpdateCardTweens();
	}

	public void ReturnDraggedCardToDefault(CardController card)
	{

		ReturnCardToHand(card);
		/*#if UNITY_ANDROID || UNITY_IOS
		ReturnSelectedCardToDefault(card);
#else
        ReturnCardToHand(card);
		#endif*/
	}

    /// <summary>
    /// Returns a hovered card to its insert.
    /// </summary>
	private void ReturnHoveredCardToHand(CardController hovCard)
    {
        // In some extreme cases with the zoom hand zoomin in and out with tweens in various states you can have a null hovCArd
        if(hovCard == null) return;

        // hide any shown targets - if they're shown, they're shown because this is hovered
        /*if (this.HideTargets != null)
        {
            HideTargets.Invoke();
        }*/

        // Get references to the hovered Card's canvas, rectTransform and insert
        RectTransform cardTrans = (RectTransform)hovCard.transform;

        // Make sure the hovered card hasn't grown in size
        cardTrans.sizeDelta = Vector2.zero;

        RectTransform cardInsert = null;
        if (this.hoveredCardInserts.TryGetValue(hovCard, out cardInsert) == false)
        {
            return;
        }
        
        if (hovCard == this.HoveredCard)
		{
			this.HoveredCard = null;
		}

        //this.ArenaCardZoom.HideKeywords();

        CanvasGroup insertCanvasGroup = cardInsert.GetComponent<CanvasGroup>();
        insertCanvasGroup.alpha = 0;
        var hovFront = ((CardFrontController)hovCard);
        if(hovFront != null)
            hovFront.OutlineCanvasGroup.alpha = 0f;

        // Kill any existing DO tweens on the hovered card
        cardTrans.DOKill();
        cardTrans.localScale = Vector3.one;

        // if the hand is zooming out have the hovered card snap to the insert position rather than tween to it
        if(this.IsUsingZoomHand && this.IsZoomingOut)
        {
            cardTrans.AttachToParent(this.transform, cardInsert.GetSiblingIndex(), blocksRaycasts: true);
            cardTrans.position = cardInsert.position;
            cardInsert.gameObject.SetActive(false);
            this.hoveredCardInserts.Remove(hovCard);
            this.insertTransforms.Enqueue(cardInsert);
            cardInsert.SetParent(this.insertsRoot);
            insertCanvasGroup.alpha = 1;
            hovCard.RaycastTag = "card";
            this.HandLayout.UpdateCardTweens();
        }
        else
        {
            //Debug.LogFormat("CardTrans Position:{0},{1},{2} insert:{3},{4},{5}", cardTrans.position.x, cardTrans.position.y, cardTrans.position.z, cardInsert.position.x, cardInsert.position.y, cardInsert.position.z);
            // Position the card directly above the insert in world coordinates (ignoring rotation)     
            //cardTrans.position = cardInsert.position + new Vector3(0f, 8f * arenaScale - this.HandLayout.GetVerticalOffset(cardInsert.GetSiblingIndex()) * (0.3f * arenaScale), 0f);
            
            // Tween the hovered card's rotation so taht it again matches the hand rotation
            cardTrans.rotation = cardInsert.localRotation;

            // Set the sortingOrder for the card insert
            Canvas insertCanvas = cardInsert.GetComponent<Canvas>();

            hovCard.RaycastTag = "returning";

            Vector3 handCardPos = Vector3.zero;
            TargetedPowerCardInfo tpci;
            if(this.targetedPowerCards.TryGetValue(hovCard.CardId, out tpci))
            {
                cardTrans.position = tpci.Card.transform.position;
                cardInsert.position = tpci.Card.transform.position;
                if(tpci.HandCard != null)
                    handCardPos = tpci.HandCard.transform.position;
            }


            if(this.IsUsingZoomHand && this.NumHoveredCards == 1 && this.transform.childCount == 1)
            {
				this.MustSetZoomHandActiveTo = false;
                int siblingIndex = cardInsert.GetSiblingIndex();
                cardTrans.AttachToParent(this.transform, siblingIndex, this.HandLayout.BaseIndex + siblingIndex * 3, blocksRaycasts: true);
                cardTrans.position = cardInsert.position;
                cardInsert.gameObject.SetActive(false);
                this.hoveredCardInserts.Remove(hovCard);
                this.insertTransforms.Enqueue(cardInsert);
                cardInsert.SetParent(this.insertsRoot);
                insertCanvasGroup.alpha = 1;
                hovCard.RaycastTag = "card";
                this.HandLayout.UpdateCardTweens();

            }
            else
            {
                // Move the hovered card toward its insert in the hand
                cardTrans.DOMove(cardInsert.position, 0.15f)
                    .OnUpdate(
                        () =>
                        {
                            //Vector3 pos = cardTrans.position;
                            //pos.x = cardInsert.position.x;
                            //cardTrans.position = pos;
                            hovCard.SortingOrder = insertCanvas.sortingOrder + 1;
                            if (this.IsUsingZoomHand)
                            {
                                cardTrans.localScale = this.transform.localScale;
                            }
                        })
                    .OnComplete(
                        () =>
                        {
                            int siblingIndex = 0;
                            if(tpci != null && tpci.Arrow != null)
                            {
                                var rectT = this.transform as RectTransform;
                                // need to put the card back in the right spot, based on where the mouse is (or where the card is??)
                                var screenPoint = RectTransformUtility.WorldToScreenPoint(this.uiCamera, handCardPos);
                                Vector2 newLocalPoint;
                                RectTransformUtility.ScreenPointToLocalPointInRectangle(rectT, screenPoint, uiCamera, out newLocalPoint);
                                siblingIndex = (int)Math.Floor((this.transform.childCount+1) * (rectT.sizeDelta.x/2 + newLocalPoint.x)/rectT.sizeDelta.x);
                                siblingIndex = Math.Max(0, siblingIndex);
                                //Debug.LogFormat("newLocalPoint:{0} width:{1} childCount:{2} siblingIndex:{3}", 
                                //    newLocalPoint.ToString(), rectT.sizeDelta.x, this.transform.childCount, siblingIndex);
                            }
                            else
                            {
                                siblingIndex = cardInsert.GetSiblingIndex();
                            }
                            cardTrans.AttachToParent(this.transform, siblingIndex, blocksRaycasts: true);
                            cardInsert.gameObject.SetActive(false);
                            this.hoveredCardInserts.Remove(hovCard);
                            this.insertTransforms.Enqueue(cardInsert);
                            cardInsert.SetParent(this.insertsRoot);
                            insertCanvasGroup.alpha = 1;
                            hovCard.RaycastTag = "card";
                            this.HandLayout.UpdateCardTweens();
                        });
            }
        }
    }
}

public class BiDictionary<T, K>
{
    private readonly Dictionary<T, K> _first;
    private readonly Dictionary<K, T> _second;

    public BiDictionary()
    {
        _first = new Dictionary<T, K>();
        _second = new Dictionary<K, T>();
    }

    public IEnumerable<T> Keys
    {
        get { return this._first.Keys; }
    }

    public IEnumerable<K> Values
    {
        get { return this._first.Values; }
    }

    public int Count
    {
        get { return this._first.Count;}
    }

    public void Clear()
    {
        this._first.Clear();
        this._second.Clear();
    }

    public void Add(T first, K second)
    {
        _first[first] = second;
        _second[second] = first;
    }

    public bool TryGetValue(T keyValue, out K result)
    {
        return _first.TryGetValue(keyValue, out result);
    }

    public bool TryGetValue(K keyValue, out T result)
    {
        return _second.TryGetValue(keyValue, out result);
    }

    public void Remove(T keyValue)
    {
        var obj = _first[keyValue];
        _first.Remove(keyValue);
        _second.Remove(obj);
    }

    public void Remove(K keyValue)
    {
        var obj = _second[keyValue];
        _second.Remove(keyValue);
        _first.Remove(obj);
    }

    public K this[T keyValue]
    {
        get
        {
            if (_first.ContainsKey(keyValue))
            {
                return _first[keyValue];
            }

            throw new System.ArgumentException("Couldn't find keyValue of " + keyValue);
        }

        set
        {
            _first[keyValue] = value;
            _second[value] = keyValue;
        }
    }

    public T this[K keyValue]
    {
        get
        {
            if (_second.ContainsKey(keyValue))
            {
                return _second[keyValue];
            }

            throw new System.ArgumentException("Couldn't find keyValue of " + keyValue);
        }

        set
        {
            _second[keyValue] = value;
            _first[value] = keyValue;
        }
    }
}

public static class NB_TransformExtensions
{
    public static void AttachToParent(this Transform trans, Transform parentTrans, int siblingIndex, int? sortingOrder = null, bool? overrideSorting = null, bool? blocksRaycasts = null)
    {
        trans.SetParent(parentTrans, true);
        trans.localScale = Vector3.one;
        trans.SetSiblingIndex(siblingIndex);

        if (sortingOrder.HasValue || overrideSorting.HasValue)
        {
            Canvas canvas = trans.GetComponent<Canvas>();
            if (canvas == null) { canvas = trans.GetComponentInChildren<Canvas>(); } // Try one level up to find a canvas

            if (canvas != null && sortingOrder.HasValue)
            {
                canvas.overrideSorting = true;
                CardController controller = canvas.GetComponent<CardFrontController>();
                if (controller == null) controller = canvas.GetComponent<CardBackController>();
                if (controller != null)
                {
                    controller.SortingOrder = sortingOrder.Value;
                    //Debug.LogFormat("Setting cardcontroller {0} sort order to {1}", controller.CardId, sortingOrder.Value);
                }
                else
                {
                    canvas.sortingOrder = sortingOrder.Value;
                }
            }
            if (canvas != null && overrideSorting.HasValue) { canvas.overrideSorting = overrideSorting.Value; }
        }

        if (blocksRaycasts.HasValue)
        {
            CanvasGroup canvasGroup = trans.GetComponent<CanvasGroup>();
            if (canvasGroup == null) { canvasGroup = trans.GetComponentInChildren<CanvasGroup>(); }
            if (canvasGroup != null) { canvasGroup.blocksRaycasts = blocksRaycasts.Value; }
        }
    }
}