﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.Game;
using DragonFoundry.GameState;
using Messages;
using System;
using System.Collections.Generic;
using DG.Tweening;
using NovaBlitz.UI;
using LinqTools;

public class PlayerArenaControllerMediator : Mediator
{
    [Inject] public PlayerArenaController View {get;set;}
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public SpawnPlayerCardInArenaSignal SpawnPlayerCardInArenaSignal {get;set;}
    [Inject] public PhaseChangedSignal PhaseChangedSignal {get;set;}
    [Inject] public GameControllerService GameControllerService {get;set;}
    [Inject] public ArenaCardRecycledSignal ArenaCardRecycledSignal {get;set;}
    [Inject] public DoneWithTurnClickedSignal DoneWithTurnClickedSignal {get;set;}
    [Inject] public LegalAbilitiesUpdatedSignal LegalAbilitiesUpdatedSignal {get;set;}
    [Inject] public GainControlOfCardSignal GainControlOfCardSignal {get;set;}
    [Inject] public GameStateInitializedSignal GameStateInitializedSignal {get;set;}
    [Inject] public PlayerResolvesUnitSignal PlayerResolvesUnitSignal {get;set;}
    [Inject] public GameData GameData { get; set; }
    [Inject] public PlayerProfile PlayerProfile { get; set; }
    [Inject] public ShowTutorialDialogSignal ShowTutorialDialogSignal { get; set; }
    [Inject] public CardStateUpdatedSignal CardStateUpdatedSignal { get;set;}

    private Phase turnPhase = Phase.NoPhase;
    
    override public void OnRegister()
    {
        this.SpawnPlayerCardInArenaSignal.AddListener(this.OnSpawnArenaCard);
        this.PhaseChangedSignal.AddListener(this.OnPhaseChanged);
        this.ArenaCardRecycledSignal.AddListener(this.OnCardRecycled);
        this.DoneWithTurnClickedSignal.AddListener(this.OnDoneWithTurn);
        this.LegalAbilitiesUpdatedSignal.AddListener(this.View.OnLegalAbilitiesUpdated);
        this.GainControlOfCardSignal.AddListener(this.OnGainControlOfCard);
        this.GameStateInitializedSignal.AddListener(this.OnGameStateInitialzied);
        this.PlayerResolvesUnitSignal.AddListener(this.OnPlayerResolvesUnit);
        this.View.CardAttacks += this.OnCardDeclareAttack;
        this.View.CardBlocks += this.OnCardDeclareBlock;
        this.View.CardActivates += this.OnCardDeclareActivate;
        
    }
    
    override public void OnRemove()
    {
        this.SpawnPlayerCardInArenaSignal.RemoveListener(this.OnSpawnArenaCard);
        this.PhaseChangedSignal.RemoveListener(this.OnPhaseChanged);
        this.ArenaCardRecycledSignal.RemoveListener(this.OnCardRecycled);
        this.PlayerResolvesUnitSignal.RemoveListener(this.OnPlayerResolvesUnit);
        this.DoneWithTurnClickedSignal.RemoveListener(this.OnDoneWithTurn);
        this.LegalAbilitiesUpdatedSignal.RemoveListener(this.View.OnLegalAbilitiesUpdated);
        this.GainControlOfCardSignal.RemoveListener(this.OnGainControlOfCard);
        this.GameStateInitializedSignal.RemoveListener(this.OnGameStateInitialzied);
        this.View.CardAttacks -= this.OnCardDeclareAttack;
        this.View.CardBlocks -= this.OnCardDeclareBlock;
    }

    private void OnPlayerResolvesUnit(ICardState cardState, GameStateReadOnly gameState)
    {
        Debug.Log("#PlayerArenaControllerMediator# Player Plays Unit");
        this.View.OnPlayCardSucceeded(cardState);
    }

    private void OnPlayCardSucceeded(ICardState cardState)
    {
        //  Only pass on the signal data if it's for the players card (ignore opponents)
        if(cardState.ControllerId == this.View.GameState.SelfId)
        {
            this.View.OnPlayCardSucceeded(cardState);
        }
    }

    private void OnGameStateInitialzied(GameStateReadOnly gameState)
    {
        this.View.GameState = gameState;
        Debug.Log("Setting GameState in player arena controller");
    }

    private void OnGainControlOfCard(ICardState cardState)
    {
        CardController affectedCard = this.ArenaCards.GetCard(cardState.Id);
        affectedCard.PlayerArenaController = this.View;
    }

    private void OnDoneWithTurn()
    {
        this.View.DoneWithTurnClicked();
    }

    private void OnCardRecycled(int cardId, bool isPlayerCard, Phase phaseCardRecycled)
    {
        // If a card was trashed in action phase.. it needs to clean up in combat
        if(isPlayerCard && phaseCardRecycled != Phase.Combat)
        {
            this.View.ArenaLayout.UpdateCardTweens();
        }

        // If a card was trashed in combat, but is cleaning up after...
        if(isPlayerCard && this.turnPhase != Phase.Combat && phaseCardRecycled == Phase.Combat )
        {
            this.View.ArenaLayout.UpdateCardTweens();
        }
    }

    private void OnCardDeclareActivate(ICardState cardState, int? targetId)
    {
        this.View.ActivateSound.Play();
        
        this.GameControllerService.SubmitActivateAction(cardState.Id, targetId);
    }

    private void OnCardDeclareBlock(ICardState cardState, int targetId)
    {
        // Pop up warnings if we're blocking with the wrong thing, or in the wrong order.
        if (GameData.CurrentGameData.GameFormat == GameFormat.Tutorial)
        {
            List<NovaBlitz.UI.DataContracts.TutorialCombatData> combatList;
            int id = this.PlayerProfile.OnboardingProgress.CurrentTutorial * 100 + this.GameControllerService.GameState.Turn.TurnNumber;
            if (GameData.CurrentGameData.TutorialAttackData.TryGetValue(id, out combatList))
            {

                NovaBlitz.UI.DataContracts.TutorialCombatData attackRule
                    = combatList.FirstOrDefault(a => a.CardId == cardState.CardId && (a.Action < 1000));
                // above 1000 means it's a Card ID, which means a block
                NovaBlitz.UI.DataContracts.TutorialCombatData blockRule
                    = combatList.FirstOrDefault(a => a.CardId == cardState.CardId && (a.Action > 1000));

                if (null != blockRule)
                {
                    // get block target
                    ICardState target = this.GameControllerService.GameState.GetCard(targetId);
                    if (target != null && blockRule.Action != target.CardId)
                    {
                        this.ShowTutorialDialogSignal.Dispatch(blockRule.Hint, true);
                        return;
                    }
                    else
                    {
                        combatList.Remove(blockRule); // it's an allowed block; process it!
                    }
                }
                else if (null != attackRule)
                {
                    this.ShowTutorialDialogSignal.Dispatch(attackRule.Hint, true);
                    return;
                }

            }
        }
        this.View.DeclareBlockSound.Play();
        this.GameControllerService.SubmitBlockAction(cardState.Id, targetId);
    }

    private void OnCardDeclareAttack(ICardState cardState)
    {
        // Pop up warnings if we're attacking with the wrong thing, or in the wrong order.
        if (GameData.CurrentGameData.GameFormat == GameFormat.Tutorial)
        {
            List<NovaBlitz.UI.DataContracts.TutorialCombatData> combatList;
            int id = this.PlayerProfile.OnboardingProgress.CurrentTutorial * 100 + this.GameControllerService.GameState.Turn.TurnNumber;
            if (GameData.CurrentGameData.TutorialAttackData.TryGetValue(id, out combatList))
            {
                NovaBlitz.UI.DataContracts.TutorialCombatData attackRule
                    = combatList.FirstOrDefault(a => a.CardId == cardState.CardId && (a.Action < 1000));
                // above 1000 means it's a Card ID, which means a block
                NovaBlitz.UI.DataContracts.TutorialCombatData blockRule
                    = combatList.FirstOrDefault(a => a.CardId == cardState.CardId && (a.Action > 1000));
                
                if(null != attackRule)
                {
                    // If you're attacking with something in the wrong order, tell you.
                    //var permanentsForPlayer = this.GameControllerService.GameState.PermanentsForPlayer(this.GameControllerService.GameState.SelfId).ToList();
                    int numberOfAttackers = this.GameControllerService.GameState.PermanentsForPlayer(this.GameControllerService.GameState.SelfId).Where(c => c.IsAttacking).Count() ;
                    if(attackRule.Action > numberOfAttackers + 1)
                    {
                        this.ShowTutorialDialogSignal.Dispatch(attackRule.Hint, true);
                        return;
                    }
                    else
                    {
                        combatList.Remove(attackRule); // it's a legal attack; process it!
                    }
                }
                else if (null != blockRule)
                {
                    this.ShowTutorialDialogSignal.Dispatch(blockRule.Hint, true);
                    return;
                }
            }
        }
        this.View.DeclareAttackSound.Play();
        this.GameControllerService.SubmitAttackAction(cardState.Id);
    }
    
    private void OnPhaseChanged(Phase turnPhase, int turnNumber, GameStateReadOnly gameState)
    {
        if (gameState == null)
            Debug.Log("Gamestate = null");
        
        if (ArenaCards != null && (turnPhase == Phase.Action || turnPhase == Phase.Combat || turnPhase == Phase.EndOfTurn))
        {
            string hand = null;
            string arena = null;
            string oppArena = null;
            try
            {
                hand = string.Join(",", gameState.HandForPlayer(gameState.SelfId).Where(c => c != null && ArenaCards.GetCard(c.Id) != null).Select(c => ArenaCards.GetCard(c.Id).CardIdAndName).ToArray());
            }
            catch (Exception e) { Debug.Log("hand exception"); throw e; }
            try
            { 
                arena = string.Join(",", gameState.PermanentsForPlayer(gameState.SelfId).Where(c => c != null && ArenaCards.GetCard(c.Id) != null).Select(c => ArenaCards.GetCard(c.Id).CardIdAndName).ToArray());
            }
            catch (Exception e) { Debug.Log("arena exception"); throw e; }
            try { 
                if(gameState.Opponent != null)
                    oppArena =  string.Join(",", gameState.PermanentsForPlayer(gameState.Opponent.Id).Where(c => c != null && ArenaCards.GetCard(c.Id) != null).Select(c => ArenaCards.GetCard(c.Id).CardIdAndName).ToArray());
            }
            catch (Exception e) { Debug.Log("opp arena exception"); throw e; }
            Debug.LogFormat("Player Arena Controller: PhaseChanged = {0}. Turn {1}\n Hand [{2}]\n Arena [{3}]\n OppArena [{4}]", turnPhase, turnNumber, hand, arena, oppArena);
        }
        else
            Debug.LogFormat("Player Arena Controller: PhaseChanged = {0}. Turn {1}{2}", turnPhase, turnNumber, ArenaCards == null ? ", ArenaCards is null" : string.Empty);
        this.turnPhase = turnPhase;
        this.View.OnPhaseChanged(turnPhase, gameState);
    }

    private void OnSpawnArenaCard(ICardState spawnCardState, int sourceCardId, CardZone fromZone, GameStateReadOnly gameState)
    {
        // If the card already exists, we're moving it
        CardFrontController newCard = (CardFrontController)this.ArenaCards.GetCard(spawnCardState.Id);
        newCard.PlayerArenaController = this.View;
        
        // Strip off any pending aniamtions (and possibly some disasterous OnComplete animation handlers)
        newCard.transform.DOKill();
        if(newCard.AttackSequence != null)
        {
            Debug.Log("killing attack sequence");
            newCard.AttackSequence.Kill();
        }
        
        //Debug.LogFormat("PlayerSpawnArenaCard: FromZONE:{0} Id:{1} name:{2}", fromZone, spawnCardState.Id, newCard.TitleLabel.text);
       
        newCard.CurrentDamage = 0;
        newCard.UpdateLabels();

        switch (fromZone)
        {
            case CardZone.Void:
                CardController sourceCard = this.ArenaCards.GetCard(sourceCardId);
                if(sourceCard != null)
                {
                    this.View.SpawnArneaCardFromSourceCard(newCard, sourceCard);
                }
                else
                {
                    this.View.SpawnArenaCardFromTrash(newCard);
                }
            break;
            case CardZone.Trash:
                this.View.SpawnArenaCardFromTrash(newCard);
            break;
            default:
                // This is a little undefined, newCard is either somewhere in play... or instantiated off screen
                // this is a little more consistant that spawning from trash or deck.
                this.View.SpawnArneaCardFromSourceCard(newCard, newCard);
            break;

        }
        this.CardStateUpdatedSignal.Dispatch(spawnCardState, gameState);
    }
}
