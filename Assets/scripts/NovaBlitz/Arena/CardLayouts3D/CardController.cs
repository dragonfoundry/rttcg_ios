﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using DG.Tweening;
using UnityEngine.UI;
using Messages;
using TMPro;
using NovaBlitz.UI;
using NovaBlitz.UI.DataContracts;
using System;

public class CardController : PoolableMonoBehaviour
{
    public virtual string CardIdAndName { get { return string.Format("id:{0}", CardId); } }
    public int CardId;
    public Image Card;
    public RawImage CardBack;
    public Canvas Canvas;
    public CanvasGroup CanvasGroup;
    public Toggle AttackToggleButton;
    public Toggle AbilityToggleButton;
    public TargetArrowTarget TargetArrowTarget;
    public bool IsActionsActive {get; private set;}
    public TextMeshProUGUI TitleLabel;
    public TextMeshProUGUI CostLabel;
    public PlayerArenaController PlayerArenaController;
    public Animator CardGlowAnimator;
    
    public int CurrentDamage{get;set;}
    public bool CanFight { get { return this.canAttack || this.canBlock; } }
    public bool CanBeSelected { get { return this.canAttack || this.canBlock || (this.canActivate && this.TargetArrowTarget.TargetAction == TargetAction.None); } }
    public bool CanAttack { get { return this.canAttack; } }
    public bool CanBlock {get { return this.canBlock; } }
    public bool CanActivate {get { return this.canActivate; }}
    public bool CanTargetAbility { get {return this.canTargetActivation;}}
    public virtual int SortingOrder { get { return Canvas.sortingOrder; } set { Canvas.sortingOrder = value; } }

    public bool HasKeywords
    {
        get
        {
            if(this.cardState != null)
            {
                if(this.cardState.Keywords != null && this.cardState.Keywords.Count > 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
    
    public CardType CardType
    {
        get 
        {
            if(this.cardState == null)
            {
                if(this.cardData == null)
                {
                    return CardType.Undefined;
                }
                else
                {
                    return this.cardData.CardType;
                }
            }
            else
            {
                if(this.cardState.IsPower) return CardType.Power;
                if(this.cardState.IsUnit) return CardType.Unit;
                return CardType.Undefined;
            }
        }
    }

    public CardZone CardZone
    {
        get{
            if(this.cardState == null)
            {
                return CardZone.NoZone;
            }
            else
            {
                return this.cardState.Zone;
            }
        }
    }

    public int ControllerID
    {
        get {
            if(this.cardState == null)
            {
                return 0;
            }
            else
            {
                return this.cardState.ControllerId;
            }
        }
    }
        
    public bool IsPower { get { if(this.cardState == null) return false; return this.cardState.IsPower; } }
    public bool IsUnit { get { if(this.cardState == null) return false; return this.cardState.IsUnit; } }
    
    protected float hoverFxAlpha = 0.0f;
    protected float combatFxAlpha = 0.0f;
    protected Sequence keywordSequence;
    protected ICardState cardState;
    protected CardData cardData;
    protected int timesAttacked = 0;
    protected int timesBlocked = 0;
    protected int timesActivated = 0;
    protected LegalAbilities legalAbilities = null;
    protected bool isPlayable = false;
    protected bool canAttack = false;
    protected bool canBlock = false;
    protected bool canActivate = false;
    protected bool canTargetActivation = false;
    
    void Start()
    {
        this.PlayerArenaController = null;
        this.IsActionsActive = false;
        
        if(this.AttackToggleButton != null)
        {
            this.AttackToggleButton.gameObject.SetActive(false);
        }
        
        if(this.AbilityToggleButton != null)
        {
            this.AbilityToggleButton.gameObject.SetActive(false);
        }
    }
    
    public bool BlocksRaycasts
    {
        set { this.CanvasGroup.blocksRaycasts = value; }
        get { return this.CanvasGroup.blocksRaycasts; }
    }
    
    public string RaycastTag
    {
        set { this.Card.tag = value;}
        get { return this.Card.tag; }
    }
    
    public void ActivateActions()
    {
        this.CanvasGroup.interactable = true;
        this.AttackToggleButton.gameObject.SetActive(this.CanAttack);

#if UNITY_IOS || UNITY_ANDROID
            this.AttackToggleButton.enabled = false;
#else
        this.AttackToggleButton.enabled = true;
#endif

        this.AbilityToggleButton.gameObject.SetActive(this.CanActivate && !this.CanTargetAbility);

#if UNITY_IOS || UNITY_ANDROID
            this.AbilityToggleButton.enabled = false;
#else
        this.AbilityToggleButton.enabled = true;
#endif

        this.hoverFxAlpha = 0f;
        this.CardGlowAnimator.SetBool("IsSelected", true);
        this.IsActionsActive = true;
        this.transform.DOScale(new Vector3(1.6f,1.6f,1.6f), 0.1f).SetEase(Ease.OutElastic);
        //this.SortingOrder = 9;
    }
    
    public void ResetRootCanvasSize()
    {
        RectTransform rootRect = (RectTransform)this.transform;
        rootRect.anchorMin = new Vector2(0.5f,0.5f);
        rootRect.anchorMax = rootRect.anchorMin;
        rootRect.pivot = rootRect.anchorMin;
        rootRect.sizeDelta = Vector2.zero;
    }
    
    protected TargetArrowController arrowController;
    protected TargetArrowTarget target;
    protected TargetArrowInfo targetArrowInfo;
    
    public virtual void AttackTarget(TargetArrowTarget target, TargetArrowController arrowController)
    {
       
    }
    
    public virtual void BlockTarget(TargetArrowTarget target, TargetArrowController arrowController)
    {
    
    }

    public virtual void ActivateTarget(TargetArrowTarget target, TargetArrowController arrowController)
    {
        
    }

    public void ClearPlayAbilityStates()
    {
        if (this.CardGlowAnimator.isInitialized)
        {
            this.CardGlowAnimator.SetBool("IsPlayable", false);
            this.CardGlowAnimator.SetBool("IsCombatActive", false);
            this.CardGlowAnimator.SetBool("IsActivatable", false);
        }
    }

    public void RefreshLegalAbilities()
    {
        this.DisplayLegalAbilities(this.legalAbilities);
    }
    
    public void DisplayLegalAbilities(LegalAbilities abilities)
    {
        this.legalAbilities = abilities;

        if (this.cardState == null || abilities == null)
        {
            //Debug.Log("Card state or abilities null");
            this.isPlayable = false;
            this.canAttack = false;
            this.canBlock = false;
            this.canActivate = false;
            this.canTargetActivation = false;
        }
        else
        {

            this.isPlayable =
                abilities.CanCast(this.cardState.Id)
                && cardState.Zone != CardZone.Stack;

            this.canAttack =
                this.timesAttacked <= 0 && abilities.CanAttack(this.cardState.Id)
                && (this.timesBlocked == 0 || this.cardState.GetStat(KeywordEnum.Vanguard) > 0);


            this.canBlock =
                timesBlocked <= this.cardState.GetStat(KeywordEnum.DoubleBlock) && abilities.CanBlock(this.cardState.Id) // This works, because DoubleBlock is 0 if you don't have it, and 1 if you do.
                && (this.timesAttacked == 0 || this.cardState.GetStat(KeywordEnum.Vanguard) > 0);


            this.canActivate =
                timesActivated <= this.cardState.GetStat(KeywordEnum.ExtraActivations) && abilities.CanActivate(this.cardState.Id);

            this.canTargetActivation = abilities.ActivateRequiresTarget(this.CardId);
        }

        if(this.CardGlowAnimator.isInitialized)
        {
            this.CardGlowAnimator.SetBool("IsPlayable", this.isPlayable);
            this.CardGlowAnimator.SetBool("IsCombatActive", this.canAttack || this.canBlock);
            this.CardGlowAnimator.SetBool("IsActivatable", this.canActivate);
        }
    }

    public void SetCardBack(int cardBackId)
    {
        CardBackDataContract contract;
        if (GameData.CardBackDictionary.TryGetValue(cardBackId, out contract) && contract != null && !string.IsNullOrEmpty(contract.ArtId))
        {
            Texture tex = AssetBundleManager.Instance.LoadAsset<Texture>(contract.ArtId);
            //Texture tex = (Texture)Resources.Load(contract.ArtId);
            if (tex != null)
            {
                this.CardBack.texture = tex;
                return;
            }
        }
        // If we fail to set a special card back, set the basic one.
        if (GameData.CardBackDictionary.TryGetValue(0, out contract) && contract != null && !string.IsNullOrEmpty(contract.ArtId))
        {
            Texture tex = AssetBundleManager.Instance.LoadAsset<Texture>(contract.ArtId);
            //Texture tex = (Texture)Resources.Load(contract.ArtId);
            if (tex != null)
            {
                this.CardBack.texture = tex;
                return;
            }
        }
    }

    public virtual void ShowAspectFrame() {}
    
    /// <summary>
    /// Initializes the view on the card
    /// </summary>
    public virtual void Initialize(ICardState cardState, int cardBackId, bool isPurified)
    {
        this.PlayerArenaController = null;
        this.IsActionsActive = false;
        this.CardId = cardState.Id;
        this.cardState = cardState;
        if(this.TargetArrowTarget != null)
        {
            this.TargetArrowTarget.Id = cardState.Id;
            TargetArrowTarget.TargetAction = TargetAction.None;
        }
        
        this.CurrentDamage = cardState.Damage;
        this.IsActionsActive = false;
        if(this.CardGlowAnimator.isInitialized)
        {
            this.CardGlowAnimator.SetBool("IsPlayable", false);
            this.CardGlowAnimator.SetBool("IsActivatable", false);
            this.CardGlowAnimator.SetBool("IsAffected", false);
            this.CardGlowAnimator.SetBool("IsCombatActive", false);
            this.CardGlowAnimator.SetBool("IsHovered", false);
            this.CardGlowAnimator.SetBool("IsSelected", false);
        }

        // Initialize cardBack, based on controller
        SetCardBack(cardBackId);
    }

    protected virtual void OnDestroy()
    {
        CardBack.texture = null;
    }

    public void ResetTrackers()
    {
        timesActivated = 0;
        timesAttacked = 0;
        timesBlocked = 0;
    }

    public override void OnRecycle()
    {
        CurrentDamage = 0;
        cardState = null;
        cardData = null;
        legalAbilities = null;
        ResetTrackers();
        isPlayable = false;
        canAttack = false;
        canBlock = false;
        canActivate = false;
        canTargetActivation = false;
    }
}
