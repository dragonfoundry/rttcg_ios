﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using DG.Tweening;

public class HandLayout3D : MonoBehaviour 
{
    // =====================
	// Public Properties
	// =====================
	public float Width { get { return ((RectTransform)transform).sizeDelta.x;}}
	public float Height { get { return ((RectTransform)transform).sizeDelta.y;}}
	public float VerticalRange = 235f;
	public AnimationCurve RotationCurve;
	public AnimationCurve PositionCurve;
	public float MaxSeperation = 205f;
	public bool IsInverted = false;
	public int MaxCardsInFlatHand = 4;
    
    public int BaseIndex = 30;
    
    [ContextMenu("UpdatePosition()")]
    /// <summary>
	/// When a card is dealt or played from the hand, this method adjusts the cards positions.
	/// <summary>
	public void UpdateCardTweens()
	{
	   	for(int i=0;i<this.transform.childCount; i++)
		{
			Transform child = this.transform.GetChild(i);

            Vector3 pos = this.GetChildPosition(child);
            child.localRotation = Quaternion.identity;
            child.Rotate(0f,0f, this.GetChildZRotation(child));
            
			// Assign the sorting order for the child canvas (if there is one)
			//Canvas canvas = child.GetComponent<Canvas>();
			//if(canvas != null) { canvas.sortingOrder = this.BaseIndex+(i*5); }
			CardController card = child.GetComponent<CardController>();
			if(card != null) { card.SortingOrder = this.BaseIndex+(i*5); }
			
			// If the child's calculated position is different than its current position
			if(Vector3.Distance(child.localPosition, pos) > 0.1f)
			{
                var cardFront = child.GetComponent<CardFrontController>();
                if (cardFront == null || cardFront.RaycastTag != "returning")
                {
                    child.DOKill();

                    // Tween the child to its new local position
                    child.DOLocalMove(pos, 0.27f).SetEase(Ease.OutExpo);
                }
            }
		}
	}
    
    // TODO: Possibly move this up into the starting hand? or possibly hand controller?
    public void TweenInStartingHand(float delayBetweenCards = 0.15f)
    {
        for(int i=0;i<this.transform.childCount; i++)
        {
            Transform child = this.transform.GetChild(i);

            Vector3 pos = this.GetChildPosition(child);
            child.localRotation = Quaternion.identity;
            child.Rotate(0f,0f, this.GetChildZRotation(child));
            
            // Assign the sorting order for the child canvas (if there is one)
            Canvas canvas = child.GetComponent<Canvas>();
            if(canvas != null) 
            { 
                canvas.overrideSorting = true;
                //Debug.LogFormat("sortingOrder: {0}", canvas.sortingOrder);
            }

            CardController card = child.GetComponent<CardController>();
			card.SortingOrder = this.GetSortingOrder(child);
            card.transform.DOKill();
            
            Sequence cardSequence = DOTween.Sequence();
            // cardSequence.AppendInterval(this.transform.childCount * 0.15f - (i*0.15f));
            cardSequence.AppendInterval(i*delayBetweenCards);
            // Tween the child to its new local position
            cardSequence.Append(card.transform.DOLocalMove(pos, 0.25f).SetEase(Ease.OutExpo));
            cardSequence.Insert(0, child.DOScale(Vector3.one, 0.25f));
            cardSequence.AppendCallback(()=>
                {
                    card.CanvasGroup.blocksRaycasts = true;
                });
            cardSequence.Play();
        } 
    }
    
    /// <summary>
    /// Given a child transform, calculate the sortingOrder to apply to the canvas
    /// </summary>
    public int GetSortingOrder(Transform child)
    {
        if(child.IsChildOf(this.transform))
        {
            return this.BaseIndex + child.GetSiblingIndex() * 5;
        }
        
        return 1;
    }
    
    /// <summary>
    /// Given a reference to a sibling, calculate what index it should be attached to in the hand
    /// </summary>
    public void CalculateSiblingIndex(Transform sibling)
    {
        int closestIndex = -1;
        float closestDistance = float.MaxValue;
        
        if(sibling.parent == this.transform)
        {
            // find the correct sibling index for the insert
            for(int i=0;i<this.transform.childCount;i++)
            {
                float normalizedPos = this.GetPositionOnCurve(i);
                float xPos = this.Width * normalizedPos  - (this.Width * 0.5f);
                float distance = Mathf.Abs(xPos - sibling.localPosition.x);
                if(distance < closestDistance)
                {
                    closestDistance = distance;
                    closestIndex = i;
                }
            }
        }
        
        bool isIndexChanged = (closestIndex > -1) && closestIndex != sibling.GetSiblingIndex();
        if(isIndexChanged)
        {
            sibling.SetSiblingIndex(closestIndex);
        }
    }
    
    /// <summary>
    /// Given a child/sibling transform caculate its Z-rotation (for fanned cards etc.)
    /// </summary>
    public float GetChildZRotation(Transform child)
    {
        int numCardsInHand = this.transform.childCount;
		int childIndex = child.GetSiblingIndex();
        
		float childPositionOnCurve = this.GetPositionOnCurve(childIndex);
        
        float zRotation = 0f;
        
        // Calculate the cards rotation
        if(numCardsInHand > 1 && numCardsInHand <= this.MaxCardsInFlatHand)
        {	
            // Special case for inverted (flip cards 180)
        }
        else
        {
            // All other cases create a nice fan shape using the curves
            zRotation = this.GetCardRotationZ(childPositionOnCurve);
        }
        
        return zRotation;
    }
    
    /// <summary>
    /// Given a child/sibling reference, what is its target local position
    /// </summary>
    public Vector3 GetChildPosition(Transform child)
    {
        int numCardsInHand = this.transform.childCount;
		int childIndex = child.GetSiblingIndex();
        
		float childPositionOnCurve = this.GetPositionOnCurve(childIndex);
		Vector3 pos;
		pos.x = this.Width * childPositionOnCurve  - (this.Width * 0.5f);
		
		// Vertical Positining
		if(numCardsInHand > 1 && numCardsInHand <= this.MaxCardsInFlatHand)
		{
			pos.y = 0;
		}
		else
		{
			pos.y = this.VerticalRange * this.GetChildPercentYOffset(childPositionOnCurve);		
		}
		
		pos.z = 0;
        
        return pos;
    }
    
    /// <summary>
    /// Using the animation curve calculate how far to offest the card at the specified index
    /// should be offest on its Y axis from center.
    /// </summary>
    public float GetVerticalOffset(int cardIndex)
    {
        float positionOnCurve = this.GetPositionOnCurve(cardIndex);
        float percentYOffset = this.GetChildPercentYOffset(positionOnCurve);
        return this.VerticalRange * percentYOffset;
    }
    
    /// <summary>
    /// Given a card index, get its normalized position on an animation curve between 0.0-1.0
    /// </summary>
	float GetPositionOnCurve(int cardIndex)
	{
		int numCardsInHand = this.transform.childCount;
		
		// CardSeperation is normalized for the curve, but MaxSeperation is in screen units so we do the conversion
		cardIndex = Mathf.Clamp(cardIndex,0,numCardsInHand);
		float cardSeperation = 1.0f / numCardsInHand;
		cardSeperation = Mathf.Min(this.MaxSeperation, cardSeperation * this.Width) / this.Width;
		
		// If the hand layout is inverted, inverse the direction of the layout
		if(this.IsInverted)
		{
			cardSeperation *= -1;
		}
		
		float curveMidpoint = 0.5f;
		float initialCurvePosition = 
			curveMidpoint // Start with the midpoint of the curve
			- (cardSeperation * numCardsInHand / 2f) // offset to the position of the left most card
			+ cardSeperation / 2f; // <- ensures with an odd number of cards, we have a middle card
			
		float cardPositionOnCurve = initialCurvePosition + cardIndex * cardSeperation;
		if(numCardsInHand <= 1)
		{ 
			// If there's only one card, make sure it's offset to the left of center
			cardPositionOnCurve = initialCurvePosition - cardSeperation * 0.5f;
		}
		else if(numCardsInHand == 2)
		{
			// If there's two cards do some custom positioning too (both left of center a little)
			cardPositionOnCurve = initialCurvePosition + cardIndex * cardSeperation - cardSeperation * 0.25f;
		}
		
		return cardPositionOnCurve;
	}
	
	/// <summary>
	/// Using the editor defined animation curve, find a cards rotation based on its
	/// point along that curve
	/// </summary>
	float GetCardRotationZ(float pointOnCurve)
	{
		if(this.transform.childCount <= this.MaxCardsInFlatHand)
		{
			return 0f;
		}
		
		// Get two y values on the curve one to the left and one to the right of pointOnCurve
		float xOffset = 0.1f;
		float y1Point = this.RotationCurve.Evaluate(pointOnCurve - xOffset * 0.5f);
		float y2Point = this.RotationCurve.Evaluate(pointOnCurve + xOffset * 0.5f);
		
		// Create a vector between those two points (this is a tangent vector to pointOnCurve)
		Vector2 tail = Vector2.right;
		Vector2 tip = new Vector2(xOffset, pointOnCurve > 0.5f ? y1Point - y2Point : y2Point - y1Point);
		
		// Calcuate the angle in degrees of that tangent vector
		float degrees = Vector2.Angle(tail,tip);
		if(pointOnCurve > 0.5f) { degrees *= -1; }
        
        if(this.IsInverted)
        {
            degrees *= -1;
        }
        
		return degrees;
	}
	
	/// <summary>
	/// Using the editor defined animation curve, find a cards vertical offset based on its
	/// point along that curve
	/// </summary>
	float GetChildPercentYOffset(float pointOnCurve)
	{
		float offset = this.PositionCurve.Evaluate(pointOnCurve) - 1f;
        if(this.IsInverted) { offset = -offset; }
        return offset;
	}
}

