﻿using UnityEngine;
using UnityEngine.EventSystems;
using LinqTools;
using System.Collections.Generic;
using DragonFoundry.GameState;
using System;
using TMPro;

public enum InputEvent
{
    MouseOverHandCard,
    MouseOutHandCard,
    MouseUp,
    MouseUpPlayerCard,
    MouseOutSourceCard,
    MouseOverSourceCard,
    MouseUpHand,
    MouseUpCard,
    MouseUpArena,
    MouseOutHand,
    MouseOverHand,
    MouseClick,
    CardDraggedUp,
    ArrowDragged,
    MouseOutWindow,
    //==========================================================================================
    // Below this line, the events don't originate in the InputEventController but rather other
    // areas in the game, often because of somethin that happened on the server. Because the
    // Player/Hand/Arena controlers are  InputEvent driven, having these additional events be 
    // fired from other areas of the game allows the Player/Hand/Arena controllers to handle 
    // these events in the same way they would any other mouse/touch input event.
    //==========================================================================================
    CardDragBegin, // CardDragBegin and CardDragEnd could be refactored to be  fired from 
    CardDragEnd,   // the InputEventController, but the "code purity" isn't worth the effort -DMac
    LostCastAbility,
    LostCombatAbility,
    LostActivatedAbility,
    SummonFromHand,
    DiscardFromHand,
    PhaseEnded, 
}

///<summary>
/// This controller is responsible for consuming Mouse/Touch input to generate InputEvents that
/// are consumed by the PlayerHandController, PlayerArenaController, and OppponentArenaController.
/// It tracks things like hover states of cards, weather a targeting arrow is being dragged or a card
/// is being dragged.
///
/// In the arena it handles all the hover / zoom in states of the cards. The PlayerHandController
/// handles hover states though MouseOverHandCard and MouseOutHandCard InputEvents.
///
/// The design goal behind this class is to keep all of the rather gnarly input handling logic in one 
/// component and let the various arena/hand controller classes be clean state machines driven by 
/// specific InputEvents.
/// </summary>
public class InputEventController : MonoBehaviour 
{
    // Publics
    public GameStateReadOnly GameState;
    public Canvas ArenaIUCanvas;
    public PlayerHandController PlayerHandController;
    public OpponentHandController OpponentHandController;
    public PlayerArenaController PlayerArenaController;
    public OpponentArenaController OpponentArenaController;
    public TargetArrowController TargetArrowController;
    public ArenaZoomController ArenaCardZoom;
    public TargetArrowTarget OpponentTarget;
    public TargetArrowTarget PlayerTarget;
    public bool IsArrowDragging { get { return this.TargetArrowController.DraggedTargetArrowInfo != null && this.TargetArrowController.DraggedTargetArrowInfo.FollowDestination == null;}}
    public float DragUpThreshold = 15f;
    public float DragUpThreshold_IOS = 30f;
   
    public CardController SelectedCard
    {
        get{ return this.selectedCard; }
        set{
            // If we're setting selectedcard to null, we want to hide  targets
            if (this.selectedCard != null && value == null)
            {
                if(PlayerArenaController.HideTargets != null)
                    PlayerArenaController.HideTargets.Invoke();
                var cardControllers = PlayerArenaController.ArenaCards.AllCardControllers;
                for (int i = 0; i < cardControllers.Count(); i++)
                {
                    cardControllers[i].RefreshLegalAbilities();
                }
            }
            else if (value != null)
            {
                var cardControllers = PlayerArenaController.ArenaCards.AllCardControllers;
                for (int i = 0; i < cardControllers.Count(); i++)
                {
                    var cardController = cardControllers[i];
                    if (cardController != value)
                        cardControllers[i].ClearPlayAbilityStates();
                }
            }
            this.selectedCard = value;
            this.ArenaCardZoom.RecycleZoomedCard();
        }
    }
    public CardController DraggedCard { get { return this.PlayerHandController.DraggedCard;}}
    
    // Privates
    private CardController hoveredArenaCard = null;
    private CardController hoveredHandCard = null;
    private TargetArrowTarget hoveredPlayerTarget = null;
    private CardController lastHoveredHandCard = null;
    private CardController selectedCard = null;
    private bool isMouseOverHand = false;
    private bool isMouseOverWindow = false;
    private float? mouseDownTime;
    private float? arenaCardHoveredTime;
    private int cardHoverFrameCounter = 0;
    private Vector2? mouseOverPos;
    private float DRAG_UP_THRESHOLD
    {
        get
        {
#if UNITY_IOS || UNITY_ANDROID//) && !UNITY_EDITOR
            return this.DragUpThreshold_IOS;
#else
			return this.DragUpThreshold;
#endif
        }
    }
    private const float DRAG_ARROW_THRESHOLD = 15;
    private Camera uiCamera;
    private Canvas MainCanvas;

    void Start()
    {
        this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
        this.MainCanvas = GameObject.Find("SessionContext").GetComponent<SessionContextView>().MainCanvas;
    }
    
  
    
    /// <summary>  
    /// Hoverd cards in the Hand and Arena are tracked seperately. (because they handle hovers differently)
    /// "hoveredArenaCard" stores a reference to the most recently hovered arena card.
    /// "hoveredHandCard" store a reference to the most recently hovered hand card.
    /// "cardMouseOver" stores a reference the the card currently hit by raycasting though the mouse.
    ///
    /// This InputEventController manages the hover state and Card ZoomIn functionality for cards in the arena.
    /// The PlayerHandController manages the hoverstate of its cards though mouseOver/mouseOut InputEvents.
    ///
    /// Cards in the Arena are tagged "arenaCard"
    /// Cards in the Hand are tagged "card" (and the insert indicators are tagged "cardInsert")
    ///
    /// When we are dragging a TargetArrow we don't want the players hand processing InputEvents
    /// When we are dragging a card from the hand we don't want the arena processing InputEvents
    /// </summary>
    void LateUpdate()
    {
        this.cardHoverFrameCounter++;
        
        // =============================
        // Base mouse pointer calculations
        // =============================
        
        // check to see if we're actually over the main canvas. If not, we're not over the window.
        bool isOverMainCanvas = RectTransformUtility.RectangleContainsScreenPoint((RectTransform)this.MainCanvas.transform, Input.mousePosition, this.uiCamera);

        // Is the mouse over the ArenaUICanvas (meaning it's over the window)
        bool isOverWindow = isOverMainCanvas && RectTransformUtility.RectangleContainsScreenPoint((RectTransform)this.ArenaIUCanvas.transform, Input.mousePosition, this.uiCamera);
                
        // <Windows Specific>
        if(Input.mousePosition.x == 0 || Input.mousePosition.x == Screen.width || Input.mousePosition.y == 0 || Input.mousePosition.y == Screen.height)
        {
            isOverWindow = false;
        }
        // Is the mouse over the PlayerHand RectTransform?
        bool isOverHand = isOverWindow && RectTransformUtility.RectangleContainsScreenPoint((RectTransform)this.PlayerHandController.transform, Input.mousePosition, this.uiCamera);
        bool isOverArena = !isOverHand;

#if UNITY_IOS || UNITY_ANDROID//) && !UNITY_EDITOR
		bool isTouching = Input.touchCount > 0 
			|| Input.GetMouseButton (0) == true 
			|| Input.GetMouseButtonUp (0) == true 
			|| Input.GetMouseButtonDown (0) == true;
		isOverArena = isOverArena && isTouching ;
		isOverWindow = isOverWindow && isTouching;
		isOverHand = isOverHand && isTouching;
		//this.isMouseOverHand = isOverHand && isTouching;
#endif
        
        bool isMustTriggerMouseOutHand = false;

        // =============================
        // Card Zooming In The Arena
        // =============================
        if (this.hoveredArenaCard != null 
			&& isOverArena
            && this.PlayerHandController.IsDraggingCard == false)
		{
			// De bug.LogFormat("Card Zoomin in arena: arenaCardHoveredTime: {0}", this.arenaCardHoveredTime);
			// Have we hovered over a card the required amount of time?
			if(Time.unscaledTime - this.arenaCardHoveredTime > 0.05f)
			{
				if(this.ArenaCardZoom.ZoomedCard != this.hoveredArenaCard 
					&& this.TargetArrowController.DraggedTargetArrowInfo == null)
				{
					((CardFrontController)this.hoveredArenaCard).OutlineCanvasGroup.alpha = 1.0f;

					if(Time.unscaledTime - this.arenaCardHoveredTime > 0.6f)
					{
						this.ArenaCardZoom.RecycleZoomedCard();
						this.ArenaCardZoom.OnZoomInCard(this.hoveredArenaCard);
					}
				}
			}
		}

		// =============================
		// Mouse Over/Out Raycasts
		// =============================
        if(isOverHand != this.isMouseOverHand)
        {
            if(isOverHand)
            {
                    CardController activeCard = this.DraggedCard;
                    if (activeCard == null) { activeCard = this.hoveredHandCard; }
                    if (activeCard == null) { activeCard = this.lastHoveredHandCard; }

                this.TriggerInput(InputEvent.MouseOverHand, activeCard);
                this.isMouseOverHand = true;
                if (this.mouseOverPos != null)
                    this.mouseOverPos = Input.mousePosition;
            }
            else if(isOverWindow) // Are we over the window still?
            {
                // Only send the MouseOutHand event if we're not dragging, or have 
                // dragged past the drag up threshold
                float distanceDragged = this.mouseOverPos.HasValue ? Vector2.Distance(Input.mousePosition, this.mouseOverPos.Value) : 0;
                if(Input.GetMouseButton(0) == false // If there's no possibility of starting a drag, send the  mouse out reliably
                || (Input.GetMouseButton(0) && distanceDragged > DRAG_UP_THRESHOLD * 3f)  // This allows distance to register a fast drag
                || this.DraggedCard != null) // however, if we're already dragging send the mouse out reliably
                {
                    isMustTriggerMouseOutHand = true;
					//Debug.LogFormat("#Input# Must trigger mouse out 1");
                }
            }
			else
			{
				isMustTriggerMouseOutHand = true;
				//Debug.LogFormat("#Input# Must trigger mouse out 1b");
			}
        }


        // Did we leave the Window entirely?
        if (isOverWindow == false && this.isMouseOverWindow == true)
        {
            // If so, let the controllers know
            CardController activeCard = this.DraggedCard;
            if(activeCard == null) { activeCard = this.hoveredArenaCard;}
            if(activeCard == null) { activeCard = this.lastHoveredHandCard;}
            //Debug.Log("Trigger Mouse out Window");
            this.TriggerInput(InputEvent.MouseOutWindow, activeCard);
            this.mouseDownTime = null;
            this.mouseOverPos = null;
            this.isMouseOverHand = false;
			this.hoveredHandCard = null;

			// If there was a hovered arena card, unhover it.
			if (this.hoveredArenaCard != null)
			{
				this.UnHoverArenaCard ();
			}
        }
			

        // Update mouseover region states
        this.isMouseOverWindow = isOverWindow;

		if (this.isMouseOverWindow)
		{
        
			// =============================
			// Mouse Down Hack
			// =============================
			if (Input.GetMouseButtonDown (0)
            // This happens when dragging onto the screen from outside the window
			        || (this.mouseOverPos == null && Input.GetMouseButton (0) == true))
			{
				// We reset the mouseOverPos to the mouse position on mouse down
				// so that we can perform accurate drag distance calculations
				this.mouseOverPos = Input.mousePosition;
				this.mouseDownTime = Time.timeSinceLevelLoad;
			}
        
       
			// =============================
			// Raycast Events (un)hover
			// =============================
			PointerEventData pointer = new PointerEventData (EventSystem.current);
			pointer.position = Input.mousePosition;
			List<RaycastResult> raycastResults = new List<RaycastResult> ();
			if(this.isMouseOverWindow)
				EventSystem.current.RaycastAll (pointer, raycastResults);
       
			CardController cardMouseIsOver = null;
			if (raycastResults.Count > 0)
			{
				// If the raycast hit something, investigate what...
				GameObject hit = raycastResults.First ().gameObject;
				// De bug.LogFormat("RaycastHit: {0} hittag: {1}", hit.name, hit.tag);
				if (hit.activeSelf == false)
				{
					Debug.LogFormat ("hit is not activeSelf: {0}", hit.name);
					return;
				}
            
				string hitTag = hit.tag;

				// Check for Player Hovers
				if (hitTag == "player")
				{
					if (this.hoveredPlayerTarget != null)
					{
						this.UnhoverPlayer ();
					}
                
					this.HoverPlayer (hit.gameObject.GetComponent<PlayerControllerBase> ().TargetArrowTarget);
				}
				else
				{
					this.UnhoverPlayer ();
				}

				// Check for arena card raycast Hits
				if (hitTag == "arenaCard")
				{
					cardMouseIsOver = hit.transform.GetComponentInParent<CardController> ();
                    // is the arena card the mouse is over different that the hovered arena card?

                    //Debug.LogFormat("Hit arena card. cmo:{0} hac:{1} chfc:{2} mbd?:{3} sc:{4}", cardMouseIsOver != null ? cardMouseIsOver.CardId : 0, hoveredArenaCard != null ? hoveredArenaCard.CardId : 0, cardHoverFrameCounter, Input.GetMouseButton(0), SelectedCard != null ? SelectedCard.CardId : 0);
					if (cardMouseIsOver != this.hoveredArenaCard && this.cardHoverFrameCounter > 1 && (Input.GetMouseButton(0) == false || SelectedCard != null || DraggedCard != null || Input.GetMouseButtonDown(0)))
					{
						// If there was a previously hovered card...
						if (this.hoveredArenaCard != null)
						{
							this.UnHoverArenaCard ();
						}
						// If there was a hovered card in the players hand and we're not starting a drag...
						if (this.hoveredHandCard != null && Input.GetMouseButton(0) == false)
						{
							this.TriggerInput (InputEvent.MouseOutHandCard, this.hoveredHandCard);
						}

                        if (this.SelectedCard == cardMouseIsOver && Input.GetMouseButton(0) == false)
                        {
                            // we're mousing back over the selected card - stop dragging
                            this.TriggerInput(InputEvent.MouseOverSourceCard, this.SelectedCard);
                        }

                        if (hoveredHandCard != null)
                        {
                            //Debug.LogError("Over arena card, but hovered hand card is not null");
                        }
                        else
                        {
                            // Set the HoverFx on cards,
                            this.HoverArenaCard(cardMouseIsOver);
                            this.mouseOverPos = Input.mousePosition;
                            //this.hoveredArenaCard = cardMouseIsOver;
                        }
					}

/*#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
					if(this.SelectedCard != null //&& this.mouseDownTime.HasValue
						&& Input.touchCount > 0 
						&& Input.GetTouch(0).phase == TouchPhase.Ended) 
#else*/
					if (this.SelectedCard != null /*&& this.mouseDownTime.HasValue*/ && Input.GetMouseButtonUp (0))
//#endif
					{
						CardFrontController clickedCard = hit.GetComponentInParent<CardFrontController> ();

						if (hit.transform.IsChildOf (this.SelectedCard.AttackToggleButton.transform))
						{
							//Debug.Log("Attack clicked");
							TargetArrowTarget target = this.OpponentTarget;
							this.SelectedCard.AttackTarget (target, this.TargetArrowController);
                            this.TriggerInput(InputEvent.LostCombatAbility, SelectedCard);
                        }
						else if (hit.transform.IsChildOf (this.SelectedCard.AbilityToggleButton.transform))
						{
							//Debug.Log("Untargeted Ability clicked");
							this.SelectedCard.ActivateTarget (null, this.TargetArrowController);
                            this.TriggerInput(InputEvent.LostActivatedAbility, SelectedCard);
                        }
						else if (clickedCard != null && clickedCard.TargetArrowTarget.TargetAction == TargetAction.Activate)
						{
							//Debug.Log("Targeted Ability clicked");
							this.SelectedCard.ActivateTarget (clickedCard.TargetArrowTarget, this.TargetArrowController);
                            this.TriggerInput(InputEvent.LostActivatedAbility, SelectedCard);
                        }
                        else if (this.SelectedCard == cardMouseIsOver)
                        {
                            // we're mousing back over the selected card - stop dragging
                            this.TriggerInput(InputEvent.MouseOverSourceCard, this.SelectedCard);
                        }
                    }
				}
	            // Check for hand card raycast hits
	            else if (hitTag == "card")
				{
					bool isUnderDragThreshold = false;
					bool isDragging = false;
					// If we're dragging, don't change hovered cards until after we've exceeded the drag threshold.
					// This prevents the feelig of dragging the card next to the one you wanted accidentally.
					if (Input.GetMouseButton (0) && this.mouseOverPos.HasValue && isMouseOverHand)
					{
						Vector2 mouseVector = ((Vector2)Input.mousePosition) - this.mouseOverPos.Value;
						float dragDistance = mouseVector.magnitude;
						if (dragDistance < DRAG_UP_THRESHOLD)
						{
							isUnderDragThreshold = true;
						}
						if (dragDistance > DRAG_UP_THRESHOLD / 3f)
							isDragging = true;

					}

					// If there was a hovered arena card, unhover it.
					if (this.hoveredArenaCard != null)
					{
						this.UnHoverArenaCard ();
					}
					if(this.PlayerHandController.IsUsingZoomHand 
						&& this.PlayerHandController.IsHandZoomedIn == false )
					{
						this.PlayerHandController.MustSetZoomHandActiveTo = true;
					}
					//Debug.LogFormat ("Hit CARD; underDrag:{0}, hoveredCard {1}", isUnderDragThreshold, hoveredHandCard == null ? "null" : hoveredHandCard.name);

					// TODO: Figure out a way to support this without slowing down the speed at which you can drag out cards
					// in sequence from your hand. 
					if ((isUnderDragThreshold == false || hoveredHandCard == null) && !Input.GetMouseButtonUp(0) && this.cardHoverFrameCounter != 1)
					{
						if(!this.PlayerHandController.IsUsingZoomHand
							|| !Input.GetMouseButton(0)
							|| isDragging
							|| (mouseDownTime.HasValue && (Time.timeSinceLevelLoad - this.mouseDownTime > 0.15f)))
						{
							cardMouseIsOver = hit.transform.GetComponentInParent<CardController> ();

                            // Is the hand card the mouse is over different than the hovered hand card?
                            if (cardMouseIsOver != this.hoveredHandCard)
                            {
                                this.PlayerHandController.MustSetZoomHandActiveTo = true;
                                Debug.LogFormat("Hovering card mouse is over {0}. hovered card: {1}", cardMouseIsOver != null ? cardMouseIsOver.CardIdAndName : "null", hoveredHandCard != null ? hoveredHandCard.CardIdAndName : "null");
                                if (hoveredHandCard != null)
                                    this.TriggerInput(InputEvent.MouseOutHandCard, this.hoveredHandCard);

                                this.mouseOverPos = Input.mousePosition;

                                this.TriggerInput(InputEvent.MouseOverHandCard, cardMouseIsOver);
                                this.hoveredHandCard = cardMouseIsOver;
                                this.cardHoverFrameCounter = 0; // Reset the hover frame counter for hand cards
                                this.lastHoveredHandCard = this.hoveredHandCard;
                            }
                        }
					}
				}
	            // Check for hitting anything other than a hand or arena card
	            else
				{
					// We hit nothing, let the arena unhover if needed
					if (this.hoveredArenaCard != null && (Input.GetMouseButton(0) == false || SelectedCard != null || DraggedCard != null))
					{
						this.UnHoverArenaCard ();
					}

					// De bug.LogFormat("hit:{0} isMouseOverHand:{1}",hit.tag, this.isMouseOverHand);

					if (hitTag != "card" && hitTag != "cardInsert")
					{
						// Is the mouse currently over hand RectTransform?

						// We are over the hand, but not over any card or insert
						if (this.hoveredHandCard != null)
						{
							float draggedDistance = this.mouseOverPos.HasValue ? Vector2.Distance (Input.mousePosition, this.mouseOverPos.Value) : 0;

							// HackHack: this frameCount thing, without it unity will sometimes raycast an not hit a card insert
							// after a card is hovered, even though the insert been created and positioned the same frame. This 
							// would cause the raycast to hit the player dock for one frame before the insert registered and blocked 
							// the raycast.
							// This caused cards to hover and unhover each frame (but only when running  in the main scene
							// when running in a seperate test scene this was no problem) :(
							if (this.cardHoverFrameCounter > 1)
							{
								if (Input.GetMouseButton (0) == false)
								{
									//Debug.LogFormat ("#Input#  MouseUpRaycast hit: {0}, {1}", hitTag, hit);
									// There was a hovered card in the hand, so let it know to unhover
									this.TriggerInput (InputEvent.MouseOutHandCard, this.hoveredHandCard);
									this.hoveredHandCard = null;
								}
								else if (Input.GetMouseButton (0) && draggedDistance > DRAG_UP_THRESHOLD)
								{
									this.TriggerInput (InputEvent.MouseOutHandCard, this.lastHoveredHandCard);
									this.hoveredHandCard = null;
								}
							}
						}
					}
					else if (hitTag == "cardInsert")
					{
						cardMouseIsOver = this.PlayerHandController.GetCardController ((RectTransform)hit.gameObject.transform.parent);
                        //Debug.LogErrorFormat("HOVERED HAND CARD CHANGED BY INSERT _ WHY??");
						//this.hoveredHandCard = cardMouseIsOver;
						//Debug.LogFormat("#Input# hitTag is 'cardInsert' and resulted in hoveredCard of {0} hand state is {1}", this.hoveredHandCard != null ? this.hoveredHandCard.TitleLabel.text : "null", this.PlayerHandController.HandState);
					}

/*#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
					if(this.SelectedCard != null && this.mouseDownTime.HasValue
						&& Input.touchCount > 0 
						&& Input.GetTouch(0).phase == TouchPhase.Ended) 
#else*/
					if (this.SelectedCard != null && this.mouseDownTime.HasValue && Input.GetMouseButtonUp (0))
//#endif
					{
						if (this.OpponentTarget.TargetAction == TargetAction.Activate && hit.transform.IsChildOf (this.OpponentTarget.transform))
						{
							//Debug.Log("Targeted ability clicked - at Opponent");
							this.SelectedCard.ActivateTarget (this.OpponentTarget, this.TargetArrowController);
                            this.TriggerInput(InputEvent.LostActivatedAbility, SelectedCard);
                        }
						else if (this.PlayerTarget.TargetAction == TargetAction.Activate && hit.transform.IsChildOf (this.PlayerTarget.transform))
						{
							//Debug.Log("Targeted ability clicked - at Player");
							this.SelectedCard.ActivateTarget (this.PlayerTarget, this.TargetArrowController);
                            this.TriggerInput(InputEvent.LostActivatedAbility, SelectedCard);
                        }
						else if (this.OpponentTarget.TargetAction == TargetAction.Attack && hit.transform.IsChildOf (this.OpponentTarget.transform))
						{
							//Debug.Log("Attack Opponent clicked");
							this.SelectedCard.AttackTarget (this.OpponentTarget, this.TargetArrowController);
                            this.TriggerInput(InputEvent.LostCombatAbility, SelectedCard);
                        }
					}
				}//</nothingHit>
			}//</raycastResults.Cout > 0>
	        else
			{
				if (this.hoveredHandCard != null)
				{
					this.TriggerInput (InputEvent.MouseOutHandCard, this.lastHoveredHandCard);
					this.hoveredHandCard = null;
				}
				// In the case where a raycastResult had zero hits would have to duplicate our hit anything 
				// other than a hand/arena card here except our raycasts always hit something because of the
				// arena background which is a rect transform that fills the whole screen.
			}

			// =============================
			// Mouse Down Events
			// =============================

/*#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) 
#else*/
			if (this.mouseDownTime.HasValue && Input.GetMouseButtonUp (0))
//#endif
        	{
				if(this.SelectedCard == null)
				{
					// We do the on mouse down tests AFTER the raycasts so we have knowlege
					// of what is being clickded / touched by the mouseDown event
					//Debug.LogFormat("#Input# MouseButton Down (Touch pressed) HoveredHandCard:{0}", (this.hoveredHandCard == null ? "null" : this.hoveredHandCard.TitleLabel.text));

					// Trigger MouseDownOpponentCard / MouseDownPlayerCard for Arena
					if (this.hoveredArenaCard != null)
					{
						this.HoverArenaCard (this.hoveredArenaCard);

						if (this.hoveredArenaCard.transform.IsChildOf (this.PlayerArenaController.transform))
						{
							// Only instruct the player arena controller to select a card if it is selectable
							if (this.hoveredArenaCard.CanBeSelected)
							{
								this.TriggerInput (InputEvent.MouseUpPlayerCard, this.hoveredArenaCard);
							}
						}
					}
				}
				else if(this.hoveredArenaCard != null)
				{
					//Debug.LogFormat ("#Input# Mouse Up on hovered arena card {0} with card selected {1}", this.hoveredArenaCard.CardId, this.SelectedCard.CardId);
					this.TriggerInput (InputEvent.MouseUp, this.hoveredArenaCard);
				}
			}


			// =============================
			// Mouse Dragging Events
			// =============================
			if (this.mouseDownTime.HasValue)
			{
				if (Input.GetMouseButton (0) && this.mouseOverPos.HasValue)
				{
					// De bug.LogFormat("#Input#SelectedCard: {0} IsDragging: {1}", selectedCard, this.PlayerHandController.IsDraggingCard);
					if (this.SelectedCard == null && this.PlayerHandController.IsDraggingCard == false)
					{
						// CardDraggedFromHand (for pulling cards out of the hand)
						float draggedY = Input.mousePosition.y - this.mouseOverPos.Value.y;
						float draggedX = Input.mousePosition.x - this.mouseOverPos.Value.x;
						Vector2 mouseVector = ((Vector2)Input.mousePosition) - this.mouseOverPos.Value;
						if (draggedX < 0)
						{
							mouseVector = this.mouseOverPos.Value - ((Vector2)Input.mousePosition);
						}

						// ensure that only upward drags are registered by multiplying by the -ve of the mousevector y direction
						float angle = Vector2.Angle (mouseVector, Vector2.right);// * mouseVector.y > 0 ? -1 : 1;
						float dragDistance = new Vector2 (draggedX, draggedY).magnitude;

						if (this.IsArrowDragging == false && dragDistance > DRAG_ARROW_THRESHOLD && hoveredArenaCard != null)
						{
							this.TriggerInput (InputEvent.MouseUpPlayerCard, this.hoveredArenaCard);
						}
						//Debug.LogFormat ("#InputEvent# DragAngle: {0} distance: {1}, hovered arena card:{2}", angle, dragDistance, hoveredArenaCard != null ? "not null" : "null");

						if (dragDistance > DRAG_UP_THRESHOLD)
						{
							if (angle > 30f)
							{
								//Debug.LogFormat ("angle above 30: {0}", angle);
								//Debug.Log ("Card dragged more than drag threshold.");
								if (this.hoveredHandCard != null && this.IsCardCastable (this.hoveredHandCard))
								{
									//Debug.LogFormat ("triggering drag card input for {0}", this.hoveredHandCard == null ? "null" : this.hoveredHandCard.name);
									this.TriggerInput (InputEvent.CardDraggedUp, this.hoveredHandCard);
									this.PlayerHandController.MustSetZoomHandActiveTo = false;
								}
								else
								{
									//Debug.LogFormat ("Card problem: {0}", this.hoveredHandCard == null ? "null" : "not castable");
								}

								// Because we possibly blocked a mouse out hand event earlier because we were dragging
								// we'll have to send it now to get the state machines caught up
								if (isOverHand == false)
								{
									isMustTriggerMouseOutHand = true;
									//Debug.LogFormat("#Input# Must trigger mouse out 2");
								}
							}
						}
						/*if(this.PlayerHandController.IsUsingZoomHand && angle < -30f)
						{
							//Debug.LogFormat ("angle below 30: {0}", angle);
							if( Input.GetButtonUp(0))
							{
								this.TriggerInput (InputEvent.CardDraggedUp, this.hoveredHandCard);
							}
						}*/
					}
					else
					{
						// ArrowDragged (for dragging target arrows out of the selecte arena card)
						float dragDistance = Vector2.Distance (Input.mousePosition, this.mouseOverPos.Value);
						if (dragDistance > DRAG_ARROW_THRESHOLD)
						{
							// Only send a drag arrow event if the card can actually target something
							if (this.IsArrowDragging == false 
                                && this.SelectedCard != null
							  && (this.SelectedCard.CanBlock
							  || this.SelectedCard.CanAttack
							  || (this.SelectedCard.CanActivate && this.SelectedCard.CanTargetAbility)))
							{
								this.TriggerInput (InputEvent.ArrowDragged, this.SelectedCard);
								this.UnHoverArenaCard ();
                            }

                            if (this.PlayerHandController.IsUsingZoomHand && PlayerHandController.HandState == HandState.CardSelected 
                                && this.PlayerHandController.IsHandZoomedIn && !this.PlayerHandController.IsZoomingIn && !this.PlayerHandController.IsZoomingOut)
                            {
                                Vector2 mouseVector = ((Vector2)Input.mousePosition) - this.mouseOverPos.Value;
                                float draggedX = Input.mousePosition.x - this.mouseOverPos.Value.x;
                                float draggedY = Input.mousePosition.y - this.mouseOverPos.Value.y;
                                if (draggedX < 0)
                                {
                                    mouseVector = this.mouseOverPos.Value - ((Vector2)Input.mousePosition);
                                }

                                // ensure that only upward drags are registered by multiplying by the -ve of the mousevector y direction
                                float angle = Vector2.Angle(mouseVector, Vector2.right);// * mouseVector.y > 0 ? -1 : 1;
                                if(angle > 30f && draggedY > 0)
                                {
                                    Debug.LogFormat("Resetting zoom. Mouseposition: {0}, mouseoverpos: {1}, angle:{2} dragdistance:{3}, draggedX:{4}, draggedY:{5}", 
                                        Input.mousePosition.ToString(), this.mouseOverPos.Value.ToString(), angle, dragDistance, draggedX, draggedY);
                                    this.PlayerHandController.MustSetZoomHandActiveTo = false;
                                }
                            }
                        }
					}
				}
			}

			// =============================
			// Mouse Up/Click Events
			// =============================
/*#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) 
#else*/
			if (this.mouseDownTime.HasValue && Input.GetMouseButtonUp (0))
//#endif
			{
				// Trigger zoom hand from clicks
				if (Time.timeSinceLevelLoad - this.mouseDownTime < 0.15f)
				{
					//Debug.Log ("#Input#Would do mouse click");
					// MouseClick
					if (this.isMouseOverHand == false || this.IsCardCastable (this.hoveredHandCard))
					{
						this.TriggerInput (InputEvent.MouseClick, null);
					}
				}
				//Debug.Log("#Input#Touch released");
				this.mouseDownTime = null;
				this.arenaCardHoveredTime = Time.unscaledTime;
				if (cardMouseIsOver != null)
				{
					// Trigger MouseClick if the time is short enough
					if (Time.timeSinceLevelLoad - this.mouseDownTime < 0.15f)
					{
						//Debug.Log ("#Input#Trigger input MouseClickCard");
						// MouseClickCard
						//this.TriggerInput(InputEvent.MouseClickCard, cardMouseIsOver);
					}
                
					// MouseUpCard - register a click or a mouse up hand, as appropriate.
					if (this.isMouseOverHand == false
#if !UNITY_IOS && !UNITY_ANDROID
						|| (this.PlayerHandController.SelectedCard == null && this.IsCardCastable (cardMouseIsOver))
#endif
					)
					{
						this.TriggerInput (InputEvent.MouseUpCard, cardMouseIsOver);
						//Debug.Log("#Input#Trigger input MouseUpCard. State: {0}");
					}
                    else //if (isOverHand)
                    {
                        //Debug.Log("#Input#Would do mouse Mouse Up Hand");
                        this.TriggerInput(InputEvent.MouseUpHand, this.hoveredHandCard == null ? this.lastHoveredHandCard : this.hoveredHandCard);
                    }
				}
				else
				{
					// Trigger MouseClick if the time is short enough
					if (Time.timeSinceLevelLoad - this.mouseDownTime < 0.15f)
					{
						//Debug.Log ("#Input#Would do mouse click");
						// MouseClick
						if (this.isMouseOverHand == false || this.IsCardCastable (this.hoveredHandCard))
						{
							this.TriggerInput (InputEvent.MouseClick, null);
						}
					}
            
					// MouseUpArena
					if (isOverHand == false)
					{
						//Debug.Log("#Input#Would do mouse Mouse Up Arena");
						CardController activeCard = this.DraggedCard;
						if (activeCard == null)
						{
							activeCard = this.hoveredArenaCard;
						}
						if (activeCard == null)
						{
							activeCard = this.hoveredHandCard;
						}

						this.TriggerInput (InputEvent.MouseUpArena, activeCard);
					}
					else
					{
						//Debug.Log("#Input#Would do mouse Mouse Up Hand");
						this.TriggerInput (InputEvent.MouseUpHand, this.hoveredHandCard == null ? this.lastHoveredHandCard : this.hoveredHandCard);
					}
                
					// TODO: Would be awesome to detect mouseUp events outside the window for PC builds.
					//isMustTriggerMouseOutHand = true;
				    //Debug.LogFormat("#Input# Must trigger mouse out 3");
				}
			}
		}
		/*else
		{
			if (this.hoveredHandCard != null)
			{
				this.TriggerInput (InputEvent.MouseOutWindow, this.lastHoveredHandCard);
				this.hoveredHandCard = null;
			}
			// In the case where a raycastResult had zero hits would have to duplicate our hit anything 
			// other than a hand/arena card here except our raycasts always hit something because of the
			// arena background which is a rect transform that fills the whole screen.
		}*/

		if(isMustTriggerMouseOutHand)// && this.DraggedCard != null || this.hoveredHandCard != null)
		{
			this.TriggerInput(InputEvent.MouseOutHand, this.DraggedCard != null ? this.DraggedCard : this.hoveredHandCard);
			this.isMouseOverHand = false;
		}
    }

    /// <summary>
    /// Sometimes we need to not re-hover rigiht away. This lets an external caller
    /// reset the internal timer for zoom-in's
    /// <summary>
    public void ResetZoomInTimer(int cardId)
    {
        if(this.hoveredArenaCard == null || this.hoveredArenaCard.CardId == cardId)
        {
            this.arenaCardHoveredTime = null;
        }
    }

    private void HoverPlayer(TargetArrowTarget playerTarget)
    {
        this.hoveredPlayerTarget = playerTarget;
        this.hoveredPlayerTarget.TargetGlowAnimator.SetBool("IsHovered", true);
    }
    
    private void UnhoverPlayer()
    {
        if(this.hoveredPlayerTarget != null)
        {
            this.hoveredPlayerTarget.TargetGlowAnimator.SetBool("IsHovered", false);
            this.hoveredPlayerTarget = null;
        }
    }

    /// <summary>
    /// Utility function that updates state and appearance for hovering
    /// a card in the arena (players card or opponents)
    /// </summry>
    private void HoverArenaCard(CardController card)
    {
        if(card.transform.localRotation.y != 0)
        {

        }
        this.hoveredArenaCard = card;
        //Debug.LogWarning("HoverArenaCard " + this.hoveredArenaCard.TitleLabel.text);
        this.hoveredArenaCard.CardGlowAnimator.SetBool("IsHovered", true);
        this.mouseOverPos = Input.mousePosition;
        this.arenaCardHoveredTime = Time.unscaledTime;
        this.ArenaCardZoom.RecycleZoomedCard();
    }
    
    /// <summary>
    /// Utility function that unhovers a prepviously hovered card
    /// in the arena and cleans up state.
    /// </summry>
    private void UnHoverArenaCard()
    {
        if(this.hoveredArenaCard != null)
        {
            // tap-tap arrow dragging for PC platforms
#if !UNITY_IOS && !UNITY_ANDROID
            if (hoveredArenaCard == SelectedCard && (SelectedCard.CanBlock
                || SelectedCard.CanAttack
                || (SelectedCard.CanActivate && SelectedCard.CanTargetAbility)))
            {
                //Debug.Log("Mouse Out Source Card");
                TriggerInput(InputEvent.MouseOutSourceCard, SelectedCard);
            }
#endif
            //Debug.LogWarning("UnhoverArenaCard " + this.hoveredArenaCard.TitleLabel.text);
            ((CardFrontController)this.hoveredArenaCard).OutlineCanvasGroup.alpha = 0.0f;
            this.hoveredArenaCard.CardGlowAnimator.SetBool("IsHovered", false);
            this.mouseOverPos = null;
            this.arenaCardHoveredTime = null;
            this.hoveredArenaCard = null;
            this.ArenaCardZoom.RecycleZoomedCard();
        }
    }
    
    private bool IsCardCastable(CardController card)
    {
		if(card == null || this.GameState == null || (GameState.Turn != null && GameState.Turn.TicksRemaining <= 0))
		{
			// if there's less than 0 ticks remaining in the turn... no card is castable
            return false;
        }
        else
        {
            return this.GameState.LegalAbilities.CanCast(card.CardId);
        }
    }
    
    /// <summary>
    /// Routes the InputEvent to the correct controllers based on the current state
    /// of the Controller.
    /// </summary>
    private void TriggerInput(InputEvent eventType, CardController cardController)
    {
        /*if(eventType == InputEvent.MouseOutHandCard || eventType == InputEvent.MouseOutHand || eventType == InputEvent.MouseOverHandCard)
        {
            Debug.LogFormat("#TRACE# Input type: {0} for {1}", eventType, cardController == null ? "null" : cardController.CardIdAndName);
        }
		if(cardController != null)
		{
			//Debug.LogFormat ("#Input# {0} for {1}{2}", eventType, cardController.name, cardController.CardId);
		}
		else
		{
			//Debug.LogFormat ("#Input# {0} for no card", eventType);
		}*/
        // If a card is dragging, don't do arena events
        if(this.PlayerHandController.IsDraggingCard == false)
        {
            this.PlayerArenaController.TriggerInput(eventType, cardController);
        }
        
		if (this.PlayerHandController.IsZoomingIn == false
		         || eventType == InputEvent.CardDraggedUp
				|| eventType == InputEvent.MouseOutHand 
				|| eventType == InputEvent.MouseOutWindow)
		{
			// If the players hand is using the HandZoom feature only send mouse up hand event
			if (this.PlayerHandController.IsUsingZoomHand)
			{  
				bool isOverWindow = RectTransformUtility.RectangleContainsScreenPoint ((RectTransform)this.ArenaIUCanvas.transform, Input.mousePosition, this.uiCamera);
				//Debug.LogFormat ("ZoomHand is over window {0} event type: {1}", isOverWindow, eventType);
				if (this.PlayerHandController.IsHandZoomedIn == false
				                && isOverWindow
				                && (eventType == InputEvent.MouseOverHand || eventType == InputEvent.MouseUpHand)) // added mouse over hand
					//&& (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer)) 
				{
					this.PlayerHandController.MustSetZoomHandActiveTo = true;
				}
				else if (this.PlayerHandController.IsHandZoomedIn
#if UNITY_IOS || UNITY_ANDROID
                    && !this.PlayerHandController.IsZoomingIn
#endif
				                    && (eventType == InputEvent.MouseOutHand
				                    || eventType == InputEvent.MouseUpArena 
									|| eventType == InputEvent.MouseOutWindow))
				{
					//Debug.LogFormat ("#Input#Deactivating player hand from InputEventController because of event {0}", eventType);
					this.PlayerHandController.MustSetZoomHandActiveTo = false;
				}
			}
			else
			{
				// Not using zoomhand, but maybe we switchd from using it to not using it like we do
				// when we go for 16:9 aspect ratio to 4:3
				if (this.PlayerHandController.IsHandZoomedIn)
				{
					this.PlayerHandController.MustSetZoomHandActiveTo = false;
				}
			}
		}

        // if the arena has a selected card, don't do hand actions
        if (this.SelectedCard == null || this.SelectedCard.RaycastTag != "arenaCard")
        {
            // If an arrow is dragging, don't do hand events 
            // (unless maybe an arrow is being dragged from the hand) -DMac
            if (this.IsArrowDragging == false)
            {
                //Debug.LogFormat("not arrow dragging {0}", eventType);
                if (this.PlayerHandController.IsZoomingOut == true && eventType == InputEvent.MouseOverHandCard)
                {
                    // Don't hover hand cards when the hand is zooming out
                }
                else
                {
                    this.PlayerHandController.TriggerInput(eventType, cardController);
                }
            }
            else if (eventType == InputEvent.MouseClick
                || eventType == InputEvent.MouseUpArena
                || eventType == InputEvent.MouseUpCard
                || eventType == InputEvent.MouseOverHand
                || eventType == InputEvent.MouseOutWindow)
            {
                Debug.LogFormat("is arrow dragging {0}", eventType);
                // If it is dragging, make sure it gets mouse events (but not card events)
                this.PlayerHandController.TriggerInput(eventType, cardController);
            }
            else
            {
                Debug.LogFormat("suppressed. is arrow dragging {0}", eventType);
            }
        }
    }
}
