﻿using UnityEngine;
using System.Collections;

public class CardBackController : CardController
{

    // Pool state
    public override PooledObjectType PooledObjectType { get { return PooledObjectType.CardBackController; } }
    public override string CardIdAndName
    {
        get
        {
            return string.Format("id:{0}:CardBack", CardId);
        }
    }
    public override void OnRecycle()
    {
        base.OnRecycle();
        Canvas.overrideSorting = true;
        CanvasGroup.blocksRaycasts = false;
        CanvasGroup.interactable = false;
        CanvasGroup.alpha = 0;
        CardBack.texture = null;
        //cardBack.Canvas.overrideSorting = false;
        Canvas.sortingLayerName = "Default";
        SortingOrder = 0;
    }
}
