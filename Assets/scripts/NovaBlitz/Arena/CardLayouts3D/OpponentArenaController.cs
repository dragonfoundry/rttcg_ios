﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using LinqTools;
using UnityEngine.EventSystems;
using strange.extensions.mediation.impl;
using Messages;
using DragonFoundry.GameState;
using NovaBlitz.Game;
using System;

public class OpponentArenaController : View
{
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public ArenaPlayers ArenaPlayers {get;set;}

    private Camera uiCamera;
    public ArenaLayout ArenaLayout;
    public PlayerHandController PlayerHandController;
    public PlayerArenaController PlayerArenaController;
    public RectTransform InsertTransform;
    public TargetArrowController TargetArrowController;
    public RectTransform DragArrowsRoot;
    public InputEventController InputEventController;
    public NovaAudioPlayer PlayCardSound;


    private Transform insertsRoot;
    private CardController cardBeingPlayed = null;
    private Queue<RectTransform> insertTransforms = new Queue<RectTransform>();
    private BiDictionary<int, RectTransform> draggedCardInserts = new BiDictionary<int, RectTransform>();
    private CardController draggedCard;
    

    
    // State transition data
    private Dictionary<InputEvent, bool> inputEvents = new Dictionary<InputEvent,bool>();
    private Dictionary<InputEvent, CardController> inputCards = new Dictionary<InputEvent,CardController>();

    private List<Transition> transitions = new List<Transition>();
    private ArenaState currentState = ArenaState.DefaultState;
    
    private enum ArenaState
    {
        DefaultState = 0,
    }
    
    class Transition
    {
        public Transition(ArenaState fromState, InputEvent inputEvent, ArenaState toState, System.Action<CardController> transitionFunction = null)
        {
            this.FromState = fromState;
            this.ToState = toState;
            this.InputEvent = inputEvent;
            this.TransitionFunction = transitionFunction;
        }
        public ArenaState FromState;
        public ArenaState ToState;
        public InputEvent InputEvent;
        public System.Action<CardController> TransitionFunction;
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        
        this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();

        // Create a new root transform for ineserts
		this.insertsRoot = new GameObject().transform;
		this.insertsRoot.name = "InsertsFor_" + this.name;
                
        // Attach the default insert to the new root
		this.InsertTransform.SetParent(this.insertsRoot);
		this.InsertTransform.gameObject.SetActive(false);
        
        this.insertTransforms.Enqueue(this.InsertTransform);
		this.InsertTransform.name = "Insert 0";
		
        // Create a few copies of the default insert
		for(int i=1;i<20;i++)
		{
			AddInsertTransform ();
		}
	}

	void AddInsertTransform()
	{
		RectTransform trans = GameObject.Instantiate(this.InsertTransform);
		trans.SetParent(this.transform);
		trans.localScale = Vector3.one;
		trans.localPosition = Vector3.zero;
		trans.name = "Insert " + insertTransformNumber;
		this.insertTransforms.Enqueue(trans);
		trans.SetParent(this.insertsRoot);
		insertTransformNumber++;
	}

	int insertTransformNumber = 1;

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (insertsRoot != null)
            Destroy(insertsRoot.gameObject);
    }
  
    // Update is called once per frame
    void Update()
    {
        // Dragged card insert updating
        if(this.draggedCard != null && this.draggedCard != this.cardBeingPlayed)
        {
            RectTransform insert = this.draggedCardInserts[this.draggedCard.CardId];
            Vector2 cardScreenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, draggedCard.transform.position);
            
            Vector3 worldPos = Vector3.zero;
            RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)this.transform, cardScreenPoint, Camera.main, out worldPos);
            int siblingIndex = this.ArenaLayout.CalculateSiblingIndex(worldPos);
            if(siblingIndex != insert.GetSiblingIndex())
            {
                insert.SetSiblingIndex(siblingIndex);
                //this.ArenaLayout.UpdateCardTweens();
            }
        }
     
        // =============================
        // State Transition Pump
        // =============================
        if(this.inputEvents.Count > 0)
        {
            // Perform State Transitions
            foreach(InputEvent inputEvent in this.inputEvents.Keys)
            {
                bool isEventSet = false;
                if(this.inputEvents.TryGetValue(inputEvent, out isEventSet))
                {
                    if(isEventSet)
                    {
                        CardController activeCard = null;
                        this.inputCards.TryGetValue(inputEvent, out activeCard);
                        this.currentState = this.DoStateTransition(this.currentState, inputEvent, activeCard);
                    }
                }
            }
            this.inputEvents.Clear();
        }
    }

    public void OnTargetedActivatedAbility(CardFrontController activatdCard, int targetId)
    {
        if(targetId != 0)
        {
            CardController targetCard = this.ArenaCards.GetCard(targetId);
            TargetArrowTarget target = null;
            
            if(targetCard != null )
            {
                target = this.ArenaCards.GetCard(targetId).TargetArrowTarget;
            }
            else
            {
                target = this.ArenaPlayers.GetPlayer(targetId).TargetArrowTarget;
            }

            // Make the staring position of the target arrow from the center of the activated ability card (for tweening)
            Vector3 startPos = activatdCard.transform.position + new Vector3(0,0,-1f);

            // Make a curved Ability Arrow
            Transform arrowParent = this.DragArrowsRoot;
            TargetArrowInfo arrowInfo = this.TargetArrowController.RequestTargetArrowInfo(activatdCard.transform, startPos, arrowParent);
            arrowInfo.ArrowType = ArrowType.Curved;
            arrowInfo.StartingColor = new Color(228/255f,147/255f,254/255f,255/255f);

            // Tween the tip of the arrow over to the target
            Sequence seq = DOTween.Sequence();
            seq.Append(
                DOTween.To(()=>arrowInfo.Destination, pos=>arrowInfo.Destination=pos, target.BoundsRect.position, 0.3f )
            );
             seq.AppendCallback(()=>{
                    Debug.LogFormat("OPPONENT Activated Ability of {0}", activatdCard == null ? "null" : activatdCard.CardIdAndName);
                    arrowInfo.FollowDestination = target.BoundsRect;
                });
            seq.AppendInterval(2.0f);
            seq.AppendCallback(()=>{
                    this.TargetArrowController.FadeOutAndReleaseTargetArrow(arrowInfo,0.5f);
                });
           
            seq.Play();
        }
    }

   
    
    public void OnCardDragBegin(CardController cardController)
    {
        RectTransform insert = this.insertTransforms.Dequeue();
        insert.gameObject.SetActive(true);
        int siblingIndex = this.ArenaLayout.CalculateSiblingIndex(cardController.transform.position, this.ArenaLayout.transform.childCount+1);
        //int sortOrder = Mathf.Max(1, siblingIndex);
        //Debug.LogFormat("Card drag begin. Arena Sibling Index is:{0}", siblingIndex);
        insert.AttachToParent(this.transform, siblingIndex);
        this.draggedCardInserts[cardController.CardId] = insert;
        insert.localScale = new Vector3(1f, 1f, 1f);
        Vector3 position = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, 0);
        insert.localPosition = position;
        this.draggedCard = cardController;
    }

    public void OnCardDragEnd()
    {
        RectTransform insert = null;
        if(this.draggedCardInserts.TryGetValue(this.draggedCard.CardId, out insert))
        {
            this.draggedCardInserts.Remove(insert);
            insert.DOKill();
            insert.AttachToParent(this.insertsRoot, 0);
            insert.gameObject.SetActive(false);
            this.insertTransforms.Enqueue(insert);
        }
        this.draggedCard = null;
       
    }
    
    
    
    public void SpawnArenaCardFromTrash(CardController spawnCard)
    {
        // Get an insert to use for the spawned card (on the far right of the arena)
        RectTransform currentInsert = this.insertTransforms.Dequeue();
        currentInsert.gameObject.SetActive(true);
        int siblingIndex = this.transform.childCount;

        Debug.LogFormat("#OpponentArena# SpawnArenaCardFromTrash Insert dequeued for spawnCard {0} with insert -{1}; {2}", spawnCard.CardIdAndName, currentInsert.gameObject.name, currentInsert.gameObject);
        
        // Position the insert in the arena
        currentInsert.AttachToParent(this.transform, siblingIndex, overrideSorting: false);
        currentInsert.transform.localScale = Vector3.one;
        currentInsert.localPosition = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, 0);
        
         // Attached the spawned card to the insert
        spawnCard.transform.AttachToParent(currentInsert, siblingIndex, overrideSorting: true, blocksRaycasts:false );
        spawnCard.transform.localScale = new Vector3(1.6f,1.6f,1.6f);
        spawnCard.transform.localPosition = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, -500);

        // Move any other cards in the arena to new positions
        this.ArenaLayout.UpdateCardTweens();
        
        // Animate the spanwed card into place
        spawnCard.CanvasGroup.alpha = 0;
        spawnCard.CanvasGroup.DOFade(1f, 0.1f);
        spawnCard.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        spawnCard.transform.DORotate(Vector3.zero, 0.2f);
        spawnCard.transform.DOLocalMove(Vector3.zero, 0.2f).OnComplete(()=>
        {
            spawnCard.RaycastTag = "arenaCard";
            spawnCard.transform.AttachToParent(currentInsert.parent, currentInsert.GetSiblingIndex(), overrideSorting: false, blocksRaycasts:true);
            spawnCard.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                
            // Return the insert to the inserts pool
            currentInsert.AttachToParent(this.insertsRoot, 0);
            currentInsert.gameObject.SetActive(false);
            this.insertTransforms.Enqueue(currentInsert);
            this.ArenaLayout.UpdateCardTweens();
            this.PlayerHandController.ShowCastTargets();
            this.PlayerArenaController.RefreshTargets();
            Debug.LogFormat("#OpponentArena# SpawnArenaCardFromTrash Insert returnedfor spawnCard {0} with insert-{1};{2}", spawnCard.CardIdAndName, currentInsert.gameObject.name, currentInsert.gameObject);
        });
    }
    
    public void SpawnArneaCardFromSourceCard(CardController spawnCard, int sourceId)
    {
        // If the source card is currently doing its "Play Card" tween, this waits for it to finish
        // to spawn the new card
        this.StartCoroutine(this.SpawnCardAnimation(spawnCard, sourceId));
    }
    
    private IEnumerator SpawnCardAnimation(CardController spawnCard, int sourceId)
    {
        CardController sourceCard = this.ArenaCards.GetCard(sourceId);

        if(sourceCard == null)
        {
            this.ArenaCards.CardsWaitingToBeRecycled.TryGetValue(sourceId, out sourceCard);
        }

        float startTime = Time.realtimeSinceStartup;

        yield return new WaitForSecondsRealtime(0.05f);
        // If we can't come up with a sourceCard after some time... just put the spawnCard in the arena...
        while (sourceCard == null || (draggedCard != null && sourceCard.CardId == this.draggedCard.CardId))
        {
            yield return new WaitForSecondsRealtime(0.05f);
            sourceCard = this.ArenaCards.GetCard(sourceId);

            // only wait for a short while before continuing on without a source card
            if (Time.realtimeSinceStartup - startTime > 1.5f )
            {
                break;
            }
        }

        // Get an insert to use for the spawned card
        RectTransform currentInsert = this.insertTransforms.Dequeue();
        currentInsert.gameObject.SetActive(true);
        int siblingIndex = sourceCard != null ? sourceCard.transform.GetSiblingIndex() +1 : 0;
        


        // Position the insert in the arena
        currentInsert.AttachToParent(this.transform, siblingIndex);
        currentInsert.transform.localScale = Vector3.one;
        currentInsert.localPosition = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, 0);;
        
        // Attached the spawned card to the insert
        spawnCard.transform.AttachToParent(currentInsert, siblingIndex, overrideSorting: true, blocksRaycasts:false );
        spawnCard.transform.localScale = new Vector3(1.6f,1.6f,1.6f);
        var xPos = this.ArenaLayout.GetXPositionForSibling(siblingIndex + 1);
        spawnCard.transform.localPosition = new Vector3(xPos, 0, -200);
        
        Debug.LogFormat("Spawning opponent card {0} in arena to xPos:{1}", spawnCard.CardIdAndName,xPos);
        
        // Move any other cards in the arena to new positions
        this.ArenaLayout.UpdateCardTweens();

        spawnCard.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        spawnCard.transform.DORotate(Vector3.zero, 0.2f);
        // Animate the spanwed card into place
        spawnCard.transform.DOLocalMoveX(0f, 0.2f).OnComplete(()=>
        {
            spawnCard.RaycastTag = "arenaCard";
            spawnCard.transform.AttachToParent(currentInsert.parent, currentInsert.GetSiblingIndex(), overrideSorting: false, blocksRaycasts:true);
            spawnCard.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            currentInsert.AttachToParent(this.insertsRoot, 0);
            currentInsert.gameObject.SetActive(false);
            this.insertTransforms.Enqueue(currentInsert);
            this.ArenaLayout.UpdateCardTweens();
            this.PlayerHandController.ShowCastTargets();
            this.PlayerArenaController.RefreshTargets();
        });
    }

    private IEnumerator waitToLayoutCoroutine = null;

    public void OnPlayCard(CardController cardController)
    {   
        cardController.DOKill();

        // Get a reference to the  Insert
        RectTransform insert = this.draggedCardInserts[cardController.CardId];

        // Calculate the screen position of the card
        Vector2 cardScreenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, cardController.transform.position);
        
        // Get the world position of the screenPoint relative to the arena layouts transform
        Vector3 worldPos = Vector3.zero;
        RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)this.transform, cardScreenPoint, Camera.main, out worldPos);

        // Use that world pos to find the sibling index that would position the insert below the card
        int siblingIndex = this.ArenaLayout.CalculateSiblingIndex(worldPos);
        if(siblingIndex != insert.GetSiblingIndex())
        {
            insert.SetSiblingIndex(siblingIndex);
        }

        // Make sure the insert is located at that position
        Vector3 position = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, 0);
        insert.localPosition = position;


        cardController.transform.AttachToParent(insert.transform, 0, sortingOrder:1);
        cardController.transform.localScale = new Vector3(1.5f,1.5f,1f);
        cardController.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        cardController.transform.DORotate(Vector3.zero, 0.2f);

        // then make sure the other arena cards get out of the way (if we arn't currently hovered over a target)
        if (this.waitToLayoutCoroutine == null)
        {
            this.waitToLayoutCoroutine = this.WaitToLayoutArena();
            this.StartCoroutine(this.waitToLayoutCoroutine);
        }

        this.StartCoroutine(this.WaitToPlayCard(cardController));
    }

    private IEnumerator WaitToPlayCard(CardController cardController)
    {
        Vector3 cameraDirection = this.uiCamera.transform.position - cardController.transform.parent.position;
        cameraDirection.Normalize();

        // Move the card over the insert
        bool isInPosition = false;
        //cardController.transform.DOScale(Vector3.one*1.1f,0.5f).SetEase(Ease.OutCubic);
        cardController.transform.DOLocalMove( -cameraDirection * (cardController.transform.localPosition.z + 200f), 0.3f).SetEase(Ease.OutCubic).OnComplete(()=>{
             cardController.CardGlowAnimator.SetBool("IsPending", true);
             isInPosition = true;
        });
        Debug.LogFormat("#OpponentArena# Resolve begun: {0}", cardController.CardIdAndName);
        float stopTime = Time.timeSinceLevelLoad + 0.35f;
        while (isInPosition == false && Time.timeSinceLevelLoad < stopTime) { yield return null;}


        bool succeeded = true;

        ((CardFrontController)cardController).TargetArrowTarget.TargetGlowAnimator.SetBool("IsPending",false);
        
        if(succeeded)
        {
            //UnityEditor.EditorApplication.isPaused = true;
            Debug.LogFormat("#OpponentArena# Resolve succeeded: {0}", cardController.CardIdAndName);
            this.PlayCardSound.Play();
        
            cameraDirection = this.uiCamera.transform.position - cardController.transform.position;
            cameraDirection.Normalize();
            Vector3 cardPos =-cameraDirection * (cardController.transform.localPosition.z + 300f);

            // Get a reference to the  Insert
            RectTransform insert = this.draggedCardInserts[cardController.CardId];
            
            Sequence seq = DOTween.Sequence();
            seq.Append(cardController.transform.DOLocalMove(cardPos, 0.1f));
            //seq.Insert(0, cardController.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.05f));
            //seq.AppendInterval(0.05f);
            seq.Append(cardController.transform.DOLocalMove(Vector3.zero, 0.08f).SetEase(Ease.InCubic));
            seq.AppendCallback(() =>
                {
                    cardController.RaycastTag = "arenaCard";
                    /*if(this.InputEventController.IsArrowDragging && this.InputEventController.TargetArrowController.GetDraggedArrowTarget() != null)
                    {
                        cardController.transform.AttachToParent(this.transform,insert.GetSiblingIndex(), overrideSorting:true, sortingOrder: 200, blocksRaycasts:true );
                    }
                    else
                    {
                        cardController.transform.AttachToParent(this.transform,insert.GetSiblingIndex(), overrideSorting:false, sortingOrder: 0, blocksRaycasts:true );
                    }*/
					cardController.transform.AttachToParent(this.transform,insert.GetSiblingIndex(), overrideSorting:false, sortingOrder: 0, blocksRaycasts:true );
                    cardController.transform.localScale = new Vector3(1.5f, 1.5f, 1f);
                    ((CardFrontController)cardController).UpdateFlyingState();
                    this.draggedCardInserts.Remove(insert);
                    insert.DOKill();
                    insert.AttachToParent(this.insertsRoot, 0);
                    this.insertTransforms.Enqueue(insert);
                    insert.gameObject.SetActive(false);
                    if(this.waitToLayoutCoroutine == null)
                        this.ArenaLayout.UpdateCardTweens();

                    // If the player is current dragging a power, this will make sure
                    // the freshly played opponent card is targetable (if indeed it is)
                    this.PlayerHandController.ShowCastTargets();
                    this.PlayerArenaController.RefreshTargets();

                    /*if(this.draggedCard != null && this.draggedCard.CardId == cardController.CardId)
                    {*/
                    this.TriggerInput(InputEvent.CardDragEnd, this.draggedCard);
                    this.draggedCard = null;
                    Debug.Log("dragged card is nulled");
                    /*}
                    else
                    {
                        Debug.LogFormat("Dragged card not nulled. DraggedId:{0}, cardId:{1}", draggedCard == null ? -1 : draggedCard.CardId, cardController.CardId);
                    }*/
                });
            seq.Play();
        }
    }

    private IEnumerator WaitToLayoutArena()
    {
        yield return null;
        CardController[] arenaCards = this.ArenaLayout.GetComponentsInChildren<CardController>();
        for (int i = 0; i < arenaCards.Length; i++)
        {
            arenaCards[i].SortingOrder = 0;
            arenaCards[i].Canvas.overrideSorting = false;
            arenaCards[i].Canvas.sortingLayerName = "Default";
        }

        while (this.InputEventController.IsArrowDragging && this.InputEventController.TargetArrowController.GetDraggedArrowTarget() != null)
        {
            yield return null;
        }

        this.ArenaLayout.UpdateCardTweens();
        this.waitToLayoutCoroutine = null;
    }
    
    public void TriggerInput(InputEvent eventType, CardController cardController)
    {
        this.inputEvents[eventType] = true;
        this.inputCards[eventType] = cardController;
    }
    
    /// <summary>
    /// Handles moving from one state to another based on an InputEvent and a reference card.
    /// See the state transitions list set up in Start() for details
    /// </summary>
    private ArenaState DoStateTransition(ArenaState currentState, InputEvent inputEvent, CardController activeCard)
    {
        Transition currentTransition = this.transitions.Where(s=>s.FromState==currentState && s.InputEvent==inputEvent).FirstOrDefault();
        if(currentTransition != null)
        {
            Debug.LogFormat("#OpponentArenaTransition# {0}->({1})->{2} ({3})", currentState, currentTransition.InputEvent, currentTransition.ToState, activeCard == null ? "null" : activeCard.CardIdAndName);
            if(currentTransition.TransitionFunction != null)
            {
                currentTransition.TransitionFunction.Invoke(activeCard);
            }
            currentState = currentTransition.ToState;
        }
        return currentState;
    }
}
