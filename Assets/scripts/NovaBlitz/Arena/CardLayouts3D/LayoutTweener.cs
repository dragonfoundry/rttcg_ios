﻿using UnityEngine;
using System.Collections;

public class LayoutTweener : MonoBehaviour 
{
	public RectTransform TweenTargetRoot;
	private RectTransform rectTransform;

	void Awake()
	{
		this.rectTransform = (RectTransform)this.transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(this.rectTransform.GetSiblingIndex() <= this.TweenTargetRoot.childCount-1)
		{
			RectTransform targetRectTransform = (RectTransform)this.TweenTargetRoot.GetChild(this.rectTransform.GetSiblingIndex());
			Vector2 direction = this.rectTransform.anchoredPosition - targetRectTransform.anchoredPosition;

            //Debug.LogFormat("LayoutTweener Update() localIndex:{1} targetChidren:{2} distance:{3}", this.rectTransform.GetSiblingIndex(), this.TweenTargetRoot.childCount, direction.magnitude);
            if ( direction.magnitude > 0)
			{
				Vector2 unitDirection = direction.normalized;
				Vector2 anchorPos = targetRectTransform.anchoredPosition;
				Vector2 step = unitDirection * Time.deltaTime * 1000f;
				if(step.magnitude > direction.magnitude)
					step = direction;

				anchorPos.x += step.x;
				targetRectTransform.anchoredPosition = anchorPos;
			}
			else
			{
				targetRectTransform.anchoredPosition = this.rectTransform.anchoredPosition;
			}
		}
	}
}
