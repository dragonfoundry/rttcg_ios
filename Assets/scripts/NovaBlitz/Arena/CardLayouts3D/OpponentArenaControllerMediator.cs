﻿using UnityEngine;
using System.Collections;
using NovaBlitz.Game;
using Messages;
using DragonFoundry.GameState;
using System;
using DG.Tweening;

public class OpponentArenaControllerMediator : strange.extensions.mediation.impl.Mediator
{
    [Inject] public OpponentArenaController View {get;set;}
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public SpawnOpponentCardInArenaSignal SpawnOpponentCardInArenaSignal {get;set;}
    [Inject] public ArenaCardRecycledSignal ArenaCardRecycledSignal {get;set;}
    [Inject] public PhaseChangedSignal PhaseChangedSignal {get;set;}
    [Inject] public CardStateUpdatedSignal CardStateUpdatedSignal { get;set;}
    [Inject] public OpponentInputActionSignal OpponentInputActionSignal { get; set; }
    [Inject] public OpponentActivateAbilitySignal OpponentActivateAbilitySignal {get;set;}
    //[Inject] public OpponentResolvesUnitSignal OpponentResolvesUnitSignal {get;set;}
    
    private Phase turnPhase = Phase.NoPhase;
    
    override public void OnRegister()
    {
        this.SpawnOpponentCardInArenaSignal.AddListener(this.OnSpawnArenaCard);
        this.PhaseChangedSignal.AddListener(this.OnPhaseChanged);
        this.ArenaCardRecycledSignal.AddListener(this.OnCardRecycled);
        this.OpponentInputActionSignal.AddListener(this.OnOpponentInputAction);
        this.OpponentActivateAbilitySignal.AddListener(this.OnOpponentActivatedAbility);
        //this.OpponentResolvesUnitSignal.AddListener(this.OnOpponentPlaysUnit);
    }
    
    override public void OnRemove()
    {
        this.SpawnOpponentCardInArenaSignal.RemoveListener(this.OnSpawnArenaCard);
        this.PhaseChangedSignal.RemoveListener(this.OnPhaseChanged);
        this.ArenaCardRecycledSignal.RemoveListener(this.OnCardRecycled);
        this.OpponentInputActionSignal.RemoveListener(this.OnOpponentInputAction);
        this.OpponentActivateAbilitySignal.RemoveListener(this.OnOpponentActivatedAbility);
        //this.OpponentResolvesUnitSignal.RemoveListener(this.OnOpponentPlaysUnit);
    }

    /*private void OnOpponentPlaysUnit(ICardState cardState, GameStateReadOnly gameState)
    {
        Debug.LogFormat("#OpponentArena# Play Unit Succeeded :{0}", cardState);
        this.View.OnPlayUnitSucceeded(cardState);
    }*/

    private void OnOpponentActivatedAbility(ICardState activatedCardState, int targetId)
    {
        Debug.LogFormat("#OpponentArena# ActivatedCardID: {0} TargetId:{1}", activatedCardState.Id, targetId);
        CardFrontController activatedCard = (CardFrontController)this.ArenaCards.GetCard(activatedCardState.Id);
        activatedCard.EnqueueScrollingText(I2.Loc.ScriptLocalization.Activated_Ability,  new Color(228/255f,147/255f,254/255f,255/255f));
        this.View.OnTargetedActivatedAbility(activatedCard, targetId);
    }

    private void OnOpponentInputAction(Messages.InputAction inputAction)
    {
        // try to get the card in the hand
        // if it's there, hover it.
        // if it's not, unhover everything.
    }

    private void OnPhaseChanged(Phase turnPhase, int turnNumber, GameStateReadOnly gameState)
    {
        this.turnPhase = turnPhase;
        if(turnPhase == Phase.PreAction)
        {
            this.View.ArenaLayout.UpdateCardTweens();
        }
    }

    private void OnCardRecycled(int cardId, bool isPlayerCard, Phase phaseCardRecycled)
    {
        // If a card was trashed in action phase.. it needs to clean up in combat
        if(!isPlayerCard && phaseCardRecycled != Phase.Combat)
        {
            this.View.ArenaLayout.UpdateCardTweens();
        }

          // If a card was trashed in combat, but is cleaning up after...
        if(!isPlayerCard && this.turnPhase != Phase.Combat && phaseCardRecycled == Phase.Combat )
        {
            this.View.ArenaLayout.UpdateCardTweens();
        }
    }
    
    private void OnSpawnArenaCard(ICardState spawnCardState, int sourceCardId, CardZone fromZone, GameStateReadOnly gameState)
    {
        // Get a reference to the new card which has been created offscreen...
        CardFrontController newCard = (CardFrontController)this.ArenaCards.GetCard(spawnCardState.Id);
        
        // Strip off any pending aniamtions (and possibly some disasterous OnComplete animation handlers)
        newCard.transform.DOKill();
        if(newCard.AttackSequence != null)
        {
            Debug.Log("killing attack sequence");
            newCard.AttackSequence.Kill();
        }
        
		Debug.LogFormat("PlayerSpawnArenaCard {0} FromZONE:{1}", newCard.CardIdAndName, fromZone);
       
        newCard.CurrentDamage = 0;
        newCard.UpdateLabels(); 
        // Get a reference to the source card's cardState
        ICardState sourceCardState = gameState.GetCard(sourceCardId);
        
        switch (fromZone)
        {
            case CardZone.Void:
                if(sourceCardState != null && (sourceCardState.Zone == CardZone.Arena || sourceCardState.Zone == CardZone.Stack))
                {
                    // If the source card is in the arena or stack use it as a reference for the spawned
                    // cards spawn animation.
                    this.View.SpawnArneaCardFromSourceCard(newCard, sourceCardId);
                }
                else
                {
                    // If the source card isn't visible in the arena we can't use it to position our spawend 
                    // cards animation tween.
                    this.View.SpawnArenaCardFromTrash(newCard);
                }
            break;
            case CardZone.Trash:
                this.View.SpawnArenaCardFromTrash(newCard);
            break;
            default:
                this.View.SpawnArenaCardFromTrash(newCard);
            break;
        }

        this.CardStateUpdatedSignal.Dispatch(spawnCardState, gameState);
    }
}
