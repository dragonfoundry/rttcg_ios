﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.Game;
using DragonFoundry.GameState;
using Messages;
using NovaBlitz.UI;
using DG.Tweening;

public class OpponentHandControllerMediator : Mediator 
{
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public OpponentHandController View {get;set;}
    [Inject] public GameStateInitializedSignal GameStateInitializedSignal {get;set;}
    [Inject] public DealCardToOpponentStartingHandSignal DealCardToOpponentStartingHandSignal {get;set;}
    [Inject] public DiscardCardFromOpponentStartingHandSignal DiscardCardFromOpponentStartingHandSignal {get;set;}
    [Inject] public OpponentCastsCardSignal OpponentCastsCardSignal {get;set;}
    [Inject] public OpponentResolvesPowerSignal OpponentResolvesPowerSignal {get;set;}
    [Inject] public DealCardToOpponentHandSignal DealCardToOpponentHandSignal {get;set;}
    [Inject] public BanishOpponentCardSignal OpponentBanishCardSignal {get;set;}
    [Inject] public SpawnAndBanishOpponentCardSignal SpawnAndBanishOpponentCardSignal {get;set;}
    [Inject] public SummonOpponentCardToArenaSignal SummonOpponentCardToArenaSignal {get;set;}
    //[Inject] public OpponentResolvesUnitSignal OpponentResolvesUnitSignal {get;set;}
    [Inject] public CardBackRecycledSignal CardBackRecycledSignal {get;set;}
    [Inject] public DiscardOpponentCardSignal DiscardOpponentCardSignal {get;set;}
    [Inject] public GameData GameData { get; set; }
    [Inject] public OpponentInputActionSignal OpponentInputActionSignal { get; set; }
    [Inject] public PlayFxSignal PlayFxSignal { get; set; }


    private bool dealTweensStarted = false;
    
    override public void OnRegister()
    {
        this.View.TriggerFxEvent += OnPlayFx;
        this.DealCardToOpponentStartingHandSignal.AddListener(this.OnCardDealtToStartingHand);
        this.GameStateInitializedSignal.AddListener(this.OnGameStateInitialzied);
        this.DiscardCardFromOpponentStartingHandSignal.AddListener(this.OnCardDiscardedFromStartingHand);
        this.OpponentResolvesPowerSignal.AddListener(this.OnOpponentResolvesPower);
        this.OpponentCastsCardSignal.AddListener(this.OnOpponentCastsCard);
        this.OpponentBanishCardSignal.AddListener(this.View.OnCardBanished);
        this.SpawnAndBanishOpponentCardSignal.AddListener(this.View.OnCardSpawnedAndBanished);
        this.DealCardToOpponentHandSignal.AddListener(this.OnCardDeal);
        this.SummonOpponentCardToArenaSignal.AddListener(this.OnSummonCardToArena);
        this.CardBackRecycledSignal.AddListener(this.View.OnCardbackRecycled);
        this.DiscardOpponentCardSignal.AddListener(this.OnOpponentDiscardCard);
        this.OpponentInputActionSignal.AddListener(this.OnOpponentInputAction);
    }

    override public void OnRemove()
    {
        this.View.TriggerFxEvent -= OnPlayFx;
        this.DealCardToOpponentStartingHandSignal.RemoveListener(this.OnCardDealtToStartingHand);
        this.GameStateInitializedSignal.RemoveListener(this.OnGameStateInitialzied);
        this.DiscardCardFromOpponentStartingHandSignal.RemoveListener(this.OnCardDiscardedFromStartingHand);
        this.OpponentResolvesPowerSignal.RemoveListener(this.OnOpponentResolvesPower);
        this.OpponentCastsCardSignal.RemoveListener(this.OnOpponentCastsCard);
        this.OpponentBanishCardSignal.RemoveListener(this.View.OnCardBanished);
        this.SpawnAndBanishOpponentCardSignal.RemoveListener(this.View.OnCardSpawnedAndBanished);
        this.DealCardToOpponentHandSignal.RemoveListener(this.OnCardDeal);
        this.SummonOpponentCardToArenaSignal.RemoveListener(this.OnSummonCardToArena);
        this.CardBackRecycledSignal.RemoveListener(this.View.OnCardbackRecycled);
        this.DiscardOpponentCardSignal.RemoveListener(this.OnOpponentDiscardCard);
        this.OpponentInputActionSignal.RemoveListener(this.OnOpponentInputAction);
    }

    private void OnPlayFx(int cardId, RectTransform rect, string fxName)
    {
        this.PlayFxSignal.Dispatch(new PlayFxParams(cardId, cardId, null, rect, null, fxName, PlayFxPath.NoPath, 0));
    }

    private void OnOpponentInputAction(Messages.InputAction inputAction)
    {
        // try to get the card in the hand
        // if it's there, hover it.
        // if it's not, unhover everything.

    }

    private void OnOpponentResolvesPower(ICardState cardState, int targetId, GameStateReadOnly gameState)
    {
        this.View.OnResolvePower(cardState);
    }

    private void OnOpponentDiscardCard(int cardId, GameStateReadOnly gamestate)
    {
        Debug.LogFormat("#OpponentHand# Discarding opponent cardback with ID:{0}", cardId);

        if(this.View.IsCardIncomming(cardId))
        {
            this.StartCoroutine(this.DelayedDiscard(cardId));
        }
        else
        {
            this.View.OnOpponentCardDiscarded(cardId);
        }
    }

    private IEnumerator DelayedDiscard(int cardId)
    {
        Debug.LogFormat("#OpponentHand# Delay Discarding opponent cardback with ID:{0}", cardId);

        while(this.View.IsCardIncomming(cardId))
        {
            yield return new WaitForSecondsRealtime(0.1f);
        }
        Debug.Log("#OpponentHand# Delay over");
        this.View.OnOpponentCardDiscarded(cardId);
    }

    private void OnGameStateInitialzied(GameStateReadOnly gameState)
    {
        this.View.GameState = gameState;
        Debug.Log("Setting GameState in opponent hand controller");
    }

    private void OnSummonCardToArena(ICardState cardState, CardZone fromZone, GameStateReadOnly gameState)
    {
        if(fromZone == CardZone.Hand)
        {
            CardController cardBack = this.ArenaCards.GetCardBack(cardState.Id);
            this.View.PlayCardFromHand(cardState, cardBack);
            // If we're summoning, then we need to ensure it resolves automatically
            //this.OpponentResolvesUnitSignal.Dispatch(cardState, gameState);
        }
    }

    private void OnCardDeal(int cardId)
    {
        this.View.DealCardBackFromDeck(cardId);
    }

    private IEnumerator _CardDragCoroutine { get; set; }
    
    private void OnOpponentCastsCard(ICardState cardState, int targetId, GameStateReadOnly gameState)
    {
        Debug.LogFormat("#OpponentHand# OnOpponentCastsCard (in mediator) id: {0}", cardState.Id);
        CardController cardBack = this.ArenaCards.GetCardBack(cardState.Id);
        if(this.View.IsCardIncomming(cardState.Id) == false)
        {
            this.View.PlayCardFromHand(cardState, cardBack, targetId);
        }
        else
        {
            Debug.LogFormat("#OpponentHand# OnOpponentCastsCard (in mediator) cards is flagged as 'incoming' so doing nothing");
            _CardDragCoroutine = this.DelayedDrag(cardState, cardBack, targetId);
            this.StartCoroutine(_CardDragCoroutine);
        }
    }

    private IEnumerator DelayedDrag(ICardState cardState, CardController cardBack, int targetId)
    {
        while (this.View.IsCardIncomming(cardState.Id))
        {
            yield return new WaitForSecondsRealtime(0.1f);
        }
        this.View.PlayCardFromHand(cardState, cardBack, targetId);
    }

    /// <summary>
    /// Sometimes when the AI plays a card it doesn't take into account that the card may still be
    /// in the process of being dealt or returning to the hand after being banished from the arena. It
    /// may try to play the card before it reaches the hand in a way that a human player wouldn't be able to.
    /// This method does a delayed play that waits until the card arrives in the hand before playing it.
    /// </summary>
    private IEnumerator DelayedPlay(ICardState cardState, CardController cardBack, int cardId, int targetId)
    {
        //CardFrontController cardFront = (CardFrontController)this.ArenaCards.GetCard(cardState.Id);

        Debug.LogFormat("#OpponentHand#DelayedPlay for card ID: {0} initiated", cardId);
        
        // What until the incoming card is no longer incomming (presumably because it's arrived in the hand)
        while(this.View.IsCardIncomming(cardId) || _CardDragCoroutine != null)
        {
            yield return new WaitForSecondsRealtime(0.1f);
        }

        // Dragged the played cardBack over the arena
        this.View.PlayCardFromHand(cardState, cardBack, targetId);
    }

    private void OnCardDiscardedFromStartingHand(int cardID)
    {
        //Debug.Log("Discard opponent starting hand card");
        CardController cardBack = this.ArenaCards.RemoveCardBack(cardID);
        cardBack.transform.DOKill();
        cardBack.Recycle();
        //Timer.Instance.StartCoroutine(DiscardStartingCardOrWaitTen(cardID));
    }

    /*private IEnumerator DiscardStartingCardOrWaitTen(int cardID)
    {
        float stopTime = Time.timeSinceLevelLoad + 10; 
        CardController cardBack = this.ArenaCards.RemoveCardBack(cardID);
        while (cardBack == null)
        {
            yield return null;
            cardBack = this.ArenaCards.RemoveCardBack(cardID);
            if (Time.timeSinceLevelLoad > stopTime)
                break;
        }
        cardBack.transform.DOKill();
        cardBack.Recycle();
    }*/

    private void OnCardDealtToStartingHand(int cardID)
    {
        //Debug.Log("Deal opponent starting hand card");
        CardController cardBack = this.ArenaCards.GetOrCreateCardBack(cardID);
        cardBack.SetCardBack(this.GameData.CurrentGameData.OpponentCardBack);
        cardBack.transform.SetParent(this.View.HandLayout.transform);
        cardBack.transform.localScale = Vector3.one;
        cardBack.transform.position = this.View.CardDealer.transform.position;;
        
        if(this.dealTweensStarted == false)
        {
            this.StartCoroutine(DealCards());
            this.dealTweensStarted = true;
        }
    }

    private IEnumerator DealCards()
    {
        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSecondsRealtime(0.5f);
            Debug.LogFormat("Opponent Deal Cards. Waited {0}s", (i + 1) * 0.5f);
        }
        this.View.HandLayout.TweenInStartingHand(0.1f);
        this.dealTweensStarted = false;
    }
}
