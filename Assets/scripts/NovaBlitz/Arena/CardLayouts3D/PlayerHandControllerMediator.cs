﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.Game;
using DragonFoundry.GameState;
using Messages;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class PlayerHandControllerMediator : Mediator
{
    [Inject] public PlayerHandController View {get;set;}
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public GameStateInitializedSignal GameStateInitializedSignal {get;set;}
    [Inject] public DealCardToHandSignal DealCardToHandSignal {get;set;}
    [Inject] public GameControllerService GameControllerService {get;set;}
    [Inject] public LegalAbilitiesUpdatedSignal LegalAbilitiesUpdatedSignal {get;set;}
    [Inject] public PlayCardFailedSignal PlayCardFailedSignal {get;set;}
    [Inject] public PlayCardSucceededSignal PlayCardSucceededSignal {get;set;}
    [Inject] public PlayerResolvesPowerSignal PlayerResolvesPowerSignal {get;set;}
    [Inject] public BanishPlayerCardSignal BanishPlayerCardSignal {get;set;}
    [Inject] public SpawnAndBanishPlayerCardSignal SpawnAndBanishPlayerCardSignal {get;set;}
    [Inject] public SummonPlayerCardToArenaSignal SummonPlayerCardToArenaSignal {get;set;}
    [Inject] public DiscardPlayerCardSignal DiscardPlayerCardSignal {get;set;}
    [Inject] public TrashPlayerCardSignal TrashPlayerCardSignal {get;set;}
    [Inject] public PhaseChangedSignal PhaseChangedSignal {get;set;}
    [Inject] public PlayFxSignal PlayFxSignal { get; set; }
    
    override public void OnRegister()
    {
        this.View.TriggerFxEvent += OnPlayFx;
        this.DealCardToHandSignal.AddListener(this.OnCardDeal);
        this.GameStateInitializedSignal.AddListener(this.OnGameStateInitialzied);
        this.LegalAbilitiesUpdatedSignal.AddListener(this.View.OnLegalAbilitiesUpdated);
        this.SummonPlayerCardToArenaSignal.AddListener(this.OnSummonCardToArena);
        this.PlayCardFailedSignal.AddListener(this.View.OnPlayCardFailed);
        this.PlayerResolvesPowerSignal.AddListener(this.OnPlayerResolvesPower);
        this.BanishPlayerCardSignal.AddListener(this.View.OnCardBanished);
        this.DiscardPlayerCardSignal.AddListener(this.OnPlayerCardDiscarded);
        this.PhaseChangedSignal.AddListener(this.OnPhaseChanged);
        this.SpawnAndBanishPlayerCardSignal.AddListener(this.View.OnCardSpawnedAndBanished);
    }
    
    override public void OnRemove()
    {
        this.View.TriggerFxEvent -= OnPlayFx;
        this.DealCardToHandSignal.RemoveListener(this.OnCardDeal);
        this.GameStateInitializedSignal.RemoveListener(this.OnGameStateInitialzied);
        this.LegalAbilitiesUpdatedSignal.RemoveListener(this.View.OnLegalAbilitiesUpdated);
        this.SummonPlayerCardToArenaSignal.RemoveListener(this.OnSummonCardToArena);
        this.PlayCardFailedSignal.RemoveListener(this.View.OnPlayCardFailed);
        this.PlayerResolvesPowerSignal.RemoveListener(this.OnPlayerResolvesPower);
        this.BanishPlayerCardSignal.RemoveListener(this.View.OnCardBanished);
        this.DiscardPlayerCardSignal.RemoveListener(this.OnPlayerCardDiscarded);
        this.PhaseChangedSignal.RemoveListener(this.OnPhaseChanged);
        this.SpawnAndBanishPlayerCardSignal.RemoveListener(this.View.OnCardSpawnedAndBanished);
    }

    private void OnPlayFx(int cardId, RectTransform rect, string fxName)
    {
        this.PlayFxSignal.Dispatch(new PlayFxParams(cardId, cardId, null, rect, null, fxName, PlayFxPath.NoPath, 0));
    }

    private void OnPlayerResolvesPower(ICardState cardState, int targetId, GameStateReadOnly gameState)
    {
        this.View.OnPlayCardSucceeded(cardState);
    }


    /// <summary>
    /// This was added because on the first day of early access there was some issues with cards coming out of the 
    /// starting had view (after a mulligan) where the first card wouldn't be blocking raycasts and couldn't be
    /// interacted with.
    ///</summary>
    private void OnPhaseChanged(Phase phase, int turnNumber, GameStateReadOnly gameState)
    {
        // on the beginning of the first turn, make sure all the hand cards are in a good state ... sort of a fixall because I can't repro the issue 
        if(phase == Phase.Action && turnNumber == 1)
        {
            Debug.Log("#PlayerHand# Player Starting hand, ensuring all cards are properly initialized");
            CardFrontController[] cards = this.GetComponentsInChildren<CardFrontController>();
            for(int i=0;i<cards.Length;i++)
            {
                CardFrontController card = cards[i];
                card.CanvasGroup.blocksRaycasts = true;
                card.RaycastTag = "card";
            }
        }
		else
		{
            if(View.DraggedCard != null || View.SelectedCard != null)
            {
                View.TriggerInput(InputEvent.MouseOutWindow, View.DraggedCard != null ? View.DraggedCard : View.SelectedCard);
            }
#if UNITY_ANDROID || UNITY_IOS
            if(this.View.IsUsingZoomHand && (phase == Phase.PreCombat || phase == Phase.GameOver))// && IsHandZoomedIn)
			{
				this.View.DeactivateZoomedHand();
			}
#endif
        }
    }

    private void OnPlayerCardDiscarded(int cardId, GameStateReadOnly gameState)
    {
        CardController card = this.ArenaCards.GetCard(cardId);

        if (card == null)
        {
            Debug.LogFormat("#PlayerHand# Couldn't find the discared cardin ArenaCards... checking CardsWaitingToBeRecycled Id={0}", cardId);
            this.ArenaCards.CardsWaitingToBeRecycled.TryGetValue(cardId, out card);

            if (card == null)
            {
                Debug.LogFormat("#PlayerHand# Discard Unable to find card id={0} to discard!", cardId);
                return;
            }
        }

        if(this.View.HoveredCard == card)
            this.View.TriggerInput(InputEvent.DiscardFromHand, card);
        else 
            this.View.OnCardDiscarded(card);
    }

    private void OnSummonCardToArena(ICardState cardState, CardZone fromZone, GameStateReadOnly gameState)
    {
        if(fromZone == CardZone.Hand)
        {
            CardController handCard = this.ArenaCards.GetCard(cardState.Id);
            if(this.View.HoveredCard == handCard)
                this.View.TriggerInput(InputEvent.SummonFromHand, handCard);
            else
                this.View.OnCardSummoned(handCard);
        }
    }

    private void OnGameStateInitialzied(GameStateReadOnly gameState)
    {
        this.View.GameState = gameState;
        Debug.Log("Setting GameState in player hand controller");
    }

    private void OnCardDeal(ICardState cardState, GameStateReadOnly gameState)
    {
         this.View.DealCardFromDeck(cardState);
    }
}
