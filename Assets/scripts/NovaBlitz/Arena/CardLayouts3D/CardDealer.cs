﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using LinqTools;

public class CardDealer : MonoBehaviour 
{
	public RectTransform CardAnimationRoot;
	public int NumCardsToDeal { get; private set;}
	public float YOffset;
	public float XOffset;
	public float MaxZ;
	public int DeckSize = 36; // Because 4 are  in the opening hand
	public int NumCardsRemaining {get { return this.numCardsRemaining;} set { this.SetNumCardsRemaining(value);}}
	public CardBackController [] DeckCards;
	private Color [] DeckCardColors;

	private Queue<System.Action<CardController>> cardDealtCallbacks = new Queue<System.Action<CardController>>();
	private Queue<System.Action> addInsertCallbacks = new Queue<System.Action>();
    private Dictionary<System.Action<CardController>, CardController> cardReferences = new Dictionary<System.Action<CardController>, CardController>();
	private List<CardController> dealFastCards = new List<CardController>();
	private Sequence dealSequence = null;
	private int topCardIndex;
	private CanvasGroup rootCanvasGroup;
	private Quaternion startRotation;
	private int numCardsRemaining;

	/// <summary>
	/// Get a reference to the local animator and get the behavoir on the default state
	/// so we can give it a reference to us.
	/// </summary>
	void Start()
	{
		this.rootCanvasGroup = this.CardAnimationRoot.GetComponent<CanvasGroup>();
		this.startRotation = this.CardAnimationRoot.rotation;
		this.topCardIndex = this.DeckCards.Length -1;

		this.DeckCardColors = new Color[this.DeckCards.Length];

		for(int i=0;i<this.DeckCards.Length;i++)
		{
			this.DeckCardColors[i] = this.DeckCards[i].CardBack.color;
		}
	}

	public bool IsCardBeingDealt(CardController card)
	{
		return this.cardReferences.Values.ToList<CardController>().Contains(card);
	}

	public void OnPointerEnter()
	{
		//Debug.Log("Pointer Enter");
		for(int i=0;i<this.DeckCards.Length;i++)
		{
			this.DeckCards[i].CardBack.color = Color.white;
		}
	}

	public void OnPointerExit()
	{
		//Debug.Log("Pointer Exit");
		for(int i=0;i<this.DeckCards.Length;i++)
		{
			this.DeckCards[i].CardBack.color = this.DeckCardColors[i];
		}
	}
    
    public void DealCard(System.Action onAddInsert, System.Action<CardController> onCardDealt, CardController card2Deal, bool fastDeal = false)
    {
        this.cardDealtCallbacks.Enqueue(onCardDealt);
		this.addInsertCallbacks.Enqueue(onAddInsert);
        this.cardReferences[onCardDealt] = card2Deal;
		this.NumCardsToDeal++;
        card2Deal.CanvasGroup.alpha = 0f;
		if(fastDeal)
		{
			this.dealFastCards.Add(card2Deal);
		}
        //Debug.LogFormat("{0} DealCard:{1}",this.gameObject.name, this.NumCardsToDeal);
    }

    /// <summary>
    /// Called by the CardDealerDefaultBehavior to decrement the numCardToDeal when it kicks
    /// off the "deal card" animation.
    /// </summary>
    public void CardDealt()
	{
		this.NumCardsToDeal--;
		CardBackController topCard = this.DeckCards[Mathf.Clamp(this.topCardIndex,0,this.DeckCards.Length-1)];
		this.CardAnimationRoot.anchoredPosition3D = ((RectTransform)topCard.transform).anchoredPosition3D;
		this.CardAnimationRoot.DOKill();
        this.CardAnimationRoot.localScale = Vector3.one;
		this.CardAnimationRoot.rotation = this.startRotation;

        //Debug.LogFormat("{0} DealCard:{1}",this.gameObject.name, this.NumCardsToDeal);
    }

    /// <summary>
    /// Called by an animation event on tohe DealCard timeline
    /// </summary>
    void AddInsert()
	{
        //Debug.Log("{0} Card Dealt: adding insert",this.gameObject.name);
        this.addInsertCallbacks.Dequeue().Invoke();
	}
	
	/// <summary>
	/// Called by an animation event on the DealCard timeline
	/// </summary>
	void DealAnimationFinished()
	{
        CardController card2deal = null;
        System.Action<CardController> cardDealCallback = this.cardDealtCallbacks.Dequeue();
        if(this.cardReferences.TryGetValue(cardDealCallback, out card2deal))
        {
            this.cardReferences.Remove(cardDealCallback);
            cardDealCallback.Invoke(card2deal);
			Debug.LogFormat("#CardDealer# Finished Dealing Card with ID:{0}", card2deal.CardIdAndName);
        }
	}

	public void SetCardBack(int cardBackId)
	{
		for(int i=0;i<this.DeckCards.Length;i++)
		{
			CardBackController cardBack = this.DeckCards[i];
			cardBack.SetCardBack(cardBackId);
		}
	}

	private void SetNumCardsRemaining(int numCards)
	{
		// Early out if nothings changed
		if(numCards == this.numCardsRemaining) { return; }
		
		// Calculate the topIndex for the stack
		float percentRemaining = 0f;
		if(numCards > 0)
		{
			percentRemaining = (float)numCards / (float)this.DeckSize;
            this.topCardIndex = (int)(System.Math.Round(percentRemaining * this.DeckCards.Length, 0, System.MidpointRounding.AwayFromZero));
        }
		else
		{
			this.topCardIndex = -1;
		}


        //Debug.LogFormat("TopCardIndex is: {0} percentRemaining: {1}", topCardIndex, percentRemaining);

        // Show/Hide the correct DeckCards to make the Deck look taller or shorter
        for (int i=0;i<this.DeckCards.Length;i++)
		{
			this.DeckCards[i].gameObject.SetActive(i <= this.topCardIndex);
		}

		// Store the numCardsRemaining
		this.numCardsRemaining = numCards;
	}

	void Update()
	{
		if(this.NumCardsToDeal > 0 && (this.dealSequence == null || this.dealSequence.IsPlaying() == false ))
		{
			RectTransform root = this.CardAnimationRoot;
			CardBackController topCard = this.DeckCards[Mathf.Clamp(this.topCardIndex,0,this.DeckCards.Length-1)];
			root.anchoredPosition3D = ((RectTransform)topCard.transform).anchoredPosition3D;
			root.rotation = this.startRotation;
			bool isFastDeal = false;

			System.Action<CardController> nextDealt = this.cardDealtCallbacks.Peek();
			if(nextDealt != null)
			{
				CardController card2Deal;
				if(this.cardReferences.TryGetValue(nextDealt, out card2Deal))
				{
					card2Deal.transform.AttachToParent(root, 0);
					card2Deal.CanvasGroup.alpha = 1f;
					card2Deal.transform.localPosition = Vector3.zero;
					card2Deal.transform.localRotation = Quaternion.identity;
					card2Deal.SortingOrder = 300;
					if(this.dealFastCards.Contains(card2Deal))
					{
						isFastDeal = true;
						this.dealFastCards.Remove(card2Deal);
					}
				}
			}

			this.rootCanvasGroup.alpha = 1f;

			Vector3 cameraDir = Camera.main.transform.position - (root.position);
			cameraDir.Normalize();
			Vector3 targetPos = root.anchoredPosition3D + cameraDir * 600f;
			targetPos.x -= this.XOffset;
			targetPos.y += cameraDir.y * this.YOffset;
			targetPos.z = Mathf.Clamp(targetPos.z,-this.MaxZ, 0);

			//UnityEditor.EditorApplication.isPaused = true;

			// start a deal sequence
			this.dealSequence = DOTween.Sequence();
			this.dealSequence.Append(root.DOAnchorPosX(targetPos.x, isFastDeal ? 0.1f : 0.15f)).SetEase(Ease.OutQuad);
			this.dealSequence.Join(root.DOAnchorPosY(targetPos.y, isFastDeal ? 0.1f :0.15f)).SetEase(Ease.InQuad);
			this.dealSequence.Join(root.DOLocalMoveZ(targetPos.z, isFastDeal ? 0.1f :0.15f)).SetEase(Ease.InQuad);
			this.dealSequence.Join(root.DORotateQuaternion(Quaternion.identity,isFastDeal ? 0.1f :0.5f)).SetEase(Ease.InQuart);
			this.dealSequence.InsertCallback(isFastDeal ? 0.1f : 0.2f, ()=>this.AddInsert());
			this.dealSequence.InsertCallback(isFastDeal ? 0.2f : 0.5f, ()=>{this.DealAnimationFinished();});
			this.dealSequence.InsertCallback(isFastDeal ? 0.25f : 0.6f, ()=>this.CardDealt());

			this.dealSequence.Play();
		}
	}
}
