﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using DG.Tweening;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using Messages;
using NovaBlitz.Game;
using DragonFoundry.GameState;
using System;

public enum InputAction
{
    HoverCard,
    UnHoverCard,
    SelectCard,
    DeSelectCard,
    MoveCardOutOfHand,
    MoveCardIntoHand,
    DragCardOverArena,
    PlayCard,
}
    
public class InputActionParams
{
    public InputActionParams(InputAction inputAction, object data)
    {
        this.InputAction = inputAction;
        this.Data =data;
    }
    public InputAction InputAction;
    public object Data;
}

public class PlayCardData
{
    public ICardState CardState;
    public int CardId;
    public int TargetId;
    public bool IsTargeted;
    public CardController CardBack;
}

public class DraggedCardData
{
    public CardController Card;
    public Vector3 DragPosition;
    public int HoveredCardPosition;
}

public class 
OpponentHandController : View
{
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public ArenaPlayers ArenaPlayers {get;set;}
    [Inject] public NovaBlitz.UI.GameData GameData { get;set;}
    [Inject] public GameControllerService GameControllerService {get;set;}
    [Inject] public NovaBlitz.UI.INovaContext SessionContext {get;set;}
    public Sprite CardSprite; // TODO: Temp
    public CardDealer CardDealer;
    public HandLayout3D HandLayout;
    public Transform ArenaFloor;
    public Canvas OverlayCanvas;
    public RectTransform VoidLocation;
    public RectTransform PlayedPowersLocation;
    public OpponentArenaController ArenaController;
    public TargetArrowController TargetArrowController;
    public ActivePowersLayout ActivePowersLayout;
    public RectTransform PlayedPowerRoot;
    public RectTransform InsertTransform;
    public NovaAudioPlayer DrawCardSound;
    public GameStateReadOnly GameState { get; set; }
    private CardController lastHoveredCard;
    //private CardController draggedCard;
    private List<int> incommingCards = new List<int>(); // CardID (because we don't konw if the card is a card back or card front currently)
    private List<int> resolvedCards = new List<int>(); // CardId (we need to track when a powerCard resolves before it's played to the PowerTray)
    private Dictionary<int,TargetedPowerCardInfo> targetedPowerCards = new Dictionary<int,TargetedPowerCardInfo>();
    private Transform draggedCardInsert;
    private Transform insertsRoot;
    private Camera uiCamera;
    //private PlayerHandController playerHandController;
    public ArenaView ArenaView { get; set; }

    private Queue<RectTransform> insertTransforms = new Queue<RectTransform>();
    //private Queue<RectTransform> powerInsertTransforms = new Queue<RectTransform>();
    private BiDictionary<CardController, RectTransform> cardInserts = new BiDictionary<CardController, RectTransform>();
    
    // State transition data
    private List<Transition> transitions = new List<Transition>();
    private HandState currentState = HandState.DefaultState;
    private Queue<InputActionParams> pendingHandActions = new Queue<InputActionParams>();
    private bool isLastActionComplete = true;
    public Action<int, RectTransform, string> TriggerFxEvent;

    //private float lastHandActionTime =0f;
    class Transition
    {
        public Transition(HandState fromState, InputAction inputAction, HandState toState, System.Action<InputActionParams> transitionFunction = null)
        {
            this.FromState = fromState;
            this.ToState = toState;
            this.InputAction = inputAction;
            this.TransitionFunction = transitionFunction;
        }
        
        public HandState FromState;
        public HandState ToState;
        public InputAction InputAction;
        public System.Action<InputActionParams> TransitionFunction;
        //public object Data;
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        //this.playerHandController = GameObject.FindObjectOfType<PlayerHandController>();

       /*        
                            _______________________________________________________________________
                            |   DefaultState  |   CardHovered   |  CardSelected  |  CardDragged   | < STATES
        ____________________|_________________|_________________|________________|________________|
        |<HoverCard>        |   CardHovered   |   CardHovered   |       X        |       X        |
        |-------------------|-----------------|-----------------|----------------|----------------|
        |<UnHoverCard>      |        X        |   DefaultState  |       X        |       X        |
        |-------------------|-----------------|-----------------|----------------|----------------|
        |<SelectCard>       |        X        |   CardSelected  |       X        |       X        |
        |-------------------|-----------------|-----------------|----------------|----------------|
        |<DeSelectCard>     |        X        |        X        |  DefaultState  |       X        |
        |-------------------|-----------------|-----------------|----------------|----------------|
        |<MoveCardOutOfHand>|        X        |        X        |  CardDragged   |       X        |
        |-------------------|-----------------|-----------------|----------------|----------------|
        |<MoveCardIntoHand> |        X        |        X        |       X        |  CardSelected  |
        |-------------------|-----------------|-----------------|----------------|----------------|
        |<DragCardOverArena>|        X        |        X        |       X        |  CardDragged   |
        |-------------------|-----------------|-----------------|----------------|----------------|
        |<PlayCard>         |        X        |        X        |       X        |  DefaultState  |
        |-------------------|-----------------|-----------------|----------------|-----------------
            ^ INPUTS
        */
        
        // From State.DefaultState
        this.transitions.Add(new Transition(HandState.DefaultState, InputAction.HoverCard, HandState.CardHovered, this.OnHoverCard));
        
        // From State.CardHovered
        this.transitions.Add(new Transition(HandState.CardHovered, InputAction.HoverCard, HandState.CardHovered, this.OnHoverCard));
        this.transitions.Add(new Transition(HandState.CardHovered, InputAction.UnHoverCard, HandState.DefaultState, this.OnUnhoverCard));
        this.transitions.Add(new Transition(HandState.CardHovered, InputAction.SelectCard, HandState.CardSelected, this.OnSelectCard));
        
        // From State.CardSelected
        this.transitions.Add(new Transition(HandState.CardSelected, InputAction.DeSelectCard, HandState.DefaultState, this.OnDeselectCard));
        this.transitions.Add(new Transition(HandState.CardSelected, InputAction.MoveCardOutOfHand, HandState.CardDragged, this.OnMoveCardOutOfHand));
        
        // From State.CardDragged
        this.transitions.Add(new Transition(HandState.CardDragged, InputAction.MoveCardIntoHand, HandState.CardSelected, this.OnMoveCardIntoHand));
        this.transitions.Add(new Transition(HandState.CardDragged, InputAction.DragCardOverArena, HandState.CardDragged, this.OnDragCardOverArena));
        this.transitions.Add(new Transition(HandState.CardDragged, InputAction.PlayCard, HandState.DefaultState, this.OnPlayCard));
        
        // Create a new root transform for ineserts
        this.insertsRoot = new GameObject().transform;
        this.insertsRoot.name = "InsertsFor_" + this.name;

        // Attach the default insert to the new root
        this.InsertTransform.gameObject.SetActive(false);
        this.InsertTransform.SetParent(this.insertsRoot);
        this.insertTransforms.Enqueue(this.InsertTransform);
        this.InsertTransform.name = "Insert 0";

        this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();

        // Create 9 copies of the default insert
        for (int i = 1; i < 19; i++)
        {
			AddInsertTransform ();
        }
    }

	void AddInsertTransform()
	{
		RectTransform trans = GameObject.Instantiate(this.InsertTransform);
		trans.name = "Insert " + insertTransformNumber;
		insertTransformNumber++;
		this.insertTransforms.Enqueue(trans);
		trans.AttachToParent(this.insertsRoot,insertTransformNumber);
	}

	int insertTransformNumber = 1;

    /// <summary>
    /// Utility method used by the PlayerHand controller so it can determine where to spawn a card from
    /// when the opponent plays a power that spawns a card and adds it to the players hand
    /// </summary>
    public CardController GetPlayedPower(int cardId)
    {
        TargetedPowerCardInfo powerCardInfo;
        if(this.targetedPowerCards.TryGetValue(cardId, out powerCardInfo))
        {
            return powerCardInfo.Card;
        }

        return null;
    }

    /// <summary>
    /// We have to maintain state about what cards are entering the hand because 
    /// their CastEvents may come before the card has tweened into the opponent hand, either from the
    /// arena or from the deck.
    /// </summary>
    public bool IsCardIncomming(int cardId)
    {
        return this.incommingCards.Contains(cardId);
    }

    /// <summary>
    /// A bit of a daisey chain, the mediator listens to discard events, this is used to discard
    /// a card from the opponents hand, but only if it's no longer in the incommingCards list.
    /// The OpponentHandMediator calls this method once the has arrived in the Hand.
    /// </summary>
    internal void OnOpponentCardDiscarded(int cardId)
    {
        CardController cardBack = this.ArenaCards.GetCardBack(cardId);

        RectTransform cardInsert = null;
        if (this.cardInserts.TryGetValue(cardBack, out cardInsert))
        {
            cardInsert.AttachToParent(this.insertsRoot,0);
            this.insertTransforms.Enqueue(cardInsert);
            this.cardInserts.Remove(cardBack);
            this.StartCoroutine(DelayTrash(cardId));
        }
        else if(cardBack.transform.parent == this.HandLayout.transform)
        {
            this.StartCoroutine(DelayTrash(cardId));
        }
    }

    /// <summary>
    /// Recycles a cardBack and then waits until the end of the frame to update the hand layout
    /// to ensure the hand properly collapses.
    /// </summary>
    private IEnumerator DelayTrash(int cardId)
    {
        CardController cardBack = this.ArenaCards.RemoveCardBack(cardId);
        cardBack.transform.DOKill();
        cardBack.Recycle();
        yield return null; // wait for end of frame
        this.HandLayout.UpdateCardTweens();
    }

    /// <summary>
    /// This method triggers a sequence of hand actions Hover->Select->MoveOutOfHand->DragOverArena->PlayCard to move a cardBack
    /// from the OpponentsHand, flip it over to reval the card front and then play it to the PowersTray or the OpponentsArena
    /// </summary>
    internal void PlayCardFromHand(ICardState cardState, CardController cardBack, int targetId = 0)
    {
        // The OpponentHandController will own removal of the cardBack now
        // remove it from the arena cards.
        Debug.LogFormat("#OpponentHandController# PlayCardFromHand  cardBack: {0} cardState.Id:{1} cardBack.Id:{2}; {3}", cardBack, cardState.Id, cardBack.CardIdAndName, cardBack);
        this.ArenaCards.RemoveCardBack(cardBack.CardId);

        this.TriggerHandAction(InputAction.HoverCard, cardBack);
        this.TriggerHandAction(InputAction.SelectCard, cardBack);
        this.TriggerHandAction(InputAction.MoveCardOutOfHand, cardBack);

        // Pick a random spot in the arena to play the card to
        System.Random random = new System.Random();
        int numCardsInArena = this.ArenaController.ArenaLayout.transform.GetComponentsInChildren<CardController>().Length;
        int arenaIndex = random.Next(0, numCardsInArena + 1);
        float arenaX = this.ArenaController.ArenaLayout.GetXPositionForSibling(arenaIndex, numCardsInArena + 1);
        Vector3 arenaLocal = new Vector3(arenaX, 20, 0);
        Vector3 arenaWorld = this.ArenaController.ArenaLayout.transform.TransformPoint(arenaLocal);
        Vector2 cardPointScreen = Camera.main.WorldToScreenPoint(arenaWorld);
        Vector3 cardPointWorld = Vector3.zero;
        RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)this.OverlayCanvas.transform, cardPointScreen, Camera.main, out cardPointWorld);

        if (cardState.IsPower)
        {
            if (this.targetedPowerCards.Count == 0 || hoverHandOffsetCount >= 4)
            {
                hoverHandOffsetCount = 0;
            }
            else
                hoverHandOffsetCount++;
            // scale factor is 1.0f in 16x9; 0.75f in 4x3;
            float scaleFactor = 1.0f;

            //dfg
            cardPointWorld.x = -15f * scaleFactor; // was -10f when cards@1.5, now -15f with cards @1.2
            int offset = (Math.Max(this.targetedPowerCards.Count, hoverHandOffsetCount) % 6) - 1;
            cardPointWorld.x += offset * 14.4f * scaleFactor; // was 18 @1.5, so should be 14.4 @1.2
            //Debug.LogFormat("#TargetedPowerCount#: TP count={0}, offset={1} cpw={2}", this.targetedPowerCards.Count, hoverHandOffsetCount, cardPointWorld);
        }

        // Drag the card out over the arena
        this.TriggerHandAction(InputAction.DragCardOverArena, new DraggedCardData { Card = cardBack, DragPosition = cardPointWorld, HoveredCardPosition = hoverHandOffsetCount });

        // Initialize PlayCardData for the PlayCard Action
        PlayCardData playCardData = new PlayCardData() { CardState = cardState, CardId = cardBack.CardId, TargetId = targetId, IsTargeted = targetId != 0, CardBack = cardBack };
            
        // Trigger Playing of the Card (either to the arena or the powers tray)
        this.TriggerHandAction(InputAction.PlayCard, playCardData);
    }

    private int hoverHandOffsetCount = 0;

    /// <summary>
    /// When a cardBack is recycled, we want to update the HandLayout just incase it needs to close some gaps
    /// </summary>
    internal void OnCardbackRecycled(int cardId)
    {
        this.HandLayout.UpdateCardTweens();
    }

    /// <summary>
    /// A hand crafted series of tweens that spawns a card in the arena and banishes it to the the opponents hand
    /// doing a nice little flip over animation in the process.
    /// </summary>
    internal void OnCardSpawnedAndBanished(ICardState sourceCardState, int spawnCardId, GameStateReadOnly gameState)
    {
        // Position them over the source card initially
        CardController sourceCardController = null;
        sourceCardController = sourceCardState == null ? null : this.ArenaCards.GetCard(sourceCardState.Id);
        CardController spawnCard = this.ArenaCards.GetCard(spawnCardId);

        ICardState cardState = gameState.GetCard(spawnCardId);
        if (cardState == null || spawnCard == null)
        {
            // Must be a card back... go ahead and create it
            Debug.LogFormat("#OpponentHand# spawning a CardBack for ID: {0}", spawnCardId);
            spawnCard = this.ArenaCards.GetOrCreateCardBack(spawnCardId);
        }

        if (sourceCardController == null && sourceCardState != null)
        {
            Debug.Log("#OpponentHand# source card is null, must be in the trashed cards list.. ");
            this.ArenaCards.CardsWaitingToBeRecycled.TryGetValue(sourceCardState.Id, out sourceCardController);
        }

        // Attach the spawned card to the overlay canvas
        spawnCard.transform.AttachToParent(this.OverlayCanvas.transform, this.OverlayCanvas.transform.childCount, sortingOrder: 90, blocksRaycasts: false);
        Vector3 sourceCardInitialPos = Vector3.zero;


        var viewPoint = uiCamera.WorldToViewportPoint(spawnCard.transform.position);
        if (viewPoint.x >= 0 && viewPoint.x <= 1 && viewPoint.y >= 0 && viewPoint.y <= 1 && viewPoint.z > 0)
        {
            // reset rotation etc; do nothing.
        }
        else if (sourceCardController == null)
        {
            Debug.Log("#OpponentHand# Ok the source card is looooong gone, we need to spawn this baby over the Void location");

            // Position the spawned card over the power tray (currently standing in for void)
            spawnCard.transform.position = this.VoidLocation.position;
            sourceCardInitialPos = this.VoidLocation.position;
        }
        else
        {
            Debug.LogFormat("#OpponentHand#SpawnedAndBanished Affected: {0}is ({1}) over source:{2} is ({3})", spawnCardId,
                (spawnCard != null ? spawnCard.name : "null"), 
                sourceCardState != null ? sourceCardState.Id : -10000,
                (sourceCardController != null ? sourceCardController.name : "null"));

            // Position the spawned card over the sourceCard
            Vector3 cameraDirection = (Camera.main.transform.position - spawnCard.transform.position).normalized;
            spawnCard.transform.position = sourceCardController.transform.position;
            sourceCardInitialPos = sourceCardController.transform.position;
            spawnCard.transform.localScale = sourceCardController.transform.localScale;
        }

        spawnCard.CanvasGroup.alpha = 0f;

        this.incommingCards.Add(spawnCard.CardId);
        Debug.LogFormat("#OpponentHand#Adding card {0} to incommingCards.", spawnCard.CardIdAndName);

        Sequence seq = DOTween.Sequence();

        seq.AppendInterval(0.1f);
        seq.Append(spawnCard.CanvasGroup.DOFade(1f, 0.1f));
        seq.Append(spawnCard.transform.DOLocalMoveZ(-100f, 0.3f));
        seq.AppendCallback(() => {
            RectTransform cardInsert;
            if (this.cardInserts.TryGetValue(spawnCard, out cardInsert) == false)
            {
                cardInsert = this.insertTransforms.Dequeue();
                cardInsert.gameObject.SetActive(true);
                this.cardInserts[spawnCard] = cardInsert;
                cardInsert.rotation = Quaternion.identity;
                //Debug.LogFormat("#OpponentHand# Adding insert for card {0}", spawnCard);
            }

            cardInsert.AttachToParent(this.HandLayout.transform, 0);
            cardInsert.localPosition = this.HandLayout.GetChildPosition(cardInsert);
            this.HandLayout.UpdateCardTweens();
        });
        seq.Append(spawnCard.transform.DOMoveY(this.transform.position.y, 0.5f).OnUpdate(() => {
            //Debug.LogFormat("#OpponentHand# Updating For {0}", spawnCard);
            RectTransform insert;
            if (this.cardInserts.TryGetValue(spawnCard, out insert) == false)
            {
                insert = this.insertTransforms.Dequeue();
                insert.gameObject.SetActive(true);
                this.cardInserts[spawnCard] = insert;
                insert.rotation = Quaternion.identity;
                //Debug.LogFormat("#OpponentHand# Adding insert for card {0}", spawnCard);
            }
            Vector3 spawnCardPos = spawnCard.transform.position;
            float distance = insert.position.x - sourceCardInitialPos.x;
            float offset = distance * Time.deltaTime * 6f;
            float distanceToCard = insert.position.x - spawnCardPos.x;
            if (Mathf.Abs(spawnCardPos.x + offset) >= Mathf.Abs(distanceToCard))
            {
                spawnCardPos.x = insert.position.x;
            }
            else
            {
                spawnCardPos.x += offset;
            }
            spawnCard.transform.position = spawnCardPos;
        }));
        seq.Join(spawnCard.transform.DOScale(Vector3.one, 0.15f));
        seq.OnComplete(()=>{
            Debug.LogFormat("#OpponentHand# OnComplete For {0}", spawnCard);
            RectTransform cardInsert = this.cardInserts[spawnCard];

            if(spawnCard is CardFrontController)
            {
                spawnCard.transform.DORotate(new Vector3(0,180,0), 0.2f)
                    .OnComplete(()=>{
                    // Create a card Back and attach it to the hand
                    CardController cardBack = ArenaCards.GetOrCreateCardBack(spawnCardId);
                    cardBack.CanvasGroup.alpha = 1f;
                    cardBack.transform.AttachToParent(this.transform, cardInsert.GetSiblingIndex(), blocksRaycasts:false);
                    cardBack.transform.position = spawnCard.transform.position;

                    // Destroy the spawn Card 
                    //if (true)
                    //{
                        Debug.LogFormat("#OpponentHand#Removing card {0} from incommingCards.", spawnCard.CardIdAndName);
                        this.ArenaCards.RemoveCard(spawnCard);
                        spawnCard.transform.DOKill();
                        spawnCard.Recycle();
                    //}
                    //else
                    //    Debug.LogFormat("#OpponentHand#Removing card {0} from incommingCards, but keeping it alive because somebody else loves it.", spawnCard.CardIdAndName);

                    this.HandLayout.UpdateCardTweens();
                    this.incommingCards.Remove(spawnCard.CardId);
                });
            }
            else if(spawnCard is CardBackController)
            {
                //Debug.LogFormat("#OpponentHand#spawnCard is card back for incoming card {0}", spawnCard.CardId);
                spawnCard.transform.AttachToParent(this.transform, cardInsert.GetSiblingIndex(), blocksRaycasts:false);
                this.ArenaCards.AddCardBack(spawnCard);

                this.incommingCards.Remove(spawnCard.CardId);
            }
            else
            {
                this.incommingCards.Remove(spawnCard.CardId);
                Debug.LogFormat("#OpponentHand#spawnCard is unknown type for incoming card {0}", spawnCard.CardIdAndName);
            }

            // Return the insert to the pool
            this.cardInserts.Remove(spawnCard);
            this.insertTransforms.Enqueue(cardInsert);
            cardInsert.AttachToParent(this.insertsRoot, 0);
            //Debug.LogFormat("#OpponentHand# Killing insert {0}", cardInsert.name);
            cardInsert.DOKill();
            this.HandLayout.UpdateCardTweens();
        });
        seq.Play();
    }

     /// <summary>
    /// A hand crafted series of tweens that takes a card in the arena and banishes it to the the opponents hand
    /// doing a nice little flip over animation in the process.
    /// </summary>
    internal void OnCardBanished(int cardId, GameStateReadOnly gameState)
    {
         // Get a reference to the arena card
        CardFrontController banishedCard = (CardFrontController)this.ArenaCards.GetCard(cardId);

        Debug.LogFormat("#OpponentHand#Banished card is {0}", banishedCard.CardIdAndName);
        

        // Play a banish effect on it
        if(TriggerFxEvent != null)
            TriggerFxEvent.Invoke(banishedCard.CardId, banishedCard.Card.rectTransform,"fx_cardbanish");
       
        // scale it up
        banishedCard.transform.AttachToParent(this.OverlayCanvas.transform, 0, sortingOrder:90, blocksRaycasts:false);
        banishedCard.transform.DOLocalMoveZ(banishedCard.transform.localPosition.z - 200, 0.4f)
        .OnComplete(()=>
        {
            // Create a card Back and attach it to the hand
            CardController cardBack = ArenaCards.GetOrCreateCardBack(cardId);
            cardBack.CanvasGroup.alpha = 1f;
            cardBack.transform.AttachToParent(this.transform, 0, blocksRaycasts:false);
            cardBack.transform.position = banishedCard.transform.position;
            
            // Destroy the banished Card 
            this.ArenaCards.RemoveCard(banishedCard);
            banishedCard.transform.DOKill();
            banishedCard.Recycle();
           
            this.HandLayout.UpdateCardTweens();
        });
    }
    
    /// <summary>
    /// clean up the inserts when we're destroyed
    /// </summary>
    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (this.insertsRoot != null)
            Destroy(this.insertsRoot.gameObject);
    }

    /// <summary>
    /// Triggers a specific hand action to perform and adds it to the pendingHandActionsQueue
    /// data contains all of the data neccessary to perform the triggered action
    /// </summary>
    private void TriggerHandAction(InputAction actionType, object data)
    {
        this.pendingHandActions.Enqueue(new InputActionParams(actionType, data));
    }
    
    /// <summary>
    /// Handler for the HoverCard InputAction
    /// </summary>
    public void OnHoverCard(InputActionParams actionParams)
    {
        CardController card = (CardController)actionParams.Data;
        
        if(card.transform.IsChildOf(this.HandLayout.transform) == false)
        {
            // Early out if the card is not a child of the hand. We can only hover cards in the hand.
            Debug.LogWarningFormat("Attempted to hover a card that wasn't in the opponents hand: {0}", card.CardIdAndName);
            return;
        }
        else
        {
            //Debug.LogFormat("#OpponentHand# hovering card with id: {0}", card.CardId);
        }

        
        if(this.lastHoveredCard != null && this.lastHoveredCard != card)
        {
            // Return the perviously hoveredCard to the hand
            this.UnhoverCard(this.lastHoveredCard);
        }
        
        this.lastHoveredCard = card;
        
        if(this.insertTransforms.Count <= 0 )
        {
            Debug.LogWarning("No insert available to hover card");
        }
        
        RectTransform cardInsert;
        if(this.cardInserts.TryGetValue(card, out cardInsert) == false)
        {
            cardInsert = this.insertTransforms.Dequeue();
            cardInsert.gameObject.SetActive(true);
            this.cardInserts[card] = cardInsert;
            cardInsert.rotation = Quaternion.identity;
        }
        Vector3 localCardPos = card.transform.localPosition;
        Quaternion localCardRot = card.transform.localRotation;
        cardInsert.AttachToParent(this.HandLayout.transform, card.transform.GetSiblingIndex());
        cardInsert.transform.localPosition = localCardPos;
        cardInsert.transform.localRotation = localCardRot;
        card.transform.AttachToParent(cardInsert, 1);
        card.transform.position = cardInsert.position;
        card.transform.DOLocalMoveY(-35, 0.02f).SetEase(Ease.OutExpo)
            .OnComplete(()=>this.isLastActionComplete = true);
    }
    
    /// <summary>
    /// Handler for the UnHoverCard InputAction
    /// </summary>
    public void OnUnhoverCard(InputActionParams actionParams)
    {
        // TODO: make this use the actionParams.data to get the correct card
        if(this.lastHoveredCard != null)
        {
            // Ignore the cardController in the actionParams because the hovered card may have changed
            this.UnhoverCard(this.lastHoveredCard);
            this.lastHoveredCard = null;
        }
    }
    
    /// <summary>
    /// Handler for the SelectCard InputAction
    /// </summary>
    public void OnSelectCard(InputActionParams actionParams)
    {
        CardController card = (CardController)actionParams.Data;
        card.transform.DOKill();

        //Debug.LogFormat("#OpponentHand# Selecting card with id: {0}", card.CardId);
        
        RectTransform cardInsert;
        if(this.cardInserts.TryGetValue(card, out cardInsert) == false)
        {
            cardInsert = this.insertTransforms.Dequeue();
            cardInsert.gameObject.SetActive(true);
            this.cardInserts[card] = cardInsert;
            cardInsert.AttachToParent(this.HandLayout.transform, card.transform.GetSiblingIndex());
        }
        cardInsert.transform.rotation = Quaternion.identity;
        // Attach card to OverlayCanvas
        card.transform.AttachToParent(this.OverlayCanvas.transform, 1, sortingOrder:150);
        card.transform.rotation = Quaternion.identity;
        this.isLastActionComplete = true;
        cardInsert.localPosition = this.HandLayout.GetChildPosition(cardInsert);
    }
    
    /// <summary>
    /// Handler for the DeSelectCard InputAction
    /// </summary>
    public void OnDeselectCard(InputActionParams actionParams)
    {
        CardController card = (CardController)actionParams.Data;
        RectTransform insert = this.cardInserts[card];
        this.HandLayout.UpdateCardTweens();
        
        card.transform.AttachToParent(insert, 1);
        card.transform.DOScale(Vector3.one, 0.1f)
            .OnComplete(()=>this.isLastActionComplete = true);
        this.UnhoverCard(card);
        
    }
    
    /// <summary>
    /// Handler for the MoveCardOutOfHand InputAction
    /// </summary>
    public void OnMoveCardOutOfHand(InputActionParams actionParams)
    {
        CardController card = (CardController)actionParams.Data;
        // Attach the insert to the arena floor and hide it
        RectTransform insert = this.cardInserts[card];
        insert.AttachToParent(this.ArenaFloor,1);
        insert.gameObject.SetActive(false);

        //Debug.LogFormat("#OpponentHand# moving card out of hand id: {0}", card.CardId);

        card.RaycastTag = "Untagged";
        ICardState cardState = this.GameControllerService.GetCardState(card.CardId);
        if(cardState.IsUnit)
        {
            Debug.LogFormat("#OpponentHand# Telling Arena OnCardDragBegin ({0})", card.CardIdAndName);
            this.ArenaController.OnCardDragBegin(card);
        }

        // Move the card forward out of the hand a bit
        card.transform.DOLocalMoveY(card.transform.localPosition.y - 35, 0.05f)
            .OnComplete(()=>this.isLastActionComplete = true);

        // Scale it to 1.0 (used to be down to 0.8 to match the player hand experience)
        card.transform.DOScale(new Vector3(1f, 1f, 1f), 0.1f);
    }
    
    /// <summary>
    /// Handler for the MoveCardIntoHand InputAction
    /// </summary>
    public void OnMoveCardIntoHand(InputActionParams actionParams)
    {
        CardController card = (CardController)actionParams.Data;
        
        this.ArenaController.OnCardDragEnd();
        
        // Attach the insert to the hand at the correct hand location
        int handIndex = (int) actionParams.Data;
        RectTransform insert = this.cardInserts[card];
        insert.AttachToParent(this.HandLayout.transform, handIndex);
        insert.gameObject.SetActive(true);
        
        // Position and rotate the insert correctly
        insert.localPosition = this.HandLayout.GetChildPosition(insert);
        this.HandLayout.UpdateCardTweens();
        
        // if there was a targeted power card, clean it up
        TargetedPowerCardInfo powerCard = null;
        if(this.targetedPowerCards.TryGetValue(card.CardId, out powerCard))
        {
            powerCard.Card.transform.DOKill();
            powerCard.Card.Recycle();
            this.targetedPowerCards.Remove(card.CardId);
        }
        
        // Get the position the card should move to relative to the insert
        Vector3 localPosition = insert.transform.localPosition;
        localPosition = insert.transform.up * -30f;
        
        // Now get that position relative to the OverlaCanvas
        Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, insert.transform.TransformPoint(localPosition));
        Vector2 relativePoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)this.OverlayCanvas.transform, screenPoint, Camera.main, out relativePoint);
        
        card.SortingOrder = this.HandLayout.GetSortingOrder(insert) + 1;
        
        // Position the card -35 below the insert
        card.transform.DOLocalMove(relativePoint,700f)
            .SetSpeedBased()
            .SetEase(Ease.InOutQuart)
            .OnComplete(()=>{
                this.isLastActionComplete = true;
                });
        card.transform.DORotateQuaternion(insert.rotation, 0.3f).SetEase(Ease.InExpo);
        card.transform.DOScale(Vector3.one, 0.1f);
    }
    
    /// <summary>
    /// Handler for the DragCardOverArena InputAction
    /// </summary>
    public void OnDragCardOverArena(InputActionParams actionParams)
    {
        DraggedCardData draggedCardData = (DraggedCardData)actionParams.Data;
        CardController cardBack = draggedCardData.Card;
        cardBack.DOKill();
        Vector3 worldPos = (Vector3)draggedCardData.DragPosition;
        
        cardBack.transform.DORotateQuaternion(Quaternion.identity, 0.3f);

        cardBack.transform.AttachToParent(PlayedPowersLocation, PlayedPowersLocation.childCount);
        ICardState cardState = this.GameState.GetCard(cardBack.CardId);

        if (cardState.IsPower)
        {
            var localPos = new Vector3(draggedCardData.HoveredCardPosition * 220f, 0, 0);
            worldPos = this.PlayedPowersLocation.TransformPoint(localPos);
        }
        //Debug.LogFormat("OpponentHand Moving card to WorldPos:{0}", worldPos);
        cardBack.transform.DOMove(worldPos, 100f)
        //cardBack.transform.DOMove(worldPos, 100f)
            .SetSpeedBased()
            .SetEase(Ease.InOutQuart)
            .OnComplete(()=>{
                cardBack.CanvasGroup.alpha = 0f;
                this.HandLayout.UpdateCardTweens();
                CardFrontController cardFront = (CardFrontController)this.ArenaCards.GetCard(cardBack.CardId);
                //bool addedCardFont = false;

                // Because Power Cards are trashed shortly (Immediately?) after their Resolve Event, if our power card was delayed 
                // in being dragged over the arena the cardFront it's expecting might already be destroyed.
                // We could check the cardsWaitingToBeRecycled list on ArenaCards, but honestly with enough delay
                // the card might have clared from that too. The most reliable thing is to just spawn
                // our own for the card flip animation and if we spawn it, make sure we recycle it as well. -DMac
                // PB - we're no longer recycling the card here; the fact that it's in ArenaCards & will take care of that.
                if(cardFront == null)
                {
                    Debug.LogFormat("#OpponentHand# create cardFront for dragged card: id:{0}:{1}", cardState.Id, cardState.GetName());
                    cardFront = (CardFrontController)this.ArenaCards.CreateCard(cardState);
                    ArenaCards.AddCard(cardFront);
                    //addedCardFont = true;
                }
                else
                {
                    cardFront.Initialize(cardState, cardState.ControllerId == GameData.CurrentGameData.PlayerId
                        ? GameData.CurrentGameData.PlayerCardBack
                        : GameData.CurrentGameData.OpponentCardBack, cardFront.IsPurified);
                }

                if (cardState.IsPower)
                {
                    cardFront.transform.AttachToParent(PlayedPowersLocation, PlayedPowersLocation.childCount);
                }
                    cardFront.transform.localScale = new Vector3(1.2f, 1.2f, 1);
                this.ArenaController.ArenaLayout.UpdateCardTweens();
                //Debug.LogFormat("DragCardOverArena: cardFront-{0};{1}", cardFront, cardFront.gameObject);
                cardFront.transform.position = cardBack.transform.position;
                cardFront.transform.rotation = Quaternion.Euler(0,180,0);
                cardFront.transform.DOLocalRotate(Vector3.zero,0.2f).OnComplete(()=>
                {
                    this.isLastActionComplete = true;
                    if (cardFront.IsPower)
                    {
                        cardFront.gameObject.SetActive(false);

                    }
                    // If we spanwed the cardFront AND the front is a Power, it's our job to recycle it after the flip over tween.
                    /*if (addedCardFont)
                    {
                        this.ArenaCards.RemoveCard(cardFront);
                        cardFront.Recycle();
                    }*/
                    //UnityEditor.EditorApplication.isPaused = true;
                });
            });
    }

    /// <summary>
    /// Handler for the PlayCard InputAction
    /// </summary>
    public void OnPlayCard(InputActionParams actionParams)
    {
        PlayCardData playCardData = (PlayCardData)actionParams.Data;
        int cardId = playCardData.CardId;
        CardController cardBack = playCardData.CardBack;
        
        // this is the proper initialization path for a card being played with gamestate
        CardController newCard = this.ArenaCards.GetCard(cardId);
        
        // Return the insert to the queue
        RectTransform insert = this.cardInserts[cardBack];
        insert.AttachToParent(this.insertsRoot, 1);
        this.cardInserts.Remove(cardBack);
        this.insertTransforms.Enqueue(insert);
        insert.DOKill();

        // If the card has been trashed during the play animation sequence, we may need to reference
        // it from the CardsWaitingToBeRecycled (trashed cards) dictionary
        if(newCard == null)
        {
            Debug.LogFormat("#OpponentHand# New card {0} is null; getting from recyle cards", cardId);
            this.ArenaCards.CardsWaitingToBeRecycled.TryGetValue(cardId, out newCard);
        }

        // if this was the lastHoveredCard...
        if(this.lastHoveredCard != null && this.lastHoveredCard == cardBack)
        {
            // ...make the OpponentHandController forget about it so it doesn't try to unhover it
            // when the next cardHover happens.
            this.lastHoveredCard = null;
        }
        if(newCard == null)
        {
            Debug.LogErrorFormat("new card is null: {0}", cardId);
        }
        if(newCard.IsUnit)
        {
            this.ArenaController.OnPlayCard(newCard);
        }
        else if(newCard.IsPower)
        {
            TargetArrowTarget target = null;
            int targetId = playCardData.TargetId;

            // Create a new pwerCard to be tweened over to the powerTray 
            // (one that isn't registered in the ArenaCards Model so the rest of the gamestate doesn't know about it)
            CardFrontController powerCard = (CardFrontController) this.ArenaCards.CreateCard(playCardData.CardState);

            if(playCardData.IsTargeted)
            {
                // Get a target reference
                target = this.GetTarget(targetId);
				Debug.LogFormat("#OpponentHand# Played an Targeted Power {0} with target with ID:{1}", powerCard.CardIdAndName, targetId);
            }
            else
            {
				Debug.LogFormat("#OpponentHand# Played an UNtargeted Power {0}", powerCard.CardIdAndName);
            }
        
            // Position the new powerCard instance over the cardBack
            powerCard.transform.position = cardBack.transform.position;
            //powerCard.transform.localScale = new Vector3(1.2f,1.2f,1f);


            // Create a new power card instance to tween over to the tray and get the associated powerCardInfo
            this.TweenPowerToActivePowers(powerCard, target);
        }

        this.isLastActionComplete = true;    
        cardBack.transform.DOKill();
        cardBack.Recycle();
    }

    /// <summary>
    /// If the resolved Power is in our powerTray than make the target arrow fully red to indicate resolution.
    /// If the resolvedPower isn't played yet, we need to store the fact that it's resolved in the resolvedCards
    /// list, so that when it is played, the card can appear resolved from the beginning.
    /// </summary>
    public void OnResolvePower(ICardState cardState)
    {
        TargetedPowerCardInfo powerCardInfo = null;
        if(this.targetedPowerCards.TryGetValue(cardState.Id, out powerCardInfo ))
        {
            this.resolvedCards.Remove(cardState.Id);
            if(powerCardInfo.Arrow != null)
            {
                powerCardInfo.Arrow.StartingColor =  new Color(255/255f,23/255f, 23/255f,255/255f);
            }
        }
        else
        {
            this.resolvedCards.Add(cardState.Id);
        }
    }
    
    /// <summary>
    /// Utility function that Unhovers a card and tweens it back to the hand. 
    /// (not to be confused with OnUnhoverCard that is the InputAction handler)
    /// </summary>
    private void UnhoverCard(CardController card)
    {
        RectTransform insert = null;
        if(this.cardInserts.TryGetValue(card, out insert))
        {
            this.isLastActionComplete = true;
            CardController activeCard = card;
            card.transform.DOKill();
            card.transform.DOLocalMoveY(0, 0.25f)
                .OnComplete(()=>{
                        activeCard.transform.AttachToParent(this.HandLayout.transform, insert.GetSiblingIndex());
                        insert.AttachToParent(this.insertsRoot, 0);
                        this.insertTransforms.Enqueue(insert);
                        insert.DOKill();
                        insert.gameObject.SetActive(false);
                        this.cardInserts.Remove(activeCard);
                        this.HandLayout.UpdateCardTweens();
                });
            activeCard.SortingOrder = this.HandLayout.GetSortingOrder(insert) +1;
            card.transform.rotation = Quaternion.identity;
            card.transform.DORotate(new Vector3(0,0,this.HandLayout.GetChildZRotation(insert)), 0.15f);
        }
    }

    /// <summary>
    /// Callback from CardDealer, an animation event on the DealCard animation
    /// triggers this callback, so we can control percisely in the animation timeline
    /// when the hand opens to accept the newly dealt card.
    /// </summary>
    private void OnAddInsert(RectTransform insertTransform)
    {
        insertTransform.gameObject.SetActive(true);
        insertTransform.SetParent(this.transform);
        insertTransform.localScale = Vector3.one;

        // New Cards Always come in on the right
        insertTransform.SetSiblingIndex(0);
        insertTransform.localPosition = this.HandLayout.GetChildPosition(insertTransform);

        this.HandLayout.UpdateCardTweens();
    }

    /// <summary>
    /// Callback from CardDealer, an animation event on the DealCard animation
    /// triggers this callback, handing us a reference to the spawned card
    /// and allowing us to set up a tween to take it to the hand.
    /// </summary>
    private void OnCardDealt(CardController cardBack, RectTransform insertTransform)
    {

        // Make the cardBack a child of its insert and make it sort above the other cards in the hand
        cardBack.transform.AttachToParent(insertTransform, 0, sortingOrder: this.transform.childCount + 120);
        cardBack.gameObject.name = "CardBack " + this.transform.childCount;
        cardBack.RaycastTag = "cardBack";
        cardBack.CanvasGroup.alpha = 1.0f;

        // Local tween position just to the right of the insert...(so it can tuck behind the card next to it)
        Vector3 localInsertPos = (Vector3.left * this.HandLayout.MaxSeperation * -0.5f);

        // ...unless it's the last card in the hand
        if (insertTransform.GetSiblingIndex() == this.transform.childCount - 1)
        {
            localInsertPos = Vector3.zero;
            cardBack.transform.DORotateQuaternion(insertTransform.rotation, 0.5f).SetEase(Ease.OutSine);
        }
        // ...or the hand is currently "flat" with no overlapping cards
        if (this.transform.childCount <= this.HandLayout.MaxCardsInFlatHand)
        {
            localInsertPos = Vector3.zero;
        }

        // ============================
        // Deal Card to Hand Tween
        // ============================
        cardBack.transform.DOLocalMove(localInsertPos, 1000f).SetSpeedBased()
        .SetEase(Ease.InQuart)
        .OnComplete(
            () =>
            {
                // Deactivate the insert transform and return it to the queue
                if (localInsertPos == Vector3.zero)
                {
                    // Make the card a child of Hand layout, have it block raycasts and have a proper sort order
                    cardBack.transform.AttachToParent(this.transform, insertTransform.GetSiblingIndex(), sortingOrder: insertTransform.GetSiblingIndex(), blocksRaycasts: true);
                    insertTransform.SetParent(this.insertsRoot);
                    insertTransform.gameObject.SetActive(false);
                    this.insertTransforms.Enqueue(insertTransform);
                    insertTransform.DOKill();
                    this.HandLayout.UpdateCardTweens();
                    // The hand knows about the cardBack so it's no longer "incomming"
                    this.incommingCards.Remove(cardBack.CardId);
                }
                else
                {
                    // Tuck it in behind the other cards
                    cardBack.SortingOrder = insertTransform.GetSiblingIndex() + 1;
                    cardBack.transform.rotation = insertTransform.rotation;
                    // Move it to it's insert
                    cardBack.transform.DOLocalMove(Vector3.zero, 0.15f)
                        .OnComplete(
                            () =>
                            {
                                // Make the card a child of Hand layout, have it block raycasts and have a proper sort order
                                cardBack.transform.AttachToParent(this.transform, insertTransform.GetSiblingIndex(), sortingOrder: cardBack.transform.GetSiblingIndex() + 1, blocksRaycasts: true);
                                insertTransform.SetParent(this.insertsRoot);
                                insertTransform.gameObject.SetActive(false);
                                this.insertTransforms.Enqueue(insertTransform);
                                insertTransform.DOKill();
                                this.HandLayout.UpdateCardTweens();
                                // The hand knows about the cardBack so it's no longer "incomming"
                                this.incommingCards.Remove(cardBack.CardId);
                            });
                }

            });
        this.DrawCardSound.Play();
    }
    
    /// <summary>
    /// Called by the game to deal a card from the deck to be place in the hand.
    /// </summary>
    public void DealCardBackFromDeck(int cardId)
    {
        //RectTransform currentInsert = null;
        if (this.insertTransforms.Count <= 1)
        {
            Debug.LogError("#OpponentHand#OUT OF INSERTS AGAIN! TODO: Find where these are going");
			AddInsertTransform ();
        }

        // Get an insert transform to use with this card deal aniamtion
        RectTransform currentInsert = this.insertTransforms.Dequeue();
        CardController cardBack = this.ArenaCards.GetOrCreateCardBack(cardId);
        cardBack.CanvasGroup.alpha = 0;
        this.incommingCards.Add(cardBack.CardId);
        bool isFastDeal = true;// this.GameState.Turn.Phase != Phase.PreAction;
        this.CardDealer.DealCard(() => OnAddInsert(currentInsert), (c) => OnCardDealt(c, currentInsert), cardBack, isFastDeal);
    }
    
    /// <summary>
    /// Get's a TargetArrowTarget associated with the specified id
    /// </summary>
    private TargetArrowTarget GetTarget(int id)
    {
        CardController card = this.ArenaCards.GetCard(id);
        
        if(card != null)
        {
            return card.TargetArrowTarget;
        }
        else if(this.ArenaCards.CardsWaitingToBeRecycled.ContainsKey(id))
        {
            return this.ArenaCards.CardsWaitingToBeRecycled[id].TargetArrowTarget;
        }
        else
        {
            PlayerControllerBase player = this.ArenaPlayers.GetPlayer(id);
            if(player != null)
            {
                return player.TargetArrowTarget;
            } 
        }
        
        return null;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        
        // =============================
        // State Transition Pump
        // =============================
        if(this.pendingHandActions.Count > 0 && this.isLastActionComplete == true)
        {
            InputActionParams actionParams = this.pendingHandActions.Dequeue();
            // Perform State Transitions
            this.currentState = this.DoStateTransition(this.currentState, actionParams);
        }
    }
    
    void LateUpdate()
    {
        // Make cards that are above inserts be exactly above them.
        for(int i=0;i<this.HandLayout.transform.childCount;i++)
        {
            Transform child = this.HandLayout.transform.GetChild(i);
            CardController cardController;
            if(this.cardInserts.TryGetValue((RectTransform)child, out cardController))
            {   
                if(/*cardController != this.draggedCard && */this.incommingCards.Contains(cardController.CardId) == false)
                {
                    this.UpdateCardXToMatchInsertX(cardController,child);
                    cardController.transform.rotation = child.transform.rotation;
                }
            }
        }
    }
    
    void UpdateCardXToMatchInsertX(CardController card, Transform insert)
    {
        if(insert.parent == this.transform)
        {
            Vector2 localPoint;
            Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, insert.position);
            RectTransform cardParentTransform = (RectTransform)card.transform.parent;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(cardParentTransform, screenPoint, Camera.main, out localPoint);
            Vector3 localPos = card.transform.localPosition;
            localPos.x = localPoint.x;
            card.transform.localPosition = localPos;
        }
    }
    
     /// <summary>
    /// Handles moving from one state to another based on an InputEvent and a reference card.
    /// See the state transitions list set up in Start() for details
    /// </summary>
    private HandState DoStateTransition(HandState currentState, InputActionParams actionParams)
    {
        Transition currentTransition = this.transitions.Where(s=>s.FromState==currentState && s.InputAction==actionParams.InputAction).FirstOrDefault();
        if(currentTransition != null)
        {
            this.isLastActionComplete = false;
            Debug.LogFormat("#OpponentHandTransition# {0}->{1}->{2}", this.currentState, currentTransition.InputAction, currentTransition.ToState);
            if(currentTransition.TransitionFunction != null)
            {
                currentTransition.TransitionFunction.Invoke(actionParams);
            }
            currentState = currentTransition.ToState;
        }
        return currentState;
    }

    private TargetedPowerCardInfo TweenPowerToActivePowers(CardFrontController powerCard, TargetArrowTarget target = null)
    {
        float parentScaleFactor = PlayedPowersLocation.lossyScale.x / powerCard.transform.parent.lossyScale.x;
        // Spawn a card to appear over top of the hand/cardback
        //var parent = powerCard.transform.parent.sca;
        //powerCard.transform.AttachToParent(PlayedPowersLocation, PlayedPowersLocation.childCount);
        
        //powerCard.Canvas.sortingLayerName = "Overlay";
        powerCard.SortingOrder = 95;

        //Debug.LogFormat("#OpponentHand#Setting power to position {0} Existing card is:{1}", powerCard.transform.position, powerCard.gameObject);
        powerCard.transform.localScale = Vector3.one * 1.2f * parentScaleFactor;    // Set the correct scale
        powerCard.CanvasGroup.alpha = 1f;                       // Make sure it's visible
        powerCard.RaycastTag = "Untagged";                      // Make sure it doesn't respond to mouseovers
        powerCard.ResetGlows();                                 // Strip away any card glows

        //powerCard.transform.AttachToParent(parent, 0);

        float cardSizeDeltaY=((RectTransform)powerCard.Card.transform).sizeDelta.y;
        float anchorY = ((RectTransform)this.SessionContext.MainCanvas.transform).sizeDelta.y - cardSizeDeltaY + 90f; // was +40f when cards were @1.5. Now +90f.

        ((RectTransform)powerCard.transform).DOBlendableLocalMoveBy(new Vector3(0,-100f),0.3f)//.SetEase(Ease.OutBounce)//.DOAnchorPosY(anchorY+cardSizeDeltaY*0.5f, 0.3f) 
            .OnComplete(()=>{
                ((RectTransform)powerCard.transform).DOAnchorPosY(anchorY,0.5f).SetEase(Ease.OutBack);
            });

        powerCard.CardGlowAnimator.SetBool("IsPowerCardFloating",true);


        TargetedPowerCardInfo targetInfo = new TargetedPowerCardInfo { Card = powerCard };
        this.targetedPowerCards[powerCard.CardId] = targetInfo;

        // If the power card has a target, create a target arrow and tween it over to it
        if(target != null)
        {
            // Make the staring position of the target arrow from the center of the power card (for tweening)
            Vector3 startPos = powerCard.transform.position + new Vector3(0,0,-1f);

            // Make a curved Red Arrow
            TargetArrowInfo arrowInfo = this.TargetArrowController.RequestTargetArrowInfo(powerCard.transform, startPos, powerCard.transform);
            arrowInfo.ArrowType = ArrowType.Curved;
            if(this.resolvedCards.Contains(powerCard.CardId))
            {
                // Use the default Red color on the target arrow
            }
            else
            {
                // Use a faded out red color while we wait for resolve
                arrowInfo.StartingColor = new Color(255/255f,23/255f, 23/255f,80/255f);
            }

            // Store it in the targetedPowerCardInfo
            targetInfo.Arrow = arrowInfo;

            // Tween the tip of the arrow over to the target
            Sequence seq = DOTween.Sequence();
            seq.Append(
                DOTween.To(()=>arrowInfo.Destination, pos=>arrowInfo.Destination=pos, target.BoundsRect.position, 0.25f )
            );
            seq.AppendCallback(()=>{
                   //Debug.Log("OPPONENT POWER PLAYED!");
                   arrowInfo.FollowDestination = target.BoundsRect;
                });
            seq.AppendInterval(2.0f);
            seq.AppendCallback(()=>{
                    this.TargetArrowController.FadeOutAndReleaseTargetArrow(arrowInfo,0.5f);
                });
            seq.Play();
        }

        // Fade out our newly created power card
        this.StartCoroutine(this.FadeOutPowerCard(powerCard.CardId));

        // Return a reference to the created power card
        return targetInfo;
    }

    private IEnumerator FadeOutPowerCard(int cardId)
    {
        //Debug.LogFormat("#OpponentHand# FadeOutPowerCard {0}", targetedCardInfo.Card);
        float timeStarted = Time.unscaledTime;
        yield return new WaitForSecondsRealtime(3f);

        TargetedPowerCardInfo targetedCardInfo = null;
        if (this.targetedPowerCards.TryGetValue(cardId, out targetedCardInfo) && targetedCardInfo.Card != null)
        {
            AnimatePowerCardToTrash(cardId, targetedCardInfo);
        }
    }

    private void AnimatePowerCardToTrash(int cardId, TargetedPowerCardInfo targetedCardInfo)
    {
        var card = targetedCardInfo.Card;
        Sequence seq = DOTween.Sequence();
        seq.Append(card.transform.DOMove(this.ArenaView.OpponentTrashIcon.transform.position, 0.8f).SetEase(Ease.OutQuint));
        seq.InsertCallback(0.2f, () => this.ArenaView.TrashCardSound.Play());
        seq.Insert(0.2f, card.CanvasGroup.DOFade(0.2f, 0.55f));
        seq.Insert(0.2f, card.transform.DOScale(0.25f, 0.5f));
        seq.AppendCallback(() =>
        {
            if (this.targetedPowerCards.TryGetValue(cardId, out targetedCardInfo) && targetedCardInfo.Card != null)
            {
                // Remove any state we might be tracking about this cards resolution
                this.resolvedCards.Remove(targetedCardInfo.Card.CardId);

                this.targetedPowerCards.Remove(targetedCardInfo.Card.CardId);
                targetedCardInfo.Card.transform.DOKill();
                this.ActivePowersLayout.ReleasePowerCard(targetedCardInfo.Card);
                //Debug.LogFormat("Recycling opponent power {0}", targetedCardInfo.Card.TitleLabel.text);
                //((CardFrontController)targetedCardInfo.Card).Canvas.sortingLayerName = "Default";
                ((CardFrontController)targetedCardInfo.Card).Recycle();
            }
            this.ActivePowersLayout.ShowTray(this.ActivePowersLayout.transform.childCount);
        });
        //targetedCardInfo.Card.CanvasGroup.DOFade(0, 0.5f).SetEase(Ease.InExpo);
        //yield return new WaitForSecondsRealtime(0.5f);

    }
}
