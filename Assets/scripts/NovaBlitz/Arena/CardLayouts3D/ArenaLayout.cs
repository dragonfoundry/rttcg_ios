﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class ArenaLayout : MonoBehaviour 
{
    // =====================
	// Public Properties
	// =====================
    public float SeparationRangeLow = 310f;
    public float SeparationRangeHigh = 360f;
    public int MaxCards = 6;
    public RectTransform InsertForDraggedCard = null;
    public float MaxSeparation
    {
        get 
        {
            // Scale the  MaxSeparation between cards based on the number of children
            float delta = (this.SeparationRangeHigh - this.SeparationRangeLow);
            float scale = Mathf.Min(this.transform.childCount / (float)this.MaxCards, 1.0f);
            return this.SeparationRangeHigh - delta*scale;
        }
    }
    public float Width { get { return ((RectTransform)transform).sizeDelta.x;}}
    
    // =====================
	// Private Properties
	// =====================
    
    [ContextMenu("UpdatePosition()")]
	/// <summary>
	/// Positions in local coordinates a list of cards relative to one another
	/// </summary>
	public void UpdateCardTweens()
	{
        //List<string> names = new List<string>();
        // Apply child tweens from left to right where siblingIndex determins position
		for(int i=0;i<this.transform.childCount;i++)
		{
            Transform child = this.transform.GetChild(i);
            if(child != this.InsertForDraggedCard)
            {
                var cardcontroller = child.GetComponent<CardController>();
                if (cardcontroller != null && cardcontroller.RaycastTag == "arenaCard")
                {
                    float xPosition = this.GetXPositionForSibling(i);
                    //names.Add(string.Format("{0}:{1}>{2}", i, cardcontroller.CardId,xPosition));
                    Vector3 position = new Vector3(xPosition, 0, 0);
                    child.transform.DOLocalMove(position, 0.2f).SetEase(Ease.InOutQuad);
                    child.localRotation = Quaternion.identity;
                }
                //else
                    //names.Add(string.Format("{0}:{1}", i, "null"));
            }
        }
        //Debug.LogFormat("updating tweens for {0} children: {1}", this.transform.childCount, string.Join(",",names.ToArray()));
    }
    
    /// <summary>
    /// For a given world position, and if there are "numSiblings" cards in the arena
    /// Calculate the arena index that would be below that world position.
    /// </summary>
    public int CalculateSiblingIndex(Vector3 worldPos, int numSiblings)
    {
        Vector3 localPos = this.transform.InverseTransformPoint(worldPos);
        
        int closestIndex = -1;
        float closestDistance = float.MaxValue;
        
        // find the correct sibling index for the insert
        for(int i=0;i<numSiblings;i++)
        {
            float targetXPos = this.GetXPositionForSibling(i,numSiblings);
            float distance = Mathf.Abs(targetXPos - localPos.x);
            if(distance < closestDistance)
            {
                closestDistance = distance;
                closestIndex = i;
              
            }
        }
        return Mathf.Max(0,closestIndex);
    }
    
    /// <summary>
    /// Calculate the arena sibling index based on the provided world pos, use 
    /// the current sibling count in the arena.
    /// </summary>
    public int CalculateSiblingIndex(Vector3 worldPos)
    {
        return this.CalculateSiblingIndex(worldPos, this.transform.childCount);
    }
    
     /// <summary>
    /// Given a card index, get its normalized position on an animation curve between 0.0-1.0
    /// </summary>
	public float GetXPositionForSibling(int siblingIndex)
	{
		return this.GetXPositionForSibling(siblingIndex, this.transform.childCount);
	}
    
    /// <summary>
    /// Calculate the horizonta position for a sibling in the arena layout
    /// </summary>
    public float GetXPositionForSibling(int siblingIndex, int childCount)
    {
        // Calculate the position of the first (left most) card in the areba based
        // on the origin being the left edge of the arena.
        float separation = childCount <= 0 ? 0 : this.MaxSeparation; // No seperation with 0 cards
		float totalCardSeparation = childCount * separation;
		float xInitial = (this.Width - totalCardSeparation) / 2f + separation * 0.5f;
        
        // Account for a center pivot
        xInitial = xInitial - this.Width * 0.5f;
        
        float xPositoin = xInitial + (siblingIndex * separation);
        return xPositoin;
    }
}
