﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using TMPro;
using DG.Tweening;
using LinqTools;
using System.Collections.Generic;
using System;
using strange.extensions.mediation.impl;
using Messages;
using NovaBlitz.Game;
using DragonFoundry.GameState;

public class PlayerArenaController : View
{
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public GameControllerService GameControllerService {get;set;}
    [Inject] public ArenaPlayers ArenaPlayers {get;set;}
    
    public ArenaLayout ArenaLayout;
    public ArenaZoomController ArenaCardZoom;
    public RectTransform InsertTransform;
    public TargetArrowController TargetArrowController;
    public InputEventController InputEventController;
    public RectTransform DragArrowsRoot;
    public GameStateReadOnly GameState { get; set; }
    public PlayerHandController PlayerHandController;

    public NovaAudioPlayer DeclareAttackSound;
    public NovaAudioPlayer DeclareBlockSound;
    public NovaAudioPlayer ActivateSound;
    public NovaAudioPlayer PlayCardSound;
    
    
    // Privates
    private Phase turnPhase;
    private Queue<RectTransform> insertTransforms = new Queue<RectTransform>();
    private Transform insertsRoot;
    private CardController cardBeingPlayed = null;
    private const float DRAG_UP_THRESHOLD = 15;


    // State transition data
    private List<Transition> transitions = new List<Transition>();
    private CardController draggedCard = null;
    private ArenaState currentState = ArenaState.DefaultState;
    public ArenaState CurrentState { get { return currentState; } }
    
    // Controller Actions
    public System.Action<ICardState> CardAttacks; // SourceId
    public System.Action<ICardState, int> CardBlocks; // SourceId, TargetId
    public System.Action<ICardState, int?> CardActivates; // SourceId, TargetId (untargeted = 0 or less)
    public Action<IEnumerable<ICardState>, bool> ShowTargets;
    public Action HideTargets;

    // Input Event state data
    private Dictionary<InputEvent, bool> inputEvents = new Dictionary<InputEvent,bool>();
    private Dictionary<InputEvent, CardController> inputCards = new Dictionary<InputEvent,CardController>();
    private float? mouseDownTime;
    private Vector2 mousePositionLastFrame;
    private Dictionary<CardController,bool> playedCards = new Dictionary<CardController,bool>();
    
    public CardFrontController DraggedCard
    {
        get { return (CardFrontController)this.draggedCard;}
    }

    public enum ArenaState
    {
        DefaultState = 0,
        CardHovered = 1,
        CardZoomed = 2,
        CardPressed = 3,
        CardSelected = 4,
        CardDragged = 6,
        ArrowDragged = 7,
        FakeArrowDragged = 8
    }
    
    class Transition
    {
        public Transition(ArenaState fromState, InputEvent inputEvent, ArenaState toState, System.Action<CardController> transitionFunction = null)
        {
            this.FromState = fromState;
            this.ToState = toState;
            this.InputEvent = inputEvent;
            this.TransitionFunction = transitionFunction;
        }
        public ArenaState FromState;
        public ArenaState ToState;
        public InputEvent InputEvent;
        public System.Action<CardController> TransitionFunction;
    }
    
    protected override void Start()
    {
        base.Start();

        // TODO: update this so it actually matches transitions
        // TODO: Add state for TargetArrow Dragging
        /*
                          _________________________________________________________________________________________________________
                          |   DefaultState  |   CardHovered   |  CardDragged   |  CardZoomed    |  CardSelected  |  ArrowDragged  | < STATES
        __________________|_________________|_________________|________________|________________|________________|________________|
        |<MouseOverCard>  |   CardHovered   |   CardHovered   |       X        |  CardHovered   |  CardSelected  |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<MouseOutCard>   |        X        |   DefaultState  |       X        |  DefaultState  |  CardSelected  |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<CardDragBegin>  |   CardDragged   |   CardDragged   |                |                |                |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<CardDragEnd>    |                 |                 |  DefaultState  |                |                |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<ZoomInCard>     |        X        |   CardZoomed    |       X        |       X        |                |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<MouseDown>      |        X        |  CardSelected   |       X        |  CardSelected  |                |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<MouseClicked>   |        X        |        X        |       X        |       X        |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<MouseUpArena>   |        X        |        X        |       X        |  CardSelected  |                |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<MouseOut>       |        X        | *DefaultState*  |       X        |       X        |                |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<MouseOutWindow> |        X        | *DefaultState*  |       X        |       X        |                |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<ArrowDragged>   |                 |                 |       X        |       X        |  ArrowDragged  |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<MouseClickCard> |                 |                 |       X        |       X        |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<PhaseEnded>     |                 |                 |       X        |       X        |  DefaultState  |
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<LostCombatAbility>|               |                 |       X        |       X        |  DefaultState  |  DefaultState
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        |<LostActivatedAbility>|            |                 |       X        |       X        |  DefaultState  |  DefaultState
        |-----------------|-----------------|-----------------|----------------|----------------|----------------|----------------|
        
            ^ INPUTS
        */
        this.transitions.Clear();
        
        // From ArenaState.DefaultState
       // this.transitions.Add(new Transition(ArenaState.DefaultState, InputEvent.MouseOverCard, ArenaState.CardHovered,this.OnCardHovered));
        this.transitions.Add(new Transition(ArenaState.DefaultState, InputEvent.CardDragBegin, ArenaState.CardDragged, null));
        this.transitions.Add(new Transition(ArenaState.DefaultState, InputEvent.MouseUpPlayerCard, ArenaState.CardSelected, this.OnSelectCard));
        
        // From ArenaState.CardDragged
        this.transitions.Add(new Transition(ArenaState.CardDragged, InputEvent.CardDragEnd, ArenaState.DefaultState, null));
        
        // From ArenaState.CardZoomed
        this.transitions.Add(new Transition(ArenaState.CardZoomed, InputEvent.MouseUpPlayerCard, ArenaState.CardSelected, this.OnSelectCard));
        this.transitions.Add(new Transition(ArenaState.CardZoomed, InputEvent.MouseUpArena, ArenaState.CardSelected, this.OnSelectCard));
        
        // From ArenaState.CardSelected
		this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.MouseUp, ArenaState.DefaultState, this.OnTryAbilityAndDeselect));
		this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.MouseUpArena, ArenaState.DefaultState, this.ReturnDraggedToDefault));
        this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.MouseUpPlayerCard, ArenaState.CardSelected, this.OnCardReselect));
        this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.MouseOutSourceCard, ArenaState.FakeArrowDragged, this.OnDragArrow));
        this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.ArrowDragged, ArenaState.ArrowDragged, this.OnDragArrow ));
        this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.LostCombatAbility, ArenaState.DefaultState, this.OnReturnToDefault));
        this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.LostActivatedAbility, ArenaState.DefaultState, this.OnReturnToDefault));
		this.transitions.Add(new Transition(ArenaState.CardSelected, InputEvent.PhaseEnded, ArenaState.DefaultState, this.OnReturnToDefault));


        // From ArenaState.ArrowDragged
        this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.MouseOverSourceCard, ArenaState.DefaultState, this.CancelDrag));
        this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.MouseUpArena, ArenaState.DefaultState, this.ReturnDraggedToDefault));
        this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.MouseUpCard, ArenaState.DefaultState, this.ReturnDraggedToDefault));
		this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.MouseUpHand, ArenaState.DefaultState, this.ReturnDraggedToDefault));
		this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.PhaseEnded, ArenaState.DefaultState, this.CancelDrag));
        this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.LostCombatAbility, ArenaState.DefaultState, this.CancelDrag));
        this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.LostActivatedAbility, ArenaState.DefaultState, this.CancelDrag));

        // From ArenaState.FakeArrowDragged
        //this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.MouseUp, ArenaState.DefaultState, this.ReturnDraggedToDefault));
        this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.MouseOverSourceCard, ArenaState.DefaultState, this.CancelDrag));
        this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.MouseUpArena, ArenaState.DefaultState, this.ReturnDraggedToDefault));
        this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.MouseUpCard, ArenaState.DefaultState, this.ReturnDraggedToDefault));
        this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.MouseUpHand, ArenaState.DefaultState, this.ReturnDraggedToDefault));
        this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.PhaseEnded, ArenaState.DefaultState, this.CancelDrag));
        this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.LostCombatAbility, ArenaState.DefaultState, this.CancelDrag));
		this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.LostActivatedAbility, ArenaState.DefaultState, this.CancelDrag));
        //this.transitions.Add(new Transition(ArenaState.FakeArrowDragged, InputEvent.MouseOverHand, ArenaState.DefaultState, this.CancelDrag));
        //this.transitions.Add(new Transition(ArenaState.ArrowDragged, InputEvent.MouseOutWindow, ArenaState.CardSelected, this.OnCardReselect));

        // Create a new root transform for ineserts
        this.insertsRoot = new GameObject().transform;
		this.insertsRoot.name = "InsertsFor_" + this.name;
        
        // Attach the default insert to the new root
		this.InsertTransform.SetParent(this.insertsRoot);
		this.InsertTransform.gameObject.SetActive(false);
        
        this.insertTransforms.Enqueue(this.InsertTransform);
		this.InsertTransform.name = "Insert 0";
		
        // Create a few copies of the default insert
		for(int i=1;i<20;i++)
		{
			AddInsertTransform ();
		}
    }

	void AddInsertTransform()
	{
		RectTransform trans = GameObject.Instantiate(this.InsertTransform);
		trans.SetParent(this.transform);
		trans.localScale = Vector3.one;
		trans.localPosition = Vector3.zero;
		trans.name = "Insert " + insertTransformNumber;
		this.insertTransforms.Enqueue(trans);
		trans.SetParent(this.insertsRoot);
		insertTransformNumber++;
	}

	int insertTransformNumber = 1;

    public void RecycleInsert(RectTransform playedCardInsert)
    {
        playedCardInsert.DOKill();
        playedCardInsert.AttachToParent(this.insertsRoot, 0);
        this.insertTransforms.Enqueue(playedCardInsert);
        playedCardInsert.gameObject.SetActive(false);
        //Debug.LogFormat("Recycling insert? {0}", this.ArenaLayout.InsertForDraggedCard == playedCardInsert ? playedCardInsert.gameObject.name : "doing nothing");
        if (this.ArenaLayout.InsertForDraggedCard == playedCardInsert)
        {
            this.ArenaLayout.InsertForDraggedCard = null;
        }
    }

    public void OnLegalAbilitiesUpdated(GameStateReadOnly gameState)
    {
        CardFrontController selectedCard = (CardFrontController)this.InputEventController.SelectedCard;
       // Debug.LogFormat("#PlayerArena# LegalAbilitiesUpdated selected card: {0}", selectedCard.CardId);
        if(selectedCard != null)
        {
            //selectedCard.DisplayLegalAbilities(gameState.LegalAbilities);
            
            switch(this.turnPhase)
            {
                case Phase.Combat:
                    bool hasCombatAbility = selectedCard.CanAttack || selectedCard.CanBlock;
            
                    if(hasCombatAbility == false)
                    {
                        Debug.LogFormat("#PlayerArena# LegalAbilitiesUpdated Lost Combat Ability in phase {0}", this.turnPhase);
                        this.TriggerInput(InputEvent.LostCombatAbility, selectedCard);
                        this.ClearTargets();
                    }
                break;
                case Phase.Action:
                    bool hasActivatedAbility = selectedCard.IsActionsActive;
                    bool isTargeted;
                    List<int> activateTargets = this.GameControllerService.GetActivateTargets(selectedCard.CardId, out isTargeted);
                    //Debug.LogFormat("#PlayerArena# LegalAbilitiesUpdated hasActivatedAbility: {0} targets:{1}", hasActivatedAbility, activateTargets);
                    if(hasActivatedAbility == true && activateTargets == null)
                    {
                        Debug.LogFormat("#PlayerArena# LegalAbilitiesUpdated Lost Activated Ability in phase {0}", this.turnPhase);
                        this.TriggerInput(InputEvent.LostActivatedAbility, selectedCard);
                        this.ClearTargets();
                    }
                break;
            }

            //refresh targets
            this.RefreshTargets();
            // We probably don't need to refresh targets - the card will be "off", so won't be dragged until new info comes in.
            //RefreshTargets();
        }
        /*
        var cardControllers = ArenaCards.AllCardControllers;
        for (int i = 0; i < cardControllers.Count(); i++)
        {
            cardControllers[i].RefreshLegalAbilities();
        }*/
    }
    
    internal void DoneWithTurnClicked()
    {
        this.ClearTargets();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (insertsRoot != null)
            Destroy(insertsRoot.gameObject);
    }

    public void AttackWithCard(ICardState cardState)
    {
        this.ClearTargets();;
        
        if(this.CardAttacks != null)
        {
            this.CardAttacks.Invoke(cardState);
        }
    }
    
    public void BlockWithCard(ICardState cardState, int targetId)
    {
        this.ClearTargets();
        
        if(this.CardBlocks != null)
        {
            this.CardBlocks.Invoke(cardState, targetId);
        }
    }


    public void ActivateCard(ICardState cardState, int? targetId)
    {
        if(targetId.HasValue)
        {
            CardController activatdCard = this.ArenaCards.GetCard(cardState.Id);
            CardController targetCard = this.ArenaCards.GetCard(targetId.Value);
            TargetArrowTarget target = null;
            
            if(targetCard != null )
            {
                target = this.ArenaCards.GetCard(targetId.Value).TargetArrowTarget;
            }
            else
            {
                target = this.ArenaPlayers.GetPlayer(targetId.Value).TargetArrowTarget;
            }

            // Make a curved Ability Arrow
            Transform arrowParent = this.DragArrowsRoot;
            TargetArrowInfo arrowInfo = this.TargetArrowController.RequestTargetArrowInfo(activatdCard.transform, target.BoundsRect, arrowParent);
            arrowInfo.ArrowType = ArrowType.Curved;
            arrowInfo.StartingColor = new Color(228/255f,147/255f,254/255f,255/255f);

            // Tween the tip of the arrow over to the target
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1.5f);
            seq.AppendCallback(()=>{
                    this.TargetArrowController.FadeOutAndReleaseTargetArrow(arrowInfo, 0.5f);
                });
        
            seq.Play();
        }


        this.ClearTargets();

        if (this.CardActivates != null)
        {
            this.CardActivates.Invoke(cardState, targetId);
        }
    }

    private void OnCardReselect(CardController activeCard)
    {
        if(activeCard != null)
        {
            if(activeCard != this.InputEventController.SelectedCard)
            {
                this.ReturnCardToDefaultArenaState(this.InputEventController.SelectedCard);
                this.ClearTargets();
            }

            if(this.PlayerHandController.IsHandZoomedIn)
            {
				this.PlayerHandController.MustSetZoomHandActiveTo = false;
            }
            
            this.InputEventController.SelectedCard = activeCard;
            this.InputEventController.SelectedCard.ActivateActions();
            if(this.InputEventController.SelectedCard.CanAttack)
            {
                // Make the opponent an attackable target
                this.ShowAttackTargets(activeCard.CardId);
            }
            if(this.InputEventController.SelectedCard.CanBlock)
            {
                this.ShowBlockTargets(activeCard.CardId);
            }
        }
        
        if(this.InputEventController.IsArrowDragging)
        {
            this.TargetArrowController.ReleaseDraggedArrow();
        }
    }
    /*
    internal void TryAddCardToActivatedAbilityTargets(CardController playedCard)
    {
        if (this.InputEventController.SelectedCard != null && this.InputEventController.SelectedCard.IsActionsActive)
        {
            bool isTargeted;
            List<int> activateTargets = this.GameControllerService.GetActivateTargets(this.InputEventController.SelectedCard.CardId, out isTargeted);

            if(activateTargets != null && activateTargets.Contains(playedCard.CardId))
            {
                Debug.Log("#PlayerArena# Adding target to card!");
                if(isTargeted)
                {
                    playedCard.TargetArrowTarget.TargetGlowAnimator.SetBool("IsCastTarget", true);
                    playedCard.TargetArrowTarget.TargetAction = TargetAction.Activate;
                }
                else
                {
                    playedCard.TargetArrowTarget.TargetGlowAnimator.SetBool("IsAffected", true); 
                    playedCard.TargetArrowTarget.TargetAction = TargetAction.Affected;
                }
                this.InputEventController.TargetArrowController.AddTarget(playedCard.TargetArrowTarget);
            }
        }
    }*/

    private void OnDeselect(CardController activeCard)
    {
        if(this.InputEventController.SelectedCard != null)
        {
            this.ReturnCardToDefaultArenaState(this.InputEventController.SelectedCard);
            this.ClearTargets();
            if(this.InputEventController.SelectedCard.CanFight)
            {
                this.InputEventController.SelectedCard.CardGlowAnimator.SetBool("IsCombatActive", true);
            }
            this.InputEventController.SelectedCard = null;
        }
    }
    
    private void OnTryAbilityAndReselect(CardController activeCard)
    {
        
    }
    
    private void OnTryAbilityAndDeselect(CardController activeCard)
    {
        CardController selectedCard = this.InputEventController.SelectedCard;
        if(activeCard != null)
        {
            TargetArrowTarget target = activeCard.TargetArrowTarget;
            
            if(selectedCard.CanBlock && target.TargetAction == TargetAction.Block)
            {
                List<int> blockTargets = this.GameControllerService.GetBlockTargets(selectedCard.CardId);
                if(blockTargets != null && blockTargets.Contains(activeCard.CardId))
                {
                    //Debug.LogFormat("Try Bock and deselect of {0}", activeCard.TitleLabel.text);
                    selectedCard.BlockTarget(activeCard.TargetArrowTarget, this.TargetArrowController);
                }
            }
            else if(selectedCard.CanActivate && selectedCard.CanTargetAbility && target.TargetAction == TargetAction.Activate)
            {
                //Debug.LogFormat("Can Activate... {0}", activeCard.TitleLabel.text);
                bool isTargeted;
                List<int> activateTargets = this.GameControllerService.GetActivateTargets(selectedCard.CardId, out isTargeted);
                if(isTargeted && activateTargets != null && activateTargets.Contains(activeCard.CardId))
                {
                    //Debug.LogFormat("Try Activate target of {0}", activeCard.TitleLabel.text);
                    selectedCard.ActivateTarget(activeCard.TargetArrowTarget, this.TargetArrowController);
                }
            }
        }
      
        
        // Continue with normal deselction
        this.ReturnCardToDefaultArenaState(this.InputEventController.SelectedCard);
        this.ClearTargets();
        this.InputEventController.SelectedCard = null;
    }
    
    private void CancelDrag(CardController activeCard)
    {
        this.TargetArrowController.ReleaseDraggedArrow();
        this.ReturnCardToDefaultArenaState(this.InputEventController.SelectedCard);
        this.ClearTargets();
        this.InputEventController.SelectedCard = null;
        var cardControllers = ArenaCards.AllCardControllers;
        for (int i = 0; i < cardControllers.Count(); i++)
        {
            cardControllers[i].DisplayLegalAbilities(GameState.LegalAbilities);
        }
    }

    private void ReturnDraggedToDefault(CardController activeCard)
    {
        if(this.InputEventController.IsArrowDragging)
        {
            TargetArrowTarget target = this.TargetArrowController.ReleaseDraggedArrow();
            
            if(target != null)
            {
				//Debug.LogFormat("Released dragged arrow on:{0} action:{1}", target.Id, target.TargetAction);
                
                if(target.TargetAction == TargetAction.Block)
                {
                    this.InputEventController.SelectedCard.BlockTarget(target, this.TargetArrowController);
                }
                else if(target.TargetAction == TargetAction.Attack)
                {
                    this.InputEventController.SelectedCard.AttackTarget(target, this.TargetArrowController);
                }
                else if (target.TargetAction == TargetAction.Activate)
                {
                    this.InputEventController.SelectedCard.ActivateTarget(target, this.TargetArrowController);
                }
            }
        }
        
        if(this.InputEventController.SelectedCard != null)
        {
            this.ReturnCardToDefaultArenaState(this.InputEventController.SelectedCard);
            this.ClearTargets();
            this.InputEventController.SelectedCard = null;
        }
    }
    
    private void ReturnCardToDefaultArenaState(CardController card)
    {
        if (card == null)
            return;
        
        card.TargetArrowTarget.TargetAction = TargetAction.None;
        card.CanvasGroup.interactable=false;
        card.CardGlowAnimator.SetBool("IsPlayable", false);
        card.CardGlowAnimator.SetBool("IsCombatActive", card.CanFight);
        card.CardGlowAnimator.SetBool("IsActivatable", card.CanActivate);
        card.CardGlowAnimator.SetBool("IsActivateTarget", false);
        card.CardGlowAnimator.SetBool("IsCastTarget", false);
        card.CardGlowAnimator.SetBool("IsAffected", false);
        card.CardGlowAnimator.SetBool("IsHovered", false);
        card.CardGlowAnimator.SetBool("IsSelected", false);
        card.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        card.transform.DORotate(Vector3.zero, 0.1f);

        card.AttackToggleButton.gameObject.SetActive(false);
        card.AbilityToggleButton.gameObject.SetActive(false);
        
        if(card.transform.localPosition.z != 0)
        {
            card.transform.DOLocalMoveZ(0, 0.15f);
        }
        card.SortingOrder = 1;
        ((RectTransform)card.transform).DOAnchorPosY(173,0.1f);
        card.transform.DOScale(new Vector3(1.5f,1.5f,1.5f), 0.1f);
    }

    private void OnDragArrow(CardController activecard)
    {
        // remove the active card from its own targets
        this.TargetArrowController.RemoveTarget(activecard.TargetArrowTarget);
        this.TargetArrowController.ClearTarget(activecard.TargetArrowTarget);
        //activecard.CanvasGroup.interactable = false;
        activecard.AttackToggleButton.gameObject.SetActive(false);
        activecard.AbilityToggleButton.gameObject.SetActive(false);

        // draw the arrow
        //Debug.LogFormat("OnDragArrow - ActiveCard is :{0}", (activecard != null ? activecard.name : "null"));
        activecard.AttackToggleButton.isOn = false;
        activecard.AbilityToggleButton.isOn = false;
        this.TargetArrowController.DragArrow(activecard.transform, this.DragArrowsRoot);
    }

    public void OnSelectCard(CardController activeCard)
    {
        //Debug.Log(activeCard.name);
        
        CardController wasSelected = this.InputEventController.SelectedCard;
        if(this.InputEventController.SelectedCard != null)
        {
            this.ReturnCardToDefaultArenaState(this.InputEventController.SelectedCard);
            this.ClearTargets();
            this.InputEventController.SelectedCard = null;
        }
        
        if(activeCard != null && activeCard != wasSelected)
        {
            if(this.PlayerHandController.IsHandZoomedIn)
            {
				this.PlayerHandController.MustSetZoomHandActiveTo = false;
            }
            
            this.InputEventController.SelectedCard = activeCard;
            this.InputEventController.SelectedCard.ActivateActions();
            RefreshTargets();
        }
    }

    public void RefreshTargets()
    {
        if (this.InputEventController.SelectedCard == null)
            return;
        this.ClearTargets();
        //this.InputEventController.SelectedCard.ActivateActions();

        if (this.InputEventController.SelectedCard.CanAttack)
        {
            // Make the opponent an attackable target
            this.ShowAttackTargets(InputEventController.SelectedCard.CardId);
        }

        if (this.InputEventController.SelectedCard.CanBlock)
        {
            // Identify block target units
            this.ShowBlockTargets(InputEventController.SelectedCard.CardId);
        }

        if (this.InputEventController.SelectedCard.CanActivate)
        {
            // Show targets for Activation if the card has an activated ability
            this.ShowActivateTargets(InputEventController.SelectedCard.CardId);
        }
    }

    private void ClearTargets()
    {
        this.TargetArrowController.ClearTargets();
        /*if (this.InputEventController.SelectedCard == null && this.HideTargets != null)
        {
            HideTargets.Invoke();
        }*/
    }

    public void OnPhaseEnded(CardController activeCard)
    {
        if(this.InputEventController.SelectedCard != null)
        {
            this.OnDeselect(this.InputEventController.SelectedCard);
        }
    }

    /*public void OnReturnToDefault(CardController activeCard)
    {
        if(activeCard == this.InputEventController.SelectedCard)
        {
            if(this.InputEventController.IsArrowDragging)
            {
                this.TargetArrowController.ReleaseDraggedArrow();
            }
            
            this.InputEventController.SelectedCard = null;
        }
        
        if(activeCard != null)
        {
            this.ReturnCardToDefaultArenaState(activeCard);
        }
    }*/

    public void OnReturnToDefault(CardController activeCard)
    {
        if (activeCard != this.InputEventController.SelectedCard)
        {
            Debug.LogWarning("Active Card and Selected card are different");
        }

        if (this.InputEventController.IsArrowDragging)
        {
            this.TargetArrowController.ReleaseDraggedArrow();
        }

        this.ClearTargets();
        this.InputEventController.SelectedCard = null;

        if (activeCard != null)
        {
            this.ReturnCardToDefaultArenaState(activeCard);
        }
    }

    public void OnZoomInCard(CardController activeCard)
    {
        this.ArenaCardZoom.OnZoomInCard(activeCard);
    }
    
    private void ShowActivateTargets(int sourceId)
    {
        bool isTargeted;
        List<int> activateTargets = this.GameControllerService.GetActivateTargets(sourceId, out isTargeted);
        if(activateTargets != null && activateTargets.Count > 0)
        {
            List<int> potentialTrashCardIds = new List<int>();
            // Targeted Abilities
            //CardController sourceCard = this.ArenaCards.GetCard(sourceId);
            for(int i=0;i<activateTargets.Count;i++)
            {
                var targetId = activateTargets[i];
                CardController activateTargetCard = this.ArenaCards.GetCard(targetId);
                if(activateTargetCard != null)
                {
                    SetTargetGlow(activateTargetCard, isTargeted);
                }
                else
                {
                    // Is the target the opponent
                    if (isTargeted && targetId == this.GameState.Opponent.Id)
                    {
                        this.ArenaPlayers.Opponent.TargetArrowTarget.TargetGlowAnimator.SetBool("IsCastTarget", true);
                        this.ArenaPlayers.Opponent.TargetArrowTarget.TargetAction = TargetAction.Activate;
                        this.InputEventController.TargetArrowController.AddTarget(this.ArenaPlayers.Opponent.TargetArrowTarget);
                    }
                    // is the target the player
                    else if (isTargeted && targetId == this.GameState.Self.Id)
                    {
                        this.ArenaPlayers.Player.TargetArrowTarget.TargetGlowAnimator.SetBool("IsCastTarget", true);
                        this.ArenaPlayers.Player.TargetArrowTarget.TargetAction = TargetAction.Activate;
                        this.InputEventController.TargetArrowController.AddTarget(this.ArenaPlayers.Player.TargetArrowTarget);
                    }
                    else
                        potentialTrashCardIds.Add(targetId);
                }
            }
            if(potentialTrashCardIds.Count > 0)
            {
                // Show trash cards; highlight them
            }
        }
    }

    private void SetTargetGlow(CardController card, bool isTargeted)
    {
        // Is the target a card?
        if (card.TargetArrowTarget.TargetGlowAnimator != null)
        {
            if (isTargeted)
            {
                card.TargetArrowTarget.TargetGlowAnimator.SetBool("IsCastTarget", true);
                card.TargetArrowTarget.TargetAction = TargetAction.Activate;
                card.CanvasGroup.interactable = true;
            }
            else
            {
                card.TargetArrowTarget.TargetGlowAnimator.SetBool("IsAffected", true);
                card.TargetArrowTarget.TargetAction = TargetAction.Affected;
            }
            this.InputEventController.TargetArrowController.AddTarget(card.TargetArrowTarget);
        }
    }
    
    private void ShowBlockTargets(int sourceId)
    {
        List<int> blockTargets = this.GameControllerService.GetBlockTargets(sourceId);
        if(blockTargets != null)
        {
            //CardController sourceCard = this.ArenaCards.GetCard(sourceId);
            for(int i=0;i<blockTargets.Count;i++)
            {
                CardController blockableCard = this.ArenaCards.GetCard(blockTargets[i]);
                if(blockableCard != null)
                {
                    if(blockableCard.TargetArrowTarget.TargetGlowAnimator != null)
                    {
                        blockableCard.TargetArrowTarget.TargetGlowAnimator.SetBool("IsBlockTarget", true);
                        blockableCard.TargetArrowTarget.TargetAction = TargetAction.Block;
                        this.InputEventController.TargetArrowController.AddTarget(blockableCard.TargetArrowTarget);
                        //blockableCard.CanvasGroup.interactable = true;
                    }
                }
            }
        }
    }
    
    private void ShowAttackTargets(int sourceId)
    {
        CardController sourceCard = this.ArenaCards.GetCard(sourceId);
        if(sourceCard.CanAttack)
        {
            sourceCard.TargetArrowTarget.TargetAction = TargetAction.Attack;
            this.InputEventController.TargetArrowController.AddFakeAttackTarget(sourceCard.TargetArrowTarget);
            Animator opponentGlowAnimator = this.ArenaPlayers.Opponent.TargetArrowTarget.TargetGlowAnimator;
            if(opponentGlowAnimator != null)
            {
                opponentGlowAnimator.SetBool("IsAttackTarget", true);
                this.ArenaPlayers.Opponent.TargetArrowTarget.TargetAction = TargetAction.Attack;   
                this.InputEventController.TargetArrowController.AddTarget(this.ArenaPlayers.Opponent.TargetArrowTarget);
            }
        }
    }
    
    void Update()
    {      
         // TEMP Card dragging stuff - Should handle multiple dragged cards? -DMac
        if(this.ArenaLayout.InsertForDraggedCard != null && this.draggedCard != this.cardBeingPlayed)
        {
            Vector3 worldPos;
            RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)this.transform, Input.mousePosition, Camera.main, out worldPos);
            int siblingIndex = this.ArenaLayout.CalculateSiblingIndex(worldPos);
            Vector3 pos = this.ArenaLayout.InsertForDraggedCard.localPosition;
            pos.x = this.ArenaLayout.GetXPositionForSibling(siblingIndex);
            this.ArenaLayout.InsertForDraggedCard.localPosition = pos;
            if(siblingIndex != this.ArenaLayout.InsertForDraggedCard.GetSiblingIndex())
            {
                this.ArenaLayout.InsertForDraggedCard.SetSiblingIndex(siblingIndex);
                this.ArenaLayout.UpdateCardTweens();
            }
        }
        
        // =============================
        // State Transition Pump
        // =============================
        if(this.inputEvents.Count > 0)
        {
            // Perform State Transitions
            foreach(InputEvent inputEvent in this.inputEvents.Keys)
            {
                bool isEventSet = false;
                if(this.inputEvents.TryGetValue(inputEvent, out isEventSet))
                {
                    if(isEventSet)
                    {
                        CardController activeCard = null;
                        this.inputCards.TryGetValue(inputEvent, out activeCard);
                        this.currentState = this.DoStateTransition(this.currentState, inputEvent, activeCard);
                    }
                }
            }
            this.inputEvents.Clear();
        }
    }
    

    /// <summary>
    /// Given the position of a card, adds an insert to the arena at the position below the card.
    /// and marks that inerst as the insertForDragged card. Stores a reference to the dragged card too.
    /// </summary>
    public void OnCardDragBegin(CardController cardController)
    {
        RectTransform insert = this.insertTransforms.Dequeue();
        insert.gameObject.SetActive(true);
        int siblingIndex = this.ArenaLayout.CalculateSiblingIndex(cardController.transform.position, this.ArenaLayout.transform.childCount+1);
        int sortOrder = Mathf.Max(1, siblingIndex);
        insert.AttachToParent(this.transform, siblingIndex, sortingOrder: sortOrder, overrideSorting: true);
        this.ArenaLayout.InsertForDraggedCard = insert;
        this.ArenaLayout.InsertForDraggedCard.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        Vector3 position = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, 0);
        insert.localPosition = position;
        this.ArenaLayout.UpdateCardTweens();
        this.draggedCard = cardController;
        this.TriggerInput(InputEvent.CardDragBegin, cardController);
    }

    public void OnCardDragEnd()
    {
        if (this.ArenaLayout.InsertForDraggedCard != null)
        {
            RecycleInsert(this.ArenaLayout.InsertForDraggedCard);
            this.ArenaLayout.UpdateCardTweens();
            this.TriggerInput(InputEvent.CardDragEnd, this.draggedCard);
            this.draggedCard = null;
        }
    }
    
    public void SpawnArneaCardFromSourceCard(CardController spawnCard, CardController sourceCard)
    {
        // If the source card is currently doing its "Play Card" tween, this waits for it to finish
        // to spawn the new card
        this.StartCoroutine(this.SpawnCardAnimation(spawnCard, sourceCard));
    }
    
    public void SpawnArenaCardFromTrash(CardController spawnCard)
    {
        //Debug.LogFormat("Spawning Card {0} from Trash", spawnCard.CardId);
        // Get an insert to use for the spawned card (on the far right of the arena)
        RectTransform currentInsert = this.insertTransforms.Dequeue();
        currentInsert.gameObject.SetActive(true);
        currentInsert.GetChild(0).gameObject.SetActive(false);
        int siblingIndex = this.transform.childCount;
        
        // Position the insert in the arena
        currentInsert.AttachToParent(this.transform, siblingIndex, overrideSorting: false);
        currentInsert.transform.localScale = Vector3.one;
        currentInsert.localPosition = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, 0);
        
         // Attached the spawned card to the insert
        spawnCard.transform.AttachToParent(currentInsert, siblingIndex, overrideSorting: true, blocksRaycasts:false );
        spawnCard.transform.localScale = new Vector3(1.6f,1.6f,1.6f);
        spawnCard.transform.localPosition = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, -500);
        
        // Move any other cards in the arena to new positions
        this.ArenaLayout.UpdateCardTweens();

        spawnCard.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        spawnCard.transform.DORotate(Vector3.zero, 0.2f);
        // Animate the spanwed card into place
        spawnCard.CanvasGroup.alpha = 0;
        spawnCard.CanvasGroup.DOFade(1f, 0.1f);
        spawnCard.transform.DOLocalMove(Vector3.zero, 0.2f).OnComplete(()=>
        {
            spawnCard.RaycastTag = "arenaCard";
            spawnCard.transform.AttachToParent(currentInsert.parent, currentInsert.GetSiblingIndex(), overrideSorting: false, blocksRaycasts:true);
            spawnCard.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            spawnCard.PlayerArenaController = this;
                
            // Return the insert to the inserts pool
            currentInsert.AttachToParent(this.insertsRoot, 0);
            currentInsert.gameObject.SetActive(false);
            currentInsert.GetChild(0).gameObject.SetActive(true);
            this.insertTransforms.Enqueue(currentInsert);
            this.PlayerHandController.ShowCastTargets();
            this.RefreshTargets();
        });
    }
    
    private IEnumerator SpawnCardAnimation(CardController spawnCard, CardController sourceCard)
    {
        // Wait for the source card to finish being played to the arena
        while (sourceCard == this.draggedCard)
        {
            yield return null;
        }
        
        // Get an insert to use for the spawned card
        RectTransform currentInsert = this.insertTransforms.Dequeue();
        currentInsert.gameObject.SetActive(true);
        currentInsert.GetChild(0).gameObject.SetActive(false);
        int siblingIndex = sourceCard.transform.GetSiblingIndex();
        
        // Position the insert in the arena
        currentInsert.AttachToParent(this.transform, siblingIndex, overrideSorting: false);
        currentInsert.transform.localScale = Vector3.one;
        currentInsert.localPosition = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex), 0, 0);;
        
        // Attached the spawned card to the insert
        spawnCard.transform.AttachToParent(currentInsert, sourceCard.transform.GetSiblingIndex(), overrideSorting: true, blocksRaycasts:false );
        spawnCard.transform.localScale = new Vector3(1.6f,1.6f,1.6f);
        spawnCard.transform.localPosition = new Vector3(this.ArenaLayout.GetXPositionForSibling(siblingIndex+1), 0, -200);
        
        // Move any other cards in the arena to new positions
        this.ArenaLayout.UpdateCardTweens();

        spawnCard.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        spawnCard.transform.DORotate(Vector3.zero, 0.2f);
        // Animate the spanwed card into place
        spawnCard.transform.DOLocalMoveX(0f, 0.2f).OnComplete(()=>
        {
            spawnCard.RaycastTag = "arenaCard";
            spawnCard.transform.AttachToParent(currentInsert.parent, currentInsert.GetSiblingIndex(), overrideSorting: false, blocksRaycasts:true);
            spawnCard.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            spawnCard.PlayerArenaController = this;
            currentInsert.AttachToParent(this.insertsRoot, 0);
            currentInsert.gameObject.SetActive(false);
            currentInsert.GetChild(0).gameObject.SetActive(true);
            this.insertTransforms.Enqueue(currentInsert);
            this.ArenaLayout.UpdateCardTweens();
            this.PlayerHandController.ShowCastTargets();
            this.RefreshTargets();
        });
    }

    public void OnPlayCard(CardController cardController)
    {
        this.StartCoroutine(this.WaitToPlayCard(cardController));
    }

    private IEnumerator WaitToPlayCard(CardController cardController)
    {

        //Debug.LogFormat("#PlayerArena# wait to play for {0}", cardController.CardIdAndName);
        Vector3 cardPos = new Vector3(0f, 15, 10);
        RectTransform playedCardInsert = (RectTransform)this.ArenaLayout.InsertForDraggedCard.transform;;
        cardController.DOKill(playedCardInsert);
        cardController.transform.AttachToParent(playedCardInsert, 0, sortingOrder:200);
        this.cardBeingPlayed = cardController;

        // Move the card over the insert
        bool isInPosition = false;
        //cardController.transform.DOScale(Vector3.one*1.1f,0.5f).SetEase(Ease.OutCubic);
        cardController.CardGlowAnimator.SetBool("IsPowerCardFloating", false);
        cardController.transform.DORotate(Vector3.zero, 0.2f);
        cardController.transform.DOLocalMove(cardPos, 0.5f).SetEase(Ease.OutCubic).OnKill(()=>{
            //Debug.LogFormat("#PlayerArena# setting pending for {0}", cardController.CardIdAndName);
             cardController.CardGlowAnimator.SetBool("IsPending", true);
             isInPosition = true;
        });

        while(isInPosition == false ) { yield return null;}

        bool succeeded = false;
        while(this.playedCards.TryGetValue(cardController, out succeeded) == false)
        {
            yield return null;
        }
        cardController.CardGlowAnimator.SetBool("IsPending", false);
        this.playedCards.Remove(cardController);
        Debug.LogFormat("#PlayerArena# PlayCard resolved! resolution is: {0}, {1}", succeeded, cardController.CardIdAndName);

        // Did the CastAction resolve to success?
        if(succeeded)
        {
            // Play a sequence to tween it down to its position in the arena
            Sequence seq = DOTween.Sequence();
            seq.Append(cardController.transform.DOLocalMoveZ(200f,0.15f));
            seq.Insert(0, cardController.transform.DOLocalMoveY(0f,0.15f));
            seq.Insert(0, cardController.transform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.15f));
            seq.AppendInterval(0.1f);
            seq.Append(cardController.transform.DOLocalMove(Vector3.zero, 0.07f).SetEase(Ease.InExpo));
            seq.OnKill(() =>
                {
                    
                    this.PlayCardSound.Play();
                    cardController.RaycastTag = "arenaCard";
                    cardController.transform.AttachToParent(this.transform,playedCardInsert.GetSiblingIndex(), overrideSorting: false, blocksRaycasts:true );
                    cardController.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                    cardController.name = "ArenaCard: CardCanvas" + this.transform.childCount;
                    cardController.PlayerArenaController = this;
                    if(cardController.IsUnit)
                    {
                        ((CardFrontController)cardController).UpdateFlyingState();
                    }
                    else
                    {
                        cardController.CardGlowAnimator.SetBool("IsAirborn",true);
                    }
                    RecycleInsert(playedCardInsert);
                    
                    this.ArenaLayout.UpdateCardTweens();
                    if(this.draggedCard != null && this.draggedCard == cardController)
                    {
                        if(this.draggedCard == this.cardBeingPlayed)
                        {
                            this.cardBeingPlayed = null;
                        }
                        this.TriggerInput(InputEvent.CardDragEnd, this.draggedCard);
                        this.draggedCard = null;
                    }
                    this.PlayerHandController.ShowCastTargets();
                    this.RefreshTargets();
                });
            seq.Play();
        }
        else
        {
            // CastAction failed! 
            // PlayerHandController returns card to hand
            // We need to null out the dragged card & make sure the drag's ended.
            this.cardBeingPlayed = null;
            if (this.draggedCard != null && this.draggedCard == cardController)
            {
                this.TriggerInput(InputEvent.CardDragEnd, this.draggedCard);
                this.draggedCard = null;
            }
        }
        this.PlayerHandController.IsPlayingCard = false;
    }

    public void OnPlayCardSucceeded(ICardState cardState)
    {
        this.playedCards.Add(this.ArenaCards.GetCard(cardState.Id),true);
    }

    public void OnPlayCardFailed(ICardState cardState)
    {
        CardController cardController = this.ArenaCards.GetCard(cardState.Id);
        this.playedCards.Add(cardController,false);
    }
    
    public void OnPhaseChanged(Phase turnPhase, GameStateReadOnly gameState)
    {
        this.turnPhase = turnPhase;

        this.TriggerInput(InputEvent.PhaseEnded, this.InputEventController.SelectedCard);
        
        switch(turnPhase)
        {
            case Phase.Combat:
                // When combat starts, display the correct glows on my arena cards
                IEnumerable<ICardState> arenaCards = gameState.PermanentsForPlayer(gameState.SelfId);
                foreach(ICardState arenaCard in arenaCards)
                {
                    CardController card = this.ArenaCards.GetCard(arenaCard.Id);
                    if(card != null)
                    {
                        card.DisplayLegalAbilities(gameState.LegalAbilities);
                    }
                }
            break;
            case Phase.PreAction:
                // After combat cards may be in odd places, so start the action phase with a layout operation
                this.ArenaLayout.UpdateCardTweens();
            break;
        }
    }

    
    public void TriggerInput(InputEvent eventType, CardController cardController)
    {
        this.inputEvents[eventType] = true;
        this.inputCards[eventType] = cardController;
    }
    
    /// <summary>
    /// Handles moving from one state to another based on an InputEvent and a reference card.
    /// See the state transitions list set up in Start() for details
    /// </summary>
    private ArenaState DoStateTransition(ArenaState currentState, InputEvent inputEvent, CardController activeCard)
    {
        Transition currentTransition = this.transitions.Where(s=>s.FromState==currentState && s.InputEvent==inputEvent).FirstOrDefault();
		if (currentTransition != null)
		{
			Debug.LogFormat ("#PlayerArena#: Transitioning {0}->({1})->{2} ({3})", currentState, currentTransition.InputEvent, currentTransition.ToState, activeCard == null ? "null" : activeCard.CardIdAndName);

			if (currentTransition.TransitionFunction != null)
			{
				currentTransition.TransitionFunction.Invoke (activeCard);
			}
			currentState = currentTransition.ToState;
		}
		//else
		//	Debug.LogFormat ("#PlayerArena#: No Transition from {0}->({1})", currentState, inputEvent);
        return currentState;
    }
}
