﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using DragonFoundry.GameState;
using strange.extensions.mediation.impl;
using NovaBlitz.Game;
using NovaBlitz.UI;
using System;
using Messages;
using System.Collections.Generic;

public class ArenaZoomController : View 
{
    public GameStateReadOnly GameState;
    public InputEventController InputEventController;
    public CardAbilityController CardAbilityController;
    [Inject] public ArenaCards ArenaCards {get;set;}
    [Inject] public GameData GameData { get; set; }
    public RectTransform ArenaZoomRoot;
    public CanvasGroup KeywordCanvasGroup;
    public RectTransform BoundsRect;
    public CardController ZoomedCard {get {return this.sourceCard;}}
    private CardController sourceCard;
    private GameObject zoomedCardInstance;
    private Sequence keywordSequence;
    private Vector3? defaultKeywordPosition;

    
    // TODO: Make this a utility function somewhere and call it from here and PlayerHandLayoutController
    void ConstrainCardToBounds(CardController draggedCard, RectTransform cardRect)
    {
        // Get the constraining Bounds rect
        Vector3[] corners = new Vector3[4];
        this.BoundsRect.GetWorldCorners(corners);
        Rect worldBounds = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[1].y - corners[0].y);
        Vector3 worldMin = new Vector3(worldBounds.xMin, worldBounds.yMin, this.BoundsRect.position.z);
        Vector3 worldMax = new Vector3(worldBounds.xMax, worldBounds.yMax, this.BoundsRect.position.z);
        Vector3 viewportMin = Camera.main.WorldToViewportPoint(worldMin);
        Vector3 viewportMax = Camera.main.WorldToViewportPoint(worldMax);
        Rect viewPortBounds = new Rect(viewportMin.x, viewportMin.y, viewportMax.x - viewportMin.x, viewportMax.y - viewportMin.y);
        
        // Get the draggedCard's bounds
        Vector3[] corners2 = new Vector3[4];
        cardRect.GetWorldCorners(corners2);
        Rect cardBounds = new Rect(corners2[0].x, corners2[0].y, corners2[2].x - corners2[0].x, corners2[1].y - corners2[0].y);
        Vector3 cardWorldMin = new Vector3(cardBounds.xMin, cardBounds.yMin, draggedCard.transform.position.z);
        Vector3 cardWorldMax = new Vector3(cardBounds.xMax, cardBounds.yMax, draggedCard.transform.position.z);
        Vector3 cardViewportMin = Camera.main.WorldToViewportPoint(cardWorldMin);
        Vector3 cardViewportMax = Camera.main.WorldToViewportPoint(cardWorldMax);
        Rect cardViewPortBounds = new Rect(cardViewportMin.x, cardViewportMin.y, cardViewportMax.x - cardViewportMin.x, cardViewportMax.y - cardViewportMin.y);
       
      
        Vector3 draggedCardViewport = Camera.main.WorldToViewportPoint(draggedCard.transform.position);
        
        if (cardViewPortBounds.xMin < viewPortBounds.xMin) { draggedCardViewport.x += viewPortBounds.xMin - cardViewPortBounds.xMin; }
        else if(cardViewPortBounds.xMax > viewPortBounds.xMax) { draggedCardViewport.x += viewPortBounds.xMax - cardViewPortBounds.xMax; }
        if (cardViewPortBounds.yMin < viewPortBounds.yMin) { draggedCardViewport.y += viewPortBounds.yMin - cardViewPortBounds.yMin; }
        else if(cardViewPortBounds.yMax > viewPortBounds.yMax) { draggedCardViewport.y += viewPortBounds.yMax - cardViewPortBounds.yMax; }
        
        Vector3 worldOffset = Camera.main.ViewportToWorldPoint(draggedCardViewport);
        draggedCard.transform.position = worldOffset; 
    }

    
    public void OnZoomInCard(CardController activeCard)
    {
        // Early out if zooming the same card
        var cardFront = activeCard as CardFrontController;
        if(activeCard == this.sourceCard || cardFront == null || cardFront.CardState == null) return;
        
        this.sourceCard = activeCard;
        
        // Spawn a copy of the hovered card
        GameObject zoomGO = ArenaCards.CreateCard(cardFront.CardState).gameObject;
        CardController cardController = zoomGO.GetComponent<CardController>();

        cardController.CardGlowAnimator.SetBool("IsPlayable", false);
        cardController.CardGlowAnimator.SetBool("IsHovered", false);
        cardController.CardGlowAnimator.SetBool("IsAffected", false);
        cardController.CardGlowAnimator.SetBool("IsActivatable", false);
        cardController.CardGlowAnimator.SetBool("IsCombatActive", false);
        cardController.CardGlowAnimator.SetBool("IsSelected", false);
        
        var cardState = cardFront.CardState;
        var cardFrontController = cardController as CardFrontController;
        bool isSlidingLeft = false;
        if (cardFrontController != null)
        {
            bool isUpdated = cardState.IsUpdated;
            cardState.IsUpdated = true;
            cardFrontController.Initialize(cardState, cardState.ControllerId == GameData.CurrentGameData.PlayerId
                ? GameData.CurrentGameData.PlayerCardBack
                : GameData.CurrentGameData.OpponentCardBack, cardFront.IsPurified);
            cardFrontController.UpdateLabels();
            cardState.IsUpdated = isUpdated;
            cardFrontController.OutlineCanvasGroup.alpha = 0;

            isSlidingLeft = activeCard.transform.GetSiblingIndex() >= 4;
            // Debug.LogFormat("IsSlidingLeft: {0} siblingIndex: {1}", isSlidingLeft, activeCard.transform.GetSiblingIndex() );
        }
        this.TriggerKeywordsPopup(cardState, false, isSlidingLeft);
        

        zoomGO.transform.AttachToParent(ArenaZoomRoot, 0, sortingOrder:1000, overrideSorting :true, blocksRaycasts:false);
        zoomGO.transform.localScale = new Vector3(1.2f,1.2f,1.2f);
        zoomGO.transform.localPosition = Vector3.zero;
        
        // Get the screen position of the card in the arena
        Vector3 viewPos = RectTransformUtility.WorldToScreenPoint(Camera.main, activeCard.transform.position);
        Vector3? destViewPos = null; 


#if UNITY_STANDALONE // PC, Mac, Linux
        viewPos.x = 125f;
#endif

#if UNITY_IOS || UNITY_ANDROID
        if(viewPos.x > Screen.width * 0.4f)
        {
            // Overwrite the xPos with the left side of the screen
            viewPos.x = Screen.width * 0.18f;
        }
        else
        {
            // Overwrite the xPos with the right side of the screen
            viewPos.x = Screen.width * 0.8f;

            // If the card has keywords leave room for them.
            if(sourceCard.HasKeywords)
            {
                Vector3 viewPosCopy = viewPos;
                viewPosCopy.x = Screen.width * 0.6f;
                destViewPos = viewPosCopy;
            }
        }
        viewPos.y = Screen.height * 0.5f;
#endif

        Vector2 localPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(this.ArenaZoomRoot, viewPos, Camera.main, out localPos);
        localPos.y += 30;
        localPos.x += 50;
        ((RectTransform)zoomGO.transform).anchoredPosition = localPos;

        if(destViewPos != null)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(this.ArenaZoomRoot, destViewPos.Value, Camera.main, out localPos);
            localPos.x += 50;
            ((RectTransform)zoomGO.transform).DOAnchorPosX(localPos.x,0.2f).SetEase(Ease.OutCubic).SetDelay(0.1f);
        }
        
        //cardController.Canvas.sortingLayerName= "Overlay";
        
        Vector3 zoom = new Vector3(2.25f, 2.25f, 2.25f);

#if UNITY_IOS || UNITY_ANDROID
        zoom.x = 2.5f;
        zoom.y = 2.5f;
        zoom.z = 1f;
#endif

        zoomGO.transform.DOScale(zoom, 0.1f)
            .OnUpdate(()=>{
                this.ConstrainCardToBounds(cardController, (RectTransform)cardController.Card.transform);
            });
        cardController.CanvasGroup.alpha = 0;
        cardController.CanvasGroup.DOFade(1f, 0.15f).SetEase(Ease.InSine);
            
        this.zoomedCardInstance = zoomGO;        
    }

    public void ShowCardKeywords(CardFrontController cardFront)
    {
        if(this.defaultKeywordPosition == null)
        {
            this.defaultKeywordPosition=this.KeywordCanvasGroup.transform.localPosition;
        }

        RectTransform cardRect = (RectTransform)cardFront.cardBackground.transform;


         // Get the cardFront's's bounds
        Vector3[] corners2 = new Vector3[4];
        cardRect.GetWorldCorners(corners2);
        Rect cardBounds = new Rect(corners2[0].x, corners2[0].y, corners2[2].x - corners2[0].x, corners2[1].y - corners2[0].y);
        Vector3 cardWorldMin = new Vector3(cardBounds.xMin, cardBounds.yMin, cardFront.transform.position.z);
        Vector3 cardWorldMax = new Vector3(cardBounds.xMax, cardBounds.yMax, cardFront.transform.position.z);
        Vector3 cardViewportMin = Camera.main.WorldToViewportPoint(cardWorldMin);
        Vector3 cardViewportMax = Camera.main.WorldToViewportPoint(cardWorldMax);
        Rect cardViewPortBounds = new Rect(cardViewportMin.x, cardViewportMin.y, cardViewportMax.x - cardViewportMin.x, cardViewportMax.y - cardViewportMin.y);

 
        this.CardAbilityController.Initialize(cardFront.CardState, this.KeywordCanvasGroup.transform.position);
        this.KeywordCanvasGroup.transform.OverlayPosition(cardFront.transform);
        this.KeywordCanvasGroup.DOFade(1f, 0.25f).SetDelay(0.1f);

        Vector3 locPos = this.KeywordCanvasGroup.transform.localPosition;
        locPos.z = 60;

        if(cardViewPortBounds.xMax > 0.8f)
        {
            // Show keywords on the left
            locPos.x -= 465;
            locPos.y -= 300;
        }
        else
        {
            // show kewywords on the right
            locPos.x += 550;
            locPos.y -= 300;
        }
        this.KeywordCanvasGroup.transform.localPosition = locPos;
    }
    
    public void TriggerKeywordsPopup(ICardState cardState, bool isKewordsFloatedUp = false, bool isKewordsSlidingLeft = false)
    {
        if(this.defaultKeywordPosition == null)
        {
            this.defaultKeywordPosition=this.KeywordCanvasGroup.transform.localPosition;
        }

        if(this.keywordSequence != null) 
        {
           this.keywordSequence.Kill();
        }

        this.KeywordCanvasGroup.transform.localPosition = this.defaultKeywordPosition.Value;
        this.CardAbilityController.Initialize(cardState, this.KeywordCanvasGroup.transform.position);
        
        this.keywordSequence = DOTween.Sequence();
        this.keywordSequence.AppendInterval(0.0f);
        this.keywordSequence.Append(this.KeywordCanvasGroup.DOFade(1f, 0.2f));  
        if(isKewordsFloatedUp)
        {
            this.keywordSequence.Insert(0,this.KeywordCanvasGroup.transform.DOLocalMoveY(-150,0.2f));      
        }
        else if(isKewordsSlidingLeft)
        {
            this.keywordSequence.Insert(0, this.KeywordCanvasGroup.transform.DOLocalMoveX(200,0.2f));
        }

        this.keywordSequence.Play();
    }
    
    public void RecycleZoomedCard()
    {
        if(this.zoomedCardInstance != null)
        {
            this.HideKeywords();
            CardFrontController cardFront = this.zoomedCardInstance.GetComponent<CardFrontController>();
            this.zoomedCardInstance.transform.DOKill();
            cardFront.CanvasGroup.DOKill();
            this.InputEventController.ResetZoomInTimer(cardFront.CardState.CardId);
            cardFront.Recycle();
            this.zoomedCardInstance = null;
            this.sourceCard = null;
        }
    }
    public void HideKeywords()
    {
        if(this.keywordSequence != null) 
        {
           this.keywordSequence.Kill();
        }
        this.KeywordCanvasGroup.DOKill();
        this.KeywordCanvasGroup.alpha = 0;
    }

    internal void CopyCardGlows(CardController glowCard)
    {
        if(glowCard != null && this.ZoomedCard != null
        && glowCard.CardId == this.ZoomedCard.CardId)
        {
            CardController overlayCard = this.zoomedCardInstance.GetComponent<CardFrontController>();
            if(overlayCard != null )
            {
                overlayCard.CardGlowAnimator.SetBool("IsPlayable", glowCard.CardGlowAnimator.GetBool("IsPlayable"));
                overlayCard.CardGlowAnimator.SetBool("IsHovered", glowCard.CardGlowAnimator.GetBool("IsHovered"));
                overlayCard.CardGlowAnimator.SetBool("IsCombatActive", glowCard.CardGlowAnimator.GetBool("IsCombatActive"));
                overlayCard.CardGlowAnimator.SetBool("IsSelected", glowCard.CardGlowAnimator.GetBool("IsSelected"));
                overlayCard.CardGlowAnimator.SetBool("IsBlockTarget", glowCard.CardGlowAnimator.GetBool("IsBlockTarget"));
                overlayCard.CardGlowAnimator.SetBool("IsAttackTarget", glowCard.CardGlowAnimator.GetBool("IsAttackTarget"));
            }
        }
    }
}
