﻿using UnityEngine;
using System.Collections;

public class CanvasMesh : MonoBehaviour 
{

	public CanvasRenderer CanvasRenderer;
    public Material Material;
    public Material AdditiveMaterial;
    private Mesh mesh;
    public Vector3[] Verts;
    public Vector2[] UVs;
    public int[] Triangles;
    public Color32[] Colors;

    public Sprite Sprite;

    public Color Color 
    {
        set 
        {
            if(this.CanvasRenderer.GetMaterial() != null)
            {
                this.CanvasRenderer.GetMaterial().color = value;
            }
        }
    }

    public void Clear()
    {
        if(this.mesh != null)
        {
            this.mesh.Clear();
            this.CanvasRenderer.SetMesh(this.mesh);
        }
    }

    public bool IsAdditiveBlending
    {
        get { return this.CanvasRenderer.GetMaterial(0) == this.AdditiveMaterial;}
        set 
        {
            if(value && this.CanvasRenderer.GetMaterial() != this.AdditiveMaterial)
            {
                this.CanvasRenderer.SetMaterial(this.AdditiveMaterial, null);
            }
            else if(!value && this.CanvasRenderer.GetMaterial() != this.Material)
            {
                this.CanvasRenderer.SetMaterial(this.Material, null);
            }
        }
    }
 
    // Use this for initialization
    void Start()
    {
        this.mesh = new Mesh();
        this.Material = new Material(this.Material);
        this.AdditiveMaterial = new Material(this.AdditiveMaterial);
        this.CanvasRenderer.SetMaterial(this.Material,null);
    }
 
    // Update is called once per frame
    void Update()
    {
        this.mesh.Clear();
        this.mesh.vertices = this.Verts;
        this.mesh.colors32 = this.Colors;
        this.mesh.uv = this.UVs;
        this.mesh.triangles = this.Triangles;
        this.CanvasRenderer.SetMesh(this.mesh);
    }

	void OnDestroy()
	{
		UnityEngine.Object.Destroy (this.Material);
		UnityEngine.Object.Destroy (this.AdditiveMaterial);
		UnityEngine.Object.Destroy (this.mesh);
		this.Material = null;
		this.AdditiveMaterial = null;
		this.mesh = null;
	}
}
