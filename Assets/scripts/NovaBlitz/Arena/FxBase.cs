
using UnityEngine;
using System.Collections;
using System;
using NovaBlitz.UI;
using System.Collections.Generic;
using DragonFoundry.Fx;

public class FxBase : PoolableMonoBehaviour
{
    //FX
    public Dictionary<string, ParticleEffectsTrigger> fxScripts = new Dictionary<string, ParticleEffectsTrigger>(StringComparer.OrdinalIgnoreCase);
    public ParticleEffectsTrigger currentParticleEffects = null;
    protected bool wasStarted = false;
    public RectTransform fxTransform;
    protected Camera uiCamera;
    protected RectTransform _ParticleBounds;
    protected Canvas _TargetCanvas;
    protected int _SortOrder = int.MinValue;
    protected string _SortLayer = string.Empty;
    protected bool _OverrideSorting = false;


    protected Vector3 GetFlatPlaneXY(Vector3 WorldSpacePoint)
    {
        Vector3 worldPos = Vector3.zero;
        if (this.uiCamera == null)
            this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();
        Vector2 screenPoint2 = RectTransformUtility.WorldToScreenPoint(this.uiCamera, WorldSpacePoint);
        RectTransformUtility.ScreenPointToWorldPointInRectangle(_ParticleBounds, screenPoint2, this.uiCamera, out worldPos);
        if (!RectTransformUtility.RectangleContainsScreenPoint(_ParticleBounds, screenPoint2, this.uiCamera))
        {
            // push the point to the edge of the particle bounds
            Vector3[] corners = new Vector3[4];
            _ParticleBounds.GetWorldCorners(corners);
            float x = worldPos.x > corners[2].x ? corners[2].x : worldPos.x < corners[0].x ? corners[0].x : worldPos.x;
            float y = worldPos.y > corners[2].y ? corners[2].y : worldPos.y < corners[0].y ? corners[0].y : worldPos.y;
            return new Vector3(x, y, worldPos.z);
        }
        return worldPos;
    }

    protected virtual void Start()
    {
        if(this.fxScripts.Count == 0)
        {
            this.InitializeFxScripts();
        }


        if (this.uiCamera == null)
            this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();

        this.currentParticleEffects = null;
    }

    public void InitializeFxScripts()
    {
        this.fxScripts.Clear();
        ParticleEffectsTrigger[] particleFx = this.GetComponentsInChildren<ParticleEffectsTrigger>(true);

        for (int i = 0; i < particleFx.Length; i++)
        {
            var vfx = particleFx[i];
            this.fxScripts.Add(vfx.gameObject.name.ToLower(), vfx);
            vfx.gameObject.SetActive(false);
        }
    }

    public void Trigger(string fxName)
    {
        if (string.IsNullOrEmpty(fxName))
        {
            this.RecycleFx();
            return;
        }
        
        if(this.fxScripts.Count == 0)
        {
            this.InitializeFxScripts();
            //Debug.LogError("Added " + this.fxScripts.Count + " fxScripts to FxTrigger");
        }

        ParticleEffectsTrigger particleFx = null;
        if (this.fxScripts.TryGetValue(fxName, out particleFx))
        {
            this.wasStarted = false;
            Timer.GameInstance.StartCoroutine(this.WaitToStartFx(particleFx));
        }
        else
        {
            Debug.LogErrorFormat("Couldn't find fx {0} to trigger. NumParticleFx: {1}", fxName, this.fxScripts.Count);
            //this.RecycleFx();
        }
    }

    protected virtual IEnumerator WaitToStartFx(ParticleEffectsTrigger particleFx)
    {
        throw new NotImplementedException();
    }

    private void OnParticlesFinished()
    {
        this.RecycleFx();
    }

    public virtual void RecycleFx()
    {
        this.Recycle();
    }
}