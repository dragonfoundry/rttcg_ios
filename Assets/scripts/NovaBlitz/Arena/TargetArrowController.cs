﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using UnityEngine.UI;
using System;
using DragonFoundry.Fx;

public class TargetArrowController : MonoBehaviour 
{
    public AnimationCurve ArrowCurve;
    public AnimationCurve OffsetCurve;
    public RectTransform ArenaTransform;
    public InputEventController InputEventController;
    public Image TargetReticle;
    public Sprite AttackArrow;
    public Sprite BlockArrow;
    public CanvasMesh CanvasMeshPrefab;
    public RectTransform BoundsRect;
    private Queue<CanvasMesh> canvasMeshes = new Queue<CanvasMesh>();
    private Dictionary<TargetArrowInfo, CanvasMesh> targetArrowMeshes = new Dictionary<TargetArrowInfo, CanvasMesh>();
    public TargetArrowInfo DraggedTargetArrowInfo; // There can be only one currently dragged target arrow controller
    private List<TargetArrowTarget> targets = new List<TargetArrowTarget>();
    private Camera uiCamera;
    public ParticleEffectsTrigger ArrowHeadFx;
    public ParticleSystem ArrowHeadFxParticles;
    
    void Start()
    {
        for(int i=0;i<5;i++)
        {
            CanvasMesh canvasMesh = this.CreateCanvasMesh();
            canvasMesh.gameObject.SetActive(false);
            this.canvasMeshes.Enqueue(canvasMesh);   
        }
        
        this.TargetReticle.gameObject.SetActive(true);
        //this.TargetReticle.CrossFadeAlpha(0,0f,true);
        
        this.uiCamera = GameObject.Find("CameraForUI").GetComponent<Camera>();

        this.gameObject.layer = 8; // put these on the main camera layer so they aren't drawn by the overlay camera
    }
    
    public void ClearTargets()
    {
        for(int i=0;i<this.targets.Count;i++)
        {
            TargetArrowTarget target = this.targets[i];
            ClearTarget(targets[i]);
          
        }
        this.targets.Clear();
    }

    public void ClearTarget(TargetArrowTarget target)
    {
        if (target.TargetGlowAnimator != null)
        {
            target.TargetGlowAnimator.SetBool("IsHovered", false);
            target.TargetGlowAnimator.SetBool("IsAttackTarget", false);
            target.TargetGlowAnimator.SetBool("IsBlockTarget", false);
            target.TargetGlowAnimator.SetBool("IsActivateTarget", false);
            target.TargetGlowAnimator.SetBool("IsCastTarget", false);
            target.TargetGlowAnimator.SetBool("IsAffected", false);
            target.TargetGlowAnimator.SetBool("IsSpinning", false);
        }
        target.TargetAction = TargetAction.None;
    }

    public void AddFakeAttackTarget(TargetArrowTarget target)
    {
        if (this.targets.Contains(target) == false)
        {
            this.targets.Add(target);
        }
    }

    public void AddTarget(TargetArrowTarget target)
    {
        if(this.targets.Contains(target) == false)
        {
            this.targets.Add(target);
            target.TargetGlowAnimator.SetBool("IsSpinning", target.TargetAction != TargetAction.Affected && target.TargetAction != TargetAction.None);
            //Debug.LogFormat("Target added. Spinning = {0} TargetAction = {1}", target.TargetAction != TargetAction.Affected && target.TargetAction != TargetAction.None, target.TargetAction);
        }
    }
    
    public void RemoveTarget(TargetArrowTarget target)
    {
        if(this.targets.Contains(target))
        {
            this.targets.Remove(target);
        }
    }

    private void DragArrow(Vector3 arrowStartWorldPos)
    {
        Vector3 arrowEnd;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(this.ArenaTransform, Input.mousePosition, this.uiCamera, out arrowEnd);   
        this.DraggedTargetArrowInfo = this.RequestTargetArrowInfo(arrowStartWorldPos,arrowEnd);
        this.DraggedTargetArrowInfo.ArrowType = ArrowType.Curved;
    }

    public TargetArrowInfo DragArrow(Transform arrowStartObject, Transform parent = null)
    {
        Vector3 arrowEnd;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(this.ArenaTransform, Input.mousePosition, this.uiCamera, out arrowEnd);   
        this.DraggedTargetArrowInfo = this.RequestTargetArrowInfo(arrowStartObject,null);
        this.DraggedTargetArrowInfo.Destination = arrowEnd;
        this.DraggedTargetArrowInfo.ArrowType = ArrowType.Curved;
        this.DraggedTargetArrowInfo.ArrowParent = parent;
        this.DrawTargetArrow(this.DraggedTargetArrowInfo);
        return this.DraggedTargetArrowInfo;
    }

    public void FadeOutAndReleaseTargetArrow(TargetArrowInfo targetArrowInfo, float duration =0.5f, float delay=0)
    {
        this.StartCoroutine(this.FadeOutArrow(targetArrowInfo,duration,delay));
    }

    protected IEnumerator FadeOutArrow(TargetArrowInfo targetArrowInfo, float duration, float delay)
    {
        Color startColor = targetArrowInfo.StartingColor;
        float startingDuration = duration;
        
        // Delay
        yield return new WaitForSecondsRealtime(delay);
        ArrowHeadFx.EndEffect();
        
        // Duration
        while(duration > 0)
        {
            duration -= Time.deltaTime;
            Color start = targetArrowInfo.StartingColor;
            start.a = startColor.a * duration/startingDuration;
            targetArrowInfo.StartingColor = start;
            yield return null;
        }
        
        this.ReleaseTargetArrowInfo(targetArrowInfo);
    }

    public IEnumerator FadeOutArrowHead(float delay, float duration)
    {
        Color startColor = this.ArrowHeadFxParticles.startColor;
        Color fadingColor = startColor;
        float startingDuration = duration;
        yield return new WaitForSecondsRealtime(delay);
        while (duration > 0)
        {
            duration -= Time.deltaTime;
            fadingColor.a = startColor.a * duration / startingDuration;
            this.ArrowHeadFxParticles.startColor = fadingColor;
            yield return null;
        }
        ArrowHeadFx.EndEffect();
    }

    public TargetArrowTarget ReleaseDraggedArrow()
    {
        return this.ReleaseDraggedArrow(this.DraggedTargetArrowInfo);
    }

    public TargetArrowTarget ReleaseDraggedArrow(TargetArrowInfo arrowInfo)
    {
        TargetArrowTarget currentTarget = null;
        if(arrowInfo == this.DraggedTargetArrowInfo)
        {
            this.DraggedTargetArrowInfo = null;
        }
        if(arrowInfo != null)
        {
            this.ReleaseTargetArrowInfo(arrowInfo);
            currentTarget = arrowInfo.CurrentTarget;
        }
        return currentTarget;
    }

    public TargetArrowTarget GetDraggedArrowTarget()
    {
        TargetArrowTarget currentTarget = null;
        if(this.DraggedTargetArrowInfo != null)
        {
            currentTarget = this.DraggedTargetArrowInfo.CurrentTarget;
        }
        return currentTarget;
    }
    
    
    public TargetArrowInfo RequestTargetArrowInfo(Transform followSource, Transform followDestination, Transform parent = null, int? childIndex = null)
    {
        TargetArrowInfo arrowInfo = new TargetArrowInfo();
        arrowInfo.FollowSource = followSource;
        arrowInfo.FollowDestination = followDestination;
        
        if(followDestination != null) { arrowInfo.Destination = followDestination.position; }

        if(followSource != null) { arrowInfo.Source = followSource.position; }

        arrowInfo.ArrowType = ArrowType.Flat;
        arrowInfo.StartingColor = new Color(255/255f,23/255f, 23/255f,255/255f);
        arrowInfo.ArrowParent = parent;
        arrowInfo.ChildIndex = childIndex;
        this.targetArrowMeshes[arrowInfo] = this.GetCanvasMesh();
        this.DrawTargetArrow(arrowInfo);
        return arrowInfo;
    }

    public TargetArrowInfo RequestTargetArrowInfo(Transform followSource, Vector3 destinationWorldPos, Transform parent = null)
    {
        TargetArrowInfo arrowInfo = new TargetArrowInfo();
        arrowInfo.FollowSource = followSource;
        arrowInfo.Destination = destinationWorldPos;
        arrowInfo.ArrowType = ArrowType.Flat;
        arrowInfo.StartingColor = new Color(255/255f,23/255f, 23/255f,255/255f);
        arrowInfo.ArrowParent = parent;
        this.targetArrowMeshes[arrowInfo] = this.GetCanvasMesh();
        this.DrawTargetArrow(arrowInfo);
        return arrowInfo;
    }
   
    private TargetArrowInfo RequestTargetArrowInfo(Vector3 sourceWorldPos, Vector3 destinationWorldPos, Transform parent = null)
    {
        TargetArrowInfo arrowInfo = new TargetArrowInfo();
        arrowInfo.Source = sourceWorldPos;
        arrowInfo.Destination = destinationWorldPos;
        arrowInfo.ArrowType = ArrowType.Flat;
        arrowInfo.StartingColor = new Color(255/255f,23/255f, 23/255f,255/255f);
        arrowInfo.ArrowParent = parent;
        this.targetArrowMeshes[arrowInfo] = this.GetCanvasMesh();
        this.DrawTargetArrow(arrowInfo);
        return arrowInfo;
    }
    
    public void ReleaseTargetArrowInfo(TargetArrowInfo targetArrowInfo)
    {
        CanvasMesh canvasMesh = null;
        if(this.targetArrowMeshes.TryGetValue(targetArrowInfo, out canvasMesh))
        {
            canvasMesh.Clear();

            //Debug.Log("Ending Arrowhead FX");
            this.ArrowHeadFx.EndEffect();
            this.targetArrowMeshes.Remove(targetArrowInfo);
            canvasMesh.transform.AttachToParent(this.transform.parent, 0);
            canvasMesh.gameObject.SetActive(false);
            this.canvasMeshes.Enqueue(canvasMesh);
        }
    }
    
    void LateUpdate()
    {
        // =============================
        // Arrow Dragging
        // =============================
        if(this.DraggedTargetArrowInfo != null && this.DraggedTargetArrowInfo.FollowDestination == null)
        {
            // Make the arrow follow the cursor / touch
            if(RectTransformUtility.RectangleContainsScreenPoint(this.BoundsRect, Input.mousePosition, this.uiCamera))
            {
                Vector3 worldPos;
                RectTransformUtility.ScreenPointToWorldPointInRectangle(this.ArenaTransform,Input.mousePosition, this.uiCamera, out worldPos);
                this.DraggedTargetArrowInfo.Destination = worldPos; 
            }
        }

        // TODO: pull this out into a function that any controller can use for their own arrows
        if(this.InputEventController != null && this.InputEventController.IsArrowDragging)
        {
            float closestDistance = float.MaxValue;
            TargetArrowTarget target = null;
            for(int i=0;i<targets.Count;i++)
            {
                TargetArrowTarget currentTarget = targets[i];
                Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(this.uiCamera, currentTarget.transform.position);
                float distance = Vector2.Distance(screenPoint, Input.mousePosition);
                if(distance < closestDistance && currentTarget.TargetAction != TargetAction.None)
                {
                    target = currentTarget;
                    closestDistance = distance;
                }
            }
            
            // Position the reticle at the arrow tip
            Vector3 worldPos = Vector3.zero;
            Vector2 screenPoint2 = RectTransformUtility.WorldToScreenPoint(this.uiCamera, this.DraggedTargetArrowInfo.Destination);
            RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)this.TargetReticle.transform.parent, screenPoint2, this.uiCamera, out worldPos);
            this.TargetReticle.transform.position = worldPos;
            
            if(target != null 
                && target.TargetAction != TargetAction.None
                && RectTransformUtility.RectangleContainsScreenPoint(target.BoundsRect, screenPoint2, this.uiCamera))
            {
                // Show Target Reticle
                //this.TargetReticle.CrossFadeAlpha(1f,0f,true);
                var main = ArrowHeadFxParticles.main;
                main.startSize = 160f;

                switch (target.TargetAction)
                {
                    case TargetAction.Attack:
                        // NOTE: This is also used for Power targeting, for some reason.
                        //this.TargetReticle.CrossFadeAlpha(1,0,true);
                        //this.TargetReticle.sprite = this.AttackArrow;
                        this.DraggedTargetArrowInfo.StartingColor = new Color32(255,23,23,255);
                        break;
                    case TargetAction.Block:
                        //this.TargetReticle.CrossFadeAlpha(1,0,true);
                        //this.TargetReticle.sprite = this.BlockArrow;
                        this.DraggedTargetArrowInfo.StartingColor = new Color32(37,243,255,255); // was (37,243,255,255)
                        break;
                    case TargetAction.Activate:
                        this.DraggedTargetArrowInfo.StartingColor = new Color32(228,147,254,255);
                        break;
                }
               
                this.DraggedTargetArrowInfo.CurrentTarget = target;
            }
            else
            {
                var main = ArrowHeadFxParticles.main;
                main.startSize = 100f;

                this.TargetReticle.CrossFadeAlpha(0f,0f, true);
                this.DraggedTargetArrowInfo.StartingColor = new Color32(120,180,190,255); // was:(200,251,255,255), before:(120,251,255,255)
                this.DraggedTargetArrowInfo.CurrentTarget = null;
            }
        }

        // Draw the arrows
        List<TargetArrowInfo> arrows = this.targetArrowMeshes.Keys.ToList<TargetArrowInfo>();
        for(int i=0;i<arrows.Count;i++)
        {
            DrawTargetArrow(arrows[i]);  
        }
    }

    private void DrawTargetArrow(TargetArrowInfo targetArrowInfo)
    {
        CanvasMesh canvasMesh = this.targetArrowMeshes[targetArrowInfo];

        if(targetArrowInfo.ArrowParent != null && canvasMesh.transform.parent != targetArrowInfo.ArrowParent)
        {
            int childIndex = targetArrowInfo.ArrowParent.childCount;
            if(targetArrowInfo.ChildIndex.HasValue)
            {
                childIndex = targetArrowInfo.ChildIndex.Value;
            }
            canvasMesh.transform.AttachToParent(targetArrowInfo.ArrowParent,childIndex);
        }
        
        Vector3 sourceWorldPos = targetArrowInfo.Source;
        Vector3 destWorldPos = targetArrowInfo.Destination;

        canvasMesh.Color = targetArrowInfo.StartingColor;

        // Set the head particles
        if (this.DraggedTargetArrowInfo != null && this.DraggedTargetArrowInfo.FollowDestination == null)
        {
            var main = ArrowHeadFxParticles.main;
            main.startColor = new Color(targetArrowInfo.StartingColor.r * 0.7f, targetArrowInfo.StartingColor.g * 0.7f, targetArrowInfo.StartingColor.b * 0.7f, targetArrowInfo.StartingColor.a);
            if (!ArrowHeadFx.gameObject.activeSelf)
            {
                //Debug.Log("Starting Arrowhead FX");
                ArrowHeadFx.gameObject.SetActive(true);
                ArrowHeadFx.StartEffect();
            }
        }

        if (targetArrowInfo.FollowSource != null)
        {
            sourceWorldPos = targetArrowInfo.FollowSource.position;
            Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(this.uiCamera, sourceWorldPos);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(this.ArenaTransform, screenPos, this.uiCamera, out sourceWorldPos);
        }
        
        if(targetArrowInfo.FollowDestination != null)
        {
            destWorldPos = targetArrowInfo.FollowDestination.position;
            Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(this.uiCamera, destWorldPos);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(this.ArenaTransform, screenPos, this.uiCamera, out destWorldPos);
        }
        
        switch(targetArrowInfo.ArrowType)
        {
            case ArrowType.Curved:
                Vector3 direction = destWorldPos - sourceWorldPos;
                Vector3 diag = new Vector3(1f,1f,0);
                diag.Normalize();
                float distance = direction.magnitude;
                float segmentLength = 1f;
                int numPoints = Mathf.RoundToInt(distance / segmentLength);
                numPoints = Mathf.Max(numPoints, 6);
                //Debug.Log("Distance: " + distance + " segments:" + numPoints);
                int numTipPoints = 3;
                direction.Normalize();
                Vector3 yDirection = direction;
                yDirection.x = 0;
                yDirection.z = 0;
                yDirection.Normalize();
                Vector3[] points = new Vector3[numPoints-numTipPoints + 1];
                Vector3[] tipPoints = new Vector3[numTipPoints];
                Vector3[] canvasPoints = new Vector3[numPoints];
                for(int i=0;i<numPoints;i++)
                {
                    float normalizedTime = i / (float)(numPoints-1);
                    float zOffset = this.ArrowCurve.Evaluate(normalizedTime);
                    Vector3[] pointsToUse = i < numPoints - numTipPoints ? points : tipPoints;
                    int indexToUse =  i < numPoints - numTipPoints ? i : i - (numPoints - numTipPoints);
                    
                    pointsToUse[indexToUse] = sourceWorldPos + (direction * (distance / (float)numPoints)) * (normalizedTime * (float)numPoints);

                    Vector3 cameraDirection = this.uiCamera.transform.position - pointsToUse[indexToUse];
                    cameraDirection.Normalize();
                    //pointsToUse[indexToUse] += (cameraDirection * zOffset * (distance / 7f));
                    pointsToUse[indexToUse].z -= zOffset * (distance / 7f);

                    pointsToUse[indexToUse].y -= (this.OffsetCurve.Evaluate(normalizedTime)) * Mathf.Clamp((distance - 20) / 20f,0f,1f);
                    canvasPoints[i]= pointsToUse[indexToUse];
                    
                    // Make the arrow and the tip connect
                    if(pointsToUse == tipPoints && indexToUse == 0)
                    {
                        points[numPoints-numTipPoints] = tipPoints[0];
                    }
                }

                canvasMesh.Verts = new Vector3[numPoints*4];
                canvasMesh.Colors = new Color32[numPoints*4];
                canvasMesh.Triangles = new int [numPoints*6];
                canvasMesh.UVs = new Vector2[canvasMesh.Verts.Length];
                canvasMesh.IsAdditiveBlending = false;

                Vector3 ortho = Vector3.Cross(direction, Vector3.forward);
                ortho.Normalize();

                for(int i=0;i<numPoints-5;i++)
                {
                    float normalizedTime = i / (float)(numPoints-1);
                    float endNormalizedTime = (i+1) / (float)(numPoints-1);
                    float curveOrthoStart = this.ArrowCurve.Evaluate(normalizedTime);
                    float curveOrthoEnd = this.ArrowCurve.Evaluate(endNormalizedTime);
                    float orthoScale = 0f;
                    float remainingPoints = numPoints - i;
                    float startOrthoSize = 35f + (curveOrthoStart * orthoScale) + ((remainingPoints / numPoints) * 20f);
                    float endOrthoSize = 35f + (curveOrthoEnd * orthoScale) + (((remainingPoints -1f) / numPoints) * 20f);
                    Vector3 startPoint = canvasMesh.transform.InverseTransformPoint(canvasPoints[i]);
                    Vector3 endPoint = canvasMesh.transform.InverseTransformPoint(canvasPoints[i+1]);

                    // Make the Arrow tip
                    if(i == numPoints - 6)
                    {
                        startOrthoSize = endOrthoSize + 45 + ((remainingPoints / numPoints) * 20f);
                        endPoint = canvasMesh.transform.InverseTransformPoint(canvasPoints[i+5]);
                        //endPoint.z += (endPoint.z - canvasPoints[i].z) * 0.3f;

                        this.AddLineSegment(i,canvasMesh.Verts,canvasMesh.Colors,canvasMesh.Triangles,canvasMesh.UVs,
                        startPoint, endPoint,
                        ortho,startOrthoSize,0);

                        int vertIndex = i * 4;
                        canvasMesh.UVs[vertIndex+0] = new Vector2(0.75f,0.0025f);
                        canvasMesh.UVs[vertIndex+1] = new Vector2(1,0.0025f);
                        canvasMesh.UVs[vertIndex+2] = new Vector2(0.87f,0.25f);
                        canvasMesh.UVs[vertIndex+3] = new Vector2(0.87f,0.25f);
                    }
                    else
                    {
                        // Rest of the lines
                        this.AddLineSegment(i,canvasMesh.Verts,canvasMesh.Colors,canvasMesh.Triangles,canvasMesh.UVs,
                        startPoint, endPoint,
                        ortho,startOrthoSize,endOrthoSize);
                    }

                    // Fade in the arrow
                    float numPointsInFade = Mathf.Clamp(numPoints - 6, 1f, 40f);
                    
                    if(i<numPointsInFade && numPoints > 5)
                    {
                        canvasMesh.Colors[i*2] = new Color(1,1,1,i/numPointsInFade);
                        canvasMesh.Colors[i*2+1] = new Color(1,1,1,i/numPointsInFade);
                    }
                }
                
            break;
            case ArrowType.Flat:
            {
                canvasMesh.Verts = new Vector3[4];
                canvasMesh.Colors = new Color32[4];
                canvasMesh.Triangles = new int [12];
                canvasMesh.UVs = new Vector2[canvasMesh.Verts.Length];
                canvasMesh.IsAdditiveBlending = true;

                Vector3 startPoint = canvasMesh.transform.InverseTransformPoint(sourceWorldPos);
                Vector3 endPoint = canvasMesh.transform.InverseTransformPoint(destWorldPos);

                Vector3 ortho2 = Vector3.Cross((endPoint - startPoint).normalized, Vector3.forward);
                ortho2.Normalize();

                //float orthoSize = 30f * (16f / (endPoint - startPoint).magnitude );

                this.AddLineSegment(0,canvasMesh.Verts,canvasMesh.Colors,canvasMesh.Triangles,canvasMesh.UVs,
                        startPoint, endPoint,
                        ortho2,30,0);

                // Poor mans second pass to make the additive effect more intense
                canvasMesh.Triangles[6] = canvasMesh.Triangles[0];
                canvasMesh.Triangles[7] = canvasMesh.Triangles[1];
                canvasMesh.Triangles[8] = canvasMesh.Triangles[2];
                canvasMesh.Triangles[9] = canvasMesh.Triangles[3];
                canvasMesh.Triangles[10] = canvasMesh.Triangles[4];
                canvasMesh.Triangles[11] = canvasMesh.Triangles[5];
                
                // Apply an alpha gradient to things
                canvasMesh.Colors[0] = new Color(1,1,1,0f);
                canvasMesh.Colors[1] = canvasMesh.Colors[0];
                canvasMesh.Colors[2] = new Color(1,1,1,0.7f);
                canvasMesh.Colors[3] = canvasMesh.Colors[2];

                canvasMesh.UVs[0] = new Vector2(0.75f,0.5f);
                canvasMesh.UVs[1] = new Vector2(1,0.5f);
                canvasMesh.UVs[2] = new Vector2(0.75f,0.75f);
                canvasMesh.UVs[3] = new Vector2(1,0.75f);
            }
            break;
        }
    }

    private void AddLineSegment(int segmentIndex, Vector3[] verts, Color32[] colors, int[] triangles, Vector2[] uvs, Vector3 startPoint, Vector3 endPoint, Vector3 ortho, float startOrthoSize, float endOrthoSize)
    {
        int vertIndex = segmentIndex * 4;
        float totalSegments = verts.Length / 4f;

        verts[vertIndex+0] = startPoint + ortho * startOrthoSize;
        verts[vertIndex+1] = startPoint - ortho * startOrthoSize;
        verts[vertIndex+2] = endPoint + ortho * endOrthoSize;
        verts[vertIndex+3] = endPoint - ortho * endOrthoSize;
        
        colors[vertIndex+0] = Color.white;
        colors[vertIndex+1] = Color.white;
        colors[vertIndex+2] = Color.white;
        colors[vertIndex+3] = Color.white;
        

        float numSegPerUV = 10;
        float minUV = (segmentIndex % numSegPerUV) / numSegPerUV;
        float maxUV = minUV + (1f/numSegPerUV);

        float minUVX = 0.0025f;
        float maxUVX = 0.25f - 0.0025f;
        float segmentOffset = segmentIndex / totalSegments;

        if(segmentOffset < 1f/3f)
        {
            // Do nothing
        }
        else if (segmentOffset < 2f/3f)
        {
            minUVX = 0.25f + 0.0025f;
            maxUVX = 0.5f - 0.0025f;
        } 
        else
        {
            minUVX = 0.5f + 0.0025f;
            maxUVX = 0.75f - 0.0025f;
        }

        uvs[vertIndex+0] = new Vector2(minUVX,minUV);
        uvs[vertIndex+1] = new Vector2(maxUVX,minUV);
        uvs[vertIndex+2] = new Vector2(minUVX,maxUV);
        uvs[vertIndex+3] = new Vector2(maxUVX,maxUV);

        int triIndex = segmentIndex * 6;

        triangles[triIndex+0] = vertIndex + 1;
        triangles[triIndex+1] = vertIndex + 0;
        triangles[triIndex+2] = vertIndex + 2;
        triangles[triIndex+3] = vertIndex + 2;
        triangles[triIndex+4] = vertIndex + 3;
        triangles[triIndex+5] = vertIndex + 1;
    }
    

    private CanvasMesh GetCanvasMesh()
    {
        CanvasMesh canvasMesh = null;
        if(this.canvasMeshes.Count > 0)
        {
            canvasMesh = this.canvasMeshes.Dequeue();
        }
        else
        {
            canvasMesh = this.CreateCanvasMesh();
        }
        canvasMesh.Clear();

        // If you're getting a renderer, you probably want it active
        canvasMesh.gameObject.SetActive(true);
        return canvasMesh;
    }

    private CanvasMesh CreateCanvasMesh()
    {
        CanvasMesh canvasMesh = null;
        GameObject go = GameObject.Instantiate(this.CanvasMeshPrefab.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
        canvasMesh = go.GetComponent<CanvasMesh>();
        go.transform.SetParent(this.transform.parent);
        go.transform.localScale = Vector3.one;
        go.transform.position = Vector3.zero;
        go.name = "CanvasMeshArrow";
        canvasMesh.Material = new Material(canvasMesh.Material);
        canvasMesh.AdditiveMaterial = new Material(canvasMesh.AdditiveMaterial);
        return canvasMesh;
    }
}



public enum ArrowType
{
    Flat,
    Curved,
}

public class TargetArrowInfo
{
    public Vector3 Destination;
    public Vector3 Source;
    public Transform FollowDestination;
    public Transform FollowSource;
    public Transform ArrowParent;
    public int? ChildIndex;
    public Color StartingColor;
    public ArrowType ArrowType;
    public TargetArrowTarget CurrentTarget;
    
}
