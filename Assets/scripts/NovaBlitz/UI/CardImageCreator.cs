﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace NovaBlitz.UI
{
    public class CardImageCreator : MonoBehaviour
    {
        private UI.GameData GameData {get;set;}
        public RectTransform Holder;
        public Camera Camera;

        public RenderTexture RenderTexture;
        private CardView cardView;
        private UnityEngine.UI.Mask mask;

        private HashSet<int> ModifiedCards = new HashSet<int>
        {
            900111,
            101088,
            900054,
            101271,
            101183,
            101162,
            101191,
            101083,
            101084,
            101181,
            900114,
            101078,
            101122,
            101184,
            900053,
            101290,
            101161,
            101165,
            101133,
            101080,
            101023,
            101117,
            101126,
            101115,
            101204,
            900100,
            101262,
            101274,
            101226,
            101025,
            101217,
            101087,
            101185,
            101255,
            101222,
            101026,
            101095,
            101201,
            900029,
            101297,
            101022,
            101275
        };

        private void Start()
        {
            //RenderTexture = this.GetComponentInChildren<Camera>().targetTexture;
        }

        [ContextMenu("Create EN Images...")]
        private void ProcessCardArtToImages()
        {
            var context = (CardBrowserView)FindObjectOfType<CardBrowserView>();
            GameData = context.GameData;
            Timer.Instance.StartCoroutine(GenerateImages());
        }

        private IEnumerator GenerateImages()
        {
            // add a card image from the pool
            if (cardView == null)
            {
                cardView = ObjectPool.SpawnPrefab<CardView>(PooledObjectType.CardView, null, this.Holder.transform);
                cardView.transform.localScale = Vector3.one;
                cardView.transform.localPosition = new Vector3(0, -7.0f, 0);
                mask = cardView.artMask.GetComponent<UnityEngine.UI.Mask>();
            }

            var directory = Path.Combine(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), "ArtExports"), "EN");
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            var basePath = Path.Combine(directory, "{0}.png");
            int count = 0;
            // for each data point in the card catalog, initialize the card & export
            foreach (var kvp in GameData.CardDictionary)
            {
                if(!GameData.CardProductCache.ContainsKey(kvp.Key) || !ModifiedCards.Contains(kvp.Key))
                {
                    continue;
                }
                var path = string.Format(basePath, kvp.Key);
                Debug.LogFormat("Exporting image to {0}", path);
                cardView.CardData = kvp.Value;
                cardView.UnitDropShadow.gameObject.SetActive(false);
                cardView.PowerDropShadow.gameObject.SetActive(false);
                yield return new WaitForEndOfFrame();
                // Load it into the CardListItemView

                // Make sure the renderTexture is the active one
                //RenderTexture.active = this.RenderTexture;
                //yield return new WaitForSecondsRealtime(1);
                // Copy the texture data from the render texture
                //Camera.Render();
                //yield return new WaitForEndOF;
                /*Texture2D tex2D = new Texture2D(this.RenderTexture.width, this.RenderTexture.height, TextureFormat.ARGB32, false, false);
                RenderTexture.active = this.RenderTexture;
                tex2D.ReadPixels(new Rect(0, 0, this.RenderTexture.width, this.RenderTexture.height), 0, 0, false);
                tex2D.Apply();*/
                RenderTexture.active = this.RenderTexture;
                Texture2D tex2D = Capture(new Rect(0, 0, this.RenderTexture.width, this.RenderTexture.height));

                // NEED TO SAVE TO PNG
                var bytes = tex2D.EncodeToPNG();
                File.WriteAllBytes(path, bytes);

                yield return null;
                //count++;
                if(count >10)
                    break;
            }
        }


        public Texture2D Capture(Rect pRect)
        {
            Camera lCamera = Camera;
            Texture2D lOut;
            var lPreClearFlags = lCamera.clearFlags;
            var lPreBackgroundColor = lCamera.backgroundColor;
            {
                lCamera.clearFlags = CameraClearFlags.Color;

                //make two captures with black and white background
                lCamera.backgroundColor = Color.black;
                lCamera.Render();
                var lBlackBackgroundCapture = CaptureView(pRect);

                lCamera.backgroundColor = Color.white;
                lCamera.Render();
                var lWhiteBackgroundCapture = CaptureView(pRect);
                
                for (int x = 0; x < lWhiteBackgroundCapture.width; ++x)
                {
                    for (int y = 0; y < lWhiteBackgroundCapture.height; ++y)
                    {
                        Color lColorWhenBlack = lBlackBackgroundCapture.GetPixel(x, y);
                        Color lColorWhenWhite = lWhiteBackgroundCapture.GetPixel(x, y);
                        if (lColorWhenBlack != Color.clear)
                        {
                            //set real color
                            lWhiteBackgroundCapture.SetPixel(x, y,
                                GetColor(lColorWhenBlack, lColorWhenWhite));
                        }
                    }
                }
                lWhiteBackgroundCapture.Apply();
                lOut = lWhiteBackgroundCapture;
                UnityEngine.Object.DestroyImmediate(lBlackBackgroundCapture);
            }
            lCamera.backgroundColor = lPreBackgroundColor;
            lCamera.clearFlags = lPreClearFlags;
            return lOut;
        }

        /// <summary>
        /// Capture a screenshot(not include GUI)
        /// </summary>
        /// <returns></returns>
        public Texture2D CaptureScreenshot()
        {
            return Capture(new Rect(0f, 0f, Screen.width, Screen.height));
        }

        /// <summary>
        /// Capture a screenshot(not include GUI) at path filename as a PNG file
        /// eg. zzTransparencyCapture.captureScreenshot("Screenshot.png")
        /// </summary>
        /// <param name="pFileName"></param>
        /// <returns></returns>
        public void CaptureScreenshot(string pFileName)
        {
            var lScreenshot = CaptureScreenshot();
            try
            {
                using (var lFile = new FileStream(pFileName, FileMode.Create))
                {
                    BinaryWriter lWriter = new BinaryWriter(lFile);
                    lWriter.Write(lScreenshot.EncodeToPNG());
                }
            }
            finally
            {
                UnityEngine.Object.DestroyImmediate(lScreenshot);
            }
        }

        //pColorWhenBlack!=Color.clear
        private Color GetColor(Color pColorWhenBlack, Color pColorWhenWhite)
        {
            float lAlpha = GetAlpha(pColorWhenBlack.r, pColorWhenWhite.r);
            return new Color(
                pColorWhenBlack.r / lAlpha,
                pColorWhenBlack.g / lAlpha,
                pColorWhenBlack.b / lAlpha,
                lAlpha);
        }


        //           Color*Alpha      Color   Color+(1-Color)*(1-Alpha)=1+Color*Alpha-Alpha
        //0----------ColorWhenZero----Color---ColorWhenOne------------1
        private float GetAlpha(float pColorWhenZero, float pColorWhenOne)
        {
            //pColorWhenOne-pColorWhenZero=1-Alpha
            return 1 + pColorWhenZero - pColorWhenOne;
        }

        private Texture2D CaptureView(Rect pRect)
        {
            Texture2D lOut = new Texture2D((int)pRect.width, (int)pRect.height, TextureFormat.ARGB32, false);
            lOut.ReadPixels(pRect, 0, 0, false);
            return lOut;
        }

    }
}
