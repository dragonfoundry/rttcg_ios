﻿using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

namespace NovaBlitz.UI
{
    public class GameLogListItem : MonoBehaviour
    {
        public Action<int> ClickLog;
        public Image Background;
        public TextMeshProUGUI Date;
        public TextMeshProUGUI Opponent;
        public TextMeshProUGUI Format;
        public TextMeshProUGUI Turns;
        public TextMeshProUGUI Result;
        public int LogNumber;
        public Sprite winSprite;
        public Sprite loseSprite;

        public void OnClick()
        {
            if (ClickLog != null)
            {
                ClickLog.Invoke(LogNumber);
            }
        }
    }
}
