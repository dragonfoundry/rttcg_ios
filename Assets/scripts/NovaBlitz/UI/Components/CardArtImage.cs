﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.UI;
using NovaBlitz;

public class CardArtImage : RawImage
{
    public ArtPositioningKey Key;
    public bool IsArtLoaded = false;
    public static Dictionary<ArtPositioningKey, ArtPositioningData> PositionData;
    public string ArtId
    {
        get { return (this.texture != null ? this.texture.name : string.Empty); }
    }

    private RectMask2D rectMask2D = null;
    private static AssetManagerService AssetManagerService { get;set;}
    
    override protected void Awake()
    {
        base.Awake();
        this.rectMask2D = this.transform.parent.gameObject.GetComponent<RectMask2D>();
        if(this.rectMask2D != null)
        {
            //this.rectMask2D.enabled = false;
        }
        if(AssetManagerService == null)
        {
            AssetManagerService = GameObject.FindObjectOfType<AssetManagerService>();
        }
    }
    
    private void ApplyDefaultAspectArt(CardAspect aspect)
    {
        string cardArtId = null;

        if (aspect == CardAspect.Arcane)
            cardArtId = "DC0353";
        else if (aspect == CardAspect.Tech)
            cardArtId = "DC0351";
        else if (aspect == CardAspect.Divine)
            cardArtId = "DC0352";
        else if (aspect == CardAspect.Nature)
            cardArtId = "DC0354";
        else if (aspect == CardAspect.Chaos)
            cardArtId = "DC0355";

        if(this.rectMask2D != null) { this.rectMask2D.enabled = true; }
        
        if(string.IsNullOrEmpty(cardArtId) == false)
        {
            this.texture = AssetBundleManager.Instance.LoadAsset<Texture>(cardArtId);
            //this.texture = Resources.Load(cardArtId) as Texture;
        }
        
        this.ResetPositioningData();
    }

    private string artIdToAsyncLoad = string.Empty;
    public void ResetArt()
    {
        if(!string.IsNullOrEmpty(this.artIdToAsyncLoad))
        {
            Debug.LogFormat("Cancelling async art load: {0}", this.artIdToAsyncLoad);
            AssetManagerService.CancelLoadAsset<Texture>(this.OnCardArtLoaded, this.artIdToAsyncLoad);
        }
        this.OnAssetLoadedCallback = null;
        this.artIdToAsyncLoad = string.Empty;
        this.texture = null;
    }
    private Action OnAssetLoadedCallback;

    public bool UpdateCardArt(string cardArtId, CardAspect aspect, Action onAssetLoadedCallback = null)
    {
        bool isAsyncLoading = false;
        if (artIdToAsyncLoad == cardArtId)
            return isAsyncLoading;
        if(!string.IsNullOrEmpty(cardArtId))
        {
            if(this.ArtId != cardArtId)
            {
                artIdToAsyncLoad = cardArtId;
                this.ApplyDefaultAspectArt(aspect);
                if (!string.IsNullOrEmpty(cardArtId))
                {
                    OnAssetLoadedCallback = onAssetLoadedCallback;
                    isAsyncLoading = !AssetManagerService.LoadAsset<Texture>(this.texture, cardArtId, this.OnCardArtLoaded);
                }
            }
            else
            {
                this.ApplyPositioningData(cardArtId);
                artIdToAsyncLoad = string.Empty;
            }
        }
        
        if(this.rectMask2D != null) { this.rectMask2D.enabled = true; }
        if(!isAsyncLoading && onAssetLoadedCallback != null)
        {
            onAssetLoadedCallback.Invoke();
        }
        return isAsyncLoading;
    }
    
    private void OnCardArtLoaded(Texture texture, string assetPath)
    {
        // this != null seems really weird for C#, but this is what unity does, afer an object 
        // is marked for deletion "this" still exists but it returns null until it's completely destroyed
        if (texture != null && this != null && this.ArtId != texture.name && this.artIdToAsyncLoad == texture.name)
        {
            if (this.rectMask2D != null) { this.rectMask2D.enabled = true; }
            this.texture = texture;
            this.ApplyPositioningData(texture.name); // Name happens to be the cardArtId
            this.artIdToAsyncLoad = string.Empty;
        }
        if (OnAssetLoadedCallback != null)
            OnAssetLoadedCallback.Invoke();
    }
    
    public void ResetPositioningData()
    {
        ((RectTransform)this.transform).localScale = Vector3.one;
        ((RectTransform)this.transform).anchoredPosition = Vector2.zero;
    }
    
    public void ApplyPositioningData(string cardArtId)
    {
        // Load and Apply positioning information
        ArtPositioningData positioningData = null;
        if(this.Key != ArtPositioningKey.NoKey 
            && string.IsNullOrEmpty(cardArtId) == false 
            && CardArtImage.PositionData.TryGetValue(this.Key, out positioningData))
        {
            // If there is position data, aply it...
            ArtPosition data = positioningData.Get(cardArtId);
            if (data != null && this != null)
            {
                ((RectTransform)this.transform).localScale = data.Scale;
                ((RectTransform)this.transform).anchoredPosition = data.Position;
            }
            else
                Debug.LogWarningFormat("missing card art data for: {0}", cardArtId);
        }
        IsArtLoaded = true;
    }
}
