using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using NovaBlitz.UI;

namespace NovaBlitz.Game
{
	public class CardAspectToggle : DataToggle<CardAspect> {}
}
