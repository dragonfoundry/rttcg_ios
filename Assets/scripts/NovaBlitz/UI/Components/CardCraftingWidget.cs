﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using TMPro;

namespace NovaBlitz.UI
{
	public class CardCraftingWidget : MonoBehaviour
	{
		public event Action EventCraftClicked;
		public event Action EventDisenchantClicked;

		[SerializeField] protected TextMeshProUGUI _NanitesTotalLabel;
		[SerializeField] protected TextMeshProUGUI _CraftPriceLabel;
		[SerializeField] protected TextMeshProUGUI _DisenchantPriceLabel;
		[SerializeField] protected Button _CraftButton;
		[SerializeField] protected Button _DisenchantButton;

		public int NanitesTotal 	{ set { _NanitesTotalLabel.text = value.ToString(); } }
		public uint CraftPrice 		{ set { _CraftPriceLabel.text = string.Format("-{0}", value); } }
		public uint DisenchantPrice { set { _DisenchantPriceLabel.text = string.Format("+{0}", value); } }
		public bool CanCraft 		{ set { _CraftButton.interactable = value; } }
		public bool CanDisenchant 	{ set { _DisenchantButton.interactable = value; } }
		public bool HideButtons 	{ set { _CraftButton.gameObject.SetActive(!value); _DisenchantButton.gameObject.SetActive(!value); } }

		#region uGUI
		public void ClickCraft() 		{ if (null != this.EventCraftClicked) this.EventCraftClicked.Invoke(); }
		public void ClickDisenchant() 	{ if (null != this.EventDisenchantClicked) this.EventDisenchantClicked.Invoke(); }
		#endregion
	}
}
