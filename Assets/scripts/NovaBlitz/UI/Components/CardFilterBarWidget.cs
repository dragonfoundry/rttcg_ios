﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections.Generic;
using TMPro;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class CardFilterBarWidget : MonoBehaviour
	{
		public event Action EventFilterClicked;
		public event Action EventResetClicked;
		public event Action EventSearchClearClicked;
		public event Action<string> EventSearchChanged;

		public TMP_InputField SearchField;
		public TextMeshProUGUI PhraseIndicator;
		public CardCostsIndicator CostsIndicator;
		public CardTypesIndicator TypesIndicator;
		public CardAspectsIndicator AspectsIndicator;
		public CardRaritiesIndicator RaritiesIndicator;

		public string SearchPhrase { set { /*this.PhraseIndicator.text = value ?? string.Empty; */this.SearchField.text = value ?? string.Empty; } }

		public void ClickFilterButton() { if (null != this.EventFilterClicked) this.EventFilterClicked.Invoke(); }
		public void ClickResetButton() { if (null != this.EventResetClicked) this.EventResetClicked.Invoke(); }
		public void ClickSearchClear() { if (null != this.EventSearchClearClicked) this.EventSearchClearClicked.Invoke(); }

		protected void OnSearchEndEdit(string phrase) { if (null != this.EventSearchChanged) this.EventSearchChanged.Invoke(phrase); }

		void Awake()
		{
			this.SearchField.onValueChanged.AddListener(OnSearchEndEdit);
		}
	}

	public abstract class MultiIconIndicator<T>
	{
		protected Dictionary<T, GameObject> _IconMap;

		protected abstract void InitMap();

		public void ShowIcons(IEnumerable<T> values)
		{
			if (null == _IconMap)
				InitMap();
			if (null == values) {
				foreach (var icon in _IconMap.Values)
					icon.SetActive(false);
				return;
			}

			foreach (var kvp in _IconMap)
				kvp.Value.SetActive(values.Contains(kvp.Key));
		}
	}

	[Serializable]
	public class CardCostsIndicator : MultiIconIndicator<int>
	{
		[SerializeField]
		protected GameObject[] _Icons;

		protected override void InitMap()
		{
			_IconMap = new Dictionary<int, GameObject>();
			for (int i=0; i<_Icons.Length; ++i)
				_IconMap[i] = _Icons[i];
			_IconMap = _Icons.ToDictionary(k => Array.IndexOf(_Icons, k), v => v);
		}
	}

	[Serializable]
	public class CardTypesIndicator : MultiIconIndicator<CardType>
	{
		[SerializeField]
		protected GameObject _UnitIcon;
		[SerializeField]
		protected GameObject _PowerIcon;

		protected override void InitMap()
		{
			_IconMap = new Dictionary<CardType, GameObject>{
				{CardType.Unit, _UnitIcon},
				{CardType.Power, _PowerIcon}
			};
		}
	}

	[Serializable]
	public class CardAspectsIndicator : MultiIconIndicator<CardAspect>
	{
		[SerializeField]
		protected GameObject _TechIcon;
		[SerializeField]
		protected GameObject _DivineIcon;
		[SerializeField]
		protected GameObject _ArcaneIcon;
		[SerializeField]
		protected GameObject _NatureIcon;
		[SerializeField]
		protected GameObject _ChaosIcon;

		protected override void InitMap()
		{
			_IconMap = new Dictionary<CardAspect, GameObject>{
				{CardAspect.Tech, _TechIcon},
				{CardAspect.Divine, _DivineIcon},
				{CardAspect.Arcane, _ArcaneIcon},
				{CardAspect.Nature, _NatureIcon},
				{CardAspect.Chaos, _ChaosIcon}
			};
		}
	}

	[Serializable]
	public class CardRaritiesIndicator : MultiIconIndicator<CardRarity>
	{
		[SerializeField]
		protected GameObject _CommonIcon;
		[SerializeField]
		protected GameObject _RareIcon;
		[SerializeField]
		protected GameObject _EpicIcon;
		[SerializeField]
		protected GameObject _LegendaryIcon;

		protected override void InitMap()
		{
			_IconMap = new Dictionary<CardRarity, GameObject>{
				{CardRarity.Common, _CommonIcon},
				{CardRarity.Rare, _RareIcon},
				{CardRarity.Epic, _EpicIcon},
				{CardRarity.Legendary, _LegendaryIcon}
			};
		}
	}
}