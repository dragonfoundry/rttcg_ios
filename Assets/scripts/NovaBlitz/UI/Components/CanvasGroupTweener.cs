﻿using UnityEngine;
using UnityEngine.UI;

namespace NovaBlitz.UI
{
	public class CanvasGroupTweener : TweenerComponent
	{
		[SerializeField]
		protected TweenerCGAlpha _Tweener;

		public override TweenerBase Tweener { get { return _Tweener; } }
	}

	[System.Serializable]
	public class TweenerCGAlpha : TweenerBase
	{
		public CanvasGroup Group;

		[SerializeField]
		protected AnimationCurve _AlphaCurve;

		protected override void Evaluate(float normTime)
		{
			this.Group.alpha = _AlphaCurve.Evaluate(normTime);
		}
	}
}
