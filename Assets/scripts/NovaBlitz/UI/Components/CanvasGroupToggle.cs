using UnityEngine;
using UnityEngine.UI;

public class CanvasGroupToggle : MonoBehaviour
{
	public CanvasGroup Group;
	public bool TogglesAlpha;
	public bool TogglesInteractable;
	public bool TogglesBlockRaycasts;

	protected bool _IsOn;
	public bool IsOn {
		get { return _IsOn; }
		set { _IsOn = value; Toggle(value); }
	}

	protected void Toggle(bool isOn)
	{
		if (this.TogglesAlpha)
			this.Group.alpha = isOn ? 1f : 0f;
		if (this.TogglesInteractable)
			this.Group.interactable = isOn;
		if (this.TogglesBlockRaycasts)
			this.Group.blocksRaycasts = isOn;
	}
}
