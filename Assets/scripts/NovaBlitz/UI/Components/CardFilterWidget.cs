using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using LinqTools;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class CardFilterWidget : MonoBehaviour
	{
        
		[SerializeField]
		protected bool _ApplyWhenChanged;
		[SerializeField]
		protected Toggle _AllToggle;
		[SerializeField]
		protected Toggle _OwnedToggle;
		[SerializeField]
		protected InputField _SearchField;
		[SerializeField]
		protected MultiIntToggle _CostFilter;
		[SerializeField]
		protected MultiCardTypeToggle _TypeFilter;
		[SerializeField]
		protected MultiCardAspectToggle _AspectFilter;
		[SerializeField]
		protected MultiCardRarityToggle _RarityFilter;

		[SerializeField] protected Button clearCostButton;
		[SerializeField] protected Button clearAspectButton;
		[SerializeField] protected Button clearRarityButton;
		[SerializeField] protected Button clearTypeButton;
		
		public event Action<CardFilterParams> EventApplyClicked;

		public bool IsFiltersApplied
		{
			get {
				return this.clearCostButton.interactable || this.clearAspectButton.interactable || this.clearRarityButton.interactable || this.clearTypeButton.interactable;
			}
		}
		
		protected void Awake()
		{
			if(_SearchField != null)
			{
				_SearchField.onEndEdit.AddListener(OnSearchFieldEdited);
			}
			_CostFilter.Init();
			_TypeFilter.Init();
			_AspectFilter.Init();
			_RarityFilter.Init();

			_CostFilter.EventChanged += OnFilterGroupChanged;
			_TypeFilter.EventChanged += OnFilterGroupChanged;
			_AspectFilter.EventChanged += OnFilterGroupChanged;
			_RarityFilter.EventChanged += OnFilterGroupChanged;

            _OwnedToggle.isOn = true;
            this.clearCostButton.onClick.AddListener(this.ClearCost);
			this.clearAspectButton.onClick.AddListener(this.ClearAspect);
			this.clearRarityButton.onClick.AddListener(this.ClearRarity);
			this.clearTypeButton.onClick.AddListener(this.ClearType);
			_OwnedToggle.onValueChanged.AddListener(v => OnFilterGroupChanged());

        }

		protected void OnSearchFieldEdited(string phrase)
		{
			if (_ApplyWhenChanged)
				ClickApply();
		}

		protected void OnFilterGroupChanged()
		{
			if (_ApplyWhenChanged)
				ClickApply();
		}
		
		public void ClickApply()
		{
			if (null != this.EventApplyClicked)
				this.EventApplyClicked.Invoke(GetParamsFromToggles());
		}

        public void ApplyTwoAspectFilter(PlayerProfile profile)
        {
            CardFilterParams filterParams;
            if (profile != null && profile.CurrentCardFilter != null)
                filterParams = profile.CurrentCardFilter;
            else
                filterParams = GetParamsFromToggles();
            filterParams.Aspects = _AspectFilter.ActiveValues.ToArray();
            if (null != this.EventApplyClicked)
                this.EventApplyClicked.Invoke(filterParams);
        }
		
        private CardFilterParams GetParamsFromToggles()
        {
            return new CardFilterParams()
            {
                CollectionType = _OwnedToggle.isOn ? CardCollectionType.MyCards : CardCollectionType.AllCards,
                SearchPhrase = _SearchField != null ? _SearchField.text : string.Empty,
                Costs = _CostFilter.ActiveValues.ToArray(),
                Types = _TypeFilter.ActiveValues.ToArray(),
                Aspects = _AspectFilter.ActiveValues.ToArray(),
                Rarities = _RarityFilter.ActiveValues.ToArray()
            };
        }

		public void ClearSearch() { if(_SearchField != null) {_SearchField.text = string.Empty; _SearchField.onEndEdit.Invoke(string.Empty);} }
		public void ClearCost() { _CostFilter.SetAll(false); this.clearCostButton.interactable = false;}
		public void ClearType() { _TypeFilter.SetAll(false); this.clearTypeButton.interactable = false;}
		public void ClearAspect() { _AspectFilter.SetAll(false); this.clearAspectButton.interactable = false;}
		public void ClearRarity() { _RarityFilter.SetAll(false); this.clearRarityButton.interactable = false;}
		public void ClearAll()
		{
			bool awc = _ApplyWhenChanged;
			_ApplyWhenChanged = false;
			ClearSearch();
			ClearCost();
			ClearType();
			ClearAspect();
			ClearRarity();
			_ApplyWhenChanged = awc;
		}

        public void LockInAspects(List<CardAspect> aspects, PlayerProfile profile, CardAspect addedAspect)
        {
            var activeFilters = _AspectFilter.ActiveValues.ToList();
			if (aspects.Count == 2 
                && (activeFilters.Count == 0 || activeFilters.Count > 2 
                    || (addedAspect != CardAspect.NoAspect && !activeFilters.Contains(addedAspect)))) // !(activeFilters.Contains(aspects[0]) && activeFilters.Contains(aspects[1]))))
            {
				bool awc = _ApplyWhenChanged;
				_ApplyWhenChanged = false;
				_AspectFilter.SetToSpecificState(aspects);
				_ApplyWhenChanged = awc;
                ApplyTwoAspectFilter(profile);
            }
        }

		public void MatchState(CardFilterParams filterState)
		{
			bool awc = _ApplyWhenChanged;
			_ApplyWhenChanged = false;
			if(filterState.CollectionType == CardCollectionType.MyCards && !_OwnedToggle.isOn)
				_OwnedToggle.Select();
			else if(!(filterState.CollectionType == CardCollectionType.MyCards) && !_AllToggle.isOn)
                _AllToggle.Select();
			_OwnedToggle.isOn = filterState.CollectionType == CardCollectionType.MyCards;
			if(_SearchField != null && (string.IsNullOrEmpty(filterState.SearchPhrase) || _SearchField.text != filterState.SearchPhrase))
			{
				_SearchField.text = filterState.SearchPhrase ?? string.Empty;
			}
			this.clearCostButton.interactable = _CostFilter.SetToSpecificState(filterState.Costs);
			this.clearTypeButton.interactable = _TypeFilter.SetToSpecificState(filterState.Types);
			this.clearAspectButton.interactable = _AspectFilter.SetToSpecificState(filterState.Aspects);
			this.clearRarityButton.interactable = _RarityFilter.SetToSpecificState(filterState.Rarities);
			_ApplyWhenChanged = awc;
		}
	}
}
