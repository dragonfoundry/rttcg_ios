using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Add this to a Selectable element to support next/prev navigation. Good for InputField, where standard navigation is locked.
	/// </summary>
	public class LinearNavigation : MonoBehaviour, ISelectHandler, IDeselectHandler
	{
		[SerializeField]
		protected RectTransform.Axis _Direction;
		[SerializeField]
		protected KeyCode[] _NextKeys;
		[SerializeField]
		protected KeyCode[] _PrevKeys;
		[SerializeField]
		protected KeyCode[] _ModifierKeys;
		
		protected Selectable _Selectable;
		protected bool _IsSelected;

		public void OnSelect(BaseEventData eventData)
		{
			_IsSelected = true;
		}

		public void OnDeselect(BaseEventData eventData)
		{
			_IsSelected = false;
		}

		void Start()
		{
			_Selectable = GetComponent<Selectable>();
			if (null == _Selectable)
				enabled = false;
		}

		// TODO: Refactor for use with a master input handler.
		void Update()
		{
			if (_IsSelected)
			{
				bool mod = false;
				bool next = false;
				bool prev = false;

				foreach (KeyCode code in _ModifierKeys)
				{
					if (Input.GetKey(code))
					{
						mod = true;
						break;
					}
				}

				foreach (KeyCode code in _NextKeys)
				{
					if (Input.GetKeyDown(code))
					{
						next = true;
						break;
					}
				}
				
				foreach (KeyCode code in _PrevKeys)
				{
					if (Input.GetKeyDown(code))
					{
						prev = true;
						break;
					}
				}

				if (next)
					Navigate(mod);
				else if (prev)
					Navigate(!mod);
			}
		}

		void Navigate(bool reverse)
		{
			bool horz = _Direction == RectTransform.Axis.Horizontal;
			Selectable target = null;
			
			switch (_Selectable.navigation.mode)
			{
			case Navigation.Mode.Explicit:
				if (horz)
					target = reverse ? _Selectable.navigation.selectOnLeft : _Selectable.navigation.selectOnRight;
				else target = reverse ? _Selectable.navigation.selectOnUp : _Selectable.navigation.selectOnDown;
				break;
			default:
				if (horz)
					target = reverse ? _Selectable.FindSelectableOnLeft() : _Selectable.FindSelectableOnRight();
				else target = reverse ? _Selectable.FindSelectableOnUp() : _Selectable.FindSelectableOnDown();
				break;
			}

			if (null != target)
				target.Select();
		}
	}
}
