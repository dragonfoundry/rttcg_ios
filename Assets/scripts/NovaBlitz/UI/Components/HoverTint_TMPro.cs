﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.EventSystems;
using TMPro;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Tints color of a graphic or TMPro text when hovered. Attach to a button in order to tint its label text.
	/// </summary>
	public class HoverTint_TMPro : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
	{
		public Color HoverColor;
		[FormerlySerializedAs("_Graphic")]
		[SerializeField]
		protected Graphic graphic;

		protected TMP_Text _TMProText;
		protected Color _NormalColor;
		protected bool _IsSelected;
		
		private Button button;
        private Toggle toggle;
		private Sprite defaultButtonImage;
		private Sprite hoveredButtonImage;

		protected void Awake()
		{
			if (null == this.graphic)
			{
				this.graphic = GetComponent<Graphic>();
			}
			
			if(this.graphic != null && this.button == null && this.toggle == null)
			{
				this.button = this.graphic.GetComponentInParent<Button>();
                this.toggle = this.graphic.GetComponentInParent<Toggle>();
                if (this.button != null && this.button.targetGraphic is Image)
				{
					this.defaultButtonImage = this.button.image.sprite;
					this.hoveredButtonImage = this.button.spriteState.highlightedSprite;
                }
                if (this.toggle != null && this.toggle.targetGraphic is Image)
                {
                    this.defaultButtonImage = this.toggle.image.sprite;
                    this.hoveredButtonImage = this.toggle.spriteState.highlightedSprite;
                }
            }
			
			_TMProText = graphic as TMP_Text;
			_NormalColor = null != _TMProText ? _TMProText.color : graphic.color;
		}
		
		public void Update()
		{
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
				if(Input.touchCount == 0)
				{
					this.SwitchColor(false);
				}
#endif
        }

        protected void SwitchColor(bool isHovered)
		{
			if (null != _TMProText)
			{
				_TMProText.color = isHovered ? this.HoverColor : _NormalColor;
			}
			graphic.color = isHovered ? this.HoverColor : _NormalColor;
			if(this.button != null)
			{
				this.button.image.sprite = isHovered ? this.hoveredButtonImage : this.defaultButtonImage;
			}
            if (this.toggle != null)
            {
                this.toggle.image.sprite = isHovered ? this.hoveredButtonImage : this.defaultButtonImage;
            }
        }

		public void OnPointerEnter(PointerEventData eventData)
		{
			SwitchColor(true);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (!_IsSelected)
				SwitchColor(false);
		}

		public void OnSelect(BaseEventData eventData)
		{
			_IsSelected = true;
			SwitchColor(true);
		}

		public void OnDeselect(BaseEventData eventData)
		{
			_IsSelected = false;
			SwitchColor(false);
		}
	}
}
