﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using NovaBlitz.Economy;
using DragonFoundry.Fx;
using GameFormat = Messages.GameFormat;
using Messages;

namespace NovaBlitz.UI
{
	[Serializable]
	public class PlayStatusWidget
	{
		public GameFormat Format;
		[SerializeField]
		public TextMeshProUGUI _WinsDisplay;

		public int Wins { set { _WinsDisplay.text = value.ToString(); } }
		public int Losses { set { 
            if (LossArray != null)
            {
                for (int i = 0; i < LossArray.Count(); i++)
                {
                        if (i < value)
                            LossArray[i].alpha = 1;
                        else
                            LossArray[i].alpha = 0;

                    }
                }
        } }
        public TextMeshProUGUI[] LossArray;
	}

	[Serializable]
	public class PlayLauncherWidget
    {
        public event Action EventPracticeClicked;
        public event Action<GameFormat> EventPlayClicked;
		public event Action<GameFormat> EventRetireClicked;
        public event Action<GameFormat, CurrencyType, int> EventPurchaseClicked;
        public event Action<GameFormat> EventFreePlayClicked;

        private Dictionary<CurrencyType, int> prices;

		public GameFormat Format;
        [SerializeField] protected Button _PracticeButton;
        [SerializeField] protected Button _PlayButton;
		[SerializeField] protected Button _RetireButton;
		[SerializeField] protected TextMeshProUGUI _PlayButtonLabel;
        
        [SerializeField] protected Button _EntryQP_Button;
        [SerializeField] protected TextMeshProUGUI _EntryQP_ButtonLabel;
        private int _qpPrice = 0;
        [SerializeField] protected Button _EntryCR_Button;
        [SerializeField] protected TextMeshProUGUI _EntryCR_ButtonLabel;
        private int _crPrice = 0;
        [SerializeField] protected Button _EntryNG_Button;
        [SerializeField] protected TextMeshProUGUI _EntryNG_ButtonLabel;
        private int _ngPrice = 0;
        [SerializeField] protected Button _EntryFREE_Button;
        [SerializeField] protected TextMeshProUGUI _EntryFREE_ButtonLabel;
        [SerializeField] protected TextMeshProUGUI _EntryFREE_Number;
        
        public ParticleEffectsTrigger PlayButtonFx;
        public ParticleEffectsTrigger FreeEntryButtonFx;

        public bool IsFreeEnabled { get { return _EntryFREE_Button.IsActive() && _EntryFREE_Button.interactable; } }
        public bool IsPlayEnabled { get { return _PlayButton.IsActive() && _PlayButton.interactable; } }
        public bool IsPracticeEnabled { set { _PracticeButton.gameObject.SetActive(value);/* _PracticeButton.interactable = value;*/ } get { return _PracticeButton.IsActive() && _PracticeButton.interactable; } }
		public bool IsReady { set
            {
                _PlayButton.interactable = value;
                _PracticeButton.interactable = value;
                _EntryQP_Button.interactable = value;
                _EntryCR_Button.interactable = value;
                _EntryNG_Button.interactable = value;
            } }
        public bool IsTournamentEntry { set { _PlayButton.gameObject.SetActive(!value); } }
		public string PlayLabel { set { _PlayButtonLabel.text = value; } }

		public void Init()
        {
            _PracticeButton.onClick.AddListener(ClickPractice);
            _PlayButton.onClick.AddListener(ClickPlay);
			_RetireButton.onClick.AddListener(ClickRetire);
            _EntryFREE_Button.onClick.AddListener(ClickFREEEntry);
            _EntryNG_Button.onClick.AddListener(ClickNGEntry);
            _EntryQP_Button.onClick.AddListener(ClickQPEntry);
            _EntryCR_Button.onClick.AddListener(ClickCREntry);
        }

        public void SetButtons(Dictionary<CurrencyType, int> entryFees, Dictionary<CurrencyType, int> balances, GameFormat format)
        {

            int freeAvailable = 0;
            if (format == GameFormat.Draft)
                balances.TryGetValue(CurrencyType.FD, out freeAvailable);
            else if (format == GameFormat.Constructed)
                balances.TryGetValue(CurrencyType.FC, out freeAvailable);
            bool isFreeAvailable = freeAvailable > 0;
            _EntryCR_Button.gameObject.SetActive(!isFreeAvailable && entryFees.TryGetValue(CurrencyType.CR, out _crPrice));
            _EntryQP_Button.gameObject.SetActive(!isFreeAvailable && entryFees.TryGetValue(CurrencyType.QP, out _qpPrice));
            _EntryNG_Button.gameObject.SetActive(entryFees.TryGetValue(CurrencyType.NG, out _ngPrice));
            _EntryFREE_Button.gameObject.SetActive(isFreeAvailable);
            _EntryFREE_Number.text = freeAvailable.ToString();
            _EntryNG_Button.interactable = !isFreeAvailable;
            _EntryCR_ButtonLabel.SetText("{0}", _crPrice);
            _EntryQP_ButtonLabel.SetText("{0}", _qpPrice);
            _EntryNG_ButtonLabel.SetText("{0}", _ngPrice);
        }


        protected void ClickPractice() { if (null != this.EventPracticeClicked) this.EventPracticeClicked.Invoke(); }
        protected void ClickPlay() { if (null != this.EventPlayClicked) this.EventPlayClicked.Invoke(this.Format); }
		protected void ClickRetire() { if (null != this.EventRetireClicked) this.EventRetireClicked.Invoke(this.Format); }
        protected void ClickQPEntry() { if (null != this.EventPurchaseClicked) this.EventPurchaseClicked.Invoke(this.Format, CurrencyType.QP, _qpPrice); }
        protected void ClickCREntry() { if (null != this.EventPurchaseClicked) this.EventPurchaseClicked.Invoke(this.Format, CurrencyType.CR, _crPrice); }
        protected void ClickNGEntry() { if (null != this.EventPurchaseClicked) this.EventPurchaseClicked.Invoke(this.Format, CurrencyType.NG, _ngPrice); }
        protected void ClickFREEEntry() { if (null != this.EventFreePlayClicked) this.EventFreePlayClicked.Invoke(this.Format); }
    }

    [Serializable]
    public class TournamentInfoWidget
    {
        public Action EventCurrentTournamentUpdated;

        public TextMeshProUGUI TournamentEntryInfo;
        public TextMeshProUGUI TournamentStatus;
        public TextMeshProUGUI TimeText;
        public TextMeshProUGUI StatusHeader;

        public TournamentEventData CurrentEvent { get; private set; }

        public Button[] EnterButtons;
        
        private bool _IsInteractible;
        public bool IsInteractible {get { return _IsInteractible; } set { if (value != _IsInteractible) {
                    _IsInteractible = value;
                    foreach (var button in EnterButtons) {
                        //button.interactable = value;
                    }
                } } }

        public bool IsPlaying { get; private set; }
        public bool IsOver { get; set; }

        public void Init(TournamentEventData currentEvent)
        {
            // Make sure the buttons and interactibility line up;
            IsInteractible = true;
            IsInteractible = false;
            if (currentEvent != null)
            {
                CurrentEvent = currentEvent;
                IsPlaying = true;
            }
            else
                CurrentEvent = GameData.TournamentSchedule.Where(t => t.Value.LastEntryTimeUTC > DateTime.UtcNow).FirstOrDefault().Value;
            if (EventCurrentTournamentUpdated != null) { EventCurrentTournamentUpdated.Invoke(); }
        }

        public void UpdateStatusAndTime()
        {
            if(StatusHeader != null)
                StatusHeader.SetText(TimeUtils.FormatDateTime(DateTime.Now));
            if (CurrentEvent == null)
            {
                IsPlaying = false;
                IsOver = true;
                IsInteractible = false;
                return;
            }
            bool canEnter = !IsPlaying && DateTime.UtcNow > CurrentEvent.StartTimeUTC && DateTime.UtcNow < CurrentEvent.LastEntryTimeUTC;
            IsOver = DateTime.UtcNow > CurrentEvent.EndTimeUTC || (!IsPlaying && DateTime.UtcNow > CurrentEvent.LastEntryTimeUTC);

            if (canEnter)
            {
                TournamentStatus.SetText(I2.Loc.ScriptLocalization.Event.RegistrationClosesIn);
                TimeText.SetText(TimeUtils.FormatTimeDifference(DateTime.UtcNow, CurrentEvent.LastEntryTimeUTC));
                IsInteractible = true;
            }
            else if (!IsPlaying)
            {
                TournamentStatus.SetText(I2.Loc.ScriptLocalization.Event.NextEventIn);
                if (CurrentEvent.LastEntryTimeUTC < DateTime.UtcNow)
                    Init(null);
                TimeText.SetText(TimeUtils.FormatTimeDifference(DateTime.UtcNow, CurrentEvent.StartTimeUTC));
                IsInteractible = false;
            }
            else if (DateTime.UtcNow < CurrentEvent.EndTimeUTC)
            {
                TournamentStatus.SetText(I2.Loc.ScriptLocalization.Event.TimeRemaining);
                TimeText.SetText(TimeUtils.FormatTimeDifference(DateTime.UtcNow, CurrentEvent.EndTimeUTC));
                IsInteractible = true;
            }
            else
            {
                TournamentStatus.SetText(I2.Loc.ScriptLocalization.Event.EventOver);
                TimeText.SetText("");
                IsInteractible = false;
            }
        }
    }

	[Serializable]
	public class AudioSettingsWidget
	{
		public event Action<int> EventSFXVolumeChanged;
		public event Action<int> EventMusicVolumeChanged;

		[SerializeField]
		protected Slider _SFXVolumeSlider;
		[SerializeField]
		protected Slider _MusicVolumeSlider;
		[SerializeField]
		protected TextMeshProUGUI _SFXVolumeLabel;
		[SerializeField]
		protected TextMeshProUGUI _MusicVolumeLabel;

		public int SFXVolume {
			get { return Mathf.RoundToInt(_SFXVolumeSlider.value); }
			set { _SFXVolumeSlider.value = (float)value; }
		}

		public int MusicVolume {
			get { return Mathf.RoundToInt(_MusicVolumeSlider.value); }
			set { _MusicVolumeSlider.value = (float)value; }
		}

		#region uGUI
		public void OnSFXVolumeSlider(float level)
		{
			int rounded = Mathf.RoundToInt(level);
			_SFXVolumeLabel.text = rounded.ToString();
			if (null != this.EventSFXVolumeChanged)
				EventSFXVolumeChanged.Invoke(rounded);
		}

		public void OnMusicVolumeSlider(float level)
		{
			int rounded = Mathf.RoundToInt(level);
			_MusicVolumeLabel.text = rounded.ToString();
			if (null != this.EventMusicVolumeChanged)
				EventMusicVolumeChanged.Invoke(rounded);
		}
		#endregion
	}
}