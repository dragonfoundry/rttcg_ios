﻿using UnityEngine;
using System;
using System.Collections;
using ExitGames.Client.Photon;
using ExitGames.Client.Photon.Chat;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using NovaBlitz.UI;
using LinqTools;

public enum PhotonMessageType
{
    NoType,
    DirectMessage,
    Challenge,
    DeclineChallenge,
    CancelChallenge
}
    
public class PlayFabChatClient : MonoBehaviour, IChatClientListener
{
    [Inject] public PlayerProfile PlayerProfile {get;set;}
    [Inject] public UpdateFriendStatusSignal UpdateFriendStatusSignal {get;set;}
    [Inject] public FriendChallengeSignal FriendChallengeSignal {get;set;}
    [Inject] public FriendChallengeDeclinedSignal FriendChallengeDeclinedSignal {get;set;}
    [Inject] public FriendChallengeCanceledSignal FriendChallengeCanceledSignal {get;set;} 
    [Inject] public AddChatMessageSignal AddChatMessageSignal{get;set;}
    [Inject] public AddGlobalChatMessagesSignal AddGlobalChatMessagesSignal { get;set;}
    [Inject] public GlobalChatMessageAddedSignal GlobalChatMessageAddedSignal { get; set; }
    [Inject] public MetagameSettings MetagameSettings { get; set; }
    [Inject] public LogCustomEventSignal LogCustomEventSignal {get;set;}
    [Inject] public ConnectedToGlobalChatSignal ConnectedToGlobalChatSignal { get; set; }
        [Inject] public SaveChatHistorySignal SaveChatHistorySignal { get; set; }
    public ChatClient chatClient;
    public bool IsChatClientConnected = false;
    public bool IsChatClientConnecting = false;

    private float chatReconnectTime = 2f;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
#if (UNITY_IOS || UNITY_ANDROID)
#else
        Application.runInBackground = true; // this must run in background or it will drop connection if not focussed.
#endif
    }
    /// <summary>To avoid that the Editor becomes unresponsive, disconnect all Photon connections in OnApplicationQuit.</summary>
    public void OnApplicationQuit()
    {
        if (this.chatClient != null)
        {
            this.chatClient.Disconnect();
			this.StopAllCoroutines();
        }
    }
    
    public void ConnectToPhoton(string appId)
    {
        if (string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.ID) || string.IsNullOrEmpty(appId) || string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.PhotonAuthToken))
            return;
        Debug.Log("Connecting to Photon with " + this.PlayerProfile.MetaProfile.ID);
        if (this.chatClient == null)
        {
            this.chatClient = new ChatClient(this);
        }

        this.IsChatClientConnecting = true;

        AuthenticationValues authVals = new AuthenticationValues(this.PlayerProfile.MetaProfile.ID);
        authVals.AuthType = CustomAuthenticationType.Custom;
        authVals.AddAuthParameter("username", this.PlayerProfile.MetaProfile.ID);
        authVals.AddAuthParameter("token", this.PlayerProfile.MetaProfile.PhotonAuthToken);

        this.chatClient.AuthValues = authVals;
        this.chatClient.ChatRegion = "US";
        this.chatClient.Connect(appId, "1.0", authVals);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.chatClient != null)
        {
            this.chatClient.Service();  // make sure to call this regularly! it limits effort internally, so calling often is ok!
        }
    }
    
    public void DebugReturn(DebugLevel level, string message)
    {
        Debug.Log("Photon DebugReturn: " + message);
    }
    
    public void OnConnected()
    {
        chatReconnectTime = 2f;
        Debug.Log("Connected to PhotonChat!", this);  
		if (Application.isPlaying)
		{
            ChatFriendsList.Clear();
            this.StartCoroutine (LoadFriendStatus ());
		}
        this.IsChatClientConnected = true;
        this.IsChatClientConnecting = false;
    }

    private IEnumerator waitForConntectionCoroutine;
    public void WaitForConnection()
    {
        if(waitForConntectionCoroutine == null)
        {
            waitForConntectionCoroutine = WaitForChatConnection();
            Timer.Instance.StartCoroutine(waitForConntectionCoroutine);
        }
    }

    private IEnumerator WaitForChatConnection()
    {
        while (!this.IsChatClientConnected)
        {
            yield return new WaitForSecondsRealtime(0.1f);
        }
        ChatChannel channel;
        if (!chatClient.TryGetChannel(this.PlayerProfile.GlobalChatRoom, false, out channel))
        {
            chatClient.Unsubscribe(chatClient.PublicChannels.Keys.ToArray());
            chatClient.Subscribe(new string[] { this.PlayerProfile.GlobalChatRoom }, -1);
        }
        else
            this.GlobalChatMessageAddedSignal.Dispatch(this.PlayerProfile.GlobalChatLog,false);
        waitForConntectionCoroutine = null;
        this.PlayerProfile.isInitializingGlobalChat = Math.Max(this.PlayerProfile.isInitializingGlobalChat, 0);
    }

    public void UpdatePhotonFriends()
    {
        this.StartCoroutine(LoadFriendStatus());
    }

    private List<string> ChatFriendsList = new List<string>();

    IEnumerator LoadFriendStatus()
    {
        while(this.PlayerProfile.IsFriendsListInitialized == false) 
        {
            yield return null;
        }
        
        //yield return null;
        yield return null;

        var friendList = this.PlayerProfile.FriendListData.Keys.ToList();
        var toAdd = friendList.Where(f => !ChatFriendsList.Contains(f)).ToArray();
        var toRemove = ChatFriendsList.Where(f => !friendList.Contains(f)).ToArray();
        ChatFriendsList = friendList;
        if (this.chatClient != null)
        {
            if (toRemove.Length > 0)
            {
                this.chatClient.RemoveFriends(toRemove);
                string removeString = string.Join(",", toRemove);
                Debug.LogFormat("removing friends from photon - {0}", removeString);
            }
            if (toAdd.Length > 0)
            {
                this.chatClient.AddFriends(toAdd);                    // Add some users to the server-list to get their status updates
                string friendString = string.Join(",", toAdd);
                Debug.LogFormat("adding friends to photon - {0}", friendString);
            }
            this.chatClient.SetOnlineStatus(ChatUserStatus.Online); // You can set your online state (without a mesage).
        }
    }
    
    public void OnDisconnected()
    {
        Debug.LogWarningFormat("PhotonChat disconnected: {0}", this.chatClient.DisconnectedCause);

        this.SaveChatHistorySignal.Dispatch();
        this.PlayerProfile.GlobalChatLog.Clear();
        this.IsChatClientConnected = false;
        this.IsChatClientConnecting = false;
        if (Application.isPlaying) 
		{
			StartCoroutine (DelayedReconnect ());
		}
    }

	IEnumerator DelayedReconnect()
	{
		yield return new WaitForSecondsRealtime(chatReconnectTime);
        //Dictionary<string,object> bodyParams = new Dictionary<string,object>();
        //bodyParams["cause"] = this.chatClient.DisconnectedCause.ToString();
        //this.LogCustomEventSignal.Dispatch("PhotonChat_DC", bodyParams);

        // Only reconnect if we have a metaprofile id AND a photon chat app ID - otherwise we're logged out.
        if (!this.IsChatClientConnected && !string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.ID) && !string.IsNullOrEmpty(this.MetagameSettings.PhotonChatAppID))
        {
            chatReconnectTime += 2f;
            this.ConnectToPhoton(this.MetagameSettings.PhotonChatAppID);
        }
	}

    public void OnChatStateChange(ChatState state)
    {
        // use OnConnected() and OnDisconnected()
        // this method might become more useful in the future, when more complex states are being used.
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        //Debug.LogFormat("Subscribed. {0}", Time.time);
        for(int i = 0; i<channels.Length; i++)
        {
            if (i < results.Length && results[i])
            {
                //Debug.LogFormat("Subscribed to {0}", channels[i]);
                if (channels[i] == this.PlayerProfile.GlobalChatRoom)
                    this.ConnectedToGlobalChatSignal.Dispatch();
            }
            else
                Debug.LogFormat("NOT Subscribed to {0}", channels[i]);
        }
    }
    
    public void OnUnsubscribed(string[] channels)
    {
        for (int i = 0; i < channels.Length; i++)
        {
            //Debug.LogFormat("Unsubscribed from {0}", channels[i]);
            if (channels[i].StartsWith("global"))
                this.PlayerProfile.GlobalChatLog.Clear();
        }
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        if (channelName != this.PlayerProfile.GlobalChatRoom)
            return;
        //Debug.LogFormat("GetMessages. {0}", Time.time);
        List<ChatMessage> chatMessages = new List<ChatMessage>();
        for(int i = 0; i<messages.Length; i++)
        {
            var data = messages[i] as string[];
            if(data != null && data.Length == 5)
            {
                if (this.PlayerProfile.isInitializingGlobalChat < 1 || this.PlayerProfile.MetaProfile.PlayFabID != data[0])
                {
                    var msg = new ChatMessage(data[0], data[1], data[2], data[3], this.PlayerProfile.MetaProfile.PlayFabID == data[0]);
                    msg.TimeStampString = data[4];
                    chatMessages.Add(msg);
                }
            }
            else
            {
                Debug.LogFormat("Global chat error. Message is {0}", data == null ? "null" : "length:" + data.Length);
            }
        }
        this.AddGlobalChatMessagesSignal.Dispatch(chatMessages);
        //if (isInitializingGlobalChat)
        //    this.PlayerProfile.isInitializingGlobalChat = false;
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        // Early out, ignore all messages from a blocked user, and from non-friends.
        if (this.PlayerProfile.BlockList.Contains(sender) || !this.PlayerProfile.FriendListData.ContainsKey(sender))
            return;

        // as the ChatClient is buffering the messages for you, this GUI doesn't need to do anything here
        // you also get messages that you sent yourself. in that case, the channelName is determinded by the target of your msg

        string[] messageData = (string[])message;
        if (messageData != null && messageData.Length > 0 && sender != this.PlayerProfile.MetaProfile.ID)
        {
            //string[] messageData = (string[])message;
            PhotonMessageType messageType = (PhotonMessageType)PhotonMessageType.Parse(typeof(PhotonMessageType), messageData[0]);
            //Debug.LogWarning("Got Photon msg: " + messageData[0]);
            
            // Dispatch incomming  Matchmaking messagges from other clients
            switch(messageType)
            {
                case PhotonMessageType.DirectMessage:
                    // Only dispatch events for remote players, we'll add our messages locally
                    if(messageData.Length == 5) // 0=type, 1=username, 2=avatar 3=message, 4=timestamp
                    {
                        var msg = new ChatMessage(sender, messageData[1], messageData[2], messageData[3], false);
                        msg.TimeStampString = messageData[4];
                        this.AddChatMessageSignal.Dispatch(sender, msg);
                    }
                    break;
                case PhotonMessageType.Challenge:
                    this.FriendChallengeSignal.Dispatch(FriendChallenge.Deserialize(messageData));
                break;
                case PhotonMessageType.DeclineChallenge:
                    Debug.LogWarningFormat("Friend challenge declined received from {0}", sender);
                    this.FriendChallengeDeclinedSignal.Dispatch(sender);
                break;
                case PhotonMessageType.CancelChallenge:
                    Debug.LogWarningFormat("Friend Challenge cancel received from {0}", sender);
                    this.FriendChallengeCanceledSignal.Dispatch(sender);
                break;
            }
        }
    }
    
    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        //Debug.Log("OnStatusUpdate: " + user + ", Status:" + status + " gotMessage: " + gotMessage + " msg:" + message);
        this.UpdateFriendStatusSignal.Dispatch(user, status);
    }
}
