﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class NovaButton : Button 
{
	public TextMeshProUGUI LabelText;
	public Image Image;
	[SerializeField] public ButtonClickedEvent OnClick;
	
	override protected void Awake()
	{
		base.Awake();
		this.transition = Transition.SpriteSwap;
	}
	
	/// <summary>
	/// Overrides the base is Hilighted button behavior and makes it so a button 
	/// is NOT hilighted if the touch count is zero (when running on IOS)
	/// </summary>
	protected bool IsHilighted(UnityEngine.EventSystems.BaseEventData eventData)
	{
		Debug.Log("Is hilighted called");
		bool isHilighted = base.IsHighlighted(eventData);
#if UNITY_IOS || UNITY_ANDROID
			isHilighted = isHilighted && Input.touchCount > 0;
#endif

        return isHilighted;
	}
	
	override protected void DoStateTransition(SelectionState state, bool instant)
	{
		base.DoStateTransition(state,instant);
		
		switch(state)
		{
			case SelectionState.Normal:
				if(this.LabelText != null) this.LabelText.color = this.colors.normalColor;
				if(this.Image != null) this.Image.color = this.colors.normalColor;
			break;
			case SelectionState.Pressed:
				if(this.LabelText != null) this.LabelText.color = this.colors.pressedColor;
				if(this.Image != null) this.Image.color = this.colors.pressedColor;
			break;
			case SelectionState.Highlighted:
				if(this.LabelText != null) this.LabelText.color = this.colors.highlightedColor;
				if(this.Image != null) this.Image.color = this.colors.highlightedColor;
			break;
			case SelectionState.Disabled:
				if(this.LabelText != null) this.LabelText.color = this.colors.disabledColor;
				if(this.Image != null) this.Image.color = this.colors.disabledColor;
			break;
		}
	}
	
	void Update()
	{
		bool isInputPresent = false;

#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			isInputPresent = Input.touchCount > 0;
#endif

#if UNITY_EDITOR || UNITY_STANDALONE
        isInputPresent = Input.GetMouseButton(0);
		#endif

		if( this.currentSelectionState == SelectionState.Highlighted && isInputPresent == false )
		{
			if(this.interactable)
			{
				this.DoStateTransition(SelectionState.Normal, true);
			}
			else
			{
				this.DoStateTransition(SelectionState.Disabled, true);
			}
		}
	}
}
