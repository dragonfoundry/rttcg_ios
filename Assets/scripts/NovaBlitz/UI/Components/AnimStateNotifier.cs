using UnityEngine;
using System;

namespace NovaBlitz.UI
{
	public class AnimStateNotifier : StateMachineBehaviour
	{
		public AnimStateTag Tag;

		public event Action<AnimStateNotification> EventEnter;
		public event Action<AnimStateNotification> EventExit;

		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (null != this.EventEnter)
				this.EventEnter.Invoke(new AnimStateNotification{
					Tag = this.Tag,
					Animator = animator,
					StateInfo = stateInfo,
					LayerIndex = layerIndex
				});
		}

		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (null != this.EventExit)
				this.EventExit.Invoke(new AnimStateNotification{
					Tag = this.Tag,
					Animator = animator,
					StateInfo = stateInfo,
					LayerIndex = layerIndex
				});
		}
	}

	public enum AnimStateTag
	{
		Default,
		Close,
		Open
	}

	public class AnimStateNotification
	{
		public AnimStateTag Tag;
		public Animator Animator;
		public AnimatorStateInfo StateInfo;
		public int LayerIndex;
	}
}
