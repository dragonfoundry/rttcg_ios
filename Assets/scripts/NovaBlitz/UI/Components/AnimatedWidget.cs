﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
	public class AnimatedWidget : AnimatedWidgetBase
	{
		public Animator Animator {get; protected set;}
		public override bool IsOpen {
			get { return this.Animator.GetBool("IsOpen"); }
			set { this.Animator.SetBool("IsOpen", value); }
		}

		protected virtual void Awake()
		{
			this.Animator = GetComponent<Animator>();
			var notifiers = this.Animator.GetBehaviours<AnimClipEndNotifier>();
			foreach (var n in notifiers) {
				if (n.Tag == AnimStateTag.Open)
					n.EventClipEnd += OnAnimOpen;
				if (n.Tag == AnimStateTag.Close)
					n.EventClipEnd +=OnAnimClose;
			}
		}

		protected virtual void OnAnimOpen(AnimStateNotification asn) { NotifyAnimOpen(); }
		protected virtual void OnAnimClose(AnimStateNotification asn) { NotifyAnimClose(); }
	}
}