﻿using UnityEngine;

namespace NovaBlitz.UI
{
	public class PlatformScaleCondition : DynamicScaleCondition
	{
		public RuntimePlatform[] Platforms;

		public override bool Evaluate()
		{
			foreach (var p in this.Platforms) {
				if (Application.platform == p)
					return true;
			} return false;
		}
	}
}
