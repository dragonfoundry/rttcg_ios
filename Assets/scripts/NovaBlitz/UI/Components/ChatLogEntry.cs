﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Crosstales.BadWord;

namespace NovaBlitz.UI
{

    public class ChatLogEntry : PoolableMonoBehaviour
    {
        public const string YOU_MESSAGE = "<size=-12><b><uppercase><#07FCF8>{0}</color></uppercase></b><#808080> - {2}{3}:{4}{5}</color></size>\n<#dddddd>{1}</color>";
        public const string OTHER_MESSAGE = "<size=-12><#808080>{2}{3}:{4}{5} - </color><b><uppercase><#07FCF8>{0}:</color></uppercase></b></size>\n<#181818>{1}</color>";
        public const string YOU_SHORT_MESSAGE = "<#dddddd>{0}</color>";
        public const string OTHER_SHORT_MESSAGE = "<#181818>{0}</color>";
        private static Color YOU_COLOR = new Color(0.11f, 0.42f, 0.47f);
        private static Color OTHER_COLOR = new Color(0.75f, 0.75f, 0.75f);

        public System.Action<ChatLogEntry> ElementClicked;
        public TextMeshProUGUI ChatMessageText;
        public CardArtImage AvatarArt;
        public RectTransform ArtMask;
        public Image Background;
        public Image LeftTriangle;
        public Image RightTriangle;
        public VerticalLayoutGroup VerticalLayout;
        public LayoutElement LayoutElement;

        public override PooledObjectType PooledObjectType { get { return PooledObjectType.GlobalChatItem; } }
        public GlobalChatView GlobalChatView { get; set; }

        public uint grouping = 0;

        private ChatMessage _ChatMessage;

        public ChatLogEntry()
        {
        }

        public void SetChatMessage(ChatMessage message, ChatLogEntry lastEntry)
        {
            _ChatMessage = message;
            if (message != null)
            {
                bool isSameAsLast = false;
                // bool isSameAsLast = lastEntry != null && lastEntry.ChatMessage != null
                    //&& message.SenderPlayFabId == lastEntry.ChatMessage.SenderPlayFabId
                    //&& message.TimeSeconds <= lastEntry.ChatMessage.TimeSeconds + 120;

                if (message.IsFromYou)
                {
                    Background.color = YOU_COLOR;
                    var time = message.SentTimeLocal;
                    ChatMessageText.text = isSameAsLast 
                        ? string.Format(YOU_SHORT_MESSAGE, BWFManager.ReplaceAll(message.Message)) 
                        : string.Format(YOU_MESSAGE, BWFManager.ReplaceAll(message.SenderDisplayName), BWFManager.ReplaceAll(message.Message), time.Hour < 10 ? "0" : string.Empty, time.Hour, time.Minute < 10 ? "0" : "", time.Minute);
                    ChatMessageText.alignment = TextAlignmentOptions.Right;
                    var rect = this.Background.transform as RectTransform;
                    VerticalLayout.padding.left = 105; // 205 - 110
                    VerticalLayout.padding.right = 127;
                    ArtMask.anchorMin = ArtMask.anchorMax = new Vector2(1.0f, 0.0f);
                    ArtMask.anchoredPosition = new Vector2(82, 0);
                    rect.offsetMin = new Vector2(200, 0);
                    rect.offsetMax = new Vector2(-130, isSameAsLast ? 0 : -32);
                    //ChatMessageText.margin = GlobalChatView == null ? Vector4.zero : new Vector4(0, 0, (this.GlobalChatView.ChatHolder.transform as RectTransform).rect.width * 0.2f, 0);
                    //AvatarArt.gameObject.SetActive(false);
                }
                else
                {
                    Background.color = OTHER_COLOR;
                    var time = message.SentTimeLocal;
                    ChatMessageText.text = isSameAsLast
                        ? string.Format(OTHER_SHORT_MESSAGE, BWFManager.ReplaceAll(message.Message))
                        : string.Format(OTHER_MESSAGE, BWFManager.ReplaceAll(message.SenderDisplayName), BWFManager.ReplaceAll(message.Message), time.Hour < 10 ? "0" : string.Empty, time.Hour, time.Minute < 10 ? "0" : "", time.Minute);
                    ChatMessageText.alignment = TextAlignmentOptions.Left;
                    VerticalLayout.padding.left = 37;
                    VerticalLayout.padding.right = 205;
                    ArtMask.anchorMin = ArtMask.anchorMax = new Vector2(0.0f, 0.0f);
                    ArtMask.anchoredPosition = new Vector2(8, 0);
                    var rect = this.Background.transform as RectTransform;
                    rect.offsetMin = new Vector2(130, 0);
                    rect.offsetMax = new Vector2(-200f, isSameAsLast ? 0 : -32);
                    //ChatMessageText.margin = GlobalChatView == null ? Vector4.zero : new Vector4((this.GlobalChatView.ChatHolder.transform as RectTransform).rect.width * 0.2f, 0, 0, 0);
                }

                // not same as last - have the timestamp at the top. force min 65.
                // same as last - turn off arrow on the above one.
                AvatarArt.UpdateCardArt(message.AvatarArtId, CardAspect.NoAspect);
                ArtMask.gameObject.SetActive(true);

                if (message.IsFromYou)
                {
                    LeftTriangle.gameObject.SetActive(false);
                    RightTriangle.gameObject.SetActive(true);
                    RightTriangle.color = YOU_COLOR;
                }
                else
                {
                    LeftTriangle.gameObject.SetActive(true);
                    RightTriangle.gameObject.SetActive(false);
                    LeftTriangle.color = OTHER_COLOR;
                }
                LayoutElement.minHeight = 35f;
                if (lastEntry != null)
                {
                    if (isSameAsLast)
                    {
                        lastEntry.LeftTriangle.gameObject.SetActive(false);
                        lastEntry.RightTriangle.gameObject.SetActive(false);
                        lastEntry.ArtMask.gameObject.SetActive(false);
                        grouping = lastEntry.grouping;
                    }
                    else
                        grouping = lastEntry.grouping + 1;
                }
                else
                    grouping = 0;

                if (!isSameAsLast)
                {
                    LayoutElement.minHeight = 65f;
                }

            }
            else
            {
                ChatMessageText.text = null;
                AvatarArt.texture = null;
            }
        }

        public ChatMessage ChatMessage
        {
            get { return _ChatMessage; }
        }

        public override void OnRecycle()
        {
            grouping = 0;
            ElementClicked = null;
            GlobalChatView = null;
            SetChatMessage(null, null);
            AvatarArt.ResetArt();
            // override to reset data values when pooled
        }

        public void OnChatLogClicked()
        {
            if (ElementClicked != null && ChatMessage != null && !ChatMessage.IsFromYou)
                ElementClicked.Invoke(this);
        }

    }
}
