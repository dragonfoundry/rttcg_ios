﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace NovaBlitz.UI
{
	public class RectNotifier : UIBehaviour, IRectTransform
	{
		public event Action EventRectChanged;
		public RectTransform RectTransform {get; protected set;}

		protected override void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
			base.Awake();
		}

		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			if (null != this.EventRectChanged) this.EventRectChanged.Invoke();
		}
	}
}
