﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using TMPro;

namespace NovaBlitz.UI
{
	public class CardEditingWidget : MonoBehaviour
	{
		public event Action EventAddClicked;
		public event Action EventRemoveClicked;

		[SerializeField] protected TextMeshProUGUI _DeckTotalLabel;
		[SerializeField] protected TextMeshProUGUI _CollectionTotalLabel;
		[SerializeField] protected Button _AddButton;
		[SerializeField] protected Button _RemoveButton;

		public string DeckTotal 	{ set { _DeckTotalLabel.text = value; } }
		public int CollectionTotal 	{ set { _CollectionTotalLabel.text = string.Format("x{0}", value); } }
		public bool CanAdd 		{ set { _AddButton.interactable = value; } }
		public bool CanRemove 	{ set { _RemoveButton.interactable = value; } }

		#region uGUI
		public void ClickAdd() 		{ if (null != this.EventAddClicked) this.EventAddClicked.Invoke(); }
		public void ClickRemove() 	{ if (null != this.EventRemoveClicked) this.EventRemoveClicked.Invoke(); }
		#endregion
	}
}
