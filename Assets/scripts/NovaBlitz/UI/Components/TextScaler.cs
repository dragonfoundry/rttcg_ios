﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace NovaBlitz.UI
{
	public class TextScaler : DynamicScaler
	{
		public float ScaleFactor = 1.5f;
		[SerializeField] protected Text[] _TextsToScale;
		[SerializeField] protected TextMeshProUGUI[] _TMProsToScale;

		public override void Scale()
		{
			foreach (var tx in _TextsToScale)
				tx.fontSize = (int)(tx.fontSize * this.ScaleFactor);
			foreach (var tx in _TMProsToScale) {
				tx.fontSize *= this.ScaleFactor;
				tx.fontSizeMin *= this.ScaleFactor;
				tx.fontSizeMax *= this.ScaleFactor;
			}
		}
	}
}
