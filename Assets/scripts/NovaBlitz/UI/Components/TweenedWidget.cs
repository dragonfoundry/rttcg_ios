﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
	public class TweenedWidget : AnimatedWidgetBase
	{
		[SerializeField]
		protected TweenerComponent _OpenTweener;
		[SerializeField]
		protected TweenerComponent _CloseTweener;

		protected bool _IsOpen;
		public override bool IsOpen {
			get { return _IsOpen; }
			set { _IsOpen = value;
				if (value) {
					if (_CloseTweener.Tweener.IsPlaying)
						_CloseTweener.Tweener.Stop();
					_OpenTweener.Tweener.Play();
				}
				else {
					if (_OpenTweener.Tweener.IsPlaying)
						_OpenTweener.Tweener.Stop();
					_CloseTweener.Tweener.Play();
				}
			}
		}

		protected virtual void Awake()
		{
			_OpenTweener.Tweener.Stop();
			_OpenTweener.Tweener.EventTweenFinished += OnTweenOpen;
			_CloseTweener.Tweener.EventTweenFinished += OnTweenClose;
		}

		protected virtual void OnTweenOpen() { NotifyAnimOpen(); }
		protected virtual void OnTweenClose() { NotifyAnimClose(); }
	}

	public abstract class TweenerBase
	{
		public event Action EventTweenFinished;

		[HideInInspector]
		public MonoBehaviour Looper;
		public float Duration = 1f;
		public float Speed = 1f;
		public float NormalizedTime {get; protected set;}
		public bool IsPlaying {get; protected set;}

		protected IEnumerator _Routine;
		protected IEnumerator CreateRoutine()
		{
			NormalizedTime = 0f;
			if (Duration > 0f) {
				while (NormalizedTime < 1f) {
					if (IsPlaying) {
						NormalizedTime += Speed / Duration * Time.deltaTime;
						NormalizedTime = Mathf.Min(NormalizedTime, 1f);
						Evaluate(NormalizedTime);
					}
					yield return null;
				}
			}
			IsPlaying = false;
			Finish();
		}

		protected virtual void Evaluate(float normTime) {}
		protected virtual void Finish() { _Routine = null; if (null != EventTweenFinished) EventTweenFinished.Invoke(); }

		public void Play() { IsPlaying = true; Looper.StartCoroutine(_Routine ?? (_Routine = CreateRoutine())); }
		public void Pause() { IsPlaying = false; if (null != _Routine) Looper.StopCoroutine(_Routine); }
		public void Stop() { Pause(); _Routine = null; NormalizedTime = 0f; Evaluate(0f); }
		public void Skip() { Pause(); _Routine = null; NormalizedTime = 1f; Evaluate(1f); Finish(); }
	}

	public abstract class TweenerComponent : MonoBehaviour
	{
		public abstract TweenerBase Tweener {get;}

		protected virtual void Awake() { this.Tweener.Looper = this; }
	}
}
