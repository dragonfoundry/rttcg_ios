﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
	public interface IAnimatedUI : IViewOpen
	{
		event Action EventAnimOpen;
		event Action EventAnimClose;
	}

	public abstract class AnimatedWidgetBase : MonoBehaviour, IAnimatedUI
	{
		public event Action EventAnimOpen;
		public event Action EventAnimClose;

		public abstract bool IsOpen {get;set;}

		protected void NotifyAnimOpen() { if (null != this.EventAnimOpen) this.EventAnimOpen.Invoke(); }
		protected void NotifyAnimClose() { if (null != this.EventAnimClose) this.EventAnimClose.Invoke(); }
	}

	public class UIAnimSequencer
	{
		public event Action EventSequenceNext;
		public event Action EventSequenceFinish;

		protected Queue<IAnimatedUI> _Sequence = new Queue<IAnimatedUI>();
		protected IAnimatedUI _Current;

		public float FallbackTimeout = 0.55f;
		public int Count { get { return _Sequence.Count; } }

		public void Enqueue(IAnimatedUI anim) { _Sequence.Enqueue(anim); }
		public void Load(IEnumerable<IAnimatedUI> anims)
		{
			_Sequence.Clear();
			foreach (var anim in anims)
				Enqueue(anim);
		}

		public void Next()
		{
			var curr = _Current;
			// if queue is not empty...
			if (_Sequence.Count > 0) {
				var next = _Sequence.Dequeue();
				Action openNext = ()=> {
					Timer.Instance.StartCoroutine(EngineUtils.ActionOnceTimeout(FallbackTimeout, OnNextOpened, a => next.EventAnimOpen += a, a => next.EventAnimOpen -= a));
					next.IsOpen = true;
					_Current = next;
				};
				// wait for current to close before opening next
				if (null != curr) {
					Timer.Instance.StartCoroutine(EngineUtils.ActionOnceTimeout(FallbackTimeout, openNext, a => curr.EventAnimClose += a, a => curr.EventAnimClose -= a));
					curr.IsOpen = false;
				} else openNext.Invoke();
			}
			else {
				_Current = null;
				if (null != curr) {
					// otherwise just close current and finish
					Timer.Instance.StartCoroutine(EngineUtils.ActionOnceTimeout(FallbackTimeout, OnFinalClosed, a => curr.EventAnimClose += a, a => curr.EventAnimClose -= a));
					curr.IsOpen = false;
				} else OnFinalClosed();
			}
		}

		protected void OnNextOpened() { if (null != this.EventSequenceNext) this.EventSequenceNext.Invoke(); }
		protected void OnFinalClosed() { if (null != this.EventSequenceFinish) this.EventSequenceFinish.Invoke(); }
	}

	/// <summary>
	/// Sequencer interaction via uGUI button. Button is only available while not transitioning.
	/// </summary>
	[Serializable]
	public class UIAnimSequencerModule
	{
		public event Action EventNextClicked;

		[SerializeField]
		protected Button _NextButton;
		protected UIAnimSequencer _Sequencer = new UIAnimSequencer();
		public UIAnimSequencer Sequencer { get { return _Sequencer; } }

		public void Begin()
		{
			_NextButton.onClick.AddListener(ClickNext);
			EngineUtils.ActionOnce(OnSequenceFinish, a => _Sequencer.EventSequenceFinish += a, a => _Sequencer.EventSequenceFinish -= a);
			EventNextClicked += OnClickNext;
			ClickNext();
		}

		#region uGUI
		public void ClickNext() { if (null != EventNextClicked) EventNextClicked.Invoke(); }
		#endregion

		#region Async
		protected void OnClickNext()
		{
			_NextButton.interactable = false;
			if (_Sequencer.Count > 0)
				EngineUtils.ActionOnce(OnSequenceNext, a => _Sequencer.EventSequenceNext += a, a => _Sequencer.EventSequenceNext -= a);
			_Sequencer.Next();
		}

		protected void OnSequenceNext()
		{
			_NextButton.interactable = true;
		}

		protected void OnSequenceFinish()
		{
			// Next button might be null if someone else destroyed the window -DMac
			if(_NextButton != null)
			{
				_NextButton.interactable = true;
				_NextButton.onClick.RemoveListener(ClickNext);
				EventNextClicked -= OnClickNext;
			}
		}
		#endregion
	}

	[Serializable]
	public class MultiViewSequencer<T, TData> where T : Component
	{
		[SerializeField]
		protected T _ViewPrefab;
		[SerializeField]
		protected RectTransform _Anchor;
		public UIAnimSequencerModule SequencerModule;

		protected T[] _Views;
		public IEnumerable<T> Instances { get { return _Views; } }

		public IEnumerable<T> Populate(IEnumerable<TData> data)
		{
			_Views = data.Select(d => {
				var view = UITools.SpawnRect(_ViewPrefab, _Anchor);
				Initialize(view, d);
				return view;
			}).ToArray();
			SequencerModule.Sequencer.Load(_Views.Select(v => v.GetComponent<IAnimatedUI>()));
			return this.Instances;
		}

		public void Purge()
		{
			if (null != _Views) {
				foreach (var view in _Views)
					GameObject.Destroy(view.gameObject);
				_Views = null;
			}
		}

		protected virtual void Initialize(T view, TData data) {}
	}
}
