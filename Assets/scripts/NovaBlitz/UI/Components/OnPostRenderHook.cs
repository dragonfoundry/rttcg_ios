﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NovaBlitz
{
    public class OnPostRenderHook : MonoBehaviour
    {
		public System.Action PostRenderCallback;

		void OnPostRender()
		{
			if(this.PostRenderCallback != null)
				this.PostRenderCallback.Invoke();
		}
    }
}