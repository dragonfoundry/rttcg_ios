﻿using UnityEngine;

namespace NovaBlitz.UI
{
	public class DPIScaleCondition : DynamicScaleCondition
	{
		public float SmallScreenThreshold = 7.5f;
		public bool IsSmallScreen {get; protected set;}

		public override bool Evaluate()
		{
			Vector2 screenInches = UITools.GetScreenSizeInches();
			this.IsSmallScreen = screenInches.magnitude < this.SmallScreenThreshold;
			return this.IsSmallScreen;
		}
	}
}
