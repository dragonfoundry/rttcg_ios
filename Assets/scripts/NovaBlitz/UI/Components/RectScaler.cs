﻿using UnityEngine;
using UnityEngine.UI;

namespace NovaBlitz.UI
{
	public class RectScaler : DynamicScaler
	{
		public Vector3 ScaleFactor = Vector3.one * 1.5f;
		[SerializeField] protected RectTransform[] _RectsToScale;
		[SerializeField] protected RectTransform[] _RectsToExpand;

		public override void Scale()
		{
			foreach (var rx in _RectsToScale){
				Vector3 ls = rx.localScale;
				ls.x *= this.ScaleFactor.x;
				ls.y *= this.ScaleFactor.y;
				ls.z *= this.ScaleFactor.z;
				rx.localScale = ls;
			}
			foreach (var rx in _RectsToExpand) {
				Vector2 sd = rx.sizeDelta;
				if (Mathf.Approximately(rx.anchorMin.x, rx.anchorMax.x))
					sd.x *= this.ScaleFactor.x;
				if (Mathf.Approximately(rx.anchorMin.y, rx.anchorMax.y))
					sd.y *= this.ScaleFactor.y;
				rx.sizeDelta = sd;
			}
		}
	}
}
