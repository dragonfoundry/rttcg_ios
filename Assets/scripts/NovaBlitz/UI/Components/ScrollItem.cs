using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Put this on an object within a ScrollRect to forward all drag events. 
	/// </summary>
	public class ScrollItem : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
	{
		protected Transform _Self;
		protected Transform _LastParent;
		protected ScrollRect _Scroll;

		void Start() { _Self = transform; FindScroll(); }
		void FindScroll() { _LastParent = _Self.parent; _Scroll = _Self.GetComponentInParent<ScrollRect>(); }

		public void OnDrag(PointerEventData eventData) { _Scroll.OnDrag(eventData); }
		public void OnBeginDrag(PointerEventData eventData) { if (_Self.parent != _LastParent) FindScroll(); _Scroll.OnBeginDrag(eventData); }
		public void OnEndDrag(PointerEventData eventData) { _Scroll.OnEndDrag(eventData); }
	}
}
