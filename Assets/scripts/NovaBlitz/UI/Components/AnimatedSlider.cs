using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;

public class AnimatedSlider : MonoBehaviour
{
	public Slider Slider;
	public TextMeshProUGUI DigitalDisplay;
	public float FillDuration;

    public bool isShortDigitalDisplay = false;
	public int Empty;
	public int Full;
	public int Progress {
		set {
			this.Slider.value = Mathf.InverseLerp(this.Empty, this.Full, value);
			if (null != DigitalDisplay) {
				DigitalDisplay.text = (Full == 0 || isShortDigitalDisplay) ? Math.Abs(value).ToString() : string.Format("{0}/<size=-5>{1}</size>", Math.Abs(value), Math.Abs(Full));
            }
		}
	}

	public event Action EventAnimComplete;

	public void Animate(int start, int end)
	{
		float duration = Math.Max(0.1f, this.FillDuration * Mathf.Abs(end-start) / Mathf.Abs(this.Full-this.Empty));
		StartCoroutine(AnimSequence(start, end, duration));
	}
	
	protected IEnumerator AnimSequence(int start, int end, float duration)
	{
            yield return StartCoroutine(EngineUtils.Tween(duration, t =>
            {
                int lerp = (int)Mathf.Lerp(start, end, t);
                this.Progress = lerp;
            }));
        
		if (null != EventAnimComplete)
			EventAnimComplete.Invoke();
	}
}
