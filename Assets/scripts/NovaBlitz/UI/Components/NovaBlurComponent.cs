﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using UnityStandardAssets.ImageEffects;
using DG.Tweening;

public class NovaBlurComponent : MonoBehaviour 
{
	public BlurOptimized BlurComponent;
	public ColorCorrectionCurves ColorCorrect;

	public bool IsAlreadyBlurred {get { return this.isAlreadyBlurred;}}
	
	private bool isAlreadyBlurred = false;
	private float startingBlurSize;

	public void Awake()
	{
		this.startingBlurSize = this.BlurComponent.blurSize;
	}

	public void BlurMainCanvas(bool isBlurred, View currentView)
	{
		//Debug.Log("BlurMainConvas: " + isBlurred + " currentView:" + currentView);
		float targetSaturation = 0.5f;
		
		if(isBlurred && this.isAlreadyBlurred == false)
		{
			this.BlurComponent.enabled = true;
			ColorCorrect.enabled = true;
			this.isAlreadyBlurred = true;
			DOTween.Kill("fade1");
			DOTween.Kill("fade2");

			ColorCorrect.saturation = 1.0f;
			BlurComponent.blurSize = this.startingBlurSize;
			DOTween.To(()=>ColorCorrect.saturation, x=>ColorCorrect.saturation=x, targetSaturation, 0.5f).SetEase(Ease.OutCubic);
			DOTween.To(()=>BlurComponent.blurSize,y=>BlurComponent.blurSize=y,0f,0.5f).SetEase(Ease.OutExpo).From();
		}
		else if(isBlurred == false && this.isAlreadyBlurred == true)
		{
			this.isAlreadyBlurred = false;
            DOTween.Kill("fade1");
            DOTween.Kill("fade2");
            DOTween.To(()=>ColorCorrect.saturation, x=>ColorCorrect.saturation=x, 1.0f, 0.3f).SetEase(Ease.OutCubic).SetId("fade1");
			DOTween.To(()=>BlurComponent.blurSize,y=>BlurComponent.blurSize=y,0f,0.1f)
				.SetId("fade2")
				.SetEase(Ease.OutCubic)
				.OnComplete(()=>{
					BlurComponent.enabled = false;
					ColorCorrect.enabled = false;
					BlurComponent.blurSize = this.startingBlurSize;
				});
		}
	}
}
