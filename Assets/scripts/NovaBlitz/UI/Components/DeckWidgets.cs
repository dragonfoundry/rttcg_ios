﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using TMPro;

namespace NovaBlitz.UI
{
	[Serializable]
	public class DeckMiniWidget
	{
		[SerializeField]
		protected TextMeshProUGUI _NameDisplay;
		[SerializeField]
		protected CardAspectsIndicator _AspectsIndicator;

		public string DeckName { get { return _NameDisplay.text; } set { if (null != _NameDisplay) _NameDisplay.text = value; } }
		public IEnumerable<CardAspect> Aspects { set { _AspectsIndicator.ShowIcons(value); } }
	}

	[Serializable]
	public class DeckRenameWidget
	{
		[SerializeField]
		protected TMP_InputField _NameInput;
		[SerializeField]
		protected NovaButton _EditButton;
		[SerializeField]
		protected NovaButton _ConfirmButton;
		//[SerializeField]
		//protected CanvasGroupToggle _CGToggle;
		public bool StayHidden;

		public string CachedName {get;set;}

		protected bool _IsRenaming;
		public bool IsRenaming {
			get { return _IsRenaming; }
			set { _IsRenaming = value; ToggleRename(value); }
		}
        private bool _IsEditable;
        public bool IsEditable
        {
            get { return _IsEditable; }
            set { _IsEditable = value; /*_EditButton.gameObject.SetActive(value);*/ _NameInput.interactable = value; }
        }


        public string FieldText {
			get { return _NameInput.text; }
			set { _NameInput.text = value != null ? value : string.Empty; }
		}

		/*public bool FieldVisible {
			set { _CGToggle.IsOn = value; }
			get { return _CGToggle.IsOn; }
		}*/

		public event Action<string> EventConfirmed;

		public void Init()
		{
			_NameInput.onEndEdit.AddListener(OnInput_EndEdit);
			//_EditButton.onClick.AddListener(OnEditClicked);
			//_ConfirmButton.onClick.AddListener(OnConfirmClicked);
		}

		protected void ToggleRename(bool isEditing)
		{
			if (isEditing) {
				this.FieldText = CachedName;
				//this.FieldVisible = true;
				_NameInput.ActivateInputField();
                _NameInput.Select();
            }
			else {
				_NameInput.DeactivateInputField();
				//if (this.StayHidden)
				//	this.FieldVisible = false;
			}
		}

		protected void OnInput_EndEdit(string name)
		{
			//TODO How to enforce confirm / cancel?
			OnConfirmClicked();
		}

		protected void OnEditClicked()
		{
            if (!this.IsRenaming)
                this.IsRenaming = true;
		}

		protected void OnConfirmClicked()
		{
			if (null != this.EventConfirmed)
				this.EventConfirmed.Invoke(this.FieldText.Trim());
			this.IsRenaming = false;
		}
	}

	[Serializable]
	public class DeckEnergyGraphWidget
	{
		[SerializeField]
		protected AnimatedSlider[] _PowerBars;
        [SerializeField]
        protected AnimatedSlider[] _UnitBars;

        public DeckData DeckData {get;set;}
        
		public void Refresh()
        {
            //set up power bars to full height, unit bars to just units.
            foreach (var bar in _PowerBars)
            {
                bar.isShortDigitalDisplay = true;
            }
            foreach (var bar in _UnitBars)
            {
                bar.isShortDigitalDisplay = true;
            }
            if (null == this.DeckData || !this.DeckData.Cards.Any())
            {
				foreach (var bar in _PowerBars)
                {
                    bar.Full = 1;
					bar.Progress = 0;
                }
                foreach (var bar in _UnitBars)
                {
                    bar.Full = 1;
                    bar.Progress = 0;
                }
                return;
			}

            var powerStats = this.DeckData.Cards.GroupBy(c => c.Cost).ToDictionary(g => g.Key, g => g.Count());
            var unitStats = this.DeckData.Cards.Where(c => c.CardType == CardType.Unit).GroupBy(c => c.Cost).ToDictionary(g => g.Key, g => g.Count());

            
            
			int maxCost = _PowerBars.Length-1; //first bar is zero

			var lowCost_power = powerStats.Where(kvp => kvp.Key < maxCost).ToDictionary(k => k.Key, v => v.Value);
            int highCost_power = 0;
            foreach (var kvp in powerStats)
            {
                if (kvp.Key >= maxCost)
                    highCost_power += kvp.Value;
            }
            var lowCost_unit = unitStats.Where(kvp => kvp.Key < maxCost).ToDictionary(k => k.Key, v => v.Value);
            int highCost_unit = 0;
            foreach (var kvp in unitStats)
            {
                if (kvp.Key >= maxCost)
                    highCost_unit += kvp.Value;
            }

            int maxQty = Math.Max(highCost_power, powerStats.Values.Max());
            foreach (var bar in _UnitBars) {
				bar.Full = maxQty;
				bar.Progress = 0;
            }
            foreach (var bar in _PowerBars)
            {
                bar.Full = maxQty;
                bar.Progress = 0;
            }
            
            foreach (var kvp in lowCost_power)
            {
                _PowerBars[kvp.Key].Progress = kvp.Value;
            }
            foreach (var kvp in lowCost_unit)
                _UnitBars[kvp.Key].Progress = kvp.Value;
            _UnitBars.Last().Progress = highCost_unit;
            _PowerBars.Last().Progress = highCost_power;


            for(int i = 0; i < _PowerBars.Count() - 1; i++)
            {
                int count;
                lowCost_power.TryGetValue(i, out count);
                _PowerBars[i].DigitalDisplay.text = count > 0 ? count.ToString() : String.Empty;
            }
            _PowerBars.Last().DigitalDisplay.text = highCost_power > 0 ? highCost_power.ToString() : String.Empty;
        }

        private void SetBars(AnimatedSlider[] unitBars, Dictionary<int,int> powerStats, Dictionary<int, int> unitStats)
        {
        }
	}
}
