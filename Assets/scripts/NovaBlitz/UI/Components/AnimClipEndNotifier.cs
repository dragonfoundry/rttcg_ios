using UnityEngine;
using System;

namespace NovaBlitz.UI
{
	public class AnimClipEndNotifier : AnimStateNotifier
	{
		[SerializeField]
		protected bool _FirstLoopOnly;
		protected bool _InFirstLoop;
		protected bool _InProgress;

		public event Action<AnimStateNotification> EventClipEnd;
		
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			_InFirstLoop = true;
			_InProgress = true;
			base.OnStateEnter(animator, stateInfo, layerIndex);
		}

		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			// This catches transitions with exit time 1 and duration 0.
			if (_InProgress && stateInfo.normalizedTime >= 1f) {
				_InProgress = false;
				if (null != this.EventClipEnd)
					this.EventClipEnd.Invoke(new AnimStateNotification{
						Tag = this.Tag,
						Animator = animator,
						StateInfo = stateInfo,
						LayerIndex = layerIndex
					});
			}
			base.OnStateExit(animator, stateInfo, layerIndex);
		}

		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if ((!_FirstLoopOnly || _InFirstLoop) && stateInfo.normalizedTime >= 1f) {
				_InFirstLoop = false;
				_InProgress = !_FirstLoopOnly;
				if (null != this.EventClipEnd)
					this.EventClipEnd.Invoke(new AnimStateNotification{
						Tag = this.Tag,
						Animator = animator,
						StateInfo = stateInfo,
						LayerIndex = layerIndex
					});
			}
		}
	}
}
