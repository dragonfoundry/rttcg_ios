﻿using UnityEngine;

namespace NovaBlitz.UI
{
	public abstract class DynamicScaler : MonoBehaviour
	{
		public DynamicScaleCondition Condition;

		public virtual void Scale() {}

		protected virtual void Start()
		{
			if (this.Condition.Evaluate())
				Scale();
		}
	}

	public abstract class DynamicScaleCondition : MonoBehaviour
	{
		public abstract bool Evaluate();
	}
}
