﻿using UnityEngine;
using System.Collections;
using strange.extensions.context.impl;
using strange.extensions.command.api;
using strange.extensions.command.impl;

using NovaBlitz.UI.Mediators;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class MainContext : MVCSContext 
	{
		private MainContextView mainContextView;
        private ClientService clientNetworking;
        private AssetBundleManager assetBundleManager;
        //private ChillingoService chillingo;
        private NovaIAPService novaIAP;
        private AssetManagerService assetManagerService;
        private CardListItemProxyService cardListItemProxyService;
        private PlayFabChatClient chatClient;
		private NovaSteamService novaSteamManager;
        private MetricsTrackingService metricsService;

        public MainContextView MainContextView { get {return mainContextView;} }

        public MainContext() : base()
		{
		}
		
		public MainContext(MonoBehaviour view, bool autoStartup) : base(view, autoStartup)
		{
			this.mainContextView = (MainContextView)view;
		}
		
		// Unbind the default EventCommandBinder and rebind the SignalCommandBinder
		protected override void addCoreComponents()
		{
			base.addCoreComponents();
			injectionBinder.Unbind<ICommandBinder>();
			injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
		}
		
		override public void Launch()
		{
			base.Launch();

            Debug.LogFormat("Start: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            // Make sure you've mapped this to a StartCommand!
            StartSignal startSignal= (StartSignal)injectionBinder.GetInstance<StartSignal>();
			startSignal.Dispatch();
		}

        protected override void mapBindings()
        {
            Debug.LogFormat("MapBindingsStart: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            // Cross Context Signals
            injectionBinder.Bind<InitializeViewSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<StartGameSessionSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ShowMainMenuSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<RejoinGameSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ShowNewConfirmDialogSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LogFeedbackSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LoadTitleDataSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<TitleDataLoadedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<AddChatMessageSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ChatMessageAddedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<AddGlobalChatMessagesSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GlobalChatMessageAddedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ConnectedToGlobalChatSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<AddFriendSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<AddFriendResponseSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ConfirmFriendSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ConfirmFriendResponseSignal>().ToSingleton().CrossContext();
            //injectionBinder.Bind<ManagePlayFabFriendListSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<DeclineFriendRequestSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<DeclineFriendResponseSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<RemoveFriendSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<RemoveFriendResponseSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<BlockFriendSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<BlockFriendResponseSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UnblockFriendSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UnblockFriendResponseSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FriendStatusUpdatedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FriendNotificationSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UpdateFriendStatusSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FriendsListLoadedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FriendChallengeSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<DeclineFriendChallengeSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FriendChallengeDeclinedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CancelFriendChallengeSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FriendChallengeCanceledSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ConnectToPhotonChatSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<SendChatMessageSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LogCustomEventSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<TeardownUISignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<AsyncProgressSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GameEventMessageSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CombatStateMessageSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<PlayerStateUpdatedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<OpponentStateUpdatedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CardStateUpdatedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GameStateInitializedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<SubmitGameActionSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ConcedeGameSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LegalAbilitiesUpdatedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<PhaseChangedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LoadAccountInfoSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<AccountEmailFoundSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UsernameNotAvailable>().ToSingleton();
            injectionBinder.Bind<InvalidEmailOrPasswordSignal>().ToSingleton();
            injectionBinder.Bind<AwaitGameStartOrCancelSignal>().ToSingleton();
            injectionBinder.Bind<InitializeProgressScreenSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LanguageChangedSignal>().ToSingleton();
            //injectionBinder.Bind<AgeGatePassedSignal> ().ToSingleton ();
            injectionBinder.Bind<SteamAuthTicketSignal>().ToSingleton();
            injectionBinder.Bind<ToFriendChallengeSignal>().ToSingleton();
            injectionBinder.Bind<AnimatedViewOpenedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<PrizeOpenedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ShowTutorialDialogSignal>().ToSingleton();
            injectionBinder.Bind<MoneyPurchaseSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<BeginVirtualPurchaseSignal>().ToSingleton().CrossContext();

            injectionBinder.Bind<SelfEmoteSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<OpponentEmoteSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<SelfInputActionSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<OpponentInputActionSignal>().ToSingleton().CrossContext();

            injectionBinder.Bind<MoveAnimatedViewToCanvasSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<AddCardToDeckSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<RemoveCardFromDeckSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CraftItemSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CardAddedToDeckSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CardRemovedFromDeckSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ShowTwoTouchSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CloseTwoTouchViewSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GameLogsExportedSignal>().ToSingleton();
            //injectionBinder.Bind<GameOverSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UnloadUnusedResourcesSignal>().ToSingleton().CrossContext();

            commandBinder.Bind<GameOverSignal>().To<ShowGameOverUICommand>();
            commandBinder.Bind<LogGameOverDataSignal>().To<LogGameOverDataCommand>();

            // Animated Views
            commandBinder.Bind<OpenAnimatedViewSignal>().To<OpenAnimatedViewCommand>();
            commandBinder.Bind<OpenBlurredAnimatedViewSignal>().To<OpenBlurredAnimatedViewCommand>();
            commandBinder.Bind<CloseAnimatedViewSignal>().To<CloseAnimatedViewCommand>();
            commandBinder.Bind<ShowMainMenuSignal>().To<ShowMainMenuCommand>();
            commandBinder.Bind<ShowNewConfirmDialogSignal>().To<ShowNewConfirmDialogCommand>();
            commandBinder.Bind<ShowNewMessageDialogSignal>().To<ShowNewMessageDialogCommand>();
            commandBinder.Bind<OpenPackSignal>().To<OpenPackCommand>();
            commandBinder.Bind<OpenPrizeSignal>().To<OpenPrizeCommand>();
            commandBinder.Bind<OpenSmartPackSignal>().To<OpenSmartPackCommand>();
            commandBinder.Bind<CraftItemSignal>().To<CraftItemCommand>();
            commandBinder.Bind<CraftResponseSignal>().To<CraftResponseCommand>();
            commandBinder.Bind<BeginVirtualPurchaseSignal>().To<BeginVirtualPurchaseCommand>();
            commandBinder.Bind<VirtualPurchaseResponseSignal>().To<VirtualPurchaseResponseCommand>();
            commandBinder.Bind<MoveAnimatedViewToCanvasSignal>().To<MoveAnimatedViewToCanvasCommand>();
            commandBinder.Bind<InitializeAnimatedViewSignal>().To<InitializeAnimatedViewCommand>();
            commandBinder.Bind<ShareScreenShotSignal>().To<ShareScreenShotCommand>();
            commandBinder.Bind<ZendeskTicketSignal>().To<ZendeskTicketCommand>();
            commandBinder.Bind<ShowTwoTouchSignal>().To<ShowTwoTouchCommand>();
            commandBinder.Bind<CloseTwoTouchViewSignal>().To<CloseTwoTouchViewCommand>();
            commandBinder.Bind<PlayFxSignal>().To<PlayFxCommand>();
            commandBinder.Bind<InitializeBaseViewsSignal>()
                .To<InitializeObjectPoolsCommand>()
                .To<InitializeBaseViewsCommand>()
                .InParallel();

            // Application Flow
            commandBinder.Bind<TeardownUISignal>().To<TeardownUICommand>();
            commandBinder.Bind<InitializeProgressScreenSignal>().To<InitializeProgressScreenCommand>();
            commandBinder.Bind<AwaitGameStartOrCancelSignal>().To<AwaitGameStartOrCancelCommand>();
            commandBinder.Bind<ServerReconnectSignal>().To<ServerReconnectCommand>();
            commandBinder.Bind<UnloadUnusedResourcesSignal>().To<UnloadUnusedResourcesCommand>();

            // Deck Commands
            commandBinder.Bind<AddCardToDeckSignal>().To<AddCardToDeckCommand>();
            commandBinder.Bind<CloneDeckSignal>().To<CloneDeckCommand>();
            commandBinder.Bind<SaveDeckSignal>().To<SaveDeckCommand>();
            commandBinder.Bind<DeleteDeckSignal>().To<DeleteDeckCommand>();
            commandBinder.Bind<DeselectDeckSignal>().To<DeselectDeckCommand>();
            commandBinder.Bind<FilterCardsSignal>().To<FilterCardsCommand>();
            commandBinder.Bind<NewDeckSignal>().To<NewDeckCommand>();
            commandBinder.Bind<RemoveCardFromDeckSignal>().To<RemoveCardFromDeckCommand>();
            commandBinder.Bind<SelectDeckSignal>().To<SelectDeckCommand>();
            commandBinder.Bind<UpdateDeckOrderSignal>().To<UpdateDeckOrderCommand>();

            // Matchmaking
            commandBinder.Bind<AcceptFriendChallengeSignal>().To<AcceptFriendChallengeCommand>();
            commandBinder.Bind<CancelFriendChallengeSignal>().To<CancelFriendChallengeCommand>();
            commandBinder.Bind<ChallengeFriendSignal>().To<ChallengeFriendCommand>();
            commandBinder.Bind<DeclineFriendChallengeSignal>().To<DeclineFriendChallengeCommand>();
            commandBinder.Bind<FriendChallengeCanceledSignal>().To<FriendChallengeCanceledCommand>();
            commandBinder.Bind<FriendChallengeSignal>().To<FriendChallengeCommand>();
            commandBinder.Bind<CancelSignal>().To<CancelCommand>();

            // Photon Chat
            commandBinder.Bind<AddChatMessageSignal>().To<AddChatMessageCommand>();
            commandBinder.Bind<AddGlobalChatMessagesSignal>().To<AddGlobalChatMessagesCommand>();
            commandBinder.Bind<ConnectToPhotonChatSignal>()
                .To<GetPhotonAuthTokenCommand>()
                .To<ConnectToPhotonChatCommand>()
                .InSequence();
            commandBinder.Bind<SendChatMessageSignal>()
                .To<GetPhotonAuthTokenCommand>()
                .To<ConnectToPhotonChatCommand>()
                .To<SendChatMessageCommand>();
            commandBinder.Bind<LoadChatHistorySignal>().To<LoadChatHistoryCommand>();
            commandBinder.Bind<SaveChatHistorySignal>().To<SaveChatHistoryCommand>();

            // Friends
            commandBinder.Bind<LoadFriendsListSignal>().To<LoadFriendsListCommand>();
            commandBinder.Bind<UpdateFriendStatusSignal>().To<UpdateFriendStatusCommand>();
            commandBinder.Bind<AddFriendSignal>().To<AddFriendCommand>();
            commandBinder.Bind<ConfirmFriendSignal>().To<ConfirmFriendCommand>();
            commandBinder.Bind<DeclineFriendRequestSignal>().To<DeclineFriendRequestCommand>();
            commandBinder.Bind<BlockFriendSignal>().To<BlockFriendCommand>();
            commandBinder.Bind<UnblockFriendSignal>().To<UnblockFriendCommand>();
            commandBinder.Bind<RemoveFriendSignal>().To<RemoveFriendCommand>();
            //commandBinder.Bind<ManagePlayFabFriendListSignal>().To<ManagePlayFabFriendListCommand>();
            commandBinder.Bind<ManageFriendResponseSignal>().To<ManageFriendResponseCommand>();
            //commandBinder.Bind<InitializeFriendListSignal>()
            //    .To<LoadFriendsListCommand>()
            //    .To<SyncPlayFabFriendListCommand>()
            //    .InSequence();

            // Playfab
            //commandBinder.Bind<SaveDraftDeckSignal>().To<SaveDraftDeckCommand>();
            commandBinder.Bind<AccountRecoverySignal>().To<AccountRecoveryCommand>();
            //commandBinder.Bind<LoadFullCatalogSignal>().To<LoadFullCatalogCommand>();
            //commandBinder.Bind<LoadCatalogSignal>().To<LoadCatalogCommand>();
            commandBinder.Bind<LoadInventorySignal>().To<LoadInventoryCommand>();
            commandBinder.Bind<LoadTitleDataSignal>().To<LoadTitleDataCommand>();
            commandBinder.Bind<LoadDataFileSignal>().To<LoadDataFileCommand>();
            commandBinder.Bind<LogCustomEventSignal>().To<LogCustomEventCommand>();
            commandBinder.Bind<UpdateDeckListSignal>().To<UpdateDeckListCommand>();
            commandBinder.Bind<LogFeedbackSignal>().To<LogFeedbackCommand>();
            commandBinder.Bind<LoginUserWithEmailSignal>().To<LoginUserWithEmailCommand>();
            commandBinder.Bind<LoginUserWithHashedEmailSignal>().To<LoginUserWithHashedEmailCommand>();
            commandBinder.Bind<AddDisplayNameSignal>().To<AddDisplayNameCommand>();
            commandBinder.Bind<MoneyPurchaseSignal>().To<MoneyPurchaseCommand>();
            commandBinder.Bind<PostPurchaseSignal>().To<PostPurchaseCommand>();
            commandBinder.Bind<RegisterUserSignal>().To<RegisterUserCommand>();
            commandBinder.Bind<AddUsernameAndPasswordSignal>().To<AddUsernameAndPasswordCommand>();
            commandBinder.Bind<VirtualPurchaseSignal>().To<VirtualPurchaseCommand>();
            commandBinder.Bind<LoadUserReadOnlyDataSignal>().To<LoadUserReadOnlyDataCommand>();
            commandBinder.Bind<SmartPackOpenedSignal>().To<SmartPackOpenedCommand>();
            commandBinder.Bind<ShowLeaderboardSignal>().To<ShowLeaderboardCommand>();
            commandBinder.Bind<RedeemCouponSignal>().To<RedeemCouponCommand>();
            commandBinder.Bind<RedeemCouponResponseSignal>().To<RedeemCouponResponseCommand>();
            commandBinder.Bind<ShowPurchaseOffersSignal>().To<ShowPurchaseOffersCommand>();

            // PlayFab Accounts
            commandBinder.Bind<LinkDeviceOrSteamSignal>().To<LinkDeviceOrSteamCommand>();
            commandBinder.Bind<UnlinkDeviceOrSteamSignal>().To<UnlinkDeviceOrSteamCommand>();
            commandBinder.Bind<LoginWithDeviceOrSteamSignal>().To<LoginWithDeviceOrSteamCommand>();
            commandBinder.Bind<UserLoggedInSignal>().To<UserLoggedInCommand>();
#if UNITY_STEAM
            commandBinder.Bind<GetSteamAuthTokenSignal>().To<GetSteamAuthTokenCommand>();
#endif

            // Settings
            commandBinder.Bind<LoadPlayerSettingsSignal>().To<LoadPlayerSettingsCommand>();
            commandBinder.Bind<LoadSettingsSignal>().To<LoadSettingsCommand>();
            commandBinder.Bind<SaveSettingsSignal>().To<SaveSettingsCommand>();
            commandBinder.Bind<SelectAvatarSignal>().To<SelectAvatarCommand>();
            commandBinder.Bind<SelectCardbackSignal>().To<SelectCardbackCommand>();
            commandBinder.Bind<ResizeArenaSignal>().To<ResizeArenaCommand>();
            commandBinder.Bind<SaveUiHintDataSignal>().To<SaveUiHintDataCommand>();
            commandBinder.Bind<BackupMetricsDataSignal>().To<BackupMetricsDataCommand>();
            // Sound

            // Startup
            commandBinder.Bind<FinishPlayerDataLoadingSignal>().To<FinishPlayerDataLoadingCommand>();

            // Analytics
            commandBinder.Bind<InitializeAnalyticsSignal>().To<InitializeAnalyticsCommand>();
            commandBinder.Bind<LogEventSignal>().To<LogEventCommand>();
            commandBinder.Bind<EndTimedAnalyticsEventSignal>().To<EndTimedAnalyticsEventCommand>();
            commandBinder.Bind<RateGameSignal>().To<RateGameCommand>();

            // Networking
            commandBinder.Bind<MatchmakeToServerSignal>().To<MatchmakeToServerCommand>();
            commandBinder.Bind<LogInToServerSignal>().To<LogInToServerCommand>();
            // Log Out command sequence (returns to the exact same state as startup)
            commandBinder.Bind<LogoutUserSignal>().To<LogoutUserCommand>();
            commandBinder.Bind<WaitForPlayerDataSignal>()
                .To<CheckAgeGateCommand>()
                .To<WaitForPlayerDataCommand>().InSequence();
            commandBinder.Bind<ConcedeGameSignal>().To<ConcedeGameCommand>();
            commandBinder.Bind<ConnectToServerSignal>().To<ConnectToServerCommand>();
            commandBinder.Bind<SubmitGameActionSignal>().To<SubmitGameActionCommand>();
            commandBinder.Bind<SubmitDraftPickSignal>().To<SubmitDraftPickCommand>();
            commandBinder.Bind<ServerErrorRaisedSignal>().To<ServerErrorHandlerCommand>();
            commandBinder.Bind<ProcessDraftPickEventsSignal>().To<ProcessDraftPickEventsCommand>();
            commandBinder.Bind<ProcessGameStateEventsSignal>().To<ProcessGameStateEventsCommand>();
            commandBinder.Bind<PlayerDataResponseSignal>().To<PlayerDataResponseCommand>();
            commandBinder.Bind<PlayerLoggedInSignal>().To<PlayerLoggedInCommand>();
            commandBinder.Bind<EmailListSignupSignal>().To<EmailListSignupCommand>();
            commandBinder.Bind<ExportGameLogSignal>().To<ExportGameLogCommand>();

            // DesignTime Model loading;
            injectionBinder.Bind<INovaContext>().To(mainContextView).ToSingleton();
            injectionBinder.Bind<UIConfiguration>().To(mainContextView.UIConfig).ToSingleton().CrossContext();
            injectionBinder.Bind<ViewReferences>().To(mainContextView.UIConfig.ViewReferences).ToSingleton();
            injectionBinder.Bind<IDragOverlay>().To(mainContextView.DragOverlay).ToSingleton();
            injectionBinder.Bind<NovaAudio>().To(GameObject.FindObjectOfType<NovaAudio>()).ToSingleton().CrossContext();

            // Models
            injectionBinder.Bind<ResourceMap>().To(mainContextView.ResourceMap).ToSingleton().CrossContext();
            injectionBinder.Bind<GameData>().ToSingleton().CrossContext();
            injectionBinder.Bind<PlayerProfile>().ToSingleton().CrossContext();
            injectionBinder.Bind<MetagameSettings>().ToSingleton();
            injectionBinder.Bind<ClientPrefs>().ToSingleton().CrossContext();
            injectionBinder.Bind<ViewStateStack>().ToSingleton().CrossContext();


            // Start up Command Sequence
            commandBinder.Bind<StartSignal>()
                .To<StartCommand>()
                .To<CheckAgeGateCommand>()
                .To<PreloadAssetBundlesCommand>()
                .To<InitializeClientPrefsCommand>()
                //.To<LoadCardArtPositionDataCommand>()
                .To<LoadSettingsCommand>()
#if ((UNITY_EDITOR && !UNITY_IOS) || RT_DEV || TEST_BUILD || DEVELOPMENT_BUILD)
                // DEV environment Settings	
                .To<InitializeDevEnvironmentCommand>()
                //.To<InitializeLiveEnvironmentCommand>()
#else
				// LIVE environment Settings
				.To<InitializeLiveEnvironmentCommand>()
                //.To<InitializeDevEnvironmentCommand>()
#endif
                .To<InitializeMetagameServicesCommand>()
                .To<WaitForPlayerDataCommand>()
                .InSequence();

            commandBinder.Bind<LoadAccountInfoSignal>()
                .To<LoadTitleDataCommand>()
                .To<LoadAccountInfoCommand>() // It's important to do this before the matchmake command - we get some race conditions otherwise
                .To<MatchmakeToServerCommand>()
                .To<FinishPlayerDataLoadingCommand>()
                .InSequence();

            // Base "StartGameSessionCommand" that launches the correct start session
            // sequence (below) and handles transition from the UI context to the Arnea
            commandBinder.Bind<StartGameSessionSignal>()
                .To<ShowProgressLoadingScreenCommand>()
                .To<MatchmakeToServerCommand>()
                .To<SaveChatHistoryCommand>()
                .To<NewStartGameSessionCommand>()
                .InSequence();

            // Practice Game Session Sequence	
            commandBinder.Bind<RejoinGameSignal>()
                .To<PreInitializeArenaCommand>()
                //.To<RejoinGameCommand>()
                .To<SendCreateGameCommand>()
                .To<WaitForCreateGameCommand>()
                .To<JoinGameCommand>()
                .To<InitializeNewArenaGameControllerCommand>()
                .To<ReadyToStartGameCommand>()
                .InSequence();

            // Play game vs. matchmaking opponent or accept a friend challenge game (draft game too)
            commandBinder.Bind<StartGameSignal>()
                .To<PreInitializeArenaCommand>()
                .To<SendCreateGameCommand>()
                .To<WaitForCreateGameCommand>()
                .To<JoinGameCommand>()
                .To<InitializeNewArenaGameControllerCommand>()
                .To<ReadyToStartGameCommand>()
                .InSequence();

            // Initiate a friend challenge session and wait for opponent to join
            commandBinder.Bind<StartChallengeGameSignal>()
                .To<PreInitializeArenaCommand>()
                .To<SendCreateGameCommand>()
                .To<WaitForCreateGameCommand>()    // Wait for your own createGameResponse
                .To<JoinGameCommand>()
                .To<InitializeNewArenaGameControllerCommand>()
                .To<ReadyToStartGameCommand>()
                .InSequence();

            // Start or resume a paid game session
            commandBinder.Bind<StartPaidGameSessionSignal>()
                .To<ShowProgressLoadingScreenCommand>()
                .To<StartPaidGameSessionCommand>()
                .To<MatchmakeToServerCommand>()
                .To<PaidGameNewOrResumeCommand>()
                .InSequence();

            // Drop from Event (Draft) flow
            commandBinder.Bind<DropFromEventSignal>()
                .To<ShowProgressLoadingScreenCommand>()
                .To<MatchmakeToServerCommand>()
                .To<DropFromEventCommand>()
                .InSequence();

            // Mediators
            mediationBinder.Bind<FirstDialogView>().To<FirstDialogMediator>();
            mediationBinder.Bind<LoginDialogNewFlowView>().To<LoginDialogNewFlowMediator>();
            mediationBinder.Bind<ForgotPasswordView>().To<ForgotPasswordMediator>();
            mediationBinder.Bind<MainHeaderView>().To<MainHeaderMediator>();
            mediationBinder.Bind<HomeButtonsView>().To<HomeButtonsMediator>();
            mediationBinder.Bind<FriendListDockView>().To<FriendListDockMediator>();
            mediationBinder.Bind<GlobalChatView>().To<GlobalChatMediator>();

            mediationBinder.Bind<ProfileMenuView>().To<ProfileMenuMediator>();
            mediationBinder.Bind<MobileTermsView>().To<MobileTermsMediator>();
            mediationBinder.Bind<PlayerProfileView>().To<PlayerProfileMediator>();
            mediationBinder.Bind<GameLogBrowserView>().To<GameLogBrowserMediator>();
            mediationBinder.Bind<QuestAndStatsView>().To<QuestAndStatsMediator>();
            mediationBinder.Bind<SettingsView>().To<SettingsMediator>();
            mediationBinder.Bind<AvatarSelectorView>().To<AvatarSelectorMediator>();
            mediationBinder.Bind<CardBackSelectorView>().To<CardBackSelectorMediator>();
            mediationBinder.Bind<LanguageSelectorView>().To<LanguageSelectorMediator>();
            mediationBinder.Bind<LocalAvatarView>().To<LocalAvatarMediator>();
            mediationBinder.Bind<LocalCardbackView>().To<LocalCardbackMediator>();
            mediationBinder.Bind<AllAchievementsView>().To<AllAchievementsMediator>();
            mediationBinder.Bind<AudioSettingsView>().To<AudioSettingsMediator>();
            mediationBinder.Bind<GameLogView>().To<GameLogMediator>();

            mediationBinder.Bind<PlayScreenColumnView>().To<PlayScreenColumnMediator>();
            mediationBinder.Bind<DeckListView>().To<DeckListMediator>();
            mediationBinder.Bind<DeckListDockView>().To<DeckListDockMediator>();
            mediationBinder.Bind<TwoTouchOverlayView>().To<TwoTouchOverlayMediator>();

            mediationBinder.Bind<EventScreenColumnView>().To<EventScreenColumnMediator>();

            mediationBinder.Bind<CardBrowserView>().To<CardBrowserMediator>();
            mediationBinder.Bind<CardListView>().To<CardListMediator>();
            //mediationBinder.Bind<CardFilterView>().To<CardFilterMediator>();
            mediationBinder.Bind<CardDetailsView>().To<CardDetailsMediator>();
            mediationBinder.Bind<DeckStatsView>().To<DeckStatsMediator>();
            mediationBinder.Bind<DeckScreenShotView>().To<DeckScreenShotMediator>();

            mediationBinder.Bind<DraftCardListView>().To<DraftCardListMediator>();
            mediationBinder.Bind<DraftPackBrowserView>().To<DraftPackBrowserMediator>();

            mediationBinder.Bind<NewConfirmDialogView>().To<NewConfirmDialogMediator>();
            mediationBinder.Bind<NewMessageDialogView>().To<NewMessageDialogMediator>();
            mediationBinder.Bind<ReconnectDialogView>().To<ReconnectDialogMediator>();

            mediationBinder.Bind<StoreView>().To<StoreMediator>();
            mediationBinder.Bind<OfferView>().To<OfferMediator>();
            mediationBinder.Bind<OpenPrizeView>().To<OpenPrizeMediator>();
            mediationBinder.Bind<PackOpenerConfirmView>().To<PackOpenerConfirmMediator>();
            mediationBinder.Bind<ConfirmVirtualPurchaseDialogView>().To<ConfirmVirtualPurchaseDialogMediator>();
            mediationBinder.Bind<RedeemCouponView>().To<RedeemCouponMediator>();

            mediationBinder.Bind<LeaderboardView>().To<LeaderboardMediator>();
            mediationBinder.Bind<EndGameDialogView>().To<EndGameDialogMediator>();
            mediationBinder.Bind<TutorialPopUpView>().To<TutorialPopUpMediator>();
            mediationBinder.Bind<RatingsView>().To<RatingsMediator>();

            // Screens
            commandBinder.Bind<ScreenTransitionSignal>().To<ScreenTransitionCommand>();
            injectionBinder.Bind<LoginScreen>().ToSingleton();
            injectionBinder.Bind<HomeScreen>().ToSingleton();
            injectionBinder.Bind<PlayScreen>().ToSingleton();
            injectionBinder.Bind<DeckBuilderScreen>().ToSingleton();
            injectionBinder.Bind<DraftScreen>().ToSingleton();
            injectionBinder.Bind<EventsScreen>().ToSingleton();

            // Gameplay Mediators
            mediationBinder.Bind<ProgressLoadingScreenView>().To<ProgressLoadingScreenMediator>();
            //mediationBinder.Bind<FeedbackView>().To<FeedbackMediator>();
            mediationBinder.Bind<ReportBugView>().To<ReportBugMediator>();

            // Signals
            //injectionBinder.Bind<CancelSignal>().ToSingleton();
            injectionBinder.Bind<CancelMatchmakingSignal>().ToSingleton();
            injectionBinder.Bind<ServerConnectedSignal>().ToSingleton();
            injectionBinder.Bind<GameJoinedSignal>().ToSingleton();
            injectionBinder.Bind<StartGameReadySignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<DiffGameStateMessageSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<PaidGameNewOrResumedSignal>().ToSingleton();
            injectionBinder.Bind<DraftSessionLoadedSignal>().ToSingleton();
            injectionBinder.Bind<DroppedFromEventSignal>().ToSingleton();
            injectionBinder.Bind<DraftNextPackSignal>().ToSingleton();
            injectionBinder.Bind<DraftOverSignal>().ToSingleton();
            injectionBinder.Bind<CancelMatchmakingResponseSignal>().ToSingleton();
            injectionBinder.Bind<GameCreatedSignal>().ToSingleton();

            injectionBinder.Bind<ProgressScreenClosedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ShowCancelProgressButtonSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<HideCancelProgressButtonSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<SetProgressScreenStatusLabelSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<PlayFabErrorRaisedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UsernameAndPasswordAddedSignal>().ToSingleton();
            injectionBinder.Bind<UserRegisteredSignal>().ToSingleton();
            injectionBinder.Bind<AccountInfoLoadedSignal>().ToSingleton();
            injectionBinder.Bind<PlayerDataLoadedSignal>().ToSingleton();
            injectionBinder.Bind<InventoryLoadedSignal>().ToSingleton();
            injectionBinder.Bind<CatalogLoadedSignal>().ToSingleton();
            injectionBinder.Bind<ServerResponseSignal>().ToSingleton();

            injectionBinder.Bind<SettingsLoadedSignal>().ToSingleton();
            injectionBinder.Bind<DeviceOrSteamUnlinkedSignal>().ToSingleton();

            injectionBinder.Bind<DecksModifiedSignal>().ToSingleton();
            injectionBinder.Bind<DeckRenamedSignal>().ToSingleton();
            injectionBinder.Bind<DeckClonedSignal>().ToSingleton();
            injectionBinder.Bind<DeckDeletedSignal>().ToSingleton();
            injectionBinder.Bind<DeckSelectedSignal>().ToSingleton();
            injectionBinder.Bind<DeckDeselectedSignal>().ToSingleton();
            injectionBinder.Bind<CardAddedToDeckSignal>().ToSingleton();
            injectionBinder.Bind<CardRemovedFromDeckSignal>().ToSingleton();
            injectionBinder.Bind<CardsFilteredSignal>().ToSingleton();

            injectionBinder.Bind<ProfileUpdatedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<DialogDismissedSignal>().ToSingleton();
            injectionBinder.Bind<DialogConfirmedSignal>().ToSingleton();
            injectionBinder.Bind<TextSubmittedSignal>().ToSingleton();
            injectionBinder.Bind<NewConfirmDialogDismissedSignal>().ToSingleton();

            injectionBinder.Bind<PackOpenedSignal>().ToSingleton();
            injectionBinder.Bind<CardTwoTouchOpenedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<CardTwoTouchClosedSignal>().ToSingleton().CrossContext();

            // Services
            injectionBinder.Bind<MetagameServices>().ToSingleton().CrossContext();
            injectionBinder.Bind<GameControllerService>().ToSingleton().CrossContext();

            //Debug.LogFormat("Bindings End: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            //MetricsTrackingService


            // Create the metrics 
            GameObject metricsTrackingService = new GameObject() { name = "MetricsTrackingService" };
            metricsTrackingService.transform.SetParent(this.mainContextView.transform);
            metricsService = metricsTrackingService.AddComponent<MetricsTrackingService>();
            injectionBinder.Bind<MetricsTrackingService>().To(metricsService).ToSingleton().CrossContext();
            //Debug.LogFormat("Metrics bound: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);

            // Create a Playfab Chat client Service and Bind it
            GameObject playfabChatClient = new GameObject { name = "PlayfabChatClient" };
            chatClient = playfabChatClient.AddComponent<PlayFabChatClient>();
            injectionBinder.Bind<PlayFabChatClient>().To(chatClient).ToSingleton();
            //Debug.LogFormat("Chat bound: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);

            // Create a ClientNetworkingService and Bind it
            GameObject clientNetworkingService = new GameObject { name = "ClientNetworking" };
            clientNetworkingService.transform.SetParent(this.mainContextView.transform);
            clientNetworking = clientNetworkingService.AddComponent<ClientService>();
            injectionBinder.Bind<ClientService>().To(clientNetworking).ToSingleton().CrossContext();
            MainContextView.clientService = clientNetworking;

            // Create a AssetBundleManager and Bind it
            GameObject assetBundleManagerService = new GameObject { name = "AssetBundleManager" };
            assetBundleManagerService.transform.SetParent(this.mainContextView.transform);
            assetBundleManager = assetBundleManagerService.AddComponent<AssetBundleManager>();
            injectionBinder.Bind<AssetBundleManager>().To(assetBundleManager).ToSingleton().CrossContext();
            //Debug.LogFormat("Client bound: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);

            // Create the NovaIAPService and Bind it.
            GameObject novaIAPService = new GameObject { name = "NovaIAP" };
            novaIAPService.transform.SetParent(this.mainContextView.transform);
            novaIAP = novaIAPService.AddComponent<NovaIAPService>();
            injectionBinder.Bind<NovaIAPService>().To(novaIAP).ToSingleton().CrossContext();

            //Debug.LogFormat("IAP bound: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);

            // Create a AssetManagerService and bind it
            GameObject assetManagerGO = new GameObject { name = "AssetManagerService" };
            assetManagerGO.transform.SetParent(this.mainContextView.transform);
            assetManagerService = assetManagerGO.AddComponent<AssetManagerService>();
            injectionBinder.Bind<AssetManagerService>().To(assetManagerService).ToSingleton().CrossContext();

            // Create a CardListItemProxyService and bind it
            GameObject cardListItemServiceGO = new GameObject { name = "CardListItemProxyService" };
            cardListItemServiceGO.transform.SetParent(this.mainContextView.transform);
            cardListItemProxyService = cardListItemServiceGO.AddComponent<CardListItemProxyService>();
            injectionBinder.Bind<CardListItemProxyService>().To(cardListItemProxyService).ToSingleton().CrossContext();

            //Debug.LogFormat("Asset Manager bound: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);


            // Create a ClientNetworkingService and Bind it
            /*GameObject chillingoService = new GameObject();
            chillingoService.name = "ChillingoService";
            chillingoService.transform.SetParent(this.mainContextView.transform);
            chillingo = chillingoService.AddComponent<ChillingoService>();
            injectionBinder.Bind<ChillingoService>().To(chillingo).ToSingleton().CrossContext();*/

            //Debug.LogFormat("Chillingo bound: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            //Make use of the IAP Context.
            UnityIAPBindingsFactory.Create(commandBinder, injectionBinder, mediationBinder);

#if UNITY_STEAM
            GameObject novaSteamService = new GameObject { name = "NovaSteamService" };
            novaSteamService.transform.SetParent(this.mainContextView.transform);
            novaSteamManager = novaSteamService.AddComponent<NovaSteamService>();
            injectionBinder.Bind<NovaSteamService>().To(novaSteamManager).ToSingleton().CrossContext();
            //Debug.LogFormat("Steam bound: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
#endif
            
        }
    }
}