﻿using UnityEngine;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.mediation.impl;
using System;
using UnityStandardAssets.ImageEffects;
using DG.Tweening;
using LinqTools;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public interface INovaContext
	{
		Canvas MainCanvas { get; }
		Canvas OverlayCanvas { get;}
		Canvas HoldingCanvas { get;}
        RectTransform ParticleBounds { get; }

		void BlurMainCanvas(bool isBlurred, View view);
        bool IsAlreadyBlurred { get; }
	}

    public class MainContextView : ContextView, INovaContext
    {
        public ClientService clientService { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public ScreenTransitionSignal ScreenTransitionSignal { get; set; }

        public Camera CameraForUI;
		public Canvas mainCanvas;
		public Canvas overlayCanvas;
		public Canvas holdingCanvas;
        public RectTransform particleBounds;
		public Canvas MainCanvas { get { return this.mainCanvas;}}
		public Canvas OverlayCanvas { get { return this.overlayCanvas;}}
		public Canvas HoldingCanvas { get { return this.holdingCanvas;} }
        public RectTransform ParticleBounds { get { return this.particleBounds; } }
        public ResourceMap ResourceMap {get; protected set;}
		public UIConfiguration UIConfig {get; protected set;}
		public DragOverlay DragOverlay {get; protected set;}
		public TwoTouchOverlayView TwoTouchOverlay {get; protected set;}
		public NovaBlurComponent NovaBlur;
		public bool IsAlreadyBlurred { get{ return this.NovaBlur.IsAlreadyBlurred;}}
		void Awake()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
            {
/*#if DEVELOPMENT_BUILD
                Debug.logger.filterLogType = LogType.Log;
#else
				Debug.logger.filterLogType = LogType.Warning;
#endif*/
                Application.targetFrameRate = 30;
            }
            else
            {

                Application.targetFrameRate = 60;
            }

            if (Application.isEditor)
            {
                Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);
                Application.SetStackTraceLogType(LogType.Warning, StackTraceLogType.ScriptOnly);
                Application.SetStackTraceLogType(LogType.Error, StackTraceLogType.ScriptOnly);
                Application.SetStackTraceLogType(LogType.Exception, StackTraceLogType.ScriptOnly);
                Application.SetStackTraceLogType(LogType.Assert, StackTraceLogType.ScriptOnly);
            }
            else
			{
				Application.SetStackTraceLogType(LogType.Exception, StackTraceLogType.ScriptOnly);
                Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
                Application.SetStackTraceLogType(LogType.Warning, StackTraceLogType.None);
                Application.SetStackTraceLogType(LogType.Error, StackTraceLogType.None);
				Application.SetStackTraceLogType(LogType.Assert, StackTraceLogType.None);
            }

            Debug.LogFormat("Main Context Waking up: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.ResourceMap = Resources.Load<ResourceMap>(NovaConfig.PATH_RESMAP);
			this.UIConfig = Resources.Load<UIConfiguration>(NovaConfig.PATH_UICFG);
			InitializeUIConfiguration ();
			this.DragOverlay = GameObject.Instantiate(this.UIConfig.DragOverlayPrefab);
			this.DragOverlay.transform.SetParent(this.OverlayCanvas.transform);
			


        }

        void Start()
        {

			context = new NovaBlitz.UI.MainContext(this, true);
			context.Start();
			this.TwoTouchOverlay = (TwoTouchOverlayView)this.UIConfig.ViewReferences.Instantiate(typeof(TwoTouchOverlayView),this.OverlayCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
			GameObject.DontDestroyOnLoad(this.gameObject);
        }

        protected void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape) && clientService.IsInGame == false) // it's a bool?, so it won't fire while the game's starting up either
            {
                if (this.CloseAnimatedViewSignal == null)
                    return;
                var view = this.UIConfig.ViewReferences.ViewStateStack.TryPeekViewState();
                if(view != null && view is AnimatedView)
                {
                    var type = view.GetType();
                    if (type == typeof(ProgressLoadingScreenView) || type == typeof(OpenPrizeView) || type == typeof(StartGameDialogView) || view is EndGameDialogView || view is StartingHandView)
                    {
                    }
                    else if (type == typeof(TwoTouchOverlayView))
                    {
                        (view as TwoTouchOverlayView).ClickScrim();
                    }
                    else
                    { 
                        this.CloseAnimatedViewSignal.Dispatch(type);
                    }
                }
                else if(clientService.IsConnected)
                {
                    if (clientService.PlayerProfile.LastMenuScreenViewed == typeof(HomeScreen))
                    {
                        this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(SettingsView));
                    }
                    else if (clientService.PlayerProfile.LastMenuScreenViewed == typeof(DraftScreen))
                    {
                        var dclv = (DraftCardListView)this.UIConfig.ViewReferences.Get(typeof(DraftCardListView));
                        if (dclv != null && dclv.IsOpen)
                            dclv.ClickDone();
                        // deck builder
                        // drafting screen
                    }
                    else if (clientService.PlayerProfile.LastMenuScreenViewed == typeof(DeckBuilderScreen))
                    {
                        var dclv = (CardListView)this.UIConfig.ViewReferences.Get(typeof(CardListView));
                        if (dclv != null && dclv.IsOpen)
                            dclv.OnDoneButtonClicked();
                        // deck builder
                        // drafting screen
                        // tournament Play screen > events
                    }
                    // tournament Play screen > events
                    else if (clientService.PlayerProfile.LastMenuScreenViewed == typeof(PlayScreen))
                    {
                        PlayScreenColumnView playScreenColumnView = (PlayScreenColumnView)this.UIConfig.ViewReferences.Get(typeof(PlayScreenColumnView));
                        if (playScreenColumnView.IsOpen == true)
                        {
                            playScreenColumnView.ClickBack();
                        }
                        // back to home
                    }
                    else if (clientService.PlayerProfile.LastMenuScreenViewed == typeof(EventsScreen))
                    {
                        EventScreenColumnView eventScreenColumnView = (EventScreenColumnView)this.UIConfig.ViewReferences.Get(typeof(EventScreenColumnView));
                        if (eventScreenColumnView.IsOpen == true)
                        {
                            eventScreenColumnView.ClickBack();
                        }
                        // back to home
                    }
                    // home screen - open/close settings.
                }
                // check to see if we're in game; if so, ignore (or fire off/close the in-game menu)

                // check the view state stack; if there's anything on it, close the top thing.

                // if there's nothing on the view state stack, and we're not on the home buttons
            }
            else if(Input.GetKeyUp(KeyCode.Menu) && clientService.IsInGame == false)
            {
                var progress = (AnimatedView)this.UIConfig.ViewReferences.Get(typeof(ProgressLoadingScreenView));
                if (progress != null && progress.IsOpen)
                    return;

                var menu = (AnimatedView)this.UIConfig.ViewReferences.Get(typeof(SettingsView));
                if (menu == null || !menu.IsOpen)
                {
                    this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(SettingsView));
                }
                else
                {
                    var view = this.UIConfig.ViewReferences.ViewStateStack.TryPeekViewState();
                    if (view is SettingsView)
                    {
                        this.CloseAnimatedViewSignal.Dispatch(typeof(SettingsView));
                    }
                }
                // in shell:
                // open the settings screen, or close it

                // in game:
            }
        }

        public void BlurMainCanvas(bool isBlurred, View currentView)
		{
			this.NovaBlur.BlurMainCanvas(isBlurred, currentView);
		}

        void OnApplicationPause(bool isPaused)
        {
            if(isPaused)
                SavePrefs();
        }


        void OnApplicationQuit()
        {
            SavePrefs();
        }

        void SavePrefs()
        { 
            // Only save the current resolution if we are in windowed mode
            if (Screen.fullScreen == false)
			{
				int width = Screen.width;
				int height = Screen.height;

				if(height + 150 > Screen.currentResolution.height)
				{
					height = Screen.currentResolution.height - 150;
				}

				Debug.LogFormat("Saving resolution: {0} {1}", width, height);
				PlayerPrefs.SetInt(NovaConfig.KEY_WINDOWED_RESOLUTION_X, width);
				PlayerPrefs.SetInt(NovaConfig.KEY_WINDOWED_RESOLUTION_Y, height);
			}

            PlayerPrefs.Save();
		}

		void InitializeUIConfiguration()
		{
			this.UIConfig.ViewReferences.loadPrefabDictionary = new System.Collections.Generic.Dictionary<Type, string> {
				{ typeof(BackgroundMainMenuView),"BackgroundMainMenu" },
				{ typeof(HomeButtonsView),"HomeButtonsPanel" },
				{ typeof(MainHeaderView),"HeaderPanel" },
				{ typeof(ProfileMenuView),"ProfilePanel" },
				{ typeof(FriendListDockView),"FriendListDockPanel" },
				{ typeof(PlayScreenColumnView),"PlayScreenColumn" },
				{ typeof(DeckListView),"DeckListPanel" },
				{ typeof(DeckListDockView),"DeckListDockPanel" },
				{ typeof(CardBrowserView),"CardBrowserPanel" },
				{ typeof(CardListView),"CardListPanel" },
				{ typeof(DeckStatsView),"DeckStatsPanel" },
				{ typeof(DraftPackBrowserView),"DraftPackBrowserPanel" },
				{ typeof(DraftCardListView),"DraftCardListPanel" },
				{ typeof(NewMessageDialogView),"NewMessageDialog" },
				{ typeof(NewConfirmDialogView),"ConfirmDialogPanel" },
				{ typeof(ProgressLoadingScreenView),"ProgressLoadingScreen" },
				{ typeof(StoreView),"StorePanel" },
                { typeof(OfferView), "SpecialOfferPanel" },
				{ typeof(AvatarSelectorView),"AvatarSelectorPanel" },
				{ typeof(CardBackSelectorView),"CardBackSelectorPanel" },
				{ typeof(InGameMenuView),"InGameMenuPanel" },
				{ typeof(PackOpenerConfirmView),"PackOpenerPanel" },
				{ typeof(ForgotPasswordView),"ForgotPasswordPanel" },
				{ typeof(DeckScreenShotView),"DeckScreenShotPanel" },
				{ typeof(ConfirmVirtualPurchaseDialogView),"ConfirmPurchaseDialogPanel" },
				{ typeof(LeaderboardView),"LeaderboardPanel" },
				{ typeof(LoginDialogNewFlowView),"LoginDialogNewFlowPanel" },
				{ typeof(EventScreenColumnView),"EventScreenColumn" },
				{ typeof(ReconnectDialogView),"ReconnectDialogPanel" },
				{ typeof(EndGameDialogView),"EndGameDialogPanel" },
				{ typeof(LanguageSelectorView),"LanguageSelector" },
				{ typeof(MobileTermsView),"MobileTermsPanel" },
				{ typeof(TwoTouchOverlayView),"TwoTouchOverlay" },
				{ typeof(RatingsView),"RatingPanel" },
				{ typeof(RedeemCouponView),"RedeemCouponPanel" },
				{ typeof(SettingsView),"SettingsPanel" },
				{ typeof(GameLogView),"GameLog" },
				{ typeof(GameLogBrowserView),"GameLogList" },
				{ typeof(OpenPrizeView),"OpenPrizeView" },
                { typeof(FirstDialogView),"FirstDialogPanel" },
                { typeof(ReportBugView),"ReportBugDialog" },
                { typeof(GlobalChatView),"GlobalChat" },
                { typeof(TutorialPopUpView),"TutorialPopUpDialogPanel" },
            };
			Debug.LogFormat ("Initialized Main Context Views: {0} entries", this.UIConfig.ViewReferences.loadPrefabDictionary.Count);
		}

		//Function is called when a lowMemoryWarning is received
		public void ReceivedMemoryWarning(string message)
		{
			Debug.LogWarningFormat("Memory Manager RECEIVED LOW MEMORY WARNING! {0}",message);
			Resources.UnloadUnusedAssets ();
		}

		public void WillResignActive1(string message)
		{
			Debug.LogWarning ("Will Resign Active 1");
		}

		public void WillResignActive2(string message)
		{
			Debug.LogWarning ("Will Resign Active 2");
		}
	}
}
