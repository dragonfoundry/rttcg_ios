﻿using UnityEngine;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;
using strange.extensions.command.api;
using strange.extensions.command.impl;

using NovaBlitz.UI.Mediators;
using NovaBlitz.UI;
using DragonFoundry.Controllers;

namespace NovaBlitz.Game
{
	public class 
    SessionContext : MVCSContext
	{
		private SessionContextView sessionContextView;
		
		public SessionContext() : base()
		{
			
		}
		
		public SessionContext(MonoBehaviour view, bool autoStartup) : base(view, autoStartup)
		{
			this.sessionContextView = (SessionContextView)view;
		}
		// Unbind the default EventCommandBinder and rebind the SignalCommandBinder
		protected override void addCoreComponents()
		{
			base.addCoreComponents();
			injectionBinder.Unbind<ICommandBinder>();
			injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
		}
		
		override public void Launch()
		{
			base.Launch();
		}
		
		protected override void mapBindings()
		{
			// Signals
            injectionBinder.Bind<DoneWithTurnClickedSignal>().ToSingleton();
            injectionBinder.Bind<TeardownArenaSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GameOverSignal>().ToSingleton().CrossContext();

            // CardEventService Signals
            injectionBinder.Bind<DealCardToStartingHandSignal>().ToSingleton();
            injectionBinder.Bind<DiscardCardFromStartingHandSignal>().ToSingleton();
            injectionBinder.Bind<DealCardToOpponentStartingHandSignal>().ToSingleton();
            injectionBinder.Bind<DiscardCardFromOpponentStartingHandSignal>().ToSingleton();
            injectionBinder.Bind<OpponentResolvesUnitSignal>().ToSingleton();
            injectionBinder.Bind<OpponentResolvesPowerSignal>().ToSingleton();
            injectionBinder.Bind<PlayerResolvesUnitSignal>().ToSingleton();
            injectionBinder.Bind<PlayerResolvesPowerSignal>().ToSingleton();
            injectionBinder.Bind<OpponentCastsCardSignal>().ToSingleton();
            injectionBinder.Bind<DealCardToOpponentHandSignal>().ToSingleton();
            injectionBinder.Bind<DealCardToHandSignal>().ToSingleton(); 
            injectionBinder.Bind<TrashPlayerCardSignal>().ToSingleton();
            injectionBinder.Bind<TrashOpponentCardSignal>().ToSingleton();
            injectionBinder.Bind<BanishPlayerCardSignal>().ToSingleton();
            injectionBinder.Bind<BanishOpponentCardSignal>().ToSingleton();
            injectionBinder.Bind<SpawnAndBanishPlayerCardSignal>().ToSingleton();
            injectionBinder.Bind<SpawnAndBanishOpponentCardSignal>().ToSingleton();
            injectionBinder.Bind<DiscardPlayerCardSignal>().ToSingleton();
            injectionBinder.Bind<DiscardOpponentCardSignal>().ToSingleton();
            injectionBinder.Bind<SpawnPlayerCardInArenaSignal>().ToSingleton();
            injectionBinder.Bind<SpawnOpponentCardInArenaSignal>().ToSingleton();
            injectionBinder.Bind<SummonPlayerCardToArenaSignal>().ToSingleton();
            injectionBinder.Bind<SummonOpponentCardToArenaSignal>().ToSingleton();
            injectionBinder.Bind<PlayerCardAttacksSignal>().ToSingleton();
            injectionBinder.Bind<OpponentCardAttacksSignal>().ToSingleton();
            injectionBinder.Bind<CardDamageTakenSignal>().ToSingleton();
            injectionBinder.Bind<PlayerDamageTakenSignal>().ToSingleton();
            injectionBinder.Bind<OpponentDamageTakenSignal>().ToSingleton();
            injectionBinder.Bind<ArenaCardRecycledSignal>().ToSingleton();
            injectionBinder.Bind<CardBackRecycledSignal>().ToSingleton();
            injectionBinder.Bind<LoseControlOfCardSignal>().ToSingleton();
            injectionBinder.Bind<GainControlOfCardSignal>().ToSingleton();
            injectionBinder.Bind<PlayCardFailedSignal>().ToSingleton();
            injectionBinder.Bind<PlayCardSucceededSignal>().ToSingleton();
            injectionBinder.Bind<CardHealedSignal>().ToSingleton();
            injectionBinder.Bind<PlayerHealedSignal>().ToSingleton();
            injectionBinder.Bind<OpponentHealedSignal>().ToSingleton();
            injectionBinder.Bind<ShowTutorialDialogSignal>().ToSingleton();
            injectionBinder.Bind<ModifyOrSetStatEventSignal>().ToSingleton();
            injectionBinder.Bind<OpponentActivateAbilitySignal>().ToSingleton();
            injectionBinder.Bind<InitiativeChangeSignal>().ToSingleton();

            // Singleton Models
            var uiConfig = Resources.Load<UIConfiguration>(NovaConfig.PATH_SESSCFG);
            injectionBinder.Bind<INovaContext>().To(sessionContextView).ToSingleton();
			injectionBinder.Bind<ViewReferences>().To(uiConfig.ViewReferences).ToSingleton();
			
			// Commands
			commandBinder.Bind<OpenAnimatedViewSignal>().To<OpenAnimatedViewCommand>();
            commandBinder.Bind<OpenBlurredAnimatedViewSignal>().To<OpenBlurredAnimatedViewCommand>();
			commandBinder.Bind<CloseAnimatedViewSignal>().To<CloseAnimatedViewCommand>();
			commandBinder.Bind<TeardownArenaSignal>().To<TeardownArenaCommand>();
            commandBinder.Bind<MoveAnimatedViewToCanvasSignal>().To<MoveAnimatedViewToCanvasCommand>();
			commandBinder.Bind<InitializeAnimatedViewSignal>().To<InitializeAnimatedViewCommand>();
			commandBinder.Bind<ResizeArenaSignal> ().To<ResizeArenaCommand> ();
            commandBinder.Bind<ExportGameLogSignal>().To<ExportGameLogCommand>();
            commandBinder.Bind<EmoteSignal>().To<EmoteCommand>();
            commandBinder.Bind<PlayFxSignal>().To<PlayFxCommand>();
            commandBinder.Bind<LogEventSignal>().To<LogEventCommand>();
            commandBinder.Bind<GameOverSignal>().To<CleanUpGameCommand>();

            // Mediators
            mediationBinder.Bind<ArenaView>().To<ArenaMediator>();
            mediationBinder.Bind<StartGameDialogView>().To<StartGameDialogMediator>();
            mediationBinder.Bind<EndGameDialogView>().To<EndGameDialogMediator>();
            mediationBinder.Bind<TutorialPopUpView>().To<TutorialPopUpMediator>();
            mediationBinder.Bind<InGameMenuView>().To<InGameMenuMediator>();
			mediationBinder.Bind<StartingHandView>().To<StartingHandMediator>();
            mediationBinder.Bind<OpponentHandController>().To<OpponentHandControllerMediator>();
            mediationBinder.Bind<PlayerHandController>().To<PlayerHandControllerMediator>();
            mediationBinder.Bind<PlayerControllerView>().To<PlayerControllerMediator>();
            mediationBinder.Bind<OpponentControllerView>().To<OpponentControllerMediator>();
            mediationBinder.Bind<TurnControllerView>().To<TurnControllerMediator>();
            mediationBinder.Bind<PlayerArenaController>().To<PlayerArenaControllerMediator>();
            mediationBinder.Bind<OpponentArenaController>().To<OpponentArenaControllerMediator>();
            mediationBinder.Bind<GameLogView>().To<GameLogMediator>();

            // Models
            injectionBinder.Bind<ArenaCards>().ToSingleton().CrossContext();
            injectionBinder.Bind<ArenaPlayers>().ToSingleton().CrossContext();

            // Services
            injectionBinder.Bind<CardEventService>().ToSingleton().CrossContext();
		}
	}	
}