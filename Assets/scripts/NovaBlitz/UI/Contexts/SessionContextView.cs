﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.mediation.impl;
using UnityStandardAssets.ImageEffects;
using DG.Tweening;
using NovaBlitz.UI;
using NovaBlitz.Game;

public class SessionContextView : ContextView, INovaContext
{
    [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
	[Inject] public ClientPrefs ClientPfefs {get;set;}

    public Camera CameraForUI;
	public Canvas mainCanvas;
	public Canvas overlayCanvas;
	public Canvas holdingCanvas;
	public Canvas overlayRaycastScrim;
    public RectTransform particleBounds;
	public Canvas MainCanvas { get { return this.mainCanvas;}}
	public Canvas OverlayCanvas { get { return this.overlayCanvas;}}
	public Canvas HoldingCanvas { get { return this.holdingCanvas;} }
    public RectTransform ParticleBounds { get { return this.particleBounds; } }
    public NovaBlurComponent NovaBlur;
	public UIConfiguration UIConfig {get; protected set;}
	public bool IsAlreadyBlurred { get{ return this.NovaBlur.IsAlreadyBlurred;}}

    void Awake()
	{
		context = new NovaBlitz.Game.SessionContext(this, true);
		context.Start ();
		GameObject.DontDestroyOnLoad(this.gameObject);
		this.UIConfig = Resources.Load<UIConfiguration>(NovaConfig.PATH_SESSCFG);
		InitializeUIConfiguration ();
		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
		{
			this.overlayRaycastScrim.gameObject.SetActive (false);
		}
		else
		{
			this.overlayRaycastScrim.gameObject.SetActive (true);
		}
    }

    public void BlurMainCanvas(bool isBlurred, View currentView)
    {
        this.NovaBlur.BlurMainCanvas(isBlurred, currentView);
	}

	void InitializeUIConfiguration()
	{
		this.UIConfig.ViewReferences.loadPrefabDictionary = new System.Collections.Generic.Dictionary<Type, string> {
			{ typeof(StartingHandView),"StartingHandPanel" },
			{ typeof(EndGameDialogView),"EndGameDialogPanel" },
			{ typeof(TutorialPopUpView),"TutorialPopUpDialogPanel" },
			{ typeof(InGameMenuView),"InGameMenuPanel" },
			{ typeof(StartGameDialogView),"StartGameDialogPanel" },
			{ typeof(GameLogView),"GameLog" },
			{ typeof(TwoTouchOverlayView),"TwoTouchOverlay" },
		};
		Debug.LogFormat ("Initialized Session Context Views: {0} entries", this.UIConfig.ViewReferences.loadPrefabDictionary.Count);
	}
}
