﻿using UnityEngine;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Maintains a list of views in the view state, the first one in the list is considered to be the owner
	/// of the ViewState
	/// </summary>
	public class ViewState
	{
		public List<AnimatedView> Views = new List<AnimatedView>();
	}

	/// <summary>
	/// Maintains a stack of View States and provides methods to Push and pop new states
	/// </summary>
	public class ViewStateStack
	{
		private Stack<ViewState> viewStates = new Stack<ViewState>();

		/// <summary>
		/// Clears all the viewstates out of the ViewStateStack. Useful for transitioning between UI / Arena etc.
		/// </summary>
		public void Clear()
		{
			Debug.Log("#ViewState#ClearingViewState!");
			this.viewStates.Clear();
		}

		/// <summary>
		/// Returns the number of ViewStates in the stack
		/// </summary>
		public int Count
		{
			get { return this.viewStates.Count;}
		}

		/// <summary>
		/// Called when a new blurred view is opened. This pushes all the existing ViewStates down in the stack and
		/// makes the new viewState the current one.
		/// </summary>
		public void PushViewState(AnimatedView view)
		{
			//Debug.Log("#ViewState#AddBlurredState: " + view.name);

			if(this.Count == 0)
			{
				this.viewStates.Push(new ViewState()); // Add a deafult viewstate so we can add a blurred one over it
			}

			ViewState newState = new ViewState();
			this.viewStates.Push(newState);
			newState.Views.Add(view);
		}

		/// <summary>
		/// If a blurred view has pushed a new ViewState onto the stack and you want to add
		/// another view to the view state so that it also appears above the blur, this method
		/// lets you add a view that will be pushed and popped when the owner of the viewState
		/// is closed or pushed down by a new state.
		/// </summary>
		public void AddViewToCurrentState(AnimatedView view)
		{
			ViewState currentState = null;
			if(this.viewStates.Count == 0)
			{
				currentState = new ViewState();
				this.viewStates.Push(currentState);
			}
			else
			{
				currentState = this.viewStates.Peek();
			}
			
			if(currentState.Views.Contains(view) == false)
			{
				currentState.Views.Add(view);
				Debug.Log("#ViewState#Add View to BlurredState: " + view.name + " owned by:" + currentState.Views.First().name);
			}
		}

        public View TryPeekViewState()
        {
            if (this.viewStates.Count > 0)
            {
                ViewState viewState = this.viewStates.Peek();
                View stateView = viewState.Views.FirstOrDefault();
                return stateView;
            }
            else
                return null;
        }

		/// <summary>
		/// When a view closes, compare it to the current ViewState to see if it owns the viewState.
		/// If it does, then we need to pop off the viewState to reveal the one below it. (if there is one)
		/// </summary>
		public bool TryToPopViewState(View closingView, out ViewState states)
		{
			//Debug.Log("#ViewState#Attempting Pop: " + closingView.name);
			if(this.viewStates.Count > 0 && closingView != null)
			{
				ViewState viewState = this.viewStates.Peek();
				View stateView = viewState.Views.FirstOrDefault();
				//Debug.Log("#ViewState#StateOwnedBy: " + ( stateView != null ? stateView.name : "null") + " and NumViewStates:" + this.Count);

				if(stateView == closingView)
				{
					//Debug.Log("#ViewState#Popped ViewState!: " + closingView.name);
					// Pop the view states
					states = this.viewStates.Pop();

					// Check to see if we've revealed a state by popping the current one
					ViewState revealedState = this.viewStates.Count > 0 ? this.viewStates.Peek() : null;

					// While we have a revealed state from the last Pop() operation...
					while(revealedState != null)
					{
						// Check and see if the view that owns this state is already closed...
						View revealedView = revealedState.Views.FirstOrDefault();
						if(revealedView != null && revealedView is AnimatedView)
						{
							if(((AnimatedView)revealedView).IsOpen == false)
							{
								// if the view is already closed... pop it off...
								this.viewStates.Pop();

								// and peek to see if there is a new revealed state
								revealedState = this.viewStates.Count > 0 ? this.viewStates.Peek() : null;
							}
							else
							{
								// Otherwise set the sentinel so we exit the loop
								revealedState = null;
							}
						}
						else
						{
							// Otherwise set the sentinel so we exit the loop
							revealedState = null;
						}
					}

					return true;
				}
			}
            states = null;
			return false;
		}

		/// <summary>
		/// Gets a reference to the current state.
		/// <summary>
		public ViewState GetCurrentState()
		{
			if(this.viewStates.Count > 0)
			{
				ViewState viewState = this.viewStates.Peek();
				return viewState;
			}

			return null;
		}
	}
}