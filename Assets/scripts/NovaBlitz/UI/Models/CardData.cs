﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public enum CardType
	{
		Undefined = 0,
		Unit = 1,
		Power = 2,
		Item = 3,
	}

	public enum CardAspect
	{
		Arcane = 0,
		Tech = 1,
		Divine = 2,
		Nature =3,
		Chaos = 4,
		NoAspect = 5
	}

	public enum KeywordEnum
	{
		NoKeyword,
		Armor,
		Evade,
		Resurgence,
		FirstStrike,
		DoubleAttack,
		DoubleBlock,
		Deathtouch,
		CantAttack,
		CantBlock,
        MustAttack,
        Flying,
		AntiAir,
		Regenerate,
		Overrun,
		Shield,
		Stealth,
		Initiative,
		Stunned,
		Poisoned,
		Defender,
		Retreat,
		Unblockable,
		Hexproof,
		Vanguard,
        LifeLink,
        LifeDrain,
        ActivatedAbility,
        AttackTrigger,
        BlockTrigger,
        SummonTrigger,
        LastBreathTrigger,
        InfiltrateTrigger,
        ExtraActivations,
        Variable,
        Frenzy,
        Syphon,
        Cowardly,
        NumberOfCombatPhases,
        Unplayable,
        Fight,
        Atk,
        Health,
        MaxEnergy,
        CurrentEnergy,
        EnergyCost,
        ActivationCost,
        Trick,
        WantsToAttack,
        OneShot,
        Hero

    }

	public enum CardRarity
	{
		Common=0,
		Rare=1,
		Epic=2,
		Legendary=3,
	}

	public enum CardSpeed
	{
		Immediate,
		Response
	}

    public abstract class CardData
    {
        public const string RED_OPEN = "<#FF8888>";
        public const string GREEN_OPEN = "<#00FF00>";
        public const string BLUE_OPEN = "<#00BBFF>";
        public const string GREY_OPEN = "<#888888>";
        public const string STRIKETHROUGH_OPEN = "<s>";
        public const string STRIKETHROUGH_CLOSE = "</s>";
        public const string COLOUR_CLOSE = "</color>";
        public const string SHORT_PARAGRAPH = "<margin=6em>";
        public const string SHORT_LAST_LINE = "<margin=7em>";

        protected CardData() { }
        public int CardID;
        public string ItemId { get { return CardID.ToString(); } }
        public int Cost;
        public int AspectIcons;
        public CardAspect Aspect;
        public CardRarity Rarity;
        public string NameLocId { get { return GameData.CARDNAME_DATA + CardID.ToString(); } }
        public string RulesLocId { get { return GameData.RULESTEXT_DATA + CardID.ToString(); } }
        public string SubtypeLocId { get { return GameData.SUBTYPE_DATA + _Subtype; } }
        public string Name
        {
            get
            {
                var locName = I2.Loc.ScriptLocalization.Get(GameData.CARDNAME_DATA + CardID.ToString());
                if (string.IsNullOrEmpty(locName))
                    return string.Empty;
                else
                    return locName;
            }
        }

        public string EffectText
        {
            get
            {
                var locRules = I2.Loc.ScriptLocalization.Get(GameData.RULESTEXT_DATA + CardID.ToString());
                if (string.IsNullOrEmpty(locRules))
                    return string.Empty;
                else
                    return locRules;
            }
        }
        public string LocalizedSearchableCardText;
        public string FxScript;
        public string ImagePath;
        public string ArtID;
        public string ArtistCredit = string.Empty;
        private int _SubtypeValue;
        public int SubtypeValue { get { return _SubtypeValue; } set { _SubtypeValue = value; string subtype; GameData.SubtypesById.TryGetValue(value, out subtype); Subtype = subtype; } }
        private string _Subtype;
        public string Subtype
        {
            get
            {
                var locSubtype = I2.Loc.ScriptLocalization.Get(GameData.SUBTYPE_DATA + _Subtype);
                if (string.IsNullOrEmpty(locSubtype))
                    return string.Empty;
                else
                    return locSubtype;
            }
            private set { _Subtype = value; }
        }

        // Unit-only data
        public int AttackPower;
        public int Health;
        public int? ActivationCost;

        public Dictionary<int, double> NewHotnessKeywords;

        abstract public CardType CardType { get; }


        public string GenerateCardText()
        {
            StringBuilder builder = new StringBuilder();
            List<StringBuilder> keywordsToDisplay = new List<StringBuilder>();
            foreach (var kvp in this.NewHotnessKeywords)
            {
                if (kvp.Value != 0 /*&& (int)kvp.Key < (int)KeywordEnum.InvisibleKeywordsBelowHere*/)
                {
                    // Show the keyword if its base value is >0
                    KeywordData keywordData = null;
                    if (GameData.KeywordsById.TryGetValue(kvp.Key, out keywordData) && (keywordData.Type == Messages.KeywordType.Condition || keywordData.Type == Messages.KeywordType.Skill))
                    {
                        // Add the number if the value is >0 and the number should be shown
                        string keywordName = I2.Loc.ScriptLocalization.Get(GameData.KEYWORD_DATA + keywordData.Name);

                        StringBuilder keywordText = new StringBuilder();
                        keywordText.Append(keywordData.Type == Messages.KeywordType.Skill ? CardData.BLUE_OPEN : CardData.RED_OPEN);
                        if (kvp.Value > 0 && keywordData.ShowNumber)
                        {
                            keywordText.Append(keywordName).Append(' ').Append(kvp.Value);
                        }
                        else
                            keywordText.Append(keywordName);
                        keywordText.Append(CardData.COLOUR_CLOSE);
                        keywordsToDisplay.Add(keywordText);
                    }
                }
            }
            string effectText = this.EffectText;
            if (keywordsToDisplay.Count > 0)
            {
                for (int i = 0; i < keywordsToDisplay.Count; i++)
                {
                    builder.Append(keywordsToDisplay[i]);
                    builder.Append(i < keywordsToDisplay.Count - 1 ? ", " : string.Empty);
                }
                if (!String.IsNullOrEmpty(effectText))
                {
                    builder.Append('\n');
                }
                //this.KeywordText = string.Join(", ", keywordsToDisplay.ToArray());
            }
            builder.Append(effectText);

            if (builder.Length > 90)
            {
                int textlength = 0;
                bool inBrackets = false;
                for (int i = 0; i < builder.Length; i++)
                {
                    if (builder[i] == '<')
                    {
                        inBrackets = true;
                        continue;
                    }
                    else if (builder[i] == '>')
                    {
                        inBrackets = false;
                        continue;
                    }
                    else if (!inBrackets)
                    {
                        textlength++;
                    }
                }
                if (textlength < 90)
                {
                    builder.Insert(0, CardData.SHORT_PARAGRAPH);
                }
                else
                {
                    inBrackets = false;
                    for (int i = builder.Length - 1; i > 0; i--)
                    {
                        if (builder[i] == '>')
                        {
                            inBrackets = true;
                            continue;
                        }
                        else if (builder[i] == '<')
                        {
                            inBrackets = false;
                            continue;
                        }
                        if (!inBrackets)
                        {
                            builder.Insert(i, CardData.SHORT_LAST_LINE);
                            break;
                        }
                    }
                }
            }
            else if (builder.Length > 0)
            {
                builder.Insert(0, CardData.SHORT_PARAGRAPH);
            }
            return builder.ToString();//this.KeywordText + "\n" + this.AbilityText;
        }

        public void SetSearchableCardText()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(this.Name).Append(' ');
            builder.Append(this.Subtype).Append(' ');
            if (this.NewHotnessKeywords != null)
            {
                foreach (var kvp in this.NewHotnessKeywords)
                {
                    KeywordData keywordData = null;
                    if (GameData.KeywordsById.TryGetValue(kvp.Key, out keywordData) && (keywordData.Type == Messages.KeywordType.Condition || keywordData.Type == Messages.KeywordType.Skill))
                    {
                        // Add the number if the value is >0 and the number should be shown
                        builder.Append(I2.Loc.ScriptLocalization.Get(GameData.KEYWORD_DATA + keywordData.Name)).Append(' ');
                    }
                }
            }
            builder.Append(this.EffectText).Append(' ');
            builder.Append(I2.Loc.ScriptLocalization.Get(this.CardType.ToString())).Append(' ');
            builder.Append(I2.Loc.ScriptLocalization.Get(this.Rarity.ToString())).Append(' ');
            builder.Append(I2.Loc.ScriptLocalization.Get(this.Aspect.ToString())).Append(' ');
            if (this.ActivationCost != null)
                builder.Append(I2.Loc.ScriptLocalization.Activated).Append(' ');
            //builder.Append(this.Cost);
            this.LocalizedSearchableCardText = builder.ToString();
        }

        protected static CardAspect ParseAspect(string aspectString)
        {
            switch (aspectString)
            {
                case "T":
                    return CardAspect.Tech;
                case "A":
                    return CardAspect.Arcane;
                case "C":
                    return CardAspect.Chaos;
                case "N":
                    return CardAspect.Nature;
                case "D":
                    return CardAspect.Divine;
            }
            Debug.Log("Aspect parse fail: " + aspectString);
            return CardAspect.NoAspect;
            //throw new ArgumentException("Unable to parse aspect string '" + aspectString + "'");
        }

        protected static CardRarity ParseRarity(string rarityString)
        {
            switch (rarityString)
            {
                case "C":
                case "":
                    return CardRarity.Common;
                case "R":
                    return CardRarity.Rare;
                case "E":
                    return CardRarity.Epic;
                case "L":
                    return CardRarity.Legendary;
            }

            Debug.Log("Rarity parse fail: " + rarityString);
            return CardRarity.Common;
        }

        protected void ParseCommonFields(CardDataContract cardDataContract)
        {
            if (this is UnitCard)
            {
                int.TryParse(cardDataContract.Atk, out this.AttackPower);
                int.TryParse(cardDataContract.Health, out this.Health);
            }

            this.CardID = cardDataContract.CardID;// int.Parse(cardDataContract.CardID);
            this.ArtID = cardDataContract.ArtID;
            this.Aspect = ParseAspect(cardDataContract.Aspect);
            this.AspectIcons = string.IsNullOrEmpty(cardDataContract.AspectIcons) ? 1 : cardDataContract.AspectIcons.Length;
            this.Cost = int.Parse(cardDataContract.Cost);
            this.Subtype = cardDataContract.Subtype;
            //newCard.Speed = (CardSpeed) Enum.Parse(typeof(CardSpeed), cardDataContract.Speed);;
            this.Rarity = ParseRarity(cardDataContract.Rarity);
            this.FxScript = cardDataContract.FxScript;
            this.ActivationCost = cardDataContract.ActivationCost;

            int subTypeValue;
            if (!String.IsNullOrEmpty(cardDataContract.Subtype) && GameData.SubtypesByName.TryGetValue(cardDataContract.Subtype, out subTypeValue))
            {
                this.SubtypeValue = subTypeValue;
            }

            this.NewHotnessKeywords = cardDataContract.Keywords;
            SetSearchableCardText();
            //this.DisplayCardText(cardDataContract);
        }
    }

    public class UnitCard : CardData
    {
        // Injected Dependencies
        //[Inject] public GameData gameData { get; set;}

		public override CardType CardType {get {return CardType.Unit;}}

		protected UnitCard(){}
		static public UnitCard Create(CardDataContract cardDataContract)
		{
			UnitCard newCard = new UnitCard();
            newCard.ParseCommonFields(cardDataContract);
			return newCard;
		}
	}

	public class PowerCard : CardData
	{
		public override CardType CardType {get {return CardType.Power;}}

		protected PowerCard(){}
		static public PowerCard Create(CardDataContract cardDataContract)
		{
			PowerCard newCard = new PowerCard();

            newCard.ParseCommonFields(cardDataContract);
            
            return newCard;
		}
	}

	public class ItemCard : CardData
	{
		public override CardType CardType {get {return CardType.Item;}}
		
		protected ItemCard(){}
		static public ItemCard Create(CardDataContract cardDataContract)
		{
			ItemCard newCard = new ItemCard();

            newCard.ParseCommonFields(cardDataContract);

			return newCard;
		}
	}

}

