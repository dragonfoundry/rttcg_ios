using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using Crosstales.BadWord;
using Messages;

namespace NovaBlitz.UI
{
	// CardItem is a card entry in DeckData's list of cards. It is a CardID and an index in the deck (and an aspect)
	public class CardItem
	{
		public CardItem(int cardID, int deckIndex, CardAspect aspect, CardType cardType, int cost ) 
		{ this.CardID = cardID; this.DeckIndex = deckIndex;  this.Aspect = aspect; this.CardType = cardType; this.Cost = cost; }
		
		public CardItem(CardData cardData, int deckIndex)
		{
			this.CardID = cardData.CardID;
			this.DeckIndex = deckIndex;
			this.Aspect = cardData.Aspect;
			//this.Name = cardData.Name;
			this.CardType = cardData.CardType;
			this.Cost = cardData.Cost;
		}
		public int CardID;
		public CardAspect Aspect;
		public int DeckIndex;
		public string Name { get { return I2.Loc.ScriptLocalization.Get(GameData.CARDNAME_DATA + CardID); } }
		public CardType CardType;
		public int Cost;
	}

	// DeckData is a Deck of cards (a list of CardItem's)
	public class DeckData 
	{
		public string ID;
        private string _Name;
		public string Name { get { return _Name; } set { _Name = BWFManager.ReplaceAll(value); } }
        public string DeckArt;
		//public int LegalDeckSize=40;
        public int MinDeckSize = 40;
        public int MaxDeckSize = 60;
        public int MaxOfEachCard=3;
		public List<CardItem> Cards = new List<CardItem>();
		public List<CardAspect> Aspects = new List<CardAspect>();
        public bool IsTutorialDeck = false;
        public GameFormat Format;
		public List<int> DeckList
		{
			get
			{
				List<int> deckList = Cards.Select(c=>c.CardID).ToList();
				return deckList;
			}
		}

        public DeckData(GameFormat format)
        {
            Format = format;
            if(Format == GameFormat.Draft)
            {
                MaxDeckSize = 30;
                MinDeckSize = 30;
                MaxOfEachCard = 30;
                Name = I2.Loc.ScriptLocalization.Draft_Deck;
            }
        }

        public bool TrimToInventory(Dictionary<int, int> OwnedCards, HashSet<int> legalCards)
        {
            //Debug.Log("trimming deck: " + Name);
            bool deckUpdated = false;
            Dictionary<int, int> deckByCount = new Dictionary<int, int>();
            for (int i = Cards.Count - 1; i >= 0; i--)
            {
                int cardId = Cards[i].CardID;
                int owned;
                int inDeck;
                if (!legalCards.Contains(cardId) || !OwnedCards.TryGetValue(cardId, out owned) || (deckByCount.TryGetValue(cardId, out inDeck) && inDeck >= owned))
                {
                    Cards.Remove(Cards[i]);
                    deckUpdated = true;
                }
                else
                {
                    deckByCount[cardId] = inDeck + 1;
                }
            }
            return deckUpdated;
        }

		public void QueryAspects()
		{
			this.Aspects = this.Cards.GroupBy(grp => grp.Aspect).Select(grp => grp.First().Aspect).ToList<CardAspect>();
		}

        public bool IsLegalDeckSize
        {
            get { return Cards.Count >= MinDeckSize && Cards.Count <= MaxDeckSize; }
        }

        public bool IsLegalAspects
        {
            get { return Aspects.Count <= 2; }
        }

        public bool IsLegalMaxCards
        {
            get { 
                foreach(var card in Cards)
                {
                    if (Cards.Where(cd => cd.CardID == card.CardID).Count() > MaxOfEachCard)
                        return false;
                }
                return true; }
        }

        public bool IsLegalDeck
        {
            get
            {
                return IsTutorialDeck || (IsLegalDeckSize && IsLegalAspects && IsLegalMaxCards);
            }
        }

        public void AddCard(int cardId, int deckIndex)
        {
            CardData cardData = null;
            if (GameData.CardDictionary.TryGetValue(cardId, out cardData))
            {
                CardAspect cardAspect = cardData.Aspect;

                // Create a new CardItem and add it to the DeckData
                CardItem cardItem = new CardItem(cardId, deckIndex, cardAspect, cardData.CardType, cardData.Cost);
                this.Cards.Add(cardItem);
            }
            else
            {
                Debug.LogWarningFormat("Could not find cardID: {0} in Card GameData for {1}", cardId, Name);
            }
        }

        public void SetPreconName()
        {
            CardAspect aspect = CardAspect.NoAspect;
            if(Cards != null || Cards.Count> 0)
            {
                aspect = Cards[0].Aspect;
            }

            switch (aspect)
            {
                case CardAspect.Arcane:
                    Name = I2.Loc.ScriptLocalization.arcane_starter;
                    break;
                case CardAspect.Tech:
                    Name = I2.Loc.ScriptLocalization.tech_starter;
                    break;
                case CardAspect.Divine:
                    Name = I2.Loc.ScriptLocalization.divine_starter;
                    break;
                case CardAspect.Nature:
                    Name = I2.Loc.ScriptLocalization.nature_starter;
                    break;
                case CardAspect.Chaos:
                    Name = I2.Loc.ScriptLocalization.chaos_ctarter;
                    break;
                default:
                    Name = I2.Loc.ScriptLocalization.Deck;
                    break;
            }
        }

        public void SetDeckArt()
        {
            //Set avatar key art
            CardData selectedCard = null;
            foreach (var cardId in this.DeckList)
            {
                CardData card;
                if (GameData.CardDictionary.TryGetValue(cardId, out card))
                {
                    if (selectedCard == null || (card.Rarity > selectedCard.Rarity || (card.Rarity == selectedCard.Rarity && card.Cost > selectedCard.Cost)))
                        selectedCard = card;
                }
            }
            if (selectedCard != null)
                this.DeckArt = selectedCard.ArtID;
        }

        public DeckData Clone()
        {
            DeckData data = new DeckData(this.Format);
            data.ID = this.ID;
            data.DeckArt = this.DeckArt;
            data.Aspects = this.Aspects.ToList();
            data.Name = this.Name;
            data.Cards = new List<CardItem>();
            foreach(var card in this.Cards)
            {
                CardData cardData;
                if(GameData.CardDictionary.TryGetValue(card.CardID, out cardData))
                {
                    data.Cards.Add(new CardItem(cardData, card.DeckIndex));
                }
            }
            return data;
        }

        public Dictionary<int,int> ReturnSimpleDeck()
        {
            Dictionary<int, int> deck = new Dictionary<int, int>();
            foreach(var card in this.Cards)
            {
                if (deck.ContainsKey(card.CardID))
                    deck[card.CardID]++;
                else
                    deck.Add(card.CardID, 1);
            }
            return deck;
        }

        public override string ToString()
        {
            return string.Format("{0} cards: [{1}]", Cards.Count, string.Join(",", Cards.Select(c => c.CardID.ToString()).ToArray()));
        }
    }

	/// <summary>
	/// The "CardListTransform" is a static helper class who's methods transform a DeckData into a collection of 
	/// CardListItems grouped by cardID. The CardLIstItems also contain a list of references back to the 
	/// CardItems in the DeckData. This is what collapses several of a single type of card into one row in the 
	/// cardList.
	/// </summary>
	public class CardListItem
	{
		public CardListItem(int cardID, List<CardItem> cards)
		{
			this.CardID = cardID;
			this.Cards = cards;
            if(cards != null)
            {
                this.Count = cards.Count;
            }
            else
            {
                this.Count = 1;
            }
		}
		public int CardID;
		public int Count;
		public List<CardItem> Cards = new List<CardItem>();
	}

	/// <summary>
	/// "View" class that transforms a flat list of cards from a deck into a collapsed list of CardListItems that
	/// contains groups of cards that have the same ID such that each unique card only has one entry in the list
	/// and a "Count" property for how many of that card are present in the Deck.
	/// </summary>
	public class CardListTransform
	{
		/// <summary>
		/// Gets the collapsed list of CardListItems from a flat deck of cards.
		/// </summary>
		public static List<CardListItem> GetList(DeckData deck)
		{
			return CardListTransform.Enumerate(deck).ToList();
		}

		/// <summary>
		/// Returns the index of a card in the transformed CardListItem list
		/// </summary>
		public static int IndexOfCard(DeckData deck, CardData cardData)
		{
			return CardListTransform.Enumerate(deck).Select((card,index) => new {card, index}).FirstOrDefault(o=>o.card.CardID == cardData.CardID).index;
		}

		/// <summary>
		/// Indexs the of a card in the transformed CardListItem list as well as the CardListItem at that index.
		/// </summary>
		public static int IndexOfCard(DeckData deck, CardData cardData, ref CardListItem foundItem)
		{
			IEnumerable<CardListItem> cardList = CardListTransform.Enumerate(deck);
			int listIndex = -1;

			var result = cardList.Select((card,index) => new {card, index}).FirstOrDefault(o=>o.card.CardID == cardData.CardID);

			if(result != null)
			{
				listIndex = result.index;
				foundItem = cardList.Where( item=>item.CardID == cardData.CardID).FirstOrDefault();
			}

			return listIndex;
		}

		/// <summary>
		/// Helper method that returns an IEnumerable collection of the CardListItem list for further Linq Queries.
		/// </summary>
		private static IEnumerable<CardListItem> Enumerate(DeckData deck)
		{
			// Initialize distinctCards to be all the CardID's in the deck grouped by cardID
			IEnumerable<int> distinctCards = deck.Cards.OrderBy(card => card.Cost).ThenBy(cd => cd.CardType).ThenBy(cx=>cx.Name).GroupBy(c=>c.CardID).Select(group => group.Key);
			
			// Next join those distinctCards's with the DeckData's CardItems and create CardListItem groups with their associated CardItems
			IEnumerable<CardListItem> cardListItemData = distinctCards.GroupJoin(deck.Cards, dcID => dcID, cards => cards.CardID, (dc,c) 
			                                              => new CardListItem(dc, c.ToList<CardItem>()));								  
			// Return the grouped/counted cardListItem Datas
			return cardListItemData;
		}
	}
}
