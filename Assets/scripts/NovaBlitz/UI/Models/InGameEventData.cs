﻿using System;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using Messages;

namespace Assets.scripts.NovaBlitz.UI.Models
{
    class InGameEventData
    {
    }

    public class EventScheduleItem // For deserializing
    {

        public string EventType {
            get { return _EventType.ToString(); }
            set { if (Enum.IsDefined(typeof(EventTypes), value)) { _EventType = (EventTypes)Enum.Parse(typeof(EventTypes), value); } } }
        public string Format {
            get { return _Format.ToString(); }
            set { if (Enum.IsDefined(typeof(GameFormat), value)) { _Format = (GameFormat)Enum.Parse(typeof(GameFormat), value); } } }
        public string Day {
            get { if (_IsEveryDay) { return GameEventConstants.ALL_DAYS; } else return _WeekDay.ToString(); }
            set { if (value == GameEventConstants.ALL_DAYS) { _IsEveryDay = true; }
                else { if (Enum.IsDefined(typeof(DayOfWeek), value)) { _WeekDay = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), value); } } } }
        public string Hour {
            get { return _StartHour.ToString(); }
            set { int.TryParse(value, out _StartHour); } }
        public string Dur {
            get { return _DurMinutes.ToString(); }
            set { int.TryParse(value, out _DurMinutes); } }
        public string Recur { get; set; }
        public string Stat { get; set; }
        public DateTime ValidFrom { get; set; }
        
        public EventTypes _EventType;
        public GameFormat _Format;
        public DayOfWeek _WeekDay;
        public bool _IsEveryDay;
        public int _StartHour;
        public int _DurMinutes;
    }

    public class GameEventConstants
    {
        public const string ALL_DAYS = "ALL";
        public const int ENTRY_TIME_LIMIT = 60;

    }

    public enum EventTypes
    {
        NoSize = 0,
        Small = 1,
        Medium = 2,
        Large = 3,
        Huge = 4,
        Monthly = 100,
    }
}
