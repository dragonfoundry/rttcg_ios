﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using PlayFab.ClientModels;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
    public class ClientPrefs
    {
        private const string clientPrefsFileName = "/clientPrefs.dat";

        public string SessionTicket { get; set; }
        public UserAccountInfo UserAccountInfo;
        public bool IsWideArena 
        { 
            get
            {
                return this.IsScreen16x9 && (this.ArenaLayout == ArenaLayout.Mobile);// || this.ArenaLayout == ArenaLayout.NoLayout);
            }
        }

        public bool IsScreen16x9
		{
			get
			{ 
				// 16f/9 is 1.777777... but for some small reoslutions sometimes float rounds down in the decimls
				// so we use 1.7 here knowing that there's some slop in anyting past the 100ths place

                // 4:3 is 1.333333 so we'll say the screen is 16:9 for the sake of our layout if the ratio is greater than 1.35,
                // if it's less than 1.35 we'll cosider it NOT 16:9

				bool is16by9 =  Camera.main.aspect >=  1.35f ;
				//Debug.LogError("IsScreen16By9 returned:" + is16by9 + " aspect is:" + Camera.main.aspect + " ArenaLayout:" + this.ArenaLayout);
				return is16by9;
			}
		}
        public ArenaLayout ArenaLayout {get;set;}
        public System.Random random = new System.Random();
        public string SteamName { get; set; }
        public string SteamId { get; set; }
        public float LastLoginTimestamp { get; set; }

        // Network
        public List<DNSDataContract> DNSoptions { get; set; }

        public void ResetClientPrefs()
        {
            SessionTicket = null;
            UserAccountInfo = null;
            DNSoptions = null;
            SteamName = null;
            SteamId = null;
            this.Save();
        }

        public string DeviceUniqueIdentifier
        {
            get
            {
#if UNITY_EDITOR
                return SystemInfo.deviceUniqueIdentifier + "editor";
#else
                return SystemInfo.deviceUniqueIdentifier;
#endif
            }
        }
		
		/// <summary>
		/// Synchronous loading of the Player preferences from the application persistent path
		/// </summary>
		public void Load() 
		{
            this.ArenaLayout = ArenaLayout.Desktop;

            FileInfo fileInfo = new FileInfo(Application.persistentDataPath + clientPrefsFileName);
			if (fileInfo.Exists) {
				
				StreamReader sr = fileInfo.OpenText();
				string contents = sr.ReadToEnd();
				Debug.Log("Loaded client prefs from file: " + contents);
				sr.Close();
				SessionTicket = contents;
                // Default to "Desktop" for Arena Layout if there isn't a PlayerPref stored for it
                this.ArenaLayout = (ArenaLayout)PlayerPrefs.GetInt("ArenaLayout", (int)ArenaLayout.Desktop);
			}

            // Force mobile ArenaLayouts to "Mobile"
            #if UNITY_IOS || UNITY_ANDROID
                   this.ArenaLayout = ArenaLayout.Mobile;
            #endif

            Debug.Log(" ArenaLayout: " + this.ArenaLayout);
        }

		/// <summary>
		/// Synchronous saving of the player preferences to the application persistent path
		/// </summary>
		public void Save() 
		{
			Debug.Log("saving client prefs: ArenaLayout: " + this.ArenaLayout);
			FileInfo fileInfo = new FileInfo(Application.persistentDataPath + clientPrefsFileName);
			StreamWriter writer = fileInfo.CreateText();
			writer.Write(SessionTicket);
			writer.Close();
			PlayerPrefs.SetInt("ArenaLayout", (int)this.ArenaLayout);
		}
	}
}
