﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;


namespace NovaBlitz.UI
{
	[System.Serializable]
	public class ViewReferences
	{
		//public View[] ViewPrefabs;

		public Dictionary<System.Type,string> loadPrefabDictionary = new Dictionary<System.Type, string>();

		private Dictionary<System.Type,View> viewDictionary = new Dictionary<System.Type,View>();
		//private Dictionary<System.Type,View> prefabDictionary = new Dictionary<System.Type,View>();
		
		[Inject] public TeardownUISignal TeardownUISignal {get;set;}
		[Inject] public ResourceMap ResourceMap {get;set;}		
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public ViewStateStack ViewStateStack { get; set; }
		[Inject] public INovaContext NovaContext {get;set;}

        public View Instantiate(System.Type viewType, Transform parent = null)
		{ 
			// If our PrefabList and PrefabDictionary are out of sync...
			/*if(this.ViewPrefabs.Length != this.prefabDictionary.Count)
			{
				//... re-initialize the dictionary from the list.
				this.prefabDictionary.Clear();
				for(int i=0;i<this.ViewPrefabs.Length;i++)
				{
					this.prefabDictionary.Add(this.ViewPrefabs[i].GetType(), this.ViewPrefabs[i]);
				}
			}*/
			//Debug.LogFormat ("Instantiating {0}", viewType.Name);

			View newView;
			if(!this.viewDictionary.TryGetValue(viewType, out newView) || newView == null)
			{
				// Instantiate a view from its prefab
				/*if(this.prefabDictionary.ContainsKey(viewType) == false)
				{
					// Throw an exception if we can't find a prefab for the viewtype
					throw new System.ArgumentException("Coud not find a prefab reference for view type '" + viewType.Name + "' in the ViewReferences prefab list.");
				}
				View prefab = this.prefabDictionary[viewType];
				newView = GameObject.Instantiate(prefab);*/

				// Instantiate from Resources.Load
				if(this.loadPrefabDictionary.ContainsKey(viewType) == false)
				{
					// Throw an exception if we can't find a prefab for the viewtype
					throw new System.ArgumentException(string.Format("Coud not find a prefab reference for view type '{0}' in the ViewReferences prefab list.", viewType.Name));
				}
				string location = this.loadPrefabDictionary[viewType];
				//Debug.LogFormat("Instantiating {0} from location {1}", viewType.Name, location);
				newView = (View)GameObject.Instantiate(Resources.Load(location,viewType));
			}

			//RectTransform prx = prefab.GetComponent<RectTransform>();
			RectTransform vrx = newView.GetComponent<RectTransform>();

			// Assign the parent transform if one was provided
			if (null != parent)
			{
				newView.transform.AttachToParent(parent,0);
			}
			else
			{
				newView.transform.localPosition = Vector3.zero;
				newView.transform.localScale = Vector3.one;
			}

			if (null != vrx)
			{
				NormalizeView(vrx);
			}

			// Store the newly instantiated view in the viewDictionary
			this.viewDictionary[viewType] = newView;

			//Debug.LogFormat ("View Dictionary has {0} entries", this.viewDictionary.Count);
            //newView.gameObject.SetActive(false);
			return newView;
		}
		
		public View Get(System.Type t)
		{
			// Retrieve the view from the view dictionary
			View v;
			this.viewDictionary.TryGetValue(t, out v);

			// If it's not found...
			if(v == null)
			{
				// ...attempt to locate one in the scene...
				Object [] existing = GameObject.FindObjectsOfType(t);
				if(existing.Length > 0)
				{
					/// ...and add it to the view dictionary for fast reference in the future.
					v = (View)existing[0]; 
					this.viewDictionary[t] = v;
					RectTransform vrx = v.GetComponent<RectTransform>();
					NormalizeView(vrx);
				}
			}

			// Return the view if one was found, null otherwise.
			return v;
		}

		public T Get<T>() where T : View
		{
			return Get(typeof(T)) as T;
		}
		
		public void DestroyAllViews()
		{
			Debug.Log("Destroying all views");

            View progressView = null;
            foreach (var kvp in this.viewDictionary)
            {
                if (kvp.Value == null || kvp.Value is ProgressLoadingScreenView)
                {
                    progressView = kvp.Value;
                    continue;
                }
                GameObject.Destroy(kvp.Value.gameObject);
            }
			this.viewDictionary.Clear();

            // Re-add the reference to the progress loading screen, as we're not destroying it.
            if (progressView != null)
                this.viewDictionary[typeof(ProgressLoadingScreenView)] = progressView;

            this.TeardownUISignal.Dispatch();
        }

        public void CloseAllViews()
        {
            foreach(var view in viewDictionary.Values)
            {
                this.CloseAnimatedViewSignal.Dispatch(view.GetType());
            }
            this.ViewStateStack.Clear();
            NovaContext.BlurMainCanvas(false, null);
        }

		protected void NormalizeView(RectTransform viewRX)
		{
			viewRX.localScale = Vector3.one;
			viewRX.localPosition = Vector3.zero;
			viewRX.sizeDelta = Vector2.zero;
		}
	}
}
