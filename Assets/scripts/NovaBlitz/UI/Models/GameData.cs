﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;
using Messages;

namespace NovaBlitz.UI
{
	public class GameData 
	{
        // Global Card Dictionary of all possible cards
        public HashSet<int> LegalCards = new HashSet<int>();
		public static Dictionary<int,CardData> CardDictionary = new Dictionary<int, CardData>();
        public static Dictionary<int, KeywordData> KeywordsById = new Dictionary<int, KeywordData>();
        public static Dictionary<string, KeywordData> KeywordsByName = new Dictionary<string, KeywordData>(StringComparer.OrdinalIgnoreCase);
        public static Dictionary<KeywordEnum, KeywordData> KeywordsByEnum = new Dictionary<KeywordEnum, KeywordData>();
        public static int OneShotId;
        public static Dictionary<int, string> SubtypesById = new Dictionary<int, string>();
        public static Dictionary<string, int> SubtypesByName = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        public Dictionary<string, string> ArtistCredits = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

		// Store the "Catalog" Cache indexed by item id
		public Dictionary<string, NovaCatalogItem> ProductCache = new Dictionary<string, NovaCatalogItem>(StringComparer.OrdinalIgnoreCase);
        public Dictionary<int, NovaCatalogItem> CardProductCache = new Dictionary<int, NovaCatalogItem>();

        // flag for whether we've cached the catalog yet PB HACK
        public bool _catalogCached = false;
        public string CatalogVersion { get; set; }

        // Store the "Store" Cache: grouped by store id and indexed by item id
        public Dictionary<string, Dictionary<string,StoreData>> StoreCache = new Dictionary<string, Dictionary<string, StoreData>>(StringComparer.OrdinalIgnoreCase);

        public static Dictionary<string, CurrencyType> CurrencyDictionary = Enum.GetValues(typeof(CurrencyType)).Cast<CurrencyType>().ToDictionary(t => t.ToString(), t => t, StringComparer.OrdinalIgnoreCase);


        // Store the IAP Products Cache
        public List<UnityIAPProduct> IAPProductCache = new List<UnityIAPProduct>();

        // Store the preconstructed decks
        public static List<DeckData> PreconstructedDecks = new List<DeckData>();
        public DeckListDataContract PreconstructedDataContract { get; set; }
        public Dictionary<int, HashSet<CardAspect>> AIDecks = new Dictionary<int, HashSet<CardAspect>>();
        public Dictionary<int, string> AIDeckStrings = new Dictionary<int, string>();

        // Store the current tournament event data
        public static Dictionary<string, TournamentEventData> TournamentDataCache = new Dictionary<string, TournamentEventData>(StringComparer.OrdinalIgnoreCase);
        public static Dictionary<DateTime, TournamentEventData> TournamentSchedule = new Dictionary<DateTime, TournamentEventData>();

        // Store the tutorial data
        public List<TutorialDataContract> TutorialList = new List<TutorialDataContract>();
        public Dictionary<int, TutorialDataContract> TutorialDictionary = new Dictionary<int, TutorialDataContract>();
        public Dictionary<int, List<TutorialCombatData>> TutorialCombatDictionary = new Dictionary<int, List<TutorialCombatData>>();

        // Store the avatar data
        public Dictionary<int, AvatarDataContract> AvatarDictionary = new Dictionary<int, AvatarDataContract>();
        public Dictionary<string, AvatarDataContract> AvatarByItemIdDictionary = new Dictionary<string, AvatarDataContract>(StringComparer.OrdinalIgnoreCase);
        public List<AvatarDataContract> FreeAvatars = new List<AvatarDataContract>();
        public string DefaultAvatarArtId { get; set; }

        // Store the card back data
        public static Dictionary<int, CardBackDataContract> CardBackDictionary = new Dictionary<int, CardBackDataContract>();
        public Dictionary<string, CardBackDataContract> CardBackByItemIdDictionary = new Dictionary<string, CardBackDataContract>(StringComparer.OrdinalIgnoreCase);
        public List<CardBackDataContract> FreeCardBacks = new List<CardBackDataContract>();
        public string DefaultCardBackArtId { get; set; }


        // Ranking
        public int[] RankThresholds;
		public Dictionary<int, IntRange> RankBrackets = new Dictionary<int, IntRange>();
        public int leagueThreshold = -25;
        public bool isLeagueUnlocked = false;
        public int eventsThreshold = -18;
        public bool isEventsUnlocked = false;

        // Achievements
        public Dictionary<string, AchievementData> AchievementDataCache = new Dictionary<string, AchievementData>();

        // ObjectPool Reference
        public UpdatedPrizeView PrizePrefab;
        public CardFrontController ArenaCardPrefab;
        public CardBackController CardBackPrefab;

        // Playfab Game Version
        public int VersionMajor;
		public int VersionMinor;
		public int VersionPatch;
        public string VersionLabel { get { return string.Format("{0}.{1}.{2}", this.VersionMajor, this.VersionMinor, this.VersionPatch); } }

        // Download URIs
        //public string ClientCardFileKey;
        //public string ClientDataFileKey;
        //public string ClientTextFileKey;

        // Information URIs
        public string url_Terms = "http://www.dragonfoundry.com/terms";
        public string url_Privacy = "http://www.dragonfoundry.com/privacy";
        public string url_EULA = "http://www.dragonfoundry.com/eula";
        public string url_Credits = "http://www.novablitz.com/credits/";
        public string url_Help = "http://www.novablitz.com/learn-to-play/";

        // Gameplay Session
        public CurrentGameData CurrentGameData { get; set; }

        // Card Ability constants
        /*public static int VANGUARD;
        public static int DOUBLE_BLOCK;
        public static int FLYING;
        public static int EXTRA_ACTIVATIONS;*/

        // Friend string constants

        public const string IS_FRIEND_REQUEST = "isRequest";
        

        // Localization constants
        public const string KEYWORD_DATA = "KeywordData/";
        public const string REMINDER_DATA = "ReminderData/";
        public const string SUBTYPE_DATA = "SubtypeData/";
        public const string RULESTEXT_DATA = "CardRulesData/";
        public const string CARDNAME_DATA = "CardNameData/";
        

        public static string SearchEnergy;
        public static string SearchMana;
        public static string SearchCost;
        public static string SearchAttack;
        public static string SearchStrength;
        public static string SearchHealth;
        public static string SearchOwned;
        public static string SearchActivated;
        public static string SearchArtist;
        public static string SearchExtra;
        
        public static void SetSearchKeys()
        {
            SearchEnergy = I2.Loc.ScriptLocalization.Energy;
            SearchMana = I2.Loc.ScriptLocalization.Mana;
            SearchCost = I2.Loc.ScriptLocalization.Cost;
            SearchAttack = I2.Loc.ScriptLocalization.Attack;
            SearchStrength = I2.Loc.ScriptLocalization.Strength;
            SearchHealth = I2.Loc.ScriptLocalization.Health;
            SearchOwned= I2.Loc.ScriptLocalization.Owned;
            SearchActivated = I2.Loc.ScriptLocalization.Activated;
            SearchArtist = I2.Loc.ScriptLocalization.Artist;
            SearchExtra = I2.Loc.ScriptLocalization.Extra;
        }
    }

    public enum SearchKey
    {
        Energy,
        Mana,
        Cost,
        Attack,
        Strength,
        Health,
        Owned,
        Activate,
    }

    public class CurrentGameData
    {
        public System.Guid GameGuid;
        public int OpponentId;
        public int OpponentAvatar;
        public int OpponentCardBack;
        public int OpponentStartingHealth;
        public string PlayerName;
        public string OpponentName;
        public int PlayerId;
        public int PlayerAvatar;
        public int PlayerCardBack;
        public int PlayerStartingHealth;
        public string OpponentPlayFabId;
        public Dictionary<string, int> PlayerStats;
        public Dictionary<string, int> OpponentStats;
        public GameFormat GameFormat;
        public Dictionary<int, List<TutorialCombatData>> TutorialAttackData;
        public PlayerData PlayerPlayerData;
        public PlayerData OppPlayerData;
        //public Guid PlayerGuid;
    }
}
