﻿using UnityEngine;
using System.Collections.Generic;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
    public class ArtPositioningData
    {
        public ArtPositioningKey Key;
        public Dictionary<string, ArtPosition> PositionsDictionary = new Dictionary<string, ArtPosition>();
        
        public ArtPositioningData()
        {
            
        }
        
        public ArtPositioningData(ArtPositioningKey key)
        {
            this.Key = key;
        }
        
        static public ArtPositioningData LoadFromStreamingAssets(ArtPositioningKey key)
        {
            ArtPositioningDataContract contract = ArtPositioningDataContract.LoadPositions(key);
            if(contract != null)
            {
                return contract.ToModel();
            }
            
            return null;
        }
        
        public ArtPositioningDataContract GetDataContract()
        {
            ArtPositioningDataContract contract = new ArtPositioningDataContract();
            contract.Key = this.Key;
            contract.Positions = new List<ArtPositionDataContract>();
            
            foreach(var kvp in this.PositionsDictionary)
            {
                contract.Positions.Add(new ArtPositionDataContract(kvp.Value.ArtId, kvp.Value.Scale, kvp.Value.Position));
            }
            
            return contract;
        }
        
        public ArtPosition Get(string artId)
        {
            ArtPosition pos = null;
            this.PositionsDictionary.TryGetValue(artId, out pos);
            return pos;
        }
    }
    
    public class ArtPosition
    {
        public string ArtId;
        public Vector3 Scale;
        public Vector2 Position;
        
        public ArtPosition() {}
        public ArtPosition(string artId, float[] vals)
        {
            this.ArtId = artId;
            this.Scale = new Vector3(vals[0], vals[1], vals[2]);
            this.Position = new Vector2(vals[3], vals[4]);
        }
    }
}
