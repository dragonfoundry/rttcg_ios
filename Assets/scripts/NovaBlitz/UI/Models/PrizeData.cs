using UnityEngine;
using UnityEngine.UI;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.Game
{
	public abstract class PrizeData
	{
		public int Quantity;
		public virtual string Name { get { return string.Empty; } }
		public virtual Sprite Sprite { get { return null; } }
        public virtual PrizeSource PrizeSource { get; set; }
	}

	public class CurrencyPrizeData : PrizeData
	{
		public CurrencyType CurrencyType;
		public override string Name { get { return CurrencyUtils.GetName(this.CurrencyType, 0); } }
		public override Sprite Sprite { get { return CurrencyUtils.GetSprite(this.CurrencyType); } }
	}

	public class ItemPrizeData : PrizeData
	{
		public string ItemID;
		public override string Name { get { return null != this.ItemData ? this.ItemData.DisplayName : this.ItemID; } }
		public NovaCatalogItem ItemData {get;set;}
        public string ArtId { get; set; }
    }

    public class CardPrizeData : PrizeData
    {
        public NovaBlitz.UI.CardData CardData;
    }
}
