using UnityEngine;
using System;
using LinqTools;
using System.Globalization;
using System.Collections.Generic;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using Messages;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
	public class PlayerProfile
	{
        [Inject] public GameData GameData { get; set; }

        // DeckData
        public List<DeckData> DeckList = new List<DeckData>();
        public DeckListDataContract DeckListDataContract { get; set; }

        public DeckData CurrentDeck = null;
		public DeckData DraftDeck = null;
        public DeckData LeagueDeck = null;
        public DeckData EventDeck = null;
        public const int DRAFT_DECK_SIZE = 30;
        public int NumDeckSlots = 20;
		public CardFilterParams CurrentCardFilter;

		// Store
		public Wallet Wallet {get;set;}
		public SmartPackInfo UnopenedPack;					// packID -> cardIDs
		public Dictionary<int, int> OwnedCards = new Dictionary<int, int>();			// cardID -> quantity
		public HashSet<string> OwnedKeys = new HashSet<string>();							// itemID
        public HashSet<string> OwnedAvatars = new HashSet<string>();
        public HashSet<string> OwnedCardBacks = new HashSet<string>();
        public HashSet<string> OwnedPlaymats = new HashSet<string>();
        public HashSet<string> BoughtStoreItems = new HashSet<string>();
        public Dictionary<string, NovaInventoryItem> Inventory = new Dictionary<string, NovaInventoryItem>(StringComparer.OrdinalIgnoreCase);
        public HashSet<int> NewCards = new HashSet<int>();

        public DateTime? GemAnnuityExpiration = null;
        public DateTime? CreditAnnuityExpiration = null;
        public DateTime? NanoBotAnnuityExpiration = null;
        public DateTime? SmartPackAnnuityExpiration = null;
        public DateTime? SteamAnnuityExpiration = null;

        // Friends
        // public Dictionary<string, NovaFriend> NovaFriendDictionary = new Dictionary<string, NovaFriend>(StringComparer.OrdinalIgnoreCase);
        //public HashSet<string> FriendList = new HashSet<string>();
        public Dictionary<string,NovaBlitzFriend> FriendListData = new Dictionary<string, NovaBlitzFriend>();
        public HashSet<string> BlockList = new HashSet<string>();
        //public Dictionary<string, FriendRequestData> PendingFriendRequests = new Dictionary<string, FriendRequestData>();
        public bool IsFriendsListInitialized = false;
		public Dictionary<string,List<ChatMessage>> ChatLogs = new Dictionary<string,List<ChatMessage>>(); // <playfabID,ChatMessages>
        //public GetFriendsListResult friendListPlayFabResult = null;
        public List<ChatMessage> GlobalChatLog = new List<ChatMessage>();

        // Friend Challenges 
        public Dictionary<string, FriendChallenge> FriendChallenges = new Dictionary<string, FriendChallenge>(StringComparer.OrdinalIgnoreCase);
		public string SelectedChallengeID;

        // readonly rating data
        public Dictionary<int, IntRange> RankBrackets = new Dictionary<int, IntRange>();

        public int CurrentRank { get { return PlayerProfileUtils.GetRank(this.UserStatistics); } }
        public int CurrentRating { get { return PlayerProfileUtils.GetRating(this.UserStatistics); } }
        public int RatingSliderMin;
        public int RatingSliderMax;

        // Readonly Player Data
        public DateTime DailyRewardTimestamp;
        public OnboardingProgress OnboardingProgress = new OnboardingProgress();
        public QuestData MostRecentCompletedQuest = null;
        //public HashSet<string> OffersViewedThisSession = new HashSet<string>();
        public RatingObject RatingObject = new RatingObject();
        public DraftProgress DraftProgress = new DraftProgress();
        public TournamentData DraftData = new TournamentData();
        public TournamentData ConstructedData = new TournamentData();
        public TournamentData TournamentData = new TournamentData();
        public TournamentEventData CurrentTournament = new TournamentEventData();

        public DateTime OffersLastSeen;
        public DateTime PackOpenerLastSeen;

        public UiHintData UiHintData = new UiHintData();
        public DateTime UiHintDataLastSaved = DateTime.MinValue;

        public bool HasUnlockedAllDecks { get {
                return OnboardingProgress != null
                    && (OnboardingProgress.RemainingOnboardingQuests == null
                        || OnboardingProgress.RemainingOnboardingQuests.Count == 0
                        || OnboardingProgress.RemainingOnboardingQuests.Max() < -5);
            } }

        // All data loaded on startup
        
        public bool IsFinishedDataLoading { get; set; }
        public bool IsAllDataLoaded {
            get { return 
                    isPlayerDataLoaded 
                    && isClientCardFileLoaded 
                    && isClientDataFileLoaded 
                    && isClientTextFileLoaded 
                    && isPlayerSettingsLoaded 
                    && isAllCatalogsLoaded
                    && isAllObjectPoolsCreated; }
            private set { isAllObjectPoolsCreated = isPlayerDataLoaded = isPlayerSettingsLoaded = isClientCardFileLoaded = isClientDataFileLoaded = isClientTextFileLoaded = isAllCatalogsLoaded = value; } }
        public bool isPlayerDataLoaded = false;
        public bool isPlayerSettingsLoaded = false;
        public bool isClientCardFileLoaded = false;
        public bool isClientDataFileLoaded = false;
        public bool isClientTextFileLoaded = false;
        public bool isAllCatalogsLoaded = false;
        public bool isAllObjectPoolsCreated = false;
        public bool? isAgeGatePassed = null;
        public int isInitializingGlobalChat = 0;
        private uint _NumberOfGlobalChatShards;
        public uint NumberOfGlobalChatShards { get { return _NumberOfGlobalChatShards; } set { _NumberOfGlobalChatShards = value; SetGlobalChatRoom(); } }
        public bool isChatLogLoaded = false;
        public bool isChatLogNeedsSaving = false;


        public Guid GameToRejoin = Guid.Empty;

        public string PlayFabId { get { return MetaProfile != null ? MetaProfile.PlayFabID : null; }
            set
            {
                if(MetaProfile != null)
                    MetaProfile.PlayFabID = value;
                uint toSet;
                if (!string.IsNullOrEmpty(value) && value.Length >= 8)
                {
                    if(uint.TryParse(value.Substring(value.Length - 8), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out toSet))
                    {
                        Debug.Log("Set chat range");
                        PlayFabIdUint = toSet;
                    }
                }
                SetGlobalChatRoom();
            }
        }
        private uint PlayFabIdUint = unchecked((uint)UnityEngine.Random.Range(int.MinValue, int.MaxValue));
        private uint GlobalChatNumber = 0;
        public string GlobalChatRoom { get { return string.Format("global_{0}", GlobalChatNumber); } }

        private void SetGlobalChatRoom()
        {
            GlobalChatNumber = PlayFabIdUint / (uint.MaxValue / Math.Max(1, NumberOfGlobalChatShards));
            Debug.LogFormat("Global chat room: {0}", GlobalChatNumber);
        }

        // Ad tracking. DO NOT RESET THIS.
        public bool isAdTrackingEnabled = false;

        // Last menu screen viewed
        public Type LastMenuScreenViewed;
        public GameFormat LastPlayScreenFormat;

        public RateGameTrackingData RateGameTrackingData = new RateGameTrackingData();

        // Stats
        public Dictionary<string,int> UserStatistics = new Dictionary<string, int>();
        public string RatingMonth
        {
            get
            {
                return PlayerProfileUtils.RatingKey;
            }
        }

        // Game Log Tracking
        public List<GameLog> GameLogs = new List<GameLog>();

        // Publicly visible info for any player
        public MetaProfile MetaProfile  = new MetaProfile();
		public GetPlayerCombinedInfoResultPayload LoginInfoResult { get; set; }

        public PlayerData PlayerData { get {
                string avatarArtId = string.Empty;
                string cardBackArtId = string.Empty;
                AvatarDataContract avContract;
                if (!string.IsNullOrEmpty(this.MetaProfile.AvatarID) && this.GameData.AvatarByItemIdDictionary.TryGetValue(this.MetaProfile.AvatarID, out avContract))
                {
                    avatarArtId = avContract.ArtId;
                }
                CardBackDataContract cbContract;
                if (!string.IsNullOrEmpty(this.MetaProfile.CardBackID) && this.GameData.CardBackByItemIdDictionary.TryGetValue(this.MetaProfile.CardBackID, out cbContract))
                {
                    cardBackArtId = cbContract.ArtId;
                }
                int qps;
                this.Wallet.Currencies.TryGetValue(Messages.CurrencyType.QP, out qps);

                var Data = new PlayerData(this.MetaProfile.PlayFabID, this.MetaProfile.Name, this.UserStatistics, this.GameData.RankBrackets,
                    avatarArtId, cardBackArtId, qps,
                    null, Messages.GameFormat.NoGame, 0, 0, 0, -1, 0, 0, false, null, 0);

                return Data;
            }
        }

        public PlayerProfile()
		{
            ResetProfile();
        }


        public void ResetProfile()
        {
            DeckList.Clear();
            DeckListDataContract = null;
            CurrentDeck = null;
            DraftDeck = null;
            LeagueDeck = null;
            EventDeck = null;
            NumDeckSlots = 20;
            CurrentCardFilter = null;

            Wallet = new Wallet();
            UnopenedPack = null;
            OwnedCards.Clear();
            OwnedKeys.Clear();
            OwnedAvatars.Clear();
            OwnedCardBacks.Clear();
            OwnedPlaymats.Clear();
            Inventory.Clear();
            BoughtStoreItems.Clear();
            NewCards.Clear();
            //FriendList.Clear();
            FriendListData.Clear();
            BlockList.Clear();
            //PendingFriendRequests.Clear();
            IsFriendsListInitialized = false;
            ChatLogs.Clear();
            //friendListPlayFabResult = null;
            FriendChallenges.Clear();
            SelectedChallengeID = null;
            RatingSliderMin = 0;
            RatingSliderMax = 0;

            DailyRewardTimestamp = DateTime.MinValue;
            OffersLastSeen = DateTime.MinValue;
            PackOpenerLastSeen = DateTime.MinValue;
            //OffersViewedThisSession.Clear();
            OnboardingProgress = new OnboardingProgress();
            RatingObject = new RatingObject();
            DraftProgress = new DraftProgress();
            DraftData = new TournamentData();
            ConstructedData = new TournamentData();
            TournamentData = new TournamentData();
            CurrentTournament = new TournamentEventData();
            /*isPlayerDataLoaded = false;
            isPlayerSettingsLoaded = false;
            isClientCardFileLoaded = false;
            isClientDataFileLoaded = false;
            isClientTextFileLoaded = false;
            isAllCatalogsLoaded = false;*/
            isChatLogLoaded = false;
            isChatLogNeedsSaving = false;
            IsFinishedDataLoading = false;
            IsAllDataLoaded = false;
            GameToRejoin = Guid.Empty;

            LastMenuScreenViewed = null;

            GameLogs.Clear();
            UserStatistics.Clear();
            MetaProfile = new MetaProfile();
            PlayFabIdUint = unchecked((uint)UnityEngine.Random.Range(int.MinValue, int.MaxValue));
            LoginInfoResult = null;
        }

		public DeckData FindDeck(string id)
		{
			if (string.IsNullOrEmpty(id))
				return null;

			foreach (DeckData deck in DeckList)
			{
				if (id == deck.ID)
					return deck;
			}
			return null;
		}

		public DeckListDataContract GetDeckListDataContract()
		{
            if(!IsAllDataLoaded) { return null; }
			DeckListDataContract deckListDataContract = new DeckListDataContract();

			foreach(DeckData deck in this.DeckList)
			{
				DeckDataContract deckDataContract = this.GetDeckDataContract(deck);
				deckListDataContract.Decks.Add(deckDataContract);
			}
            
            return IsAllDataLoaded ? deckListDataContract : null;
		}

		public DeckDataContract GetDeckDataContract(DeckData deckData)
		{
			DeckDataContract deckDataContract = new DeckDataContract();
			foreach(CardItem cardItem in deckData.Cards)
			{
				DeckListItemDataContract cardDataContract = this.GetCardDataContract(cardItem);
				deckDataContract.Cards.Add(cardDataContract);
			}
			deckDataContract.Id = deckData.ID;
			deckDataContract.Name = deckData.Name;
			return deckDataContract;
		}

		private DeckListItemDataContract GetCardDataContract(CardItem cardItem)
		{
			DeckListItemDataContract cardDataContract = new DeckListItemDataContract { Id = cardItem.CardID };
			return cardDataContract;
		}

		public NovaBlitzFriend GetFriend(string playfabID)
		{
            if (string.IsNullOrEmpty(playfabID))
                return null;
            NovaBlitzFriend friend;
            this.FriendListData.TryGetValue(playfabID, out friend);
			return friend;
		}

		public bool IsChallengeIncoming(string playfabID)
		{
			return this.FriendChallenges.Keys.Any(id => id == playfabID);
        }

        public void AddCard(int cardId)
        {
            if (OwnedCards.ContainsKey(cardId))
            {
                OwnedCards[cardId] += 1;
                if (OwnedCards[cardId] <= 3)
                    NewCards.Add(cardId);
            }
            else
            {
                OwnedCards.Add(cardId, 1);
                NewCards.Add(cardId);
            }
        }

        public IEnumerable<StoreData> GetPurchasableAnnuities()
        {
            List<StoreData> items;
            Dictionary<string, StoreData> store;
            if(this.GameData.StoreCache.TryGetValue(NBEconomy.STORE_MAIN, out store))
            {
                items = store.Values.Where(item => item.tags != null && item.tags.Contains("annuity")).ToList();
                foreach(var item in items.ToList())
                {
                    NovaInventoryItem inventoryItem;
                    if(Inventory.TryGetValue(item.ItemId, out inventoryItem) && (!inventoryItem.Expiration.HasValue || (inventoryItem.Expiration.Value - DateTime.UtcNow).TotalDays >= 5))
                    {
                        items.Remove(item);
                    }
                }
                items.OrderBy(item => item.VirtualPrices.Count > 0 ? item.VirtualPrices.First().Value : 0);

                StoreData packsItem;
                if (store.TryGetValue(NovaIAPService.SMARTPACKS_01, out packsItem))
                {
                    items.Add(packsItem);
                }
                StoreData draftsItem;
                if (store.TryGetValue(NovaIAPService.DRAFTS_01, out draftsItem))
                {
                    items.Add(draftsItem);
                }
            }
            else
            {
                items = new List<StoreData>();
            }
            return items;
        }

        public List<StoreData> GetCurrencyOffers(int maxToReturn, CurrencyType currency)
        {
            List<StoreData> data = new List<StoreData>();

            NovaInventoryItem inventoryItem;
            StoreData storeData;
            Dictionary<string, StoreData> store;
            GameData.StoreCache.TryGetValue(NBEconomy.STORE_MAIN, out store);
            switch (currency)
            {
                case CurrencyType.CR:
                    // show credits_03, credits_01 and either the annuity, or the credits_02
                    if (!this.Inventory.TryGetValue(NovaIAPService.ANNUITY_CREDIT, out inventoryItem))
                    {
                        if (store.TryGetValue(NovaIAPService.ANNUITY_CREDIT, out storeData))
                            data.Add(storeData);
                    }
                    else
                    {
                        if (store.TryGetValue(NovaIAPService.CREDITS_02, out storeData))
                            data.Add(storeData);
                    }
                    if (store.TryGetValue(NovaIAPService.CREDITS_03, out storeData))
                        data.Add(storeData);
                    if (store.TryGetValue(NovaIAPService.CREDITS_01, out storeData))
                        data.Add(storeData);
                    break;
                case CurrencyType.NC:
                    // show nanobots_03, nanobots_01 and either the annuity, or the nanobots_02
                    if (!this.Inventory.TryGetValue(NovaIAPService.ANNUITY_NANOBOT, out inventoryItem))
                    {
                        if (store.TryGetValue(NovaIAPService.ANNUITY_NANOBOT, out storeData))
                            data.Add(storeData);
                    }
                    else
                    {
                        if (store.TryGetValue(NovaIAPService.NANOBOTS_02, out storeData))
                            data.Add(storeData);
                    }
                    if (store.TryGetValue(NovaIAPService.NANOBOTS_03, out storeData))
                        data.Add(storeData);
                    if (store.TryGetValue(NovaIAPService.NANOBOTS_01, out storeData))
                        data.Add(storeData);
                    break;
                case CurrencyType.NG:
                    // show three, depending on previous purchase history
                    if (this.Inventory.TryGetValue(NovaIAPService.GEMS_06, out inventoryItem) 
                        || this.Inventory.TryGetValue(NovaIAPService.GEMS_05, out inventoryItem) 
                        || this.Inventory.TryGetValue(NovaIAPService.GEMS_04, out inventoryItem)
                        || this.Inventory.TryGetValue(NovaIAPService.GEMS_03, out inventoryItem))
                    {
                        if (store.TryGetValue(NovaIAPService.GEMS_04, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_05, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_06, out storeData))
                            data.Add(storeData);
                    }
                    else if(this.Inventory.TryGetValue(NovaIAPService.GEMS_02, out inventoryItem))
                    {
                        if (store.TryGetValue(NovaIAPService.GEMS_03, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_04, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_05, out storeData))
                            data.Add(storeData);
                    }
                    else if (this.Inventory.TryGetValue(NovaIAPService.GEMS_01, out inventoryItem))
                    {
                        if (store.TryGetValue(NovaIAPService.GEMS_02, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_03, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_04, out storeData))
                            data.Add(storeData);
                    }
                    else
                    {
                        if (store.TryGetValue(NovaIAPService.GEMS_01, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_02, out storeData))
                            data.Add(storeData);
                        if (store.TryGetValue(NovaIAPService.GEMS_03, out storeData))
                            data.Add(storeData);
                    }
                    break;
            }
            return data;
        }

        public List<StoreData> GetOffers(int maxToReturn, bool addPurchasable)
        {
            List<StoreData> offers = new List<StoreData>();
            // add daily card offers (if you own less than maxToReturn of that card)
            int creditsOffers = 0;
            if (this.OnboardingProgress != null && this.OnboardingProgress.DailyOffers != null)
            {
                foreach (var offer in this.OnboardingProgress.DailyOffers.OrderBy(offer => offer.Currency).ThenBy(offer => offer.IsWeeklyCard ? 0 : 1).ThenBy(offer => offer.Price))
                {
                    if (offer.Currency == CurrencyType.RM)
                    {
                        NovaInventoryItem item;
                        Dictionary<string, StoreData> store;
                        StoreData storeData;
                        if (offers.Count < maxToReturn && !this.Inventory.TryGetValue(offer.ItemId, out item)
                            && GameData.StoreCache.TryGetValue(NBEconomy.STORE_OFFER, out store)
                            && store.TryGetValue(offer.ItemId, out storeData))
                        {
                            offers.Add(new StoreData
                            {
                                ItemId = storeData.ItemId,
                                StoreId = NBEconomy.STORE_OFFER,
                                UnityPurchasingProduct = storeData.UnityPurchasingProduct,
                                tags = storeData.tags,
                                VirtualPrices = storeData.VirtualPrices,//Vnew Dictionary<string, uint> { { offer.Currency.ToString(), offer.Price } },
                                IsWeeklyCard = offer.IsWeeklyCard,
                                ExpiryTime = offer.ExpiryTime,
                            });
                        }
                    }
                    else if(offers.Count < maxToReturn && creditsOffers < maxToReturn -1)
                    {
                        int cardId;
                        int owned;
                        if (!int.TryParse(offer.ItemId, out cardId) || !this.OwnedCards.TryGetValue(cardId, out owned) || owned < 3)
                        {
                            offers.Add(new StoreData
                            {
                                ItemId = offer.ItemId,
                                StoreId = NBEconomy.ITEMCLASS_CARD,
                                VirtualPrices = new Dictionary<string, uint> { { offer.Currency.ToString(), offer.Price } },
                                IsWeeklyCard = offer.IsWeeklyCard,
                                ExpiryTime = offer.ExpiryTime,
                            });
                            creditsOffers++;
                        }
                    }
                }
            }
            if(addPurchasable && offers.Count < maxToReturn)
            {
                offers.AddRange(GetPurchasableAnnuities().Take(maxToReturn - offers.Count));
            }
            return offers;
        }
    }
	
	public class NovaBlitzFriend
	{
		public NovaBlitzFriend(NovaFriend friendInfo, int chatUserStatus, string defaultAvatar)
		{
			this.FriendData = friendInfo;
			this.ChatUserStatus = chatUserStatus;
            this.AvatarArtId = defaultAvatar;
            this.AvatarId = 0;
            /*if(string.IsNullOrEmpty(FriendInfo.DisplayName))
            {
                FriendInfo.DisplayName = string.Format("?:{0}",FriendInfo.PlayFabId);
            }*/
            //isSteamFriend = friendInfo.IsSteamFriend;
        }
		public NovaFriend FriendData;
		public int ChatUserStatus;
		public bool IsBlocked { get; set; }
        public int AvatarId { get; set; }
        public string AvatarArtId { get; set; }
        private string AvatarCatalogId { get { return "avatar_" + AvatarId; } }
        //public bool isSteamFriend { get; set; }

        // Online friends
        // requests
        // offline friends
        // pending
        public int SortOrder { get
            {
                return IsBlocked ? 11 : FriendData.IsRequest ? (ChatUserStatus > 0 ? 15 : 14) : FriendData.IsPending ? 12 : ChatUserStatus > 0 ? 16 : 13;
            } }
    }

    public class RateGameTrackingData
    {
        public RateGameTrackingData()
        {
            LastTimeRatingPanelSeen = DateTime.MinValue;
            ListedVersion = string.Empty;
            ListedMajorVersion = 0;
            HasRatedListedVersion = false;
            TimesSeen = 0;
            IsRankUpSeen = false;
            IsPurchaseSeen = false;
            IsWinStreakSeen = false;
            IsEventEndSeen = false;
            IsDraftQuestSeen = false;
        }

        public DateTime LastTimeRatingPanelSeen { get; set; }
        public string ListedVersion { get; set; }
        public int ListedMajorVersion { get; set; }
        public bool HasRatedListedVersion { get; set; }
        public int TimesSeen { get; set; }
        public bool IsRankUpSeen { get; set; }
        public bool IsPurchaseSeen { get; set; }
        public bool IsWinStreakSeen { get; set; }
        public bool IsEventEndSeen { get; set; }
        public bool IsDraftQuestSeen { get; set; }

    }
}