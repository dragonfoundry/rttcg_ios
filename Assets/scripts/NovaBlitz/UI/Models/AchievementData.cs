using System.Collections.Generic;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class AchievementData
	{
		public string Name;
        public string ArtId;
		public bool IsCumulative;
		public bool IsDescending;
		public int[] LevelThresholds;
		public Dictionary<int, IntRange> LevelBrackets;
        public int oldScore { get; set; }
        public int newScore { get; set; }
        public int oldLevel { get; set; }
        public int newLevel { get; set; }
    }
}
