using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using System;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class KeywordData
	{
        public int Id;
        public string Name;
        public Messages.KeywordType Type;
		public bool ShowNumber;

		
		protected KeywordData(){}
		static public KeywordData Create(KeywordDataContract keywordDataContract)
		{
			KeywordData newKeyword = new KeywordData ();
			/*List<string> names = Enum.GetNames (typeof(KeywordEnum)).OfType<string>().ToList();

			if (!names.Contains(keywordDataContract.Keyword, StringComparer.OrdinalIgnoreCase))
				Debug.LogError(string.Format("Keyword {0} not implemented!", keywordDataContract.Keyword));
            else
           		newKeyword.Keyword = (KeywordEnum)Enum.Parse(typeof(KeywordEnum), keywordDataContract.Keyword, true);*/

            newKeyword.Id = keywordDataContract.Id;
            newKeyword.Name = keywordDataContract.Name;
            newKeyword.Type = keywordDataContract.Type;
			newKeyword.ShowNumber = keywordDataContract.ShowNumber;
			return newKeyword;
		}
	}
}

