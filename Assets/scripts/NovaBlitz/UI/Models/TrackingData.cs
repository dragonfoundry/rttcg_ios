﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using ProtoBuf;
using Messages;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
    [ProtoContract]
    public class TrackingData
    {
        [ProtoMember(1)]  public Dictionary<int, int> tracking_stats = new Dictionary<int, int>();
        [ProtoMember(2)]  public Dictionary<int, int> tutorial_time_s = new Dictionary<int,int>();
        [ProtoMember(10)] public DateTime LastPurchaseDate { get; set; }
        [ProtoMember(11)] public DateTime InstallDate { get; set; }

        [JsonIgnore] public Dictionary<int, int> session_stats = new Dictionary<int, int>();
        [JsonIgnore] public DateTime GameStartTime { get; set; }
        
        private Dictionary<TrackingStat, SessionStat> SessionDict = new Dictionary<TrackingStat, SessionStat>
        {
            {TrackingStat.AllTime_PlayMins, SessionStat.Sess_PlayMins },
            {TrackingStat.AllTime_Played_Games, SessionStat.Sess_Played_Games },
            {TrackingStat.AllTime_Won_Games, SessionStat.Sess_Won_Games },

            {TrackingStat.AllTime_Played_Casual, SessionStat.Sess_Played_Casual },
            {TrackingStat.AllTime_Played_Draft, SessionStat.Sess_Played_Draft },
            {TrackingStat.AllTime_Played_League, SessionStat.Sess_Played_League },
            {TrackingStat.AllTime_Played_Event, SessionStat.Sess_Played_Event },

            {TrackingStat.AllTime_Won_Casual, SessionStat.Sess_Won_Casual },
            {TrackingStat.AllTime_Won_Draft, SessionStat.Sess_Won_Draft },
            {TrackingStat.AllTime_Won_League, SessionStat.Sess_Won_League },
            {TrackingStat.AllTime_Won_Event, SessionStat.Sess_Won_Event },

            {TrackingStat.AllTime_Open_CR_BasicPack, SessionStat.Sess_Open_CR_BasicPack },
            {TrackingStat.AllTime_Open_SP_SmartPack, SessionStat.Sess_Open_SP_SmartPack },
            {TrackingStat.AllTime_Open_NG_SmartPack, SessionStat.Sess_Open_NG_SmartPack },
            {TrackingStat.AllTime_Open_NG_MegaPack, SessionStat.Sess_Open_NG_MegaPack },

            {TrackingStat.AllTime_CardsScrapped, SessionStat.Sess_CardsScrapped },
            {TrackingStat.AllTime_CardsCrafted, SessionStat.Sess_CardsCrafted },
        };



        public void SetValue(TrackingStat stat, int val)
        {
            tracking_stats[(int)stat] = val;
            SessionStat session;
            if (SessionDict.TryGetValue(stat, out session))
            {
                session_stats[(int)session] = val;
            }
        }

        public int GetValue(TrackingStat stat)
        {
            int value;
            tracking_stats.TryGetValue((int)stat, out value);
            return value;
        }

        public void UpdateValue(TrackingStat stat, int valToAdd)
        {
            int current;
            tracking_stats.TryGetValue((int)stat, out current);
            tracking_stats[(int)stat] = valToAdd + current;

            SessionStat session;
            if (SessionDict.TryGetValue(stat, out session))
            {
                int currentSession;
                tracking_stats.TryGetValue((int)stat, out currentSession);
                session_stats[(int)session] = valToAdd + currentSession;
            }
        }

        public void SetDate(TrackingDate date)
        {
            switch(date)
            {
                case TrackingDate.Install_Date:
                    InstallDate = DateTime.UtcNow;
                    break;
                case TrackingDate.Purchase_Date:
                    LastPurchaseDate = DateTime.UtcNow;
                    break;
                default:
                    break;
            }
        }
    }

    // tracks
    // game start
    // game end

    public enum TrackingStat
    {
        NoStat = 0,
        // game start; also track for game length
        AllTime_Played_Games = 1,
        AllTime_Played_Casual = 2,
        AllTime_Played_Draft = 3,
        AllTime_Played_League = 4,
        AllTime_Played_Event = 5,
        AllTime_Played_Practice = 6,
        // game end
        AllTime_Won_Games = 11,
        AllTime_Won_Casual = 12,
        AllTime_Won_Draft = 13,
        AllTime_Won_League = 14,
        AllTime_Won_Event = 15,
        AllTime_Won_Practice = 15,
        AllTime_PlayMins = 94,
        Time_S_GamePlay_Since_Install = 96,
        Time_S_Tutorial_Step = 95,
        Credits_Earned = 50,
        Gems_Earned = 52,
        QualifierPoints_Earned = 56,
        SmartPacks_Earned = 58,

        // set these from data too (but also check stored data to see if this is already set)
        Prev_Mega_Credits = 80,
        Prev_Mega_NanoBots = 81,
        Prev_Mega_SmartPacks = 82,

        // on entry (also track currency)
        Total_Entries_Draft = 45,
        Total_Entries_League = 46,
        Total_Entries_Event = 47,

        // crafting - on confirm craft/scrap
        NanoBots_Earned = 54,
        NanoBots_Spent = 55,
        Has_Crafted = 85,
        AllTime_CardsScrapped = 76,
        AllTime_CardsCrafted = 77,

        // Pack opening (also need to track balances)
        AllTime_Open_CR_BasicPack = 70,
        AllTime_Open_SP_SmartPack = 72,
        AllTime_Open_NG_SmartPack = 74,
        AllTime_Open_NG_MegaPack = 75,

        // set when share is clicked
        Has_FB_Share = 84,

        // Set on session start
        Total_Sessions = 90,

        // set on any virtual currency transaction
        Credits_Spent = 51,
        Gems_Spent = 53,
        QualifierPoints_Spent = 57,
        SmartPacks_Spent = 59,

        // On purchase completed successfully
        Has_Paid = 83,
        Total_IAP_USD = 93,
    }


    public enum TrackingDate
    {
        NoDate = 0,
        Install_Date = 1,
        Purchase_Date = 2,
    }

    public enum SessionStat
    {
        NoStat = 0,
        Sess_PlayMins = 94,
        Sess_Played_Games = 201,
        Sess_Won_Games = 202,

        Sess_Played_Casual = 204,
        Sess_Played_Draft = 205,
        Sess_Played_League = 206,
        Sess_Played_Event = 207,

        Sess_Won_Casual = 210,
        Sess_Won_Draft = 211,
        Sess_Won_League = 212,
        Sess_Won_Event = 213,

        Sess_Open_CR_BasicPack = 215,
        Sess_Open_SP_SmartPack = 216,
        Sess_Open_NG_SmartPack = 219,
        Sess_Open_NG_MegaPack = 220,

        Sess_CardsScrapped = 221,
        Sess_CardsCrafted = 222,

    }

    public enum SpendArea
    {
        NoArea = 0,
        DraftEntry,
        LeagueEntry,
        Avatar,
        CardBack,
        Packs,
        TournamentEntry,
        Crafting,
        Scrap,
        Rewards,
    }
}
