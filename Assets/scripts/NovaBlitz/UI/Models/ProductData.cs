using UnityEngine.Purchasing;
using System.Collections.Generic;
using Messages;

namespace NovaBlitz.Economy
{
	public class ProductData
	{
        // Currently mirrors PlayFab fields, but is meant to be service-independent.
        public string ItemInstanceID;
        public string ItemId;
		public string ItemClass;
		public string CatalogVersion;
		public string DisplayName;
		public string Description;
		public bool CanBecomeCharacter;
		public bool IsStackable;
        public string ItemImageURL;

		public string CustomData;
		public string[] Tags;
		public string[] GrantedIfHas;

		// Use bundle to grant virtual currency upon acquisition.
		public ConsumableData Consumable;
		public ContainerData Container;
		public BundleData Bundle;

		// Real money is specified in US cents, using code "RM".
		public Dictionary<CurrencyType, uint> VirtualPrices;
	}

    public class StoreData
    {
		public string StoreId;
		public string ItemId;
		public Dictionary<string, uint> VirtualPrices;
        //public ProductData ProductData;
        public Product UnityPurchasingProduct;
        public System.DateTime ExpiryTime;
        public string tags;
        public int NumberOwned;
        public bool IsWeeklyCard;
    }

	public class ConsumableData
	{
		// Period is the time elapsed beyond acquisition upon which the item will be discarded, specified in seconds.
		public uint? UsageCount;
		public uint? UsagePeriod;
		public string UsagePeriodGroup;
	}

	public class ContainerData
	{
		public string KeyItemID;
		public string[] ItemContents;
		public string[] ResultTableContents;
		public Dictionary<string, uint> VirtualCurrencyContents;
	}

	public class BundleData
	{
		public string[] Items;
		public string[] ResultTables;
		public Dictionary<string, uint> VirtualCurrencies;
	}
}