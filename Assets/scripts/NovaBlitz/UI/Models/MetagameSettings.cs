﻿using UnityEngine;
using System.Collections;

namespace NovaBlitz.UI
{
	public class MetagameSettings 
	{
		public string GameTitleId;
		public string PhotonChatAppID;
        public bool IsTargetingDev;

    }
}
