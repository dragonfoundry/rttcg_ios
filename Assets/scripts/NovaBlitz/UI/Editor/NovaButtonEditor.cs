﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;


[CustomEditor(typeof(NovaButton))]
public class NovaButtonEditor : Editor 
{
	
	
	/// <summary>
	/// Main GUI of the editor
	/// </summary>
	public override void OnInspectorGUI()
    {
		//this.DrawDefaultInspector();
		
		NovaButton novaButton = (NovaButton)this.serializedObject.targetObject;
		
		novaButton.interactable = EditorGUILayout.Toggle("Interactable",novaButton.interactable);
		
		novaButton.targetGraphic = (Graphic)EditorGUILayout.ObjectField("Sprite Graphic",novaButton.targetGraphic,typeof(Graphic), true);
		novaButton.LabelText = (TextMeshProUGUI)EditorGUILayout.ObjectField("Label Text", novaButton.LabelText, typeof(TextMeshProUGUI),true);
		novaButton.Image = (Image)EditorGUILayout.ObjectField("Image",novaButton.Image, typeof(Image),true);
		
		SpriteState spriteState = novaButton.spriteState;

		EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Disabled Sprite", GUILayout.Width(EditorGUIUtility.labelWidth));
			spriteState.disabledSprite = (Sprite)EditorGUILayout.ObjectField( spriteState.disabledSprite, typeof(Sprite), true);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Hilighted Sprite", GUILayout.Width(EditorGUIUtility.labelWidth));
			spriteState.highlightedSprite = (Sprite)EditorGUILayout.ObjectField( spriteState.highlightedSprite, typeof(Sprite), true);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Pressed Sprite", GUILayout.Width(EditorGUIUtility.labelWidth));
			spriteState.pressedSprite = (Sprite)EditorGUILayout.ObjectField( spriteState.pressedSprite, typeof(Sprite), true);
		EditorGUILayout.EndHorizontal();
		
		novaButton.spriteState = spriteState;
		
		ColorBlock colorBlock = novaButton.colors;
		colorBlock.normalColor = EditorGUILayout.ColorField("Normal Color", novaButton.colors.normalColor);
		colorBlock.highlightedColor = EditorGUILayout.ColorField("Highlight Color", novaButton.colors.highlightedColor);
		colorBlock.pressedColor = EditorGUILayout.ColorField("Pressed Color", novaButton.colors.pressedColor);
		colorBlock.disabledColor = EditorGUILayout.ColorField("Disabled Color", novaButton.colors.disabledColor);
		
		EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Space();
			if(GUILayout.Button("Apply Default Colors"))
			{
				colorBlock.normalColor = new Color(1f,150f/255f,0f,1f); // Orange
				colorBlock.highlightedColor = new Color(1f,1f,55f/255f,1f); // Yellow
				colorBlock.pressedColor = colorBlock.highlightedColor; // Yellow (2)
				colorBlock.disabledColor = new Color(0.5f,0.5f,0.5f,0.5f); // transparent gray
			}
		EditorGUILayout.EndHorizontal();
		
		novaButton.colors = colorBlock;
		
		EditorGUILayout.Space();
		
		SerializedProperty onClickProp = this.serializedObject.FindProperty("OnClick");
		EditorGUILayout.PropertyField(onClickProp);
		this.serializedObject.ApplyModifiedProperties();
		
		// Copy the NovaButton OnClick event into the base button onClick
		novaButton.onClick = novaButton.OnClick;
	}
}
