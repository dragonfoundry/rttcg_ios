﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;  
using Newtonsoft.Json;
using UnityEngine.Audio;
using LinqTools;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.UI;
using UnityEngine.UI;
using System.IO;

[CustomEditor(typeof(CardArtImage))]
public class CardArtImageEditor : Editor 
{
    private RectMask2D mask;
    private ArtPositioningKey key;
    private GUIStyle labelStyle;
    private bool isArtPickerVisible = false;
    private string filterText = string.Empty;
    private RectTransform rectTrans;
    private Vector3 scrollPosition;
    private List<CardArtListItem> cardArtList;
    private Queue<CardArtListItem> loadTextureQueue = new Queue<CardArtListItem>();
    private List<CardArtPositionUpdate> positionUpdates = new List<CardArtPositionUpdate>();
    private int ticksSinceLastTextureLoaded = 0;
    private Texture defaultCardArtTexture;
    private Vector2 initialPosition;
    private Vector3 initialScale;
    private GUISkin inspectorSkin;
    public ArtPositioningData PositionData
    {
        get 
        {
            if(CardArtImage.PositionData == null)
            {
                return null;
            }
            ArtPositioningData positionData = null;
            CardArtImage.PositionData.TryGetValue(this.key, out positionData);
            return positionData;
        }
    }
    
    
    /// <summary>
    /// This is the data that is stored to populate items in the art picker
    /// </summary>
    private class CardArtListItem
    {
        public CardArtListItem(string artId, Texture texture = null) { this.ArtId = artId; this.Texture = texture;}
        public string ArtId;
        public Texture Texture;
    }
    
    /// <summary>
    /// This is the data record that stores the working set of unsaved position updates
    /// </summary> 
    private class CardArtPositionUpdate
    {
        public string ArtId;
        public Vector2 Position;
        public Vector3 Scale;
    }
    
    /// <summary>
	/// When the CardArtImage editor is loaded, Initialize it.
	/// </summary>
	private void OnEnable()
	{
        this.mask = ((CardArtImage)this.serializedObject.targetObject).gameObject.GetComponentInParent<RectMask2D>();
        this.labelStyle = new GUIStyle(EditorStyles.boldLabel);
        this.labelStyle.normal.textColor = Color.white;
        this.labelStyle.active.textColor = Color.white;
        this.labelStyle.hover.textColor = Color.white;
        this.labelStyle.normal.background = this.MakeTex(1,1,new Color(0,0,0,0.5f));
        
        // Look up the CardArtImage component and its rectTransform
        CardArtImage cardArtImage = (CardArtImage)this.serializedObject.targetObject;
        this.rectTrans = cardArtImage.rectTransform;
        
        this.defaultCardArtTexture = this.labelStyle.normal.background;
        
        // Scan the card list directory for card art files and load them into a list
        this.InitializeCardArtList();
        
        // Load position data from the local .json file if there is one
        this.key = (ArtPositioningKey)this.serializedObject.FindProperty("Key").enumValueIndex;
        if(PositionData == null) 
        { 
            CardArtImage.PositionData = new Dictionary<ArtPositioningKey, ArtPositioningData>();
            CardArtImage.PositionData[this.key] = new ArtPositioningData(this.key);
        }
        else
        {
            // Attempt to load positions from file
            ArtPositioningData positionData = null;
            ArtPositioningDataContract contract = ArtPositioningDataContract.LoadPositions(this.key);
            if(contract != null)
            {
                positionData = contract.ToModel();    
            }
            
            if(positionData == null)
            {
                // if we can't find that, create blank position data
                positionData= new ArtPositioningData(this.key);
            }
            
            // Store the position data in the static reference
            CardArtImage.PositionData[this.key] = positionData;
        }
        
        // Get a reference to the inspector skin
        this.inspectorSkin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
        
        // Apply initial positioning data to the cardArt
        this.InitializeCardArtPositions(cardArtImage.ArtId);
    }
    
    private void FlipHorizontal()
    {
        ArtPositioningData localData = CardArtImage.PositionData[this.key];
        foreach(var kvp in localData.PositionsDictionary)
        {
            CardArtPositionUpdate update = this.GetPositionUpdate(kvp.Key);
            Vector3 newScale = kvp.Value.Scale;
            newScale.x *= -1f;
            update.Scale = newScale;
            update.Position = kvp.Value.Position;
        }
    }
    
    /// <summary>
    /// When we have textuers in the loading queue, make sure the editor keeps repaining
    /// so that OnInspectorGUI keeps getting called and the card are loading queue continues
    /// to get pumped.
    /// </summary>
    public override bool RequiresConstantRepaint()
    {
        // Requre constant repaints while we have textures pending loading
        // this keeps our OnInspectorGUI calling so we can increment our ticks
        return this.loadTextureQueue.Count > 0;
    }
    
    /// <summary>
	/// Main GUI of the editor
	/// </summary>
	public override void OnInspectorGUI()
    {
        // Tick counts for texture loading
        if(this.loadTextureQueue.Count > 0)
        {
            this.ticksSinceLastTextureLoaded++;
        }

        CardArtImage cardArtImage = (CardArtImage)this.serializedObject.targetObject;
        
        // Positioning Key
        cardArtImage.Key = (ArtPositioningKey)EditorGUILayout.EnumPopup("Key",(ArtPositioningKey)this.serializedObject.FindProperty("Key").enumValueIndex);
        if(GUI.changed) 
        {
            this.serializedObject.ApplyModifiedProperties();
            this.key = cardArtImage.Key;
        }
        
        // Enable Masking
        if(this.mask != null)
        {
            this.mask.enabled = EditorGUILayout.Toggle("Enable Masking", this.mask.enabled);
        }
        else
        {
            EditorGUILayout.LabelField("Enable Masking", "**Mask Not Found**");
        }
        
        /*
        if(GUILayout.Button("Flip Horizontal"))
        {
            this.FlipHorizontal();
        }
        */
        
        // Start Details group
        EditorGUILayout.BeginHorizontal(); 
        
        // Left Column
        EditorGUILayout.BeginVertical(GUILayout.Width(100));
            this.DrawCardArtButton(cardArtImage.ArtId, cardArtImage.texture != null ? cardArtImage.texture : this.defaultCardArtTexture, (id)=>{ this.isArtPickerVisible = !this.isArtPickerVisible;});
        EditorGUILayout.EndVertical();
        
        // Right Column
        EditorGUILayout.BeginVertical();
        EditorGUIUtility.labelWidth = 55;
            
            // Texture (artId)
            EditorGUILayout.LabelField("Texture",cardArtImage.ArtId);
            
            // Position
            Vector2 newPos = EditorGUILayout.Vector2Field("Position", this.rectTrans.anchoredPosition);
            if(newPos.x != this.initialPosition.x || newPos.y != this.initialPosition.y)
            {
                CardArtPositionUpdate update = GetPositionUpdate(cardArtImage.ArtId);
                this.rectTrans.anchoredPosition = newPos;
                update.Position = newPos;
                update.Scale = this.rectTrans.localScale;
                this.initialPosition = newPos;
            }
            
            // Scale
            Vector3 newScale = EditorGUILayout.Vector3Field("Scale", this.rectTrans.localScale);
            if(newScale.x != this.initialScale.x || newScale.y != this.initialScale.y || newScale.z != this.initialScale.z)
            {
                CardArtPositionUpdate update = GetPositionUpdate(cardArtImage.ArtId);
                update.Scale = newScale;
                update.Position = this.rectTrans.anchoredPosition;
                this.rectTrans.localScale = newScale;
                this.initialScale = newScale;
            }
            
            // Uniform Scale
            EditorGUILayout.BeginHorizontal();
                GUI.changed = false;
                GUILayout.Space(EditorGUIUtility.labelWidth + 5);
                GUI.enabled = Mathf.Abs(this.rectTrans.localScale.x) == this.rectTrans.localScale.y;
                float uScale = GUILayout.HorizontalSlider(this.rectTrans.localScale.y,0f,2f);
                if(GUI.changed)
                {
                    Vector3 newUScale = new Vector3(this.rectTrans.localScale.x < 0 ? -uScale : uScale, uScale, 1f);
                    CardArtPositionUpdate update = GetPositionUpdate(cardArtImage.ArtId);
                    update.Scale = newUScale;
                    update.Position = this.rectTrans.anchoredPosition;
                    this.rectTrans.localScale = newUScale;
                }
                GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
            
            // Buttons
            EditorGUILayout.BeginHorizontal();
                // Save Changes Button
                if(this.positionUpdates.Count > 0)
                {
                    // If there are pending changes... show them
                    if(GUILayout.Button("Save Changes (" + this.positionUpdates.Count + ")"))
                    {
                        this.SaveUpdatedPositions();
                    }
                }
                else
                {
                    // Otherwise display a disabled "Save Changes" button
                    GUI.enabled = false;
                    GUILayout.Button("Save Changes");
                    GUI.enabled = true;
                }
                
                // Publish 
                GUI.enabled = false;
                GUILayout.Button("Publish to Playfab");
                GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
            
        EditorGUIUtility.labelWidth = 0;
        EditorGUILayout.EndVertical();

        // End Details group
        EditorGUILayout.EndHorizontal();
        
        if(this.isArtPickerVisible)
        {
            EditorGUIUtility.labelWidth = 35;
            this.filterText =EditorGUILayout.TextField("Filter:", this.filterText);
            EditorGUIUtility.labelWidth = 0;
            this.scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.Height(500));
                this.LayoutCardArtButtons(this.filterText);
            EditorGUILayout.EndScrollView();
        }
    }
    
    /// <summary>
    /// Utility function that draws a card art button. It takes as a paramter a callback to be called when the button is clicked.
    /// allowing it to be called from multiple contexts and do different things based on that contect.
    /// </summary>
    private void DrawCardArtButton(string artId, Texture texture, System.Action<string> OnClick, bool showName=false, bool showMissing=false, bool showUpdate=false)
    {
        float toggleWidth = 99f;
        float toggleHeight = texture.height * (toggleWidth/texture.width);
        if(GUILayout.Button(texture, GUILayout.Width(toggleWidth), GUILayout.Height(toggleHeight)))
        {
            if(OnClick != null)
            {
                OnClick.Invoke(artId);
            }
        }
        
        Rect rect = GUILayoutUtility.GetLastRect();
        Rect mRect = new Rect(rect);
        
        if(showName)
        {
            rect.x +=6;
            rect.width -= 12;
            rect.y +=6;
            rect.height = labelStyle.CalcHeight(new GUIContent(artId), rect.width);
            GUI.Label(rect, artId, this.labelStyle);
        }
        
        mRect.x += mRect.width - 32;
        mRect.y += mRect.height - 32;
        mRect.width = 32;
        mRect.height = 32;
        
        if(showUpdate)
        {
            GUI.Label(mRect," ",this.inspectorSkin.GetStyle("CN EntryInfo"));
        }
        else if(showMissing)
        {
            GUI.Label(mRect," ",this.inspectorSkin.GetStyle("CN EntryError"));
        }
    }
    
    /// <summary>
    /// Attempts to retrieve a pending position update for a specific artId, if there isn't one
    /// this function creates one and adds it to the positionUpdates list.
    /// </summary>
    private CardArtPositionUpdate GetPositionUpdate(string artId)
    {
        CardArtPositionUpdate update = this.positionUpdates.Where(d=>d.ArtId == artId).FirstOrDefault();
        if(update == null)
        {
            update = new CardArtPositionUpdate();
            update.ArtId = artId;
            this.positionUpdates.Add(update);
        }
        
        return update;   
    }
    
    /// <summary>
    /// Utility function to make background textures for GUI styles
    /// </summary>
    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width*height];
 
        for(int i = 0; i < pix.Length; i++)
            pix[i] = col;
 
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
 
        return result;
    }
    
    /// <summary>
    /// Lays out the Card Art Picker. To change the number of columns it displays change the
    /// numColums variable at the top of the function. Could calculate number of columns based
    /// on the buttonWidth and total width of the editor ... but... lazy. -Dmac
    /// </summary>
    private void LayoutCardArtButtons(string filterText)
    {
        List<CardArtListItem> artItemsToDisplay = this.cardArtList;
        bool isFiltered = false;
        
        if(string.IsNullOrEmpty(filterText) == false)
        {
            artItemsToDisplay = this.cardArtList.Where(d=>d.ArtId.StartsWith(filterText,System.StringComparison.CurrentCultureIgnoreCase)).ToList();
            isFiltered = true;
        }
        
        int currentIndex = 0;
        int numColumns = 4;
        
        EditorGUILayout.BeginHorizontal();
        for(int i=0;i<artItemsToDisplay.Count;i++)
        {
            CardArtListItem item = artItemsToDisplay[i];
            
            // End the previous row and start a new one
            if(currentIndex % numColumns == 0 && currentIndex != 0)
            {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
            }
            
            // If the cardArtListItem doesn't have a texture, add it to the texture loading queue
            if(item.Texture == null && this.loadTextureQueue.Contains(item) == false)
            {
                this.loadTextureQueue.Enqueue(item);
            }
            
            bool isMissingPos = this.PositionData != null ? PositionData.Get(item.ArtId) == null : true;
            bool isUpdated = this.positionUpdates.Where(d=>d.ArtId==item.ArtId).FirstOrDefault() != null;
            
            // Draw the CardArtButton
            this.DrawCardArtButton(item.ArtId, item.Texture != null ? item.Texture : this.defaultCardArtTexture, this.CardArtSelected, isFiltered, isMissingPos, isUpdated);
            
            currentIndex++;
        }
        EditorGUILayout.EndHorizontal();
        
        this.DoTextureLoading(3);
    }
    
    /// <summary>
    /// The callback for button clicks on art buttons the art picker
    /// </summary>
    private void CardArtSelected(string artId)
    {
         CardArtImage cardArtImage = (CardArtImage)this.serializedObject.targetObject;
         CardArtListItem item = this.cardArtList.Where(d=>d.ArtId == artId).FirstOrDefault();
         if(item != null)
         {
             cardArtImage.texture = item.Texture;
             EditorUtility.SetDirty (cardArtImage.gameObject);
             this.InitializeCardArtPositions(cardArtImage.ArtId);
         }
    }
    
    /// <summary>
    /// For a specific card art, try to apply positioning to the gameObject in scene.
    /// This function attempts to use an updatedPosition, then falls back to the position model,
    /// then finally assigns default positions.
    /// </summary>
    private void InitializeCardArtPositions(string artId)
    {
        // Look up position information for the cardArt currently displayed in the CardArtImage
        ArtPosition currentArtPosition = null; 
        
        if(PositionData != null)
        {
            currentArtPosition = PositionData.Get(artId);
        }
        
        CardArtPositionUpdate update = this.positionUpdates.Where(d=>d.ArtId==artId).FirstOrDefault();
        
        // If we have updated position data use that
        if(update != null)
        {
            this.initialPosition = update.Position;
            this.initialScale = update.Scale;
        }
        // If there is position data stored about this object
        else if(currentArtPosition != null)
        {
            // Store the position data as the initial values
            this.initialPosition = currentArtPosition.Position;
            this.initialScale = currentArtPosition.Scale;
        }
        else
        {
            // Otherwise set up some default initial values
            this.initialPosition = Vector2.zero;
            this.initialScale = Vector3.one;    
        }
        
        // Apply the initial values to the cardArtImages rectTransform
        this.rectTrans.anchoredPosition = this.initialPosition;
        this.rectTrans.localScale = this.initialScale;
    }
    
    /// <summary>
    /// Worker function that breaks up loading of card art textures over a few frames
    /// using the loadTexturequeue and a tickCount incremented by OnInspectorGUI
    /// </summary>
    private void DoTextureLoading(int numTexturesToLoad)
    {
        if(this.ticksSinceLastTextureLoaded > 0)
        {
            for(int i=0;i<numTexturesToLoad;i++)
            {
                if(this.loadTextureQueue.Count > 0)
                {
                    CardArtListItem item = this.loadTextureQueue.Dequeue();
                    item.Texture = AssetBundleManager.Instance.LoadAsset<Texture>(item.ArtId);// Resources.Load(item.ArtId) as Texture;
                    this.ticksSinceLastTextureLoaded = 0;
                }
                else
                {
                    return;
                }
            }
        }
    }
    
    /// <summary>
    /// Applies the updated positions to the positon model then generates a dataContract of the model
    /// and writes it to the streamingAssets folder.
    /// </summary>
    private void SaveUpdatedPositions()
    {        
        // Loop though all the updates and apply them to the positioning model
        for(int i=0;i<this.positionUpdates.Count;i++)
        {
            CardArtPositionUpdate update = this.positionUpdates[i];
            ArtPosition model = PositionData.Get(update.ArtId);
            
            // Add the update to the model if a model doesn't already exist
            if(model == null)
            {
                model = new ArtPosition();
                model.ArtId = update.ArtId;
                PositionData.PositionsDictionary[model.ArtId] = model;
            }
            
            // Apply the updates to the model
            model.Scale = update.Scale;
            model.Position = update.Position;
        }

        var betterPosDict = new Dictionary<string, float[]>();
        foreach (var pos in PositionData.PositionsDictionary.Values.OrderBy(v => v.ArtId))
        {
            betterPosDict[pos.ArtId] = new float[] {
                System.Math.Sign(pos.Scale.x) * (float)System.Math.Ceiling(System.Math.Abs(pos.Scale.x) * 100)/100f,
                System.Math.Sign(pos.Scale.y) * (float)System.Math.Ceiling(System.Math.Abs(pos.Scale.y) * 100)/100f,
                System.Math.Sign(pos.Scale.z) * (float)System.Math.Ceiling(System.Math.Abs(pos.Scale.z) * 100)/100f,
                System.Math.Sign(pos.Position.x) * (float)System.Math.Ceiling(System.Math.Abs(pos.Position.x) * 100)/100f,
                System.Math.Sign(pos.Position.y) * (float)System.Math.Ceiling(System.Math.Abs(pos.Position.y) * 100)/100f, };
        }
        string converted = JsonConvert.SerializeObject(betterPosDict, Formatting.None);
        string filePath = System.IO.Path.Combine("Assets/AssetBundles/", this.key.ToString() + "_better2.json");
        File.WriteAllText(filePath, converted);
        Debug.LogFormat("{0} Art File position written", betterPosDict.Count);
    }

    /// <summary>
    /// Loops though all the art files in the cardArt folder and attemps to populate elements in the cardAtList
    /// that we use to populate the art picker. Note, textuers aren't loaded at this time, just the card art names
    /// which correspond 1to1 with the artId
    /// </summary>
    private void InitializeCardArtList()
    {
        // get the system file paths of all the files in the asset folder
        string dataPath  = Application.dataPath;
        string folderPath = Path.Combine(Path.Combine(dataPath, "Card Art"), "Resources");          
        string[] filePathsArray = Directory.GetFiles(folderPath);
        
        // Initialize a new cardArt list (potentially dereferencing an existing one)
        this.cardArtList = new List<CardArtListItem>();
        this.loadTextureQueue.Clear();
        
        // Loop through all the card art in the resources folder
        for(int i=0;i<filePathsArray.Length;i++)
        {
            // Only try to load non .meta files
            if(filePathsArray[i].EndsWith(".png") || filePathsArray[i].EndsWith(".psd") )
            {
                // Extract the file name
                int index = filePathsArray[i].LastIndexOf(Path.DirectorySeparatorChar)+1;
                string cardArtId = filePathsArray[i].Substring(index, filePathsArray[i].Length - index-4);
                
                // Load the file into a texture
                Texture texture = AssetBundleManager.Instance.LoadAsset<Texture>(cardArtId);// Resources.Load(cardArtId) as Texture;
                
                if(texture != null)
                {
                    // Use it as a string key in the cardArtList
                    this.cardArtList.Add(new CardArtListItem(cardArtId));
                }
                else
                {
                    Debug.Log("Unable to load texture: Assets" + Path.DirectorySeparatorChar + "Card Art" +Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + cardArtId );
                }
            }
        }
    }
}
