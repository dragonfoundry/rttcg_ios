using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using strange.extensions.mediation.impl;
using NovaBlitz.UI;

public class ResourceTool : EditorWindow
{
	#region Editor
	
	[MenuItem("Nova Blitz/Resource Tool")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(ResourceTool));
	}
	
	void OnGUI()
	{
		if (GUILayout.Button("Save Resource Map"))
			SaveResourceMap();
	}
	
	#endregion
	
	#region Asset

	private void SaveResourceMap()
	{
		// Create new map if one does not exist.
		string[] maps = FindResources<ResourceMap>("Assets/Resources");
		bool exists = maps.Length > 0;
		ResourceMap rmap = exists ? Resources.Load<ResourceMap>(maps[0]) : CreateResource<ResourceMap>();

		// Modify data.
		rmap.AvatarTexturePaths = FindResources<Texture>("Assets/Resources/texture/avatar");
		rmap.CardBackTexturePaths = FindResources<Texture>("Assets/Resources/texture/cardback");

		// Save file.
		SaveResource<ResourceMap>(rmap, exists);
	}

	#endregion
	
	#region Utility
	
	private T CreateResource<T>() where T : ScriptableObject
	{
		Debug.Log(string.Format("Creating new {0}...", typeof(T)));
		return ScriptableObject.CreateInstance<T>();
	}
	
	private void SaveResource<T>(T res, bool exists) where T : ScriptableObject
	{
		string typeName = typeof(T).ToString();
		if (exists)
		{
			EditorUtility.SetDirty(res);
			AssetDatabase.SaveAssets();
		}
		else
			AssetDatabase.CreateAsset(res, string.Format(@"Assets/Resources/{0}.asset", typeName));
		Debug.Log(string.Format("{0} saved!", typeName));
	}
	
	private string[] FindResources<T>(string path) where T : UnityEngine.Object
	{
		string filter = typeof(T).IsSubclassOf(typeof(MonoBehaviour)) ? "t:Prefab" : string.Format("t:{0}", Regex.Replace(typeof(T).ToString(), @".+\.(\w+)$", @"$1"));
		string[] assets = AssetDatabase.FindAssets(filter, new string[] { path });
		return AssetsToResources<T>(assets);
	}
	
	private string[] AssetsToResources<T>(string[] assets) where T : UnityEngine.Object
	{
		int assetCt = assets.Length;
		List<string> resources = new List<string>();
		
		for (int i=0; i<assetCt; ++i)
		{
			// Resolve asset path and extract resource qualifier.
			string fullPath = AssetDatabase.GUIDToAssetPath(assets[i]);
			T res = AssetDatabase.LoadAssetAtPath(fullPath, typeof(T)) as T;
			if (null == res)
				continue;

			// Strip root path and file extension.
			resources.Add(Regex.Replace(fullPath, @".+Resources/(.+)\.\w+$", @"$1"));
		}
		return resources.ToArray();
	}
	
	#endregion
}