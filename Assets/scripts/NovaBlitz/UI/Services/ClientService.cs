﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using DragonFoundry.Networking;
using DragonFoundry.Common.MessageSocket;
using System;
using System.Net;
using System.Net.Sockets;
using Messages;
using NovaBlitz.UI;
using LinqTools;

namespace NovaBlitz.Game
{
    public class ClientService : MonoBehaviour
    {
        // Message raised signals
        [Inject] public GameCreatedSignal GameCreatedSignal { get; set; }
        [Inject] public GameJoinedSignal GameJoinedSignal { get; set; }
        [Inject] public StartGameReadySignal StartGameReadySignal { get; set; }
        [Inject] public DiffGameStateMessageSignal DiffGameStateMessageSignal {get;set;}
		[Inject] public DraftNextPackSignal DraftNextPackSignal {get;set;}
		[Inject] public DraftOverSignal DraftOverSignal {get;set;}
		[Inject] public GameOverSignal GameOverSignal {get;set; }
        [Inject] public ServerErrorRaisedSignal ServerErrorSignal { get; set; }
		[Inject] public ServerResponseSignal ServerResponseSignal {get;set;}
        [Inject] public CancelMatchmakingResponseSignal CancelMatchmakingResponseSignal { get; set; }
        [Inject] public PaidGameNewOrResumedSignal PaidGameNewOrResumedSignal { get; set; }
        [Inject] public PlayerDataResponseSignal PlayerDataResponseSignal { get; set; }
        [Inject] public ManageFriendResponseSignal ManageFriendResponseSignal { get; set; }
        [Inject] public PlayerLoggedInSignal PlayerLoggedInSignal { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public CraftResponseSignal CraftResponseSignal { get; set; }
        [Inject] public SmartPackOpenedSignal SmartPackOpenedSignal { get; set; }
        [Inject] public VirtualPurchaseResponseSignal VirtualPurchaseResponseSignal { get; set; }
        [Inject] public RedeemCouponResponseSignal RedeemCouponResponseSignal { get; set; }
        [Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal { get; set; }
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }
        [Inject] public ServerReconnectSignal ServerReconnectSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public PlayFabChatClient PlayFabChatClient {get;set;}
        [Inject] public UnloadUnusedResourcesSignal UnloadUnusedResourcesSignal { get; set; }
	    [Inject] public CancelSignal CancelSignal {get;set;}
        [Inject] public CancelMatchmakingSignal CancelMatchmakingSignal { get; set; }

        [Inject] public SelfEmoteSignal SelfEmoteSignal { get; set; }
        [Inject] public OpponentEmoteSignal OpponentEmoteSignal { get; set; }
        [Inject] public SelfInputActionSignal SelfInputActionSignal { get; set; }
		[Inject] public OpponentInputActionSignal OpponentInputActionSignal { get; set; }
		[Inject] public ConnectToPhotonChatSignal ConnectToPhotonChatSignal {get;set;}

        // ========================
        // Public Properties
        // ========================
        public bool IsConnected {get; private set;}
        public bool IsSessionReady {get; set;}
		public Guid SessionGuid {get;set; }
        private int RetryConnectTime = 1;
        public bool? IsInGame { get; set; }

        private System.Random random = new System.Random();

        // ========================
        // Private Properties
        // ========================
        private string Hostname = "localhost";
		private Int32 port = 7777;

        private KeepAliveMessage keepAliveMessage = new KeepAliveMessage();

        private ClientMessageSocket clientSocket;
		private readonly ConcurrentQueue<MessageBase> outgoing = new ConcurrentQueue<MessageBase>();
		private readonly ConcurrentQueue<MessageBase> incoming = new ConcurrentQueue<MessageBase>();
		private IEnumerator connectToServerCoRoutine;
        //private IEnumerator keepAliveCoroutine;
        private IEnumerator reauthenticateEveryTwelveHours;
        //private int alive = 0;
        //private int ack = 0;
        private DisconnectCode DisconnectCode = DisconnectCode.NoCode;

        private DateTime LastConnectedDateTime;

        /// <summary>
        /// Start's a CoRoutine that connects to the specified server asynchronously
        /// onFinished( bool isConnected, string finishedReason)
        /// </summary>
        public void ConnectToServerAsync(string hostname, Int32 port, Action<bool,string> onFinished)
		{
            this.Hostname = hostname;
			this.port = port;
			this.clientSocket = new ClientMessageSocket();
			if(this.connectToServerCoRoutine != null)
			{
				this.StopCoroutine(this.connectToServerCoRoutine);
			}
			this.IsConnected = false;
			this.IsSessionReady = false;
            this.DisconnectCode = DisconnectCode.NoCode;
            this.connectToServerCoRoutine = ConnectToServer(onFinished);
			this.StartCoroutine(connectToServerCoRoutine);
		}

        public void OnApplicationQuit()
        {

            //if (this.clientSocket != null && this.clientSocket.connectionState != null && this.clientSocket.connectionState.socket != null)
            //    this.clientSocket.connectionState.socket.Close();
            //Disconnect();
        }

        /// <summary>
        /// Sends a diconnect message to the server
        /// </summary>
        public void Disconnect()
		{
            if (this.IsConnected)
            {
                DisconnectMessage dcMessage = new DisconnectMessage { DisconnectCode = DisconnectCode.ClientDisconnectRequest };
                this.SendMessage(dcMessage);
                Debug.Log("#ClientService#Client Service sent Disconnect Message");
            }
		}

        public void OnLogout()
        {
            this.DisconnectCode = DisconnectCode.UserLoggedOut;
            Debug.Log("Logged out. Disconnecting");
            OnDisconnect();
        }

        public void ForceDisconnect(DisconnectCode code)
        {
            this.DisconnectCode = code;
            OnDisconnect();
        }

        public void DisconnectAndReconnect()
        {
            if (this.IsConnected)
            {
                this.DisconnectCode = DisconnectCode.PleaseLogOutAndBackInAgain;
                Debug.Log("reconnecting");
                OnDisconnect();
            }
        }

        private void OnDisconnect()
        {
            this.CancelSignal.Dispatch();
            this.CancelMatchmakingSignal.Dispatch();
            Timer.Instance.StopAllCoroutines();
            Timer.GameInstance.StopAllCoroutines();

            var scv = GameObject.FindObjectOfType<SessionContextView>();
            if (scv != null)
            {
                foreach (Transform t in scv.MainCanvas.transform)
                {
                    var pmb = t.GetComponent<PoolableMonoBehaviour>();
                    if (pmb != null)
                    {
                        Debug.LogFormat("OnDisconnect: Recycling PMB: {0}", pmb.PooledObjectType);
                        pmb.Recycle();
                    }
                    else
                        GameObject.Destroy(t.gameObject);
                }
            }
            else
                Debug.LogError("Session context view is null. That shouldn't be possible");

            this.PlayerProfile.ResetProfile();
            Debug.Log("Disconnected: On Disconnect");
            if(this.connectToServerCoRoutine != null)
            {
                StopCoroutine(this.connectToServerCoRoutine);
                connectToServerCoRoutine = null;
            }
            Messages.MessageBase message = null;
            while (this.incoming.Count > 0) { this.incoming.TryDequeue(out message); }
            while (this.outgoing.Count > 0) { this.outgoing.TryDequeue(out message); }
            this.IsConnected = false;
            this.IsSessionReady = false;
            this.clientSocket = null;
            this.ServerReconnectSignal.Dispatch(DisconnectCode);
            /*
            switch (DisconnectCode)
            {
                // If they logged out, do nothing here.
                case DisconnectCode.UserLoggedOut:
                    break;
                // if there's a problem, notify the user
                case DisconnectCode.ServerDownForMaintenance:
                case DisconnectCode.NeedToRestartClient:
                case DisconnectCode.DataParseFailed:
                case DisconnectCode.ClientOutOfDate:
                case DisconnectCode.BlockedUser:
                case DisconnectCode.MessagingError:
                case DisconnectCode.ClientDisconnectRequest:
                    this.ServerDisconnectNotificationSignal.Dispatch(DisconnectCode);
                    break;
                // If there's an auth problem, log them out
                case DisconnectCode.AuthenticationFailed:
                case DisconnectCode.LoggedInElsewhere:
                    Reconnect();
                    break;
                // for these errors, try to reconnect
                case DisconnectCode.PleaseLogOutAndBackInAgain:
                    SilentReconnect();
                    break;
                case DisconnectCode.NotLoggedIn:
                case DisconnectCode.ServerCrash:
                case DisconnectCode.NoCode:
                default:
                    {
                        //this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(I2.Loc.ScriptLocalization.Error.ConnectionFailed,
                        //    I2.Loc.ScriptLocalization.Disconnect.Reconnect, OnConfirmDialogDismissed, false));
                        Reconnect();
                        break;
                    }

            }*/
        }

        /*private void OnConfirmDialogDismissed(bool isTrue)
        {
            if(isTrue)
            {
                this.ViewReferences.CloseAllViews();
                this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect);
            }
            else
            {
                this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(I2.Loc.ScriptLocalization.Error.ConnectionFailed,
                    I2.Loc.ScriptLocalization.Disconnect.Reconnect, OnConfirmDialogDismissed, false));
            }
        }*/

        /*public void SilentReconnect()
        {
            if (this.IsConnected)
                return;
            Debug.LogWarning("Requested reconnect - trying silently");
            ReconnectNow();

            if (this.IsConnected)
                return;
            Debug.LogWarning("Requested reconnect failed - trying manually");
            this.ServerDisconnectNotificationSignal.Dispatch(DisconnectCode);
        }*/

        /*public void Reconnect()
        {
            if (this.IsConnected)
                return;
            if(this.ServerDisconnectNotificationSignal == null)
            {
                Application.Quit();
                //If we are running in the editor Stop playing the scene
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
                return;
            }
            this.ServerDisconnectNotificationSignal.Dispatch(this.DisconnectCode);
        }*/

        /// <summary>
        /// Worker coRoutine that negotiates the handshake between the ClientService
        /// and a dragonFoundry Server
        /// </summary>
        private IEnumerator ConnectToServer(Action<bool,string> onFinished)
		{
			//IPAddress address = IPAddress.Parse(this.Hostname);
			//IPEndPoint remote = new IPEndPoint(address, this.port);
			
            var reconnectView = this.ViewReferences.Get<ReconnectDialogView>();

            while (this.IsConnected == false)
            {
                if (reconnectView != null)
                {
                    reconnectView.Initialize(Messages.DisconnectCode.NotConnected, 0);
                }

                try 
				{

                    // try 3 times to get the IP host entry
                    IPHostEntry resolvedServer = null;
                    for (int i = 2; i >= 0; i--)
                    {
                        try
                        {
							resolvedServer = Dns.GetHostEntry(Hostname);// == "localhost" ? Hostname : "dualstack."+Hostname);
                            if (resolvedServer != null)
                                break;
                        }
                        catch (Exception ex)
                        {
                            if (i == 0)
                                throw ex;
                        }
                    }
                    for (int i = 0; i < resolvedServer.AddressList.Length; i++)
                    {
                        Debug.LogFormat("IP: {0}, {1}, {2}", resolvedServer.AddressList[i].GetAddressBytes(), resolvedServer.AddressList[i].AddressFamily, resolvedServer.AddressList[i].ToString());
                    }
                    
                    // get a list. Interleave IPv4, then IPV6, at random
                    var addrIPv4 = resolvedServer.AddressList.Where(a => a.AddressFamily == AddressFamily.InterNetwork).OrderBy(a => random.Next(0, 100)).ToList();
                    var addrIPv6 = resolvedServer.AddressList.Where(a => a.AddressFamily == AddressFamily.InterNetworkV6).OrderBy(a => random.Next(0, 100)).ToList();
					List<IPAddress> addressList;
					//Debug.LogErrorFormat("IP addresses resolved: ipv4:{0}:{1} ipv6:{2}:{3}", addrIPv4.Count, addrIPv4.FirstOrDefault(), addrIPv6.Count, addrIPv6.FirstOrDefault());
					if(addrIPv6.Count > 0)
					{
						if(addrIPv4.Count > 0)
							addressList = addrIPv6.Concat(addrIPv4).ToList();
						else
							addressList = addrIPv6;
					}
					else 
						addressList = addrIPv4;
					/*List<IPAddress> addressList = new List<IPAddress>();
                    var ipv4count = addrIPv4.Count;
                    var ipv6count = addrIPv6.Count;
                    var count = Math.Max(ipv4count, ipv6count);
                    for (int i = 0; i < count; i++)
                    {
                        if (i < ipv4count)
                        {
                            addressList.Add(addrIPv4[i]);
                        }
                        if (i < ipv6count)
                        {
                            addressList.Add(addrIPv6[i]);
                        }
                    }*/
                    //IPAddress[] addressList = addrIPv4.Zip(addrIPv6, (f, s) => new[] { f, s }).SelectMany(f => f).ToArray();
                    //var addressList = addrIPv4.ToArray();
                    /*Debug.LogFormat("IP: {0} addresses. {1}v4, {2}v6",addressList.Length, addrIPv4.Count(), addrIPv6.Count());
                    for(int i = 0; i<addressList.Length; i++)
                    {
                        Debug.LogFormat("IP: {0}, {1}, {2}", addressList[i].GetAddressBytes(), addressList[i].AddressFamily, addressList[i].ToString());
                    }*/

                    for (int i = 0; i < addressList.Count; i++)
                    {
                        IPAddress address = addressList[i];
						//Debug.LogErrorFormat("Connecting to {0} via {1}", address.ToString(), address.AddressFamily == AddressFamily.InterNetworkV6 ? "ipv6" : "ipv4");
                        IPEndPoint serverEndPoint = new IPEndPoint(address, this.port);
                        this.clientSocket.connectionState.socket =
                            new Socket(
                                address.AddressFamily,
                                SocketType.Stream,
                                ProtocolType.Tcp);
                        try
                        {
                            this.clientSocket.connectionState.socket.Connect(serverEndPoint);
                            this.IsConnected = true;
                            RetryConnectTime = 1;
                            // Wire up the recieve handler
                            this.clientSocket.MessageAvailable += this.ReceiveIncoming;
                            break;
                        }
                        catch (SocketException ex)
                        {
                            if (this.clientSocket.connectionState.socket != null)
                                this.clientSocket.connectionState.socket.Close();
                            if (i == resolvedServer.AddressList.Length - 1)
                            {
                                Debug.LogErrorFormat(
									"Failed to connect to the server. {0}. Exception {1}", Hostname,ex);
                            }
                        }
                    }

                    if (this.IsConnected)
                        break;



                    //var hostentry = Dns.GetHostEntry(Hostname);
                    /*var ipV4List = hostentry.AddressList.Where(a => a.AddressFamily == AddressFamily.InterNetwork).ToList();
                    var ipV6List = hostentry.AddressList.Where(a => a.AddressFamily == AddressFamily.InterNetworkV6).ToList();
                    if(ipV4List.Count > 0)
                    {
                        this.clientSocket.connectionState.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        
                    }
                    else if (ipV6List.Count > 0)
                    {
                        this.clientSocket.connectionState.socket = new Socket(AddressFamily.InterNetworkV6, SocketType.Stream, ProtocolType.Tcp);
                    }
                    else
                    {

                    }*/
                    //this.clientSocket.connectionState.socket = new Socket(AddressFamily.InterNetworkV6, SocketType.Stream, ProtocolType.Tcp);
                    //IPEndPoint endPoint = new IPEndPoint()
                    //this.clientSocket.Connect(Hostname, 7777);
                    //this.clientSocket.Connect(remote);
					//break;
				}
				catch (SocketException se) 
				{
					Debug.LogErrorFormat("Could not resolve server DNS name: {0} from Hostname {1}", se.Message, Hostname);
                }
                RetryConnectTime *= 2;
                yield return new WaitForSecondsRealtime(3);
                OpenAnimatedViewSignal.Dispatch(typeof(ReconnectDialogView));
                if(reconnectView == null)
                    reconnectView = this.ViewReferences.Get<ReconnectDialogView>();
                reconnectView.Initialize(DisconnectCode.Reconnecting, RetryConnectTime);
                // Notify wait time on retry connect;
                yield return new WaitForSecondsRealtime(RetryConnectTime);
            }
			
			this.connectToServerCoRoutine = null;

            // Restart the 12 hour clock for re-logins.
            if (this.reauthenticateEveryTwelveHours != null)
            {
                StopCoroutine(this.reauthenticateEveryTwelveHours);
            }
            //this.reauthenticateEveryTwelveHours = null;
            reauthenticateEveryTwelveHours = ReauthenticateInTwelveHours();
            StartCoroutine(reauthenticateEveryTwelveHours);

			if(onFinished != null)
			{
				onFinished.Invoke(true,"#ClientService#Successfully connected to server.");
			}
			
			yield break;
		}

        public IEnumerator ReauthenticateInTwelveHours()
        {
            Debug.Log("#ClientService# starting reauthentication timer");
            //yield return new WaitForSecondsRealtime(30);
            DateTime targetTime = DateTime.UtcNow.AddHours(12);
            while(DateTime.UtcNow < targetTime)
            {
                yield return new WaitForSecondsRealtime(30); // wait 1 minute, try again
                SendMessage(keepAliveMessage);
            }

            // Use the normal "please reconnect" flow. Same as if your server scales down.
            Debug.Log("#ClientService# hit reauthentication timer - forcing re-login");
            this.ReconnectAtNextGoodOpportunity();

			if (this.reauthenticateEveryTwelveHours != null)
				StopCoroutine (this.reauthenticateEveryTwelveHours);
            this.reauthenticateEveryTwelveHours = null;
            //reauthenticateEveryTwelveHours = ReauthenticateInTwelveHours();
            //StartCoroutine(reauthenticateEveryTwelveHours);
        }

        public void ReconnectAtNextGoodOpportunity()
        {
            // Use the connect to server coroutine here because it will get wiped out whenever we try to reconnect (which is the behaviour we want!)
            if(this.connectToServerCoRoutine != null)
            {
                this.StopCoroutine(this.connectToServerCoRoutine);
                this.connectToServerCoRoutine = null;
            }
            this.connectToServerCoRoutine = WaitForNextOpportunity();
            StartCoroutine(this.connectToServerCoRoutine);
        }

        public IEnumerator WaitForNextOpportunity()
        {
            this.PlayerProfile.IsFinishedDataLoading = false; // need to ensure you'll reload your data
            if (IsInGame == true)
            {
                // If you're in a game right now, wait for the game to end & put the reconnect in the end of game screen.
                while (IsInGame == true)
                {
					yield return new WaitForSecondsRealtime(0.1f);
                }
            }
            else
            {
                // wait for 15 minutes to see if the player starts & finishes a game - this'll put the reconnect into the end of game progress loading screen, which is pretty unobtrusive.
                var targetTime = DateTime.UtcNow.AddMinutes(15);
                while (IsInGame != true && DateTime.UtcNow < targetTime)
                {
					yield return new WaitForSecondsRealtime(0.1f);
                }

                while (IsInGame == true)
                {
					yield return new WaitForSecondsRealtime(0.1f);
                }
            }
            DisconnectAndReconnect();
        }

        /*public void ReconnectNow()
        {
            if (this.IsConnected)
                return;
            else if(this.connectToServerCoRoutine != null)
            {
                StopCoroutine(connectToServerCoRoutine);
                this.connectToServerCoRoutine = null;// ConnectToServer(OnFinished);
                //this.StartCoroutine(connectToServerCoRoutine);
            }
            this.MatchmakeToServerSignal.Dispatch();
        }*/
		
		public void StopAsyncRequest(IEnumerator asyncRequest)
		{
			this.StopCoroutine(asyncRequest);
		}
        
        // Update is called once per frame
        void Update()
        {
			// Incoming and outgoing message pump
			if(this.IsConnected)
			{
                if (this.clientSocket == null)
                {
                    Debug.LogError("#ClientService#Socket is null. Disconnecting");
                    this.OnDisconnect();
                }
                else
                {
                    try
                    {
                        // Send
                        this.SendOutgoing(clientSocket);
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError("#ClientService#Error sending Packet: " + e.ToString());
                        this.OnDisconnect();
                    }

                    try
                    {
                        // And Receive
                        this.clientSocket.NonblockingPoll();
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError("#ClientService#Error receiving Packet: " + e.ToString());
                        this.OnDisconnect();
                    }
                }
			}
			
			// Session specific message pump
			if(this.IsSessionReady)
			{
				Messages.MessageBase m = this.FetchMessage();

				while (m != null)
				{
                    // we've got a message from the server, so any timeout is ignored.
                    CancelTimeout();
                    //log.AddLogMessage(m);
                    if (m.Error != ErrorCode.NoError)
                    {
                        //ServerErrorSignal.Dispatch(m.Error, m);
                        Debug.LogErrorFormat("#ClientService#Server Error {0} in message {1}", m.Error, m.GetType().ToString());
                    }
                    
                    /*if (m is KeepAliveMessage)
                    {
                        // Send back a response
                        this.SendMessage(new KeepAliveAck());
                    }
                    else if (m is KeepAliveAck)
                    {
                        // Ack from the client's keepalive send
                        this.ack++;
                    }*/

                    if (m is KeepAliveMessage)
                    {
                        this.SendMessage(new KeepAliveAck());
                    }
                    else if (m is KeepAliveAck)
                    { }
                    else if (m is ConcedeGameResponse)
                    { }
                    else if (m is DiffGameStateMessage)
                    {
                        // Parse and Dispatch the DiffMessage
                        DiffGameStateMessage diffMessage = (DiffGameStateMessage)m;
                        //Debug.LogWarning("ClientService:" + this.GetInstanceID() + " got DiffMessage with " + (diffMessage.GameEvents != null ? diffMessage.GameEvents.Count.ToString() : "0") + " gameEvents");

                        if (diffMessage.SomethingHasHappened)
                        {
                            this.DiffGameStateMessageSignal.Dispatch(diffMessage);
                        }
                        if(diffMessage.OppEmote != 0)
                        {
                            this.OpponentEmoteSignal.Dispatch(diffMessage.OppEmote);
                        }
                        if(diffMessage.YouEmote != 0)
                        {
                            this.SelfEmoteSignal.Dispatch(diffMessage.YouEmote);
                        }
                        if(diffMessage.YouInputAction != null)
                        {
                            this.SelfInputActionSignal.Dispatch(diffMessage.YouInputAction);
                        }
                        if (diffMessage.OppInputAction != null)
                        {
                            this.OpponentInputActionSignal.Dispatch(diffMessage.OppInputAction);
                        }
                    }
                    else if (m is GameActionResponse)
                    {
                        // We don't currently use this...
                        //GameActionResponse gameActionResponse = (GameActionResponse)m;
                        //this.GameActionSignal.Dispatch(gameActionResponse);
                        //Debug.Log("#ClientService#Mesage: GameActionResponse");
                    }
                    else if (m is CreateGameResponse)
                    {
                        CreateGameResponse createGameResponse = (CreateGameResponse)m;
                        this.GameCreatedSignal.Dispatch(createGameResponse);
                        //Debug.Log("#ClientService#Mesage: CreateGameResponse");
                    }
                    else if (m is JoinGameResponse)
                    {
                        JoinGameResponse joinGameResponse = (JoinGameResponse)m;
                        this.GameJoinedSignal.Dispatch(joinGameResponse);
                        //Debug.Log("#ClientService#Mesage: JoinGameResponse");
                    }
                    else if (m is ReadyToStartGameResponse)
                    {
                        ReadyToStartGameResponse readyToStartGameResponse = (ReadyToStartGameResponse)m;
                        this.StartGameReadySignal.Dispatch(readyToStartGameResponse);
                        //Debug.Log("#ClientService#Mesage: ReadyToStartGameResponse");
                    }
                    else if (m is GameOverMessage)
                    {
                        // Parse and Dispatch the GameOverMEssage
                        GameOverMessage gameOverMessage = (GameOverMessage)m;
                        //Debug.LogWarning("#ClientService#ClientService recieved: " + gameOverMessage);
                        this.GameOverSignal.Dispatch(gameOverMessage);
                    }
                    else if (m is PaidGameNewOrResumeResponse)
                    {
                        PaidGameNewOrResumeResponse paidGameNewOrResumeResponse = (PaidGameNewOrResumeResponse)m;
                        this.PaidGameNewOrResumedSignal.Dispatch(paidGameNewOrResumeResponse);
                        //Debug.Log("#ClientService#Mesage: PaidGameNewOrResumeResponse");
                    }
                    else if (m is DraftNextPack)
                    {
                        // Parse and Dispatch the DraftMessage
                        DraftNextPack draftMessage = (DraftNextPack)m;
                        this.DraftNextPackSignal.Dispatch(draftMessage);
                    }
                    else if (m is DraftOverMessage)
                    {
                        // Parse and Dispatch the DraftOverMessage
                        this.DraftOverSignal.Dispatch();
                    }
                    else if (m is CancelMatchmakingResponse)
                    {
                        CancelMatchmakingResponse cancelMatchmakingResponse = (CancelMatchmakingResponse)m;
                        this.CancelMatchmakingResponseSignal.Dispatch(cancelMatchmakingResponse);
                        //Debug.Log("#ClientService#Mesage: CancelMatchmakingResponse");
                    }
                    else if (m is VirtualPurchaseResponse)
                    {
                        VirtualPurchaseResponse craftResponse = (VirtualPurchaseResponse)m;
                        this.VirtualPurchaseResponseSignal.Dispatch(craftResponse);
                        //Debug.Log("#ClientService#Mesage: Virtual Purchase Response");
                    }
                    else if (m is RedeemCouponResponse)
                    {
                        RedeemCouponResponse response = (RedeemCouponResponse)m;
                        this.RedeemCouponResponseSignal.Dispatch(response);
                    }
                    else if (m is PlayerLogInResponse)
                    {
                        PlayerLogInResponse playerLogInResponse = (PlayerLogInResponse)m;
                        this.PlayerLoggedInSignal.Dispatch(playerLogInResponse);
                        //Debug.Log("#ClientService#Mesage: PlayerLogInResponse");
                    }
                    else if (m.Error != ErrorCode.NoError)
                    {
                        // Anything above here handles errors within the signal recievers. Goal is to have this at the bottom, and all errors handled by the signal receivers
                        // Actual error handling is done at the very top of this section...
                    }
                    else if (m is ManageFriendResponse)
                    {
                        ManageFriendResponse manageFriendResponse = (ManageFriendResponse)m;
                        this.ManageFriendResponseSignal.Dispatch(manageFriendResponse);
                        //Debug.Log("#ClientService#Mesage: ManageFriendResponse");

                    }
                    else if (m is ErrorResponse)
                    {
                        ErrorResponse errorResponse = (ErrorResponse)m;
                        this.ServerErrorSignal.Dispatch(errorResponse.Error, errorResponse);
                        //Debug.LogErrorFormat("#ClientService#Error Response: {0}", (m as ErrorResponse).Error.ToString());
                    }
                    else if (m is CraftResponse)
                    {
                        CraftResponse craftResponse = (CraftResponse)m;
                        this.CraftResponseSignal.Dispatch(craftResponse);
                        //Debug.Log("#ClientService#Mesage: CraftResponse");
                    }
                    else if (m is OpenPackResponse)
                    {
                        OpenPackResponse openResponse = (OpenPackResponse)m;
                        this.SmartPackOpenedSignal.Dispatch(openResponse);
                        //Debug.Log("Mesage: OpenPackResponse");
                    }
                    else if (m is PlayerDataResponse)
                    {
                        PlayerDataResponse playerDataResponse = (PlayerDataResponse)m;
                        this.PlayerDataResponseSignal.Dispatch(playerDataResponse);
                        //Debug.Log("#ClientService#Mesage: PlayerDataResponse");
                    }
                    else if (m is DropFromEventResponse)
                    {
                        // Exists, but we don't currently send it - we use GameOverMessage instead.
                        //DropFromEventResponse dropFromEventResponse = (DropFromEventResponse)m;
                        //this.DroppedFromEventSignal.Dispatch(dropFromEventResponse);
                        //Debug.Log("#ClientService#Mesage: DropFromEventResponse");

                    }
                    else if (m is DisconnectMessage)
                    {
                        DisconnectMessage disconnectMessage = (DisconnectMessage)m;
                        if(disconnectMessage.DisconnectCode == DisconnectCode.PleaseLogOutAndBackInAgain)
                        {
                            this.ReconnectAtNextGoodOpportunity();
                        }
                        else
                        {
                            this.DisconnectCode = disconnectMessage.DisconnectCode;
                            this.OnDisconnect();
                        }
                        //Debug.Log("#ClientService#Mesage: DisconnectMessage");

                    }
                    else
                    {
                        Debug.LogFormat("Unhandled Message: {0}", m.GetType().ToString());
                    }

                    m = this.FetchMessage();
				}
			}
        }
		
		/// <summary>
		/// Utility method to recieve bytes from the socket and add them to the incomming queue
		/// </summary>
		private void ReceiveIncoming(UInt32 connectionId, byte[] bytes) 
		{
			Messages.MessageBase msg = MessageSerializer.DeserializeFromBytes(bytes) as Messages.MessageBase;
            //Debug.Log("Received Incomming msg: " + msg.GetType().Name);
            if (msg != null)
                this.incoming.Enqueue(msg);
		}
		
		/// <summary>
		/// Pumps the outgoing message queue trying to send messages
		/// </summary>
		private void SendOutgoing(ClientMessageSocket outgoingSocket)
		{
			while (outgoing.Count > 0)
			{
				Messages.MessageBase toSend = null;
				if (!outgoing.TryDequeue(out toSend))
				{
					// Better luck next loop!
					return;
				}

                CheckForTimeout(toSend.Id);
                //Debug.Log("Sending mesage " + toSend.GetType().Name);
                MessageSerializer.SerializeToClientMessageSocket(outgoingSocket, toSend);
                if(toSend != null && toSend is DisconnectMessage)
                {
                    CancelTimeout();
                    OnDisconnect();
                }
			}
		}


        private IEnumerator _ServerTimeoutCheck;
        private void CheckForTimeout(byte id)
        {
            if (_ServerTimeoutCheck == null)
            {
                //StopCoroutine(ServerTimeoutCheck);
                _ServerTimeoutCheck = ServerTimeoutCheckCoroutine(id);
                StartCoroutine(_ServerTimeoutCheck);
            }
        }
        private IEnumerator ServerTimeoutCheckCoroutine(byte id)
        {
            yield return new WaitForSecondsRealtime(30);
            Debug.LogErrorFormat("DISCONNECTED - figure out why - message sent was {0}", id);
            _ServerTimeoutCheck = null;
            OnDisconnect();
        }
        private void CancelTimeout()
        {
            if (_ServerTimeoutCheck != null)
                StopCoroutine(_ServerTimeoutCheck);
            _ServerTimeoutCheck = null;
        }

        /// <summary>
        /// Push message to the server. This will queue the message to be sent by the background thread, and
        /// will return immediately.
        /// 
        /// The message that is given to this function should be considered to be "owned" by the Client's worker
        /// thread, and should no longer be used by the main thread.
        /// </summary>
        /// <param name="message">The message to send. Do not access or re-use this message from the main
        /// thread after calling this function.</param>
        public void SendMessage(Messages.MessageBase message)
		{
			//Debug.Log("#ClientService#" + String.Format("Sending message of type {0}", message.GetType()));
			outgoing.Enqueue(message);
		}

		/// <summary>
		/// Fetch a message that was sent by the server. This will return immediately with the next message sent
		/// by the server, or null if there is currently no message waiting.
		/// </summary>
		/// <returns>The next message sent by the server, or null if there is no message waiting.</returns>
		public Messages.MessageBase FetchMessage()
		{
			Messages.MessageBase incoming;
			if (this.incoming.TryDequeue(out incoming))
			{
				//Debug.Log("Incomming message found! " + incoming.GetType().Name);
				return incoming;
			}
			
			return null;
		}


        #region Mono
        /// <summary>
        /// Called by Unity when the application goes into the background or is resumed on IOS
        /// </summary>
        void OnApplicationPause(bool isPaused)
        {
            Debug.LogWarningFormat("OnApplicationPause: {0}", isPaused);
            if (isPaused)
            {
                this.CancelMatchmakingSignal.Dispatch();
                if (this._ServerTimeoutCheck != null)
                {
                    StopCoroutine(_ServerTimeoutCheck);
                    _ServerTimeoutCheck = null;
                }
            }
            else
            {
                Debug.Log("Reconnect: Sending keepalive confimation");
                SendMessage(new KeepAliveMessage());
            }
#if UNITY_IOS || UNITY_ANDROID
			// If we're resuming from standby, referesh the friends list to get the latest statuses
			if(isPaused == false)
			{
                // ping the server - see if we're still connected

                // Reconnect to chat, if need be
				if(this.ConnectToPhotonChatSignal != null 
					&& (PlayerProfile.FriendListData != null && PlayerProfile.FriendListData.Count > 0) 
					&& (this.PlayFabChatClient == null 
						|| this.PlayFabChatClient.chatClient == null 
						|| this.PlayFabChatClient.chatClient.State == ExitGames.Client.Photon.Chat.ChatState.Disconnected))
				{
					Debug.LogWarning("Reconnecting to PhotonChat");
					this.ConnectToPhotonChatSignal.Dispatch(string.Empty, new ChatMessage(),false);
				}
			}
			else
			{
                Debug.Log("Background: Unloading assets");
                this.UnloadUnusedResourcesSignal.Dispatch();
				/*if(this.PlayFabChatClient != null 
					&& this.PlayFabChatClient.chatClient != null 
					&& this.PlayFabChatClient.chatClient.State != ExitGames.Client.Photon.Chat.ChatState.Disconnected)
				{
					Debug.LogWarning("Background: Disconnecting from Photon");
					this.PlayFabChatClient.chatClient.Disconnect();
				}*/
			}

			/*
			// Track analytics sessions
			if(this.PlayerProfile != null && this.PlayerProfile.isAdTrackingEnabled)
			{
				TuneSDK.Tune.MeasureSession();
				Fiksu.UploadPurchase(Fiksu.FiksuPurchaseEvent.EVENT2, 0, "USD");
			}*/
#endif
            }
        #endregion
    }
}