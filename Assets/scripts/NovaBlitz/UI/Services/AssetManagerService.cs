﻿using UnityEngine;
using UnityEngine.UI;
using LinqTools;
using System.Collections;
using System;
using System.Collections.Generic;
using NovaBlitz.UI;

namespace NovaBlitz
{
	public class AssetManagerService : MonoBehaviour 
	{
		class BaseAssetCache 
		{
			public string AssetPath {get; protected set;}
			public bool IsDoneLoading 
			{
				// True if we have a non null request and the request is done
				get { return this.Request != null && this.Request.isDone; }
			}
			public AssetBundleRequest Request = null;
            public HashSet<Texture> RawImages = new HashSet<Texture>();

			virtual public void InvokeCallbacks() {}	

            public void UpdateRawImages(Texture texture)
            {
                //RawImages.RemoveWhere(ri => ri == null);
                RawImages.Add(texture);
            }

            public void CleanRawImages()
            {
                RawImages.RemoveWhere(tex => tex == null);
            }
		}
		
		
		class AssetCache<T> : BaseAssetCache where T : UnityEngine.Object
		{
			// Asset, AssetPath callbacks
			public List<Action<T,string>> Callbacks = null;
			
			public AssetCache(string assetPath)
			{
				this.AssetPath = assetPath;
				this.Callbacks = new List<Action<T,string>>();	
			}
			
			public T Asset
			{
				get { if(this.IsDoneLoading) { return (T) this.Request.asset; } else { return null; } }
			}
			
			override public void InvokeCallbacks()
			{
				//Debug.LogError("Invoking Callbacks:" + this.AssetPath + " isDoneLoading:" + this.IsDoneLoading);
				if(this.Callbacks.Count == 0) { return; }
				
				for(int i=0;i<this.Callbacks.Count;i++)
				{
					this.Callbacks[i].Invoke((T)this.Asset, this.AssetPath);
				}
				
				this.Callbacks.Clear();
			}
		}
		
		private Dictionary<string,BaseAssetCache> assets = new Dictionary<string,BaseAssetCache>();
		private List<BaseAssetCache> assetsToLoad = new List<BaseAssetCache>();
		private IEnumerator assetLoaderEnumerator = null;
		
		// If whomever requested an asset no longer needs it,
		// they can call this method to unregister for it and also
		// remove it from the pending asset load queue.
		public void CancelLoadAsset<T>(Action<T,string> onAssetLoaded, string assetPath) where T : UnityEngine.Object
		{
			BaseAssetCache baseAsset = null;
            if (this.assets.TryGetValue(assetPath, out baseAsset))
            {
                AssetCache<T> asset = (AssetCache<T>)baseAsset;
                int countBefore = asset.Callbacks.Count;
                asset.Callbacks.Remove(onAssetLoaded);
                if (asset.Callbacks.Count == 0)
                {
                    this.assetsToLoad.Remove(asset);
                }
            }
            else
                Debug.LogFormat("Failed to cancel art load for {0}", assetPath);
		}

        public void UnlinkUnusedArtAssets()
        {
            // stop loading new assets
            /*if(assetLoaderEnumerator != null)
            {
                Timer.Instance.StopCoroutine(assetLoaderEnumerator);
                assetLoaderEnumerator = null;
            }
            assetsToLoad.Clear();*/
            int i = 0;
            
            // Unlink cache for existing assets
            foreach(var kvp in assets.ToList())
            {
                kvp.Value.CleanRawImages();
                if (kvp.Value.RawImages.Count == 0)
                {
                    assets.Remove(kvp.Key);
                    i++;
                }
            }
            Debug.LogFormat("Asset Manager cleaned {0} references. {1} remain.", i, assets.Count);
        }

		// Returns true if asset is ready
		public bool LoadAsset<T>(Texture destinationTexture, string assetPath, Action<T,string>onAssetLoaded) where T : UnityEngine.Object
		{
			if(string.IsNullOrEmpty(assetPath))
			{
				return false;
			}
			
			// Try to find the requested asset in the cache
			BaseAssetCache asset = null;
			if(this.assets.TryGetValue(assetPath, out asset))
            {
                asset.UpdateRawImages(destinationTexture);
                // Is the asset already loaded?
                if (asset.IsDoneLoading)
				{
					// If the asset is already loaded, invoke the callback and bail
					onAssetLoaded.Invoke((T)asset.Request.asset, assetPath);
					return true;
				}
				else
				{
					// If the asset isn't already loaded, add the callback to the existing cache item
					AssetCache<T> assetCache = (AssetCache<T>)asset;
					assetCache.Callbacks.Add(onAssetLoaded);
					
					// If the asset isn't currently loading, re add it to the loading queue
					if(this.assetsToLoad.Contains(assetCache) == false)
					{
						this.assetsToLoad.Insert(0,assetCache);
					}
				}
			}
			else
			{
				// Create a new AssetCache for the requested asset
				AssetCache<T> assetCache = new AssetCache<T>(assetPath);

                // And add the target
                assetCache.UpdateRawImages(destinationTexture);

                // and add the callback
                assetCache.Callbacks.Add(onAssetLoaded);
				
				// Add it to the end of assetsToLoad list
				this.assetsToLoad.Add(assetCache);
				
				// Also add it ot the asset cache;
				this.assets[assetPath] = assetCache;
			}
			
			// Kick off any async loading tasks that might need to be done
			this.LoadAsync();
			return false;
		}
		
		private void LoadAsync()
		{
			if(this.assetLoaderEnumerator == null && this.assetsToLoad.Count > 0)
			{
				this.assetLoaderEnumerator = this.DoAssetLoading();
				this.StartCoroutine(this.assetLoaderEnumerator);
			}
		}
		
		private IEnumerator DoAssetLoading()
		{
			// While there are pending assets to load
			while(this.assetsToLoad.Count > 0)
			{
				// Get the first one in the assetToLoad list
				BaseAssetCache cache = this.assetsToLoad[0];

                // Load its asset path async
                cache.Request = AssetBundleManager.Instance.LoadAssetAsync<Texture>(cache.AssetPath);// Resources.LoadAsync(cache.AssetPath);
                
				// Wait for the asset loading to complete (asyncronously)
				yield return cache.Request;
				
				// The AssetCache item can now inform any callbacks of the loaded asset
				cache.InvokeCallbacks();
				
				// Remove the AssetCache item from the asset to load list
				this.assetsToLoad.Remove(cache);

                // Wait an extra frame before loading another asset
                //yield return null;

                // Use this return to test weather the CardArtCallbakcs can deal with getting their objects yanked out from under them. (start a game while UI is loading art images)
                //yield return new WaitForSecondsRealtime(0.1f);
            }

            // If we got here we are out of assets to load!
            // Reset the coroutine enumerator reference so that the DoAssetLoading coRoutine will
            // be kicked off again when a new assets needs to be loaded
            this.assetLoaderEnumerator = null;
		}
	}
}
