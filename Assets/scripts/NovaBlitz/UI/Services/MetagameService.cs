﻿using UnityEngine;
using System.Collections;
using PlayFab;
using PlayFab.Events;
using NovaBlitz.Game;
using Messages;

namespace NovaBlitz.UI
{
	public class MetagameServices 
	{
		[Inject] public PlayFabErrorRaisedSignal PlayFabErrorRaisedSignal{get;set;}
		[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal{get;set;}
        [Inject] public ClientService ClientService { get; set; }

	    private PlayFabEvents _playFabEvents;
		public void Init(string titleID)
		{
			//PlayFabSettings.UseDevelopmentEnvironment = false;
		    PlayFabSettings.IsTesting = false;
			PlayFabSettings.TitleId = titleID;
			PlayFabSettings.LogLevel = PlayFabLogLevel.All;
		    _playFabEvents = PlayFabEvents.Init();
		    _playFabEvents.OnGlobalErrorEvent += PlayFabGlobalErrorHandler;
		}
		
		public void UnregisterGlobalErrorHandler()
		{
			//PlayFabSettings.GlobalErrorHandler = null;
		    _playFabEvents.OnGlobalErrorEvent -= PlayFabGlobalErrorHandler;
		}

		void PlayFabGlobalErrorHandler(object request, PlayFabError error) 
		{
			Debug.LogWarningFormat("ErrorCode:{0} message:{1} details:{2} httpcode: {3} httpstatus:{4}",
                error.Error, error.ErrorMessage, error.ErrorDetails, error.HttpCode, error.HttpStatus);
            /*
            if (error.HttpCode == 0)
			{
				ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.CouldNotConnect, I2.Loc.ScriptLocalization.Error.TimedOutMessage));
			}
			else */if(error.Error == PlayFabErrorCode.Unknown || error.Error == PlayFabErrorCode.UnknownError)
			{
				// for .NET exceptoins the message gets stored in the HttpStatus so show that instead of the message.. the message is a callstack
				ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.Unknown_Error, I2.Loc.ScriptLocalization.Error.Unknown_Error_Message));
                Debug.LogErrorFormat("Unknown Error: {0}", error.HttpStatus);
			}
            
            switch (error.Error)
            {
                case PlayFabErrorCode.AccountAlreadyLinked:
                case PlayFabErrorCode.AccountNotLinked:
                    return;
                default:
                    // send an error raised signal FIRST, unless we already returned becasue we want to ignore the error.
                    this.PlayFabErrorRaisedSignal.Dispatch(error);
                    switch (error.Error)
                    {
                        case PlayFabErrorCode.AccountBanned:
                            this.ClientService.ForceDisconnect(DisconnectCode.BlockedUser);
                            break;
                        case PlayFabErrorCode.ServiceUnavailable:
                        case PlayFabErrorCode.NotAuthorized:
                        case PlayFabErrorCode.UnableToConnectToDatabase:
                        case PlayFabErrorCode.ExpiredAuthToken:
                        case PlayFabErrorCode.InternalServerError:
                            this.ClientService.ForceDisconnect(DisconnectCode.PlayFabConnectionError);
                            break;
                    }
                    break;
            }

        }
	}
}
