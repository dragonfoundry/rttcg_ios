﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Purchasing;
using PlayFab;
using PlayFab.ClientModels;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
    public class NovaSteamStoreController// : UnityPurchasing, IStoreController
    {
        public Action<PlayFabErrorCode> SteamPurchasegError { get; set; }

        //private ConfigurationBuilder _builder;
        private string _catalogId;
        private string _orderId;
        private StoreData _currentProduct;

        //private readonly IStoreListener _listener = new NovaStoreListener();

        //private static NovaSteamStoreController _instance;

        public void Initialize(/*ConfigurationBuilder builder, */string catalogId)
        {
            //_instance = this;
            //this._builder = builder;
            this._catalogId = catalogId;
            //Initialize(_listener, builder);
            Debug.LogFormat("STEAM catalog initialized: {0}", catalogId);
        }

        public void InitiatePurchase(StoreData product)
        {
            var items = new List<ItemPurchaseRequest>()
            {
                new ItemPurchaseRequest()
                {
                    ItemId = product.ItemId,
                    Quantity = 1,
                    Annotation = "Client Purchase Request"
                }
            };

            PlayFabClientAPI.StartPurchase(new StartPurchaseRequest()
            {
                CatalogVersion = _catalogId,
                StoreId = product.StoreId,
                Items = items
            }, (result) =>
            {
                _orderId = result.OrderId;
                _currentProduct = product;
                var paymentOption = result.PaymentOptions.Find(po=>po.ProviderName == "Steam");
                //NOTE: We don't care about the success of this call, Steam will trigger the callbacks in the game
                //      once the purchase is completed and/or cancelled.
                PlayFabClientAPI.PayForPurchase(new PayForPurchaseRequest()
                {
                    ProviderName = paymentOption.ProviderName,
                    Currency = paymentOption.Currency,
                    OrderId = result.OrderId
                }, OnPurchaseSuccess, OnPurchaseFailed);
            }, OnPurchaseFailed);
        }

        public StoreData GetCurrentProduct()
        {
            return _currentProduct;
        }

        private void OnPurchaseSuccess(PayForPurchaseResult result)
        {

        }

        private void OnPurchaseFailed(PlayFabError error)
        {
            if (this.SteamPurchasegError != null)
                this.SteamPurchasegError.Invoke(error.Error);
            /*
            //TODO: Better error handling.
            Debug.Log(string.Format("PlayFabError: {0}", error.ErrorMessage));
            if (error.ErrorDetails == null) { return; }
            foreach (var kvp in error.ErrorDetails)
            {
                foreach (var e in kvp.Value)
                {
                    Debug.Log(e);
                }
            }*/
        }

        public void ConfirmPendingPurchase(StoreData product, Action<StoreData> callback)
        {
            PlayFabClientAPI.ConfirmPurchase(new ConfirmPurchaseRequest()
            {
                OrderId = _orderId
            }, (confirmResult) =>
            {
                callback(product);
                //TODO: Do i do anything with this data?
                _orderId = string.Empty;
                _currentProduct = null;
            }, OnPurchaseFailed);
        }

        /*public static void SetProducts(ProductCollection products)
        {
            _instance.products = products;
        }

        #region Not Implemented.
        public void ConfirmPendingPurchase(Product product)
        {
            throw new System.NotImplementedException();
        }

        public void InitiatePurchase(string productId)
        {
            throw new System.NotImplementedException();
        }

        public void InitiatePurchase(Product product, string payload)
        {
            throw new System.NotImplementedException();
        }

        public void InitiatePurchase(string productId, string payload)
        {
            throw new System.NotImplementedException();
        }
        #endregion*/

        //public ProductCollection products { get; private set; }
    }

    /*public class NovaStoreListener : IStoreListener
    {
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            return new PurchaseProcessingResult();
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            NovaSteamStoreController.SetProducts(controller.products);
        }
    }*/
}