﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using LinqTools;
using System.Text;

namespace NovaBlitz.UI
{
    /*
    public class ChillingoService : MonoBehaviour
    {
        [Inject] public AgeGatePassedSignal AgeGatePassedSignal { get; set; }
		[Inject] public PlayerProfile PlayerProfile { get; set; }

        public const string CHILLINGO_SERVICE = "ChillingoServiceObject";

		public bool? IsAgeVerificationNeeded = null;
        private bool? _IsAgeGatePassed = null;
        public bool? IsAgeGatePassed
        {
            get
            {
                return _IsAgeGatePassed;
            }
            private set
            {
                _IsAgeGatePassed = value;
				if (PlayerProfile != null)
					this.PlayerProfile.isAgeGatePassed = value;
				else
					Debug.Log ("#ChillingoService# Player Profile null");
            }
        }
		//public bool? IsAgeVerificationCriteriaMet = null;

        void Awake()
        {
            this.name = CHILLINGO_SERVICE;
//#if UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE
            // Register for the Terms ageVerificationCriteriaMet callback
            Terms.registerForTermsNotification(Terms.AgeVerificationCallbackIdentifier.CRITERIA_MET, CHILLINGO_SERVICE, "OnAgeVerificationCriteriaMet");

            // Register for the Terms ageVerificationCriteriaNotMet callback
            Terms.registerForTermsNotification(Terms.AgeVerificationCallbackIdentifier.CRITERIA_NOT_MET, CHILLINGO_SERVICE, "OnAgeVerificationCriteriaNotMet");

            // Register for the Terms ageVerificationPendingDialogDisplay callback
            Terms.registerForTermsNotification(Terms.AgeVerificationCallbackIdentifier.PENDING_DIALOG_DISPLAY, CHILLINGO_SERVICE, "OnAgeVerificationPendingDialogDisplay");

            // Register for the Offers pauseDrawing callback
            Offers.registerForOffersNotification(Offers.OffersCallbackIdentifier.PAUSEDRAWING, CHILLINGO_SERVICE, "OnPauseDrawing");

            // Register for the Offers pauseDrawing callback
            Offers.registerForOffersNotification(Offers.OffersCallbackIdentifier.RESTARTDRAWING, CHILLINGO_SERVICE, "OnRestartDrawing");
            
            // Register for the Offers offersRelease callback
            Offers.registerForOffersNotification(Offers.OffersCallbackIdentifier.OFFERSRELEASED, CHILLINGO_SERVICE, "OnOffersReleased");

            // Register for the Offers offersClosed callback
            Offers.registerForOffersNotification(Offers.OffersCallbackIdentifier.OFFERSCLOSED, CHILLINGO_SERVICE, "OnOffersClosed");

            Debug.Log("Chillingo Services Registered");
            this.StartCoroutine(WaitOneFrameCoroutine());
//#endif
            //this.OnApplicationStart();
        }

        public IEnumerator WaitOneFrameCoroutine()
        {
            yield return null;
            //yield return new WaitForSecondsRealtime(3);
            OnApplicationStart();
        }

        public void OnApplicationStart()
        {
            //Debug.Log("#Chillingo# Initializing Chillingo terms and offers");

            // Was the application published before 1st July 2013?
            // If so the preCOPPA option should be set to 'true'
            bool preCOPPA = false;

            // Has your producer specified that a custom skin would be more appropriate?
            // If so then set the useCustomSkin to 'true' and add the required assets 
            // to the XCode project once output
            bool useCustomSkin = false;
            
            // Initialise the Terms.  
            // This will call the Objective-C code when running on device
            Terms.initialiseTermsSession(preCOPPA, useCustomSkin, Terms.ComplianceLevel.AGE_SENSETIVE);

            // Initialise the offers with the 'universal' theme and in portrait only.
            Offers.initialiseOffersSession("universal", Offers.OffersInterfaceOrientationMask.OffersInterfaceOrientationMaskLandscapeAll, Offers.OffersAndroidStoreType.GOOGLE_PLAY);
            //Debug.Log("#Chillingo# Terms and offers initialized");

        }

        /// <summary>
        /// Maybe on the Defeat screen? It'd be a good place for Offers...
        /// </summary>
        public void OnEnterFrontEnd()
        {
            // Show the UI from the bottom right
//#if UNITY_ANDROID || UNITY_IOS
            Debug.Log("#Chillingo# Activating Offers");
            Offers.activateOffersUI(Offers.OffersCornerToDisplayFrom.BottomRight);
//#endif
        }

        /// <summary>
        /// Maybe on the Defeat screen? It'd be a good place for Offers...
        /// </summary>
        public void OnExitFrontEnd()
        {
            // Deactivate the UI.  
//#if UNITY_ANDROID || UNITY_IOS
            Debug.Log("#Chillingo# Deactivating offers");
            Offers.deactivateOffersUI();
//#endif
        }

        /// <summary>
        /// Show the Terms ???
        /// </summary>
        public void CheckTerms()
        {
            //bool doWeNeedToShowTheTermsUIFlag = true;
            // Check to see if we need to show the UI
			//Debug.Log("#Chillingo# waiting for age verification check");
			StartCoroutine (WaitForTermsCheck());

        }

		public IEnumerator WaitForTermsCheck()
		{
            if ((Application.platform != RuntimePlatform.IPhonePlayer) && (Application.platform != RuntimePlatform.Android))
            {
                IsAgeVerificationNeeded = false;
                IsAgeGatePassed = true;
            }
            else
            {
                while (this.IsAgeVerificationNeeded == null)
                {
                    yield return null;
                }
            }
                //Debug.Log("#Chillingo# Checking Terms");
                if (IsAgeVerificationNeeded == true)
                {
                    // Show the Terms UI  
                    // This will call the Objective-C code when running on device
                    //Debug.Log("#Chillingo# Showing Terms UI");
                    //#if UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE
                    Terms.showTermsUI();
                    //#endif
                    IsAgeGatePassed = null;
                }
                else
                {
                    Debug.Log("#Chillingo# Not showing terms UI");

                    yield return null;
                    this.AgeGatePassedSignal.Dispatch();
                }
            

		}

        public void OnApplicationPause(bool isPaused)
        {
            if(isPaused)
            {
                Offers.closeOffersSession();
                //Terms.closeTermsSession();
            }
            else
            {
                Offers.initialiseOffersSession("universal", Offers.OffersInterfaceOrientationMask.OffersInterfaceOrientationMaskLandscapeAll, Offers.OffersAndroidStoreType.GOOGLE_PLAY);
            }
        }

        public void OnApplicationQuit()
        {
//#if UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE
            // Close the Terms session
            // This will call the Objective-C code
            Debug.Log("#Chillingo# closing Chillingo session");

            Terms.closeTermsSession();
            // Close the Offers session
            // This will call the Objective-C code
            Offers.closeOffersSession();
//#endif
        }
        

        public void OnAgeVerificationCriteriaMet(string message)
        {
            // Enable telemetry and/or advertising API's
            Debug.Log("#Chillingo# [ChillingoServiceObject] OnAgeVerificationCriteriaMet message received");
			this.IsAgeVerificationNeeded = false;

			IsAgeGatePassed = true;
            this.AgeGatePassedSignal.Dispatch();
        }

        public void OnAgeVerificationCriteriaNotMet(string message)
		{
			Debug.Log ("#Chillingo# [ChillingoServiceObject] OnAgeVerificationCriteriaNotMet message received");
			this.IsAgeVerificationNeeded = false;
			IsAgeGatePassed = false;
            this.AgeGatePassedSignal.Dispatch();

            // Account creation becomes "no email, pre-gen username == PlayFab Id";
            // Turn off "log out" button
            // Turn off "forgot password" link

            
			// * OR: if the user is "under 13", their email address is hashed before registering it. 
			// * They won't be able to reset passwords...
			// * And username is skipped, so it'll be blank, and we can pull playFab Id for username. 
			// * "Forgot password" on login is skipped. And "forgot password" in game is skipped.
			// * "Log out" will work properly.
			// * To hash the email, hash it, then append "@hashed.novablitz", so it registers as a real email address.
			



            // turn off chat window in friends view (but allow challenges)

            // Disable telemetry, etc.
        }      
 
        public void OnAgeVerificationPendingDialogDisplay(string message)
        {
            Debug.Log("#Chillingo# [ChillingoServiceObject] OnAgeVerificationPendingDialogDisplay message received");
            IsAgeGatePassed = null;
            this.IsAgeVerificationNeeded = true;
        }

        public void OnPauseDrawing()
        {
            Debug.Log("#Chillingo# [ChillingoServiceObject] OnPauseDrawing message received");

        }

        public void OnRestartDrawing()
        {
            Debug.Log("#Chillingo# [ChillingoServiceObject] OnRestartDrawing message received");

        }

        public void OnOffersReleased()
        {
            Debug.Log("#Chillingo# [ChillingoServiceObject] OnOffersReleased message received");

        }

        public void OnOffersClosed()
        {
            Debug.Log("#Chillingo# [ChillingoServiceObject] OnOffersClosed message received");

        }
    }*/
}
