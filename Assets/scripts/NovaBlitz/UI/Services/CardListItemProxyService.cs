﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LinqTools;

namespace NovaBlitz.UI
{
    public class CardListItemProxyService : MonoBehaviour
    {
        public delegate void ProxyLoadedCallback(Texture2D listItemTexture);
		[Inject] public AssetManagerService AssetManagerService {get;set;}
		[Inject] public UIConfiguration UIConfiguration { get; set; }
		public RenderTexture RenderTexture;

        public CardListItemView renderableCardListItem;
		private Texture2D tempCardArtTexture;
		private IEnumerator itemLoadingEnumerator = null;
        private List<ListItemProxyData> itemsToRender = new List<ListItemProxyData>();
		private ListItemProxyData currentlyRenderedItem = null;

        private class ListItemProxyData
        {
			public CardListItem CardListItem;
			public CardData CardData;
			public int MaxOfEachCard;
			public ProxyLoadedCallback ProxyLoadedCallback;
			//public GameObject TargetObject;
        }

		IEnumerator Start()
		{
			while(this.UIConfiguration == null) yield return 0;

			string prefabToLoad = this.UIConfiguration.IsSmallScreen ? "CardListItemPhoneRenderTextureCamera" : "CardListItemRenderTextureCamera";

			GameObject cardListItemRenderTextureCameraPrefab = Resources.Load(prefabToLoad) as GameObject;
			GameObject cardListItemRenderGO = GameObject.Instantiate(cardListItemRenderTextureCameraPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			cardListItemRenderGO.transform.position = new Vector3(-145f,0,100f);

			// Set up local variables that reference the Render to Texture Camera
			renderableCardListItem = cardListItemRenderGO.GetComponentInChildren<CardListItemView>();
			RenderTexture = cardListItemRenderGO.GetComponentInChildren<Camera>().targetTexture;
			cardListItemRenderGO.GetComponent<OnPostRenderHook>().PostRenderCallback += this.OnPostRender;
		}

		///<summary>
		/// Workhorse function of the ProxyService, given the data required to render a cardListItem this method will render the
		/// cardListItem and signal the caller via a callback method that the item has been rendered and provide the texture to use
		///</summary>
        public void GetProxyImage(CardListItem cardListItem, CardData cardData, int maxOfEachCard, ProxyLoadedCallback onProxyLoaded, bool isPriority=false)
        {
			ListItemProxyData listItemData = new ListItemProxyData() { CardListItem=cardListItem, 
																		CardData=cardData, 
																		MaxOfEachCard=maxOfEachCard, 
																		ProxyLoadedCallback=onProxyLoaded };

			if(this.tempCardArtTexture == null)
				this.tempCardArtTexture = new Texture2D(210,280);

			AssetManagerService.LoadAsset<Texture>(this.tempCardArtTexture, cardData.ArtID, 
			(tex,path)=>{
				if(isPriority)
				{
					this.itemsToRender.Insert(0,listItemData);
				}
				else
				{
					this.itemsToRender.Add(listItemData);
				}
				this.RenderAsync();
			});
            return;
        }	

		///<summary>
		/// Remove any pending proxy image render requests for the specified cardListItem if any are in queue
		///</summary>
		public void CancelProxyImageRequest(CardListItem cardListItem)
		{
			ListItemProxyData proxyData = this.itemsToRender.Where((o)=>o.CardListItem == cardListItem).FirstOrDefault();
			if(proxyData != null && proxyData != this.currentlyRenderedItem)
			{
				this.itemsToRender.Remove(proxyData);
			}
		}

		///<summary>
		/// Private utility function that kicks off the item rendering coroutine if it isn't already started
		///</summary>
		private void RenderAsync()
		{
			if(this.itemLoadingEnumerator == null && this.itemsToRender.Count > 0)
			{
				this.itemLoadingEnumerator = this.DoItemRendering();
				this.StartCoroutine(this.itemLoadingEnumerator);
			}
		}

		///<summary>
		/// Copies the rendered contents of the renderTexture out to a newly allocated Texture2D
		/// making sure to disable mipampping on the new texture so the results don't appear filtered/blurry.
		/// Once the new texture is initialized this method invokes the ProxyLoadedCallback to let the caller
		/// know the texture has been asyncronoulys generated.
		///<summary>
		void OnPostRender()
		{
			// If the currentlyRenderedItem variable is set....
			if(this.itemsToRender.Contains(this.currentlyRenderedItem))
			{
				// Copy the texture data from the render texture
				Texture2D tex2D = new Texture2D(this.RenderTexture.width, this.RenderTexture.height, TextureFormat.RGBA32, false, false);
				tex2D.ReadPixels(new Rect(0,0, this.RenderTexture.width, this.RenderTexture.height),0, 0,false);
				tex2D.Apply();

				// Invoke the onProxyLoaded Callback with a refernce to the rendered texture
				if(this.currentlyRenderedItem.ProxyLoadedCallback != null)
					this.currentlyRenderedItem.ProxyLoadedCallback.Invoke(tex2D);

				// Remove the currentlyRenderedItem from the itemsToRender list so we can get going rendering other pending items
				this.itemsToRender.Remove(this.currentlyRenderedItem);
			}
		}
		
		///<summary>
		/// Coroutine that handles staging of queued items waiting to be rendered
		///</summary>
		private IEnumerator DoItemRendering()
		{
			// While there are pending assets to load
			while(this.itemsToRender.Count > 0)
			{
				// Get the first itemdata in the itemsToRender list
				ListItemProxyData itemData = this.itemsToRender[0]; 
				
				// Load it into the CardListItemView
				this.renderableCardListItem.Initialize(itemData.CardListItem, itemData.CardData, itemData.MaxOfEachCard);
                
				// Make sure the renderTexture is the active one
				RenderTexture.active = this.RenderTexture;

				// Wait until the end of frame before moving on, giving OnPostRender a chance to render the texture
				yield return 0;

				// Only set the currentlyRenderedItem variable once we're sure the item is rendered
				this.currentlyRenderedItem = itemData;
			}
			
			// If we got here we are out of listItems to render!
			// Reset the coroutine enumerator reference so that the DoItemRendering coRoutine will
			// be kicked off again when a new item needs to be rendered
			this.itemLoadingEnumerator = null;
		}
    }
}