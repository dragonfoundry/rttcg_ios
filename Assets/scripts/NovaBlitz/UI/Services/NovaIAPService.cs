﻿using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using UnityEngine.Purchasing.Default;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json.Linq;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using Messages;
#if UNITY_STEAM
using Steamworks;
#endif

namespace NovaBlitz.UI
{
    public class NovaIAPService : MonoBehaviour, IStoreListener
    {
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal { get; set; }
        [Inject] public ShowPurchaseOffersSignal ShowPurchaseOffersSignal { get; set; }
        [Inject] public LogEventSignal LogEventSignal { get; set; }
        [Inject] public RateGameSignal RateGameSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }
        [Inject] public MoneyPurchaseSignal MoneyPurchaseSignal { get; set; }
        [Inject] public BeginVirtualPurchaseSignal BeginVirtualPurchaseSignal { get; set; }
        [Inject] public ZendeskTicketSignal ZendeskTicketSignal { get; set; }
        /*[Inject] public PurchaseStartedSignal PurchaseStartedSignal { get;set;}
        [Inject] public PurchaseInStoreSignal PurchaseInStoreSignal { get;set;}
        [Inject] public PurchaseCompletedSignal PurchaseCompletedSignal { get;set;}*/

        private IStoreController controller;
        public NovaSteamStoreController SteamController { get; set; }
        private IExtensionProvider extensions;
        private IAppleExtensions m_AppleExtensions;

        public IStoreController Controller { get { return controller; } }

        private bool m_PurchaseInProgress;

        private PurchaseEventArgs thisArgs;

        public const string ANNUITY_NANOBOT = "annuity_nanobot";
        public const string ANNUITY_CREDIT = "annuity_credit";
		public const string ANNUITY_SMARTPACK = "annuity_smartpack";
        public const string ANNUITY_STEAM = "annuity_steam";
        public const string ANNUITY_GEM = "annuity_gem";
		public const string MONTH_NANOBOT = "month_nanobot";
		public const string MONTH_CREDIT = "month_credit";
		public const string MONTH_SMARTPACK = "month_smartpack";
		public const string MONTH_GEM = "month_gem";
        public const string GEMS_01 = "gems_01";
        public const string GEMS_02 = "gems_02";
        public const string GEMS_03 = "gems_03";
        public const string GEMS_04 = "gems_04";
        public const string GEMS_05 = "gems_05";
        public const string GEMS_06 = "gems_06";
        public const string CREDITS_01 = "credits_01";
        public const string CREDITS_02 = "credits_02";
        public const string CREDITS_03 = "credits_03";
        public const string NANOBOTS_01 = "nanobots_01";
        public const string NANOBOTS_02 = "nanobots_02";
        public const string NANOBOTS_03 = "nanobots_03";
        public const string SMARTPACKS_01 = "smartpacks_01";
        public const string DRAFTS_01 = "drafts_01";
        public const string OFFER_01 = "offer_01";
        public const string OFFER_02 = "offer_02";
        public const string OFFER_03 = "offer_03";
        public const string OFFER_04 = "offer_04";
        public const string OFFER_05 = "offer_05";
        public const string OFFER_06 = "offer_06";
        public const string OFFER_07 = "offer_07";
        public const string OFFER_08 = "offer_08";
        public const string OFFER_09 = "offer_09";
        public const string OFFER_10 = "offer_10";

        public readonly List<string> ProductList = new List<string>
        {
            ANNUITY_NANOBOT,
            ANNUITY_CREDIT,
            ANNUITY_SMARTPACK,
            GEMS_01,
            GEMS_02,
            GEMS_03,
            GEMS_04,
            GEMS_05,
            GEMS_06,
            CREDITS_01,
            CREDITS_02,
            CREDITS_03,
            NANOBOTS_01,
            NANOBOTS_02,
            NANOBOTS_03,
            SMARTPACKS_01,
            DRAFTS_01,
            OFFER_01,
            OFFER_02,
            OFFER_03,
            OFFER_04,
            OFFER_05,
            OFFER_06,
            OFFER_07,
            OFFER_08,
            OFFER_09,
            OFFER_10,
        };

        public const string IOS_ITEM_PREFIX = "";
		public const string GP_ITEM_PREFIX = "";

        public const string STORE_ITEM_NAME = "StoreItemName/";
        public const string STORE_ITEM_SHORT = "StoreItemShortDescription/";
        public const string STORE_ITEM_LONG = "StoreItemLongDesc/";

        private static NovaIAPService _instance;

        public void Awake()
        {
            _instance = this;
            m_PurchaseInProgress = false;
#if UNITY_STEAM
            Debug.Log("IAP: Initializing Steam Store Controller");
            SteamController = new NovaSteamStoreController();
            SteamController.SteamPurchasegError += OnSteamPurchaseError;
            var catalogId = "1"; //TODO: Populate from where you store the catalog id.
            SteamController.Initialize(catalogId);
#elif UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
            return;
            var module = StandardPurchasingModule.Instance();

            // The FakeStore supports: no-ui (always succeeding), basic ui (purchase pass/fail), and 
            // developer ui (initialization, purchase, failure code setting). These correspond to 
            // the FakeStoreUIMode Enum values passed into StandardPurchasingModule.useFakeStoreUIMode.
            module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

            Debug.Log("IAP: Generating Builder");
            var builder = ConfigurationBuilder.Instance(module);
            // This enables the Microsoft IAP simulator for local testing.
            // You would remove this before building your release package.
            //builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = true;
            //builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2O/9/H7jYjOsLFT/uSy3ZEk5KaNg1xx60RN7yWJaoQZ7qMeLy4hsVB3IpgMXgiYFiKELkBaUEkObiPDlCxcHnWVlhnzJBvTfeCPrYNVOOSJFZrXdotp5L0iS2NVHjnllM+HA1M0W2eSNjdYzdLmZl1bxTpXa4th+dVli9lZu7B7C2ly79i/hGTmvaClzPBNyX+Rtj7Bmo336zh2lYbRdpD5glozUq+10u91PMDPH+jqhx10eyZpiapr8dFqXl5diMiobknw9CgcjxqMTVBQHK6hS0qYKPmUDONquJn280fBs1PTeA6NMG03gb9FLESKFclcuEZtvM8ZwMMRxSLA9GwIDAQAB");

            Debug.Log("IAP: Builder Generated");
            
            foreach (var str in ProductList)
            {
                var appStoreString = str;
                if (str == ANNUITY_CREDIT)
                    appStoreString = MONTH_CREDIT;
                else if (str == ANNUITY_NANOBOT)
                    appStoreString = MONTH_NANOBOT;
                else if (str == ANNUITY_SMARTPACK)
                    appStoreString = MONTH_SMARTPACK;

                builder.AddProduct(str, ProductType.Consumable, new IDs {
                    {IOS_ITEM_PREFIX + appStoreString, AppleAppStore.Name},
                    {GP_ITEM_PREFIX + appStoreString, GooglePlay.Name}
                });
            }

            // Write Amazon's JSON description of our products to storage when using Amazon's local sandbox.
            // This should be removed from a production build.
            //builder.Configure<IAmazonConfiguration>().WriteSandboxJSON(builder.products);

            UnityPurchasing.Initialize(this, builder);
#endif
        }

        /// <summary>
        /// Called when Unity IAP is ready to make purchases.
        /// </summary>
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("Nova IAP initialized");
            this.controller = controller;
            this.extensions = extensions;
            ListUnityIAPs();
            m_AppleExtensions = this.extensions.GetExtension<IAppleExtensions>();


            // On Apple platforms we need to handle deferred purchases caused by Apple's Ask to Buy feature.
            // On non-Apple platforms this will have no effect; OnDeferred will never be called.
            m_AppleExtensions.RegisterPurchaseDeferredListener(OnDeferred);

            // TODO - populate the store view & set buttons to active.
            // this.PopulateStoreSignal.Dispatch();

			// Ensure every product has its data
			if (GameData != null && GameData.StoreCache != null)
			{
				foreach (var store in GameData.StoreCache.Values)
				{
					foreach (var storeItem in store.Values)
					{
						Product UnityProduct = null;
						if (this != null
						   && Controller != null
						   && Controller.products != null
						   && Controller.products.all.Count () > 0)
						{
							UnityProduct = Controller.products.WithID (storeItem.ItemId);
							storeItem.UnityPurchasingProduct = UnityProduct;
							Debug.LogFormat ("IAP init: Product for item {0} is {1}", storeItem.ItemId, 
								UnityProduct == null ? "Null" : "id:" + UnityProduct.definition.id + "/storeId:" + UnityProduct.definition.storeSpecificId);
						}
					}
				}
			}
        }

        public void OnDestroy()
        {
#if UNITY_STEAM
            SteamController.SteamPurchasegError -= OnSteamPurchaseError;
                #endif
        }

        public void OnSteamPurchaseError(PlayFabErrorCode errorCode)
        {
            switch(errorCode)
            {
                case PlayFabErrorCode.FailedByPaymentProvider:
                    this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams { Title = I2.Loc.ScriptLocalization.Error.RateLimited, Message = I2.Loc.ScriptLocalization.Error.RateLimitedExplanation });
                    break;
                case PlayFabErrorCode.InvalidPaymentProvider:
                default:
                    this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams { Title = I2.Loc.ScriptLocalization.Failed, Message = I2.Loc.ScriptLocalization.Error.Purchase_Failed });
                    break;
            }
            m_PurchaseInProgress = false;
        }

        /// <summary>
        /// Called when Unity IAP encounters an unrecoverable initialization error.
        ///
        /// Note that this will not be called if Internet is unavailable; Unity IAP
        /// will attempt initialization until it becomes available.
        /// </summary>
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("Billing failed to initialize! " + error.ToString());
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    // Ask the user if billing is disabled in device settings.
                    Debug.Log("Billing disabled!");
                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    // Developer configuration error; check product metadata.
                    Debug.Log("No products available for purchase!");
                    break;
            }
        }

        /// <summary>
        /// Start the purchase flow from the store, and other areas; direct to Steam or Unity Purchasing as appropriate
        /// </summary>
        public void StartPurchaseFlow(StoreData storeData)
        {
#if UNITY_STEAM
            if(!SteamUtils.IsOverlayEnabled())
            {
                Debug.Log("purchase couldn't be completed - no overlay enabled");
                this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.OverlayDisabled, I2.Loc.ScriptLocalization.Error.OverlayEnable, false));
                return;
            }
#endif
            if (m_PurchaseInProgress)
            {
                this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.Purchase_Failed, I2.Loc.ScriptLocalization.Error.PurchaseInProgress, false));
                Debug.Log("purchase in progress");
                return;
            }
            this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(string.Empty, string.Empty, true));

            m_PurchaseInProgress = true;
#if UNITY_STEAM
            // Use Steam purchasing
            if (storeData == null || string.IsNullOrEmpty(storeData.ItemId))
            {
                Debug.Log("null purchase data");
                return;
            }
            SteamController.InitiatePurchase(storeData);
#else
            // Use Unity Purchasing
            if(storeData == null || storeData.UnityPurchasingProduct == null)
            {
                Debug.Log("null purchase data");
                return;
            }
			Debug.Log ("Initiating purchase");
            Controller.InitiatePurchase(storeData.UnityPurchasingProduct);
            //this.PurchaseStartedSignal.Dispatch();
#endif

        }

        public void OnStoreWindowOpened()
        {

            //this.PurchaseInStoreSignal.Dispatch();
        }

        /// <summary>
        /// Called when a purchase completes.
        ///
        /// May be called at any time after OnInitialized().
        /// </summary>
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            //this.PurchaseCompletedSignal.Dispatch();
            //Debug.Log ("ProcessPurchase: Start");
            if(args == null)
            {
                Debug.Log("Purchasing: Args are null");
                m_PurchaseInProgress = false;
                return PurchaseProcessingResult.Complete;
            }
            
            Debug.LogFormat("ProcessPurchase: Start: {0}. Receipt: {1}", args.purchasedProduct.definition.id, args.purchasedProduct.receipt);
            //Debug.Log("Receipt: " + args.purchasedProduct.receipt);
            
            // RECEIPT VALIDATION HERE
            try
            {
                JObject receiptJObject = JObject.Parse(args.purchasedProduct.receipt);
                JToken storeNameJtoken;
                receiptJObject.TryGetValue("Store", out storeNameJtoken);
				        //Debug.Log("Store Token: " + storeNameJtoken.ToString());
                JToken transatctionIdJtoken;
                receiptJObject.TryGetValue("TransactionID", out transatctionIdJtoken);
                //Debug.Log("Transaction Token: " + transatctionIdJtoken.ToString());
                JToken payloadJtoken;
                receiptJObject.TryGetValue("Payload", out payloadJtoken);
                //Debug.Log("Payload Token: " + payloadJtoken.ToString());

                if (payloadJtoken == null)
                    throw new DataMisalignedException();


                var wrapper = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(args.purchasedProduct.receipt);
                string storeString = (string)wrapper["Store"];
                string transactionIdString = (string)wrapper["Store"];
                string payloadString = (string)wrapper["Store"];

                if (payloadString == null)
                    throw new DataMisalignedException(string.Format("Payload is null for Store:{0}, transactionid:{1}", storeString, transactionIdString));

                var itemId = args.purchasedProduct.definition.id;
                uint price = 0;
                StoreData storeData;
                Dictionary<string,StoreData> store;
                if(this.GameData.StoreCache.TryGetValue(NBEconomy.STORE_MAIN, out store)
                    && store.TryGetValue(itemId, out storeData))
                {
                    storeData.VirtualPrices.TryGetValue(CurrencyType.RM.ToString(), out price);
                }

                switch ((string)storeNameJtoken)
                {
                    case GooglePlay.Name:
                        {
                            var gpDict = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(payloadString);
                            string jsonString = (string)gpDict["json"];
                            string signatureString = (string)gpDict["signature"];
                            Debug.LogFormat("Validating Google Play Purchase from dictionary. \nReceiptJson:{0} Signature:{1}", jsonString, signatureString);

                            JObject googlePayload = JObject.Parse((string)payloadJtoken);
                            
                            JToken dataO;
                            googlePayload.TryGetValue("json", out dataO);
                            JToken signatureO;
                            googlePayload.TryGetValue("signature", out signatureO);

                            var request = new ValidateGooglePlayPurchaseRequest
                            {
                                ReceiptJson = (string)dataO,
                                CurrencyCode = "USD",
                                PurchasePrice = price,
                                Signature = (string)signatureO
                            };
                            Debug.LogFormat("Validating Google Play Purchase. \nReceiptJson:{0} Signature:{1}", request.ReceiptJson, request.Signature);
                            PlayFabClientAPI.ValidateGooglePlayPurchase(request, OnGooglePurchaseValidated, OnValidationFailed, args);
                            this.LogEventSignal.Dispatch(new LogEventParams{
                              EventName = LogEventParams.EVENT_PURCHASE, 
                              EventValue = (int)request.PurchasePrice, 
                              ISOCurrencyCode = request.CurrencyCode,
                              ItemId = args.purchasedProduct.definition.id});
                            break;
                        }
                        case AppleAppStore.Name:
                        {
						                Debug.Log("Validation on IOS");


                            var request = new ValidateIOSReceiptRequest
                                {
                                    ReceiptData = (string)payloadJtoken,
                                    CurrencyCode = "USD",
                                    PurchasePrice = (int)price,
                                };
                            PlayFabClientAPI.ValidateIOSReceipt(request, OnIOSPurchaseValidated, OnValidationFailed, args);
                            this.LogEventSignal.Dispatch(new LogEventParams{
                              EventName = LogEventParams.EVENT_PURCHASE, 
                              EventValue = request.PurchasePrice, 
                              ISOCurrencyCode = request.CurrencyCode,
                              ItemId = args.purchasedProduct.definition.id});
                            break;
                        }
                    /*case AmazonApps.Name:
                        {
                            var request = new ValidateAmazonReceiptRequest
                            {
                                ReceiptId =,
                                UserId =,
                                CurrencyCode = "USD",
                                PurchasePrice = price
                            };
                            break;
                        }
                    case WindowsStore.Name:
                        {
                            break;
                        }*/
                    default:
					              Debug.Log("Store validation: Store not found");
                        throw new NotImplementedException();
                 
                }
            }
            catch (Exception except)
            {
              try {

                  m_PurchaseInProgress = false;
                  Debug.Log("Invalid receipt, not unlocking content. Exception: " + except.ToString());
                  this.CloseAnimatedViewSignal.Dispatch(typeof(NewMessageDialogView));
                  ReportFailedPurchase(args != null && args.purchasedProduct != null ? args.purchasedProduct.receipt : "null product", "exception");
                  return PurchaseProcessingResult.Complete;
              }
              catch (Exception exception) {
                Debug.Log("Exception during invalid receipt logging. Exception: " + exception.ToString());
                return PurchaseProcessingResult.Complete;
              }
            }

            return PurchaseProcessingResult.Pending;
        }

        public void ReportFailedPurchase(string token, string tag)
        {
            var ticket = new ZendeskTicket
            {
                ContactType = "purchasefailure",
                Description = token,
                Subject = string.Format("Purchase Failure [From: {0} ({1})]",
                    this.PlayerProfile.MetaProfile.Name,
                    this.PlayerProfile.MetaProfile.PlayFabID),
                Happiness = -1,
                EmailAddress = this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo.Email
            };
            ticket.Tags.Add("purchasing");
            ticket.Tags.Add(tag);
            this.ZendeskTicketSignal.Dispatch(ticket, ReportType.Purchasing, new List<ChatMessage>());
        }

        public void OnGooglePurchaseValidated(ValidateGooglePlayPurchaseResult result)
        {
            m_PurchaseInProgress = false;
            var args = result.CustomData as PurchaseEventArgs;
            if (args != null)
            {
                controller.ConfirmPendingPurchase(args.purchasedProduct);
			}
            // call to the server to tell it to re-pull inventory
            OnPurchaseSuccessful(args.purchasedProduct.definition.id, (result.Request as ValidateGooglePlayPurchaseRequest).ReceiptJson, (result.Request as ValidateGooglePlayPurchaseRequest).Signature);
        }

        public void OnIOSPurchaseValidated(ValidateIOSReceiptResult result)
        {
			Debug.Log ("IOS purchase validated");
            m_PurchaseInProgress = false;
            var args = result.CustomData as PurchaseEventArgs;
            if (args != null)
            {
				Debug.Log ("Confirming pending purchase");
                controller.ConfirmPendingPurchase(args.purchasedProduct);
			}
            // call to the server to tell it to re-pull inventory
            OnPurchaseSuccessful(args.purchasedProduct.definition.id, (result.Request as ValidateIOSReceiptRequest).ReceiptData, null);
        }


        public void OnValidationFailed(PlayFabError error)
        {
			Debug.LogFormat ("Validation Failed. Code:{0} Message:{1} Details:{2}",error.Error, error.ErrorMessage, error.ErrorDetails);
			m_PurchaseInProgress = false;
			var args = error.CustomData as PurchaseEventArgs;
			if (args != null)
			{
				Debug.LogFormat ("Failed to validate pending purchase id:{0} title:{1}", args.purchasedProduct.definition.id, args.purchasedProduct.metadata.localizedTitle);
                //controller.ConfirmPendingPurchase(args.purchasedProduct);
                ReportFailedPurchase(args.purchasedProduct.receipt, "validation");
            }
            this.CloseAnimatedViewSignal.Dispatch(typeof(NewMessageDialogView));
        }

        /// <summary>
        /// Called when a purchase fails.
        /// </summary>
        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
			//Debug.Log ("Purchase Failed");
            Debug.LogErrorFormat("Purchase failed. Id: {0}. Store Id: {1}. Reason: {2}", i.definition.id, i.definition.storeSpecificId, p);

            m_PurchaseInProgress = false;

            if (p == PurchaseFailureReason.PurchasingUnavailable)
            {
                // IAP may be disabled in device settings.
            }
			else if (p == PurchaseFailureReason.UserCancelled || p == PurchaseFailureReason.Unknown)
			{
			}
			else if(p == PurchaseFailureReason.SignatureInvalid)
			{
                if(i.receipt != null)
                    ReportFailedPurchase(i.receipt, "validation");
            }
            this.CloseAnimatedViewSignal.Dispatch(typeof(NewMessageDialogView));
        }


        public void ListUnityIAPs()
        {
            /*foreach (var product in controller.products.all)
            {
                Debug.Log(product.metadata.localizedTitle);
                Debug.Log(product.metadata.localizedDescription);
                Debug.Log(product.metadata.localizedPriceString);
            }*/

            foreach (var item in controller.products.all)
            {
                if (item.availableToPurchase)
                {
                    Debug.Log(string.Join(" - ",
                        new[]
                        {
                        item.metadata.localizedTitle,
                        item.metadata.localizedDescription,
                        item.metadata.isoCurrencyCode,
                        item.metadata.localizedPrice.ToString(),
                        item.metadata.localizedPriceString
                        })
                        + ": "
                        + item.availableToPurchase.ToString());
                }
            }
        }


        /// <summary>
        /// iOS Specific.
        /// This is called as part of Apple's 'Ask to buy' functionality,
        /// when a purchase is requested by a minor and referred to a parent
        /// for approval.
        /// 
        /// When the purchase is approved or rejected, the normal purchase events
        /// will fire.
        /// </summary>
        /// <param name="item">Item.</param>
        private void OnDeferred(Product item)
        {
            Debug.Log("Purchase deferred: " + item.definition.id);
        }

        private void OnPurchaseSuccessful(string itemId, string receipt, string signature)
        {
            Debug.LogFormat("Purchased Product with ID: {0}. Requesting inventory.", itemId);
            // call to the server to tell it to re-pull inventory
            _instance.ClientService.SendMessage(new PlayerDataRequest { IsRefreshRequest = true, RequestInventory = true, RequestWallet = true });
            // close the store & pop a "purchase successful" notification
			StoreView view = (StoreView)this.ViewReferences.Get(typeof(StoreView));
			if (view != null)
				view.hasPurchasedSomething = true;

            // fire up the rating panel.
            this.RateGameSignal.Dispatch(RateGamePoint.Purchase);

            // track it with the Metrics Service
            NovaCatalogItem productData;
            uint price;
            StoreData storeData = null;
            Dictionary<string, StoreData> storeDatabase;
            if((this.GameData.StoreCache.TryGetValue(NBEconomy.STORE_MAIN, out storeDatabase) 
                && storeDatabase.TryGetValue(itemId, out storeData) 
                && storeData.VirtualPrices != null 
                && storeData.VirtualPrices.TryGetValue(CurrencyType.RM.ToString(), out price))
                || (this.GameData.ProductCache.TryGetValue(itemId, out productData) 
                    && productData.VirtualCurrencyPrices.TryGetValue(CurrencyType.RM, out price)))
            {
                Debug.LogFormat("Logging IAP for {0} costing USD {1:00}", itemId, price/100m);
                this.MetricsTrackingService.TrackIAP(itemId, (int)price, receipt, signature);
                _instance.LogIAP(itemId, price);
            }
            else
            {
                Debug.LogError("Could not log IAP and price; not found in product cache");
            }

            // then fire up the message dialog. Should come up over the rating panel, so the rating panel "appears" when the message dialog disappears.
            _instance.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(
                I2.Loc.ScriptLocalization.Purchase_Successful,
                string.Format(I2.Loc.ScriptLocalization.You_Bought_An_Item, I2.Loc.ScriptLocalization.Get(STORE_ITEM_NAME + itemId), string.Empty, string.Empty),
                false,
                storeData));
        }

        public void LogIAP(string itemId, uint USDprice)
        {
            //UnityEngine.Analytics.Analytics.Transaction(itemId, USDprice/100m, "USD", null, null);
            this.LogEventSignal.Dispatch(new LogEventParams
            {
                EventName = LogEventParams.EVENT_PURCHASE,
                EventValue = (int)USDprice,
                ISOCurrencyCode = "USD",
                ItemId = itemId
            });
        }

#if (UNITY_STEAM)
        public static void PurchaseCallback(MicroTxnAuthorizationResponse_t param)
        {
            var isAuthorized = Convert.ToBoolean(param.m_bAuthorized);
            if (isAuthorized)
            {
                var purchasedProduct = _instance.SteamController.GetCurrentProduct();
                _instance.m_PurchaseInProgress = false;
                _instance.SteamController.ConfirmPendingPurchase(purchasedProduct, (product) =>
                {
                    _instance.OnPurchaseSuccessful(product.ItemId, null, null);
                });

                uint price;
                purchasedProduct.VirtualPrices.TryGetValue(CurrencyType.RM.ToString(), out price);

                UnityEngine.Analytics.Analytics.Transaction(purchasedProduct.ItemId, price / 100m, "USD", null, null);
            }
            else
            {
                _instance.CloseAnimatedViewSignal.Dispatch(typeof(NewMessageDialogView));
            }

            _instance.m_PurchaseInProgress = false;
        }
#endif
        public void BeginVirtualPurchase(StoreData storeData, PoolableMonoBehaviour newView)
        {
            BeginVirtualPurchaseSignal.Dispatch(storeData, newView);
        }

        public void BeginCashPurchase(StoreData storeData)
        {
            MoneyPurchaseSignal.Dispatch(storeData);
        }
    }
}
