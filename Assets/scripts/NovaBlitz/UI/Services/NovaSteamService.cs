﻿using System;
using UnityEngine;
using System.Collections;
using NovaBlitz.Game;
#if UNITY_STEAM
using Steamworks;
#endif

namespace NovaBlitz.UI
{
    public class NovaSteamService : SteamManager
    {

        private uint pcbTicket;
        private byte[] SteamTicket;

#if UNITY_STEAM
        //These are steam Callbacks.
        public Callback<GetAuthSessionTicketResponse_t> _authSessionTicketCallback;
        protected Callback<MicroTxnAuthorizationResponse_t> _microTxnAuthCallback;
#endif

        protected override void OnEnable()
        {
            base.OnEnable();
            if (Initialized)
            {
#if UNITY_STEAM
                //Bind to steam callbacks,  FYI - I hate the way they do event binding as it is Abnormal.
                //_authSessionTicketCallback = Callback<GetAuthSessionTicketResponse_t>.Create(this.AuthCallback); //bind to the Steam GetAuthSessionTicketCallback
                _microTxnAuthCallback = Callback<MicroTxnAuthorizationResponse_t>.Create(NovaIAPService.PurchaseCallback);
#endif
            }
        }


        protected byte[] _SteamTicket;
        protected uint _pcbTicket;
        public static string lastErrorCode = string.Empty;

        /*
        public void GetAuthToken()
        {
            if (!Initialized)
            {
                this.ClientService.ForceDisconnect(Messages.DisconnectCode.Reason15);
            }
#if UNITY_STEAM
                _SteamTicket = new byte[1024];
            SteamUser.GetAuthSessionTicket(_SteamTicket, 1024, out _pcbTicket);
#else
            Debug.LogErrorFormat("Calling steam auth when not on steam");
            this.ClientService.ForceDisconnect(Messages.DisconnectCode.Reason15);
#endif
        }

#if UNITY_STEAM
        public void AuthCallback(GetAuthSessionTicketResponse_t param)
        {
            if(param.m_eResult != EResult.k_EResultOK)
            {
                lastErrorCode = param.m_eResult.ToString();
                Debug.LogErrorFormat("Steam Erroe on authentication. Error code: {0}", lastErrorCode);
                this.ClientService.ForceDisconnect(Messages.DisconnectCode.Reason15);
                return;
            }
            Debug.Log(param.m_eResult);
            Debug.Log("Steam Auth Callback - Processing AuthTicket");
            byte[] actualTicket = new byte[_pcbTicket]; // create a new array that matches the m_pcbTicket length
            Debug.Log("Steam Auth Callback - Processing AuthTicket - step 2");
            Array.Copy(_SteamTicket, actualTicket, _pcbTicket); // copy the first m_pcbTicket bytes
            Debug.Log("Steam Auth Callback - Processing AuthTicket - step 3");
            string authStr = BitConverter.ToString(actualTicket).Replace("-", ""); // convert the actual ticket to a hexadecimal string
            Debug.LogFormat("Steam Callback Auth String Acquired: {0}", authStr);
            this.SteamAuthTicketSignal.Dispatch(authStr);
        }
#endif*/
    }
}