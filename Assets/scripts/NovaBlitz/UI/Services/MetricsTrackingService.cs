﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Messages;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;
using GameAnalyticsSDK;

namespace NovaBlitz.UI
{
    public class MetricsTrackingService : MonoBehaviour
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public BackupMetricsDataSignal BackupMetricsDataSignal { get; set; }
        [Inject] public ZendeskTicketSignal ZendeskTicketSignal { get; set; }
        [Inject] public ClientService ClientService { get; set; }

        public DateTime LastBackedUpTime { get; set; }

        public TrackingData TrackingData = new TrackingData();

        private const string STORAGE_SUFFIX = "tracking_data_cache";

        private const string DAYS_SINCE_INSTALL = "Days_Since_Install";
        private const string DAYS_SINCE_LAST_PURCHASE = "Days_Since_Last_Purchase";

        // pull this from data when we export,
        private const string CURRENT_RANK = "Current_Rank";
        private const string BEST_RANK = "Best_Rank";
        private const string BEST_DRAFT = "Best_Draft";
        private const string BEST_LEAGUE = "Best_League";
        private const string BEST_EVENT = "Best_Event";
        private const string OWNED_CARDBACK = "Owned_CardBack";
        private const string OWNED_AVATARS = "Owned_Avatars";
        private const string CURRENT_MEGA_CREDITS = "Current_Mega_Credits";
        private const string CURRENT_MEGA_NANOBOTS = "Current_Mega_NanoBots";
        private const string CURRENT_MEGA_SMARTPACKS = "Current_Mega_SmartPacks";
        private const string GEMS_BALANCE = "Gems_Balance";
        private const string CREDITS_BALANCE = "Credits_Balance";
        private const string NANOBOTS_BALANCE = "NanoBots_Balance";
        private const string QUALIFIERPOINTS_BALANCE = "QualifierPoints_Balance";
        private const string SMARTPACKS_BALANCE = "SmartPacks_Balance";
        

        public void OnSessionStart()
        {
            LastBackedUpTime = DateTime.UtcNow;
            // pull the data file? We start the session before we load from PlayFab...
            TrackingData = ImportFromPlayerPrefs();

            TrackingData.UpdateValue(TrackingStat.Total_Sessions, 1);
            if (TrackingData.InstallDate == DateTime.MinValue)
                TrackingData.SetDate(TrackingDate.Install_Date);
            GA_SetSpend();
            SavedException = null;
        }

//#if !UNITY_EDITOR
		void OnEnable() { Application.logMessageReceivedThreaded += HandleLog; }
		void OnDisable() { Application.logMessageReceivedThreaded -= HandleLog; }
		private const string CRASH = "error_crash";
		private const string EXCEPTION = "error_exception";
		private const string EVENT_NAME = "event";
		private const string STACK_TRACE = "stack_trace";
		private const string PLATFORM = "platform";
		private const string PLAYFABID = "playfab_id";
		private const string LAST_LOG_ENTRIES = "events_prev";
		private const string VERSION = "version";
		private const string UNKNOWN = "unknown";
		private DateTime NextLogTime = DateTime.MinValue;
		private Queue<string> LogQueue = new Queue<string> ();

        public Queue<string> LongLogQueue = new Queue<string>();

        private bool IsExceptionSentThisSession = false;
        public static string SavedException { get; set; }

        void HandleLog(string logString, string stackTrace, LogType type)
		{
            if (type == LogType.Exception && NextLogTime < DateTime.UtcNow )
			{
				NextLogTime = DateTime.UtcNow.AddMinutes (1);
				var eventData = new Dictionary<string,object> ();
				eventData [EVENT_NAME] = logString;
				eventData [PLATFORM] = Application.platform.ToString ();
				eventData [VERSION] = this.GameData != null ? this.GameData.VersionLabel : UNKNOWN;
				//eventData [PLAYFABID] = this.PlayerProfile != null && this.PlayerProfile.MetaProfile != null 
				//	? this.PlayerProfile.MetaProfile.PlayFabID
				//	: UNKNOWN;

                // logging the exception without the stack trace and last log entries, to see if that helps.
                UnityEngine.Analytics.Analytics.CustomEvent(EXCEPTION, eventData);

                eventData[STACK_TRACE] = stackTrace;
                StringBuilder builder = new StringBuilder ();
				while(LogQueue.Count > 0)
				{
					builder.Append (LogQueue.Dequeue ()).AppendLine();
				}
				eventData [LAST_LOG_ENTRIES] = builder.ToString ();
                //UnityEngine.Analytics.Analytics.CustomEvent (EXCEPTION, eventData);
			}
			else
			{
				LogQueue.Enqueue (logString);
				while (LogQueue.Count > 20)
				{
					LogQueue.Dequeue ();
				}
            }
            string longLogString = string.Format("{0}: {1}:{2} {3}{4}{5}",
                        type,
						DateTime.Now.ToString(TimeString),
                        DateTime.Now.Millisecond,
                        logString,
                        type == LogType.Exception ? ": " : string.Empty,
                        type == LogType.Exception ? stackTrace : string.Empty);

			// Ignore duplicate strings
			/*for(int i = 1; i < LongLogQueue.Count && i < 5; i++)
			{
				if(longLogString.Equals(LongLogQueue[LongLogQueue.Count - i]))
					return;
			}*/


            LongLogQueue.Enqueue(longLogString);

            while (LongLogQueue.Count > 500)
            {
                LongLogQueue.Dequeue();
            }
            
            if(type == LogType.Exception && string.IsNullOrEmpty(SavedException))
            {
                SavedException = longLogString;
            }
            return;
            // Message the server if this is the first exception found
            if (!IsExceptionSentThisSession && !string.IsNullOrEmpty(SavedException)
                && ClientService.IsConnected
                && !Application.isEditor
                && !string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.ID))
            {
                IsExceptionSentThisSession = true;
                var ticket = new ZendeskTicket
                {
                    ContactType = "Exception",
                    Happiness = -1,
                    Subject = string.Format(type == LogType.Exception ? "Unhandled Exception: {0}" : "Tracked Error: {0}", logString),
                    Description = SavedException,
                    EmailAddress = "none@none.none",
                };
                this.ZendeskTicketSignal.Dispatch(ticket, ReportType.Exception, new List<ChatMessage>());
            }
        }

		private const string TimeString = "HH:mm:ss";
        //#endif
        
        public void LoadTrackingFromPlayFabResponse(string dataString)
        {
            TrackingData data = ParseFromString(dataString);
            int value;
            foreach(var kvp in data.tracking_stats)
            {
                TrackingData.tracking_stats.TryGetValue(kvp.Key, out value);
                if (value < kvp.Value)
                    TrackingData.tracking_stats[kvp.Key] = kvp.Value;
            }
            foreach (var kvp in data.tutorial_time_s)
            {
                TrackingData.tutorial_time_s.TryGetValue(kvp.Key, out value);
                if (value < kvp.Value)
                    TrackingData.tutorial_time_s[kvp.Key] = kvp.Value;
            }
            if(data.LastPurchaseDate > TrackingData.LastPurchaseDate)
            {
                TrackingData.LastPurchaseDate = data.LastPurchaseDate;
            }
            if(data.InstallDate > TrackingData.InstallDate)
            {
                TrackingData.InstallDate = data.InstallDate;
            }
            SaveData();
        }

        public TrackingData ParseFromString(string dataString)
        {
            TrackingData trackingData;
            if (!string.IsNullOrEmpty(dataString))
            {
                try
                {
                    trackingData = JsonConvert.DeserializeObject<TrackingData>(dataString);
                }
                catch
                {
                    trackingData = new TrackingData();
                }
            }
            else
                trackingData = new TrackingData();
            return trackingData;
        }

        public void SaveData()
        {
            string json = JsonConvert.SerializeObject(
                this.TrackingData, 
                Formatting.None, 
                new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }
                );
            PlayerPrefs.SetString(this.PlayerProfile.MetaProfile.PlayFabID + STORAGE_SUFFIX, json);
            // Backup
            if ((DateTime.UtcNow - LastBackedUpTime).TotalMinutes > 10)
            {
                this.BackupMetricsDataSignal.Dispatch(json);
                LastBackedUpTime = DateTime.UtcNow;
            }
        }

        public TrackingData ImportFromPlayerPrefs()
        {
            string dataString = PlayerPrefs.GetString(this.PlayerProfile.MetaProfile.PlayFabID + STORAGE_SUFFIX, null);
            return ParseFromString(dataString);
        }

        public void ResetSession()
        {
            LastBackedUpTime = DateTime.MinValue;
            SaveData();
            this.TrackingData.session_stats = new Dictionary<int, int>();
            TrackingData.UpdateValue(TrackingStat.Total_Sessions, 1);
        }

        public void OnApplicationPaused(bool isPaused)
        {
            if(isPaused)
            {
                ResetSession();
            }
        }

        public void OnApplicatonQuit()
        {
            ResetSession();
        }


        private Hashtable ExportValues()
        {
            var hash = new Hashtable();
            hash[PLATFORM] = Application.platform.ToString();
            hash[VERSION] = this.GameData != null ? this.GameData.VersionLabel : UNKNOWN;
            foreach (var kvp in TrackingData.tracking_stats)
            {
                try { hash[((TrackingStat)kvp.Key).ToString()] = kvp.Value; }
                catch { }
            }
            foreach (var kvp in TrackingData.session_stats)
            {
                try { hash[((SessionStat)kvp.Key).ToString()] = kvp.Value; }
                catch { }
            }
            if (TrackingData.InstallDate > DateTime.MinValue)
                hash[DAYS_SINCE_INSTALL] = (DateTime.UtcNow - TrackingData.InstallDate).TotalDays;
            if (TrackingData.LastPurchaseDate > DateTime.MinValue)
                hash[DAYS_SINCE_LAST_PURCHASE] = (DateTime.UtcNow - TrackingData.LastPurchaseDate).TotalDays;

            int value;
            if (PlayerProfile.Wallet != null && PlayerProfile.Wallet.Currencies != null)
            {
                if (PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.CR, out value))
                    hash[CREDITS_BALANCE] = value;
                if (PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.NG, out value))
                    hash[GEMS_BALANCE] = value;
                if (PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.NC, out value))
                    hash[NANOBOTS_BALANCE] = value;
                if (PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.SP, out value))
                    hash[SMARTPACKS_BALANCE] = value;
                if (PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.QP, out value))
                    hash[QUALIFIERPOINTS_BALANCE] = value;
            }

            if (PlayerProfile.OwnedAvatars != null && PlayerProfile.OwnedAvatars.Count > 0)
                hash[OWNED_AVATARS] = PlayerProfile.OwnedAvatars.Count;
            if (PlayerProfile.OwnedCardBacks != null && PlayerProfile.OwnedCardBacks.Count > 0)
                hash[OWNED_AVATARS] = PlayerProfile.OwnedCardBacks.Count;
            if (PlayerProfile.UserStatistics != null)
            {
                if (PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.LIFETIME_LEVEL, out value))
                    hash[BEST_RANK] = value;
                if (PlayerProfile.UserStatistics.TryGetValue(NBEconomy.PLAYER_STATS_MMR, out value))
                    hash[CURRENT_RANK] = value;
                if (PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.BEST_DRAFT, out value))
                    hash[BEST_DRAFT] = value;
                if (PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.BEST_LEAGUE, out value))
                    hash[BEST_LEAGUE] = value;
                if (PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.BEST_EVENT, out value))
                    hash[BEST_EVENT] = value;
            }

            if (PlayerProfile.BoughtStoreItems != null && PlayerProfile.BoughtStoreItems.Contains(NovaIAPService.ANNUITY_CREDIT))
            {
                hash[CURRENT_MEGA_CREDITS] = 1;
                TrackingData.SetValue(TrackingStat.Prev_Mega_Credits, 1);
            }
            if (PlayerProfile.BoughtStoreItems != null && PlayerProfile.BoughtStoreItems.Contains(NovaIAPService.ANNUITY_SMARTPACK))
            {
                hash[CURRENT_MEGA_SMARTPACKS] = 1;
                TrackingData.SetValue(TrackingStat.Prev_Mega_SmartPacks, 1);
            }
            if (PlayerProfile.BoughtStoreItems != null && PlayerProfile.BoughtStoreItems.Contains(NovaIAPService.ANNUITY_NANOBOT))
            {
                hash[CURRENT_MEGA_NANOBOTS] = 1;
                TrackingData.SetValue(TrackingStat.Prev_Mega_NanoBots, 1);
            }
            return hash;
        }


        public void TrackGameStart(GameFormat format)
        {
            TrackingData.GameStartTime = DateTime.UtcNow;
            TrackingData.UpdateValue(TrackingStat.AllTime_Played_Games, 1);
            switch (format)
            {
                case GameFormat.Draft:
                    TrackingData.UpdateValue(TrackingStat.AllTime_Played_Draft, 1);
                    if(TrackingData.GetValue(TrackingStat.AllTime_Played_Draft) == 1)
                    {
                        //FuseService.RegisterCustomEvent(FUSE_FIRST_TIME_PLAY_TYPE, PARAM_GAME_FORMAT, format.ToString(), ExportValues());
                    }
                    break;
                case GameFormat.Constructed:
                    TrackingData.UpdateValue(TrackingStat.AllTime_Played_League, 1);
                    if (TrackingData.GetValue(TrackingStat.AllTime_Played_League) == 1)
                    {
                        //FuseService.RegisterCustomEvent(FUSE_FIRST_TIME_PLAY_TYPE, PARAM_GAME_FORMAT, format.ToString(), ExportValues());
                    }
                    break;
                case GameFormat.Tournament:
                case GameFormat.Annual:
                case GameFormat.Monthly:
                    TrackingData.UpdateValue(TrackingStat.AllTime_Played_Event, 1);
                    if (TrackingData.GetValue(TrackingStat.AllTime_Played_Event) == 1)
                    {
                        //FuseService.RegisterCustomEvent(FUSE_FIRST_TIME_PLAY_TYPE, PARAM_GAME_FORMAT, format.ToString(), ExportValues());
                    }
                    break;
                case GameFormat.Tutorial:
                    break;
                case GameFormat.Practice:
                    TrackingData.UpdateValue(TrackingStat.AllTime_Played_Practice, 1);
                    if (TrackingData.GetValue(TrackingStat.AllTime_Played_Practice) == 1)
                    {
                        //FuseService.RegisterCustomEvent(FUSE_FIRST_TIME_PLAY_TYPE, PARAM_GAME_FORMAT, format.ToString(), ExportValues());
                    }
                    break;
                default:
                    TrackingData.UpdateValue(TrackingStat.AllTime_Played_Casual, 1);
                    if (TrackingData.GetValue(TrackingStat.AllTime_Played_Casual) == 1)
                    {
                        //FuseService.RegisterCustomEvent(FUSE_FIRST_TIME_PLAY_TYPE, PARAM_GAME_FORMAT, format.ToString(), ExportValues());
                    }
                    break;
            }
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, format.ToString(),
                format == GameFormat.Tutorial
                    ? string.Format("tutorial{0}", PlayerProfile.OnboardingProgress.CurrentTutorial)
                    : string.Format("rank{0}", PlayerProfile.CurrentRank));
            SaveData();
        }
        
        public void TrackGameEnd(GameOverMessage msg)
        {
            GA_SetRank();
            double gameTimeSeconds = (DateTime.UtcNow - TrackingData.GameStartTime).TotalSeconds;
            if (gameTimeSeconds > 0 && gameTimeSeconds < 1800) // ignore games longer than 30 min, or shorter than 0 min
            {
                TrackingData.UpdateValue(TrackingStat.Time_S_GamePlay_Since_Install, (int)gameTimeSeconds);
                int seconds;
                TrackingData.tracking_stats.TryGetValue((int)TrackingStat.Time_S_GamePlay_Since_Install, out seconds);
                TrackingData.SetValue(TrackingStat.AllTime_PlayMins, seconds / 60);


                if (msg.GameFormat == GameFormat.Tutorial && !this.PlayerProfile.OnboardingProgress.TutorialsCompleted)
                {
                    int currentTime;
                    this.TrackingData.tutorial_time_s.TryGetValue(this.PlayerProfile.OnboardingProgress.CurrentTutorial, out currentTime);
                    this.TrackingData.tutorial_time_s[this.PlayerProfile.OnboardingProgress.CurrentTutorial] = currentTime + (int)gameTimeSeconds;
                }
                GA_SetPlayTime(seconds);
            }
            var prizes = msg.GrantedCurrencies;
            if (prizes != null && prizes.Count > 0)
            {
                int credits;
                if (prizes.TryGetValue(CurrencyType.CR, out credits))
                {
                    TrackingData.UpdateValue(TrackingStat.Credits_Earned, credits);
                    TrackVirtualSpend(SpendArea.Rewards, CurrencyType.CR, -credits, msg.GameFormat.ToString());
                }
                int gems;
                if (prizes.TryGetValue(CurrencyType.NG, out gems))
                {
                    TrackVirtualSpend(SpendArea.Rewards, CurrencyType.NG, -gems, msg.GameFormat.ToString());
                }
                int packs;
                if (prizes.TryGetValue(CurrencyType.SP, out packs))
                { 
                    TrackVirtualSpend(SpendArea.Rewards, CurrencyType.SP, -packs, msg.GameFormat.ToString());
                }
                int qps;
                if (prizes.TryGetValue(CurrencyType.QP, out qps))
                {
                    TrackVirtualSpend(SpendArea.Rewards, CurrencyType.QP, -qps, msg.GameFormat.ToString());
                }
                int nanobots;
                if (prizes.TryGetValue(CurrencyType.NC, out nanobots))
                {
                    TrackVirtualSpend(SpendArea.Rewards, CurrencyType.NC, -nanobots, msg.GameFormat.ToString());
                }
            }
            if (msg.HasWon)
            {
                TrackingData.UpdateValue(TrackingStat.AllTime_Won_Games, 1);
                switch (msg.GameFormat)
                {
                    case GameFormat.Draft:
                        TrackingData.UpdateValue(TrackingStat.AllTime_Won_Draft, 1);
                        break;
                    case GameFormat.Constructed:
                        TrackingData.UpdateValue(TrackingStat.AllTime_Won_League, 1);
                        break;
                    case GameFormat.Tournament:
                    case GameFormat.Annual:
                    case GameFormat.Monthly:
                        TrackingData.UpdateValue(TrackingStat.AllTime_Won_Event, 1);
                        break;
                    case GameFormat.Tutorial:
                        // get current
                        TrackingData.UpdateValue(TrackingStat.AllTime_Won_Practice, 1);
                        if(!this.PlayerProfile.OnboardingProgress.TutorialsCompleted)
                        {
                            // log tutorial step time
                            int time_s;
                            this.TrackingData.tutorial_time_s.TryGetValue(this.PlayerProfile.OnboardingProgress.CurrentTutorial, out time_s);
                            var variables = ExportValues();
                            variables[TrackingStat.Time_S_Tutorial_Step.ToString()] = time_s;
                        }
                        break;
                    case GameFormat.Practice:
                        TrackingData.UpdateValue(TrackingStat.AllTime_Won_Practice, 1);
                        break;
                    default:
                        TrackingData.UpdateValue(TrackingStat.AllTime_Won_Casual, 1);
                        break;
                }
                
                if(msg.CurrentMMR < this.PlayerProfile.CurrentRank && this.PlayerProfile.CurrentRank != 0)
                {
                    //FuseService.RegisterCustomEvent(FUSE_RANK_UP, PARAM_RANK, msg.CurrentMMR.ToString(), ExportValues());
                }
                if(msg.GameFormat == GameFormat.Tutorial)
                    GameAnalytics.NewDesignEvent(string.Format("{0}:{1}:Win", msg.GameFormat, PlayerProfile.OnboardingProgress.CurrentTutorial));
                else
                    GameAnalytics.NewDesignEvent(string.Format("{0}:Win:{1}", msg.GameFormat, msg.CurrentMMR));
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, msg.GameFormat.ToString(), 
                    msg.GameFormat == GameFormat.Tutorial 
                        ? string.Format("tutorial{0}", PlayerProfile.OnboardingProgress.CurrentTutorial)
                        : string.Format("rank{0}",PlayerProfile.CurrentRank),
                    msg.CurrentRating);
            }
            else
            {
                if (msg.GameFormat == GameFormat.Tutorial)
                    GameAnalytics.NewDesignEvent(string.Format("{0}:{1}:Loss", msg.GameFormat, PlayerProfile.OnboardingProgress.CurrentTutorial));
                else
                    GameAnalytics.NewDesignEvent(string.Format("{0}:Loss:{1}", msg.GameFormat, msg.CurrentMMR));
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, msg.GameFormat.ToString(),
                    msg.GameFormat == GameFormat.Tutorial
                        ? string.Format("tutorial{0}", PlayerProfile.OnboardingProgress.CurrentTutorial)
                        : string.Format("rank{0}", PlayerProfile.CurrentRank),
                    msg.CurrentRating);
            }
            SaveData();
        }

        public void TrackAchievement(string achievement, int value)
        {
            GameAnalytics.NewDesignEvent(string.Format("Achievement:{0}:{1}", achievement, value));
        }

        /// <summary>
        /// SPEND AMOUNT should be NEGATIVE for scrap - scrap is a REFUND.
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="isCraft"></param>
        /// <param name="cards"></param>
        /// <param name="spendAmount"></param>
        public void TrackCrafting(int cardId, bool isCraft, int cards, int spendAmount)
        {
            CardData cardData;
            GameData.CardDictionary.TryGetValue(cardId, out cardData);
            //var cardString = String.Format("{0}_{1}_{2}_{3}_{4}", cardId, cardData.CardType.ToString(), cardData.Rarity.ToString(), cardData.Aspect.ToString(), cardData.Cost);

            if (isCraft)
            {
                TrackVirtualSpend(SpendArea.Crafting, CurrencyType.NC, spendAmount, "card");
                TrackingData.UpdateValue(TrackingStat.AllTime_CardsCrafted, cards);
                TrackingData.SetValue(TrackingStat.Has_Crafted, 1);
                //FuseService.RegisterCustomEvent(FUSE_SPEND_NB_FOR_CRAFTING, PARAM_CARD_INFO, cardString, ExportValues());
                for(int i = 0; i < cards; i++)
                    GameAnalytics.NewDesignEvent(string.Format("Craft:{0}", cardId));
            }
            else
            {
                TrackVirtualSpend(SpendArea.Scrap, CurrencyType.NC, spendAmount, "card");
                TrackingData.UpdateValue(TrackingStat.AllTime_CardsScrapped, cards);
                //FuseService.RegisterCustomEvent(FUSE_SCRAP_CARD, PARAM_CARD_INFO, cardString, ExportValues());
                for (int i = 0; i < cards; i++)
                    GameAnalytics.NewDesignEvent(string.Format("Scrap:{0}", cardId));
            }
            SaveData();
        }

        public void TrackSocialShare()
        {
            TrackingData.SetValue(TrackingStat.Has_FB_Share, 1);
            GameAnalytics.NewDesignEvent("SocialShare");
            SaveData();
        }

        public void TrackAccountCreationStep(AccountCreationStep step, bool isSuccess)
        {
            GameAnalytics.NewDesignEvent(string.Format("Signup:{0}:{1}",step, isSuccess ? "Yes" : "No"));
        }

        public void TrackPackOpening(string itemId, CurrencyType currency, int price)
        {
            TrackVirtualSpend(SpendArea.Packs, currency, price, itemId);
            switch (itemId)
            {
                case NBEconomy.ITEMID_BASIC_PACK:
                    if (currency == CurrencyType.CR)
                    {
                        TrackingData.UpdateValue(TrackingStat.AllTime_Open_CR_BasicPack, 1);
                        //FuseService.RegisterCustomEvent(FUSE_SPEND_CR_FOR_PACKS, PARAM_PACK_TYPE, NBEconomy.ITEMID_BASIC_PACK, ExportValues());
                    }
                    break;
                case NBEconomy.ITEMID_SMART_PACK:
                    if (currency == CurrencyType.SP)
                    {
                        TrackingData.UpdateValue(TrackingStat.AllTime_Open_SP_SmartPack, 1);
                        //FuseService.RegisterCustomEvent(FUSE_SPEND_SP_FOR_PACKS, PARAM_PACK_TYPE, NBEconomy.ITEMID_SMART_PACK, ExportValues());
                    }
                    else if (currency == CurrencyType.NG)
                    {
                        TrackingData.UpdateValue(TrackingStat.AllTime_Open_NG_SmartPack, 1);
                        //FuseService.RegisterCustomEvent(FUSE_SPEND_NG_FOR_PACKS, PARAM_PACK_TYPE, NBEconomy.ITEMID_SMART_PACK, ExportValues());
                    }
                    break;
                case NBEconomy.ITEMID_MEGA_PACK:
                    if (currency == CurrencyType.NG)
                        TrackingData.UpdateValue(TrackingStat.AllTime_Open_NG_MegaPack, 1);
                    //FuseService.RegisterCustomEvent(FUSE_SPEND_NG_FOR_PACKS, PARAM_PACK_TYPE, NBEconomy.ITEMID_MEGA_PACK, ExportValues());
                    break;
            }
            SaveData();
        }

        public void TrackVirtualPurchase(CurrencyType currency, int price, string itemId = null, GameFormat format = GameFormat.NoGame)
        {
            GameAnalytics.NewDesignEvent(string.Format("{0}:{1}:{2}", "Purchase", itemId, currency));
            var variables = ExportValues();
            switch (currency)
            {
                case CurrencyType.CR:
                    variables[PARAM_AMOUNT_CREDITS] = price;
                    break;
                case CurrencyType.NG:
                    variables[PARAM_AMOUNT_GEMS] = price;
                    break;
                case CurrencyType.NC:
                    variables[PARAM_AMOUNT_NANOBOTS] = price;
                    break;
                case CurrencyType.SP:
                    variables[PARAM_AMOUNT_SMARTPACKS] = price;
                    break;
                case CurrencyType.QP:
                    variables[PARAM_AMOUNT_QPS] = price;
                    break;
            }

            if (!string.IsNullOrEmpty(itemId))
            {
                AvatarDataContract avatar;
                if (this.GameData.AvatarByItemIdDictionary.TryGetValue(itemId, out avatar))
                {
                    //FuseService.RegisterCustomEvent(currency == CurrencyType.NG ? FUSE_SPEND_NG_FOR_AVATAR : FUSE_SPEND_CR_FOR_AVATAR, PARAM_AVATAR_ID, itemId, variables);
                    TrackVirtualSpend(SpendArea.Avatar, currency, price, "avatar");
                }
                CardBackDataContract cardBack;
                if (this.GameData.CardBackByItemIdDictionary.TryGetValue(itemId, out cardBack))
                {
                    //FuseService.RegisterCustomEvent(currency == CurrencyType.NG ? FUSE_SPEND_NG_FOR_CARDBACK : FUSE_SPEND_CR_FOR_CARDBACK, PARAM_CARDBACK_ID, itemId, variables);
                    TrackVirtualSpend(SpendArea.CardBack, currency, price, "cardBack");
                }
            }
            else if (format != GameFormat.NoGame)
            {
                switch (format)
                {
                    case GameFormat.Draft:
                        TrackVirtualSpend(SpendArea.DraftEntry, currency, price, format.ToString());
                        /*if (currency == CurrencyType.CR)
=======
                        if (currency == CurrencyType.CR)
>>>>>>> origin/master
                            FuseService.RegisterCustomEvent(FUSE_SPEND_CR_FOR_DRAFT, PARAM_AMOUNT_CREDITS, price.ToString(), variables);
                        else if (currency == CurrencyType.NG)
                            FuseService.RegisterCustomEvent(FUSE_SPEND_NG_FOR_DRAFT, PARAM_AMOUNT_GEMS, price.ToString(), variables);*/
                        break;
                    case GameFormat.Constructed:
                        TrackVirtualSpend(SpendArea.LeagueEntry, currency, price, format.ToString());
                        /*if (currency == CurrencyType.CR)
=======
                        if (currency == CurrencyType.CR)
>>>>>>> origin/master
                            FuseService.RegisterCustomEvent(FUSE_SPEND_CR_FOR_LEAGUE, PARAM_AMOUNT_CREDITS, price.ToString(), variables);
                        else if (currency == CurrencyType.NG)
                            FuseService.RegisterCustomEvent(FUSE_SPEND_NG_FOR_LEAGUE, PARAM_AMOUNT_GEMS, price.ToString(), variables);*/
                        break;
                    case GameFormat.Tournament:
                    case GameFormat.Monthly:
                    case GameFormat.Annual:
                        TrackVirtualSpend(SpendArea.TournamentEntry, currency, price, format.ToString());
                        /*if (currency == CurrencyType.QP)
=======
                        if (currency == CurrencyType.QP)
>>>>>>> origin/master
                            FuseService.RegisterCustomEvent(FUSE_SPEND_QP_FOR_EVENT, PARAM_AMOUNT_QPS, price.ToString(), variables);
                        else if (currency == CurrencyType.NG)
                            FuseService.RegisterCustomEvent(FUSE_SPEND_NG_FOR_EVENT, PARAM_AMOUNT_GEMS, price.ToString(), variables);*/
                        break;
                }
            }
            SaveData();
        }

        public void TrackIAP(string itemId, int UScents, string receipt, string signature)
        {
            TrackingData.SetDate(TrackingDate.Purchase_Date);
            TrackingData.UpdateValue(TrackingStat.Total_IAP_USD, (int)Math.Ceiling(UScents / 100f));
            TrackingData.SetValue(TrackingStat.Has_Paid, 1);
            //FuseService.RegisterCustomEvent(FUSE_IAP_PURCHASE, PARAM_IAP_ID, itemId, ExportValues());
            GA_SetSpend();
#if UNITY_ANDROID
            GameAnalytics.NewBusinessEventGooglePlay("USD", UScents, "itemType", itemId, "cart", receipt, signature);
#elif UNITY_IOS
            GameAnalytics.NewBusinessEventIOS("USD", UScents, "itemType", itemId, "cart", receipt);
#else
            GameAnalytics.NewBusinessEvent("USD", UScents, itemId, itemId, null);
#endif
            GameAnalytics.NewDesignEvent(string.Format("{0}:{1}:{2}", "IAP", itemId, UScents));
            SaveData();
        }

        #region Private Internal 
        private void TrackVirtualSpend(SpendArea area, CurrencyType currency, int price, string itemId)
        {
            var areaString = area.ToString();
            Hashtable variables = new Hashtable();
            if (price > 0)
            {
                switch (currency)
                {
                    case CurrencyType.CR:
                        TrackingData.UpdateValue(TrackingStat.Credits_Spent, price);
                        variables[PARAM_AMOUNT_CREDITS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_CREDITS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "Credits", price, areaString, itemId);
                        break;
                    case CurrencyType.NG:
                        TrackingData.UpdateValue(TrackingStat.Gems_Spent, price);
                        variables[PARAM_AMOUNT_GEMS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_GEMS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "Gems", price, areaString, itemId);
                        break;
                    case CurrencyType.SP:
                        TrackingData.UpdateValue(TrackingStat.SmartPacks_Spent, price);
                        variables[PARAM_AMOUNT_SMARTPACKS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_PACKS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "SmartPacks", price, areaString, itemId);
                        break;
                    case CurrencyType.QP:
                        TrackingData.UpdateValue(TrackingStat.QualifierPoints_Spent, price);
                        variables[PARAM_AMOUNT_QPS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_QPS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "QPs", price, areaString, itemId);
                        break;
                    case CurrencyType.NC:
                        TrackingData.UpdateValue(TrackingStat.NanoBots_Spent, price);
                        variables[PARAM_AMOUNT_NANOBOTS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_NANOBOTS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "NanoBots", price, areaString, itemId);
                        break;
                }
            }
            else if (price < 0)
            {
                switch (currency)
                {
                    case CurrencyType.CR:
                        TrackingData.UpdateValue(TrackingStat.Credits_Earned, -price);
                        variables[PARAM_AMOUNT_CREDITS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_CREDITS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Credits", -price, areaString, itemId);
                        break;
                    case CurrencyType.NG:
                        TrackingData.UpdateValue(TrackingStat.Gems_Earned, -price);
                        variables[PARAM_AMOUNT_GEMS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_GEMS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Gems", -price, areaString, itemId);
                        break;
                    case CurrencyType.SP:
                        TrackingData.UpdateValue(TrackingStat.SmartPacks_Earned, -price);
                        variables[PARAM_AMOUNT_SMARTPACKS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_PACKS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "SmartPacks", -price, areaString, itemId);
                        break;
                    case CurrencyType.QP:
                        TrackingData.UpdateValue(TrackingStat.QualifierPoints_Earned, -price);
                        variables[PARAM_AMOUNT_QPS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_QPS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "QPs", -price, areaString, itemId);
                        break;
                    case CurrencyType.NC:
                        TrackingData.UpdateValue(TrackingStat.NanoBots_Earned, -price);
                        variables[PARAM_AMOUNT_NANOBOTS] = price;
                        //FuseService.RegisterCustomEvent(FUSE_NANOBOTS_ECONOMY, PARAM_TARGET_AREA, area.ToString(), variables);
                        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "NanoBots", -price, areaString, itemId);
                        break;
                }

            }
        }

        public void GA_SetPlayTime(int timeS)
        {
            var hours = timeS / 60 / 60;
            if(hours <1)
                GameAnalytics.SetCustomDimension03("played<1");
            else if (hours < 2)
                GameAnalytics.SetCustomDimension03("played<2");
            else if (hours < 5)
                GameAnalytics.SetCustomDimension03("played<5");
            else if (hours < 10)
                GameAnalytics.SetCustomDimension03("played<10");
            else if (hours < 20)
                GameAnalytics.SetCustomDimension03("played<20");
            else if (hours < 50)
                GameAnalytics.SetCustomDimension03("played<50");
            else if (hours < 100)
                GameAnalytics.SetCustomDimension03("played<100");
            else if (hours < 200)
                GameAnalytics.SetCustomDimension03("played<200");
            else if (hours < 500)
                GameAnalytics.SetCustomDimension03("played<500");
            else if (hours < 1000)
                GameAnalytics.SetCustomDimension03("played<1000");
            else if (hours < 2000)
                GameAnalytics.SetCustomDimension03("played<2000");
            else if (hours < 5000)
                GameAnalytics.SetCustomDimension03("played<5000");
            else if (hours < 10000)
                GameAnalytics.SetCustomDimension03("played<10000");
            else
                GameAnalytics.SetCustomDimension03("played10000+");

        }

        public void GA_SetRank()
        {
            var rank = this.PlayerProfile.CurrentRank;
            if (rank < 0)
                GameAnalytics.SetCustomDimension02("NovaRank");
            else if (rank == 0)
            { }
            else if(rank <= 6)
                GameAnalytics.SetCustomDimension02("Diamond");
            else if (rank <= 12)
                GameAnalytics.SetCustomDimension02("Platinum");
            else if (rank <= 18)
                GameAnalytics.SetCustomDimension02("Gold");
            else
                GameAnalytics.SetCustomDimension02(String.Format("Rank{0}", rank));
        }

        private void GA_SetSpend()
        {
            int spend;
            this.TrackingData.tracking_stats.TryGetValue((int)TrackingStat.Total_IAP_USD, out spend);
            if (spend > 0)
            {
                //ZendeskTicket.Tags.Add("spend");
                if (spend > 0 && spend <= 10)
                {
                    GameAnalytics.SetCustomDimension01("spend<10");
                }
                else if (spend > 10 && spend <= 20)
                {
                    GameAnalytics.SetCustomDimension01("spend<20");
                }
                else if (spend > 20 && spend <= 50)
                {
                    GameAnalytics.SetCustomDimension01("spend<50");
                }
                else if (spend > 50 && spend <= 100)
                {
                    GameAnalytics.SetCustomDimension01("spend<100");
                }
                else if (spend > 100 && spend <= 500)
                {
                    GameAnalytics.SetCustomDimension01("spend<500");
                }
                else if (spend > 500 && spend <= 1000)
                {
                    GameAnalytics.SetCustomDimension01("spend<1000");
                }
                else if (spend > 1000)
                {
                    GameAnalytics.SetCustomDimension01("spend1000+");
                }
            }
            else
                GameAnalytics.SetCustomDimension01("spend0");
        }

        private const string FUSE_CREDITS_ECONOMY = "Credits Economy";
        private const string FUSE_GEMS_ECONOMY = "Gems Economy";
        private const string FUSE_NANOBOTS_ECONOMY = "Nano Bots Economy";
        private const string FUSE_PACKS_ECONOMY = "Smart Pack Economy";
        private const string FUSE_QPS_ECONOMY = "Qualifier Point Economy";
        private const string FUSE_SPEND_NG_FOR_DRAFT = "Spend Gems on Draft Entry";
        private const string FUSE_SPEND_CR_FOR_DRAFT = "Spend Credits on Draft Entry";
        private const string FUSE_SPEND_NG_FOR_LEAGUE = "Spend Gems on League Entry";
        private const string FUSE_SPEND_CR_FOR_LEAGUE = "Spend Credits on League Entry";
        private const string FUSE_SPEND_NG_FOR_EVENT = "Spend Gems on Tournament Entry";
        private const string FUSE_SPEND_QP_FOR_EVENT = "Spend QPs on Tournament Entry";
        private const string FUSE_SPEND_CR_FOR_AVATAR = "Spend Credits on Avatar";
        private const string FUSE_SPEND_NG_FOR_AVATAR = "Spend Credits on Avatar";
        private const string FUSE_SPEND_CR_FOR_CARDBACK = "Spend Credits on Card Back";
        private const string FUSE_SPEND_NG_FOR_CARDBACK = "Spend Credits on Card Back";
        private const string FUSE_SPEND_NB_FOR_CRAFTING = "Spend Nanobots on Crafting";
        private const string FUSE_SCRAP_CARD = "Spend Nanobots on Scrap";
        private const string FUSE_RANK_UP = "Rank Up";
        private const string FUSE_TUTORIAL = "Tutorial";
        private const string FUSE_FIRST_TIME_PLAY_TYPE = "First time play Battle Type";
        private const string FUSE_SPEND_CR_FOR_PACKS = "Spend Credits on Packs";
        private const string FUSE_SPEND_SP_FOR_PACKS = "Spend Smart Packs on Packs";
        private const string FUSE_SPEND_NG_FOR_PACKS = "Spend Gems on Packs";
        private const string FUSE_IAP_PURCHASE = "IAP Purchase";

        private const string PARAM_TARGET_AREA = "Target area";
        private const string PARAM_AMOUNT_GEMS = "Transaction Amount (Gems)";
        private const string PARAM_AMOUNT_CREDITS = "Transaction Amount (Credits)";
        private const string PARAM_AMOUNT_NANOBOTS = "Transaction Amount (NanoBots)";
        private const string PARAM_AMOUNT_SMARTPACKS = "Transaction Amount (SmartPacks)";
        private const string PARAM_AMOUNT_QPS = "Transaction Amount (QPs)";
        private const string PARAM_EVENT_TYPE = "Tournament Type";
        private const string PARAM_AVATAR_ID = "Avatar Purchased";
        private const string PARAM_CARDBACK_ID = "Cardback Purchased";
        private const string PARAM_CARD_INFO = "Card";
        private const string PARAM_RANK = "Rank Number";
        private const string PARAM_TUTORIAL_STEP = "Tutorial Step";
        private const string PARAM_GAME_FORMAT = "Battle Type";
        private const string PARAM_PACK_TYPE = "Pack Type Purchased";
        private const string PARAM_IAP_ID = "IAP ID";
        #endregion


        void OnApplicationFocus(bool hasFocus)
        {
            Debug.Log(hasFocus ? "OnApplicationFocus: Regained Focus" : "OnApplicationFocus: Application Lost Focus");
        }
    }
    
    public enum AccountCreationStep
    {
        NoStep,
        StartFlow,
        Email,
        MailingList,
        Password,
        Username,
        CompleteFlow,
        LoggedIn,
        PasswordReset,
    }
}
