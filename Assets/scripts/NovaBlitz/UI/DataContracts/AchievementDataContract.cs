using System.Collections.Generic;

namespace NovaBlitz.UI.DataContracts
{
	public class AchievementDataContract
	{
		public bool is_cumulative;
		public int[] levels;
		//public Dictionary<int, List<KeyValuePair<string,int>>> rewards;
        public string ArtId;

		private const string KEY_ISCULUMATIVE = "is_cumulative";
		private const string KEY_LEVELS = "levels";
		private const string KEY_REWARDS = "rewards";
        private const string KEY_ART_ID = "ArtId";
        
        public AchievementData ToModel(string name)
		{
			bool descending = this.levels[0] > this.levels[1];

            var ach = new AchievementData() {
                Name = name,
                ArtId = this.ArtId,
                IsCumulative = this.is_cumulative,
                IsDescending = descending,
                LevelThresholds = this.levels,
                LevelBrackets = EngineUtils.BracketsToDictionary(EngineUtils.GenerateBrackets(this.levels, descending)),
			};

            return ach;
		}
	}
}
