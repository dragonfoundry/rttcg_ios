using UnityEngine;
using System.Collections;
using PlayFab.Internal;
using System.Collections.Generic;
using NovaBlitz.UI;

namespace NovaBlitz.UI.DataContracts
{
	public class DNSDataContract
	{
		public string Hostname;
		public string Port;
	}
}
