﻿
namespace NovaBlitz.UI.DataContracts
{
    public class UiHintData
    {
        public int Practice { get; set; }
        public int Play { get; set; }
        public int Draft { get; set; }
        public int League { get; set; }
        public int Event { get; set; }
        public int OpenPacks { get; set; }
        public int Store { get; set; }
        public int Crafting { get; set; }
        public int Scrapping { get; set; }
        public int Rankings { get; set; }
        public int DeckBuilder { get; set; }
        public int AddDeck { get; set; }
        public int EditDeck { get; set; }
    }

    // 0 = not shown.
    // 1-9 = stages, if there are stages.
    // >10 = complete.

    // LOGIC:
    // Practice, Play, Draft, League, Event, in that order. Two for each, one in the main menu, one in the appropriate play screen.
    // Highlights the option. Does NOT prevent other actions.
    // OpenPacks, Store, Crafting, Rankings, whenever they come up.

    public enum UiHint
    {
        NoHint,
        Practice,
        Play,
        Draft,
        League,
        Event,
        OpenPacks,
        Store,
        Crafting,
        Rankings,
        DeckBuilder
    }
}
