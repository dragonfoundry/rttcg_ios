﻿using UnityEngine;
using System.Collections;
using PlayFab.Internal;
using System.Collections.Generic;

namespace NovaBlitz.UI.DataContracts
{
    public class CardBackDataContract
    {
        public int Id;
        public string ArtId;
        public bool IsFree;
    }
}
