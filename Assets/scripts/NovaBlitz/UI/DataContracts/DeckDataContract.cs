using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayFab.Internal;


namespace NovaBlitz.UI.DataContracts
{
	public class DeckDataContract
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public List<DeckListItemDataContract> Cards  = new List<DeckListItemDataContract>();
        public string DeckArt { get; set; }
	}
}
