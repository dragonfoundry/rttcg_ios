
using System.Collections.Generic;

namespace NovaBlitz.UI.DataContracts
{
	public class DeckListDataContract
	{
		public List<DeckDataContract> Decks = new List<DeckDataContract>();
    }
}
