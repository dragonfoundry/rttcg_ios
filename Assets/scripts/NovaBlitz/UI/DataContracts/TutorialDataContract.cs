﻿using UnityEngine;
using System.Collections;
using PlayFab.Internal;
using System.Collections.Generic;

namespace NovaBlitz.UI.DataContracts
{
    public class TutorialDataContract
    {
        public double TutorialId;
        public string Title;
        public string ArtId;
        public string Hint;
        public List<int> Deck;
        public DeckData TutorialDeck;
    }

    public class TutorialCombatData
    {
        public int Game;
        public int Turn;
        public int CardId;
        public int Action;
        public string Hint;

        public TutorialCombatData Clone()
        {
            return new TutorialCombatData { Action = this.Action, Game = this.Game, Turn = this.Turn, CardId = this.CardId, Hint = this.Hint };
        }
    }
}
