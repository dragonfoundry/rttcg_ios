using UnityEngine;
using System;
using System.Collections;
using PlayFab.Internal;
using System.Collections.Generic;

namespace NovaBlitz.UI.DataContracts
{
	public class KeywordDataContract
	{
        public int Id;
        public string Name;
        public Messages.KeywordType Type;
		public bool ShowNumber;
		
	}
}
