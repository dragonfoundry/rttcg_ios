﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace NovaBlitz.UI.DataContracts
{
    public enum ArtPositioningKey
    {
        NoKey,
        CardListItem,
        HeroPortrait,
    }

    public class ArtPositioningDataContract
    {
        public ArtPositioningKey Key;
        public List<ArtPositionDataContract> Positions;
        
        public ArtPositioningData ToModel()
        {
            ArtPositioningData model = new ArtPositioningData();
            model.Key = this.Key;
            model.PositionsDictionary = new Dictionary<string, ArtPosition>();
            
            for(int i=0;i<this.Positions.Count;i++)
            {
                ArtPositionDataContract positionData = this.Positions[i];
                ArtPosition positionModel = new ArtPosition(positionData.Id, positionData.Vals);
                model.PositionsDictionary[positionData.Id] = positionModel;
            }
            
            return model;
        }
        
        /// <summary>
        /// Loads art positioning model information from the corresponding file in the streamingAssets folder
        /// </summary>
        static public ArtPositioningDataContract LoadPositions(ArtPositioningKey key)
        {
            ArtPositioningDataContract contract = null;
            string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, key.ToString() + ".json");
            
            try
            {
                WWW reader = new WWW("file://" + filePath);
                while(! reader.isDone) {}
                string json = reader.text;
                contract = Newtonsoft.Json.JsonConvert.DeserializeObject<ArtPositioningDataContract>(json);
                
            }
            catch (Exception except)
            {
                Debug.LogError(except.ToString());
                Debug.LogWarningFormat("Failed loading art positining data for:{0}", filePath);
            }
           
            return contract;
        }
    }


    public class ArtPositionDataContract
    {
        // An array of 5 floats with the following format [scaleX, scaleY, scaleZ, posX, posY]
        // Because there may be 100's of these in a single JSON file, this notation is used to 
        // keep the .json file a little morme compressed
        public float[] Vals;
        public string Id;

        // Deafult Constructor
        public ArtPositionDataContract() { }

        public ArtPositionDataContract(string artId, Vector3 scale, Vector2 position)
        {
            this.Vals = new float[5];
            this.Vals[0] = scale.x;
            this.Vals[1] = scale.y;
            this.Vals[2] = scale.z;
            this.Vals[3] = position.x;
            this.Vals[4] = position.y;

            this.Id = artId;
        }
        
       
    }
}