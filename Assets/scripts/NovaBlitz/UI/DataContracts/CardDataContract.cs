using UnityEngine;
using System.Collections;
using PlayFab.Internal;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NovaBlitz.UI.DataContracts
{
	public class CardDataContract
	{
		public int CardID;
		public string ArtID;
		public string Name;
		public string Aspect;
		public string AspectIcons;
		public string Rarity;
		public bool IsEnergy;
		public bool IsPower;
		public bool IsUnit;
        public bool IsItem;
        public string Cost;
		public string Atk;
		public string Health;
		public string FxScript;
		public string Subtype;
        public int? ActivationCost;

        public Dictionary<int, double> Keywords = new Dictionary<int, double>();
	}
}
