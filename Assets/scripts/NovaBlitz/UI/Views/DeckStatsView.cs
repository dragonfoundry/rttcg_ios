using UnityEngine;
using System;
using System.Collections;
using LinqTools;
using Messages;
using NovaBlitz.Game;
using strange.extensions.mediation.impl;
using TMPro;
using DragonFoundry.Fx;

namespace NovaBlitz.UI
{
	public class DeckStatsView : AnimatedView
	{
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}

		[SerializeField] protected TextMeshProUGUI _NameDisplay;
		[SerializeField] protected TextMeshProUGUI _CountDisplay;
		[SerializeField] protected GameObject _ButtonsNode;
        public TextMeshProUGUI UnitCount;
        public TextMeshProUGUI PowerCount;

		public CardAspectsIndicator AspectsIndicator;
		public DeckEnergyGraphWidget EnergyGraph;
        public DeckRenameWidget Renamer;
        public NovaButton PracticeButton;
        public ParticleEffectsTrigger PracticeButtonFx;
        public Action PlayPracticeButtonFx;

        private DeckData _DeckData;
		public DeckData DeckData { get { return _DeckData; } set { _DeckData = value; Debug.LogFormat("DeckData updated. {0}",value == null? "null" : value.Name); } }

		public string DeckName {
			set { if (null != _NameDisplay) _NameDisplay.text = value; }
		}

		public int CardCount {
			set { _CountDisplay.SetText("{0}/{1}", value, this.DeckData.MinDeckSize); }
		}

		public bool IsEditable { set { Renamer.IsEditable = value; } }

		public event Action EventScreenShotClicked;
		public event Action EventCloneClicked;
		public event Action EventDeleteClicked;
        public event Action EventPracticeClicked;

		protected override void Awake()
		{
			base.Awake();
            //this.IsOpen = false;
			this.Renamer.Init();
		}

		public void OnScrimClicked()
		{
			this.CloseAnimatedViewSignal.Dispatch(this.GetType());
		}

		public void Refresh(bool isEditable)
		{
			if (null == this.DeckData)
				return;
			
			this.DeckName = this.DeckData.Name;
			this.CardCount = this.DeckData.Cards.Count;
            this.UnitCount.text = this.DeckData.Cards.Count(c => c.CardType == CardType.Unit).ToString();
            this.PowerCount.text = this.DeckData.Cards.Count(c => c.CardType == CardType.Power).ToString();
            this.AspectsIndicator.ShowIcons(this.DeckData.Aspects);
            this.PracticeButton.interactable = this.DeckData.IsLegalDeck;
			this.EnergyGraph.DeckData = this.DeckData;
			this.EnergyGraph.Refresh();
            if (this.PlayPracticeButtonFx != null)
                this.PlayPracticeButtonFx.Invoke();

            this.Renamer.IsEditable = isEditable;
			this.Renamer.FieldText = this.DeckData.Name;
			this.Renamer.CachedName = this.DeckData.Name;
		}

		public void ClickClone()
		{
			if (null != this.EventCloneClicked)
				this.EventCloneClicked.Invoke();
		}

		public void ClickDelete()
		{
			if (null != this.EventDeleteClicked)
				this.EventDeleteClicked.Invoke();
		}

		public void ClickScreenShot()
		{
			if (null != this.EventScreenShotClicked)
				this.EventScreenShotClicked.Invoke();
        }

        public void ClickPractice()
        {
            if (null != this.EventPracticeClicked)
                this.EventPracticeClicked.Invoke();
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class DeckStatsMediator : Mediator
	{
		[Inject] public DeckStatsView View {get;set;}
		
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public DeckSelectedSignal DeckSelectedSignal {get;set;}
		[Inject] public CardAddedToDeckSignal CardAddedToDeckSignal {get;set;}
		[Inject] public CardRemovedFromDeckSignal CardRemovedFromDeckSignal {get;set;}
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get;set;}
        
        [Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal { get; set; }
        [Inject] public SaveDeckSignal SaveDeckSignal {get;set;}
		[Inject] public CloneDeckSignal CloneDeckSignal {get;set;}
		[Inject] public DeleteDeckSignal DeleteDeckSignal {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
		[Inject] public StartGameSessionSignal StartGameSessionSignal {get;set;}
        
        [Inject] public SaveUiHintDataSignal SaveUiHintDataSignal { get; set; }

		public override void OnRegister()
		{
			//this.DeckSelectedSignal.AddListener(OnDeckSelected);
			this.CardAddedToDeckSignal.AddListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.AddListener(OnCardRemovedFromDeck);
			this.ProfileUpdatedSignal.AddListener(OnDeckRenamed);

			this.View.Renamer.EventConfirmed += OnRenameConfirmed;
			this.View.EventCloneClicked += OnCloneInput;
			this.View.EventDeleteClicked += OnDeleteInput;
			this.View.EventScreenShotClicked += OnScreenShot_DeckButtonClicked;
            this.View.EventPracticeClicked += OnPracticeClicked;
            this.View.PlayPracticeButtonFx += UpdateUiHints;

			this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            UpdateUiHints();

        }

        public override void OnRemove()
		{
			//this.DeckSelectedSignal.RemoveListener(OnDeckSelected);
			this.CardAddedToDeckSignal.RemoveListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.RemoveListener(OnCardRemovedFromDeck);
			this.ProfileUpdatedSignal.RemoveListener(OnDeckRenamed);
			
			this.View.Renamer.EventConfirmed -= OnRenameConfirmed;
			this.View.EventCloneClicked -= OnCloneInput;
			this.View.EventDeleteClicked -= OnDeleteInput;
			this.View.EventScreenShotClicked -= OnScreenShot_DeckButtonClicked;
            this.View.EventPracticeClicked -= OnPracticeClicked;
            this.View.PlayPracticeButtonFx -= UpdateUiHints;

            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                //OnDeckSelected(this.PlayerProfile.CurrentDeck);
                this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                PlayFx(null);
            }
        }

		#region Signal Listeners
		/*protected void OnDeckSelected(DeckData selectedDeck)
		{
			this.View.DeckData = selectedDeck;
			//this.View.Refresh();
		}*/

		protected void OnCardAddedToDeck(CardData card, DeckData deck)
		{
			if (deck != this.View.DeckData || !this.View.IsOpen)
				return;
			this.View.Refresh(this.View.Renamer.IsEditable);
		}

		protected void OnCardRemovedFromDeck(CardData card, DeckData deck)
		{
			if (deck != this.View.DeckData || !this.View.IsOpen)
				return;
			this.View.Refresh(this.View.Renamer.IsEditable);
		}

		protected void OnDeckRenamed(ProfileSection section)
		{
            if(section != ProfileSection.DeckList || !this.View.IsOpen)
			//if (deckData != this.View.DeckData)
				return;
			this.View.Refresh(this.View.Renamer.IsEditable);
		}
        
        #endregion

        #region View Listeners
        protected void OnRenameConfirmed(string name)
		{
			if (string.IsNullOrEmpty(name))
            {
                Debug.LogFormat("Entered Deck name was null or empty. Reset to {0}", this.View.DeckData.Name);
                this.View.Renamer.FieldText = this.View.DeckData.Name;
            }
            else if(name != this.View.DeckData.Name)
            { 
                Debug.LogFormat("Renaming deck to {0}", name);
                this.View.DeckData.Name = name;
				this.SaveDeckSignal.Dispatch(this.View.DeckData);
                this.CloseAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
            }
            else
            {
                Debug.LogFormat("Deck name was already {0}", name);
            }
		}
		
		private void OnCloneInput()
		{
            // Save current
            //this.SaveDeckSignal.Dispatch(this.View.DeckData);

            if (this.PlayerProfile.DeckList.Count < this.PlayerProfile.NumDeckSlots)
            {
                this.CloneDeckSignal.Dispatch(this.View.DeckData);
                this.CloseAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
            }
            else
            {
                NewMessageDialogParams dlgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.OutOfDeckSlots,
                    string.Format(I2.Loc.ScriptLocalization.Error.OutOfDeckSlotsMessage, this.PlayerProfile.NumDeckSlots, 0, 0));// + " decks, please remove an existing deck before cloning a new one.");
                this.ShowNewMessageDialogSignal.Dispatch(dlgParams);
            }
		}

		private void OnDeleteInput()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
            this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(string.Format(I2.Loc.ScriptLocalization.DeleteDeck, this.View.DeckData.Name, 0, 0),
                I2.Loc.ScriptLocalization.DeleteDeckCantBeUndone, OnConfirmDialogDismissed, false));
            //this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(DeleteDeckConfirmView));
			//((DeleteDeckConfirmView)this.ViewReferences.Get(typeof(DeleteDeckConfirmView))).Initialize(this.View.DeckData.Name);
			//OnScreenShot_DeckButtonClicked ();
		}

        protected void UpdateUiHints()
        {
            if (this.View.DeckData != null && this.View.DeckData.IsLegalDeck && this.PlayerProfile.UiHintData.Practice <= 1)
            {
                PlayFx(this.View.PracticeButtonFx);
            }
            else
            {
                PlayFx(null);
                //SetGlow(null);
            }
        }

        private ParticleEffectsTrigger _PlayingTrigger;
        public void PlayFx(ParticleEffectsTrigger trigger)
        {
            if (_PlayFxCoroutine != null)
            {
                Timer.Instance.StopCoroutine(_PlayFxCoroutine);
            }
            _PlayFxCoroutine = PlayFxCoroutine(trigger);
            Timer.Instance.StartCoroutine(_PlayFxCoroutine);
        }
        private IEnumerator _PlayFxCoroutine;
        private IEnumerator PlayFxCoroutine(ParticleEffectsTrigger trigger)
        {
            yield return null;
            if (_PlayingTrigger != trigger)
            {
                if (_PlayingTrigger != null)
                {
                    Debug.LogFormat("StoppingFX");
                    _PlayingTrigger.EndEffect();
                    _PlayingTrigger.gameObject.SetActive(false);
                    _PlayingTrigger = null;
                }

                if (this.View.IsOpen && trigger != null)
                {
                    trigger.gameObject.SetActive(true);
                    _PlayingTrigger = trigger;
                    while (!trigger.gameObject.activeInHierarchy)
                    {
                        yield return null;
                    }
                    Debug.LogFormat("StartingFX");
                    trigger.StartEffect();
                    //trigger.gameObject.SetActive(false);
                    //trigger.gameObject.SetActive(true);
                }
            }
        }

        protected void OnPracticeClicked()
        {
            Debug.Log("practice clicked");
            if (this.PlayerProfile.UiHintData.Practice <= 1)
            {
                this.PlayerProfile.UiHintData.Practice = 2;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }
            var startGameSessionData = new StartGameSessionObject
            {
                Deck = this.View.DeckData,
                GameFormat = GameFormat.Practice,
                PlayFabIdToChallenge = string.Empty
            };
            this.CloseAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
            var cardList = (CardListView)this.ViewReferences.Get(typeof(CardListView));
            var draftCardList = (DraftCardListView)this.ViewReferences.Get(typeof(DraftCardListView));
            if (cardList != null && cardList.IsOpen && cardList.DoneDeckBuilding != null)
            {
                Debug.Log("Done deckbuilding");
                cardList.DoneDeckBuilding.Invoke(cardList.DeckData);
            }
            if (draftCardList != null && draftCardList.IsOpen && draftCardList.EventDoneClicked != null)
                draftCardList.EventDoneClicked.Invoke();
            this.StartGameSessionSignal.Dispatch(startGameSessionData);
        }

        private void OnConfirmDialogDismissed(bool isConfirmed)
        {
            if(isConfirmed)
                this.DeleteDeckSignal.Dispatch(this.PlayerProfile.CurrentDeck);
        }

		private void OnScreenShot_DeckButtonClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch (typeof(DeckScreenShotView));
            var view = ViewReferences.Get(typeof(DeckScreenShotView)) as DeckScreenShotView;
            view.Refresh(this.View.DeckData);
        }

		#endregion
	}
}
