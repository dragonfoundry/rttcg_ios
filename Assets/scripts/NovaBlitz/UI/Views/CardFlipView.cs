﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

namespace NovaBlitz.UI
{
	public class CardFlipView : View
	{
		[SerializeField]
		View _ZoomPrefab;
		[SerializeField]
		protected Text _Name;
		[SerializeField]
		protected Image _Graphic;
		[SerializeField]
		protected GameObject _HoverEffect;
		[SerializeField]
		protected CardView _CardPrefab;

		protected CardView _CardInstance;
		protected RectTransform _RootRX;
		protected Animator _Anim;
		protected View _ZoomInstance;

		public event System.Action<bool> EventFlipped;

		public CardData Card {
			get { return _CardInstance.CardData; }
			set { _CardInstance.CardData = value; }
		}

		public bool FaceUp
		{
			get { return _Anim.GetBool("FaceUp"); }
			set { _Anim.SetBool("FaceUp", value); }
		}

		protected override void Awake()
		{
			_RootRX = GetComponent<RectTransform>();
			_Anim = GetComponent<Animator>();
			_CardInstance = _CardPrefab.Spawn();

			var prx = _CardPrefab.GetComponent<RectTransform>();
			var irx = _CardInstance.GetComponent<RectTransform>();
			irx.SetParent(_RootRX);
			UITools.MatchRect(prx, irx);
            irx.anchorMin = new Vector2(0.5f, 0.5f);
            irx.anchorMax = new Vector2(0.5f, 0.5f);
            irx.anchoredPosition = new Vector2(80, 135);
            base.Awake();
		}

		protected override void Start()
		{
			base.Start();
			transform.localScale = Vector3.one;
			Vector3 pos = transform.localPosition;
			pos.z = 0f;
			transform.localPosition = pos;
		}

		public void InstantUp()
		{
			FaceUp = true;
			_Anim.SetTrigger("InstantUp");
		}

		public void TriggerEventFlipped()
		{
			if (null != EventFlipped)
				EventFlipped.Invoke(FaceUp);
		}

		protected void Zoom()
		{
			// TODO: This should be a command to spawn a shared view. ex: InspectCardSignal.Dispatch(cardData);
			/*if (null == _ZoomInstance)
				_ZoomInstance = GameObject.Instantiate(_ZoomPrefab, transform.position, Quaternion.identity) as View;
			_ZoomInstance.transform.SetParent(transform.parent);
			_ZoomInstance.Card = this.Card;*/
		}

		public void Click()
		{
			if (FaceUp)
				Zoom();
			else FaceUp = true;
		}

		public void Hover(bool hover)
		{
			_HoverEffect.SetActive(hover);
		}

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (_CardInstance != null)
                _CardInstance.Recycle();
        }
	}
}
