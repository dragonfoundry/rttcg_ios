﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using I2.Loc;
using strange.extensions.mediation.impl;
using NovaBlitz.Game;
using NovaBlitz.UI.Mediators;

namespace NovaBlitz.UI
{
    public class LanguageSelectorView : AnimatedView
    {
        public MultiLocalizationSelectorView MultiLanguage;
        public ToggleGroup LanguageToggles;
        public TextMeshProUGUI TitleText;
        public event Action<LocalizationView> EventLanguageClicked;
        public Action OnClickClose;

        public void ClickLanguage(LocalizationView view)
        {
            if (EventLanguageClicked != null)
                EventLanguageClicked.Invoke(view);
        }

        public void RefreshLanguages()
        {
            foreach(var view in MultiLanguage.Instances)
            {
                view.UpdateLocalizationView(view.Data);
            }
        }

        public void SetTitle()
        {
            var currentLanguage = LocalizationManager.CurrentLanguage;
            var langname = I2.Loc.ScriptLocalization.Get(currentLanguage);
            if (string.IsNullOrEmpty(langname))
                langname = currentLanguage;
            this.TitleText.SetText(string.Format("{0}: {1}", I2.Loc.ScriptLocalization.Language, langname));
        }

        public void ClickClose()
        {
            if(OnClickClose != null)
            {
                OnClickClose.Invoke();
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            MultiLanguage.Purge();
        }
    }

}

// need to populate a language list with all available languages
// and get the current selected language
// Save the language selection in some kind of settings (account? Device?)
// Check for a selected language on startup
// Check to download a new language file on startup

namespace NovaBlitz.UI.Mediators
{
    public class LanguageSelectorMediator : Mediator
    {
        [Inject] public LanguageSelectorView View { get; set; }
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        
        override public void OnRegister()
        {
            //this.InitializeLanguageSelector();
            this.View.EventLanguageClicked += OnLanguageClicked;
            this.View.OnClickClose += OnCloseClicked;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        override public void OnRemove()
        {
            this.View.EventLanguageClicked -= OnLanguageClicked;
            this.View.OnClickClose -= OnCloseClicked;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {

                InitializeLanguageSelector();
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
                // Unblur on close
                //this.NovaContext.BlurMainCanvas(false, this.View);
            }
        }

        public void OnLanguageClicked(LocalizationView view)
        {
            view.SetLanguage.ApplyLanguage();
            view._Toggle.isOn = true;
            foreach(var card in GameData.CardDictionary.Values)
            {
                card.SetSearchableCardText();
            }
            GameData.SetSearchKeys();
            
            this.View.SetTitle();
            this.View.RefreshLanguages();
            this.LanguageChangedSignal.Dispatch();
            this.CloseAnimatedViewSignal.Dispatch(typeof(LanguageSelectorView));
        }

        public void InitializeLanguageSelector()
        {
            this.View.SetTitle();
            var listOfAllLangunages = I2.Loc.LocalizationManager.GetAllLanguages();
            listOfAllLangunages.Remove("\r");
            // Use this to populate the language list
            var currentLanguage = LocalizationManager.CurrentLanguage;

            List<LocalizationData> languageDataList = new List<LocalizationData>();
            foreach(var language in listOfAllLangunages)
            {
                languageDataList.Add(new LocalizationData { languageName = language, LanguageSelectorView = this.View });
            }
            this.View.MultiLanguage.Populate(languageDataList);
        }

        public void OnCloseClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(LanguageSelectorView));
        }
    }
}
