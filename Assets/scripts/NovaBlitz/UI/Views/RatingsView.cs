﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LinqTools;
using Newtonsoft.Json;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	public class RatingsView : AnimatedView
	{
		public Action CloseClicked;
		public Action RateGameClicked;
		public Action FeedbackClicked;
		public Action LaterClicked;
        public RateGamePoint RateGamePoint;

		public void OnScrimClicked()
		{
			if (this.CloseClicked != null)
			{
				this.CloseClicked.Invoke ();
			}
		}

		public void OnRateGameClicked()
		{
			if (this.RateGameClicked != null)
			{
				this.RateGameClicked.Invoke ();
			}
			
		}

		public void OnFeedbackClicked()
		{
			if (this.FeedbackClicked != null)
			{
				this.FeedbackClicked.Invoke ();
			}
			
		}

		public void OnLaterClicked()
		{
			if (this.LaterClicked != null)
			{
				this.LaterClicked.Invoke ();
			}

		}
	}
}


namespace NovaBlitz.UI.Mediators
{
    public class RatingsMediator : Mediator
    {
        [Inject] public RatingsView View { get; set; }

        [Inject] public PlayerProfile PlayerProfile {get;set;}

        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public LogEventSignal LogEventSignal { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
		{
			this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
			this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
			this.View.CloseClicked += OnCloseClicked;
			this.View.FeedbackClicked += OnFeedbackClicked;
			this.View.RateGameClicked += OnRateGameClicked;
			this.View.LaterClicked += OnLaterClicked;

		}

		/// <summary>
		/// Called on Remove from the context to unrgister signals and event handlers
		/// </summary>
		public override void OnRemove()
		{
			this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
			this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
			this.View.CloseClicked -= OnCloseClicked;
			this.View.FeedbackClicked -= OnFeedbackClicked;
			this.View.RateGameClicked -= OnRateGameClicked;
			this.View.LaterClicked -= OnLaterClicked;
		}

		/// <summary>
		/// Inovked when a view is OPENED with the OpenAnimatedViewSignal
		/// </summary>
		private void OnOpenAnimatedView(Type viewType)
		{
			// Is the view that was opened our view?
			if(viewType == this.View.GetType())
			{
			}
		}

		/// <summary>
		/// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
		/// </summary>
		private void OnCloseAnimatedView(Type viewType)
		{
			// Is the view thatw s closed our view?
			if(viewType == this.View.GetType())
			{
			}
		}

		private void OnCloseClicked()
		{
			OnLaterClicked ();
		}

		private void OnFeedbackClicked()
		{
            Debug.Log("Rate Game: Feedback clicked");

            this.CloseAnimatedViewSignal.Dispatch(typeof(RatingsView));
            this.InitializeAnimatedViewSignal.Dispatch(typeof(ReportBugView),
                    (v) => { ((ReportBugView)v).Initialize(ReportType.RateGameFeedback, null, null); });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ReportBugView));
            LogRatingEvent("feedback");
        }

		private void OnRateGameClicked()
		{
            Debug.Log("Rate game: Rate game clicked");
            this.CloseAnimatedViewSignal.Dispatch(typeof(RatingsView));
#if UNITY_STEAM
            // steam store rating
            Application.OpenURL("http://store.steampowered.com/recommended/recommendgame/388370");
#elif UNITY_IOS
			// app rating flow
			// 
			Application.OpenURL("itms-apps://itunes.apple.com/app/com.dragonfoundry.novablitz");
#elif UNITY_ANDROID
			// android rating flow
			Application.OpenURL("market://details?id=com.dragonfoundry.novablitz/");
#endif

            // stop nagging the player to rate!!!
            try
            {
                this.PlayerProfile.RateGameTrackingData.HasRatedListedVersion = true;
                string converted = JsonConvert.SerializeObject(PlayerProfile.RateGameTrackingData);
                PlayerPrefs.SetString(this.PlayerProfile.MetaProfile.PlayFabID + NovaConfig.RATE_GAME_SUFFIX, converted);
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("Rating Feedback exception {0}", e);
            }
            LogRatingEvent("success");
        }

		private void OnLaterClicked()
		{
            Debug.Log("Rate Game: Later Clicked");
			#if UNITY_STEAM
			#elif UNITY_IOS
			#elif UNITY_ANDROID
			#endif
			this.CloseAnimatedViewSignal.Dispatch(typeof(RatingsView));
            LogRatingEvent("later");

        }

        private void LogRatingEvent(string action)
        {
            this.LogEventSignal.Dispatch(new LogEventParams {
                EventName = "rate_game",
                EventDetails = new Dictionary<string, string> {
                    { "success", action == "success" ? "true": "false" },
                    { "action", action },
                    { "source", View.RateGamePoint.ToString() }
                }
            });
        }
	}


}
