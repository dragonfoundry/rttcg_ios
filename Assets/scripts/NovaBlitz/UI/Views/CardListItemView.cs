using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using DG.Tweening;
using I2.Loc;

namespace NovaBlitz.UI
{
	public class CardListItemView : BaseCardView
	{
		public TextMeshProUGUI CostLabel;
		public TextMeshProUGUI CountLabel;
		public TextMeshProUGUI NameLabel;
        //public Localize NameLocalize;
		public Color DefaultTextColor;
		public Color RedTextColor;
		//public Image CostImage;
		public Image Overlay;
		public Image FrameImage;
		public CanvasGroup CanvasGroup;
        public CardArtImage CardArtImage;


		[System.Serializable]
		public struct FrameImagesClass
		{
			public Sprite Arcane;
			public Sprite Tech;
			public Sprite Divine;
			public Sprite Nature;
			public Sprite Chaos; 
		}
		public FrameImagesClass UnitFrameImages = new FrameImagesClass();
		public FrameImagesClass PowerFrameImages = new FrameImagesClass();

		private Dictionary<CardAspect, Sprite> unitFrameImages = new Dictionary<CardAspect, Sprite>();
		private Dictionary<CardAspect, Sprite> powerFrameImages = new Dictionary<CardAspect, Sprite>();
		private CardListItem cardListItem;
		//private bool isCountLabelTemporary = false;
        private int _MaxOfEachCard = 0;
        
        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.CardListItemView; } }

        // Use this for initialization
        void Awake()
		{
			this.powerFrameImages[CardAspect.Arcane] = this.PowerFrameImages.Arcane;
			this.powerFrameImages[CardAspect.Tech] = this.PowerFrameImages.Tech;
			this.powerFrameImages[CardAspect.Divine] = this.PowerFrameImages.Divine;
			this.powerFrameImages[CardAspect.Nature] = this.PowerFrameImages.Nature;
			this.powerFrameImages[CardAspect.Chaos] = this.PowerFrameImages.Chaos;

			this.unitFrameImages[CardAspect.Arcane] = UnitFrameImages.Arcane;
			this.unitFrameImages[CardAspect.Tech] = this.UnitFrameImages.Tech;
			this.unitFrameImages[CardAspect.Divine] = this.UnitFrameImages.Divine;
			this.unitFrameImages[CardAspect.Nature] = this.UnitFrameImages.Nature;
			this.unitFrameImages[CardAspect.Chaos] = this.UnitFrameImages.Chaos;
			
		}
		
		public override void SelectCard(bool isSelected)
		{
			//Debug.LogFormat("CardListItemView:{0} isSelected:{1}", this.transform.parent.gameObject.name, isSelected);
			this.Overlay.DOFade(isSelected ? 0.6f : 0f, 0.1f);
			this.IsSelected = isSelected;
		}
		
		public override void SetCardArt(Texture cardArtTexture)
		{
			if(cardArtTexture != null)
			{
				this.CardArtImage.texture = cardArtTexture;
				this.CardArtImage.ApplyPositioningData(cardArtTexture.name);
			}
		}
		
		public void Initialize(CardListItem cardListItem, CardData cardData, int maxOfEachCard, System.Action onAssetLoadedCallback = null)
		{
            this._MaxOfEachCard = maxOfEachCard;
			this.cardListItem = cardListItem;
			this.ApplyCardData(cardData, maxOfEachCard);
            Overlay.DOFade(0.0f, 0.0f);
            //if(cardData != null)
            //    Debug.LogFormat("initializing card {0} art {1}", cardData.CardID, cardData.ArtID);
            this.CardArtImage.UpdateCardArt(cardData.ArtID, cardData.Aspect, onAssetLoadedCallback);
            var rect = this.transform as RectTransform;
            if (rect != null)
            {
                rect.anchorMax = new Vector2(0.5f, 0.5f);
                rect.anchorMin = new Vector2(0.5f, 0.5f);
                rect.anchoredPosition = new Vector2(0, 0);
            }
        }
		
		private void ApplyCardData(CardData cardData, int maxOfEachCard)
		{
            if (this.cardListItem == null || cardData == null)
                return;
			this.CountLabel.text = cardListItem.Count.ToString();
			
			this.CardData = cardData;
			
			// HackHack: to enable to the code to transition away from resource cards
			if(cardData.CardType == CardType.Item)
			{
				maxOfEachCard = 0;
			}
			
			this.CountLabel.color = cardListItem.Count > maxOfEachCard ? this.RedTextColor : this.DefaultTextColor;	
			this.CountLabel.gameObject.SetActive(true);
		}

		public override void RefreshIsVisible()
		{
			this.CanvasGroup.alpha = _IsVisible ? 1f : 0f;
			this.CanvasGroup.blocksRaycasts = _IsVisible;
		}

		public override void RefreshCardData()
		{
			RefreshCardData(this.CardData);
		}

		protected void RefreshCardData(CardData cardData)
		{
			this.CostLabel.text = cardData.Cost.ToString();
			this.NameLabel.text = cardData.Name;
			this.CountLabel.gameObject.SetActive(false);

            Sprite frame;
			switch(cardData.CardType)
			{
			case CardType.Power:
				PowerCard power = (PowerCard) cardData;
                if(this.powerFrameImages.TryGetValue(power.Aspect, out frame))
                    this.FrameImage.sprite = frame;
				break;
			case CardType.Unit:
				UnitCard unit = (UnitCard) cardData;
                if (this.powerFrameImages.TryGetValue(unit.Aspect, out frame))
                    this.FrameImage.sprite = frame;
				break;
			case CardType.Item:
                if (this.powerFrameImages.TryGetValue(cardData.Aspect, out frame))
                    this.FrameImage.sprite = frame;
				break;
			}

            this.ApplyCardData(cardData, _MaxOfEachCard);

        }

        public void UpdateLocalization()
        {
            if (this.CardData == null)
                return;
            this.NameLabel.text = this.CardData.Name;
        }
		
		public void TweenHighlight(bool isIncrementing )
		{
			Vector3 scaleSize = new Vector3(1.15f, 1.15f, 1.15f);
            this.Overlay.DOFade(0.6f, 0.0f).OnComplete(() => this.Overlay.DOFade(0.0f, 0.6f));
			if(isIncrementing)
			{
                this.gameObject.transform.DOScale(isIncrementing ? scaleSize : Vector3.one,0.0f).OnComplete(() => this.gameObject.transform.DOScale(Vector3.one, 0.0f));
			}
		}

        public override void OnRecycle()
        {
            base.OnRecycle();

            CardArtImage.ResetArt();
            IsVisible = true;
            IsSelected = false;
            CardData = null;
            Overlay.DOFade(0.0f, 0.0f);
        }
    }
}
