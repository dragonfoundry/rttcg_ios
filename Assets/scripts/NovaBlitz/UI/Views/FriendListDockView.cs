﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using PlayFab.ClientModels;
using ExitGames.Client.Photon.Chat;
using LinqTools;

namespace NovaBlitz.UI
{
    public class FriendListDockView : AnimatedView
	{
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		public PlayerProfile PlayerProfile {get;set;} // Eventually the chat log will be a seperate view and it can inject this dependency in it's own Mediator
        [Inject] public GameData GameData { get; set; }
		public event System.Action<string> EventChatOpened;
		public event System.Action<string> EventChallengeClicked;
		public event System.Action<string> EventDeclineClicked;
        
		public Toggle PlusButtonToggle;
		public Toggle ChatSettingsToggle;
		public TMP_InputField ChatLogTextInput;
		public TMP_InputField AddFriendInputField;
		public System.Action<bool> FriendListToggled;
		public System.Action<string> AddFriendClicked;
		public System.Action<string> RemoveFriendClicked;
		public System.Action<string> BlockFriendClicked;
        public System.Action<string> ConfirmFriendRequestClicked;
        public System.Action<string> DeclineFriendRequestClicked;
        public System.Action<string> BlockFriendRequestClicked;
        public System.Action<string[]> SubmitChatMessage; // Friend playfab id, my message
        public System.Action<FriendNotification> OnFriendInfoUpdated;
		public Button AddFriendButton;
		public Button RemoveFriendButton;
		public Button BlockFriendButton;
		public FriendListItem FriendListItemPrefab;
		public Transform Items;
		public TextMeshProUGUI ChatLogNameLabel;
		public TextMeshProUGUI ChatLogItemPrefab;
		public GameObject OfflineFriendsItem;
		public Transform ChatLogItems;
		//public NovaAudioPlayer ClickAudio;
        public GameObject OnlineChat;
		public GameObject OfflineChat;
        public GameObject ChallengeRoot;
		public GameObject ChatHolder;
        public ScrollRect ChatScrollRect;

        public GameObject ConfirmRequestPanel;
        public TextMeshProUGUI FriendToConfirmName;
        public Button ConfirmFriendRequestButton;
        public Button DeclineFriendRequestButton;
		[SerializeField] protected TextMeshProUGUI _ChallengeHeader;
		[SerializeField] protected Button _ChallengeButton;
		[SerializeField] protected TextMeshProUGUI _ChallengeLabel;
		[SerializeField] protected Button _DeclineButton;
		[SerializeField] protected TextMeshProUGUI _ChallengeNotification;
        
        
        public string SelectedFriendId { get; set; }
        [HideInInspector] public string SelectedFriend;

        public const string ANIM_EXPAND = "IsExpanded";
		public const string ANIM_CHAT = "IsChatting";
        public const string ANIM_CHAT_SETTINGS = "IsChatSettingsToggled";
        public const string ANIM_ADD = "IsAddingFriend";
        public const string ANIM_NOTIFY = "IsNotifying";
        
        public TextMeshProUGUI FriendConfirmHeader;
        public TextMeshProUGUI confirmFriendRequestButtonLabel;
        public TextMeshProUGUI declineFriendRequestButtonLabel;

        public TextMeshProUGUI addFriendButtonLabel;
        public TextMeshProUGUI removeFriendButtonLabel;
        public TextMeshProUGUI blockFriendButtonLabel;
		private string myPlayfabId;
		private bool IsAgeGatePassed = false;
		public string chatLogPlayfabId { get; set; }
        public string confirmPanelPlayFabId { get; set; }
        private FriendListItem[] childItems = new FriendListItem[0];

        public Queue<ChatLogEntry> ChatLogItemCache = new Queue<ChatLogEntry>();
        public ChatLogEntry ChatItemPrefab { get; set; }
        private const int MaxGlobalChatItems = 100;

        public const string YOU_MESSAGE = "<b><#FF8000>You:</color><#DDDDDD> {0}</color></b>";
        //public const string END_MESSAGE = "</color></b>";
        public const string OTHER_USERNAME = "<b><#0080FF>{0}:</color><#DDDDDD> {1}</color></b>";
        //public const string END_OTHER_USERNAME = ":</color><#DDDDDD> ";

        public bool IsChallengeEnabled { set { _ChallengeButton.interactable = value; } }
        public bool IsChallengeIncoming
        {
            set
            {
                _ChallengeHeader.text = value ? I2.Loc.ScriptLocalization.YouAreBeingChallenged : I2.Loc.ScriptLocalization.Click_to_challenge;
                _ChallengeLabel.text = value ? I2.Loc.ScriptLocalization.Accept : I2.Loc.ScriptLocalization.Challenge;
                _DeclineButton.interactable = value;
                _DeclineButton.gameObject.SetActive(value);
            }
        }

        public string AddFriendButtonText
        {
            set
            {
                if (this.addFriendButtonLabel != null)
                {
                    this.addFriendButtonLabel.text = value;
                }
            }
        }

        public string RemoveFriendButtonText
        {
            set
            {
                if (this.removeFriendButtonLabel != null)
                {
                    this.removeFriendButtonLabel.text = value;
                }
            }
        }

        public string BlockFriendButtonText
        {
            set
            {
                if (this.blockFriendButtonLabel != null)
                {
                    this.blockFriendButtonLabel.text = value;
                }
            }
        }

        private int numUnreadChatLogs = 0;
        public int NumUnreadChatLogs
        {
            get { return this.numUnreadChatLogs; }
            set
            {
                this.numUnreadChatLogs = Mathf.Max(0, value);
                //this.ChatNotificationGameObject.SetActive(numUnreadChatLogs != 0);
            }
        }


        /// <summary>
        /// Initializes the Dock to display the FriendsList
        /// </summary>
		public void Initialize(Dictionary<string,NovaBlitzFriend> friendsList, string localPlayfabId, bool? isAgeGatePassed, bool isOnlineStatusUpdate)
		{
			// Turn off chat for Age < 13
			IsAgeGatePassed = isAgeGatePassed == true;
			ChatHolder.gameObject.SetActive (IsAgeGatePassed);

			this.myPlayfabId = localPlayfabId;

			//Debug.Log("Initializing friend screen");
			
			this.OfflineFriendsItem.gameObject.SetActive(false);
			
			List<NovaBlitzFriend> sortedFriendsList = friendsList.Values.Where(v => v.FriendData != null && v.FriendData.DisplayName != null).OrderByDescending(f => f.SortOrder).ThenBy(f => f.FriendData.DisplayName).ToList();
			
			// If we need to add more items because there are more friends thatn list items...
			int numItemsToAdd = sortedFriendsList.Count - childItems.Length;
			if(numItemsToAdd > 0)
            {
                //Debug.LogFormat("Num friends to add {0}", numItemsToAdd);
                for (int i=0;i<numItemsToAdd;i++)
				{
					FriendListItem friendListItem = this.AddFriendListItem();
                    friendListItem.gameObject.SetActive(false);
                }
                childItems = this.Items.GetComponentsInChildren<FriendListItem>(true);
            }
            else if(numItemsToAdd < 0)
            {
                for(int i = numItemsToAdd+1; i <=0; i++)
                {
                    childItems[-i].Recycle();
                }
                childItems = this.Items.GetComponentsInChildren<FriendListItem>(true);
            }
            for (int i = 0; i < childItems.Length; i++)
            {
                childItems[i].gameObject.SetActive(false);
            }

            int numberBlockedFriends = 0;
            int numberRequestFriends = 0;
            int numberPendingFriends = 0;
            int numberOnlineFriends = 0;
            bool isChatNotification = false;
            bool isChallengeNotification = false;
            bool isFriendRequest = false;
            
            for (int i=0;i< childItems.Length;i++)
			{
				FriendListItem friendListItem = childItems[i];
				if(i<sortedFriendsList.Count)
				{
					NovaBlitzFriend friend = sortedFriendsList[i];
					friendListItem.Initialize(this, friend, GameData);
                    friendListItem.gameObject.SetActive(true);
					// Is the chatlog currently open to this friend? 
					if(this.chatLogPlayfabId == friend.FriendData.PlayFabId)
					{
                        // If so, update the chat log input accordingly
                        this.OnlineChat.SetActive(friendListItem.IsOnline);
						this.OfflineChat.SetActive(friendListItem.IsOnline == false);
                        // and turn on/off the "challenge" section accordingly
                        this.ChallengeRoot.SetActive(friendListItem.IsOnline);
					}
                    else
                    {
                        List<ChatMessage> chatLog;
                        if (this.PlayerProfile.ChatLogs.TryGetValue(friend.FriendData.PlayFabId, out chatLog) && chatLog != null)
                        {
                            friendListItem.HasUnreadMessages = chatLog.Any(c => !c.isRead);
                        }
                    }

                    friendListItem.IsChallenging = this.PlayerProfile.FriendChallenges.ContainsKey(friend.FriendData.PlayFabId);

                    if (friendListItem.IsChallenging)
                        isChallengeNotification = true;
                    if(friendListItem.HasUnreadMessages)
                        isChatNotification = true;
                    if (friend.IsBlocked)
                    {
                        numberBlockedFriends++;
                    }
                    else if (friend.FriendData.IsRequest)
                    {
                        isFriendRequest = true;
                        numberRequestFriends++;
                        if (friendListItem.IsOnline)
                        {
                            numberOnlineFriends++;
                        }
                    }
                    else if (friend.FriendData.IsPending)
                    {
                        numberPendingFriends++;
                    }
                    else if (friendListItem.IsOnline)
                    {
                        numberOnlineFriends++;
                    }
                }
				else
				{
					friendListItem.Recycle ();
				}
            }
            if (!isOnlineStatusUpdate)
            {
                //Debug.LogFormat("Initialized {0} friends on a list {1} long. {2} online", sortedFriendsList.Count, childItems.Length, numberOnlineFriends);

                // Instantiate and initialize friends list items
                if (SelectedFriend != null)
                {
                    var selectedFriend = GetFriendListItem(SelectedFriend);
                    if (selectedFriend != null)
                    {
                        selectedFriend.Glow.SetActive(false);
                        selectedFriend.ItemButton.onClick.Invoke();
                    }
                }
            }
            else if(SelectedFriend != null)
            {
                var selectedFriend = GetFriendListItem(SelectedFriend);
                if (selectedFriend != null)
                {
                    selectedFriend.Glow.SetActive(true);
                }
            }
            if (this.OnFriendInfoUpdated != null)
                this.OnFriendInfoUpdated.Invoke(new FriendNotification {
                    numberOfFriends = childItems.Length,
                    numberOnlineFriends = numberOnlineFriends,
                    isChallengeRequest = isChallengeNotification,
                    isChatMessage = isChatNotification,
                    isFriendRequest = isFriendRequest });

            this.OfflineFriendsItem.transform.SetSiblingIndex(numberOnlineFriends);
			this.OfflineFriendsItem.SetActive(true);
		}

		public void UpdateChallenge(string playfabID, bool incoming)
		{
			var target = childItems.FirstOrDefault(f => f.PlayfabId == playfabID);
			if (null != target) {
				target.IsChallenging = incoming;
				if (target.PlayfabId == this.SelectedFriend)
					this.IsChallengeIncoming = incoming;
			}

            if (this.OnFriendInfoUpdated != null)
                this.OnFriendInfoUpdated.Invoke(new FriendNotification { isChallengeRequest = childItems.Any(f => f.IsChallenging) });
        }

		public void NotifyChallenge(string playfabID, PhotonMessageType msg)
		{
			var target = GetFriendListItem(playfabID);
			if (null != target && target.PlayfabId == this.SelectedFriend) {
				string note = string.Empty;
				switch (msg) {
				case PhotonMessageType.CancelChallenge:
					note = I2.Loc.ScriptLocalization.IncomingChallengeCanceled;
					break;
				case PhotonMessageType.DeclineChallenge:
					note = I2.Loc.ScriptLocalization.ChallengeDeclined;
					break;
				}
				_ChallengeNotification.text = note;
				this.Animator.SetTrigger(ANIM_NOTIFY);
			}
		}

		public void HideFriendChatLog()
		{
            //Debug.Log("HideFriendChatLog");
			//this.ClickAudio.Play();
			this.chatLogPlayfabId = null;
			this.Animator.SetBool(ANIM_CHAT, false);
		}

        public void HideConfirmPanel()
        {
            //Debug.Log("HideConfirmPanel");
            //this.ClickAudio.Play();
            this.confirmPanelPlayFabId = null;
            this.ConfirmRequestPanel.gameObject.SetActive(false);
            //this.Animator.SetBool(ANIM_CONFIRM, false);
        }

		private void AddChatMessagesToLog(string friendPlayfabId)//, NovaBlitzFriend friend)
		{
			List<ChatMessage> chatLog = null;
            //Debug.LogFormat("AddChatMessageToLog: {0}", friendPlayfabId);

            // Destroy any existing chat log entries
            /*for(int i=0; i<this.ChatLogItems.childCount; i++)
			{
				Destroy(this.ChatLogItems.GetChild(i).gameObject);
			}*/
            ClearChatItems();
			// Populate the chatlog, no more than CHAT_LOG_SIZE items
			if(this.PlayerProfile.ChatLogs.TryGetValue(friendPlayfabId, out chatLog))
			{
				Debug.LogFormat("Found {0} Existing chat items for {1}", chatLog.Count, friendPlayfabId);
				for(int i=Mathf.Max(0, chatLog.Count-NovaConfig.CHAT_LOG_MAX_SIZE); i<chatLog.Count; i++)
				{
                    //var msg = chatLog[i];
                    //if(friend != null && msg.SenderPlayfabId != this.PlayerProfile.MetaProfile.PlayFabID)
                    //    msg.SenderAvatarArtId = friend.AvatarArtId;
					this.AddChatMessageToLog(friendPlayfabId, chatLog[i], false);
				}
			}
		}

		/// <summary>
		/// Initializes and adds a chatItem to the current chat log if the log is currently visible
		/// </summary>
		public void AddChatMessageToLog(string friendPlayfabId, ChatMessage chatMessage, bool isCombined)
		{
            Debug.LogFormat("IsChatting:{0} chatLogID:{1}, friend:{2} From:{3} my:{4}", this.Animator.GetBool(ANIM_CHAT), this.chatLogPlayfabId, friendPlayfabId, chatMessage.SenderPlayFabId, this.myPlayfabId );
            if ((this.chatLogPlayfabId == friendPlayfabId 
				|| chatMessage.SenderPlayFabId == this.chatLogPlayfabId))
			{
                if(isCombined && LastEntryAdded != null)
                {
                    LastEntryAdded.SetChatMessage(LastEntryAdded.ChatMessage,null);
                }
                else
                    AddChatItem(chatMessage);
                // Initialize and add a chatItem to the log
                if(chatMessage.isRead == false)
                {
                    chatMessage.isRead = true;
                    this.PlayerProfile.isChatLogNeedsSaving = true;
                }
                //Debug.LogFormat("Adding message to log '{0}'", chatMessage);
            }
			else if(IsAgeGatePassed)
			{
				//Debug.LogFormat("Has unread messages True! {0},{1}", friendPlayfabId, chatMessage.Message);
                // Get the friend list item associated with this chat message
				FriendListItem friendItem = this.GetFriendListItem(friendPlayfabId);
				if(friendItem != null)
				{
					//Debug.Log ("Setting chat message Flag on object");
					// Set the chat alert icon on that item
					friendItem.HasUnreadMessages = true;
                    if (this.OnFriendInfoUpdated != null)
                        this.OnFriendInfoUpdated.Invoke(new FriendNotification { isChatMessage = true });
                    --this.NumUnreadChatLogs;
				}
            }
		}

        public void ClearChatItems()
        {
            while (ChatLogItemCache.Count > 0)
            {
                var item = ChatLogItemCache.Dequeue();
                item.Recycle();
            }
            LastEntryAdded = null;
        }
        private ChatLogEntry LastEntryAdded;
        private void AddChatItem(ChatMessage message)
        {
            if (ChatLogItemCache.Count >= MaxGlobalChatItems)
            {
                var toRecycle = ChatLogItemCache.Dequeue();
                toRecycle.Recycle();
            }

            var chatItem = ObjectPool.Spawn(this.ChatItemPrefab);
            ChatLogItemCache.Enqueue(chatItem);
            chatItem.SetChatMessage(message, LastEntryAdded);
            LastEntryAdded = chatItem;


            chatItem.transform.AttachToParent(this.ChatLogItems.transform, this.ChatLogItems.transform.childCount);
            chatItem.transform.localScale = Vector3.one;
            chatItem.transform.localPosition = Vector3.zero;
        }

        public void AddMessages(List<ChatMessage> messages)
        {
            foreach (var message in messages)
            {
                AddChatItem(message);
            }
        }

        public FriendListItem AddFriendListItem()
		{
            FriendListItem friendListItem = this.FriendListItemPrefab.Spawn();

            friendListItem.transform.SetParent(this.Items, true);
            friendListItem.transform.localPosition = Vector3.zero;
            friendListItem.transform.localScale = Vector3.one;
			return friendListItem;
		}

		private FriendListItem GetFriendListItem(string playfabId)
		{
            return childItems.Where(f=>f.PlayfabId == playfabId).FirstOrDefault();
		}

		#region uGUI
		public void ClickChallenge() { if (null != this.EventChallengeClicked && null != this.SelectedFriend) this.EventChallengeClicked.Invoke(this.SelectedFriend); }
		public void ClickDecline() { this.IsChallengeIncoming = false; if (null != this.EventDeclineClicked && null != this.SelectedFriend) this.EventDeclineClicked.Invoke(this.SelectedFriend); }

		public void ToggleExpand(bool isOn)
		{
		}

        /// <summary>
        /// Invoked when the user clicks the "Confirm button on the confirm friend dialog
        /// </summary>
        public void ConfirmFriendRequest()
        {
            if (this.ConfirmFriendRequestClicked != null)
            {
                this.ConfirmFriendRequestButton.interactable = false;
                this.DeclineFriendRequestButton.interactable = false;
                this.confirmFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Confirming;
                this.ConfirmFriendRequestClicked.Invoke(this.confirmPanelPlayFabId);
            }
        }
        
        /// <summary>
        /// Invoked when the user clicks the "Decline" button on the confirm friend dialog
        /// </summary>
        public void DeclineFriendRequest()
        {
            if (this.DeclineFriendRequestClicked != null)
            {
                this.ConfirmFriendRequestButton.interactable = false;
                this.DeclineFriendRequestButton.interactable = false;
                this.declineFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Declining;
                this.DeclineFriendRequestClicked.Invoke(this.confirmPanelPlayFabId);
            }
        }
        
        /// <summary>
        /// Invoked when the user clicks the "Remove" button on the friend chat dialog
        /// <summary>
        public void RemoveFriend()
		{
			if(this.RemoveFriendClicked != null)
            {
                this.RemoveFriendButton.interactable = false;
                this.BlockFriendButton.interactable = false;
                this.RemoveFriendButtonText = I2.Loc.ScriptLocalization.Removing;
                this.RemoveFriendClicked.Invoke(this.chatLogPlayfabId);
			}	
		}
		
		/// <summary>
		/// Invoked when the user clicks the "Block" button on the friend chat dialog, or the "block" button on the confirm dialog, if we enable that.
		/// <summary>
		public void BlockFriend()
		{
			if(this.BlockFriendClicked != null)
            {
                this.BlockFriendButton.interactable = false;
                this.RemoveFriendButton.interactable = false;
                this.BlockFriendButtonText = I2.Loc.ScriptLocalization.Blocking;
                this.BlockFriendClicked.Invoke(this.chatLogPlayfabId);
			}
		}

		/// <summary>
		/// Invoked by the little settings gear in the top right corner of the chat log frame
		/// </summary>
		public void OnChatSettingsToggled(bool isActive)
		{
			this.Animator.SetBool(ANIM_CHAT_SETTINGS, isActive);
			this.RemoveFriendButtonText = I2.Loc.ScriptLocalization.Remove;
            this.BlockFriendButtonText = I2.Loc.ScriptLocalization.Block;
            this.RemoveFriendButton.interactable = isActive;
            this.BlockFriendButton.interactable = isActive;
        }
        
        /// <summary>
        /// When someone clicks the scrim that appears during the Challenge friend flow
        /// </summary>
        public void OnScrimClicked()
		{
			FriendListItem friendItem = this.GetFriendListItem(this.chatLogPlayfabId);
			if (friendItem != null)
				friendItem.Glow.SetActive(false);
			HideFriendChatLog();
            HideConfirmPanel();
            this.PlusButtonToggle.isOn = false;

            this.CloseAnimatedViewSignal.Dispatch(this.GetType());
		}

		/// <summary>
		/// Used when the little "+" button in the friends list titlebar is pressed
		/// </summary>
		public void OnAddFriendToggled(bool isActive)
		{
			this.Animator.SetBool(ANIM_ADD, isActive);
			
			if(isActive)
			{
				this.AddFriendButtonText = I2.Loc.ScriptLocalization.Add_Friend;
                this.AddFriendInputField.text = string.Empty;
				StartCoroutine(SelectInputField(this.AddFriendInputField));
			}
		}
		
		/// <summary>
		/// Delays a fraction of a second before activating an input field
		/// </summary>
		private IEnumerator SelectInputField(TMP_InputField inputField)
		{
			yield return new WaitForSecondsRealtime(0.3f);
			inputField.ActivateInputField();
            inputField.Select();
        }

        /// <summary>
        /// Triggered when the chat input loses focus, if it came from an enter being pressed.. send the chat
        /// and keep the focus on the input box
        /// </summary>
        public void EndAddFriendEdit()
        {
            if (Input.GetKeyDown(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !AddFriendInputField.wasCanceled))
            {
                AddFriend();
            }
        }

        /// <summary>
        /// Invoked by the "Add Friend" button on the Add Friend panel.
        /// </summary>
        private void AddFriend()
		{
			if(this.AddFriendClicked != null)
			{
				this.AddFriendButton.interactable = false;
				this.AddFriendButtonText = I2.Loc.ScriptLocalization.Searching;
				this.AddFriendClicked.Invoke(this.AddFriendInputField.text);
			}
		}

        /// <summary>
        /// Called when a friendlist item is clicked, if you aren't yet friends.
        /// </summary>
        /// <param name="playFabId"></param>
        public void DoFriendRequestConfim(string playFabId, string friendName, FriendRequestConfirmType type)
        {
            if (this.hideConfirmPanelCoroutine != null)
                StopCoroutine(this.hideConfirmPanelCoroutine);
            this.confirmPanelPlayFabId = playFabId;
            this.ConfirmRequestPanel.gameObject.SetActive(true);
            this.FriendToConfirmName.text = friendName;
            this.HideFriendChatLog();
            foreach (var friend in childItems)
            {
                friend.Glow.SetActive(friend.PlayfabId == playFabId);
            }
            switch(type)
            {
                case FriendRequestConfirmType.Blocked:
                    this.ConfirmFriendRequestButton.interactable = true;
                    this.DeclineFriendRequestButton.interactable = false;
                    ConfirmFriendRequestButton.gameObject.SetActive(true);
                    DeclineFriendRequestButton.gameObject.SetActive(false);
                    FriendConfirmHeader.text = I2.Loc.ScriptLocalization.Blocked;
                    confirmFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Unblock;
                    declineFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Cancel;
                    break;
                case FriendRequestConfirmType.Request:
                    this.ConfirmFriendRequestButton.interactable = true;
                    this.DeclineFriendRequestButton.interactable = true;
                    ConfirmFriendRequestButton.gameObject.SetActive(true);
                    DeclineFriendRequestButton.gameObject.SetActive(true);
                    FriendConfirmHeader.text = I2.Loc.ScriptLocalization.Friend_Request_From;
                    confirmFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Confirm;
                    declineFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Decline;
                    break;
                case FriendRequestConfirmType.Pending:
                    this.ConfirmFriendRequestButton.interactable = false;
                    this.DeclineFriendRequestButton.interactable = true;
                    ConfirmFriendRequestButton.gameObject.SetActive(false);
                    DeclineFriendRequestButton.gameObject.SetActive(true);
                    FriendConfirmHeader.text = I2.Loc.ScriptLocalization.Friend_Requested;
                    confirmFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Confirm;
                    declineFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Cancel;
                    break;
            }
        }

		/// <summary>
		/// Called by the FriendsListItems when they are clicked (in default friend mode)
		/// </summary>
		public void DoFriendChatLog(string playfabId, string playerName, bool isSameFriend)
        {
            if (this.hideChatPanelCoroutine != null)
                StopCoroutine(this.hideChatPanelCoroutine);
            //this.ClickAudio.Play();
            HideConfirmPanel();
            this.RemoveFriendButton.interactable = true;
			this.RemoveFriendButtonText = I2.Loc.ScriptLocalization.Remove;
			this.BlockFriendButton.interactable = true;
			this.BlockFriendButtonText = I2.Loc.ScriptLocalization.Block;
			this.ChatSettingsToggle.isOn = false;
            if(!isSameFriend)
            {
                ChatLogTextInput.text = string.Empty;
            }

            // Get a list of all the friends list items
            bool isChatNotification = false;
            bool isChallengeNotification = false;
            foreach (var friend in childItems)
            {
                if (friend.PlayfabId != playfabId)
                {
                    friend.Glow.SetActive(false);
                    List<ChatMessage> chatLog;
                    if(this.PlayerProfile.ChatLogs.TryGetValue(friend.PlayfabId, out chatLog) && chatLog != null)
                    {
                        friend.HasUnreadMessages = chatLog.Any(c => !c.isRead);
                    }

                }
                else
                {
                    friend.Glow.SetActive(true);
                    friend.HasUnreadMessages = false;
                    --this.NumUnreadChatLogs;
                    NovaBlitzFriend nbFriend;
                    this.PlayerProfile.FriendListData.TryGetValue(playfabId, out nbFriend);
                    this.RemoveFriendButton.gameObject.SetActive(nbFriend == null || !nbFriend.FriendData.IsSteamFriend);
                    this.ChatLogNameLabel.text = playerName;
                    this.chatLogPlayfabId = friend.PlayfabId;
                    this.AddChatMessagesToLog(friend.PlayfabId); //, nbFriend);
                    this.StartCoroutine(IsChatting());
                    this.OnlineChat.SetActive(friend.IsOnline);
                    this.OfflineChat.SetActive(friend.IsOnline == false);
                    this.ChallengeRoot.SetActive(friend.IsOnline);

                    if (friend.IsOnline && this.isActiveAndEnabled)
                    {
#if UNITY_STANDALONE || UNITY_EDITOR
                        StartCoroutine(SelectInputField(this.ChatLogTextInput));
#endif
                    }
                }
                if (friend.HasUnreadMessages)
                    isChatNotification = true;
                if (friend.IsChallenging)
                    isChallengeNotification = true;
            }
            if (this.OnFriendInfoUpdated != null)
                this.OnFriendInfoUpdated.Invoke(new FriendNotification {
                    isChatMessage = isChatNotification,
                    isChallengeRequest = isChallengeNotification });

			if (null != this.EventChatOpened)
				this.EventChatOpened.Invoke(playfabId);
		}

		IEnumerator IsChatting()
		{
			yield return null;
			this.Animator.SetBool(ANIM_CHAT, true);
		}

		/// <summary>
		/// Triggered when the chat input loses focus, if it came from an enter being pressed.. send the chat
		/// and keep the focus on the input box
		/// </summary>
		public void EndChatEdit()
		{
			if(Input.GetKeyDown(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !ChatLogTextInput.wasCanceled))
            {
				DoSubmitChatMessage();
				this.ChatLogTextInput.ActivateInputField();
                this.ChatLogTextInput.Select();
			}
		}

		/// <summary>
		/// Used by the UI to signle adding a message typed in by the user to a chat
		/// <summary>
		private void DoSubmitChatMessage()
		{
			string friendPlayfabId = this.chatLogPlayfabId;
			string message = this.ChatLogTextInput.text.Trim();
			this.ChatLogTextInput.text = string.Empty;
            this.ChatScrollRect.verticalNormalizedPosition = 0;

            // Only send chat messsages with content
            if (string.IsNullOrEmpty(message)== false)
			{
				this.SubmitChatMessage.Invoke(new string[] { friendPlayfabId, message });
			}
		}



        private void ClickReportFriend(ChatMessage message)
        {
            //if(this.R)
        }

        ///<summary>
        /// Called when the input changes on the username field
        ///</summary>
        public void UsernameValueChanged(string value)
		{
			this.AddFriendButton.interactable = true;
			this.AddFriendButtonText = I2.Loc.ScriptLocalization.Add_Friend;
		}
        #endregion

        protected override void OnDestroy()
        {
            base.OnDestroy();
            for (int i = 0; i < childItems.Length; i++)
            {
                childItems[i].Recycle();
            }
            ClearChatItems();
        }

        #region Async
        private IEnumerator hideAddFriendPanelCoroutine;
        private IEnumerator hideConfirmPanelCoroutine;
        private IEnumerator hideChatPanelCoroutine;

        public void HideFriendPanelInTwoSeconds()
        {
            if (this.hideAddFriendPanelCoroutine != null)
                StopCoroutine(this.hideAddFriendPanelCoroutine);
            this.hideAddFriendPanelCoroutine = HideAddFriendPanelCoroutine();
            StartCoroutine(this.hideAddFriendPanelCoroutine);
        }

        private IEnumerator HideAddFriendPanelCoroutine()
        {
            yield return new WaitForSecondsRealtime(1);
            //Debug.Log("HideAddFriendPanel");
            this.PlusButtonToggle.isOn = false;
        }


        public void HideConfirmPanelInOneSecond(string playFabId)
        {
            if (this.hideConfirmPanelCoroutine != null)
                StopCoroutine(this.hideConfirmPanelCoroutine);
            this.hideConfirmPanelCoroutine = HideConfirmPanelCoroutine(playFabId);
            StartCoroutine(this.hideConfirmPanelCoroutine);
        }

        private IEnumerator HideConfirmPanelCoroutine(string playFabId)
        {
            yield return new WaitForSecondsRealtime(1);
            if (this.confirmPanelPlayFabId == playFabId)
                HideConfirmPanel();
        }


        public void HideChatPanelInOneSecond(string playFabId)
        {
            if (this.hideChatPanelCoroutine != null)
                StopCoroutine(this.hideChatPanelCoroutine);
            this.hideChatPanelCoroutine = HideChatPanelCoroutine(playFabId);
            StartCoroutine(this.hideChatPanelCoroutine);
        }

        private IEnumerator HideChatPanelCoroutine(string playFabId)
        {
            yield return new WaitForSecondsRealtime(1);
            if (this.chatLogPlayfabId == playFabId)
                HideFriendChatLog();
        }

        #endregion

    }

    public enum FriendRequestConfirmType
    {
        NoType,
        Request,
        Pending,
        Blocked,
    }
}
