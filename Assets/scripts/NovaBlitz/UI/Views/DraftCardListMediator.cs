﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using System;
using LinqTools;
using System.Collections.Generic;
using Messages;
using UnityEngine.UI;

namespace NovaBlitz.UI
{
    public class DraftCardListMediator : Mediator
    {
        [Inject] public DraftCardListView View {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public UIConfiguration UIConfiguration {get;set;}
        [Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public SubmitDraftPickSignal SubmitDraftPickSignal {get;set;}
        //[Inject] public SaveDraftDeckSignal SaveDraftDeckSignal {get;set;}
        [Inject] public DraftOverSignal DraftOverSignal {get;set;}
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal{get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }


        private static int MAX_VISIBLE_DECK_ITEMS = 21;
        
        override public void OnRegister()
        {
			this.View.CardListItemPrefab = this.UIConfiguration.CardListItemPrefab;
			this.View.DraggableCardPrefab = this.UIConfiguration.DraggableCardPrefab;
            this.View.EventDoneClicked += this.OnDoneDrafting;
            this.View.EventCardDropped += this.OnView_CardDropped;
            this.View.CardsDrafted += this.OnCardsDrafted;
            this.View.CardListItemDraggedTo += this.OnCardListItemDraggedTo;
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.DraftOverSignal.AddListener(this.OnDraftOver);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);

            OnOpenAnimatedView(typeof(DraftCardListView));
        }

        override public void OnRemove()
        {
            this.View.EventDoneClicked -= this.OnDoneDrafting;
            this.View.EventCardDropped -= this.OnView_CardDropped;
            this.View.CardsDrafted -= this.OnCardsDrafted;
            this.View.CardListItemDraggedTo -= this.OnCardListItemDraggedTo;
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.DraftOverSignal.RemoveListener(this.OnDraftOver);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
        }
        

        #region View Listeners
        /// <summary>
        /// When A card is added to the draft deck. Initaliaze the deck in the PlayerProfile if
        /// needed. Add the card to the deck, and save the deck to playfab.
        /// </summary>
        protected void OnView_CardDropped(CardData cardData)
		{
			if(this.PlayerProfile.DraftDeck == null)
			{
				this.PlayerProfile.DraftDeck = new DeckData(GameFormat.Draft);
                this.PlayerProfile.DraftDeck.SetDeckArt();
			}

			this.AddCardToList(cardData, this.PlayerProfile.DraftDeck, this.View.DeckListScrollRect, MAX_VISIBLE_DECK_ITEMS);
		}
		#endregion

        private void OnCardListItemDraggedTo(CardData cardData, DraftDropTarget draftDropTarget)
        {
            switch(draftDropTarget)
            {
                case DraftDropTarget.DeckList:
                    //Debug.LogFormat("Moving card '{0}' to DeckList", cardData.Name);
                    this.AddCardToList(cardData, this.PlayerProfile.DraftDeck, this.View.DeckListScrollRect, MAX_VISIBLE_DECK_ITEMS);
                break;
            }
        }

        /// <summary>
        /// Handles the DraftOverEvent for when the draft finishes. Sets the done button lable
        /// to "Done" and updates the draft status in the PlayerProfile
        /// </summary>
        private void OnDraftOver()
        {
            Debug.Log("Draft Over");
            this.View.DoneButtonLabel.text = I2.Loc.ScriptLocalization.Done;
			this.View.ClickDone();
			if(this.PlayerProfile.DraftData.event_status != TournamentStatus.playing)
            {
				this.PlayerProfile.DraftData.event_status = TournamentStatus.playing;
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.DraftDeck);
            }
        }
        
        /// <summary>
        /// Detects the DraftCardListView opening and does some initialzation.
        /// Populates the "unused" deck in the player profile that gets used to manage
        /// the unusedCards list in the draft screen.
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            if(viewType == this.View.GetType())
            {   
                DeckData draftDeck = this.PlayerProfile.DraftDeck;
                
                Debug.LogWarningFormat("DraftCardListMediator: Draft deck is:{0} draftStatus:{1}", draftDeck, this.PlayerProfile.DraftData.event_status);
                
                // if we're drafting and there's a draft deck...
                if(this.PlayerProfile.DraftData.event_status == Messages.TournamentStatus.drafting 
                    && draftDeck != null)
				{ 
					// add the draft deck cardListITems to the deckList
					this.View.Recycle();
                    this.View.Initialize(draftDeck, this.View.DeckListScrollRect, this.View.DraftDeckListDropTarget, MAX_VISIBLE_DECK_ITEMS); 
                }
                
                this.View.UpdateHeaders(draftDeck == null ? 0 : draftDeck.Cards.Count, PlayerProfile.DRAFT_DECK_SIZE);
            }
            else if(this.View.IsOpen && viewType == typeof(TwoTouchOverlayView))
            {
                // Makek sure we move to the overlay view state when the twotouch opens
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if (this.View.IsOpen && viewType == this.View.GetType())
            {

            }
            else if (viewType == typeof(TwoTouchOverlayView))
            {
                // Makek sure we move to the overlay view state when the twotouch opens
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Main, 0.0f);
            }
        }

        public void OnLanguageChanged()
        {
            this.View.UpdateHeaders(this.PlayerProfile.DraftDeck == null? 0 : this.PlayerProfile.DraftDeck.Cards.Count, PlayerProfile.DRAFT_DECK_SIZE);
        }

        /// <summary>
        /// When a card is drafted, submit the drat pick, update the profile's drafted cards 
        /// list and drafting status
        /// </summary>
        private void OnCardsDrafted(CardData firstCard, int pickNumber)
        {
            /*if (firstCard == null)
                Debug.Log("Card drafted. First card is null");
            else if (this.PlayerProfile == null)
                Debug.Log("Draft player profile is null");
            else if (this.PlayerProfile.DraftProgress == null)
                Debug.Log("Draft progress is null");
            else if (this.PlayerProfile.DraftProgress.DraftedCards == null)
                Debug.Log("Draft progress is null");
            else if (this.PlayerProfile.DraftData == null)
                Debug.Log("Draft data is null");
            else
                Debug.Log("Cards Drafted");*/

            Messages.DraftPick draftPick = new Messages.DraftPick();
            draftPick.CardToPick = firstCard.CardID;
            //draftPick.SecondCardToPick = Int32.Parse(secondCard.CardID);
            draftPick.PickNumber = pickNumber;
            this.SubmitDraftPickSignal.Dispatch(draftPick);
            
            // Have the profile know about this drafted card without having to reload it form playfab
            if(this.PlayerProfile.DraftProgress.DraftedCards == null)
            {
                this.PlayerProfile.DraftProgress.DraftedCards = new List<int>();
            }

            this.PlayerProfile.DraftProgress.DraftedCards.Add(firstCard.CardID);
            
            //this.SaveDraftDeckSignal.Dispatch(this.PlayerProfile.DraftDeck);
            
            // TODO: do this work from a command
            if(this.PlayerProfile.DraftData.event_status != TournamentStatus.drafting)
            {
				this.PlayerProfile.DraftData.event_status = TournamentStatus.drafting;
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.DraftDeck);
            }
        }
        
        /// <summary>
        /// Handles the DoneButton being clicked in the view, various cleanup tasks and hiding/showing
        /// of UI.
        /// </summary>
        private void OnDoneDrafting()
        {
            if (this.PlayerProfile.DraftDeck == null)
				this.PlayerProfile.DraftDeck = new DeckData(GameFormat.Draft);

            // Clear this out as it might be in a weird state (one of two cards drafted for example)
            this.PlayerProfile.DraftDeck.Cards.Clear();
            foreach (var cardId in this.PlayerProfile.DraftProgress.DraftedCards)
            {
                CardData cardData;
                if (GameData.CardDictionary.TryGetValue(cardId, out cardData))
                {
                    CardItem card = new CardItem(cardData, -2);
                    this.PlayerProfile.DraftDeck.Cards.Add(card);
                }
            }
			this.PlayerProfile.DraftDeck.QueryAspects();
            this.PlayerProfile.DraftDeck.SetDeckArt();

            // TODO: update the profile form a command
            if (this.PlayerProfile.DraftData.event_status == TournamentStatus.not_playing)
				this.PlayerProfile.DraftData.event_status = TournamentStatus.drafting;
			this.ProfileUpdatedSignal.Dispatch(ProfileSection.DraftDeck);
        }
        
        /// <summary>
        /// Adds a CardListItemView to the list of cardItems specified in the parametrs.
        /// also adds the card to the deckData specified in the parameters.
        /// </summary>
        private CardListItemView AddCardToList(CardData cardData, DeckData deckToAddTo, ScrollRect scrollRect, int maxVisibleListItems )
        {
            // Get the index of the card in the transformed cardList
			CardListItem listItemData = null;
			CardListItemView cardListItemView = null;
			int indexInView = 0;
            
            // Add the Card item to the deck
            deckToAddTo.Cards.Add(new CardItem(cardData,-1));
            // Update the deck's aspects
            deckToAddTo.QueryAspects();
            deckToAddTo.SetDeckArt();

            // Find its index in a CardItemList
            indexInView = CardListTransform.IndexOfCard(deckToAddTo,cardData, ref listItemData);    
           
			// Get a reference to the existing cardListItemView (if one exists)
			if(indexInView < this.View.Cards.Count())
			{
				var existing = this.View.Cards.FirstOrDefault(c => c.CardData.CardID == cardData.CardID);
				if (null != existing)
					cardListItemView = (CardListItemView)existing.CardDisplay;
			}
			
			// If one is found... does it match the card being added to the deck?
			if(cardListItemView != null && cardListItemView.CardData.CardID == cardData.CardID)
			{
				// Initialize the View with the correct ListItem Data
				cardListItemView.Initialize(listItemData, cardData, deckToAddTo.MaxOfEachCard);
			}
			else
			{
				// Create a new item at the specified index
				cardListItemView = this.View.AddCardItemToList(listItemData, indexInView, deckToAddTo.MaxOfEachCard);
			}		
		
			// Play a tween affect on the affected item in the list
			cardListItemView.TweenHighlight(true);

			// Scroll the list so that the added card is visible
			/*if(this.View.Cards.Count() > 1)
			{
				float scrollbarValue = 1.0f - (indexInView / (float)(this.View.Cards.Count()-1));
				//StartCoroutine(SetScrollBarValue(scrollbarValue));
			}*/
            
            this.View.UpdateHeaders(this.PlayerProfile.DraftDeck.Cards.Count, PlayerProfile.DRAFT_DECK_SIZE);
            
            return cardListItemView;
        }
    }
}