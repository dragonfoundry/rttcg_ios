﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TMPro;
using NovaBlitz.Economy;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using Messages;

namespace NovaBlitz.UI
{
    public class ConfirmVirtualPurchaseDialogView : AnimatedView
    {
        //[Inject] public PlayerProfile PlayerProfile { get; set; }

        public Action<bool, CurrencyType, uint> DialogDismissed; // bools isConfirmed
        public TextMeshProUGUI MessageLabel;
        public TextMeshProUGUI TitleLabel;
        public TextMeshProUGUI CRcostLabel;
        public TextMeshProUGUI NGcostLabel;
        public Button BuyCRButton;
        public Button BuyNGButton;
        public Button CloseButton;
        public HorizontalLayoutGroup LayoutGroup;

        public uint CRprice { get; set; }
        public uint NGprice { get; set; }
        public string StoreId { get; set; }
        public string ItemId { get; set; }

        public void Initialize(StoreData storeData, PoolableMonoBehaviour view, int currentCurrency)
        {
            // discard any existing view that's attached
            foreach(Transform child in LayoutGroup.transform)
            {
                var poolable = child.GetComponent<PoolableMonoBehaviour>();
                if(poolable != null)
                {
                    poolable.Recycle();
                }
                else
                    Destroy(child.gameObject);
            }

            // Clone the view & attach it for display
            view.transform.AttachToParent(LayoutGroup.transform, 0);
            view.transform.localPosition = Vector3.zero;

            if(view.PooledObjectType == PooledObjectType.CardView)
                view.transform.localScale = Vector3.one * 2;

            //get store
            uint creditsPrice;
            uint gemsPrice;
            if (storeData.VirtualPrices != null 
                && (storeData.VirtualPrices.TryGetValue(CurrencyType.CR.ToString(), out creditsPrice) 
                | storeData.VirtualPrices.TryGetValue(CurrencyType.NG.ToString(), out gemsPrice)))
            {
                ItemId = storeData.ItemId;
                CRprice = creditsPrice;
                NGprice = gemsPrice;
                StoreId = storeData.StoreId;

                
                CRcostLabel.SetText("{0}<sprite=0>", CRprice);
                NGcostLabel.SetText("{0}<sprite=1>", NGprice);


                BuyCRButton.interactable = creditsPrice > 0;
                BuyCRButton.gameObject.SetActive(creditsPrice > 0);
                BuyNGButton.interactable = gemsPrice > 0;
                BuyNGButton.gameObject.SetActive(gemsPrice > 0);
                CloseButton.interactable = true;
                this.MessageLabel.text = string.Empty;
            }
            else
            {
                // if failed, display error message
                MessageLabel.text = I2.Loc.ScriptLocalization.Error.Item_Not_Found;
                BuyCRButton.interactable = false;
                BuyCRButton.gameObject.SetActive(false);
                BuyNGButton.interactable = false;
                BuyNGButton.gameObject.SetActive(false);
                CloseButton.interactable = true;
            }

            // get item

        }
        public void OnClickCR()
        {
            ClickBuy(CurrencyType.CR);
        }
        public void OnClickNG()
        {
            ClickBuy(CurrencyType.NG);
        }

        private void ClickBuy(CurrencyType currency)
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(true, currency, currency == CurrencyType.CR ? CRprice : NGprice);
            }
        }

        public void OnClickCancel()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(false, CurrencyType.xx, 0);
            }
        }
    }
}

namespace NovaBlitz.UI
{
    public class ConfirmVirtualPurchaseDialogMediator : Mediator
    {
        [Inject] public ConfirmVirtualPurchaseDialogView View { get; set; }
        [Inject] public VirtualPurchaseSignal VirtualPurchaseSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public VirtualPurchaseResponseSignal VirtualPurchaseResponseSignal { get; set; }
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
        [Inject] public ShowPurchaseOffersSignal ShowPurchaseOffersSignal { get; set; }
        [Inject] public INovaContext NovaContext {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public ViewReferences ViewReferences {get;set;}

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.DialogDismissed += OnDismissed;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.DialogDismissed -= OnDismissed;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.VirtualPurchaseResponseSignal.RemoveListener(OnVirtualPurchaseResponse);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
               
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                this.VirtualPurchaseResponseSignal.RemoveListener(OnVirtualPurchaseResponse);
            }
        }

        private void OnDismissed(bool isConfirmed, CurrencyType currency, uint price)
        {
            if (isConfirmed && price > 0)
            {
                int balance;
                this.PlayerProfile.Wallet.Currencies.TryGetValue(currency, out balance);
                if (currency == CurrencyType.CR && price > balance)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.CreditsOffer);
                }
                else if (currency == CurrencyType.NG && price > balance)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.GemsOffer);
                }
                else
                {
                    var request = new VirtualPurchaseRequest { Currency = currency, ItemId = View.ItemId, ExpectedPrice = (int)price, StoreId = View.StoreId };

                    this.View.MessageLabel.text = I2.Loc.ScriptLocalization.Purchase_Submitted;
                    this.View.BuyCRButton.interactable = false;
                    this.View.BuyNGButton.interactable = false;
                    this.View.CloseButton.interactable = false;
                    this.VirtualPurchaseResponseSignal.RemoveListener(OnVirtualPurchaseResponse);
                    this.VirtualPurchaseResponseSignal.AddListener(OnVirtualPurchaseResponse);
                    this.VirtualPurchaseSignal.Dispatch(request);
                }
            }
            else
            {
                this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
            }
        }

        private void OnVirtualPurchaseResponse(VirtualPurchaseResponse response)
        {
            this.VirtualPurchaseResponseSignal.RemoveListener(OnVirtualPurchaseResponse);
            Debug.LogFormat("OnVirtualPurchase: {0}", response.Error);
            if (response.Error == ErrorCode.NoError)
            {
                this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
            }
            else
            {
                this.View.MessageLabel.text = I2.Loc.ScriptLocalization.Error.Purchase_Failed;
            }
        }
    }
}
