using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using LinqTools;
using DragonFoundry.Fx;
using TMPro;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class DeckListView : AnimatedView
    {
        [Inject] public IDragOverlay DragOverlay { get; set; }
        public PlayerProfile PlayerProfile { get; set; }
        public GameData GameData { get; set; }
        public NovaAudioPlayer MusicTheme;
        public NovaAudioPlayer ItemClickAudio;
        public GameObject EmptySlotPrefab;
        public GridLayoutGroup ItemsGrid;
        public ToggleGroup ToggleGroup;
        public ScrollRect ScrollRect;
        public NovaButton AddDeckButton;
        public ParticleEffectsTrigger AddDeckButtonFx;
        public Action<DeckListItem> DeckSelected;
        public Action<DeckListItem> DeckDeselected;
        public Action DeckAdd;
        public Action DeckListClosed;
        public Action DeckDeleted;
        public Action SaveDecks;
        public Action<DeckData> DeckReordered;
        public int NumDecks { get; private set; }

        private bool isReordered = false;
        public bool IsDeckOrderNeedsSaving { get; set; }

        [HideInInspector]
        public DeckListItem DeckListItemPrefab;
        protected List<DeckListItem> _ListItems = new List<DeckListItem>();

        public bool IsTutorial { set { AddDeckButton.gameObject.SetActive(!value); } get { return !AddDeckButton.gameObject.activeSelf; }}

		public DeckData SelectedDeck { get { var item = _ListItems.FirstOrDefault(li => li.DeckButton.isOn); return null != item ? item.DeckData : null; } }

		protected override void Start()
		{
			base.Start();
			var deckListItemRX = this.DeckListItemPrefab.GetComponent<RectTransform>();
			this.ItemsGrid.cellSize = deckListItemRX.sizeDelta;
		}

		public void ToggleDeck(string deckID)
		{
			var deckItem = _ListItems.FirstOrDefault(item => null != item.DeckData && item.DeckData.ID == deckID);
			if (null != deckItem)
			{
				deckItem.DeckButton.isOn = true;
				deckItem.Background.gameObject.SetActive(false);
				//deckItem.SelectedOverlay.gameObject.SetActive(true);

			}
		}

		public DeckListItem GetDeckListItem(string deckId)
		{
			return _ListItems.FirstOrDefault(li => li.DeckData.ID == deckId);
		}

		public void OnDeleteButtonClicked()
		{
			if(this.DeckDeleted != null)
			{
				this.DeckDeleted.Invoke();
			}
		}

		bool AnimatorIsPlaying()
		{
			return this.Animator.GetCurrentAnimatorStateInfo(0).length > this.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
		}

		bool AnimatorIsPlaying(string stateName)
		{
     		return AnimatorIsPlaying() && this.Animator.GetCurrentAnimatorStateInfo(0).IsName(stateName);
  		}

		/// <summary>
		/// Event handler for the deckList button/Toggle UI elements
		/// </summary>
		public void OnDeckToggled(DeckListItem deckListItem, bool isActive)
		{
			if(deckListItem.Background.gameObject.activeSelf == isActive)
			{
				//Debug.LogFormat("IsOpen: {0} DeckListDefault isPlaying:{1}", this.IsOpen, this.Animator.GetCurrentAnimatorStateInfo(0).IsName("DeckListDefault"));
				if(this.IsOpen && this.AnimatorIsPlaying("DeckListDefault") || this.isInitializing)
				{
					deckListItem.ShowDeleteButton(isActive);
				}
				else
				{
					// Only play the delete button animation if the state is changing
					deckListItem.ShowDeleteButton(isActive, true);
					this.ItemClickAudio.Play();
				}
			}

			if (isActive) 
			{
				deckListItem.Background.gameObject.SetActive(false);
				//deckListItem.SelectedOverlay.gameObject.SetActive(true);
				if (this.DeckSelected != null)
				{
					this.DeckSelected.Invoke(deckListItem);
				}
			}
			else 
			{
				deckListItem.Background.gameObject.SetActive(true);
				//deckListItem.SelectedOverlay.gameObject.SetActive(false);
				this.ItemClickAudio.Play();
			}
		}

		//<summary>
		// When the ADD DECK button is clicked, this method handles it
		//</summmary>
		public void OnDeckAdded()
		{
			if(this.DeckAdd != null) { DeckAdd.Invoke(); }
		}

		//<summary>
		// Called from the Deck closed animation timeline, used for coordinating transitions.
		//</summary>
		public void OnDeckListClosed()
		{
            CheckAndSaveDecks();

            if (this.DeckListClosed != null)
			{
				this.DeckListClosed.Invoke();
			}
		}

		private bool isInitializing = false;
		/// <summary>
		/// Create the UI elements for the Deck List Data
		/// </summary>
		public void Initialize(string selectedDeckId = null)
		{
            List<DeckData> deckList;
            int numDeckSlots = 1;
            if (IsTutorial)
            {
                deckList = new List<DeckData> { this.PlayerProfile.CurrentDeck };
            }
            else
            {
                deckList = PlayerProfile.DeckList;
                numDeckSlots = this.PlayerProfile.NumDeckSlots;
            }

            this.isInitializing = true;
			Debug.Log("isInitializing DeckListView...");

			// If the player has no decks, give them the precons
			if(deckList.Count == 0)
			{
				foreach(var deck in GameData.PreconstructedDecks)
				{
					deckList.Add(deck);
				}
			}
			
			this.NumDecks = deckList.Count;
			this.isReordered = false;

			// Create all the items you'd need
			if(numDeckSlots + 1 > this.ItemsGrid.transform.childCount)
			{
				int listItemsToCreate = (numDeckSlots + 1) - this.ItemsGrid.transform.childCount;
				for(int i=0;i<listItemsToCreate; i++)
				{
                    DeckListItem deckListItem = this.DeckListItemPrefab.Spawn();
					_ListItems.Add(deckListItem);
					deckListItem.transform.SetParent(this.ItemsGrid.transform, false);
                    deckListItem.transform.localScale = Vector3.one;
				}
			}

			DeckListItem selectedItem = null;
			int selectedItemSlot = 0;

			// Initialize the decklist items
			for(int i=0; i<deckList.Count && i<_ListItems.Count; i++)
			{
                if (deckList[i] == null)
                    Debug.LogErrorFormat("Decklist {0} is null. {1} decks total", i, NumDecks);
				DeckListItem deckListItem = _ListItems[i];
				deckListItem.DragOverlay = this.DragOverlay;
				deckListItem.BoundsRect = (RectTransform)this.transform;
                deckListItem.IsTutorial = this.IsTutorial;
				deckListItem.InitializeDeckButton(deckList[i], this);
				deckListItem.gameObject.name = string.Format("{0}. {1}", i+1, deckList[i].Name);
				deckListItem.gameObject.SetActive(true);
				deckListItem.transform.SetSiblingIndex(i);

				if(deckList[i].ID == selectedDeckId)
				{
                    // Make the selected item look selected (Toggled)
                    deckListItem.DeckButton.isOn = true;
                    deckListItem.ShowDeleteButton(true);
                    /*if (this.IsOpen)
					{
						deckListItem.DeckButton.isOn = true;
						//deckListItem.SelectedOverlay.gameObject.SetActive(true);
					}*/
					selectedItem = deckListItem;
					selectedItemSlot = i;
				}
				else
				{ 
					// Reset the selected state of all deck listItems that aren't selected
					if (deckListItem.DeckButton.isOn)
					{
						deckListItem.DeckButton.isOn = false;
						//deckListItem.SelectedOverlay.gameObject.SetActive(false);
					}
				}

				//deckListItem.Overlay.gameObject.SetActive(deckListItem.SelectedOverlay.gameObject.activeSelf == false);
				
			}

			int addButtonListIndex = deckList.Count;

			// Add any empty slots
			int numEmptySlots = numDeckSlots - deckList.Count;
			for(int i=1;i<=numEmptySlots; i++)
			{
				DeckListItem emptySlotItem = _ListItems[addButtonListIndex + i -1];
				emptySlotItem.InitializeEmptySlot();
				emptySlotItem.gameObject.SetActive(true);
			}

			// Hide any extra items
			for(int i=numDeckSlots; i < _ListItems.Count; i++)
			{
				_ListItems[i].gameObject.SetActive(false);
			}
			
			this.ItemsGrid.enabled = false;
			this.ItemsGrid.enabled = true;
			
			if(selectedItem != null)
			{
				Timer.Instance.StartCoroutine(PositionList(selectedItem, selectedItemSlot));
			}
			this.isInitializing = false;
		}
		
		//<summary>
		// CoRoutine that positions the decklist on the selected deck 2 frames after it's been initialized
		//</summary>
		IEnumerator PositionList(DeckListItem selectedItem, int selectedItemSlot)
		{
			yield return null;
			yield return null;
            yield return null;
            if (this.ItemsGrid == null)
                yield break;
            var gridObject = this.ItemsGrid.GetComponent<GridLayoutGroup>();
            if (gridObject == null)
                yield break;
            float itemYOffset = (selectedItemSlot -3) * gridObject.cellSize.y;
			if (itemYOffset < 0)
				itemYOffset = 0;

			//((RectTransform)this.ScrollRect.content.transform).anchoredPosition = new Vector3(0f,itemYOffset );
			float scrollRectHeight = ((RectTransform)this.ScrollRect.transform).rect.height;
			Debug.LogFormat("itemYOffset {0} ScrollRectHeight:{1}", selectedItem.RectTransform.anchoredPosition.y, scrollRectHeight);
			
			if( -selectedItem.RectTransform.anchoredPosition.y > scrollRectHeight)
			{
				((RectTransform)this.ScrollRect.content.transform).anchoredPosition = new Vector3(-143f,itemYOffset );
				//int pages = (int)(itemYOffset / scrollRectHeight);
				//((RectTransform)this.ScrollRect.content.transform).anchoredPosition = new Vector3(0f,pages * scrollRectHeight - scrollRectHeight );
			}
		}
		
		public void PositionDraggedItem(DeckListItem listItem, DeckListItem draggedItem )
		{
			RectTransform gridTransform = (RectTransform)this.ItemsGrid.transform;
			
			float listHeight = gridTransform.rect.height;
			float itemHeight = listItem.RectTransform.rect.height;
			
			Vector3 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, draggedItem.transform.position);
			Vector2 localPoint = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(gridTransform, screenPoint, Camera.main, out localPoint);
			
			int index = (int)Mathf.Floor(-localPoint.y / itemHeight);
			index = Mathf.Clamp(index, 0, this.NumDecks-1);
			
			if(index != listItem.transform.GetSiblingIndex())
			{
				listItem.transform.SetSiblingIndex(index);
				this.ItemsGrid.enabled = false;
				this.ItemsGrid.enabled = true;
				this.isReordered = true;
			}
			
		}
		
		public void StartDragging()
		{
			this.isReordered = false;
		}
		
		public void EndDragging(DeckData data)
		{
			if(this.isReordered)
			{
				if(this.DeckReordered != null)
				{
					this.DeckReordered.Invoke(data);
				}
			}
		}

		//<summary>
		// Loops though all the Deck elements in the UI and retrieves a List of their ID's
		// We loop though them because it retrieves them in the new order they appear in the UI.
		//</summary> 
		public List<string> GetDeckIDs()
		{
			List<string> orderedDeckIDs = new List<string>();
			
			for(int i=0;i<this.NumDecks;i++)
			{
				DeckListItem listItem = this.ItemsGrid.transform.GetChild(i).GetComponent<DeckListItem>();
				//Debug.LogFormat("{0}. {1}", i, listItem.DeckData.Name);
				orderedDeckIDs.Add(listItem.DeckData.ID);
				
			}
			
			return orderedDeckIDs;
		}

        protected void OnApplicationPause(bool pause)
        {
            CheckAndSaveDecks();
        }
        protected void OnApplicationQuit()
        {
            CheckAndSaveDecks();
        }

        protected override void OnDestroy()
        {
            CheckAndSaveDecks();
            base.OnDestroy();
            foreach(var item in _ListItems)
            {
                item.Recycle();
            }
            _ListItems.Clear();
        }

        private void CheckAndSaveDecks()
        {
            if(IsDeckOrderNeedsSaving && SaveDecks != null)
            {
                IsDeckOrderNeedsSaving = false;
                SaveDecks.Invoke();
            }
        }
    }
}
