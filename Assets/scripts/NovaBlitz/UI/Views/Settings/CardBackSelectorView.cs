using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using NovaBlitz.Economy;
using System.Text;
using PlayFab.ClientModels;
using Messages;

namespace NovaBlitz.UI
{
	public class CardBackSelectorView : AnimatedView
    {
        [Inject] public UIConfiguration UIConfiguration { get; set; }

        public ToggleGroup ToggleGroup;
        public MultiCardbackView MultiCardBack;
		public string SelectedID {get;set; }
        public string BuyingId { get; set; }

        public event Action<string> EventConfirm;

        [Inject]
        public BeginVirtualPurchaseSignal BeginVirtualPurchaseSignal { get; set; }

        public void Select(string id)
		{
			this.SelectedID = id;
			foreach (var view in this.MultiCardBack.Instances) {
				if (id == view.CardBackID) {
					var sel = view.GetComponent<Selectable>();
					if (null != sel)
						sel.Select();
					var tog = view.GetComponentInChildren<Toggle>();
					if (null != tog)
						tog.isOn = true;
					break;
				}
			}
		}
		
		public void ClickCardBack(CardBackView view)
		{
            BuyingId = null;
            if (view.IsOwned == false)
            {
                if (view._BuyButton.IsActive() && view.StoreData != null)
                {
                    this.BuyingId = view.CardBackID;
                    CardBackView newView = UIConfiguration.CardBackViewPrefab.Spawn();// View.Instantiate(view, Vector3.zero, Quaternion.identity) as CardBackView;
                    newView.UpdateCardBackView(view.CardBackData);
                    newView._BuyButton.gameObject.SetActive(false);
                    newView._NotOwnedScrim.gameObject.SetActive(false);
                    BeginVirtualPurchaseSignal.Dispatch(view.StoreData, newView);
                    // open the purchase with virtual currency window
                }
                // Otherwise do nothing, as you don't own it, and you can't buy it.
            }
            else
            {
                this.SelectedID = view.CardBackID;
                view._Toggle.isOn = true;
            }

        }

        public void Confirm()
		{
			if (null != EventConfirm)
				EventConfirm.Invoke(this.SelectedID);
		}

        protected override void OnDestroy()
        {
            base.OnDestroy();
            MultiCardBack.Purge();
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class CardBackSelectorMediator : Mediator
	{
		[Inject] public CardBackSelectorView View {get;set;}
		
        [Inject] public GameData GameData { get; set; }

		[Inject] public ResourceMap ResourceMap {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public SelectCardbackSignal SelectCardbackSignal { get;set;}
        [Inject] public CatalogLoadedSignal CatalogLoadedSignal { get; set; }
        [Inject] public InventoryLoadedSignal InventoryLoadedSignal { get; set; }
		[Inject] public PostPurchaseSignal PostPurchaseSignal {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
		
		public override void OnRegister()
        {
            InitializeCardBackData();
            
			this.View.EventConfirm += OnConfirm;
            this.InventoryLoadedSignal.AddListener(InitializeCardBackData);
            this.CatalogLoadedSignal.AddListener(InitializeDueToCatalog);
            this.PostPurchaseSignal.AddListener(InitializeDueToPurchase);
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.ProfileUpdatedSignal.AddListener(InitializeDueToProfileUpdate);
        }
		
		public override void OnRemove()
		{
			this.View.EventConfirm -= OnConfirm;
            this.InventoryLoadedSignal.RemoveListener(InitializeCardBackData);
            this.CatalogLoadedSignal.RemoveListener(InitializeDueToCatalog);
            this.PostPurchaseSignal.RemoveListener(InitializeDueToPurchase);
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.ProfileUpdatedSignal.RemoveListener(InitializeDueToProfileUpdate);
        }
		
		protected void OnConfirm(string id)
		{
			this.SelectCardbackSignal.Dispatch(id);
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
		}


        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
            }
        }



        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
            }
        }

        private void InitializeDueToPurchase(ItemInstance[] instances)
        {
            var cardBacks = instances.Where(i => i.ItemClass == NBEconomy.ITEMCLASS_CARDBACK).ToList();
            if (cardBacks.Count() > 0)
            {
                foreach (var cardBack in cardBacks)
                {
                    PlayerProfile.OwnedCardBacks.Add(cardBack.ItemId);
                }
                InitializeCardBackData();
                View.Select(cardBacks[0].ItemId);
            }
        }

        private void OnLanguageChanged()
        {
            InitializeCardBackData();
        }

        private void InitializeDueToCatalog(string version)
        {
            InitializeCardBackData();
        }

        private void InitializeDueToProfileUpdate(ProfileSection section)
        {
            if (section == ProfileSection.Inventory && View.BuyingId != null)
            {
                InitializeCardBackData();
                View.Select(View.BuyingId);
                View.BuyingId = null;
            }
        }

        private void InitializeCardBackData()
        {
            // build all the data
            List<CardBackData> cardBackDataList = new List<CardBackData>();
            foreach (var kvp in GameData.CardBackDictionary)
            {
                StringBuilder builder = new StringBuilder(NBEconomy.CATALOG_CARDBACK_PREFIX);
                builder.Append(kvp.Value.Id);
                string itemId = builder.ToString();
                bool isOwned = PlayerProfile.OwnedCardBacks.Contains(itemId);

                var data = new CardBackData() { cardBackArtId = kvp.Value.ArtId, cardBackId = (int)kvp.Value.Id, cardBackItemId = itemId, cardBackSelectorView = this.View };
                NovaCatalogItem item;
                if (GameData.ProductCache.TryGetValue(itemId, out item))
                {
                    data.cardBackItem = item;
                    data.isOwned = isOwned;
                }
                if (kvp.Value.IsFree)
                    data.isOwned = true;

                StoreData storeData = null;
                Dictionary<string, StoreData> storeDict;
                if (GameData.StoreCache.TryGetValue(NBEconomy.STORE_CARDBACK, out storeDict) && storeDict.TryGetValue(itemId, out storeData))
                    data.storeItem = storeData;

                if (data.isOwned == true || storeData != null)
                    cardBackDataList.Add(data);
            }
            
            this.View.MultiCardBack.Populate(cardBackDataList);
            this.View.Select(this.PlayerProfile.MetaProfile.CardBackID);
        }
    }
}
