﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections.Generic;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	public class AudioSettingsView : View
	{
		public event Action EventSave;
		public AudioSettingsWidget AudioSettings;

		public void Save() { if (null != this.EventSave) this.EventSave.Invoke(); }

		#region uGUI
		public void OnSFXVolumeSlider(float level) { this.AudioSettings.OnSFXVolumeSlider(level); }
		public void OnMusicVolumeSlider(float level) { this.AudioSettings.OnMusicVolumeSlider(level); }
		#endregion
	}
}

namespace NovaBlitz.UI.Mediators
{
	public class AudioSettingsMediator : Mediator
	{
		[Inject] public AudioSettingsView View {get;set;}

		[Inject] public NovaAudio NovaAudio {get;set;}
		[Inject] public SaveSettingsSignal SaveSettingsSignal {get;set;}
		[Inject] public LoadSettingsSignal LoadSettingsSignal {get;set;}
		[Inject] public SettingsLoadedSignal SettingsLoadedSignal {get;set;}

		public override void OnRegister()
		{
			this.View.EventSave += OnViewSave;
			this.View.AudioSettings.EventSFXVolumeChanged += OnSFXVolumeChanged;
			this.View.AudioSettings.EventMusicVolumeChanged += OnMusicVolumeChanged;
			this.SettingsLoadedSignal.AddListener(OnSettingsLoaded);
			OnSettingsLoaded();
		}

		public override void OnRemove()
		{
			this.View.EventSave -= OnViewSave;
			this.View.AudioSettings.EventSFXVolumeChanged -= OnSFXVolumeChanged;
			this.View.AudioSettings.EventMusicVolumeChanged -= OnMusicVolumeChanged;
			this.SettingsLoadedSignal.RemoveListener(OnSettingsLoaded);
		}

		#region View Listeners
		protected void OnSFXVolumeChanged(int volume)
		{
			this.NovaAudio.SoundFxVolume = volume;
		}

		protected void OnMusicVolumeChanged(int volume)
		{
			this.NovaAudio.MusicVolume = volume;
		}

		protected void OnViewSave()
		{
			this.SaveSettingsSignal.Dispatch();
		}
		#endregion

		#region Signal Listeners
		protected void OnSettingsLoaded()
		{
			this.View.AudioSettings.SFXVolume = this.NovaAudio.SoundFxVolume;
			this.View.AudioSettings.MusicVolume = this.NovaAudio.MusicVolume;
		}
		#endregion
	}
}
