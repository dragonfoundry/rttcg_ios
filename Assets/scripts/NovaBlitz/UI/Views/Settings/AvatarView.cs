using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using System.Text;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
	public class AvatarView : PoolableMonoBehaviour
    {
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.AvatarView; } }
        [SerializeField]
		protected TextMeshProUGUI _NameDisplay;
        
        public CardArtImage AvatarArt;
        
        public Toggle _Toggle;

        [SerializeField]
        public Image _NotOwnedScrim;

        public Button AvatarButton;
        public Button _BuyButton;
        [SerializeField]
        protected TextMeshProUGUI _BuyPrice;

        public bool? IsOwned;
        private string AvatarArtId;
        public StoreData StoreData;
        
		public string AvatarID { get; set; }
        public AvatarData AvatarData { get; set; }

        public string PlayerName {
			get { return null != _NameDisplay ? _NameDisplay.text : null; }
			set { if (null != _NameDisplay) _NameDisplay.text = value; }
		}

        public void UpdateAvatarView(AvatarData data)
        {
            AvatarData = data;
            IsOwned = data.isOwned;
            AvatarArtId = data.avatarArtId;
            AvatarID = data.avatarItemId;


            // Update the art
            AvatarArt.UpdateCardArt(AvatarArtId, CardAspect.NoAspect);

            // If there's an avatar Item,

            // If we can own it (not true for free avatars, and we do own it, we can select it; if not, we can't, so show the padlock.
            if (data.isOwned == false)
            {
                _NotOwnedScrim.gameObject.SetActive(true);

                // get buy price, if any
                if (data.storeItem != null && data.storeItem.VirtualPrices != null)
                {
                    StoreData = data.storeItem;

                    uint rmPrice;
                    uint crPrice;
                    if (data.storeItem.VirtualPrices.TryGetValue(CurrencyType.RM.ToString(), out rmPrice))
                    {
                        StringBuilder builder = new StringBuilder();
                        builder.Append('$');
                        builder.Append(rmPrice);
                        _BuyPrice.text = builder.ToString();
                        _BuyPrice.color = Color.green;
                        _BuyButton.gameObject.SetActive(true);
                    }
                    else if (data.storeItem.VirtualPrices.TryGetValue(CurrencyType.CR.ToString(), out crPrice))
                    {
                        _BuyPrice.text = crPrice.ToString();
                        _BuyButton.gameObject.SetActive(true);
                    }
                    
                    // get alt text???
                }
                // Apply buy price if any.
            }
            else // we don't care whether we own it, or this is null (which means it's free)
            {
                _BuyButton.gameObject.SetActive(false);
                _NotOwnedScrim.gameObject.SetActive(false);
            }
        }

        public override void OnRecycle()
        {
            AvatarData = null;
            transform.localScale = Vector3.one;
            _Toggle.group = null;
            StoreData = null;
            AvatarArt.ResetArt();
            _BuyButton.gameObject.SetActive(false);
        }

        protected void OnDestroy()
        {
            AvatarArt.ResetArt();
        }
    }

    public class AvatarData
    {
        public int avatarId { get; set; }
        public string avatarArtId { get; set; }
        public NovaCatalogItem avatarItem { get; set; }
        public bool? isOwned { get; set; }
        public string avatarItemId { get; set; }
        public StoreData storeItem { get; set; }
        public AvatarSelectorView AvatarSelectorView { get; set; }
    }

	[System.Serializable]
	public class MultiAvatarView : MultiRect<AvatarView, AvatarData>
	{
		protected override void Initialize(AvatarView instance, AvatarData data)
		{
            instance.UpdateAvatarView(data);
            instance._BuyButton.onClick.AddListener(() => { data.AvatarSelectorView.ClickAvatar(instance); });
            instance.AvatarButton.onClick.AddListener(() => { data.AvatarSelectorView.ClickAvatar(instance); });
            //var toggleGroup = data.AvatarSelectorView.GetComponentInChildren(typeof(ToggleGroup)) as ToggleGroup;
            instance._Toggle.group = data.AvatarSelectorView.ToggleGroup;
            //toggleGroup.RegisterToggle(instance._Toggle);
        }
	}
}
