using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Just a wrapper to keep an instance synced with local profile.
	/// In the future this could be refactored to sync with any player's profile.
	/// </summary>
	public class LocalAvatarView : View
	{
		public AvatarView AvatarDisplay;
	}
}

namespace NovaBlitz.UI.Mediators
{
	public class LocalAvatarMediator : Mediator
	{
		[Inject] public LocalAvatarView View {get;set;}
		[Inject] public GameData GameData { get;set;}

		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		
		public override void OnRegister()
		{
			RefreshView();
			this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
		}

		public override void OnRemove()
		{
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
		}

		protected void RefreshView()
		{
			this.View.AvatarDisplay.PlayerName = this.PlayerProfile.MetaProfile.Name;
            AvatarDataContract avatar;
            if (this.GameData.AvatarByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.AvatarID, out avatar) && !string.IsNullOrEmpty(avatar.ArtId))
            {
                this.View.AvatarDisplay.AvatarArt.UpdateCardArt(avatar.ArtId, CardAspect.NoAspect);
            }
		}

		protected void OnProfileUpdated(ProfileSection section)
		{
			if (section != ProfileSection.Inventory)
				return;
			RefreshView();
		}
	}
}
