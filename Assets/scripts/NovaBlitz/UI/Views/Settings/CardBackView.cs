using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using System.Text;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
	public class CardBackView : PoolableMonoBehaviour
	{
		[Inject] public ResourceMap ResourceMap {get;set;}

        public override PooledObjectType PooledObjectType { get { return PooledObjectType.CardBackView; } }

        [SerializeField]
        public RawImage _CardBackDisplay;
        
        public Toggle _Toggle;

        [SerializeField]
        public Image _NotOwnedScrim;

        public Button CardBackButton;
        public Button _BuyButton;
        [SerializeField]
        protected TextMeshProUGUI _BuyPrice;

        public bool? IsOwned;
        private int _CardBackId;
        public StoreData StoreData;

        public string CardBackID { get; set; }
        public CardBackData CardBackData { get; set; }        		

        public void SetCardBackArt(string artId)
        {
            Texture tex = AssetBundleManager.Instance.LoadAsset<Texture>(artId);
            //Texture tex = (Texture)Resources.Load(artId);
            if (tex == null)
            {
                tex = AssetBundleManager.Instance.LoadAsset<Texture>("BasicCardBack");
                //tex = (Texture)Resources.Load("BasicCardBack");
            }
            _CardBackDisplay.texture = tex;
        }

        public void UpdateCardBackView(CardBackData data)
        {
            CardBackData = data;
            IsOwned = data.isOwned;
            CardBackID = data.cardBackItemId;

            SetCardBackArt(data.cardBackArtId);
            // If there's an card back Item,

            // If we can own it (not true for free card backs, and we do own it, we can select it; if not, we can't, so show the padlock.
            if (data.isOwned == false)
            {
                _NotOwnedScrim.gameObject.SetActive(true);

                // get buy price, if any
                if (data.storeItem != null && data.storeItem.VirtualPrices != null)
                {
                    StoreData = data.storeItem;

                    uint rmPrice;
                    uint crPrice;
                    if (data.storeItem.VirtualPrices.TryGetValue(CurrencyType.RM.ToString(), out rmPrice))
                    {
                        StringBuilder builder = new StringBuilder();
                        builder.Append('$');
                        builder.Append(rmPrice);
                        _BuyPrice.text = builder.ToString();
                        _BuyPrice.color = Color.green;
                        _BuyButton.gameObject.SetActive(true);
                    }
                    else if (data.storeItem.VirtualPrices.TryGetValue(CurrencyType.CR.ToString(), out crPrice))
                    {
                        _BuyPrice.text = crPrice.ToString();
                        _BuyButton.gameObject.SetActive(true);
                    }

                    // get alt text???
                }
                // Apply buy price if any.
            }
            else // we don't care whether we own it, or this is null (which means it's free)
            {
                _BuyButton.gameObject.SetActive(false);
                _NotOwnedScrim.gameObject.SetActive(false);
            }
        }
        
        public override void OnRecycle()
        {
            _BuyButton.gameObject.SetActive(false);
            CardBackData = null;
            transform.localScale = Vector3.one;
            _Toggle.group = null;
            StoreData = null;
            _CardBackDisplay.texture = null;
        }

        protected void OnDestroy()
        {
            _CardBackDisplay.texture = null;
        }
    }

    public class CardBackData
    {
        public int cardBackId { get; set; }
        public string cardBackArtId { get; set; }
        public NovaCatalogItem cardBackItem { get; set; }
        public bool? isOwned { get; set; }
        public string cardBackItemId { get; set; }
        public StoreData storeItem { get; set; }
        public CardBackSelectorView cardBackSelectorView { get; set; }
    }

    [System.Serializable]
	public class MultiCardbackView : MultiRect<CardBackView, CardBackData>
	{
		protected override void Initialize(CardBackView instance, CardBackData data)
        {
            instance.UpdateCardBackView(data);
            instance._BuyButton.onClick.AddListener(() => { data.cardBackSelectorView.ClickCardBack(instance); });
            instance.CardBackButton.onClick.AddListener(() => { data.cardBackSelectorView.ClickCardBack(instance); });
            instance._Toggle.group = data.cardBackSelectorView.ToggleGroup;
        }
    }
}