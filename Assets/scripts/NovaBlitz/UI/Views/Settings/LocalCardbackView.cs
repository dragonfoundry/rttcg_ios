using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Just a wrapper to keep an instance synced with local profile.
	/// In the future this could be refactored to sync with any player's profile.
	/// </summary>
	public class LocalCardbackView : View
	{
		public CardBackView CardbackDisplay;
	}
}

namespace NovaBlitz.UI.Mediators
{
	public class LocalCardbackMediator : Mediator
	{
		[Inject] public LocalCardbackView View {get;set;}
		[Inject] public GameData GameData { get;set;}


		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		
		public override void OnRegister()
		{
			RefreshView();
			this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
		}
		
		public override void OnRemove()
		{
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
		}
		
		protected void RefreshView()
		{
			this.View.CardbackDisplay.CardBackID = this.PlayerProfile.MetaProfile.CardBackID;

            CardBackDataContract contract;
            if (this.GameData.CardBackByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.CardBackID, out contract) && !string.IsNullOrEmpty(contract.ArtId))
            {
                this.View.CardbackDisplay.SetCardBackArt(contract.ArtId);
            }
        }
		
		protected void OnProfileUpdated(ProfileSection section)
		{
			if (section != ProfileSection.Inventory)
				return;
			RefreshView();
		}
	}
}
