﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
    public enum ArenaLayout
    {
        NoLayout = 0,
        Mobile = 1,
        Desktop = 2
    }
    public class SettingsView : AnimatedView
    {
        public event Action EventHelpClicked;
        public event Action EventFullScreenClicked;
        public event Action EventLogoutClicked;
        public event Action EventExitClicked;
        public event Action EventResetPasswordClicked;
        public event Action EventSelectLanguageClicked;
        public event Action EventReportBugClicked;
        public event Action EventCloseClicked;
        public event Action EventProtectAccountClicked;
        public Action<ArenaLayout> EventArenaLayoutToggled;

        public AudioSettingsView AudioSettingsView;
        
        [SerializeField] public GameObject _FullScreenNode;
        [SerializeField] public GameObject _ArenaLayoutNode;
        [SerializeField] public Toggle _AutoToggle;
        [SerializeField] public Toggle _MobileToggle;
        [SerializeField] public Toggle _DesktopToggle;
      
        [SerializeField] protected TextMeshProUGUI LanguageName;

        public NovaButton ResetPassword;
        public NovaButton LogOut;
        public NovaButton Exit;
        public NovaButton ProtectAccount;
        public TextMeshProUGUI PlayFabId;
        
        public Image FullScreenImage;
        public Sprite FullScreenOff;
        public Sprite FullScreenOn;

        public GameObject _AccountSettings;

        protected override void Update()
        {
            base.Update();
            if(Screen.fullScreen)
            {
                FullScreenImage.sprite = FullScreenOn;

            }
            else
            {
                FullScreenImage.sprite = FullScreenOff;
            }
        }


        public void OnScrimClicked()
        {
            if (this.EventCloseClicked != null)
            {
                this.EventCloseClicked.Invoke();
            }
        }

        public void OnIsMobileArenaToggled(bool isOn)
        {
            Debug.LogFormat("IsMobileArena:{0}", isOn);
            if (this.EventArenaLayoutToggled != null)
            {
                if(isOn == true)
                {
                    this.EventArenaLayoutToggled.Invoke(ArenaLayout.Mobile);
                }
            }
        }

        public void OnIsDesktopArenaToggled(bool isOn)
        {
            Debug.LogFormat("IsDesktopArena:{0}", isOn);
            if(this.EventArenaLayoutToggled != null)
            {
                if(isOn == true)
                {
                    this.EventArenaLayoutToggled.Invoke(ArenaLayout.Desktop);
                }
            }
        }

        public void OnIsAutoArenaToggled(bool isOn)
        {
            Debug.LogFormat("IsAutoArena:{0}", isOn);
            if(this.EventArenaLayoutToggled != null)
            {
                if(isOn == true)
                {
                    this.EventArenaLayoutToggled.Invoke(ArenaLayout.NoLayout);
                }
            }
        }

        public void Initialize(ClientPrefs clientPrefs, PlayerProfile playerProfile)
        {
            bool isAccountProtected = playerProfile.MetaProfile.IsNameSet;
            //_AccountSettings.gameObject.SetActive(playerProfile.isAgeGatePassed == true);
            ResetPassword.gameObject.SetActive(isAccountProtected && playerProfile.isAgeGatePassed == true);
            //LogOut.gameObject.SetActive(isAccountProtected);
            //ProtectAccount.gameObject.SetActive(!isAccountProtected);
            PlayFabId.text = string.Format("{0}: {1}", I2.Loc.ScriptLocalization.Account, playerProfile.MetaProfile.PlayFabID);

            Exit.gameObject.SetActive(Application.platform != RuntimePlatform.IPhonePlayer);

            switch(clientPrefs.ArenaLayout)
            {
                case ArenaLayout.NoLayout:
                    this._AutoToggle.isOn = true;
                break;
                case ArenaLayout.Mobile:
                    this._MobileToggle.isOn = true;
                break;
                case ArenaLayout.Desktop:
                    this._DesktopToggle.isOn = true;
                break;
            }
            var language = I2.Loc.ScriptLocalization.Get(I2.Loc.LocalizationManager.CurrentLanguage);
            if (string.IsNullOrEmpty(language))
            {
                language = I2.Loc.ScriptLocalization.Language;
            }
            LanguageName.SetText(language);
            
#if UNITY_IOS || UNITY_ANDROID
			    this._FullScreenNode.SetActive(false);
                this._ArenaLayoutNode.SetActive(false);
#else
#endif
        }

        #region uGUI
        public void ClickHelp() { if (null != this.EventHelpClicked) this.EventHelpClicked.Invoke(); }
        public void ClickFullScreen() { if (null != this.EventFullScreenClicked) this.EventFullScreenClicked.Invoke(); }
        public void ClickLogout() { if (null != this.EventLogoutClicked) this.EventLogoutClicked.Invoke(); }
        public void ClickExit() { if (null != this.EventExitClicked) this.EventExitClicked.Invoke(); }
        public void ClickResetPassword() { if (null != this.EventResetPasswordClicked) this.EventResetPasswordClicked.Invoke(); }
        public void ClickSelectLanguage() { if (null != this.EventSelectLanguageClicked) this.EventSelectLanguageClicked.Invoke(); }
        public void ClickReportBug() { if (null != this.EventReportBugClicked) this.EventReportBugClicked.Invoke(); }
        public void ClickProtectAccount() { if (null != this.EventProtectAccountClicked) this.EventProtectAccountClicked.Invoke(); }
        #endregion
    }
}

namespace NovaBlitz.UI.Mediators
{
    public class SettingsMediator : Mediator
    {
        [Inject]
        public SettingsView View { get; set; }

        [Inject]
        public ClientPrefs ClientPrefs { get; set; }
        [Inject]
        public GameData GameData { get; set; }
        [Inject]
        public PlayerProfile PlayerProfile { get; set; }
        [Inject]
        public LogoutUserSignal LogoutUserSignal { get; set; }
        [Inject]
        public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal { get; set; }
        [Inject]
        public AccountRecoverySignal AccountRecoverySignal { get; set; }
        [Inject]
        public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject]
        public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }
        [Inject]
        public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject]
        public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject]
        public ViewReferences ViewReferences { get; set; }

        [Inject]
        public ShowLeaderboardSignal ShowLeaderboardSignal { get; set; }
        [Inject]
        public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
        [Inject]
        public INovaContext NovaContext { get; set; }
        [Inject]
        public NewConfirmDialogDismissedSignal NewConfirmDialogDismissedSignal { get; set; }
        [Inject]
        public LanguageChangedSignal LanguageChangedSignal { get; set; }
        [Inject]
        public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject]
        public UIConfiguration UIConfiguration { get; set; }
        [Inject]
        public ShareScreenShotSignal ShareScreenShotSignal { get; set; }


        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.EventHelpClicked += OnHelpClicked;
            this.View.EventFullScreenClicked += OnFullScreenClicked;
            this.View.EventLogoutClicked += OnLogoutClicked;
            this.View.EventExitClicked += OnExitClicked;
            this.View.EventResetPasswordClicked += OnResetPasswordClicked;
            this.View.EventArenaLayoutToggled += OnArenaLayoutToggled;
            this.View.EventSelectLanguageClicked += OnSelectLanguageClicked;
            this.View.EventCloseClicked += OnCloseClicked;
            this.View.EventReportBugClicked += OnReportBugClicked;
            this.View.EventProtectAccountClicked += OnProtectAccountClicked;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.ProfileUpdatedSignal.AddListener(this.OnProfileUpdated);

            //this.View.Initialize(this.ClientPrefs, this.PlayerProfile);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.EventHelpClicked -= OnHelpClicked;
            this.View.EventFullScreenClicked -= OnFullScreenClicked;
            this.View.EventLogoutClicked -= OnLogoutClicked;
            this.View.EventResetPasswordClicked -= OnResetPasswordClicked;
            this.View.EventArenaLayoutToggled -= OnArenaLayoutToggled;
            this.View.EventSelectLanguageClicked -= OnSelectLanguageClicked;
            this.View.EventCloseClicked -= OnCloseClicked;
            this.View.EventReportBugClicked -= OnReportBugClicked;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.ProfileUpdatedSignal.RemoveListener(this.OnProfileUpdated);
        }

        protected void OnSelectLanguageClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LanguageSelectorView));
        }

        protected void OnReportBugClicked()
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(ReportBugView),
                    (v) => { ((ReportBugView)v).Initialize(ReportType.Feedback, null, null); });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ReportBugView));
        }

        protected void OnArenaLayoutToggled(ArenaLayout layout)
        {
            this.ClientPrefs.ArenaLayout = layout;
            this.ClientPrefs.Save();
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
                this.View.Initialize(this.ClientPrefs, this.PlayerProfile);
                this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
            }
        }
        
        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
                this.View.AudioSettingsView.Save();
            }
        }

        private void OnCloseClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(SettingsView));
        }

        private void OnLanguageChanged()
        {
            this.View.Initialize(this.ClientPrefs, this.PlayerProfile);
        }

        private void OnProfileUpdated(ProfileSection section)
        {
            if (this.View.IsOpen && section == ProfileSection.Inventory)
            {
                this.View.Initialize(this.ClientPrefs, this.PlayerProfile);
            }
        }

        #region View Listeners

        protected void OnHelpClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(MobileTermsView));
        }

        protected void OnFullScreenClicked()
        {
            if (!Screen.fullScreen)
            {
#if UNITY_STANDALONE_LINUX
				// This is a hack to handle some dual monitors on Linux (because Linux reports the full screen area, not the area of a single monitor)
				if (Screen.currentResolution.width/2 > Screen.currentResolution.height)
					Screen.SetResolution(Screen.currentResolution.width/2, Screen.currentResolution.height, true, 60);
				else
					Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true, 60);
#else
                Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true, 60);
#endif
            }
            else
            {
                if (null != _CR_ResetCanvases)
                    StopCoroutine(this._CR_ResetCanvases);

                Screen.fullScreen = false;
                _CR_ResetCanvases = ResetCanvases();
                StartCoroutine(_CR_ResetCanvases);
            }
        }

        protected void OnProtectAccountClicked()
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView), (v) => {
                ((LoginDialogNewFlowView)v).Initialize(new LoginDialogSettings
                {
                    IsAddUsernameAndPassword = true,
                    IsAgeGatePassed = this.PlayerProfile.isAgeGatePassed,
                    IsCancelable = true,
                    SteamName = this.ClientPrefs.SteamName,
                });
            });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));
        }

        protected void OnLogoutClicked()
        {
            this.NewConfirmDialogDismissedSignal.AddOnce(OnConfirmDialogDismissed);
            this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(I2.Loc.ScriptLocalization.Log_Out, I2.Loc.ScriptLocalization.Confirm_logout, OnLogoutDialog, false));
        }

        protected void OnExitClicked()
        {
            this.NewConfirmDialogDismissedSignal.AddOnce(OnConfirmDialogDismissed);
            this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(I2.Loc.ScriptLocalization.Exit, I2.Loc.ScriptLocalization.Confirm_exit, OnExitDialog, false));
        }


        protected void OnConfirmDialogDismissed(bool isIgnored)
        {

        }

        protected void OnResetPasswordClicked()
        {
            if (this.ClientPrefs.UserAccountInfo != null & this.ClientPrefs.UserAccountInfo.PrivateInfo.Email != null)
                this.AccountRecoverySignal.Dispatch(this.ClientPrefs.UserAccountInfo.PrivateInfo.Email);
            else
                this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ForgotPasswordView));
        }
        
        #endregion

        #region Async
        protected IEnumerator _CR_ResetCanvases;
        protected IEnumerator ResetCanvases()
        {
            yield return new WaitForSecondsRealtime(0.5f);
            if (!Screen.fullScreen)
            {
                Screen.SetResolution(1600, 900, false);
            }
        }

        protected void OnExitDialog(bool confirmed)
        {
            if (confirmed)
            {
                Application.Quit();
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
                //this.WaitForLoginSignal.Dispatch();
            }
        }

        protected void OnLogoutDialog(bool confirmed)
        {
            if (confirmed)
            {
                this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.UnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect);
                //this.WaitForLoginSignal.Dispatch();
            }
        }
        #endregion
    }
}
