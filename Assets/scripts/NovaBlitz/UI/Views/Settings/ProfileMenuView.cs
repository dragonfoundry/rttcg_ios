
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
	public class ProfileMenuView : AnimatedView
	{
        public Action EventProfileInitialized;
		public event Action EventAvatarClicked;
		public event Action EventCardBackClicked;
        public event Action EventShowLeaderboardClicked;
        public event Action EventCloseClicked;
        public event Action<ShareScreenShotParams> EventShareClicked;
        

		[SerializeField]
		protected Toggle _PlayerToggle;
		[SerializeField]
		protected Toggle _AchievementsToggle;
		[SerializeField]
		protected Toggle _SettingsToggle;

        public GameObject PlayerCardParent;
        public PlayerCardView PlayerCard;

        public Button ShareButton;
        public NativeShare SharingPlugin;
        public RectTransform QuestHolder;
        public QuestView Quest { get; set; }
		public AllAchievementsView AchievementsView;

        public void OpenPlayerTab() { _PlayerToggle.Select(); _PlayerToggle.isOn = true; }
		public void OpenAchievementsTab() { _AchievementsToggle.Select(); _AchievementsToggle.isOn = true; }
		public void OpenSettingsTab() { _SettingsToggle.Select(); _SettingsToggle.isOn = true; }

		public void OnScrimClicked()
		{
            if(this.EventCloseClicked != null)
            {
                this.EventCloseClicked.Invoke();
            }
		}
        
        public void OnShareClick()
        {
            if (this.EventShareClicked != null)
            {
                this.EventShareClicked.Invoke(new ShareScreenShotParams(SharingPlugin, "this text", ShareButton, ShareButton.gameObject));
            }
        }
        

		public void Initialize(ClientPrefs clientPrefs, bool? isAgeGatePassed)
		{
            if(this.EventProfileInitialized != null)
            {
                this.EventProfileInitialized.Invoke();
            }

#if UNITY_IOS || UNITY_ANDROID

#else
            this.ShareButton.gameObject.SetActive(false);
#endif
        }

        #region uGUI
        public void ClickAvatar() { if (null != this.EventAvatarClicked) this.EventAvatarClicked.Invoke(); }
		public void ClickCardBack() { if (null != this.EventCardBackClicked) this.EventCardBackClicked.Invoke(); }
        public void ClickLeaderboard() { if (null != this.EventShowLeaderboardClicked) this.EventShowLeaderboardClicked.Invoke(); }
        #endregion

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (this.Quest != null)
                this.Quest.Recycle();
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class ProfileMenuMediator : Mediator
	{
		[Inject] public ProfileMenuView View {get;set;}

        [Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public LogoutUserSignal LogoutUserSignal {get;set;}
		[Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal {get;set;}
		[Inject] public AccountRecoverySignal AccountRecoverySignal {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}

        [Inject] public ShowLeaderboardSignal ShowLeaderboardSignal { get; set; }
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
		[Inject] public NewConfirmDialogDismissedSignal NewConfirmDialogDismissedSignal {get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject] public UIConfiguration UIConfiguration { get; set; }
        [Inject] public ShareScreenShotSignal ShareScreenShotSignal { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            if(this.View.PlayerCard == null)
            {
                this.View.PlayerCard = GameObject.Instantiate(this.UIConfiguration.PlayerCardPrefab);
                this.View.PlayerCard.transform.SetParent(this.View.PlayerCardParent.transform, false);
                this.View.PlayerCard.StatsPanel.gameObject.SetActive(true);
                this.View.PlayerCard.QuestPanel.gameObject.SetActive(false);
                this.View.PlayerCard.ButtonArea.gameObject.SetActive(true);
                this.View.PlayerCard.PlayStatusPanel.SetActive(false);
                this.View.PlayerCard.VerticalStatusPanel.SetActive(false);
            }
            this.View.EventProfileInitialized += OnInitialize;
            this.View.EventShowLeaderboardClicked += OnShowGameLogClicked;
            this.View.EventAvatarClicked += OnMenu_AvatarClicked;
            this.View.EventCardBackClicked += OnMenu_CardBackClicked;
            this.View.EventShareClicked += OnShareClicked;
            this.View.EventCloseClicked += OnCloseClicked;
            this.View.PlayerCard.ClickGameLog += OnShowGameLogClicked;
            this.View.PlayerCard.ClickAvatar += OnMenu_AvatarClicked;
            this.View.PlayerCard.ClickCardBack += OnMenu_CardBackClicked;
            this.View.PlayerCard.ClickRedeem += OnMenu_RedeemClicked;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
			this.ProfileUpdatedSignal.AddListener(this.OnProfileUpdated);
			this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);

            //this.View.Initialize(this.ClientPrefs, this.PlayerProfile.isAgeGatePassed);
        }

		/// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
		{
            this.View.EventProfileInitialized -= OnInitialize;
            this.View.EventShowLeaderboardClicked -= OnShowGameLogClicked;
            this.View.EventAvatarClicked -= OnMenu_AvatarClicked;
            this.View.EventCardBackClicked -= OnMenu_CardBackClicked;
            this.View.EventShareClicked -= OnShareClicked;
            this.View.EventCloseClicked -= OnCloseClicked;
            this.View.PlayerCard.ClickGameLog -= OnShowGameLogClicked;
            this.View.PlayerCard.ClickAvatar -= OnMenu_AvatarClicked;
            this.View.PlayerCard.ClickCardBack -= OnMenu_CardBackClicked;
            this.View.PlayerCard.ClickRedeem -= OnMenu_RedeemClicked;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
			this.ProfileUpdatedSignal.RemoveListener(this.OnProfileUpdated);
			this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
        }
        		
		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
				this.View.Initialize(this.ClientPrefs, this.PlayerProfile.isAgeGatePassed);
				this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
				this.View.AchievementsView.PopulateView ();
            }
        }

        /// <summary>
        /// Called by the view when it initializes, so we can initialize bound things.
        /// </summary>
        private void OnInitialize()
        {
            /*string avatarArtId = string.Empty;
            string cardBackArtId = string.Empty;
            AvatarDataContract avContract;
            if(!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.AvatarID) && this.GameData.AvatarByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.AvatarID, out avContract))
            {
                avatarArtId = avContract.ArtId;
            }
            CardBackDataContract cbContract;
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.CardBackID) && this.GameData.CardBackByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.CardBackID, out cbContract))
            {
                cardBackArtId = cbContract.ArtId;
            }
            int qps;
            this.PlayerProfile.Wallet.Currencies.TryGetValue(Messages.CurrencyType.QP, out qps);

            var Data = new PlayerData(this.PlayerProfile.MetaProfile.PlayFabID, this.PlayerProfile.MetaProfile.Name, this.PlayerProfile.UserStatistics, this.GameData.RankBrackets,
                avatarArtId, cardBackArtId, qps,
                null, Messages.GameFormat.NoGame, 0, 0, 0, -1, 0, 0, false, null, 0);*/

            this.View.PlayerCard.Initialize(PlayerProfile.PlayerData);


            if (this.View.Quest != null)
                this.View.Quest.Recycle();
            if (PlayerProfile.OnboardingProgress.CurrentQuest != null)
            {
                var quest = ObjectPool.SpawnPrefab<QuestView>(PooledObjectType.QuestView);
                quest.transform.AttachToParent(this.View.QuestHolder.transform, 0);
                quest.transform.localPosition = Vector3.zero;
                quest.transform.localRotation = Quaternion.identity;
                quest.Initialize(PlayerProfile.OnboardingProgress.CurrentQuest);
                View.Quest = quest;
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
            }
        }

        private void OnCloseClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(ProfileMenuView));
        }
        
        private void OnProfileUpdated(ProfileSection section)
        {
            if(this.View.IsOpen && section == ProfileSection.Inventory)
            {
				switch (section)
				{
					case ProfileSection.Inventory:
						this.View.Initialize(this.ClientPrefs, this.PlayerProfile.isAgeGatePassed);
						break;
					case ProfileSection.Statistics:
						this.View.AchievementsView.PopulateView ();
						break;
				}
            }
		}

		protected void OnLanguageChanged()
		{
			if(this.View.IsOpen )
				this.View.AchievementsView.PopulateView ();
		}

        #region View Listeners
        
        protected void OnShowGameLogClicked()
        {
            if(this.PlayerProfile.GameLogs != null)
            {
                this.InitializeAnimatedViewSignal.Dispatch(typeof(GameLogBrowserView),
                    (v) =>
                    {
                        ((GameLogBrowserView)v).Initialize(this.PlayerProfile.GameLogs);
                    });
            }
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(GameLogBrowserView));
        }

        protected void OnShareClicked(ShareScreenShotParams Params)
        {
            this.ShareScreenShotSignal.Dispatch(Params);
        }

        protected void OnMenu_AvatarClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(AvatarSelectorView));
            //ToggleView<AvatarSelectorView>(true);
        }

        protected void OnMenu_CardBackClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(CardBackSelectorView));
            //ToggleView<CardBackSelectorView>(true);
        }

        protected void OnMenu_RedeemClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(RedeemCouponView));
        }
        #endregion
	}
}
