using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using NovaBlitz.Economy;
using System.Text;
using PlayFab.ClientModels;
using Messages;

namespace NovaBlitz.UI
{
	public class AvatarSelectorView : AnimatedView
    {
        public ToggleGroup ToggleGroup;
        public MultiAvatarView MultiAvatar;
		public string SelectedID {get;set; }
        public string BuyingId { get; set; }
        [Inject] public UIConfiguration UIConfiguration { get; set; }

        public event Action<string> EventConfirm;

        [Inject] public BeginVirtualPurchaseSignal BeginVirtualPurchaseSignal { get; set; }

        public void Select(string id)
		{
			this.SelectedID = id;
			foreach (var view in this.MultiAvatar.Instances) {
				if (id == view.AvatarID) {
					var sel = view.GetComponent<Selectable>();
					if (null != sel)
						sel.Select();
					var tog = view.GetComponentInChildren<Toggle>();
					if (null != tog)
						tog.isOn = true;
					break;
				}
			}
		}

		public void ClickAvatar(AvatarView view)
		{
            BuyingId = null;
            if (view.IsOwned == false)
            {
                if (view._BuyButton.IsActive() && view.StoreData != null)
                {
                    this.BuyingId = view.AvatarID;
                    AvatarView newView = this.UIConfiguration.AvatarItemPrefab.Spawn();// View.Instantiate(view, Vector3.zero, Quaternion.identity) as AvatarView;
                    newView.UpdateAvatarView(view.AvatarData);
                    newView._BuyButton.gameObject.SetActive(false);
                    newView._NotOwnedScrim.gameObject.SetActive(false);
                    BeginVirtualPurchaseSignal.Dispatch(view.StoreData, newView);
                    // open the purchase with virtual currency window
                }
                // Otherwise do nothing, as you don't own it, and you can't buy it.
            }
            else
            {
                this.SelectedID = view.AvatarID;
                view._Toggle.isOn = true;
            }

        }

		public void Confirm()
		{
			if (null != EventConfirm)
				EventConfirm.Invoke(this.SelectedID);
		}

        protected override void OnDestroy()
        {
            base.OnDestroy();
            MultiAvatar.Purge();
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class AvatarSelectorMediator : Mediator
	{
		[Inject] public AvatarSelectorView View {get;set;}

        [Inject] public GameData GameData { get; set; }

		[Inject] public ResourceMap ResourceMap {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public SelectAvatarSignal SelectAvatarSignal {get;set;}
        [Inject] public CatalogLoadedSignal CatalogLoadedSignal { get; set; }
        [Inject] public InventoryLoadedSignal InventoryLoadedSignal { get; set; }
		[Inject] public PostPurchaseSignal PostPurchaseSignal {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        
        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
		public override void OnRegister()
		{
            InitializeAvatarData();

			this.View.EventConfirm += OnConfirm;
            this.InventoryLoadedSignal.AddListener(InitializeAvatarData);
            this.CatalogLoadedSignal.AddListener(InitializeDueToCatalog);
            this.PostPurchaseSignal.AddListener(InitializeDueToPurchase);
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.ProfileUpdatedSignal.AddListener(InitializeDueToProfileUpdate);

        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.EventConfirm -= OnConfirm;
            this.InventoryLoadedSignal.RemoveListener(InitializeAvatarData);
            this.CatalogLoadedSignal.RemoveListener(InitializeDueToCatalog);
            this.PostPurchaseSignal.RemoveListener(InitializeDueToPurchase);
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.ProfileUpdatedSignal.RemoveListener(InitializeDueToProfileUpdate);

        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
            }
        }


        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
            }
        }

        private void InitializeDueToPurchase(ItemInstance[] instances)
        {
            var avatars = instances.Where(i => i.ItemClass == NBEconomy.ITEMCLASS_AVATAR).ToList();
            if (avatars.Count() > 0)
            {
                foreach(var avatar in avatars)
                {
                    if (PlayerProfile.OwnedAvatars == null)
                        PlayerProfile.OwnedAvatars = new HashSet<string>();
                    PlayerProfile.OwnedAvatars.Add(avatar.ItemId);
                }
                InitializeAvatarData();
                View.Select(avatars[0].ItemId); 
            }
        }

        private void OnLanguageChanged()
        {
            InitializeAvatarData();
        }

        private void InitializeDueToCatalog(string version)
        {
            InitializeAvatarData();
        }

        private void InitializeDueToProfileUpdate(ProfileSection section)
        {
            if(section == ProfileSection.Inventory && View.BuyingId != null)
            {
                InitializeAvatarData();
                View.Select(View.BuyingId);
                View.BuyingId = null;
            }
        }

        private void InitializeAvatarData()
        {
            // build all the data
            List<AvatarData> avatarDataList = new List<AvatarData>();
            foreach (var kvp in GameData.AvatarDictionary)
            {
                StringBuilder builder = new StringBuilder(NBEconomy.CATALOG_AVATAR_PREFIX);
                builder.Append(kvp.Value.Id);
                string itemId = builder.ToString();
                bool isOwned = PlayerProfile.OwnedAvatars != null && PlayerProfile.OwnedAvatars.Contains(itemId);
                
                var data = new AvatarData() { avatarArtId = kvp.Value.ArtId, avatarId = (int)kvp.Value.Id, avatarItemId = itemId, AvatarSelectorView = this.View };
                NovaCatalogItem item;
                if(GameData.ProductCache.TryGetValue(itemId, out item))
                {
                    data.avatarItem = item;
                    data.isOwned = isOwned;
                }
                if (kvp.Value.IsFree)
                    data.isOwned = true;

                StoreData storeData = null;
                Dictionary<string, StoreData> storeDict;
                if (GameData.StoreCache.TryGetValue(NBEconomy.STORE_AVATAR, out storeDict) && storeDict.TryGetValue(itemId, out storeData))
                    data.storeItem = storeData;

                if (data.isOwned == true || storeData != null)
                    avatarDataList.Add(data);
            }
            this.View.MultiAvatar.Populate(avatarDataList);
            this.View.Select(this.PlayerProfile.MetaProfile.AvatarID);
        }

		protected void OnConfirm(string id)
		{
			this.SelectAvatarSignal.Dispatch(id);
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
		}
    }
}
