﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using I2.Loc;
using strange.extensions.mediation.impl;
using NovaBlitz.Game;
using NovaBlitz.UI.Mediators;

namespace NovaBlitz.UI
{

    public class LocalizationView : View
    {
        public Button LocalizeButton;
        public Toggle _Toggle;
        public LocalizationData Data { get; set; }
        public SetLanguage SetLanguage;
        public TextMeshProUGUI ButtonText;

        public void UpdateLocalizationView(LocalizationData data)
        {
            Data = data;
            if (data.languageName == I2.Loc.LocalizationManager.CurrentLanguage)
            {
                _Toggle.isOn = true;
            }
            var lang = I2.Loc.ScriptLocalization.Get(data.languageName);
            if(string.IsNullOrEmpty(lang))
            {
                lang = data.languageName;
            }   
            ButtonText.SetText(lang);//(I2.Loc.ScriptLocalization.Get("Language/" + data.languageName));
            // set active/inactive
        }
    }

    public class LocalizationData
    {
        public string languageName { get; set; }
        public LanguageSelectorView LanguageSelectorView { get; set; }
    }

    [System.Serializable]
    public class MultiLocalizationSelectorView : MultiRect<LocalizationView, LocalizationData>
    {
        protected override void Initialize(LocalizationView instance, LocalizationData data)
        {
            instance.UpdateLocalizationView(data);
            instance.LocalizeButton.onClick.AddListener(() => { data.LanguageSelectorView.ClickLanguage(instance); });
            instance._Toggle.group = data.LanguageSelectorView.LanguageToggles;
            instance.SetLanguage._Language = data.languageName;
        }
    }
}
