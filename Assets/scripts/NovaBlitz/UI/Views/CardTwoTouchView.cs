﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using TMPro;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class CardTwoTouchView : BaseCardView, IRectTransform
	{
		public event Action<CardData> EventCardChanged;

		public float CardScale = 1f;
		public Vector3 CardOffset;
		public CardEditingWidget EditWidget;
		public CardCraftingWidget CraftWidget;
		public CanvasGroup CardAbilityCanvasGroup;
		public Action CardClicked;
		public bool IsTweening = false;

        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.CardTwoTouchView; } }

        [SerializeField] protected RectTransform _CardNode;
		[SerializeField] protected BaseCardView _CardPrefab;
		protected BaseCardView _CardDisplay;
		private Vector3 defaultScale = Vector3.one;

		public RectTransform RectTransform {get; protected set;}
		public CanvasGroup CanvasGroup {get; protected set;}

		public bool IsEditable {
			set { this.EditWidget.gameObject.SetActive(value);
				this.CraftWidget.gameObject.SetActive(value); }
		}

		public void HideCollectionTotal()
		{
			if(_CardDisplay is CardView)
			{
				CardView cardView = (CardView)_CardDisplay;
				cardView.CardCountFrame.gameObject.SetActive(false);
			}
		}

		override public void ShowNewCardTag(bool isVisible)
        {
            //Debug.LogFormat("ShowNewCardTag: {0}", isVisible );
			_CardDisplay.ShowNewCardTag(isVisible);
        }

		public override void ShowCollectionTotal(bool isVisible, int collectionTotal = 0, int deckQuantity = 0)
		{
			if(_CardDisplay is CardView)
			{
                //Debug.LogFormat("Show Collection total {0}, isVisible:{1}", collectionTotal, isVisible);
                CardView cardView = (CardView)_CardDisplay;
				if(isVisible)
				{
					cardView.SetQuantities(collectionTotal, deckQuantity, isTwoTouch:true);
					cardView.MakeFullbright();
					cardView.CardArt.color = Color.white;
					cardView.CardNodes.gameObject.SetActive(false);
                    cardView.NewCardTag.gameObject.SetActive(false);
                }
				else
				{
					cardView.CardCountFrame.gameObject.SetActive(false);
					cardView.NewCardTag.gameObject.SetActive(false);
					cardView.CardCountLabel.text= string.Empty;
				}
			}
		}

		public override void RefreshCardData()
		{
			if (null == _CardDisplay) 
			{
				_CardDisplay = UITools.PoolSpawnRect(_CardPrefab, Vector3.zero, _CardNode);
				_CardDisplay.transform.localPosition = this.CardOffset;
				var cardView = _CardDisplay as CardView;
				if (null != cardView)
				{
					cardView.Canvas.transform.localScale = Vector3.one * this.CardScale;
				}
				
			}
			_CardDisplay.CardData = _CardData;
			if(_CardDisplay is CardView)
			{
				CardView cardView = (CardView)_CardDisplay;
				Texture texture = AssetBundleManager.Instance.LoadAsset<Texture>(_CardData.ArtID); //Resources.Load(_CardData.ArtID) as Texture;
				this.defaultScale = cardView.ContentCanvasGroup.transform.localScale;
				if(texture != null)
				{
					cardView.CardArt.texture = texture; 
				}
				else
				{
					cardView.ApplyAspectArt();
				}
			}
			if (null != this.EventCardChanged && null != CardData) this.EventCardChanged.Invoke(_CardData);
		}

		public override void RefreshIsVisible()
		{
			this.CanvasGroup.blocksRaycasts = _IsVisible;
			this.CanvasGroup.alpha = _IsVisible ? 1f : 0f;
		}

		private Sequence settleSeq = null;
		public void PlaySettleIntoPlaceTween()
		{
			if(_CardDisplay is CardView)
			{
				CardView cardView = (CardView)_CardDisplay;
				cardView.ContentCanvasGroup.transform.DOKill();
				if(this.settleSeq != null ) { this.settleSeq.Kill(); }
				this.settleSeq = DOTween.Sequence();
				this.settleSeq.Append(cardView.ContentCanvasGroup.transform.DOScale(this.defaultScale, 0f));
				this.settleSeq.Append(cardView.ContentCanvasGroup.transform.DOScale(new Vector3(0.84f,0.84f,0.84f),0.5f).SetEase(Ease.InCubic));
				this.settleSeq.Append(cardView.ContentCanvasGroup.transform.DOScale(this.defaultScale,0.5f).SetEase(Ease.OutCubic));
			}
		}

		float? pointerDownTime = null;
		public void OnPointerDown()
		{
			this.pointerDownTime = Time.unscaledTime;
			//Debug.Log("Pointer Down");
			if(this.CardAbilityCanvasGroup != null)
			{
				this.CardAbilityCanvasGroup.DOFade(1.0f, 0.15f);
			}
		}

		public void OnPointerUp()
		{
			//Debug.Log("Pointer Up");
			if(this.CardAbilityCanvasGroup != null)
			{
				this.CardAbilityCanvasGroup.DOFade(0f, 0.3f);
			}

			if(this.pointerDownTime != null && (Time.unscaledTime - this.pointerDownTime.Value) < 0.3f)
			{
				if(this.CardClicked != null)
				{
					this.CardClicked.Invoke();
				}
			}
		}

		public void OnPointerEnter()
		{
			//Debug.Log("OnPointerEnter");
			if(DOTween.IsTweening(this.transform) == false)
			{
				if(this.CardAbilityCanvasGroup != null)
				{
					this.CardAbilityCanvasGroup.DOFade(1.0f, 0.15f);
				}
			}
		}

		public void OnPointerExit()
		{
			//Debug.Log("OnPointerExit");
			this.pointerDownTime = null;

			if(DOTween.IsTweening(this.transform) == false)
			{
				if(this.CardAbilityCanvasGroup != null)
				{
					this.CardAbilityCanvasGroup.DOFade(0f, 0.3f);
				}
			}
		}

		void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
			this.CanvasGroup = GetComponent<CanvasGroup>();
		}
	}
}