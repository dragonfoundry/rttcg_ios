﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.Mediators;
using TMPro;

namespace NovaBlitz.UI
{
	public class PlayerProfileView : View
	{
		public Slider RankSliderCurrent;
		[SerializeField] protected TextMeshProUGUI _RankLabelCurrent;
		[SerializeField] protected TextMeshProUGUI _RatingLabelCurrent;
		[SerializeField] protected TextMeshProUGUI _DateLabelCurrent;

        public int RatingCurrent { set { _RatingLabelCurrent.text = value.ToString(); } }
        public int RankCurrent { set { _RankLabelCurrent.text = value.ToString(); } }
		public DateTime DateCurrent { set { _DateLabelCurrent.text = FormatDate(value); } }

		protected string FormatDate(DateTime date) { return DateTime.Now.ToString("MMMM yyyy").ToUpper(); }
	}
}

namespace NovaBlitz.UI.Mediators
{
	public class PlayerProfileMediator : Mediator
	{
		[Inject] public PlayerProfileView View {get;set;}

		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}


		public override void OnRegister()
		{
			this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
			RefreshView();
		}

		public override void OnRemove()
		{
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
		}

		protected void RefreshView()
		{
            /*this.PlayerProfile.UpdateRankAndRating();

				var bracket = EngineUtils.GenerateLevelUpSteps(PlayerProfile.CurrentRating, PlayerProfile.CurrentRating, this.GameData.RankBrackets).Last();
				this.View.RankSliderCurrent.minValue = bracket.BracketRange.Min - bracket.BracketRange.Max * 0.1f;
				this.View.RankSliderCurrent.maxValue = bracket.BracketRange.Max;
				this.View.RankSliderCurrent.value = PlayerProfile.CurrentRating;
				this.View.RankCurrent = PlayerProfile.CurrentRank;
            this.View.RatingCurrent = PlayerProfile.CurrentRating;
            this.View.DateCurrent = DateTime.Now;*/
		}

		protected void OnProfileUpdated(ProfileSection section)
		{
			if (section != ProfileSection.Statistics)
				return;
			RefreshView();
		}
	}
}
