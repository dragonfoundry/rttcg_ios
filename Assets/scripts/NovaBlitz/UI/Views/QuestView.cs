﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Messages;
using I2.Loc;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class QuestView : PoolableMonoBehaviour
    {
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.QuestView; } }

        public Image QuestInactive;
        public TextMeshProUGUI QuestTimer;

        public Image QuestActive;
        public TextMeshProUGUI QuestDetails;

        public GameObject CreditsPrize;
        public TextMeshProUGUI CreditsAmount;
        public GameObject FreeDraftPrize;
        public GameObject SmartPackPrize;
        public GameObject StarterDeckPrize;
        public RawImage StarterArt;
        public TextMeshProUGUI StarterName;

        public Image[] PipBackgrounds;
        public Image[] Pips;

        public QuestData QuestData { get; set; }
        TimeSpan TimeSpan { get; set; }

        public void Update()
        {
            if(QuestInactive.gameObject.activeSelf && QuestData != null)
            {
                var timeSpan = QuestData.RefreshTime - Server.Time;
                if (TimeSpan.TotalSeconds != timeSpan.TotalSeconds)
                {
                    TimeSpan = timeSpan;
                    QuestTimer.text = string.Format(I2.Loc.ScriptLocalization.Quest_Refresh, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
                    if(TimeSpan.TotalSeconds <= 0)
                    {
                        QuestInactive.gameObject.SetActive(false);
                        QuestActive.gameObject.SetActive(true);
                        QuestData.IsActive = true; // This modifies the quest data stored in the profile, so it'll be carried over to other views
                    }
                }
            }
        }

        public void Initialize(QuestData questData)
        {
            QuestData = questData;
            //QuestData.IsActive = (questData.RefreshTime - DateTime.UtcNow).TotalSeconds < 0;

            for (int i = 0; i< PipBackgrounds.Length && i<Pips.Length; i++)
            {
                PipBackgrounds[i].gameObject.SetActive(questData.TargetScore > i && questData.TargetScore <= PipBackgrounds.Length);
                Pips[i].gameObject.SetActive(questData.CurrentScore > i && questData.TargetScore <= PipBackgrounds.Length);
            }
            string questString = string.Format(ScriptLocalization.Get(string.Format("QuestDetails/{0}", questData.ID)), questData.TargetScore);
            if(questData.TargetScore > PipBackgrounds.Length)
            {
                questString = questString + string.Format("\n<align=center>{0}/{1}", questData.CurrentScore, questData.TargetScore);
                QuestDetails.rectTransform.sizeDelta = new Vector2(280, 115);
            }
            else
            {
                QuestDetails.rectTransform.sizeDelta = new Vector2(280, 95);
            }
            QuestDetails.text = questString;
            CreditsAmount.text = questData.Amount.ToString();
            CreditsPrize.gameObject.SetActive(questData.Currency == CurrencyType.CR);
            SmartPackPrize.gameObject.SetActive(questData.Currency == CurrencyType.SP);
            FreeDraftPrize.gameObject.SetActive(questData.Currency == CurrencyType.FD);
            StarterDeckPrize.gameObject.SetActive(questData.Aspect != Aspect.NoAspect);
            string artId = null;
            switch (questData.Aspect)
            {
                case Aspect.Arcane:
                    StarterName.text = I2.Loc.ScriptLocalization.arcane_starter;
                    artId = "ArcaneCardBack";
                    break;
                case Aspect.Tech:
                    StarterName.text = I2.Loc.ScriptLocalization.tech_starter;
                    artId = "TechCardBack";
                    break;
                case Aspect.Divine:
                    StarterName.text = I2.Loc.ScriptLocalization.divine_starter;
                    artId = "DivineCardBack";
                    break;
                case Aspect.Nature:
                    StarterName.text = I2.Loc.ScriptLocalization.nature_starter;
                    artId = "NatureCardBack";
                    break;
                case Aspect.Chaos:
                    StarterName.text = I2.Loc.ScriptLocalization.chaos_ctarter;
                    artId = "ChaosCardBack";
                    break;
                default:
                    StarterName.text = string.Empty;
                    break;
            }
            if (artId != null)
            {
                Texture tex = AssetBundleManager.Instance.LoadAsset<Texture>(artId);
                //Texture tex = (Texture)Resources.Load(artId);
                if (tex == null)
                {
                    tex = AssetBundleManager.Instance.LoadAsset<Texture>("BasicCardBack");
                    //tex = (Texture)Resources.Load("BasicCardBack");
                }
                StarterArt.texture = tex;
            }

            QuestActive.gameObject.SetActive(questData.IsActive);
            QuestInactive.gameObject.SetActive(!questData.IsActive);
        }

        private string ToTweenString;

        private int _TweenValue;
        public int TweenValue { get { return _TweenValue; } set {
                if (value != _TweenValue && ToTweenString != null)
                {
                    _TweenValue = value;
                    QuestDetails.text = string.Format(ToTweenString, value, QuestData.TargetScore);
                }
            }
        }

        public void TweenQuestAmount(int newScore)
        {
            if (QuestData == null || newScore <= QuestData.CurrentScore)
                return;
            ToTweenString = string.Format(ScriptLocalization.Get(string.Format("QuestDetails/{0}", QuestData.ID)), QuestData.TargetScore) + "\n<align=center>{0}/{1}";

            Sequence seq = DOTween.Sequence();
            if (QuestData.TargetScore > PipBackgrounds.Length)
            {
                //Debug.Log("Tweening text");
                seq.Append(DOTween.To(x => { TweenValue = (int)x; }, QuestData.CurrentScore, newScore, 1.5f));
                // tweening text
            }
            else
            {
                //Debug.LogFormat("Tweening pips from {0} to {1}", QuestData.CurrentScore, newScore);
                float delay = 0.0f;
                for(int i = Math.Max(0,QuestData.CurrentScore); i < Math.Min(Math.Min(Pips.Length, newScore), QuestData.TargetScore); i++)
                {
                    //Debug.LogFormat("Tweening pip {0}", i);
                    // tween that pip - punch scale, fade in
                    var pip = Pips[i];
                    seq.Insert(0.0f, pip.DOFade(0.0f, 0.0f));
                    seq.InsertCallback(0.01f, ()=> pip.gameObject.SetActive(true));
                    seq.Insert(delay+0.01f, pip.transform.DOPunchScale(Vector3.one * 2.0f, 1.0f, vibrato: 1, elasticity: 0.5f));
                    seq.Insert(delay+0.01f, pip.DOFade(1.0f, 0.3f));
                    delay += 0.3f;
                }
                // tweening pips
            }

            if(newScore >= QuestData.TargetScore)
            {
                seq.Append(QuestActive.DOColor(Color.white, 0.5f));
                seq.Join(this.transform.DOPunchScale(Vector3.one * 0.75f, 1.0f, vibrato: 1, elasticity: 0.5f));
            }
        }

        public void OnLocalize()
        {
            if (QuestData != null)
                Initialize(QuestData);
        }

        public override void OnRecycle()
        {

            QuestActive.color = Color.white;
            StarterArt.texture = null;
            QuestData = null;
        }
    }


    /*public class QuestData
    {
        public int ID { get; set; }
        public string ArtId { get; set; }
        public CurrencyType Currency { get; set; }
        public Aspect Aspect { get; set; }
        public int RewardAmount { get; set; }
        public int TargetScore { get; set; }
        public int CurrentScore { get; set; }
        public DateTime RefreshTime { get; set; }
        public bool IsActive { get; set; }

        public QuestData()
        {
        }
    }*/
}
