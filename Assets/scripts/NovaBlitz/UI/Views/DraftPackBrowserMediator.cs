﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using System;
using Messages;
using LinqTools;
using strange.extensions.context.impl;
using UnityEngine.UI;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class DraftPackBrowserMediator : Mediator
    {
		[Inject] public DraftPackBrowserView View {get;set;}
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal{get;set;}
		[Inject] public SubmitDraftPickSignal SubmitDraftPickSignal {get;set;}
		[Inject] public DraftNextPackSignal DraftNextPackSignal {get;set;}
		[Inject] public DraftOverSignal DraftOverSignal {get;set;}
		[Inject] public GameData GameData {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public INovaContext ContextView {get;set;}
        [Inject] public PlayerProfile PlayerProfile {get;set;}
		
		private bool isReadyForNextPack = false;
        
		override public void OnRegister()
		{
			this.AnimatedViewOpenedSignal.AddListener(OnOpenAnimatedView);
			this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
			this.View.CardPrefab = this.UIConfiguration.CardPrefab;
			this.View.DraggableCardPrefab = this.UIConfiguration.DraggableCardPrefab;
			this.DraftOverSignal.AddListener(this.OnDraftOver);
            this.DraftNextPackSignal.AddListener(this.OnNormalPick);
            this.SubmitDraftPickSignal.AddListener(this.OnSubmitDraftPick);
        }

        override public void OnRemove()
		{
			this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
			this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
			this.DraftOverSignal.RemoveListener(this.OnDraftOver);
			this.SubmitDraftPickSignal.RemoveListener(this.OnSubmitDraftPick);
            this.DraftNextPackSignal.RemoveListener(OnNormalPick);
            this.SubmitDraftPickSignal.RemoveListener(this.OnSubmitDraftPick);
        }

        private void OnSubmitDraftPick(DraftPick draftPick)
        {
			this.isReadyForNextPack = false;
			float fadeDuration = 0.25f;
            this.View.ClearPack(fadeDuration, draftPick.CardToPick);
			this.View.HeaderLabel.text = I2.Loc.ScriptLocalization.DraftNextPack;
			StartCoroutine(MarkIsReadyFlag(fadeDuration));
        }
		
		IEnumerator MarkIsReadyFlag(float delay)
		{
			yield return new WaitForSecondsRealtime(delay);
			this.isReadyForNextPack = true;
		}

        private void OnDraftOver()
        {
           this.View.ClearPack();
		   this.View.HeaderLabel.text = I2.Loc.ScriptLocalization.DraftDraftComplete;
        }

        private void OnOpenAnimatedView(Type viewType)
        {
            if(viewType == typeof(DraftPackBrowserView))
			{
				this.View.Recycle();
                View.Initialize();
                
                this.isReadyForNextPack = true;
                if (this.PlayerProfile.DraftProgress != null && this.PlayerProfile.DraftProgress.NextPack != null && this.PlayerProfile.DraftProgress.NextPack.Count > 0)
                {
                    StartCoroutine(this.SpawnPackCards(new DraftNextPack { DraftProgress = this.PlayerProfile.DraftProgress }));
                }
            }
        }
		
		private void OnCloseAnimatedView(Type viewType)
		{
			if(viewType == typeof(DraftPackBrowserView))
			{
			}
		}
		
		private void OnNormalPick(DraftNextPack nextPack)
		{
			Debug.LogFormat("OnNormalPick: num cards to pick from: {0}", nextPack.DraftProgress.NextPack.Count );
            this.PlayerProfile.DraftProgress = nextPack.DraftProgress;
			StartCoroutine(this.SpawnPackCards(nextPack));
		}
		
		IEnumerator SpawnPackCards(DraftNextPack nextPack)
        {
			while(this.isReadyForNextPack == false) yield return null;

            Debug.Log("Spawn next pack");

			for(int i=0;i<nextPack.DraftProgress.NextPack.Count;i++)
			{
				CardView cardView = this.View.SpawnCard(GameData.CardDictionary[nextPack.DraftProgress.NextPack[i]]);
                Debug.LogFormat("draft cardid: {0} card:{1}", nextPack.DraftProgress.NextPack[i], cardView == null ? "null" : cardView.TitleLocalizer.Term);
                cardView.DraftPickNumber = nextPack.DraftProgress.PickNumber;
				cardView.CanvasGroup.alpha = 0;
            }
            foreach(var spacer in View.Spacers)
            {
                spacer.gameObject.SetActive(false);
            }

			this.View.DealCardPack();
            this.View.HeaderLabel.text = I2.Loc.ScriptLocalization.DraftNextPick;
		}
    }
}