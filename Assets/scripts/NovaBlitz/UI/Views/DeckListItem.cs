﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using LinqTools;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class DeckListItem : PoolableMonoBehaviour, IRectTransform
	{
		// Drag Stuff
		[SerializeField] protected DeckListItem deckListItemPrefab;
		[SerializeField] DraggableItem DragItem;
		public DeckListItem DraggedDeckListItem {get; protected set;}
		public IDragOverlay DragOverlay {get;set;}
		public RectTransform BoundsRect;
		
		// View Stuff
		public TextMeshProUGUI DeckNameLabel;
		public TextMeshProUGUI CardCountLabel;
		public Toggle DeckButton;
		public CanvasGroup CanvasGroup;
		public Image EmptySlotImage;
		public Image EmptySlotSelectedImage;
		public Image Background;
		//public Image SelectedOverlay;
		public CanvasGroup UnderlapTrashFrame;
		public CanvasGroup OverlapTrashFrame;
		public NovaButton TrashButton;
        public Image UnderlapTrashButton;

        public Image ArcaneAspect;
		public Image TechAspect;
		public Image DivineAspect;
		public Image NatureAspect;
		public Image ChaosAspect;
		public Sprite ThumbSelect;
		public Sprite ThumbDefault;
		public Image ThumbImage;
        public Image LockImage;
		public NovaAudioPlayer TickSound;

        public CardArtImage DeckArt;

		public DeckData DeckData {get; private set;}

		private DeckListView deckListView;

		public RectTransform RectTransform {get; protected set;}
		private Dictionary<CardAspect, Image> AspectIcons = new Dictionary<CardAspect, Image>();

        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.DeckListItem; } }

        public bool IsVisible {
			set { this.CanvasGroup.alpha = value ? 1f : 0f;
				this.CanvasGroup.interactable = value;
				this.CanvasGroup.blocksRaycasts = value; }
		}

        public bool IsTutorial { get; set; }

		void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
			this.AspectIcons[CardAspect.Arcane] = this.ArcaneAspect;
			this.AspectIcons[CardAspect.Tech] = this.TechAspect;
			this.AspectIcons[CardAspect.Divine] = this.DivineAspect;
			this.AspectIcons[CardAspect.Nature] = this.NatureAspect;
			this.AspectIcons[CardAspect.Chaos] = this.ChaosAspect;
						
			this.DragItem.EventBeginDrag += this.OnDragItem_BeginDrag;
			this.DragItem.EventDrag += OnDragItem_Drag;
			this.DragItem.EventEndDrag += OnDragItem_EndDrag;
			
			/*
			this.DragItem.EventClick += OnDragItem_Click;
			this.DragItem.EventLongPress += OnDragItem_LongPress;
			this.DragItem.EventLongPressUp += OnDragItem_LongPressUp;
			*/
		}

		public void OnDeleteButtonClicked()
		{
			if(this.deckListView != null && this.deckListView.DeckDeleted != null )
			{
				this.deckListView.DeckDeleted.Invoke();
			}
		}

		private Sequence buttonSeq = null;
		public void ShowDeleteButton(bool isVisible, bool isAnimated=false)
		{
            //isVisible = IsTutorial ? false : isVisible;
			RectTransform underlapTrans = (RectTransform)this.UnderlapTrashFrame.transform;
			RectTransform overlapTrans = (RectTransform)this.OverlapTrashFrame.transform;

            //Debug.LogFormat("ShowDeleteButton: {0} isVisible:{1}isAnimated:{2};{3}", this.gameObject.name, isVisible, isAnimated, this.gameObject);

            if (this.buttonSeq != null)
			{
				this.buttonSeq.Kill();
			}

			if(isAnimated == false)
			{
				this.OverlapTrashFrame.alpha = isVisible? 1:0;
				this.UnderlapTrashFrame.alpha = isVisible? 1:0;
				Vector2 anchorPos = underlapTrans.anchoredPosition;
				anchorPos.x = isVisible ? 76f : 0f;
				overlapTrans.anchoredPosition = anchorPos;
				underlapTrans.anchoredPosition = anchorPos;
				this.OverlapTrashFrame.blocksRaycasts = isVisible;
			}
			else
			{	
				// Create a new buttons tween sequence
				this.buttonSeq = DOTween.Sequence();


				if(isVisible)
				{
					this.UnderlapTrashFrame.alpha = 1;
					Vector2 anchorPos = underlapTrans.anchoredPosition;
					anchorPos.x = 115;
					overlapTrans.anchoredPosition = anchorPos;
					
					// Tween in the button
					buttonSeq.Append(underlapTrans.DOAnchorPosX(0,0));
					buttonSeq.Append(underlapTrans.DOAnchorPosX(97f,0.15f).SetEase(Ease.OutCirc));
					buttonSeq.AppendInterval(0.02f);
					buttonSeq.AppendCallback(()=>{
						this.UnderlapTrashFrame.alpha=0;
						this.OverlapTrashFrame.alpha=1;
						this.OverlapTrashFrame.blocksRaycasts = true;
					});
					buttonSeq.Append(overlapTrans.DOAnchorPosX(76f,0.09f)).SetEase(Ease.InCirc);
					buttonSeq.AppendCallback(()=>this.TickSound.Play());
					buttonSeq.Play();
				}
				else
				{
					this.ShowDeleteButton(false);
				}
			}
		}
		
		void LateUpdate()
		{
			if(this.DraggedDeckListItem != null)
			{
				this.ConstrainToBounds();
			}
		}
		
		void ConstrainToBounds()
		{
			
			RectTransform deckListItemRect = (RectTransform)this.DraggedDeckListItem.transform;
			
			 // Get the constraining Bounds rect
			Vector3[] corners = new Vector3[4];
			this.BoundsRect.GetWorldCorners(corners);
			Rect worldBounds = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[1].y - corners[0].y);
			Vector3 worldMin = new Vector3(worldBounds.xMin, worldBounds.yMin, this.BoundsRect.position.z);
			Vector3 worldMax = new Vector3(worldBounds.xMax, worldBounds.yMax, this.BoundsRect.position.z);
			Vector3 viewportMin = Camera.main.WorldToViewportPoint(worldMin);
			Vector3 viewportMax = Camera.main.WorldToViewportPoint(worldMax);
			Rect viewPortBounds = new Rect(viewportMin.x, viewportMin.y, viewportMax.x - viewportMin.x, viewportMax.y - viewportMin.y);

			// Get the dragged item's bounds
			Vector3[] corners2 = new Vector3[4];
			deckListItemRect.GetWorldCorners(corners2);
			Rect itemBounds = new Rect(corners2[0].x, corners2[0].y, corners2[2].x - corners2[0].x, corners2[1].y - corners2[0].y);
			Vector3 itemWorldMin = new Vector3(itemBounds.xMin, itemBounds.yMin, this.DraggedDeckListItem.transform.position.z);
			Vector3 itemWorldMax = new Vector3(itemBounds.xMax, itemBounds.yMax, this.DraggedDeckListItem.transform.position.z);
			Vector3 itemViewportMin = Camera.main.WorldToViewportPoint(itemWorldMin);
			Vector3 itemViewportMax = Camera.main.WorldToViewportPoint(itemWorldMax);
			Rect itemViewportBounds = new Rect(itemViewportMin.x, itemViewportMin.y, itemViewportMax.x - itemViewportMin.x, itemViewportMax.y - itemViewportMin.y);

			Vector3 draggedItemViewport = Camera.main.WorldToViewportPoint(this.DraggedDeckListItem.transform.position);

			if (itemViewportBounds.xMin < viewPortBounds.xMin) { draggedItemViewport.x += viewPortBounds.xMin - itemViewportBounds.xMin; }
			else if (itemViewportBounds.xMax > viewPortBounds.xMax) { draggedItemViewport.x += viewPortBounds.xMax - itemViewportBounds.xMax; }
			if (itemViewportBounds.yMin < viewPortBounds.yMin) { draggedItemViewport.y += viewPortBounds.yMin - itemViewportBounds.yMin; }
			else if (itemViewportBounds.yMax > viewPortBounds.yMax) { draggedItemViewport.y += viewPortBounds.yMax - itemViewportBounds.yMax; }

			Vector3 worldOffset = Camera.main.ViewportToWorldPoint(draggedItemViewport);
			this.DraggedDeckListItem.transform.position = worldOffset;
		}

        private void OnDragItem_BeginDrag(Vector2 pos)
        {
            this.SpawnDraggedListItem(pos);
			this.deckListView.StartDragging();
        }
		
		protected void OnDragItem_Drag(Vector2 pos)
		{
			if (null != this.DraggedDeckListItem)
			{
				this.DragOverlay.Move(this.DraggedDeckListItem.RectTransform, pos);
				this.deckListView.PositionDraggedItem(this, this.DraggedDeckListItem);
			}
		}
		
		protected void OnDragItem_EndDrag(Vector2 pos)
		{
			this.DespawnDraggedDeckListItem();
            if (this.DeckData != null)
            {
                this.deckListView.EndDragging(this.DeckData);
            }
		}
		
		protected void DespawnDraggedDeckListItem()
		{
			if (null != this.DraggedDeckListItem)
			{
				this.EmptySlotSelectedImage.gameObject.SetActive(false);
				this.DraggedDeckListItem.ThumbImage.sprite = this.ThumbDefault;
				Sequence seq = DOTween.Sequence();
				seq.Append(this.DraggedDeckListItem.transform.DOMove(this.transform.position, 0.3f).SetEase(Ease.OutCubic));
				seq.AppendCallback(()=>{
					this.EmptySlotImage.gameObject.SetActive(false);
                    this.ThumbImage.DOFade(1f,0);
					this.CanvasGroup.alpha =1.0f;
					this.DeckButton.gameObject.SetActive(true);
				});
				seq.AppendInterval(0.05f);
				//seq.AppendCallback(()=>GameObject.Destroy(this.DraggedDeckListItem.gameObject));
				seq.AppendCallback(()=> {this.DraggedDeckListItem.Recycle(); this.DraggedDeckListItem = null;});
				seq.Play();
			}
		}
		
		protected void SpawnDraggedListItem(Vector2 pos)
		{
			if (null == this.DraggedDeckListItem) 
			{
                this.DraggedDeckListItem = UITools.PoolSpawnRect(this.deckListItemPrefab, Vector3.zero, this.deckListView.transform);
                this.DraggedDeckListItem.RectTransform.sizeDelta = this.deckListItemPrefab.RectTransform.sizeDelta;
				this.DragOverlay.Place(this.DraggedDeckListItem.RectTransform);
				this.DragOverlay.Move(this.DraggedDeckListItem.RectTransform, pos);
				this.DraggedDeckListItem.InitializeDeckButton(this.DeckData);
                this.DraggedDeckListItem.IsVisible = true;
				this.DraggedDeckListItem.CanvasGroup.blocksRaycasts = false;
                this.DraggedDeckListItem.Background.gameObject.SetActive(true);
                this.DraggedDeckListItem.RectTransform.pivot = new Vector2(0.1f, 0.5f);
                this.DraggedDeckListItem.ThumbImage.sprite = this.ThumbSelect;
				this.DraggedDeckListItem.transform.localScale = new Vector3(1.1f,1.1f,1.1f);
				
				this.CanvasGroup.alpha = 0.25f;
				//this.EmptySlotSelectedImage.gameObject.SetActive(true);
				//this.EmptySlotImage.gameObject.SetActive(true);
				//this.DeckButton.gameObject.SetActive(false);
				
				this.ThumbImage.DOFade(0f,0);

				
				if (null != this.DeckData)
				{
					this.DraggedDeckListItem.gameObject.name = string.Format("Dragged {0}", this.DeckData.Name);
				}
			}
            else
                Debug.Log("Failed to spawn Dragged Deck List Item");
        }

        public void OnDeckToggled(bool isActive)
		{
			if(this.deckListView != null)
			{
				this.deckListView.OnDeckToggled(this, isActive);
			}
		}

		public void InitializeEmptySlot()
		{
			this.ShowDeleteButton(false);
			this.DeckButton.gameObject.SetActive(false);
			this.DeckButton.isOn = false;
			this.ThumbImage.gameObject.SetActive(false);
			//this.AddDeckButton.gameObject.SetActive(false);
			this.EmptySlotImage.gameObject.SetActive(true);
			this.gameObject.name = "EmptyDeckSlot";
		}
		
		public void InitializeDeckButton(DeckData deck, DeckListView deckListView = null)
        {
            if (deck == null)
                Debug.LogErrorFormat("DeckListItem: Deck is null");
            
            this.deckListView = deckListView;
            this.DeckData = deck;

            bool isUnlocked = true;
            // Set button images
            if (deckListView != null)
            {
                this.DeckButton.group = deckListView.ToggleGroup;
                this.IsTutorial = this.IsTutorial || !deckListView.PlayerProfile.HasUnlockedAllDecks;
                if (IsTutorial)
                {
                    isUnlocked = (deckListView.PlayerProfile.OnboardingProgress != null 
                        && deckListView.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests != null
                        && (deckListView.PlayerProfile.OnboardingProgress.CurrentQuest == null
                        || !(deck.ID == "startArcane" && (deckListView.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests.Contains(-1) || deckListView.PlayerProfile.OnboardingProgress.CurrentQuest.ID == -1)
                            || deck.ID == "startDivine" && (deckListView.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests.Contains(-2) || deckListView.PlayerProfile.OnboardingProgress.CurrentQuest.ID == -2)
                            || deck.ID == "startNature" && (deckListView.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests.Contains(-3) || deckListView.PlayerProfile.OnboardingProgress.CurrentQuest.ID == -3)
                            || deck.ID == "startChaos" && (deckListView.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests.Contains(-4) || deckListView.PlayerProfile.OnboardingProgress.CurrentQuest.ID == -4)
                        )));

                }
                else
                {
                }
                this.TrashButton.gameObject.SetActive(!IsTutorial);
                this.UnderlapTrashButton.gameObject.SetActive(!IsTutorial);
            }

            this.ShowDeleteButton(false);
			this.EmptySlotImage.gameObject.SetActive(false);
            this.DeckButton.gameObject.SetActive(true);
			this.ThumbImage.gameObject.SetActive(true);
            this.DragItem.gameObject.SetActive(isUnlocked);
            UpdateLabels(isUnlocked);

            
			this.DeckButton.onValueChanged.RemoveListener(this.OnDeckToggled);
            this.DeckButton.onValueChanged.AddListener(this.OnDeckToggled);
            this.DeckArt.gameObject.SetActive(isUnlocked);
            this.LockImage.gameObject.SetActive(!isUnlocked);
            if (isUnlocked)
            {
                this.DeckArt.UpdateCardArt(DeckData.DeckArt, (DeckData.Aspects.Count > 0 ? DeckData.Aspects[0] : CardAspect.Chaos));
                this.DeckButton.interactable = true;
            }
            else
            {
                this.DeckButton.interactable = false;
            }

			// Show the correct aspects
			this.DisableAspecIcons();
			for(int i=0;i<deck.Aspects.Count; i++)
			{
				// Turn on the aspect icon if present in the deck aspects
				this.AspectIcons[deck.Aspects[i]].gameObject.SetActive(true);
			}
        }

        public void UpdateLabels(bool isUnlocked)
        {
            var deck = this.DeckData;
            if (deck == null)
                return;
            // Set deck labels
            this.DeckNameLabel.text = deck.Name;
            if(!isUnlocked)
            {
                this.CardCountLabel.text = string.Empty;
            }
            else if (!deck.IsLegalDeck)
            {
                this.CardCountLabel.color = new Color(242 / 256f, 89 / 256f, 53 / 256f);
                this.CardCountLabel.text = string.Format("{0}/{1} {2}", deck.Cards.Count, deck.Cards.Count > deck.MaxDeckSize ? deck.MaxDeckSize : deck.MinDeckSize, I2.Loc.ScriptLocalization.NotLegal);
            }
            else
            {
                this.CardCountLabel.color = new Color(158 / 256f, 251 / 256f, 255 / 256f);
                this.CardCountLabel.text = string.Format("{0}/{1}", deck.Cards.Count, deck.MinDeckSize);
            }
        }

        private void DisableAspecIcons()
		{
			foreach(Image aspectIcon in this.AspectIcons.Values) { aspectIcon.gameObject.SetActive(false); }
		}
		
		public void PointerEnter()
		{
			this.ThumbImage.sprite = this.ThumbSelect;
		}
		
		public void PointerExit()
		{
			this.ThumbImage.sprite = this.ThumbDefault;
		}

        public override void OnRecycle()
        {
            DeckArt.ResetArt();
            DeckData = null;
        }
    }
}
