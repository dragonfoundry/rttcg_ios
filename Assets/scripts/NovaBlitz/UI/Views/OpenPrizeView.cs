﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using TMPro;
using strange.extensions.mediation.impl;
using NovaBlitz.Economy;
using DG.Tweening;
using DragonFoundry.Fx;
using NovaBlitz.Game;
using Messages;
using System.Text;

namespace NovaBlitz.UI
{
    [Serializable]
    public class RewardNode
    {
        public RectTransform BaseRect;
        public RectTransform RewardItemRoot;
        [HideInInspector]
        public ParticleEffectsTrigger CurrentFx;
        [HideInInspector]
        public bool IsRevealed;
        [HideInInspector]
        public int NumOwned;
        [HideInInspector]
        public Messages.Prize PrizeData;
        [HideInInspector]
        public bool IsRevealComplete;
        private Image _RewardImage;
        public Image RewardImage { get { if (_RewardImage == null) _RewardImage = BaseRect.GetComponent<Image>(); return _RewardImage; } }

        public void Reset()
        {
            this.IsRevealed = false;
            this.IsRevealComplete = false;
            this.CurrentFx = null;
            this.NumOwned = 0;
            this.PrizeData = null;
        }
    }

    public class OpenPrizeView : AnimatedView
    {
        private PlayerProfile PlayerProfile { get; set; }
        private GameOverMessage Message { get; set; }
        public PrizeViewType PrizeViewType { get; set; }
        
        [Inject] public PlayFxSignal PlayFxSignal { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

        [SerializeField] protected ParticleEffectsTrigger PackFX;
        [SerializeField] protected RectTransform CardLayout;
        [SerializeField] protected RectTransform CardLayoutArrangement;
        [SerializeField] protected RectTransform[] DestinationNodes;
        [SerializeField] protected TextMeshProUGUI[] PrizeTextNodes;
        [SerializeField] public NovaButton DoneButton;

        public RewardNode[] RewardNodes;
        public RectTransform RevealDestination;
        public CanvasGroup DoneButtonCanvasGroup;
        public CanvasGroup ShareButtonCanvasGroup;
        public GameObject ClickScrim;
        public Image MiddleScrim;

        public NovaAudioPlayer RevealRarity;
        public NovaAudioPlayer RevealCard;
        public NovaAudioPlayer CardFlip;
        public NovaAudioPlayer CardZoom;
        public NovaAudioPlayer[] CardDeal = new NovaAudioPlayer[]
        {
            new NovaAudioPlayer("fx_trails_2"),
            new NovaAudioPlayer("fx_trails_2"),
            new NovaAudioPlayer("fx_trails_2"),
            new NovaAudioPlayer("fx_trails_2"),
            new NovaAudioPlayer("fx_trails_2"),
            new NovaAudioPlayer("fx_trails_2"),
        };

        public ParticleEffectsTrigger RareFx;
        public ParticleEffectsTrigger EpicFx;
        public ParticleEffectsTrigger LegendaryFx;
        public ParticleEffectsTrigger RotateFx;

        [SerializeField] protected float DealInterval;
        [SerializeField] protected float _DealDuration;
        [SerializeField] protected float _FlipDelay;
        [SerializeField] protected float _FlipInterval;
        [SerializeField] protected float _FlipDuration;

        [SerializeField] public CanvasGroup _Pack;
        [SerializeField] public Image _BasicPack;
        [SerializeField] public Image _SmartPack;
        [SerializeField] public Image _MegaPack;


        [SerializeField] public RewardWidgetView DailyRewardWidget;
        [SerializeField] public GenericRewardWidgetView GenericRewardWidget;
        [SerializeField] public GameObject FirstRewardPanel;
        public RectTransform TopPanel;
        public RectTransform BottomPanel;
        private float topPanelAnchorPosY;
        private float bottomPanelAnchorPosY;

        // have wider nodes & zoom scales for 16x9? 6 could be 1-2-2-1 rather than 2-2-2
        // 1-2-3 right across middle, scale to fit
        // 5 could be max, with "done in bottom middle?
        // 4x3 is tablet, so optimize size for 16x9, and scale down as needed.

        /*private Vector2[] OldSixNodes = new Vector2[]
        {
            new Vector2(-450,250),
            new Vector2(0,300),
            new Vector2(450,250),
            new Vector2(450,-250),
            new Vector2(0,-300),
            new Vector2(-450,-250)
        };*/

        private Vector2[] SixNodes = new Vector2[]
        {
            new Vector2(-675,0),
            new Vector2(-225,270),
            new Vector2(225,270),
            new Vector2(675,0),
            new Vector2(-225,-270),
            new Vector2(225,-270)
        };
        private Vector2[] FiveNodes = new Vector2[]
        {
            new Vector2(-500,270),
            new Vector2(0,270),
            new Vector2(500,270),
            new Vector2(250,-270),
            new Vector2(-250,-270)
        };
        private Vector2[] FourNodes = new Vector2[]
        {
            new Vector2(-700,0),
            new Vector2(-225,0),
            new Vector2(225,0),
            new Vector2(700,0)
        };
        private Vector2[] ThreeNodes = new Vector2[]
        {
            new Vector2(-600,0),
            new Vector2(0,0),
            new Vector2(600,0)
        };
        private Vector2[] TwoNodes = new Vector2[]
        {
            new Vector2(-400,0),
            new Vector2(400,0)
        };
        private Vector2[] OneNode = new Vector2[]
        {
            new Vector2(0,0),
        };


        /*
         * Need to:
         * Animate the nodes into position (right number, right positions)
         * Ignore old nodes
         * Test the SHIT out of this.
         */

        private float prizeScale = 1.6f;
        private bool isWaitingForDoneButton = false;


        public Button ShareButton;
        public GameObject ShareButtonPanel;
        public NativeShare SharingPlugin;

        protected List<PoolableMonoBehaviour> _PrizeViews = new List<PoolableMonoBehaviour>();
        protected List<Prize> Prizes = new List<Prize>();
        protected bool _IsDealing;

        public Action<ShareScreenShotParams> EventShareClicked;
        public System.Action OnDoneButton;
        private RectTransform zoomedReward;
        //private Vector3 cameraPosition;

        override protected void Start()
        {
            base.Start();
            this.ClickScrim.SetActive(false);
            this.MiddleScrim.DOFade(0.0f, 0.0f);
            this.topPanelAnchorPosY = this.TopPanel.anchoredPosition.y;
            this.bottomPanelAnchorPosY = this.BottomPanel.anchoredPosition.y;
            this.TopPanel.DOAnchorPosY(0,0);
            this.BottomPanel.DOAnchorPosY(0,0);
            //this.cameraPosition = GameObject.Find("CameraForUI").transform.position;
        }

        private void LateUpdate()
        {
            if(this.IsOpen && isWaitingForDoneButton)
            {
                bool isAllCardsFlipped = true;
                for (int i = 0; i < this.RewardNodes.Length && i < Prizes.Count; i++)
                {
                    if (this.RewardNodes[i].IsRevealed == false)
                    {
                        isAllCardsFlipped = false;
                    }
                }

                if (isAllCardsFlipped)
                {
                    isWaitingForDoneButton = false;
                    this.StartCoroutine(this.ShowDoneButton(0f));
                }
            }
        }

        /// <summary>
        /// Called by the click scrim that is enabled when a reward item is zoomed to the RevealDestination (front and center!)
        /// </summary>
        public void ScrimClicked()
        {
            if (DOTween.IsTweening("zooming") || this.zoomedReward == null) { return; }
            
            this.CardZoom.Play();

            this.zoomedReward.DOLocalMove(Vector3.zero, 0.25f)
                .OnComplete(() => {
                    this.ClickScrim.SetActive(false);
                    this.zoomedReward = null;
                });
            this.MiddleScrim.DOFade(0.0f, 0.25f);
            // Check to see if this is the last unrevealed reward and it's time to show the Done Button
            // or if all the remaining cards are common and already owend
            //this.StartCoroutine(this.WaitToShowDoneOrRevealButton(0.1f));
        }

        /// <summary>
        /// Invoked by the EventTrigger component attached to each of the destination points in the View. Each one is configured
        /// to pass its destination node to this method as a parameter.
        /// </summary>
        public void NodeClicked(Transform nodeClicked)
        {
            //if (this.numCardsDealing > 0) return;

            // Loop though all the destination nodes to see which one matches the nodeClicked
            for (int i = 0; i < this.DestinationNodes.Length - this.numCardsDealing; i++)
            {
                // If we find a match we know what card is clicked
                if (this.DestinationNodes[i] == nodeClicked)
                {
                    // Do the reward clicked behavior
                    this.RewardClicked(i);

                    // Exit the loop, we're done
                    break;
                }
            }
        }

        public void PointerEnter(Transform nodeHovered)
        {
            //Debug.LogFormat("Hovered:{0}", nodeHovered);
            // Loop though all the destination nodes to see which one matches the nodeClicked
            for (int i = 0; i < this.DestinationNodes.Length && i < this._PrizeViews.Count; i++)
            {
                // If we find a match we know what card is clicked
                if (this.DestinationNodes[i] == nodeHovered)
                {
                    RewardNode rewardNode = this.RewardNodes[i];
                    CardFrontController cardFront = this._PrizeViews[i] as CardFrontController;
                    if (cardFront != null)
                    {
                        if (rewardNode.IsRevealed == false)
                        {
                            cardFront.RarityGlowCanvasGroup.transform.DOScale(Vector3.one * 1.06f, 0.4f);
                            //cardFront.RarityGlowCanvasGroup.alpha = 0;
                            //cardFront.RarityGlowCanvasGroup.gameObject.SetActive(true);
                            //cardFront.RarityGlowCanvasGroup.DOFade(1.0f, 0.4f);
                        }
                        // Exit the loop, we're done
                        break;
                    }
                    UpdatedPrizeView prizeView = this._PrizeViews[i] as UpdatedPrizeView;
                    if(prizeView != null)
                    {
                        if (rewardNode.IsRevealed == false && (prizeView.AchievementWidget == null || prizeView.AchievementWidget.TweenSequence == null || !prizeView.AchievementWidget.TweenSequence.IsPlaying()))
                        {
                            prizeView.RarityGlowCanvasGroup.transform.DOScale(Vector3.one * 1.06f, 0.4f);
                            //prizeView.RarityGlowCanvasGroup.alpha = 0;
                            //prizeView.RarityGlowCanvasGroup.gameObject.SetActive(true);
                            //prizeView.RarityGlowCanvasGroup.DOFade(1.0f, 0.4f);
                            //prizeView.RarityGlowCanvasGroup.gameObject.SetActive(prizeView.CanvasGroup.interactable);
                        }
                        // Exit the loop, we're done
                        break;
                    }
                }
            }
        }

        public void PointerExit(Transform nodeUnhovered)
        {
            // Loop though all the destination nodes to see which one matches the nodeClicked
            for (int i = 0; i < this.DestinationNodes.Length && i < this._PrizeViews.Count; i++)
            {
                // If we find a match we know what card is clicked
                if (this.DestinationNodes[i] == nodeUnhovered)
                {
                    RewardNode rewardNode = this.RewardNodes[i];
                    if (rewardNode.IsRevealed == false)
                    {
                        CardFrontController cardFront = this._PrizeViews[i] as CardFrontController;
                        if (cardFront != null)
                        {
                            cardFront.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.4f);
                            //cardFront.RarityGlowCanvasGroup.DOFade(0.0f, 0.4f);
                        }
                        else
                        {
                            UpdatedPrizeView prize = this._PrizeViews[i] as UpdatedPrizeView;
                            if (prize != null)
                            {
                                prize.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.4f);
                                //prize.RarityGlowCanvasGroup.DOFade(0.0f, 0.4f);
                            }
                        }
                    }
                    // Exit the loop, we're done
                    break;
                }
            }
        }

        private void ZoomAndScrimReward(RectTransform rootTrans)
        {
            // Zoom the reward to center screen and add a scrim
            rootTrans.DOMoveX(RevealDestination.position.x, 0.25f).SetId("zooming").OnComplete(() => this.zoomedReward = rootTrans);//.SetEase(Ease.InQuad);
            rootTrans.DOMoveY(RevealDestination.position.y, 0.25f);//.SetEase(Ease.InQuad);
            rootTrans.DOMoveZ(RevealDestination.position.z, 0.25f).SetEase(Ease.OutCirc);
            this.MiddleScrim.DOFade(0.8f, 0.25f);

            // Store a reference to the zoomed reward
            

            this.CardZoom.Play();

            // Activate the click scrim so that it requires an additional click to hide the card
            this.ClickScrim.SetActive(true);
        }

        private void DoRevealTween(RewardNode rewardNode, CardFrontController cardFront, UpdatedPrizeView prizeView, string notificationText, Color notificationColor)
        {
            Vector3 revealDirection = this.RevealDestination.position - rewardNode.RewardItemRoot.position;
            revealDirection.Normalize();

            this.CardFlip.Play();

            // Rotate the reward so it's facing front (revealed)
            rewardNode.RewardItemRoot.DORotate(Vector3.zero, 0.2f).SetEase(Ease.InQuad);
            rewardNode.RewardImage.raycastTarget = false;

            Sequence seq = DOTween.Sequence();
            seq.Append(rewardNode.RewardItemRoot.DOLocalMove(revealDirection * 150, 0.3f).SetEase(Ease.OutBack));
            seq.Append(rewardNode.RewardItemRoot.DOLocalMove(Vector3.zero, 0.3f).SetEase(Ease.OutBack));
            if (cardFront != null)
                seq.AppendCallback(() => cardFront.EnqueueScrollingText(notificationText, notificationColor));
            else if (prizeView != null)
            {
                seq.AppendCallback(() => {
                    prizeView.ScrollText();
                });
            }
            seq.AppendCallback(() => {
                rewardNode.RewardImage.raycastTarget = true;
            });
            seq.Play();

            // Check to see if this is the last unrevealed reward and it's time to show the Done Button
            // or if all the remaining cards are common and already owend
            //;
        }

        /// <summary>
        ///	When a given reward is clicked, this method produces the correct outcomes based on the rarity of card, how many are owned,
        /// and how many times it's been clicked. It does this by tracking state about the reward (card) being displayed and the 
        /// number of times it's been clicked in RewardNode.
        /// </summary>
        private void RewardClicked(int i)
        {
            if (DOTween.IsTweening("zooming") || RewardNodes[i].RewardItemRoot.childCount == 0) { return; }
            
            if(i >= _PrizeViews.Count || i < 0)
            {
                /*StringBuilder builder = new StringBuilder();
                for(int k = 0; k < RewardNodes.Length; k++)
                {
                    var node = RewardNodes[k];
                    builder.Append(k).Append(": revealed:").Append(node.IsRevealed).Append(" revealComplete:").Append(node.IsRevealComplete).Append(" Children:").Append(node.RewardItemRoot.childCount).Append(' ');
                }
                var str = string.Format("OpenPrizeView.RewardNodeClicked - index:{0} RewardNodes:{1} Prizes:{2} Nodes:{3}", i, RewardNodes.Count(), _PrizeViews.Count, builder.ToString());
                this.MetricsTrackingService.SavedException = string.Format("[CAUGHT] OpenPrizeView - exception: {0}", str);
                Debug.LogFormat(str);*/
                i = -1;
                for(int j = 0; j < _PrizeViews.Count && j < RewardNodes.Length; j++)
                {
                    if(RewardNodes[j] != null && !RewardNodes[j].IsRevealed)
                    {
                        i = j;
                        break;
                    }
                }
                if (i == -1)
                    return;
            }
            RewardNode rewardNode = this.RewardNodes[i];
            // Get some references to objects we're going to be operating on
            CardFrontController cardFront = _PrizeViews[i] as CardFrontController;
            UpdatedPrizeView prizeView = _PrizeViews[i] as UpdatedPrizeView;
            if (prizeView != null && !prizeView.CanvasGroup.interactable)
            {
                // this is a click on an animating prize. ignore it.
                rewardNode.RewardItemRoot.transform.DOScale(Vector3.one * 1.1f, 0.25f).OnComplete(() => rewardNode.RewardItemRoot.transform.DOScale(Vector3.one, 0.25f));
            }
            else if (cardFront != null || prizeView != null)
            {
                RectTransform rootTrans = rewardNode.RewardItemRoot;

                Color notificationColor;
                string notificationText = string.Empty;
                if (cardFront != null)
                {
                    switch (cardFront.CardData.Rarity)
                    {
                        case CardRarity.Rare:
                            notificationColor = cardFront.RareColor.BurnColor;
                            break;
                        case CardRarity.Epic:
                            notificationColor = cardFront.EpicColor.GlowColor;
                            break;
                        case CardRarity.Legendary:
                            notificationColor = cardFront.LegendaryColor.BurnColor;
                            break;
                        case CardRarity.Common:
                        default:
                            notificationColor = cardFront.CommonColor.BurnColor;
                            break;
                    }

                    int owned;
                    if (Message != null && Message.GameFormat == GameFormat.Tutorial)
                        owned = 1;
                    else
                        PlayerProfile.OwnedCards.TryGetValue(cardFront.CardData.CardID, out owned);
                    switch (owned)
                    {
                        case 1:
                            switch (cardFront.CardData.Rarity)
                            {
                                case CardRarity.Common:
                                    notificationText = I2.Loc.ScriptLocalization.New_Common;
                                    break;
                                case CardRarity.Rare:
                                    notificationText = I2.Loc.ScriptLocalization.New_Rare;
                                    break;
                                case CardRarity.Epic:
                                    notificationText = I2.Loc.ScriptLocalization.New_Epic;
                                    break;
                                case CardRarity.Legendary:
                                    notificationText = I2.Loc.ScriptLocalization.New_Legendary;
                                    break;
                            }
                            break;
                        case 2:
                            notificationText = I2.Loc.ScriptLocalization.Second_Card;
                            break;
                        case 3:
                            notificationText = I2.Loc.ScriptLocalization.Third_Card;
                            break;
                        default:
                            break;
                    }
                    if (cardFront.PrizeData != null && cardFront.PrizeData.PrizeSource == PrizeSource.onboardingCard && PlayerProfile.OnboardingProgress != null)
                    {
                        switch (cardFront.CardData.Aspect)
                        {
                            case CardAspect.Arcane:
                                notificationText = string.Format(I2.Loc.ScriptLocalization.OnboardingGrantNotification,
                                    PlayerProfile.OnboardingProgress.ArcaneRewardGranted,
                                    NBEconomy.MAX_ASPECT_REWARDS,
                                    I2.Loc.ScriptLocalization.Arcane);
                                break;
                            case CardAspect.Tech:
                                notificationText = string.Format(I2.Loc.ScriptLocalization.OnboardingGrantNotification,
                                    PlayerProfile.OnboardingProgress.TechRewardGranted,
                                    NBEconomy.MAX_ASPECT_REWARDS,
                                    I2.Loc.ScriptLocalization.Tech);
                                break;
                            case CardAspect.Divine:
                                notificationText = string.Format(I2.Loc.ScriptLocalization.OnboardingGrantNotification,
                                    PlayerProfile.OnboardingProgress.DivineRewardGranted,
                                    NBEconomy.MAX_ASPECT_REWARDS,
                                    I2.Loc.ScriptLocalization.Divine);
                                break;
                            case CardAspect.Nature:
                                notificationText = string.Format(I2.Loc.ScriptLocalization.OnboardingGrantNotification,
                                    PlayerProfile.OnboardingProgress.NatureRewardGranted,
                                    NBEconomy.MAX_ASPECT_REWARDS,
                                    I2.Loc.ScriptLocalization.Nature);
                                break;
                            case CardAspect.Chaos:
                                notificationText = string.Format(I2.Loc.ScriptLocalization.OnboardingGrantNotification,
                                    PlayerProfile.OnboardingProgress.ChaosRewardGranted,
                                    NBEconomy.MAX_ASPECT_REWARDS,
                                    I2.Loc.ScriptLocalization.Chaos);
                                break;
                        }
                    }
                }
                else
                {
                    notificationText = string.Format("+{0} {1}", prizeView.Data.Quantity, CurrencyUtils.GetName(prizeView.Data.CurrencyCode, prizeView.Data.Quantity));
                    switch (prizeView.Data.CurrencyCode)
                    {
                        case CurrencyType.CR:
                            notificationColor = prizeView.CreditsColor.BurnColor;
                            break;
                        case CurrencyType.NG:
                        case CurrencyType.SP:
                        case CurrencyType.QP:
                            notificationColor = prizeView.NovaGemColor.BurnColor;
                            break;
                        case CurrencyType.NC:
                            notificationColor = prizeView.NanoBotColor.BurnColor;
                            break;
                        case CurrencyType.FC:
                        case CurrencyType.FD:
                        case CurrencyType.FT:
                            notificationColor = prizeView.FreeEventColor.BurnColor;
                            break;
                        default:
                            notificationColor = Color.white;// prizeView.ItemColor.BurnColor;
                            notificationText = string.Empty;
                            break;
                    }
                }

                if (rewardNode.IsRevealed == false)
                {
                    // Mark the item as revealed
                    rewardNode.IsRevealed = true;

                    // Kill any rotation on the card
                    rootTrans.DOKill();

                    // Clicks on unrevealed rare cards (or rarer)
                    Sequence seq = DOTween.Sequence();

                    seq.AppendCallback(() =>
                    {
                    // play a celebratory sound
                    this.RevealRarity.Play();

                    // Trigger sparkly particles on click that are specific to each rarity type
                    this.TriggerParticles(cardFront == null ? CardRarity.Legendary : cardFront.CardData.Rarity, rootTrans);
                    });
                    seq.AppendInterval(0.1f);
                    seq.AppendCallback(() =>
                    {
                    // Fancy reveal for rare cards
                    this.DoRevealTween(rewardNode, cardFront, prizeView, notificationText, notificationColor);
                    });
                    if (cardFront != null)
                    {
                        seq.InsertCallback(0.2f, () =>
                        {
                        // Halfway though the rotation tween, switch the shape of the alpha mask to match
                        // the type of card it is.
                        cardFront.RarityGlow1.sprite = cardFront.CardType == CardType.Power ? cardFront.PowerAlphaMask : cardFront.UnitAlphaMask;
                            cardFront.RarityGlow2.sprite = cardFront.CardType == CardType.Power ? cardFront.PowerAlphaMask : cardFront.UnitAlphaMask;
                            cardFront.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.4f);
                            cardFront.RarityGlowCanvasGroup.gameObject.SetActive(true);
                        //cardFront.RarityGlowCanvasGroup.DOFade(1.0f, 0.4f);
                    });
                    }
                    else
                    {
                        seq.InsertCallback(0.2f, () =>
                        {
                            prizeView.RarityGlowCanvasGroup.transform.localPosition = new Vector3(0.0f, -0.5f, 0.0f);
                            prizeView.RarityGlow1.sprite = prizeView.PrizeAlphaMask;
                            prizeView.RarityGlow2.sprite = prizeView.PrizeAlphaMask;
                            prizeView.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.4f);
                            prizeView.RarityGlowCanvasGroup.gameObject.SetActive(true);
                        //prizeView.RarityGlowCanvasGroup.DOFade(1.0f, 0.4f);
                        prizeView.SetTypeGlowColor(prizeView.Data.CurrencyCode, true);
                        });
                    }
                    //if(!string.IsNullOrEmpty(NotificationText))
                    //    seq.InsertCallback(0.2f, ()=> );
                    seq.InsertCallback(0.2f, () =>
                    {
                    // Play a celebratory sound at the right time
                    this.RevealCard.Play();
                    });
                    seq.AppendCallback(() => rewardNode.IsRevealComplete = true);
                    seq.Play();

                }
                // Clicks on revealed reward 
                else if (cardFront != null && rewardNode.IsRevealComplete)
                {
                    // Always Zoom the reward to center screen and add a scrim
                    // When clicking on revealed cards
                    this.ZoomAndScrimReward(rootTrans);
                }
            }
        }

        ///<summary>
        /// Utility method that takes a rarity and a transform and triggers the correct particles attached to the "root"
        /// specified in the paramter List
        ///</summary>
        private void TriggerParticles(CardRarity rarity, RectTransform root)
        {
            ParticleEffectsTrigger particles = null;
            switch (rarity)
            {
                case CardRarity.Rare:
                    particles = this.RareFx;
                    break;
                case CardRarity.Epic:
                    particles = this.EpicFx;
                    break;
                case CardRarity.Legendary:
                    particles = this.LegendaryFx;
                    break;
            }

            if (particles != null)
            {
                particles.gameObject.SetActive(true);
                particles.gameObject.transform.SetParent(root);
                particles.gameObject.transform.localPosition = Vector3.zero;
                particles.StartEffect();
            }
        }

        private void TriggerRotateParticles(RectTransform root)
        {
            ParticleEffectsTrigger particles = null;
            particles = this.RotateFx;

            if (particles != null)
            {
                particles.gameObject.SetActive(true);
                particles.gameObject.transform.SetParent(root);
                particles.gameObject.transform.localPosition = Vector3.zero;
                particles.StartEffect();
            }
        }


        /// <summary>
        /// Rotates and shakes a given rewardNode with an intensity derived from its rarity (shakeRarity)
        /// </summary>
        private void RotateAndShake(RewardNode rewardNode, CardRarity shakeRarity)
        {
            float rarityVal = (float)shakeRarity;
            float loopTime = 1.5f - rarityVal * 0.2f;

            RectTransform rootTrans = rewardNode.RewardItemRoot;

            rootTrans.DOMoveX(rootTrans.position.x + 0.5f, loopTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
            rootTrans.DOMoveY(rootTrans.position.y + 0.5f, loopTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine).SetDelay(loopTime * 0.5f);

            //float rotateAmount = rarityVal * 3.0f + 2.0f;
            //rootTrans.rotation = Quaternion.Euler(new Vector3(0,rootTrans.rotation.eulerAngles.y, -rotateAmount * 0.5f));
            //rootTrans.DORotate(new Vector3(0,rootTrans.rotation.eulerAngles.y, rootTrans.rotation.eulerAngles.z + rotateAmount), 0.05f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        }
        
        private int numCardsDealing = 0;
        public void Play(PlayerProfile playerProfile)
        {
            this.LegendaryFx.gameObject.SetActive(false);
            this.EpicFx.gameObject.SetActive(false);
            this.RareFx.gameObject.SetActive(false);

            this.numCardsDealing = 0;

            float delayBetweenCards = this.DealInterval;
            int count = Mathf.Min(_PrizeViews.Count, DestinationNodes.Length);
            int i = 0;
            for (i = 0; i < count; i++)
            {
                var prizeView = _PrizeViews[i] as UpdatedPrizeView;
                if (prizeView != null)
                {
                    prizeView.CollectButtonClicked = RewardClicked;
                    prizeView.NodeNumber = i;
                }
                _PrizeViews[i].gameObject.SetActive(true);
                var prize = _PrizeViews[0] as UpdatedPrizeView;
                if(count != 1 || prize == null || prize.Data == null || prize.Data.PrizeId != "tech_starter")
                {
                    this.DealCard(i,  i * delayBetweenCards, count * delayBetweenCards);
                }
                else
                {
                    prize.CanvasGroup.alpha = 0;
                }
                this.RewardNodes[i].Reset();
                if (playerProfile.OwnedCards == null)
                {
                    this.RewardNodes[i].NumOwned = 0;
                }
                else
                {
                    int owned;
                    var card = _PrizeViews[i] as CardFrontController;
                    if (card != null)
                    {
                        playerProfile.OwnedCards.TryGetValue(card.CardData.CardID, out owned);
                        this.RewardNodes[i].NumOwned = owned;
                        Debug.LogFormat("{0} owned:{1}", _PrizeViews[i].gameObject.name, owned);
                    }
                }
            }
            isWaitingForDoneButton = true;
            if (_PrizeViews.Count == 1)
            {
                var prize = _PrizeViews[0] as UpdatedPrizeView;
                if(prize != null && prize.Data != null && prize.Data.PrizeId == "tech_starter")
                {
                    FirstRewardPanel.gameObject.SetActive(true);
                    this.ShowIntroAnimation(playerProfile.GameData);
                }
            }
            /*
            List<CardData> cardData = null;
            var deck = GameData.PreconstructedDecks.Where(d => d.Aspects.Contains(CardAspect.Tech)).FirstOrDefault();
            if (deck != null)
            {
                cardData = new List<CardData>();
                foreach(var card in deck.Cards)
                {
                    cardData.Add(GameData.CardDictionary[card.CardID]);
                }
                cardData.OrderBy(c => UnityEngine.Random.Range(0, 10000));
            }*/
        }

        [ContextMenu("Intro...")]
        private void ShowIntroAnimation(GameData gameData, IEnumerable<CardData> cardData = null)
        {
            this.FirstRewardPanel.gameObject.SetActive(true);
            List<CardFrontController> cards = new List<CardFrontController>();

            int numberOfCards = 30;
            float gap = 0.1f;

            Dictionary<int, NovaBlitz.UI.CardData> dict = GameData.CardDictionary;
            int angle = (int)UnityEngine.Random.Range(0, 360);
            if (cardData == null || cardData.Count() == 0)
                cardData = RandomValues(dict).Take(numberOfCards);
            else
                numberOfCards = cardData.Count();
            foreach (CardData value in cardData)
            {
                CardFrontController cview = gameData.ArenaCardPrefab.Spawn();
                cview.Initialize(value, "default");
                cards.Add(cview);
                cview.transform.SetParent(this.FirstRewardPanel.transform);
                cview.transform.localScale = Vector3.one;
                cview.Canvas.sortingOrder = 1000;
                float x = Mathf.Sin(Mathf.Deg2Rad * angle) * 130;
                float y = Mathf.Cos(Mathf.Deg2Rad * angle) * 130;
                //Debug.LogFormat("Angle = {0}, x:{1} y:{2}", angle, x, y);
                angle += UnityEngine.Random.Range(45, 315);
                //if(angle > 3)
                cview.transform.DOLocalMove(new Vector3(x, y,300),0);
                cview.transform.DORotate(new Vector3(0,-180f,0),0);
                cview.transform.localScale = Vector3.one * 0.25f;
            }

            this.TopPanel.DOAnchorPosY(this.topPanelAnchorPosY, 0.4f).SetEase(Ease.OutQuad);

            Sequence seq = DOTween.Sequence();
            float currentInsertTime = 0.0f;
            for(int i = 0; i < numberOfCards; i++)
            {
                var card = cards[i];
                seq.Insert(currentInsertTime, card.CanvasGroup.DOFade(1.0f, 0.2f));
                seq.Insert(currentInsertTime, ((RectTransform)card.transform).DOLocalMoveZ(-1700f,1.4f).SetEase(Ease.InCubic));//.DOAnchorPos3D(new Vector3(0, 0, -1600f), 2.0f));
                seq.Insert(currentInsertTime, card.transform.DOScale(Vector3.one, 5.2f));
                seq.Insert(currentInsertTime + 0.3f, card.transform.DOLocalRotate(Vector3.zero, 0.3f));
                seq.InsertCallback(currentInsertTime + 0.2f, () => {
                    this.TriggerRotateParticles((RectTransform)card.transform);
                });
                seq.InsertCallback(currentInsertTime + 0.6f, () => card.CardGlowAnimator.SetBool("IsPlayable", true));
                currentInsertTime += gap;
            }

            /*seq.Append(cards[0].CanvasGroup.DOFade(1.0f,0.2f));
            seq.Join(((RectTransform)cards[0].transform).DOAnchorPos3D(new Vector3(400,400,-1600f), 2.0f));
            seq.Join(cards[0].transform.DOScale(Vector3.one, 5.2f));
            seq.Insert(0.5f,cards[0].transform.DOLocalRotate(Vector3.zero, 0.3f));
            seq.InsertCallback(0f, ()=>{
                this.TriggerRotateParticles((RectTransform)cards[0].transform);
            });
            seq.InsertCallback(0.7f, ()=>cards[0].CardGlowAnimator.SetBool("IsPlayable", true));

            seq.Insert(1.0f,cards[1].CanvasGroup.DOFade(1.0f,0.2f));
            seq.Insert(1.0f,((RectTransform)cards[1].transform).DOAnchorPos3D(new Vector3(1300f,525f,-1600f), 2.0f));
            seq.Insert(1.0f,cards[1].transform.DOScale(Vector3.one, 5.2f));
            seq.Insert(1.4f,cards[1].transform.DOLocalRotate(Vector3.zero, 0.3f));
            seq.InsertCallback(1.1f, ()=>{
                this.TriggerRotateParticles((RectTransform)cards[1].transform);
            });
            seq.InsertCallback(1.6f, ()=>cards[1].CardGlowAnimator.SetBool("IsPlayable", true));

            seq.Insert(1.5f,cards[2].CanvasGroup.DOFade(1.0f,0.2f));
            seq.Insert(1.5f,((RectTransform)cards[2].transform).DOAnchorPos3D(new Vector3(1150f,800f,-1600f), 2.0f));
            seq.Insert(1.5f,cards[2].transform.DOScale(Vector3.one, 5.2f));
            seq.Insert(1.9f,cards[2].transform.DOLocalRotate(Vector3.zero, 0.3f));
            seq.InsertCallback(1.6f, ()=>{
                this.TriggerRotateParticles((RectTransform)cards[2].transform);
            });
            seq.InsertCallback(2.1f, ()=>cards[2].CardGlowAnimator.SetBool("IsPlayable", true));

            seq.Insert(2.0f,cards[3].CanvasGroup.DOFade(1.0f,0.2f));
            seq.Insert(2.0f,((RectTransform)cards[3].transform).DOAnchorPos3D(new Vector3(920,300,-1600f), 2.0f));
            seq.Insert(2.0f,cards[3].transform.DOScale(Vector3.one, 5.2f));
            seq.Insert(2.4f,cards[3].transform.DOLocalRotate(Vector3.zero, 0.3f));
            seq.InsertCallback(2.1f, ()=>{
                this.TriggerRotateParticles((RectTransform)cards[3].transform);
            });
            seq.InsertCallback(2.6f, ()=>cards[3].CardGlowAnimator.SetBool("IsPlayable", true));

            seq.Insert(2.5f,cards[4].CanvasGroup.DOFade(1.0f,0.2f));
            seq.Insert(2.5f,((RectTransform)cards[4].transform).DOAnchorPos3D(new Vector3(750,600,-1600f), 2.0f));
            seq.Insert(2.5f,cards[4].transform.DOScale(Vector3.one, 5.2f));
            seq.Insert(2.9f,cards[4].transform.DOLocalRotate(Vector3.zero, 0.3f));
            seq.InsertCallback(2.6f, ()=>{
                this.TriggerRotateParticles((RectTransform)cards[4].transform);
            });
            seq.InsertCallback(3.1f, ()=>cards[4].CardGlowAnimator.SetBool("IsPlayable", true));
            currentInsertTime = 2.5f;*/

            seq.InsertCallback(currentInsertTime + 1.0f, ()=>this.DealCard(0,0,0.2f));
            seq.InsertCallback(currentInsertTime + 1.5f, ()=>{
                this.BottomPanel.DOAnchorPosY(this.bottomPanelAnchorPosY, 0.4f).SetEase(Ease.OutQuad);
                });
            seq.OnComplete(()=>{
                this.RotateFx.transform.SetParent(this.transform);
                foreach(CardFrontController card in cards)
                {
                    card.ResetGlows();
                    card.Recycle();
                }
            });
            seq.SetDelay(0.4f);
        }

        public IEnumerable<TValue> RandomValues<TKey, TValue>(IDictionary<TKey, TValue> dict)
        {
            System.Random rand = new System.Random();
            List<TValue> values = Enumerable.ToList(dict.Values);
            int size = dict.Count;
            while(true)
            {
                yield return values[rand.Next(size)];
            }
        }

        private IEnumerator ShowDoneButton(float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            this.DoneButtonCanvasGroup.DOFade(1.0f, 1.0f);
            this.DoneButtonCanvasGroup.interactable = true;
            this.DoneButtonCanvasGroup.blocksRaycasts = true;
            this.DoneButton.transform.DOMoveX(375f, 0.5f).From(); // from offscreen right

            this.ShareButtonCanvasGroup.DOFade(1.0f, 1.0f);
            this.ShareButtonCanvasGroup.interactable = true;
            this.ShareButtonCanvasGroup.blocksRaycasts = true;
            this.ShareButton.transform.DOMoveX(-375f, 0.5f).From(); // from offscreen left
            //_Pack.gameObject.SetActive(false);
        }

        public void Initialize()
        {
            this.ShareButtonPanel.gameObject.SetActive(false);
            this.GenericRewardWidget.gameObject.SetActive(false);
            this.DailyRewardWidget.gameObject.SetActive(false);
            this.FirstRewardPanel.gameObject.SetActive(false);

            this.DoneButtonCanvasGroup.alpha = 0;
            this.DoneButtonCanvasGroup.interactable = false;
            this.DoneButtonCanvasGroup.blocksRaycasts = false;
            this.ShareButtonCanvasGroup.alpha = 0;
            this.ShareButtonCanvasGroup.interactable = false;
            this.ShareButtonCanvasGroup.blocksRaycasts = false;
            _Pack.gameObject.SetActive(false);
        }

        public void Populate(List<Prize> prizeList, IEnumerable<CardData> cards, UpdatedPrizeView prizeViewPrefab, CardFrontController arenaCardPrefab, 
            string cardBack, GameData gameData, PlayerProfile playerProfile, PrizeViewType prizeViewType, GameOverMessage message)
        {
            Message = message;
            PrizeViewType = prizeViewType;
            PlayerProfile = playerProfile;
            RecyclePrizes();
            if (null == cards || null == prizeList || prizeList.Count + cards.Count() <= 0)
            {
                Debug.Log("pack or cards are null.");
                return;
            }
            Prizes = prizeList;//.Take(5).ToList();

            Vector2[] destinations;
            switch(Prizes.Count)
            {
                case 1:
                    destinations = OneNode;
                    prizeScale = 2.25f;
                    break;
                case 2:
                    destinations = TwoNodes;
                    prizeScale = 2.25f;
                    break;
                case 3:
                    destinations = ThreeNodes;
                    prizeScale = 2.25f;
                    break;
                case 4:
                    destinations = FourNodes;
                    prizeScale = 2.0f;
                    break;
                case 5:
                    destinations = FiveNodes;
                    prizeScale = 1.7f;
                    break;
                case 6:
                    destinations = SixNodes;
                    prizeScale = 1.7f;
                    break;
                default:
                    while (Prizes.Count > 6)
                    {
                        Debug.LogErrorFormat("{0} prizes. Removed {1}", Prizes.Count, Prizes.Last().PrizeId);
                        Prizes.Remove(Prizes.Last());
                    }
                    destinations = SixNodes;
                    prizeScale = 1.7f;
                    break;
            }
            
            OnRectTransformDimensionsChange();

            for (int i = 0; i < DestinationNodes.Count(); i++)
            {
                var node = DestinationNodes[i];
                if (i >= destinations.Count())
                {
                    node.gameObject.SetActive(false);
                }
                else
                {
                    node.anchoredPosition = destinations[i];
                    node.gameObject.SetActive(true);
                    node.transform.localScale = Vector3.one * prizeScale;
                }
            }

            foreach (var text in PrizeTextNodes)
            {
                text.GetComponent<CanvasGroup>().alpha = 0;
            }

            for(int i = 0; i < Prizes.Count; i++)
            {
                Prize prize = Prizes[i];
                var cdata = cards.FirstOrDefault(c => c.ItemId == prize.PrizeId);
                if (cdata != null)
                {
                    // Spawn
                    CardFrontController cview = arenaCardPrefab.Spawn();
                    cview.Initialize(cdata, cardBack);
                    cview.PrizeData = prize;

                    cview.gameObject.SetActive(false);
                    _PrizeViews.Add(cview);
                    cview.transform.SetParent(CardLayout, true);
                    cview.CanvasGroup.alpha = 0;
                    cview.transform.localScale = Vector3.one * prizeScale;
                    cview.transform.rotation = Quaternion.Euler(0, 180, 0);
                    cview.transform.localPosition = Vector3.zero;
                    cview.gameObject.name = string.Format("{0} (Clone)", cdata.Name);

                    Debug.LogFormat("Card Prize initialized at {0}/{1}: {2}", i, Prizes.Count, cview.CardIdAndName);
                }
                else
                {
                    UpdatedPrizeView prizeView = prizeViewPrefab.Spawn();
                    prizeView.Initialize(prize, gameData, playerProfile, message);
                    _PrizeViews.Add(prizeView);
                    prizeView.transform.SetParent(CardLayout, true);
                    prizeView.CanvasGroup.alpha = 0;
                    prizeView.transform.localScale = Vector3.one * prizeScale;
                    prizeView.transform.rotation = Quaternion.Euler(0, 180, 0);
                    prizeView.transform.localPosition = Vector3.zero;
                    prizeView.gameObject.name = string.Format("{0} {1} (Clone)", prize.PrizeType, prize.PrizeId);

                    Debug.LogFormat("Prize initialized at {0}/{1}: {2}", i, Prizes.Count, prizeView.gameObject.name);
                }
            }
            
            _PrizeViews = _PrizeViews.OrderBy(c => UnityEngine.Random.value).ToList();


            switch (prizeViewType)
            {
                case PrizeViewType.DLC:
                    PopulateDailyRewards(playerProfile.OnboardingProgress, message.IsDailyRewards, true);
                    break;
                case PrizeViewType.DailyRewards:
                    PopulateDailyRewards(playerProfile.OnboardingProgress, message.IsDailyRewards, false);
                    break;
                case PrizeViewType.TournamentEnd:
                case PrizeViewType.SeasonEnd:
                    PopulateGenericRewards(playerProfile, prizeViewType, message);
                    break;
                case PrizeViewType.BasicPack:
                case PrizeViewType.SmartPack:
                case PrizeViewType.MegaPack:
                    OpenPack();
                    break;
                case PrizeViewType.GameEnd:
                case PrizeViewType.Achievement:
                default:
                    OnCollectClick();
                    break;
            }
        }

        public void OpenPack()
        {
            // tween in the pack - from the bottom & zoom in
            this._Pack.gameObject.SetActive(true);
            this._BasicPack.gameObject.SetActive(PrizeViewType == PrizeViewType.BasicPack);
            this._SmartPack.gameObject.SetActive(PrizeViewType == PrizeViewType.SmartPack);
            this._MegaPack.gameObject.SetActive(PrizeViewType == PrizeViewType.MegaPack);
            Sequence seq = DOTween.Sequence();
            seq.Insert(0.0f, _Pack.transform.DOMove(new Vector3(0, -200, 0), 1.5f).SetEase(Ease.OutBack).From());
            seq.Insert(0.0f, _Pack.transform.DOScale(new Vector3(0.2f, 0.2f, 0.2f), 1.5f).From());
            seq.Insert(0.0f, this._Pack.DOFade(1.0f, 0.0f));

            seq.AppendCallback(() => OnCollectClick());
        }

        public void OnCollectClick()
        {
            this.DailyRewardWidget.gameObject.SetActive(false);
            this.CardLayoutArrangement.gameObject.SetActive(true);
            this.GenericRewardWidget.gameObject.SetActive(false);
            this.Play(PlayerProfile);

#if UNITY_IOS || UNITY_ANDROID
            this.ShareButtonPanel.gameObject.SetActive(true);
#endif
        }


        public void PopulateDailyRewards(OnboardingProgress progress, bool isDailyRewards, bool isDLC)
        {
            DailyRewardWidget.Initialize(progress, isDailyRewards, isDLC);
            DailyRewardWidget.ThisCanvasGroup.alpha = 0;
            DailyRewardWidget.ThisCanvasGroup.DOFade(1.0f, 0.5f);
            DailyRewardWidget.gameObject.SetActive(true);
            this.CardLayoutArrangement.gameObject.SetActive(false);
        }

        public void PopulateGenericRewards(PlayerProfile playerProfile, PrizeViewType prizeViewType, GameOverMessage gameOverMessage)
        {
            GenericRewardWidget.Initialize(playerProfile, prizeViewType, gameOverMessage);
            GenericRewardWidget.ThisCanvasGroup.alpha = 0;
            GenericRewardWidget.ThisCanvasGroup.DOFade(1.0f, 0.5f);
            GenericRewardWidget.gameObject.SetActive(true);
            this.CardLayoutArrangement.gameObject.SetActive(false);
        }

        public void RecyclePrizes()
        {
            foreach (var view in this._PrizeViews)
            {
                view.Recycle();
            }
            this._PrizeViews.Clear();
        }

        
        void OnRectTransformDimensionsChange()
        {
            if (CardLayoutArrangement.rect.width / CardLayoutArrangement.rect.height < 1.6f)
            {
                float modification = 500f;
                CardLayoutArrangement.anchoredPosition3D = new Vector3(0, 0, modification);
                RevealDestination.DOLocalMoveZ(-(2250/ prizeScale) +575 - modification, 0.0f);
            }
            else
            {
                CardLayoutArrangement.anchoredPosition3D = new Vector3(0, 0, 0);
                RevealDestination.DOLocalMoveZ(-(2250 / prizeScale) + 575, 0.0f);
            }

        }

        /// <summary>
        /// Plays the deal animation for a card at a specific index in the card list with a specified delay
        /// before the animation starts playing
        /// </summary>
        protected void DealCard(int index, float delay, float interactableDelay)
        {
            this.numCardsDealing++;

            RectTransform cardTrans = (RectTransform)this._PrizeViews[index].transform;
            var node = this.RewardNodes[index];
            node.RewardImage.raycastTarget = false;
            node.RewardItemRoot.rotation = Quaternion.Euler(0, 180, 0);
            cardTrans.SetParent(node.RewardItemRoot);
            CardFrontController cardFront = this._PrizeViews[index] as CardFrontController;
            UpdatedPrizeView prize = this._PrizeViews[index] as UpdatedPrizeView;

            if(cardFront != null)
            {
                cardFront.SetRarityGlowColor(cardFront.CardData.Rarity);
                cardFront.RarityGlow1.sprite = cardFront.UnitAlphaMask;
                cardFront.RarityGlow2.sprite = cardFront.UnitAlphaMask;
                cardFront.RarityGlowCanvasGroup.DOFade(0.0f, 0.0f);
                cardFront.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.0f);
            }
            else if ( prize != null)
            {
                if (prize.Data.PrizeSource == PrizeSource.achievement || prize.Data.PrizeSource == PrizeSource.quest)
                {
                    prize.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.0f);
                    prize.RarityGlowCanvasGroup.transform.localPosition = new Vector3(0.0f, 40f, 0.0f);
                }
                else
                {
                    prize.RarityGlowCanvasGroup.transform.DOScale(Vector3.one, 0.0f);
                    prize.RarityGlowCanvasGroup.transform.localPosition = new Vector3(0.0f, -0.5f, 0.0f);
                    prize.RarityGlow1.sprite = prize.UnitAlphaMask;
                    prize.RarityGlow2.sprite = prize.UnitAlphaMask;
                }
                prize.RarityGlowCanvasGroup.DOFade(0.0f, 0.0f);

            }

            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(delay);
            seq.Append(cardFront != null ? cardFront.CanvasGroup.DOFade(1f, 0.05f) : prize.CanvasGroup.DOFade(1f, 0.05f));
            seq.Insert(delay, cardTrans.DOScale(0.4f, 0.2f).From());
            seq.Insert(delay, cardTrans.DOLocalMove(Vector3.zero, this._DealDuration).SetEase(Ease.OutCubic));
            seq.InsertCallback(delay, () => this.RevealCard.Play());// CardDeal[Math.Max(0, Math.Min(CardDeal.Length,index))].Play());
            seq.Append(this.PrizeTextNodes[index].GetComponent<CanvasGroup>().DOFade(1f, 1.0f));
            seq.InsertCallback(interactableDelay/* + this._DealDuration*/, () => {
                // All cards rotate slowly
                //this.CardFlip.Play();

                node.RewardImage.raycastTarget = true;
                if (cardFront != null)
                {
                    cardFront.RarityGlowCanvasGroup.gameObject.SetActive(true);
                    cardFront.RarityGlowCanvasGroup.DOFade(1.0f, 1.0f);
                    //cardFront.RarityGlow1.sprite = cardFront.UnitAlphaMask;
                    //cardFront.RarityGlow2.sprite = cardFront.UnitAlphaMask;
                }
                if (prize != null)
                {
                    prize.SetTypeGlowColor(prize.Data.CurrencyCode, false);
                    prize.RarityGlowCanvasGroup.gameObject.SetActive(true);
                    prize.RarityGlowCanvasGroup.DOFade(1.0f, 1.0f);
                }
                if (!node.IsRevealed)
                {
                    //Debug.LogWarningFormat("Rotating and shaking {0}", index);
                    this.RotateAndShake(node, CardRarity.Common);
                }
                this.numCardsDealing--;
                if(this.numCardsDealing == 0)
                {
                    this._Pack.DOFade(0.0f, 1.0f);
                }
            });
            seq.Play();
        }
        
        /// <summary>
        /// Called by the OnClick button handler of the Done NovaButton
        /// </summary>
        public void DoneButtonClicked()
        {
            this.LegendaryFx.EndEffect();
            this.EpicFx.EndEffect();
            this.RareFx.EndEffect();

            if (OnDoneButton != null)
            {
                OnDoneButton.Invoke();
            }
        }

        public void OnShareClick()
        {
            if (this.EventShareClicked != null)
            {
                this.EventShareClicked.Invoke(new ShareScreenShotParams(SharingPlugin, "", ShareButton, ShareButtonPanel));
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            foreach(var node in RewardNodes)
            {
                node.RewardItemRoot.DOKill();
            }
        }
    }
}
