﻿using System;
using System.Globalization;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using Messages;

namespace NovaBlitz.UI
{
    public class GenericRewardWidgetView : View
    {
        public Action CollectAction;

        // season views

        // game end views

        // tournament views

        public CanvasGroup ThisCanvasGroup;
        public PlayStatusWidget TournamentStatusWidget;
        public TextMeshProUGUI tournamentHeader;
        public TextMeshProUGUI tournamentFormat;
        public GameObject Tournament;

        public TextMeshProUGUI SeasonText;
        public TextMeshProUGUI SeasonHeader;
        public GameObject Season;
        
        private bool IsCollect;

        public void OnClickCollect()
        {
            if (CollectAction != null)
            {
                CollectAction.Invoke();
            }
        }

        public void Initialize(PlayerProfile playerProfile, PrizeViewType prizeViewType, GameOverMessage gameOverMessage)
        {
            Season.SetActive(prizeViewType == PrizeViewType.SeasonEnd);
            Tournament.SetActive(prizeViewType == PrizeViewType.TournamentEnd);

            switch (prizeViewType)
            {
                case PrizeViewType.SeasonEnd:
                    var datastring = string.Format(" old: {0} new: {1}", gameOverMessage.PrevMMR, gameOverMessage.CurrentMMR);
                    SeasonHeader.text = string.Format(
                            I2.Loc.ScriptLocalization.Season,
                            CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.UtcNow.AddMonths(-1).Month),
                            0,
                            0);
                    SeasonText.text = string.Format(I2.Loc.ScriptLocalization.Season_Prize_Awarded,
                        CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.UtcNow.AddMonths(-1).Month), 0, 0) + datastring;

                    // show:
                    // season rank attained
                    // tell players their rank will be reset (if it is reset)
                    break;
                case PrizeViewType.TournamentEnd:
                    switch(gameOverMessage.GameFormat)
                    {
                        case GameFormat.Draft:
                            tournamentHeader.text = I2.Loc.ScriptLocalization.DraftDraftComplete;
                            tournamentFormat.text = I2.Loc.ScriptLocalization.Draft;
                            break;
                        case GameFormat.Constructed:
                            tournamentHeader.text = I2.Loc.ScriptLocalization.ConstructedComplete;
                            tournamentFormat.text = I2.Loc.ScriptLocalization.League;
                            break;
                        default:
                            tournamentHeader.text = I2.Loc.ScriptLocalization.TournamentComplete;
                            tournamentFormat.text = I2.Loc.ScriptLocalization.Tournament;
                            break;
                    }
                    TournamentStatusWidget.Format = gameOverMessage.GameFormat;
                    TournamentStatusWidget.Wins = gameOverMessage.WinCount;
                    TournamentStatusWidget.Losses = gameOverMessage.LossCount;
                    break;
            }
        }
    }
}
