﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TMPro;
using Messages;

namespace NovaBlitz.UI
{
    public class ReconnectDialogView : AnimatedView
    {
        public Action<ReconnectAction, string> DialogDismissed; // bools isConfirmed
        public TextMeshProUGUI MessageLabel;
        public TextMeshProUGUI TitleLabel;
        public TextMeshProUGUI CountDownTimeLabel;
        public TextMeshProUGUI ReconnectButtonLabel;
        public Button ReconnectButton;
        public Button ExitButton;
        private IEnumerator countdown;

        public DisconnectCode DisconnectCode;
        private string Url = string.Empty;
        
        public void Initialize(DisconnectCode disconnectCode, int countDownTime)
        {
            this.gameObject.SetActive(true);
            Url = null;
            ReconnectButton.interactable = true;
            ExitButton.interactable = true;
            DisconnectCode = disconnectCode;
            ReconnectButton.gameObject.SetActive(true);

            switch (disconnectCode)
            {
                case DisconnectCode.Reason15:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Error.SteamAuth;
                    string errorMessage = string.IsNullOrEmpty(NovaSteamService.lastErrorCode) ? string.Empty : NovaSteamService.lastErrorCode;
                    MessageLabel.text = string.Format(I2.Loc.ScriptLocalization.Error.SteamAuthMessage, errorMessage, 0,0);
                    ReconnectButton.gameObject.SetActive(false);
                    break;
                case DisconnectCode.LoggedInElsewhere:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Logged_Out;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Disconnect.Logged_In_Elsewhere;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Reconnect;
                    break;
                case DisconnectCode.ClientDisconnectRequest:
                case DisconnectCode.UserLoggedOut:
                case DisconnectCode.PleaseLogOutAndBackInAgain:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Logged_Out;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Error.SessionTokenInvalidMessage;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Reconnect;
                    break;
                case DisconnectCode.ServerDownForMaintenance:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Not_Connected;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Disconnect.Down_For_Maintenance;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Info;
                    Url = "https://twitter.com/novablitz";
                    break;
                case DisconnectCode.DataParseFailed:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Not_Connected;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Disconnect.Game_Data_Parse_Failed;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Game_Support;
                    Url = "http://www.novablitz.com";
                    break;
                case DisconnectCode.MessagingError:
                case DisconnectCode.ClientOutOfDate:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Not_Connected;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Disconnect.Out_of_Date;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Update;
                    Url = "http://www.novablitz.com/game/download";
                    break;
                case DisconnectCode.BlockedUser:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Account_Blocked;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Contact_Game_Support;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Game_Support;
                    Url = "http://www.novablitz.com";
                    break;
                case DisconnectCode.NotConnected: // Not an error; a state during the reconnect to server process
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Not_Connected;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Reconnecting;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Reconnecting;
                    ReconnectButton.interactable = false;
                    ExitButton.interactable = false;
                    break;
                case DisconnectCode.Reconnecting: // Not an error; a state during the reconnect to server process
                    TitleLabel.text = I2.Loc.ScriptLocalization.Disconnect.Not_Connected;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Disconnect.ReconnectingIn;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Reconnect;
                    break;
                case DisconnectCode.PlayFabConnectionError:
                case DisconnectCode.NotLoggedIn:
                case DisconnectCode.ServerCrash:
                case DisconnectCode.NoCode:
                default:
                    TitleLabel.text = I2.Loc.ScriptLocalization.Error.ConnectionFailed;
                    MessageLabel.text = I2.Loc.ScriptLocalization.Error.ConnectionFailedMessage;
                    ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Disconnect.Reconnect;
                    break;

            }
            
            StopEnumerations();
            countdown = CountdownToNextReconnect(countDownTime);
            StartCoroutine(countdown);
        }

        private void StopEnumerations()
        {
            if (countdown != null)
                StopCoroutine(countdown);
            countdown = null;
            CountDownTimeLabel.gameObject.SetActive(false);
        }

        private IEnumerator CountdownToNextReconnect(int countDownTime)
        {
            int time = countDownTime;
            CountDownTimeLabel.gameObject.SetActive(true);
            while (time > 0)
            {
                CountDownTimeLabel.text = time.ToString();
                yield return new WaitForSecondsRealtime(1);
                time--;
            }
            StopEnumerations();
        }

        public void OnClickReconnect()
        {
            if (this.DialogDismissed != null)
            {
                StopEnumerations();
                // Do either reconnect or the url, if there is one.
                if(!string.IsNullOrEmpty(Url))
                    this.DialogDismissed.Invoke(ReconnectAction.OpenExternalLink, Url);
                else if (this.DisconnectCode == DisconnectCode.Reconnecting || this.DisconnectCode == DisconnectCode.NotConnected)
                    this.DialogDismissed.Invoke(ReconnectAction.ReconnectToServerOnly, string.Empty);
                else
                    this.DialogDismissed.Invoke(ReconnectAction.Reconnect, string.Empty);
            }
        }

        public void OnClickExit()
        {
            if (this.DialogDismissed != null)
            {
                StopEnumerations();
                if (!string.IsNullOrEmpty(Url))
                    this.DialogDismissed.Invoke(ReconnectAction.OpenExternalLink, Url);
                // Do both exit and the Url
                this.DialogDismissed.Invoke(ReconnectAction.Exit, string.Empty);
            }
        }
    }
}
