﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using System;

namespace NovaBlitz.UI.Mediators
{
    public class CardDetailsMediator : Mediator
    {
		[Inject] public CardDetailsView View {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal{get;set;}
		
		override public void OnRegister()
		{
			this.View.CloseDetails += this.OnCloseDetails;
		}
		
		override public void OnRemove()
		{
			this.View.CloseDetails -= this.OnCloseDetails;
			
		}

        private void OnCloseDetails()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(CardDetailsView));
			this.transform.SetAsFirstSibling();
        }
    }
}