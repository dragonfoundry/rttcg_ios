﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using PlayFab.ClientModels;
using ExitGames.Client.Photon.Chat;
using NovaBlitz.UI.DataContracts;
using System;

namespace NovaBlitz.UI.Mediators
{
    public class FriendListDockMediator : Mediator
    {
		[Inject] public FriendListDockView View {get;set;}

		[Inject] public PlayerProfile PlayerProfile {get;set;}
        
		[Inject] public ChatMessageAddedSignal ChatMessageAddedSignal {get;set;}
		[Inject] public SendChatMessageSignal SendChatMessageSignal {get;set;}

		[Inject] public FriendStatusUpdatedSignal FriendStatusUpdatedSignal {get;set;}
		[Inject] public AddFriendSignal AddFriendSignal{get;set;}
		[Inject] public AddFriendResponseSignal AddFriendResponseSignal {get;set;}

        [Inject] public ConfirmFriendSignal ConfirmFriendSignal { get; set; }
        [Inject] public ConfirmFriendResponseSignal ConfirmFriendResponseSignal { get; set; }
        [Inject] public RemoveFriendSignal RemoveFriendSignal {get;set;}
		[Inject] public RemoveFriendResponseSignal RemoveFriendResponseSignal{get;set;}
		[Inject] public BlockFriendSignal BlockFriendSignal {get;set;}
		[Inject] public BlockFriendResponseSignal BlockFriendResponseSignal {get;set;}
		[Inject] public UnblockFriendSignal UnblockFriendSignal { get;set;}
		[Inject] public UnblockFriendResponseSignal UnblockFriendResponseSignal { get;set;}
        [Inject] public DeclineFriendRequestSignal DeclineFriendRequestSignal { get; set; }
        [Inject] public DeclineFriendResponseSignal DeclineFriendResponseSignal { get; set; }
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public LogEventSignal LogEventSignal {get;set;}

        [Inject] public ChallengeFriendSignal ChallengeFriendSignal {get;set;}
		[Inject] public FriendChallengeSignal FriendChallengeSignal {get;set;}
		[Inject] public FriendChallengeCanceledSignal FriendChallengeCanceledSignal {get;set;}
		[Inject] public FriendChallengeDeclinedSignal FriendChallengeDeclinedSignal {get;set;}
		[Inject] public AcceptFriendChallengeSignal AcceptFriendChallengeSignal {get;set;}
		[Inject] public DeclineFriendChallengeSignal DeclineFriendChallengeSignal {get;set;}

        [Inject] public FriendNotificationSignal FriendNotificationSignal { get; set; }
        [Inject] public SaveChatHistorySignal SaveChatHistorySignal { get; set; }
        
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }

        [Inject] public ToFriendChallengeSignal ToFriendChallengeSignal { get; set; }

        [Inject] public HomeScreen HomeScreen { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public UIConfiguration UIConfiguration { get; set; }


        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister ()
		{
			this.View.EventChatOpened += this.OnChatOpened;
            this.View.EventChallengeClicked += this.OnFriendChallengeClicked;
            this.View.EventDeclineClicked += this.OnDeclineClicked;
			this.View.AddFriendClicked += this.OnAddFriendClicked;
			this.View.RemoveFriendClicked += this.OnRemoveFriendClicked;
			this.View.BlockFriendClicked += this.OnBlockFriendClicked;
			this.View.SubmitChatMessage += this.OnSubmitChatMessage;
            this.View.OnFriendInfoUpdated += this.OnDoFriendChatLog;



            this.View.ConfirmFriendRequestClicked += this.OnConfirmFriendRequestClicked;
            this.View.DeclineFriendRequestClicked += this.OnDeclineFriendRequestClicked;
            this.View.BlockFriendRequestClicked += this.OnBlockFriendRequestClicked;

            this.AddFriendResponseSignal.AddListener(OnAddFriendResponse);
            this.ConfirmFriendResponseSignal.AddListener(OnConfirmFriendResponse);
            this.DeclineFriendResponseSignal.AddListener(OnDeclineFriendResponse);
            this.BlockFriendResponseSignal.AddListener(OnBlockFriendResponse);
            this.UnblockFriendResponseSignal.AddListener(OnUnblockFriendResponse); 
            this.RemoveFriendResponseSignal.AddListener(OnRemoveFriendResponse);
            this.ChatMessageAddedSignal.AddListener(OnChatMessageAdded);
			this.FriendStatusUpdatedSignal.AddListener(OnFriendStatusUpdate);
			this.FriendChallengeSignal.AddListener(OnFriendChallenge);
			this.FriendChallengeCanceledSignal.AddListener(OnFriendChallengeCanceled);
			this.FriendChallengeDeclinedSignal.AddListener(OnFriendChallengeDeclined);
			this.AcceptFriendChallengeSignal.AddListener(OnAcceptChallenge);
			this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);

            this.View.ChatItemPrefab = UIConfiguration.GlobalChatPrefab;
            this.View.PlayerProfile = PlayerProfile;
			Timer.Instance.StartCoroutine(DelayUpdateFriendList ());
            // update the friend list on register

            //this.View.UpdateFriendList(this.PlayerProfile.FriendListData);

            //StartCoroutine(DelayUpdateFriendList());
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove ()
		{
			this.View.EventChatOpened -= this.OnChatOpened;
            this.View.EventChallengeClicked -= this.OnFriendChallengeClicked;
            this.View.EventDeclineClicked -= this.OnDeclineClicked;
			this.View.AddFriendClicked -= this.OnAddFriendClicked;
			this.View.RemoveFriendClicked -= this.OnRemoveFriendClicked;
			this.View.BlockFriendClicked -= this.OnBlockFriendClicked;
			this.View.SubmitChatMessage -= this.OnSubmitChatMessage;
            this.View.OnFriendInfoUpdated -= this.OnDoFriendChatLog;

            this.View.ConfirmFriendRequestClicked -= this.OnConfirmFriendRequestClicked;
            this.View.DeclineFriendRequestClicked -= this.OnDeclineFriendRequestClicked;
            this.View.BlockFriendRequestClicked -= this.OnBlockFriendRequestClicked;

            this.AddFriendResponseSignal.RemoveListener(OnAddFriendResponse);
            this.ConfirmFriendResponseSignal.RemoveListener(OnConfirmFriendResponse);
            this.DeclineFriendResponseSignal.RemoveListener(OnDeclineFriendResponse);
            this.BlockFriendResponseSignal.RemoveListener(OnBlockFriendResponse);
            this.UnblockFriendResponseSignal.RemoveListener(OnUnblockFriendResponse);
            this.RemoveFriendResponseSignal.RemoveListener(OnRemoveFriendResponse);
            this.ChatMessageAddedSignal.RemoveListener(OnChatMessageAdded);
			this.FriendStatusUpdatedSignal.RemoveListener(OnFriendStatusUpdate);
			this.FriendChallengeSignal.RemoveListener(OnFriendChallenge);
			this.FriendChallengeCanceledSignal.RemoveListener(OnFriendChallengeCanceled);
			this.FriendChallengeDeclinedSignal.RemoveListener(OnFriendChallengeDeclined);
			this.AcceptFriendChallengeSignal.RemoveListener(OnAcceptChallenge);
			this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
        }

		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
				this.View.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed, false);

				this.LogEventSignal.Dispatch (new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_FRIENDS, 
					IsTimedEvent = true, 
					IsTimedEventOver = false
				});
			}
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
				this.View.ChatSettingsToggle.isOn = false; 
				this.LogEventSignal.Dispatch (new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_FRIENDS, 
					IsTimedEvent = true, 
					IsTimedEventOver = true
				});
                this.SaveChatHistorySignal.Dispatch();
            }
        }

        protected void RefreshChallengePanel(string playfabID)
		{
			NovaBlitzFriend friend = this.PlayerProfile.GetFriend(playfabID);
			this.View.IsChallengeEnabled = null != friend && friend.ChatUserStatus == ChatUserStatus.Online;
			FriendChallenge incoming; this.PlayerProfile.FriendChallenges.TryGetValue(playfabID, out incoming);
			this.View.IsChallengeIncoming = null != incoming;
		}

        protected void OnLanguageChanged()
        {
            this.View.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed, false);
            //this.View.UpdateFriendList(this.PlayerProfile.FriendListData);

        }

        #region View Listeners

        protected void OnChatOpened(string playfabID)
		{
			RefreshChallengePanel(playfabID);
		}

        protected void OnFriendChallengeClicked(string playfabID)
        {
            NovaBlitzFriend friend = this.PlayerProfile.GetFriend(playfabID);
            if (null != friend && friend.ChatUserStatus == ChatUserStatus.Online)
            {
                this.PlayerProfile.SelectedChallengeID = playfabID;
                this.CloseAnimatedViewSignal.Dispatch(typeof(FriendListDockView));
                this.ToFriendChallengeSignal.Dispatch(playfabID);
            }
        }
        protected void OnDeclineClicked(string playfabID)
		{
			if (this.PlayerProfile.IsChallengeIncoming(playfabID)) {
				this.View.UpdateChallenge(playfabID, false);
				this.DeclineFriendChallengeSignal.Dispatch(playfabID);
			}
		}

		/// <summary>
		/// When the "[+]" button in the title of the friends list bar has been clicked
		/// </summary>
		private void OnAddFriendClicked(string titleDisplayName)
		{
			this.AddFriendSignal.Dispatch(titleDisplayName, false);
		}

        /// <summary>
        /// When the Confirm button is clicked - could be either to confirm a request, or to unblock a blocked request.
        /// </summary>
        private void OnConfirmFriendRequestClicked(string friendPlayfabId)
        {
            if (this.PlayerProfile.BlockList.Contains(friendPlayfabId))
                this.UnblockFriendSignal.Dispatch(friendPlayfabId);
            else
                this.ConfirmFriendSignal.Dispatch(friendPlayfabId);
        }

        /// <summary>
        /// When the Decline button is clicked
        /// </summary>
        private void OnDeclineFriendRequestClicked(string friendPlayfabId)
        {
            this.DeclineFriendRequestSignal.Dispatch(friendPlayfabId);
        }

        /// <summary>
        /// Handles when the "block" button is clicked, on friend confirm
        /// </summary>
        private void OnBlockFriendRequestClicked(string playfabId)
        {
            this.BlockFriendSignal.Dispatch(playfabId);
        }


        /// <summary>
        /// Handles when the user clicks remove on a friend 
        /// </summary>
        private void OnRemoveFriendClicked(string friendPlayfabId)
		{
/*#if UNITY_STEAM
            NovaBlitzFriend friend;
            if(PlayerProfile.FriendListData.TryGetValue(friendPlayfabId, out friend) && friend.FriendData.IsSteamFriend)
            {
                Application.OpenURL("steam://friends/");
            }
            else
#endif*/
                this.RemoveFriendSignal.Dispatch(friendPlayfabId);
		}
        
        /*private void OnReportFriendClicked(ChatMessage message)
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(ReportBugView),
                    (v) => { ((ReportBugView)v).Initialize(ReportType.ReportPlayerGlobal, message, this.PlayerProfile.GlobalChatLog); });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ReportBugView));
        }*/

        /// <summary>
        /// Handles when a user clicks Bock on a friend's chat log
        /// </summary>
        private void OnBlockFriendClicked(string playfabId)
        {
            this.BlockFriendSignal.Dispatch(playfabId);
        }

		/// <summary>
		/// chat message submitted though the UI
		/// </summary>
		private void OnSubmitChatMessage(string[] parameters)
		{
            AvatarDataContract avContract = null;
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.AvatarID))
                this.GameData.AvatarByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.AvatarID, out avContract);
            var chatMessage = new ChatMessage(this.PlayerProfile.MetaProfile.PlayFabID, avContract == null ? string.Empty : avContract.ArtId, this.PlayerProfile.MetaProfile.Name, parameters[1], true);
            this.SendChatMessageSignal.Dispatch(parameters[0], chatMessage,false);
		}

        private void OnDoFriendChatLog(FriendNotification notifications)
        {
            this.FriendNotificationSignal.Dispatch(notifications);
        }

#endregion

#region Signal Listeners
        /// <summary>
        /// Listener for the ChatMessageAdded signal, adds the message to the log if the correct log is visible
        /// </summary>
        private void OnChatMessageAdded(string friendPlafabId, ChatMessage chatMessage, bool isCombined)
		{
            //NovaBlitzFriend friend;
            //if (this.PlayerProfile.FriendListData.TryGetValue(friendPlafabId, out friend))
            //    chatMessage.SenderAvatarArtId = friend.AvatarArtId;
  			this.View.AddChatMessageToLog(friendPlafabId, chatMessage, isCombined);
		}

        /// <summary>
        /// Handles when a friend comes on or offline
        /// </summary>
        private void OnFriendStatusUpdate(string friendPlayfabId, int status)
        {
            this.View.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed, true);
        }

		/// <summary>
		/// Incoming challenge
		/// </summary>
		protected void OnFriendChallenge(FriendChallenge friendChallenge)
		{
			this.View.UpdateChallenge(friendChallenge.FriendPlayFabId, true);
		}

		protected void OnFriendChallengeCanceled(string playfabID)
		{
			this.View.UpdateChallenge(playfabID, false);
			this.View.NotifyChallenge(playfabID, PhotonMessageType.CancelChallenge);
		}

		protected void OnFriendChallengeDeclined(string playfabID)
		{
			this.View.UpdateChallenge(playfabID, false);
			this.View.NotifyChallenge(playfabID, PhotonMessageType.DeclineChallenge);
		}

		protected void OnAcceptChallenge(string playfabID)
		{
			this.View.UpdateChallenge(playfabID, false); //clear incoming status
		}
#endregion


        /// <summary>
        /// When playfab returns status about our add friend request
        /// </summary>
        private void OnAddFriendResponse(string friendPlayFabId, bool isConfirmed)
        {
            //if (this.View.IsOpen)
            {
                if (isConfirmed)
                {
                    this.View.AddFriendButtonText = I2.Loc.ScriptLocalization.Friend_Requested;
                    Timer.Instance.StartCoroutine(DelayUpdateFriendList());
                    //StartCoroutine(DelayUpdateFriendList());
                }
                else
                {
                    //this.View.AddFriendButton.interactable = false;
                    this.View.AddFriendButtonText = I2.Loc.ScriptLocalization.User_Not_Found;
                }
                this.View.HideFriendPanelInTwoSeconds();
                /*if (null != this.View.SelectedFriend && this.View.SelectedFriend.PlayfabId == friendPlayFabId)
                {
                    this.View.SelectedFriend = null;
                    this.View.HideFriendPanelInTwoSeconds();
                    this.View.HideConfirmPanelInOneSecond(friendPlayFabId);
                }*/
            }
        }

        private void OnConfirmFriendResponse(string friendPlayFabId, bool isConfirmed)
        {
            //if (this.View.IsOpen)
            {
                if (isConfirmed)
                {
                    this.View.chatLogPlayfabId = friendPlayFabId;
                    Timer.Instance.StartCoroutine(DelayUpdateFriendList());
                }
                if (this.View.SelectedFriend == friendPlayFabId)
                {
                    this.View.HideConfirmPanelInOneSecond(friendPlayFabId);
                    //this.View.SelectedFriend.ItemButton.onClick.Invoke();
                }
            }
            
        }

        private void OnDeclineFriendResponse(string friendPlayFabId, bool isConfirmed)
        {
            //if (this.View.IsOpen)
            {
                if (isConfirmed)
                {
                    Timer.Instance.StartCoroutine(DelayUpdateFriendList());
                }
                if (this.View.SelectedFriend == friendPlayFabId)
                {
                    this.View.SelectedFriend = null;
                    this.View.HideConfirmPanelInOneSecond(friendPlayFabId);
                    this.View.HideChatPanelInOneSecond(friendPlayFabId);
                }
            }
        }

        /// <summary>
        /// Handles the result of a remove friend action coming back from PlayFab
        /// </summary>
        private void OnRemoveFriendResponse(string friendPlayFabId, bool isConfirmed)
        {
            //if (this.View.IsOpen)
            {
                if (isConfirmed)
                {
                    Timer.Instance.StartCoroutine(DelayUpdateFriendList());
                }
                if (this.View.SelectedFriend == friendPlayFabId)
                {
                    this.View.SelectedFriend = null;
                    this.View.HideConfirmPanelInOneSecond(friendPlayFabId);
                    this.View.HideChatPanelInOneSecond(friendPlayFabId);
                }
            }
		}

		private void OnBlockFriendResponse(string friendPlayFabId, bool isBlockSuccessful)
        {
            //if (this.View.IsOpen)
            {
                if (isBlockSuccessful)
                {
                    Timer.Instance.StartCoroutine(DelayUpdateFriendList());
                }
                else
                {
                    this.View.BlockFriendButtonText = I2.Loc.ScriptLocalization.Failed;
                }
                if (this.View.SelectedFriend == friendPlayFabId)
                {
                    this.View.SelectedFriend = null;
                    this.View.HideConfirmPanelInOneSecond(friendPlayFabId);
                    this.View.HideChatPanelInOneSecond(friendPlayFabId);
                }
            }
        }

        private void OnUnblockFriendResponse(string friendPlayFabId, bool isBlockSuccessful)
        {
            //if (this.View.IsOpen)
            {
                if (isBlockSuccessful)
                {
                    Timer.Instance.StartCoroutine(DelayUpdateFriendList());
                }
                else
                {
                    this.View.confirmFriendRequestButtonLabel.text = I2.Loc.ScriptLocalization.Failed;
                }
                if (this.View.SelectedFriend == friendPlayFabId)
                {
                    //this.View.SelectedFriend = null;
                    this.View.HideConfirmPanelInOneSecond(friendPlayFabId);
                    this.View.HideChatPanelInOneSecond(friendPlayFabId);
                }
            }
        }

        /// <summary>
        /// Waits two frames to give the friends list a chance to add new items before updating it
        /// <summary>
        IEnumerator DelayUpdateFriendList()
		{
			yield return null;
			yield return null;
			if(this.View != null && this.PlayerProfile != null && this.PlayerProfile.FriendListData != null)
				this.View.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed, false);
		}
    }
}
