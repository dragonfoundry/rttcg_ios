﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;
using GameFormat = Messages.GameFormat;
using TournamentStatus = Messages.TournamentStatus;
using UnityEngine.EventSystems;
//using EntryMethod = Messages.EntryMethod;
using Messages;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class PlayScreenColumnView : AnimatedView
	{
		
		public event Action<GameFormat> EventFormatChanged;
		public event Action EventBackClicked;
        public event Action EventOpenStore;
        public Action PointerClickDeckImage;
        public Action PointerClickDeckNameInput;
        public Action EventPlayScreenOpened;
		public Action<string> EventDeckRenamed;
		public Action EventCloneClicked;
		public Action EventDeleteClicked;
		public RectTransform DeckImageRoot;
		public CanvasGroup EditButtonsCanvasGroup;
		
		public TMP_InputField DeckNameInput;
		public ShineFx DeckInfoShineFx;
		private Dictionary<ShineFx,Sequence>  buttonSequences = new Dictionary<ShineFx,Sequence>();
        private Dictionary<ShineFx,bool> buttonHoverStates = new Dictionary<ShineFx, bool>();

        public DeckData CurrentViewDeck { get; set; }

        [SerializeField] public TextMeshProUGUI _HeaderDisplay;
		[SerializeField] protected TextMeshProUGUI _EventInfoDisplay;
		[SerializeField] protected TextMeshProUGUI _FriendNameLabel;
		[SerializeField] protected GameObject _FriendRoot;
		[SerializeField] protected GameObject _EventInfoRoot;
		[SerializeField] protected GameObject _IllegalMessage;
        [SerializeField] protected GameObject _DeckEditButton;
        
        //public AvatarView AvatarView;
        public RawImage DeckArt;
        public RawImage EventArt;

        public CanvasGroupToggle DeckInfoToggle;
		public DeckMiniWidget DeckMini;
        
		public PlayLauncherWidget LauncherWidget;

        public TournamentInfoWidget LeftTournamentWidget;
        public TournamentInfoWidget CenterTournamentWidget;

        public GameObject LeftTournamentInfo;
        public GameObject CenterTournamentInfo;

        public GameObject PlayerCardParent;
        public PlayerCardView PlayerCard;

        public GameObject QuestParent;
        public QuestView QuestView { get; set; }

        private const string ANIM_EXPAND = "IsExpanded";
		private const string ANIM_STATUS = "EventStatus";

		protected GameFormat _Format = GameFormat.Casual;
		public GameFormat Format {
			get { return _Format; }
			set { _Format = value; SwitchFormat(value); }
		}

		public bool IsDeckLegal { set { /*_IllegalMessage.SetActive(!value);*/ this.LauncherWidget.IsReady = value; } }
        public bool IsTutorial { set { EditButtonsCanvasGroup.gameObject.SetActive(!value); } }

		public bool IsExpanded {
			get { return this.Animator.GetBool(ANIM_EXPAND); }
			set { this.Animator.SetBool(ANIM_EXPAND, value); }
		}

		protected bool _IsFormatInProgress;
		public bool IsFormatInProgress {
			get { return _IsFormatInProgress; }
			set { _IsFormatInProgress = value; this.Animator.SetBool(ANIM_STATUS, value); this.LauncherWidget.IsTournamentEntry = !value; }
		}

		//public string SubHeader { set { _SubHeaderDisplay.text = value; } }
		public string EventInfo { set { _EventInfoRoot.SetActive(!string.IsNullOrEmpty(value)); _EventInfoDisplay.text = value; } }
		//public int EntryFee { set { _EntryFeeLabel.text = string.Format("{0} CR", value); _EntryFeeLabel.gameObject.SetActive(value > 0); } } //TODO use widget for currency display

        public TournamentEventData TournamentEventData { get; set; }

        public bool isChallengeIncoming = false;
        protected NovaBlitzFriend _ChallengeOpponent;
		public NovaBlitzFriend ChallengeOpponent {
			get { return _ChallengeOpponent; }
			set { _ChallengeOpponent = value;
				//_FriendRoot.SetActive(null != value);
				//_FriendNameLabel.text = null != value ? value.FriendInfo.Username : string.Empty;
				if (null != value) this.LauncherWidget.PlayLabel = string.Format("{0}\n({1})", (isChallengeIncoming ? I2.Loc.ScriptLocalization.Accept : I2.Loc.ScriptLocalization.Challenge), value.FriendData.DisplayName);
			}
		}
        
        public Dictionary<CurrencyType, int> EntryFees { get; set; }


        protected override void Update()
        {
            base.Update();
            if (Format == GameFormat.Tournament)
            {
                UpdateTournamentData();
            }
        }

		private float animTime = 0f;

		void LateUpdate()
		{
			
			AnimatorStateInfo animatorStateInfo = this.Animator.GetCurrentAnimatorStateInfo(0);

			if(animatorStateInfo.IsName("Open") && animatorStateInfo.normalizedTime >= 1f && this.DeckNameInput.gameObject.activeSelf == false)
			{
				animTime += Time.deltaTime;
				Vector2 pos = this.DeckImageRoot.anchoredPosition;
				pos.y += Mathf.Sin(animTime * 2f) * 0.1f;
				this.DeckImageRoot.anchoredPosition = pos;
			}
		}

        protected void UpdateTournamentData()
        {

            // do a time check for button active

            // Update the time stamps
            // current time remaining to register
            if (IsFormatInProgress)
            {
                LeftTournamentWidget.UpdateStatusAndTime();
            }
            else
            {
                CenterTournamentWidget.Init(null);
                CenterTournamentWidget.UpdateStatusAndTime();
            }
        }

        protected void SwitchFormat(GameFormat format)
		{
			if (null != this.EventFormatChanged)
				this.EventFormatChanged.Invoke(format);
		}


		#region Mono
		protected override void Awake()
		{
			base.Awake();
			this.LauncherWidget.Init();
            this.DeckNameInput.onEndEdit.AddListener(OnEndEditDeckName);
		}
		#endregion

		#region uGUI
		public void ClickBack() { if (null != this.EventBackClicked && this.Format != GameFormat.Tutorial) this.EventBackClicked.Invoke(); }
        public void OpenStore() { if (null != this.EventOpenStore) this.EventOpenStore.Invoke(); }
        public void PlayScreenOpened() { if (null != this.EventPlayScreenOpened ) this.EventPlayScreenOpened.Invoke(); }
		#endregion
		
		public void ShineDeckInfo(float initialDelay = 0f)
        {
            if(this.IsOpen)
    			this.StartCoroutine(this.DelayButtonShine(initialDelay));
        }
		
		private IEnumerator DelayButtonShine(float initialDelay)
		{
			this.HideButtonShine(this.DeckInfoShineFx);
			yield return new WaitForSecondsRealtime(initialDelay);
			this.ShowButtonShine(this.DeckInfoShineFx);
			yield return new WaitForSecondsRealtime(0.3f);
			this.HideButtonShine(this.DeckInfoShineFx);
		}
		
		private void ShowButtonHover(ShineFx shineFx)
		{
			Sequence seq =  DOTween.Sequence();
			seq.Append(shineFx.Glare.DOFade(35/255f,0.2f));
			seq.AppendCallback(()=>{this.OnShineComplete(shineFx);});
            seq.Play();
		}
		
		private void ShowButtonShine(ShineFx shineFx)
        {   
            // Start the "Show" animation from the hidden state
            this.HideButtonShine(shineFx);
            
            // Move the shine and fade the glare in and out
            Sequence seq =  DOTween.Sequence();
            seq.Append(shineFx.Shine.transform.DOLocalMoveY(510f, 0.35f));
            seq.Join(shineFx.Glare.DOFade(95f/255f,0.1f));
            seq.Insert(0.1f,shineFx.Glare.DOFade(35/255f,0.2f));
            seq.AppendCallback(()=>{this.OnShineComplete(shineFx);});
            seq.Play();
            this.buttonSequences[shineFx] = seq;
            
            // Set the state of the shine to hovered
            this.buttonHoverStates[shineFx] = true;
        }
        
        private void OnShineComplete(ShineFx shineFx)
        {
            // Check and see if the shine has become unhovered...
            if(this.buttonHoverStates[shineFx] == false)
            {
                Sequence seq = null;
                if(this.buttonSequences.TryGetValue(shineFx, out seq) && seq.IsPlaying())
                {
                    seq.Kill();
                }
                
                // ... and if it has, hide any residuals of the shine
                this.HideButtonShine(shineFx);
            }
        }
        
        private void HideButtonShine(ShineFx shineFx)
        {
            // Set the state of this shine to NOT hovered
            this.buttonHoverStates[shineFx] = false;
            
            // Cancel any current sequence for this shine
            Sequence seq = null;
            if(this.buttonSequences.TryGetValue(shineFx, out seq) && seq.IsPlaying())
            {
                // Do nothing if there's a playing sequence
            }
            else
            {
                // Move the button shine offscreen bottom    
                shineFx.Shine.transform.DOLocalMoveY(-500f,0f);
                
                // Make the glare completely transparent
                shineFx.Glare.DOFade(0f,0f);
                
                // Remove any sequence references might be hanging around
                this.buttonSequences.Remove(shineFx);
            }
        }
		public void PointerEnterDeckInfo()
		{
            if (Format == GameFormat.Tutorial)
                return;
			this.buttonHoverStates[this.DeckInfoShineFx] = true;
			if(this.DeckNameInput.gameObject.activeSelf == false)
			{
				this.ShowButtonHover(this.DeckInfoShineFx);
			}
		}
		
		public void PointerExitDeckInfo()
        {
            if (Format == GameFormat.Tutorial)
                return;
            this.buttonHoverStates[this.DeckInfoShineFx] = false;
			if(this.DeckNameInput.gameObject.activeSelf == false)
			{
				this.HideButtonShine(this.DeckInfoShineFx);
			}
		}
		
		public void PointerClickDeckInfo()
        {
            if (PointerClickDeckImage != null)
                PointerClickDeckImage.Invoke();

        }
        public void PointerClickDeckNameEdit()
        {
            if (PointerClickDeckImage != null)
                PointerClickDeckImage.Invoke();
            if (Application.platform == RuntimePlatform.Android && PointerClickDeckNameInput != null && DeckNameInput.gameObject.activeSelf == false)
                PointerClickDeckNameInput.Invoke();

        }

        public void OnEndEditDeckName(string deckName)
        {
            //this.DeckNameInput.DeactivateInputField();
            //this.OnDeckNameEndEdit(deckName);
            PointerClickDeckInfo();
        }
		
		public void OnDeckNameEndEdit(string deckName)
        {
            this.DeckNameInput.DeactivateInputField();
            this.DeckMini.DeckName = deckName;

			PointerEventData pointer = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
			List<RaycastResult> raycastResults = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointer, raycastResults);
			Debug.LogFormat("End edit. Deck name: {0}", deckName);
       
			bool hideButtons = true;
			if(raycastResults.Count > 0)
			{
				// If the raycast hit something, investigate what...
				GameObject hit = raycastResults.First().gameObject;
				if(hit.tag == "button")
				{
					Debug.LogFormat("Hit a button:{0}", hit.gameObject);
					hideButtons = false;
				}
			}
            if(hideButtons && this.EventDeckRenamed != null)
            {
                this.EventDeckRenamed.Invoke(deckName);
            }

            Timer.Instance.StopCoroutine(DelayHideEditButtons(hideButtons));
            Timer.Instance.StartCoroutine(DelayHideEditButtons(hideButtons));
		}

		public IEnumerator DelayHideEditButtons(bool hideButtons)
		{
			while(Input.GetMouseButton(0) == true || Input.GetMouseButtonUp(0) == true || Input.touchCount > 0)
			{
				yield return null;
            }
            this.DeckNameInput.gameObject.SetActive(false);
            if (hideButtons)
            {
                this.EditButtonsCanvasGroup.alpha = 0;
                this.EditButtonsCanvasGroup.interactable = false;
                this.EditButtonsCanvasGroup.blocksRaycasts = false;

            }

            this.HideButtonShine(this.DeckInfoShineFx);
        }

		public void OnCloneButtonClicked()
		{
			if(this.EventCloneClicked != null)
			{
				this.EventCloneClicked.Invoke();
			}

			this.EditButtonsCanvasGroup.alpha = 0;
			this.EditButtonsCanvasGroup.interactable = false;
			this.EditButtonsCanvasGroup.blocksRaycasts = false;
		}

		public void OnDeleteButtonClicked()
		{
			if(this.EventDeleteClicked != null)
			{
				this.EventDeleteClicked.Invoke();
			}

			this.EditButtonsCanvasGroup.alpha = 0;
			this.EditButtonsCanvasGroup.interactable = false;
			this.EditButtonsCanvasGroup.blocksRaycasts = false;
		}

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (QuestView != null)
                QuestView.Recycle();
        }
	}
}
