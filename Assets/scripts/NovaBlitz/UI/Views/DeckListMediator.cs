﻿using UnityEngine;
using System;
using System.Collections;
using LinqTools;
using strange.extensions.mediation.impl;
using strange.extensions.context.impl;
using System.Collections.Generic;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Game;

namespace NovaBlitz.UI.Mediators
{
	public class DeckListMediator : Mediator 
	{
		[Inject] public DeckListView View {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public GameData GameData { get;set;}
		[Inject] public CloneDeckSignal CloneDeckSignal {get;set;}
		[Inject] public NewDeckSignal NewDeckSignal {get;set;}
		[Inject] public UpdateDeckOrderSignal UpdateDeckOrderSignal {get;set;}
		[Inject] public UpdateDeckListSignal UpdateDeckListSignal {get;set;}
		[Inject] public SelectDeckSignal SelectDeckSignal {get;set;}
        [Inject] public DeckSelectedSignal DeckSelectedSignal { get; set; }
        [Inject] public DeselectDeckSignal DeselectDeckSignal {get;set;}
		[Inject] public INovaContext ContextView {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}
		[Inject] public StartGameSessionSignal StartGameSessionSignal {get;set;}
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
		[Inject] public InitializeViewSignal InitializeViewSignal {get;set;}

		/// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
		public override void OnRegister()
		{
            this.View.PlayerProfile = PlayerProfile;
            this.View.GameData = GameData;
			this.View.DeckListItemPrefab = this.UIConfiguration.DeckListItemPrefab;
			this.View.DeckSelected += this.OnDeckSelected;
			this.View.DeckDeselected += this.OnDeckDeselected;
			this.View.DeckAdd += this.OnDeckAdd;
			this.View.DeckDeleted += this.OnDeckDeleted;
			this.View.DeckReordered += this.OnDeckReordered;
            this.View.SaveDecks += this.OnSaveDecks;
            this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
			this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.DeckSelectedSignal.AddListener(this.OnDeckSelectedByCommand);
			this.InitializeViewSignal.AddListener(this.OnInitializeView);
        }

		/// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
		{
			this.View.DeckSelected -= this.OnDeckSelected;
			this.View.DeckDeselected -= this.OnDeckDeselected;
			this.View.DeckAdd -= this.OnDeckAdd;
			this.View.DeckDeleted -= this.OnDeckDeleted;
			this.View.DeckReordered -= this.OnDeckReordered;
            this.View.SaveDecks -= this.OnSaveDecks;
			this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.DeckSelectedSignal.RemoveListener(this.OnDeckSelectedByCommand);
			this.InitializeViewSignal.RemoveListener(this.OnInitializeView);
        }

        private void OnInitializeView(Type viewType)
        {
			if(viewType == this.View.GetType())
			{
				this.View.Initialize(/*this.PlayerProfile.DeckList, this.PlayerProfile.NumDeckSlots,*/ this.PlayerProfile.CurrentDeck != null ? this.PlayerProfile.CurrentDeck.ID : null);
			}
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                this.View.Initialize(/*this.PlayerProfile.DeckList, this.PlayerProfile.NumDeckSlots,*/ this.PlayerProfile.CurrentDeck != null ? this.PlayerProfile.CurrentDeck.ID : null);
                var deck = this.PlayerProfile.CurrentDeck ?? this.PlayerProfile.DeckList.FirstOrDefault();
				if (null != deck)
				{
					this.View.ToggleDeck(deck.ID);
				}
            }
        }

		private void OnDeckDeleted()
        {
			PlayScreenColumnView playScreenView = this.ViewReferences.Get<PlayScreenColumnView>();
			playScreenView.OnDeleteButtonClicked();
		}

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
              
            }
        }

        private void OnLanguageChanged()
        {
            if(this.View.IsOpen)
                this.OnInitializeView(this.View.GetType());
        }

		private void OnProfileUpdated(ProfileSection section)
		{
			if (this.View.IsOpen && section == ProfileSection.DeckList)
			{
                this.OnInitializeView(this.View.GetType());
			}
        }

		private void OnViewIsOpenChanged(bool isOpen)
		{
			if (isOpen) {
				var deck = this.PlayerProfile.CurrentDeck ?? this.PlayerProfile.DeckList.FirstOrDefault();
				if (null != deck)
					this.View.ToggleDeck(deck.ID);
			}
		}

		private void OnDeckSelected(DeckListItem deckListItem)
		{
			if (this.PlayerProfile.CurrentDeck != deckListItem.DeckData)
				this.SelectDeckSignal.Dispatch(deckListItem.DeckData);
		}
		
		private void OnDeckDeselected(DeckListItem deckListItem)
		{
			this.DeselectDeckSignal.Dispatch(deckListItem.DeckData);
		}

		private void OnDeckAdd()
		{
			if(this.PlayerProfile.DeckList.Count < this.PlayerProfile.NumDeckSlots)
			{
				this.NewDeckSignal.Dispatch();
			}
			else
			{
                NewMessageDialogParams dlgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.OutOfDeckSlots,
                    string.Format(I2.Loc.ScriptLocalization.Error.OutOfDeckSlotsMessage, this.PlayerProfile.NumDeckSlots, 0, 0));// + " decks, please remove an existing deck before adding a new one.");
				this.ShowNewMessageDialogSignal.Dispatch(dlgParams);
			}
		}

		private void OnDeckReordered(DeckData data)
		{
			List<string> deckIDs = this.View.GetDeckIDs();
			this.UpdateDeckOrderSignal.Dispatch(deckIDs);
            this.View.IsDeckOrderNeedsSaving = true;
            this.SelectDeckSignal.Dispatch(data);
        }

        private void OnSaveDecks()
        {
            Debug.Log("Saving Decks");
            DeckListDataContract dataContract = this.PlayerProfile.GetDeckListDataContract();
            if(dataContract != null)
            {
                this.UpdateDeckListSignal.Dispatch(dataContract, false);
            }
        }

        private void OnDeckSelectedByCommand(DeckData data)
        {
			if(this.View != null && this.View.IsOpen)
			{
				this.View.ToggleDeck(data.ID);
			}
        }
    }
}
