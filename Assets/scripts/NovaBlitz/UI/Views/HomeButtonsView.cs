﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using DragonFoundry.Fx;
using GameFormat = Messages.GameFormat;
using Messages;
using DG.Tweening;

namespace NovaBlitz.UI
{
    
    [Serializable] public class ButtonShines
    {
        public ShineFx PlayButtonShine;
        public ShineFx DraftButtonShine;
        public ShineFx LeagueButtonShine;
        public ShineFx EventsButtonShine;
    }
    
    [Serializable] public class ShineFx
    {
        public Image Shine;
        public Image Glare;
    }
    
	public class HomeButtonsView : AnimatedView
	{
        [Inject] public INovaContext NovaContext { get; set; }
        public event Action EventCasualButtonClicked;
        public event Action EventDraftButtonClicked;
        public event Action EventLeagueButtonClicked;
        public event Action EventEventsButtonClicked;
        public event Action EventLeaderboardClicked;
        public event Action EventFriendsClicked;
        public event Action EventChatClicked;
        public event Action EventSettingsClicked;
		public event Action EventAchievementsClicked;
		public event Action EventCollectionClicked;
        public event Action EventFeedbackClicked;
        public event Action EventStoreClicked;
		public event Action EventPacksClicked;
        public event Action EventProfileClicked;
        public event Action EventTutorialClicked; //TODO temporary
		public event Action EventAnnouncementClicked;
        public event Action EventHomeButtonsOpened;
        public Action<bool?> ScrollOffer;
        public Action EventUpdateHints;
        
        [SerializeField]
        public ButtonShines ButtonShines = new ButtonShines();
        private Dictionary<ShineFx,Sequence>  buttonSequences = new Dictionary<ShineFx,Sequence>();
        private Dictionary<ShineFx,bool> buttonHoverStates = new Dictionary<ShineFx, bool>();
        private ShineFx pressedButtonShine = null;
        public RectTransform BigButtonsRow;
        public Button CasualButton;

        [SerializeField] protected Button _BuyPacksButton;
		[SerializeField] protected Button _OpenPacksButton;
        [SerializeField] public Image _OpenPacksNotification;
        [SerializeField] protected TextMeshProUGUI _PacksQuantityDisplay;
        
        //DRAFTING
        [SerializeField] protected GameObject _DraftDisplayActive;
		[SerializeField] protected GameObject _DraftButtonInactive;
        public PlayStatusWidget DraftStatusWidget;
        public Image DraftArt;
        public Button DraftButton;
        public TextMeshProUGUI DraftRankLimit;

        // LEAGUES
        [SerializeField] protected GameObject _LeagueDisplayActive;
		[SerializeField] protected GameObject _LeagueButtonInactive;
        public PlayStatusWidget LeagueStatusWidget;
        public Image LeagueArt;
        public Button LeagueButton;
        public TextMeshProUGUI LeagueRankLimit;

        // EVENTS
        [SerializeField] protected GameObject _TournamentDisplayActive;
        [SerializeField] protected GameObject _TournamentButtonInactive;
        public PlayStatusWidget EventsStatusWidget;
        public TournamentInfoWidget TournamentInfoWidget;
        public Image TournamentArt;
        public Button TournamentButton;
        public TextMeshProUGUI TournamentRankLimit;
        [SerializeField] protected GameObject _TournamentInfo;


        [SerializeField] protected TextMeshProUGUI _AnnouncementDisplay;

        public Button PlayHintAnchor;
        public Button DraftHintAnchor;
        public Button LeagueHintAnchor;
        public Button EventsHintAnchor;
        public Button StoreHintAnchor;
        public Button PacksHintAnchor;

        private int _PacksUnopened;
        public int PacksUnopened { get { return _PacksUnopened; } set {
                _PacksUnopened = value;
                //_BuyPacksButton.gameObject.SetActive(value < 1);
                _OpenPacksNotification.gameObject.SetActive(value > 0);
				_PacksQuantityDisplay.text = value.ToString();
                
                this._OpenPacksNotification.transform.DOKill();
                this._OpenPacksNotification.transform.localScale = Vector3.one;
                this._OpenPacksNotification.transform.DOPunchScale(Vector3.one * 0.2f, 5.0f, vibrato:1, elasticity:0.25f).SetLoops(-1);

            } }

        public ParticleEffectsTrigger ButtonFx;

        public RectTransform OfferHolder;
        private bool OfferScrollPaused;
        public float OfferScrollTimeLimit = 4.5f;
        public float OfferScrollDeltaTime { get; set; }
        public StoreItemView OfferItem { get; set; }
        public Image[] OfferDots;


        public PlayerCardView PlayerCard { get; set; }
        public RectTransform PlayerCardHolder;
        public TextMeshProUGUI SeasonInfoText;

        public Material GrayscaleMaterial { get; set; }
        private RectTransform _RectTransform;
		
		public bool DraftActive { get { return _DraftDisplayActive.activeSelf; } set { _DraftDisplayActive.SetActive(value); } }
		public bool LeagueActive { get { return _LeagueDisplayActive.activeSelf; } set { _LeagueDisplayActive.SetActive(value); } }
        public bool EventActive { get { return _TournamentDisplayActive.activeSelf; } set { _TournamentDisplayActive.SetActive(value); } }
        public string Announcement { set { _AnnouncementDisplay.text = value; } }

        private bool _DraftAccessible;
        private bool _LeagueAccessible;
        private bool _EventsAccessible;
        public bool DraftAccessible
        {
            get { return _DraftAccessible; }
            set
            {
                _DraftAccessible = value;
                DraftButton.interactable = value;
                //DraftButton.gameObject.SetActive(value);
                _DraftButtonInactive.SetActive(!value);
                DraftArt.material = value ? DraftArt.defaultMaterial : GrayscaleMaterial;
                //DraftRankLimit.SetText("{0}", GameData.draftThreshold);
                ButtonShines.DraftButtonShine.Shine.gameObject.SetActive(value);
                ButtonShines.DraftButtonShine.Glare.gameObject.SetActive(value);
            } }
        public bool LeagueAccessible
        {
            get { return _LeagueAccessible; }
            set
            {
                _LeagueAccessible = value;
                LeagueButton.interactable = value;
                //LeagueButton.gameObject.SetActive(value);
                _LeagueButtonInactive.SetActive(!value);
                LeagueArt.material = value ? LeagueArt.defaultMaterial : GrayscaleMaterial;
                //LeagueRankLimit.SetText("{0}", GameData.leagueThreshold);
                ButtonShines.LeagueButtonShine.Shine.gameObject.SetActive(value);
                ButtonShines.LeagueButtonShine.Glare.gameObject.SetActive(value);

            } }
        public bool EventsAccessible
        {
            get { return _EventsAccessible; }
            set
            {
                _EventsAccessible = value;
                TournamentButton.interactable = value;
                //TournamentButton.gameObject.SetActive(value);
                _TournamentButtonInactive.SetActive(!value);
                TournamentArt.material = value ? TournamentArt.defaultMaterial : GrayscaleMaterial;
                _TournamentInfo.SetActive(value);
                //TournamentRankLimit.SetText("{0}", GameData.tournamentThreshold);
                ButtonShines.EventsButtonShine.Shine.gameObject.SetActive(value);
                ButtonShines.EventsButtonShine.Glare.gameObject.SetActive(value);
            } }

        public INovaContext Context { get; set; }

        protected override void Awake()
        {
            base.Awake();
            this._RectTransform = (this.transform as RectTransform);
        }

        protected override void Update()
        {
            base.Update();
            this.TournamentInfoWidget.UpdateStatusAndTime();
            Vector2 pos = this.BigButtonsRow.anchoredPosition;
            pos.y = Mathf.Sin(Time.time * 2f) * 2.5f;
            this.BigButtonsRow.anchoredPosition = pos;
            
            if(this.IsOpen == false) return;


            // Set the season text
            var nextSeasonStart = new DateTime(Server.Time.Year, Server.Time.Month, 1).AddMonths(1);
            var minutesLeft = Math.Max(0,(nextSeasonStart - Server.Time).TotalMinutes);

            if (SeasonInfoText != null)
            {
                if (minutesLeft >= 48 * 60)
                    SeasonInfoText.text = string.Format(I2.Loc.ScriptLocalization.Season_Days_Left, "month", (int)(minutesLeft / 24 / 60), 0, 0, 0);
                else
                    SeasonInfoText.text = string.Format(I2.Loc.ScriptLocalization.Season_Hours_Left, "month", (int)(minutesLeft / 60), (int)(minutesLeft % 60), 0, 0);
            }

            if (_RectTransform.rect.height > 0)
            {
                // set the scale of the home buttons (between 0.7 (4x3) and 0.85 (16x9))
                var scale = Vector3.one * Math.Max(0.7f, Math.Min(0.85f, _RectTransform.rect.width / _RectTransform.rect.height / 1.90f));
                foreach (Transform child in BigButtonsRow.transform)
                {
                    child.localScale = scale;
                }
            }
            //bool isButtonHovered = false;
            if (Input.GetMouseButtonUp(0))
            {
                this.HideButtonShine(this.ButtonShines.PlayButtonShine);
                this.HideButtonShine(this.ButtonShines.DraftButtonShine);
                this.HideButtonShine(this.ButtonShines.LeagueButtonShine);
                this.HideButtonShine(this.ButtonShines.EventsButtonShine);
            }
            else if(Input.GetMouseButtonDown(0))
            {
                foreach(var kvp in this.buttonHoverStates)
                {
                    if(kvp.Value == true)
                    {
                        this.pressedButtonShine = kvp.Key;
                        break;
                    }
                }
            }

            // Offer scroll
            if(!OfferScrollPaused)
            {
                if (NovaContext != null && !NovaContext.IsAlreadyBlurred)
                {
                    OfferScrollDeltaTime += Time.deltaTime;
                    if (OfferScrollDeltaTime > OfferScrollTimeLimit && ScrollOffer != null)
                    {
                        ScrollOffer(true);
                        OfferScrollDeltaTime = 0;
                    }
                }
                else
                {
                    OfferScrollDeltaTime = 0;
                }
            }
        }

        protected void SetRankLegality()
        {
            //gfx.defaultMaterial : GrayscaleMaterial;
        }

        #region uGUI
        protected void ClickCasualButton()
		{
			if (null != this.EventCasualButtonClicked)
				this.EventCasualButtonClicked.Invoke();
        }
        protected void ClickDraftButton()
        {
            if (null != this.EventDraftButtonClicked)
                this.EventDraftButtonClicked.Invoke();
        }
        protected void ClickLeagueButton()
        {
            if (null != this.EventLeagueButtonClicked)
                this.EventLeagueButtonClicked.Invoke();
        }
        protected void ClickEventsButton()
        {
            if (null != this.EventEventsButtonClicked)
                this.EventEventsButtonClicked.Invoke();
        }
        public void ClickUpButton()
        {
            if (null != this.ScrollOffer)
                this.ScrollOffer.Invoke(false);
        }
        public void ClickDownButton()
        {
            if (null != this.ScrollOffer)
                this.ScrollOffer.Invoke(true);
        }

        public void ClickCasual() 
        {
            this.DoBigButtonClickTween(this.CasualButton, ()=> ClickCasualButton());
        }
		public void ClickEvent() 
        {
            this.DoBigButtonClickTween(this.TournamentButton, ()=>ClickEventsButton()); 
        }
		public void ClickDraft() 
        {
            this.DoBigButtonClickTween(this.DraftButton, ()=> ClickDraftButton()); 
        }
		public void ClickLeague() 
        { 
            this.DoBigButtonClickTween(this.LeagueButton, ()=> ClickLeagueButton());
        }
        
        private void DoBigButtonClickTween(Button bigButton, Action onClick)
        {
            Transform buttonRoot = bigButton.transform.parent;
            Sequence seq = DOTween.Sequence();
            seq.Append(buttonRoot.DOLocalMoveZ(50f,0.1f));
            seq.Append(buttonRoot.DOLocalMoveZ(0f,0.1f));
            seq.AppendCallback(()=>onClick.Invoke());
            seq.Play();
        }

        /*public void SetGlow(CanvasGroup glowCanvas)
        {
            CasualGlowCanvas.DOFade(0.0f, 1.0f);
            DraftGlowCanvas.DOFade(0.0f, 1.0f);
            LeagueGlowCanvas.DOFade(0.0f, 1.0f);
            EventGlowCanvas.DOFade(0.0f, 1.0f);
            StoreGlowCanvas.DOFade(0.0f, 1.0f);
            OpenPacksGlowCanvas.DOFade(0.0f, 1.0f);
            if(glowCanvas != null)
                glowCanvas.DOFade(1.0f, 1.0f);
        }*/
        
        public void PlayFx(Button button)
        {
            if (_PlayFxCoroutine != null)
            {
                Timer.Instance.StopCoroutine(_PlayFxCoroutine);
            }
            _PlayFxCoroutine = PlayFxCoroutine(button);
            Timer.Instance.StartCoroutine(_PlayFxCoroutine);
        }
        private IEnumerator _PlayFxCoroutine;
        public bool IsPlaying { get; set; }
        private IEnumerator PlayFxCoroutine(Button button)
        {
            yield return null;
            if (button == null || !this.IsOpen)
            {
                this.ButtonFx.EndEffect();
                this.ButtonFx.gameObject.SetActive(false);
                IsPlaying = false;
            }
            else
            {
                this.ButtonFx.transform.AttachToParent(button.transform, button.transform.childCount);
                this.ButtonFx.transform.localPosition = Vector3.zero;

                ButtonFx.gameObject.SetActive(true);
                while (!ButtonFx.gameObject.activeInHierarchy)
                {
                    yield return null;
                }
                ButtonFx.StartEffect();
                IsPlaying = true;
            }
        }
        

        public void ClickLeaderboard() { if (null != this.EventLeaderboardClicked) this.EventLeaderboardClicked.Invoke(); }
        public void ClickFriends() { if (null != this.EventFriendsClicked) this.EventFriendsClicked.Invoke(); }
        public void ClickChat() { if (null != this.EventChatClicked) this.EventChatClicked.Invoke(); }
        public void ClickSettings() { if (null != this.EventSettingsClicked) this.EventSettingsClicked.Invoke(); }
		public void ClickAchievements() { if (null != this.EventAchievementsClicked) this.EventAchievementsClicked.Invoke(); }
        public void ClickMyDecks() { ClickCasualButton(); } 
        public void ClickFeedback() { if (null != this.EventFeedbackClicked) this.EventFeedbackClicked.Invoke(); }
        public void ClickCollection() { if (null != this.EventCollectionClicked) this.EventCollectionClicked.Invoke(); }
		public void ClickStore() { if (null != this.EventStoreClicked) this.EventStoreClicked.Invoke(); }
		public void ClickPacks() { if (null != this.EventPacksClicked) this.EventPacksClicked.Invoke(); }
        public void ClickProfile() { if (null != this.EventProfileClicked) this.EventProfileClicked.Invoke(); }
        public void ClickTutorial() { if (null != this.EventTutorialClicked) this.EventTutorialClicked.Invoke(); } //TODO temporary

		public void ClickAnnouncement() { if (null != this.EventAnnouncementClicked) this.EventAnnouncementClicked.Invoke(); }

        public void HomeButtonsOpened() { if (null != this.EventHomeButtonsOpened) this.EventHomeButtonsOpened.Invoke(); }

		#endregion
        
        public void PointerEnterPlayButton() { this.ShowButtonShine(this.ButtonShines.PlayButtonShine); }
        public void PointerExitPlayButton() { this.HideButtonShine(this.ButtonShines.PlayButtonShine); }
        public void PointerEnterDraftButton() { this.ShowButtonShine(this.ButtonShines.DraftButtonShine); }
        public void PointerExitDraftButton() { this.HideButtonShine(this.ButtonShines.DraftButtonShine); }
        public void PointerEnterLeagueButton() { this.ShowButtonShine(this.ButtonShines.LeagueButtonShine); }
        public void PointerExitLeagueButton() { this.HideButtonShine(this.ButtonShines.LeagueButtonShine); }
        public void PointerEnterEventsButton() { this.ShowButtonShine(this.ButtonShines.EventsButtonShine); }
        public void PointerExitLEventsButton() { this.HideButtonShine(this.ButtonShines.EventsButtonShine); }
        public void PointerEnterOffers() { OfferScrollPaused = true; }
        public void PointerExitOffers() { OfferScrollPaused = false; }

        private void ShowButtonShine(ShineFx shineFx)
        {   
            // Start the "Show" animation from the hidden state
            this.HideButtonShine(shineFx);
            
            if(Input.GetMouseButton(0) || Input.touchCount > 0)
            {
                Debug.LogFormat("PressedButton is {0}", shineFx.Shine.transform.parent.parent.name);
                this.pressedButtonShine = shineFx;
            }
            else
            {
                this.pressedButtonShine = null;
            }
            
            // Move the shine and fade the glare in and out
            Sequence seq =  DOTween.Sequence();
            seq.Append(shineFx.Shine.transform.DOLocalMoveY(525f, 0.3f));
            seq.Join(shineFx.Glare.DOFade(95f/255f,0.1f));
            seq.Insert(0.1f,shineFx.Glare.DOFade(35/255f,0.1f));
            seq.AppendCallback(()=>{this.OnShineComplete(shineFx);});
            seq.Play();
            this.buttonSequences[shineFx] = seq;
            
            // Set the state of the shine to hovered
            this.buttonHoverStates[shineFx] = true;
        }
        
        private void OnShineComplete(ShineFx shineFx)
        {
            // Check and see if the shine has become unhovered...
            if(this.buttonHoverStates[shineFx] == false)
            {
                Sequence seq = null;
                if(this.buttonSequences.TryGetValue(shineFx, out seq) && seq.IsPlaying())
                {
                    seq.Kill();
                }
                
                // ... and if it has, hide any residuals of the shine
                this.HideButtonShine(shineFx);
            }
        }
        
        private void HideButtonShine(ShineFx shineFx)
        {
            // Set the state of this shine to NOT hovered
            this.buttonHoverStates[shineFx] = false;
            
            if(this.pressedButtonShine == shineFx 
                && (Input.touchCount == 0 || Input.GetMouseButton(0) == false))
            {
               this.StartCoroutine(this.UnpressButton(shineFx));
            }
            
            // Cancel any current sequence for this shine
            Sequence seq = null;
            if(this.buttonSequences.TryGetValue(shineFx, out seq) && seq.IsPlaying())
            {
                // Do nothing if there's a playing sequence
            }
            else
            {
                // Move the button shine offscreen bottom    
                shineFx.Shine.transform.DOLocalMoveY(-480f,0f);
                
                // Make the glare completely transparent
                shineFx.Glare.DOFade(0f,0f);
                
                // Remove any sequence references might be hanging around
                this.buttonSequences.Remove(shineFx);
            }
        }
        
        private IEnumerator UnpressButton(ShineFx pressedShine)
        {
            yield return null;
            if(this.pressedButtonShine == pressedShine)
            {
                this.pressedButtonShine = null;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            this._OpenPacksNotification.transform.DOKill();
            var offers = this.OfferHolder.GetComponentsInChildren<StoreItemView>();
            foreach(var offer in offers)
            {
                offer.Recycle();
            }
            if(this.OfferItem != null)
                this.OfferItem.Recycle();
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class HomeButtonsMediator : Mediator
	{
		[Inject] public HomeButtonsView View {get;set;}

		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public PlayerDataLoadedSignal PlayerDataLoadedSignal {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        [Inject] public INovaContext NovaContext { get; set; }
        [Inject] public NovaIAPService NovaIAPService { get; set; }

		private const string LABEL_URI = "<u>Click here</u> for more info.";

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
		public override void OnRegister()
		{

            if (this.View.PlayerCard == null && this.View.PlayerCardHolder != null)
            {
                this.View.PlayerCard = GameObject.Instantiate(this.UIConfiguration.PlayerCardPrefab);
                this.View.PlayerCard.transform.SetParent(this.View.PlayerCardHolder, false);
                this.View.PlayerCard.StatsPanel.gameObject.SetActive(true);
                this.View.PlayerCard.QuestPanel.gameObject.SetActive(false);
                this.View.PlayerCard.ButtonArea.gameObject.SetActive(true);
                this.View.PlayerCard.PlayStatusPanel.SetActive(false);
                this.View.PlayerCard.VerticalStatusPanel.SetActive(false);
                this.View.PlayerCard.Initialize(this.PlayerProfile.PlayerData);
                this.View.PlayerCard.CardBack.gameObject.SetActive(false);
                this.View.PlayerCard.ButtonArea.gameObject.SetActive(false);
                this.View.PlayerCard.StatsPanel.gameObject.SetActive(false);
                //this.View.PlayerCard.NameDisplay.gameObject.SetActive(false);
            }

            this.View.EventUpdateHints += this.UpdateUiHints;
            this.View.GrayscaleMaterial = UIConfiguration.GrayscaleMaterial;
			this.PlayerDataLoadedSignal.AddListener(OnPlayerDataLoaded);
            this.ProfileUpdatedSignal.AddListener(OnPlayerProfileUpdated);
			this.View.EventAnnouncementClicked += this.OnAnnouncementClicked;
            this.View.EventHomeButtonsOpened += this.UpdateUiHints;
            this.View.ScrollOffer += PlayOfferScroll;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.MoveAnimatedViewToCanvasSignal.AddListener(this.OnMoveAnimatedViewToCanvas);

            //RefreshPlayer();
            UpdateUiHints();
            //RefreshAnnouncements();
		}

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
        {
            this.View.EventUpdateHints -= this.UpdateUiHints;
            this.PlayerDataLoadedSignal.RemoveListener(OnPlayerDataLoaded);
            this.ProfileUpdatedSignal.RemoveListener(OnPlayerProfileUpdated);
			this.View.EventAnnouncementClicked -= this.OnAnnouncementClicked;
            this.View.EventHomeButtonsOpened -= this.UpdateUiHints;
            this.View.ScrollOffer -= PlayOfferScroll;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.MoveAnimatedViewToCanvasSignal.RemoveListener(this.OnMoveAnimatedViewToCanvas);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                this.RefreshPlayer();
                //UpdateUiHints();
                this.RefreshAnnouncements();
                PopulateOffers();
            }
        }

        private void OnCloseAnimatedView(Type viewType)
        {
            if(viewType == this.View.GetType())
            {
                View.ButtonFx.EndEffect();
                View.ButtonFx.gameObject.SetActive(false);
            }
            else if (this.View.IsOpen && 
                (viewType == typeof(StoreView) || viewType == typeof(PackOpenerConfirmView)))
                {
                this.UpdateUiHints();
            }
        }

        protected void RefreshPlayer()
        {
            bool draftActive = (this.PlayerProfile.DraftData.event_status == Messages.TournamentStatus.playing || this.PlayerProfile.DraftData.event_status == Messages.TournamentStatus.drafting);
            this.View.DraftActive = draftActive;
            bool leagueActive = this.PlayerProfile.ConstructedData.event_status == Messages.TournamentStatus.playing;
            this.View.LeagueActive = leagueActive;
            bool tournamentActive = (this.PlayerProfile.TournamentData.event_status == Messages.TournamentStatus.playing || this.PlayerProfile.TournamentData.event_status == Messages.TournamentStatus.drafting);
            this.View.EventActive = tournamentActive;

            int rating = this.PlayerProfile.CurrentRating;
            int rank = this.PlayerProfile.CurrentRank;
            this.View.DraftAccessible = draftActive 
                || this.PlayerProfile.OnboardingProgress == null 
                || ((this.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests == null || !this.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests.Contains(-5))
                    && (this.PlayerProfile.OnboardingProgress.CurrentQuest == null  || this.PlayerProfile.OnboardingProgress.CurrentQuest.ID != -5));
            this.View.LeagueAccessible = leagueActive || GameData.isLeagueUnlocked && rank <= GameData.leagueThreshold;
            this.View.EventsAccessible = tournamentActive || GameData.isEventsUnlocked && rank <= GameData.eventsThreshold;
            // END TODO
            this.View.DraftStatusWidget.Wins = this.PlayerProfile.DraftData.event_wins;
			this.View.DraftStatusWidget.Losses = this.PlayerProfile.DraftData.event_losses;
			this.View.LeagueStatusWidget.Wins = this.PlayerProfile.ConstructedData.event_wins;
			this.View.LeagueStatusWidget.Losses = this.PlayerProfile.ConstructedData.event_losses;
            this.View.EventsStatusWidget.Wins = this.PlayerProfile.TournamentData.event_wins;
            this.View.EventsStatusWidget.Losses = this.PlayerProfile.TournamentData.event_losses;
            this.View.TournamentInfoWidget.Init(this.PlayerProfile.CurrentTournament);
            int packs = 0;
            int gems = 0;
            if (this.PlayerProfile != null && this.PlayerProfile.Wallet != null)
            {
                this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.SP, out packs);
                this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.NG, out gems);
            }
            this.View.PacksUnopened = packs;

        }



        public void UpdateUiHints()
        {
            if (this.View.IsOpen)
            {
                int numberOfPacks;
                /*if (this.View.DraftAccessible && this.PlayerProfile.UiHintData.Draft == 0)
                {
                    this.View.PlayFx(this.View.DraftHintAnchor);
                    //this.View.SetGlow(this.View.DraftGlowCanvas);
                }
                else if (this.View.LeagueAccessible && this.PlayerProfile.UiHintData.League == 0)
                {
                    this.View.PlayFx(this.View.LeagueHintAnchor);
                    //this.View.SetGlow(this.View.LeagueGlowCanvas);
                }
                else if (this.View.EventsAccessible && this.PlayerProfile.UiHintData.Event == 0)
                {
                    this.View.PlayFx(this.View.EventsHintAnchor);
                    //this.View.SetGlow(this.View.EventGlowCanvas);
                }*/
                if (this.PlayerProfile.UiHintData.OpenPacks == 0 
                    && this.PlayerProfile.Wallet != null
                    && this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.SP, out numberOfPacks)
                    && numberOfPacks > 0)
                {
                    this.View.PlayFx(this.View.PacksHintAnchor);
                    //this.View.SetGlow(this.View.OpenPacksGlowCanvas);
                }
                else if (this.PlayerProfile.UiHintData.Store == 0)
                {
                    this.View.PlayFx(this.View.StoreHintAnchor);
                    //this.View.SetGlow(this.View.StoreGlowCanvas);
                }
                else if (this.PlayerProfile.UiHintData.Play == 0)
                {
                    this.View.PlayFx(this.View.PlayHintAnchor);
                    //this.View.SetGlow(this.View.CasualGlowCanvas);
                }
                else if (this.View.EventsAccessible && !View.EventActive
                    && this.PlayerProfile.Wallet != null 
                    && this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.FT, out numberOfPacks) && numberOfPacks > 0)
                {
                    this.View.PlayFx(this.View.EventsHintAnchor);
                }
                else if (this.View.LeagueAccessible && !View.LeagueActive
                    && this.PlayerProfile.Wallet != null
                    && this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.FC, out numberOfPacks) && numberOfPacks > 0)
                {
                    this.View.PlayFx(this.View.LeagueHintAnchor);
                }
                else if (this.View.DraftAccessible && !View.DraftActive
                    && this.PlayerProfile.Wallet != null
                    && this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.FD, out numberOfPacks) && numberOfPacks > 0)
                {
                    this.View.PlayFx(this.View.DraftHintAnchor);
                }
                /*Selse if (this.PlayerProfile.Wallet != null
                    && this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.SP, out numberOfPacks)
                    && numberOfPacks > 0)
                {
                    this.View.PlayFx(this.View.PacksHintAnchor);
                    //this.View.SetGlow(this.View.OpenPacksGlowCanvas);
                }*/
                else
                {
                    this.View.PlayFx(null);
                    //this.View.SetGlow(null);
                }
                /*
                UiHint hint = UiHint.NoHint;
                int hintNumber = 0;
                bool showHint = false;
                Button anchor = null; if (this.PlayerProfile.UiHintData.OpenPacks <=1 
                    && this.View.PacksHintAnchor.interactable
                    && this.PlayerProfile.Wallet != null
                    && this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.SP, out numberOfPacks) 
                    && numberOfPacks > 0)
                {
                    hint = UiHint.OpenPacks;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.PacksHintAnchor;
                    this.View._OpenPacksNotification.gameObject.SetActive(false);
                }
                else if (this.PlayerProfile.UiHintData.Store == 0 && this.View.StoreHintAnchor.interactable)
                {
                    hint = UiHint.Store;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.StoreHintAnchor;
                }
                else if(this.PlayerProfile.UiHintData.Practice <= 0 && this.View.PlayHintAnchor.interactable)
                {
                    hint = UiHint.Practice;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.PlayHintAnchor;
                }
                /*else if (this.PlayerProfile.UiHintData.DeckBuilder == 0)
                {
                    hint = UiHint.DeckBuilder;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.PlayHintAnchor;
                }*/
                /*else if (this.PlayerProfile.UiHintData.Play <= 1 && this.View.PlayHintAnchor.interactable)
                {
                    hint = UiHint.Play;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.PlayHintAnchor;
                }*/
                /*else if (this.PlayerProfile.UiHintData.Draft ==0 && this.View.DraftAccessible)
                {
                    hint = UiHint.Draft;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.DraftHintAnchor;
                }
                else if (this.PlayerProfile.UiHintData.League == 0 && this.View.LeagueAccessible)
                {
                    hint = UiHint.League;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.LeagueHintAnchor;
                }
                else if (this.PlayerProfile.UiHintData.Event == 0 && this.View.EventsAccessible)
                {
                    hint = UiHint.Event;
                    hintNumber = 0;
                    showHint = true;
                    anchor = this.View.LeagueHintAnchor;
                }
                if (showHint && hint != UiHint.NoHint && anchor != null)
                    this.ShowUiHintSignal.Dispatch(new ShowUiHintData(anchor, hint, hintNumber, NovaContext.MainCanvas, false));*/
            }
        }

        private IEnumerator _WaitThenFixFx;
        protected void OnMoveAnimatedViewToCanvas(AnimatedView view, NovaCanvas canvas, float delayTime)
        {
            if (this.View.IsOpen && _WaitThenFixFx == null)
            {
                _WaitThenFixFx = WaitThenFixFx();
                Timer.Instance.StartCoroutine(_WaitThenFixFx);
            }

        }

        private IEnumerator WaitThenFixFx()
        {
            yield return null;
            if (this.View.IsOpen && View.IsPlaying)
            {
                if (this.NovaContext.IsAlreadyBlurred)
                {
                    View.ButtonFx.EndEffect();
                    View.ButtonFx.gameObject.SetActive(false);
                }
                else
                {
                    View.ButtonFx.gameObject.SetActive(true);
                    View.ButtonFx.StartEffect();
                }
            }
            _WaitThenFixFx = null;
        }

        protected void RefreshAnnouncements()
		{
		}

		#region Signal Listeners
		protected void OnPlayerDataLoaded()
		{
			RefreshPlayer();
        }
        protected void OnPlayerProfileUpdated(ProfileSection section)
        {
            RefreshPlayer();
            if (this.View.IsOpen && _WaitToPopulateCoroutine == null)
            {
                _WaitToPopulateCoroutine = WaitToPopuateCoroutine();
                Timer.Instance.StartCoroutine(WaitToPopuateCoroutine());
            }
        }
        protected void OnLanguageChanged()
        {
            RefreshPlayer();
            RefreshAnnouncements();
        }
        #endregion

		protected void OnAnnouncementClicked()
		{
		}

        private int OffersIndex;
        private List<Economy.StoreData> OffersList;
        public void PopulateOffers()
        {
            string existingOfferId = string.Empty;
            if(OffersList != null && OffersIndex >= 0 && OffersIndex < OffersList.Count)
            {
                existingOfferId = OffersList[OffersIndex].ItemId;
                // get the existing location; reinitialize to it if possible
            }
            
            OffersList = PlayerProfile.GetOffers(View.OfferDots.Length, true);
            OffersIndex = 0;
            //Debug.LogFormat("Offers: {0}", string.Join(",", OffersList.Select(x => x.ItemId).ToArray()));
            for(int i = 0; i < View.OfferDots.Length; i++)
            {
                View.OfferDots[i].gameObject.SetActive(i < OffersList.Count);
                if (i < OffersList.Count && OffersList[i].ItemId == existingOfferId)
                    OffersIndex = i;
            }
            PlayOfferScroll(null);
        }

        Sequence offerSeq;
        private int OfferScrollDistance = 600;
        public void PlayOfferScroll(bool? scrollDown)
        {
            if (OffersList == null || OffersList.Count == 0 || offerSeq != null)
                return;
            this.View.OfferScrollDeltaTime = 0;
            if(scrollDown.HasValue)
                OffersIndex += scrollDown.Value ? 1 : -1;
            if (OffersIndex < 0)
                OffersIndex = OffersList.Count - 1;
            if (OffersIndex >= OffersList.Count)
                OffersIndex = 0;

            Economy.StoreData storeData = OffersList[OffersIndex];

            var newItem = ObjectPool.SpawnPrefab<StoreItemView>(PooledObjectType.StoreItem);
            newItem.StoreData = storeData;
            newItem.InitializeOfferItem(this.PlayerProfile, NovaIAPService.ProductList);
            //newItem.EventClicked = OnView_ItemClicked;
            // Add new offer (top or bottom)
            newItem.transform.AttachToParent(this.View.OfferHolder, 0);
            newItem.transform.localPosition = new Vector3(0, scrollDown.HasValue ? (scrollDown.Value ? OfferScrollDistance : -OfferScrollDistance) : 0, 0);
            var oldItem = View.OfferItem;
            
            offerSeq = DOTween.Sequence();
            offerSeq.Append(newItem.transform.DOBlendableLocalMoveBy(new Vector3(0, scrollDown.HasValue ? (scrollDown.Value ? -OfferScrollDistance : OfferScrollDistance) : 0, 0), scrollDown.HasValue ? 1.0f : 0.0f));
            if (oldItem != null)
            {
                offerSeq.Join(View.OfferItem.transform.DOBlendableLocalMoveBy(new Vector3(0, scrollDown.HasValue ? (scrollDown.Value ? -OfferScrollDistance : OfferScrollDistance) : 0, 0), scrollDown.HasValue ? 1.0f : 0.0f));
            }
            offerSeq.AppendCallback(() => {
                if (oldItem != null)
                    oldItem.Recycle();
                offerSeq = null;
            });
            for (int i = 0; i < OffersList.Count && i < View.OfferDots.Length; i++)
            {
                if (OffersIndex == i)
                    offerSeq.Insert(0.25f, View.OfferDots[i].DOFade(0.9f, 0.75f));
                else
                    offerSeq.Insert(0.0f, View.OfferDots[i].DOFade(0.3f, 0.75f));
            }
            View.OfferItem = newItem;
        }


        #region Signal Listeners
        private IEnumerator _WaitToPopulateCoroutine;
        private IEnumerator WaitToPopuateCoroutine()
        {
            yield return null;
            PopulateOffers();
            _WaitToPopulateCoroutine = null;
        }
        #endregion
    }
}
