﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using PlayFab.ClientModels;
using ExitGames.Client.Photon.Chat;
using LinqTools;
using strange.extensions.mediation.impl;
using System;
using NovaBlitz.UI.DataContracts;
using DG.Tweening;

namespace NovaBlitz.UI
{

    public class GlobalChatView : AnimatedView
	{       
        public Action ScrimClicked;
		public Action<string> AddFriendClicked;
        public Action<ChatMessage> ReportFriendClicked;
        public event Action<string> EventChallengeClicked;
        //public event Action<string> EventDeclineClicked;
        public System.Action<string> SubmitChatMessage; // Friend playfab id, my message
        public Queue<ChatLogEntry> ChatLogItemCache = new Queue<ChatLogEntry>();

        public NovaButton BottomButton;
        public CanvasGroup BottomButtonGroup;
        public TextMeshProUGUI BottomButtonText;
		public GameObject ChatHolder;
        public TMP_InputField ChatLogTextInput;
        public CanvasGroup ChatRoot;
        public ScrollRect ScrollRect;


        public NovaButton AddFriendButton;
        public NovaButton ReportButton;
        public RectTransform ButtonsPanel;
        /*[SerializeField] protected TextMeshProUGUI _ChallengeHeader;
		[SerializeField] protected Button _ChallengeButton;
		[SerializeField] protected TextMeshProUGUI _ChallengeLabel;
		[SerializeField] protected Button _DeclineButton;
		[SerializeField] protected TextMeshProUGUI _ChallengeNotification;*/

        public bool IsChatInteractible { get { return ChatRoot.interactable; }
            set
            {
                if(!value || ChatRoot.interactable != value)
                {
                    (this.ChatLogTextInput.placeholder as TextMeshProUGUI).text = value ? I2.Loc.ScriptLocalization.Send_Message : I2.Loc.ScriptLocalization.WaitingForServer;
                }
                ChatRoot.interactable = value;
            }
        }
        
        public PlayerProfile PlayerProfile { get; set; }
        public ChatLogEntry ChatItemPrefab { get; set; }
        private const int MaxGlobalChatItems = 100;
        public int MissedMessageCount { get; set; }

        /*private int numUnreadChatLogs = 0;
        public int NumUnreadChatLogs
        {
            get { return this.numUnreadChatLogs; }
            set
            {
                this.numUnreadChatLogs = Mathf.Max(0, value);
                //this.ChatNotificationGameObject.SetActive(numUnreadChatLogs != 0);
            }
        }*/

        /*public void PopulateView(List<ChatMessage> messages)
        {
            ClearChatItems();
            if(messages != null)
                AddMessages(messages);

            StartCoroutine(SelectInputField(this.ChatLogTextInput));
        }*/

        public void ClearChatItems()
        {
            if (_AddMessageEnumerator != null)
            {
                Timer.Instance.StopCoroutine(_AddMessageEnumerator);
                _AddMessageEnumerator = null;
            }
            messageQueue.Clear();
            while (ChatLogItemCache.Count > 0)
            {
                var item = ChatLogItemCache.Dequeue();
                item.Recycle();
            }
            LastEntryAdded = null;
        }

        private ChatLogEntry LastEntryAdded;
        private void AddChatItem(ChatMessage message)
        {
            if(ChatLogItemCache.Count >= MaxGlobalChatItems)
            {
                var toRecycle = ChatLogItemCache.Dequeue();
                if (selectedEntry == toRecycle)
                    OnChatLogClicked(null);
                toRecycle.Recycle();
            }

            var chatItem = ObjectPool.Spawn(this.ChatItemPrefab);
            ChatLogItemCache.Enqueue(chatItem);
            chatItem.transform.AttachToParent(this.ChatHolder.transform, this.ChatHolder.transform.childCount);
            chatItem.transform.localScale = Vector3.one;
            chatItem.transform.localPosition = Vector3.zero;
            chatItem.GlobalChatView = this;
            chatItem.ElementClicked = OnChatLogClicked;
            chatItem.SetChatMessage(message, LastEntryAdded);
            LastEntryAdded = chatItem;
            // reposition the button panel, if it's open and this message is being added to the group
            if (selectedEntry != null && chatItem.grouping == selectedEntry.grouping && ButtonsPanel.gameObject.activeSelf)
            {
                ButtonsPanel.gameObject.SetActive(false);
                selectedEntry = null;
                OnChatLogClicked(chatItem);
            }
            //else
                //MaintainScrollPosition(chatItem.transform as RectTransform);
        }

        public void OnScrollValueChanged(Vector2 newValue)
        {
            if(ScrollRect.verticalNormalizedPosition == 0 && this.BottomButtonGroup.blocksRaycasts)
            {
                HideBottomButton();
            }
        }

        public void HideBottomButton()
        {
            BottomButtonGroup.DOKill();
            BottomButtonGroup.DOFade(0.0f, 0.5f);
            this.BottomButtonGroup.blocksRaycasts = false;
        }

        public void ShowBottomButton()
        {
            BottomButtonGroup.DOKill();
            BottomButtonGroup.DOFade(1.0f, 0.5f);
            this.BottomButtonGroup.blocksRaycasts = true;
        }

        public void GoToBottom()
        {
            ScrollRect.DOKill();
            OnChatLogClicked(null);
            DOTween.To(x => ScrollRect.verticalNormalizedPosition = x, ScrollRect.verticalNormalizedPosition, 0.0f, 0.5f);
            HideBottomButton();
        }

        /*private void MaintainScrollPosition(RectTransform addedEntry)
        {
            // ignore if we're at the bottom, without a button open
            float scrollposition = ScrollRect.verticalNormalizedPosition;
            if (scrollposition > 0 || ButtonsPanel.gameObject.activeSelf)
            {

                LayoutRebuilder.ForceRebuildLayoutImmediate(ScrollRect.transform as RectTransform);
                float updatedScrollposition = ScrollRect.verticalNormalizedPosition;
                var holderHeight = (ChatHolder.transform as RectTransform).rect.height;
                var addedHeight = addedEntry == null ? 0 : addedEntry.rect.height;

                var newScrollPosition = Math.Min(1.0f, Math.Max(0.0f, scrollposition));
                Debug.LogFormat("old pos{0} new pos:{1} fixed pos:{2}, currenth:{3}, itemh:{4}", scrollposition, updatedScrollposition, newScrollPosition, holderHeight, addedHeight);

                ScrollRect.verticalNormalizedPosition = newScrollPosition;
            }
        }*/

        public void AddMessages(List<ChatMessage> messages,bool isCombined)
        {
            if(isCombined && LastEntryAdded != null)
            {
                LastEntryAdded.SetChatMessage(LastEntryAdded.ChatMessage, null);
            }
            foreach(var message in messages)
            {
                messageQueue.Enqueue(message);
                newMessageCount++;
            }
            if(_AddMessageEnumerator == null)
            {
                _AddMessageEnumerator = AddMessageEnumerator();
                Timer.Instance.StartCoroutine(_AddMessageEnumerator);
            }
        }

        private Queue<ChatMessage> messageQueue = new Queue<ChatMessage>();
        private IEnumerator _AddMessageEnumerator;
        private int newMessageCount;

        public IEnumerator AddMessageEnumerator()
        {
            bool isBottomShown = false;
            while(messageQueue.Count > 0)
            {
                // 100+ = 0.02s, 10- = 0.1f
                float delay = Math.Min(0.1f, Math.Max(0.01f, 0.7f/messageQueue.Count));
                yield return new WaitForSecondsRealtime(delay);
                if(!isBottomShown && messageQueue.Count > 0 && (ButtonsPanel.gameObject.activeSelf || ScrollRect.verticalNormalizedPosition != 0.0f))
                {
                    ShowBottomButton();
                    isBottomShown = true;
                }
                if(!ButtonsPanel.gameObject.activeSelf)
                {
                    if (messageQueue.Count > 0)
                    {
                        AddChatItem(messageQueue.Dequeue());
                    }
                }
                if(newMessageCount > 1)
                    BottomButtonText.SetText(I2.Loc.ScriptLocalization.New_Messages, newMessageCount);
                else
                    BottomButtonText.SetText(I2.Loc.ScriptLocalization.New_Message);
            }
            _AddMessageEnumerator = null;
            newMessageCount = 0;
        }
		
        /// <summary>
        /// When someone clicks the scrim that appears during the Challenge friend flow
        /// </summary>
        public void OnScrimClicked()
		{
            if (this.ScrimClicked != null)
                this.ScrimClicked.Invoke();
		}
        ChatLogEntry selectedEntry;

        public void OnChatLogClicked(ChatLogEntry entry)
        {
            if (entry == null || (ButtonsPanel.gameObject.activeSelf && selectedEntry != null && entry.grouping == selectedEntry.grouping))
            {
                selectedEntry = null;
                ButtonsPanel.gameObject.SetActive(false);
            }
            else
            {
                var newSelected = ChatLogItemCache.LastOrDefault(c => c.grouping == entry.grouping);
                if (newSelected != null)
                { 
                    selectedEntry = newSelected;
                    // position pane
                    ButtonsPanel.gameObject.SetActive(true);
                    int currentIndex = ButtonsPanel.transform.GetSiblingIndex();
                    int newIndex = selectedEntry.transform.GetSiblingIndex();
                    newIndex += currentIndex > newIndex ? 1 : 0;
                    ButtonsPanel.SetSiblingIndex(newIndex);

                    var SenderPlayFabId = selectedEntry.ChatMessage != null ? selectedEntry.ChatMessage.SenderPlayFabId : this.PlayerProfile.MetaProfile.PlayFabID;
                    AddFriendButton.gameObject.SetActive(this.PlayerProfile.MetaProfile.PlayFabID != SenderPlayFabId && !this.PlayerProfile.FriendListData.ContainsKey(SenderPlayFabId));
                    ReportButton.gameObject.SetActive(this.PlayerProfile.MetaProfile.PlayFabID != SenderPlayFabId);
                }
                else
                {
                    selectedEntry = null;
                    ButtonsPanel.gameObject.SetActive(false);
                }
            }

            //MaintainScrollPosition(entry == null ? null : entry.transform as RectTransform);
        }

        public void OnAddFriendClicked()
        {
            if (AddFriendClicked != null && this.selectedEntry != null && this.selectedEntry.ChatMessage != null)
            {
                AddFriendClicked.Invoke(this.selectedEntry.ChatMessage.SenderPlayFabId);
            }
                OnChatLogClicked(null);
        }

        public void OnReportFriendClicked()
        {
            if (ReportFriendClicked != null && this.selectedEntry != null && this.selectedEntry.ChatMessage != null)
            {
                ReportFriendClicked.Invoke(this.selectedEntry.ChatMessage);
            }
                OnChatLogClicked(null);
        }


        /// <summary>
        /// Delays a fraction of a second before activating an input field
        /// </summary>
        public IEnumerator SelectInputField(TMP_InputField inputField)
		{
			yield return new WaitForSecondsRealtime(0.3f);
			inputField.ActivateInputField();
            inputField.Select();
        }

		/// <summary>
		/// Triggered when the chat input loses focus, if it came from an enter being pressed.. send the chat
		/// and keep the focus on the input box
		/// </summary>
		public void EndChatEdit()
		{
			if(Input.GetKeyDown(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !ChatLogTextInput.wasCanceled))
			{
				DoSubmitChatMessage();
                StartCoroutine(SelectInputField(ChatLogTextInput));
				//this.ChatLogTextInput.ActivateInputField();
			}
		}

		/// <summary>
		/// Used by the UI to signle adding a message typed in by the user to a chat
		/// <summary>
		private void DoSubmitChatMessage()
		{
			string message = this.ChatLogTextInput.text.Trim();
			this.ChatLogTextInput.text = string.Empty;
            //this.ScrollRect.transform.LayoutUpdater.ForceLayoutImmediate();
            // Only send chat messsages with content
            if (!string.IsNullOrEmpty(message) && SubmitChatMessage != null)
			{
				SubmitChatMessage.Invoke(message);
            }
            this.ScrollRect.verticalNormalizedPosition = 0;
            OnChatLogClicked(null);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ClearChatItems();
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
    public class GlobalChatMediator : Mediator
    {
		[Inject] public GlobalChatView View {get;set;}
        
        [Inject] public GameData GameData { get; set; }
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public UIConfiguration UIConfiguration { get; set; }
		[Inject] public PlayFabChatClient PlayFabChatClient {get;set;}

		[Inject] public GlobalChatMessageAddedSignal GlobalChatMessagesAddedSignal { get;set;}
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
		[Inject] public LogEventSignal LogEventSignal {get;set;}
		[Inject] public SendChatMessageSignal SendChatMessageSignal {get;set;}
        [Inject] public ConnectToPhotonChatSignal ConnectToPhotonChatSignal { get; set; }
    [Inject] public ConnectedToGlobalChatSignal ConnectedToGlobalChatSignal { get; set; }
        
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }
        
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        
		[Inject] public AddFriendSignal AddFriendSignal{get;set;}
        [Inject] public ToFriendChallengeSignal ToFriendChallengeSignal { get; set; }
		[Inject] public BlockFriendSignal BlockFriendSignal {get;set;}
        /*
        

		[Inject] public FriendStatusUpdatedSignal FriendStatusUpdatedSignal {get;set;}
		[Inject] public AddFriendResponseSignal AddFriendResponseSignal {get;set;}

        [Inject] public ConfirmFriendSignal ConfirmFriendSignal { get; set; }
        [Inject] public ConfirmFriendResponseSignal ConfirmFriendResponseSignal { get; set; }
        [Inject] public RemoveFriendSignal RemoveFriendSignal {get;set;}
		[Inject] public RemoveFriendResponseSignal RemoveFriendResponseSignal{get;set;}
		[Inject] public BlockFriendResponseSignal BlockFriendResponseSignal {get;set;}
		[Inject] public UnblockFriendSignal UnblockFriendSignal { get;set;}
		[Inject] public UnblockFriendResponseSignal UnblockFriendResponseSignal { get;set;}
        [Inject] public DeclineFriendRequestSignal DeclineFriendRequestSignal { get; set; }
        [Inject] public DeclineFriendResponseSignal DeclineFriendResponseSignal { get; set; }

        [Inject] public ChallengeFriendSignal ChallengeFriendSignal {get;set;}
		[Inject] public FriendChallengeSignal FriendChallengeSignal {get;set;}
		[Inject] public FriendChallengeCanceledSignal FriendChallengeCanceledSignal {get;set;}
		[Inject] public FriendChallengeDeclinedSignal FriendChallengeDeclinedSignal {get;set;}
		[Inject] public AcceptFriendChallengeSignal AcceptFriendChallengeSignal {get;set;}
		[Inject] public DeclineFriendChallengeSignal DeclineFriendChallengeSignal {get;set;}

        [Inject] public FriendNotificationSignal FriendNotificationSignal { get; set; }
        
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}


        [Inject] public HomeScreen HomeScreen { get; set; }
        */

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister ()
		{
            this.View.EventChallengeClicked += this.OnFriendChallengeClicked;
			this.View.AddFriendClicked += this.OnAddFriendClicked;
            this.View.ReportFriendClicked += this.OnReportFriendClicked;
            this.View.SubmitChatMessage += this.OnSubmitChatMessage;
            this.View.ScrimClicked += OnScrimClicked;

            this.GlobalChatMessagesAddedSignal.AddListener(OnChatMessagesAdded);
			this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.ConnectedToGlobalChatSignal.AddListener(this.OnGlobalChatConnected);

            this.View.ChatItemPrefab = UIConfiguration.GlobalChatPrefab;
            this.View.PlayerProfile = PlayerProfile;
            this.View.ScrollRect.onValueChanged.AddListener(this.View.OnScrollValueChanged);
            
            // remove this if we initialize this view earlier
            //OnOpenAnimatedView(this.View.GetType());
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove ()
		{
            this.View.EventChallengeClicked -= this.OnFriendChallengeClicked;
			this.View.AddFriendClicked -= this.OnAddFriendClicked;
            this.View.ReportFriendClicked -= this.OnReportFriendClicked;
            this.View.SubmitChatMessage -= this.OnSubmitChatMessage;
            this.View.ScrimClicked -= OnScrimClicked;

            this.View.ScrollRect.onValueChanged.RemoveListener(this.View.OnScrollValueChanged);

            this.GlobalChatMessagesAddedSignal.RemoveListener(OnChatMessagesAdded);
			this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.ConnectedToGlobalChatSignal.RemoveListener(this.OnGlobalChatConnected);

            if(PlayFabChatClient.chatClient != null)
                PlayFabChatClient.chatClient.Unsubscribe(PlayFabChatClient.chatClient.PublicChannels.Keys.ToArray());
        }

		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                this.View.OnChatLogClicked(null);
                this.View.BottomButtonGroup.alpha = 0;
                this.View.BottomButtonGroup.blocksRaycasts = false;
                this.View.ClearChatItems();
                this.PlayerProfile.isInitializingGlobalChat = -1;
                this.View.IsChatInteractible = false;
                this.ConnectToPhotonChatSignal.Dispatch(string.Empty, new ChatMessage(),true);
                this.View.MissedMessageCount = 0;
                // connect to global chat and fetch messages
                // populate the chat window

                //this.View.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed, false);

                this.LogEventSignal.Dispatch (new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_FRIENDS, 
					IsTimedEvent = true, 
					IsTimedEventOver = false
				});
			}
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                this.LogEventSignal.Dispatch (new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_FRIENDS, 
					IsTimedEvent = true, 
					IsTimedEventOver = true
				});
            }
        }

        private void OnScrimClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
        }

        protected void OnLanguageChanged()
        {
            //this.View.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed, false);
            //this.View.UpdateFriendList(this.PlayerProfile.FriendListData);
        }

        #region View Listeners

        protected void OnChatOpened(string playfabID)
		{

		}

        private void OnGlobalChatConnected()
        {
            this.View.IsChatInteractible = true;
        }

        protected void OnFriendChallengeClicked(string playfabID)
        {
            NovaBlitzFriend friend = this.PlayerProfile.GetFriend(playfabID);
            if (null != friend && friend.ChatUserStatus == ChatUserStatus.Online)
            {
                this.PlayerProfile.SelectedChallengeID = playfabID;
                this.CloseAnimatedViewSignal.Dispatch(typeof(FriendListDockView));
                this.ToFriendChallengeSignal.Dispatch(playfabID);
            }
        }

		/// <summary>
		/// When the "[+]" button in the title of the friends list bar has been clicked
		/// </summary>
		private void OnAddFriendClicked(string playFabId)
		{
			this.AddFriendSignal.Dispatch(playFabId, true);
		}

        private void OnReportFriendClicked(ChatMessage message)
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(ReportBugView),
                    (v) => { ((ReportBugView)v).Initialize(ReportType.ReportPlayerGlobal, message, this.PlayerProfile.GlobalChatLog); });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ReportBugView));
        }
        
        /// <summary>
        /// Handles when the "block" button is clicked, on friend confirm
        /// </summary>
        private void OnBlockFriendRequestClicked(string playfabId)
        {
            this.BlockFriendSignal.Dispatch(playfabId);
        }

        

		/// <summary>
		/// Handles when a user clicks Bock on a friend's chat log
		/// </summary>
        private void OnBlockFriendClicked(string playfabId)
        {
            this.BlockFriendSignal.Dispatch(playfabId);
        }

		/// <summary>
		/// chat message submitted though the UI
		/// </summary>
		private void OnSubmitChatMessage(string message)
		{
            //Debug.LogFormat("submitting chat message {0}", message);
            AvatarDataContract avContract = null;
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.AvatarID))
                this.GameData.AvatarByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.AvatarID, out avContract);
            var chatMessage = new ChatMessage(this.PlayerProfile.MetaProfile.PlayFabID, avContract == null ? string.Empty : avContract.ArtId, this.PlayerProfile.MetaProfile.Name, message, true);
			this.SendChatMessageSignal.Dispatch(this.PlayerProfile.GlobalChatRoom, chatMessage, false);
            this.PlayerProfile.isInitializingGlobalChat = 1;
        }
#endregion
        
        /// <summary>
        /// Listener for the ChatMessageAdded signal, adds the message to the log if the correct log is visible
        /// </summary>
        private void OnChatMessagesAdded(List<ChatMessage> chatMessages, bool isCombined)
        {
            if (!this.View.IsOpen)
            {
                this.View.MissedMessageCount += chatMessages.Count;
                if (this.View.MissedMessageCount > 100)
                    PlayFabChatClient.chatClient.Unsubscribe(PlayFabChatClient.chatClient.PublicChannels.Keys.ToArray());

            }
            else
            {
                this.View.IsChatInteractible = true;
                //Debug.LogFormat("received {0} chat messages", chatMessages.Count);
                //if (this.PlayerProfile.isInitializingGlobalChat)
                //    this.View.PopulateView(chatMessages);
                //else
                    this.View.AddMessages(chatMessages,isCombined);
            }
		}
        
    }
}
