﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using System;
using NovaBlitz.Game;
using Messages;
using NovaBlitz.UI;

namespace NovaBlitz.UI.Mediators
{
    public class ProgressLoadingScreenMediator : Mediator
    {
		[Inject] public ProgressLoadingScreenView View {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
        [Inject] public ProgressScreenClosedSignal ProgressScreenClosedSignal {get;set;}
        [Inject] public ShowCancelProgressButtonSignal ShowCancelProgressButtonSignal {get;set;}
        [Inject] public HideCancelProgressButtonSignal HideCancelProgressButtonSignal {get;set;}
        [Inject] public SetProgressScreenStatusLabelSignal SetProgressScreenStatusLabelSignal {get;set;}
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public ClientService ClientService { get; set; }



        private System.Action onCancelClicked;

        override public void OnRegister()
		{
			this.View.CancelClicked += this.OnCancelClicked;
            this.View.EventAnimClose += this.OnLoadingScreenClosed;
			this.AsyncProgressSignal.AddListener(this.OnAsyncProgress);
            this.ShowCancelProgressButtonSignal.AddListener(this.OnShowCancelButton);
            this.HideCancelProgressButtonSignal.AddListener(this.OnHideCancelButton);
            this.SetProgressScreenStatusLabelSignal.AddListener(this.OnSetStatusLabel);
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
        }
        
        override public void OnRemove()
		{
			this.View.CancelClicked -= this.OnCancelClicked;
            this.View.EventAnimClose -= this.OnLoadingScreenClosed;
			this.AsyncProgressSignal.RemoveListener(this.OnAsyncProgress);
            this.ShowCancelProgressButtonSignal.RemoveListener(this.OnShowCancelButton);
            this.HideCancelProgressButtonSignal.RemoveListener(this.OnHideCancelButton);
            this.SetProgressScreenStatusLabelSignal.RemoveListener(this.OnSetStatusLabel);
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
        }

        private void OnOpenAnimatedView(Type viewType)
        {
            // This is a little bit of an adjustment to make sure the ProgressLoadingScreen stays over top
            // of the StartingHand view loading behind it
            if(viewType == typeof(StartingHandView))
            {
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
            }
            else if (viewType == typeof(ProgressLoadingScreenView))
            {
                this.ShowCancelProgressButtonSignal.RemoveListener(this.OnShowCancelButton);
                this.ShowCancelProgressButtonSignal.AddListener(this.OnShowCancelButton);
            }
        }

        private void OnSetStatusLabel(string text)
        {
            this.View.StatusText = text;
        }

        private void OnHideCancelButton()
        {
            this.ShowCancelProgressButtonSignal.RemoveListener(OnShowCancelButton);
            this.onCancelClicked = null;
            this.View.CancelButton.interactable = false;
            this.View.CancelButton.gameObject.SetActive(false);
        }

        private void OnShowCancelButton(Action onCancleButtonClicked)
        {
            var startGame = (StartGameDialogView)this.ViewReferences.Get(typeof(StartGameDialogView));
            if(startGame == null || !startGame.IsOpen)
            {
                this.View.CancelButton.gameObject.SetActive(true);
                this.View.CancelButton.interactable = true;
                this.onCancelClicked = onCancleButtonClicked;
            }
            else if(this.View.isActiveAndEnabled)
            {
                // Do this to ensure we're still connected and things are moving forward. If we don't get an ack, the disconnect panel pops.
                StartCoroutine(PingServerUntilGameStarts());
            }
        }

        private IEnumerator PingServerUntilGameStarts()
        {
            while(this.View.IsOpen)
            {
                yield return new WaitForSecondsRealtime(1);
                this.ClientService.SendMessage(new KeepAliveMessage());
            }
        }

        private void OnLoadingScreenClosed()
        {
            this.ProgressScreenClosedSignal.Dispatch();
            this.onCancelClicked = null;
        }

        private void OnCancelClicked()
        {
            if(this.onCancelClicked != null)
            {
                this.onCancelClicked.Invoke();
            }
            
            this.View.CancelButton.interactable = false;
        }

        private void OnAsyncProgress(AsyncProgress progressType, float progress)
        {
            this.View.SetProgress(progressType, 1.0f);
            this.View.SetProgress(progressType, progress);
		}
    }
}
