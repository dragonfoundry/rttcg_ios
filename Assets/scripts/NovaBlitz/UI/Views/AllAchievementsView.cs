using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LinqTools;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	public class AllAchievementsView : View
	{
		public Action OnPopuplateView;
		public MultiAchievementView MultiView;
        protected override void OnDestroy()
        {
            base.OnDestroy();
            MultiView.Purge();
        }

		public void PopulateView()
		{
			if (OnPopuplateView != null)
				OnPopuplateView.Invoke ();
		}
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class AllAchievementsMediator : Mediator
	{
		[Inject] public AllAchievementsView View {get;set;}

		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}

		public override void OnRegister()
		{
			//PopulateView();
			this.View.OnPopuplateView += PopulateView;
        }

		public override void OnRemove()
		{
			this.View.OnPopuplateView -= PopulateView;
        }

		protected void PopulateView()
		{
			if (null != this.GameData.AchievementDataCache)
            {
                foreach(var ach in this.GameData.AchievementDataCache)
                {
                    int score;
                    int level;
                    this.PlayerProfile.UserStatistics.TryGetValue(ach.Key, out score);
                    this.PlayerProfile.UserStatistics.TryGetValue("ach_" + ach.Key, out level);
                    ach.Value.oldLevel = level;
                    ach.Value.newLevel = level;
                    ach.Value.oldScore = score;
                    ach.Value.newScore = score;
                }
                
				var views = this.View.MultiView.Populate(this.GameData.AchievementDataCache.Values);

                var orderedViews = views.OrderBy(v => v.Level).ThenBy(v => v.Rating).ToList();
                foreach (var view in orderedViews)
                {
                    view.gameObject.transform.SetAsFirstSibling();
                }
            }
        }
	}
}
