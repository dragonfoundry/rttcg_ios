﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class NewConfirmDialogMediator : Mediator 
	{
		[Inject] public NewConfirmDialogView View {get;set;}
		[Inject] public NewConfirmDialogDismissedSignal NewConfirmDialogDismissed {get;set;}
        [Inject] public ServerResponseSignal ServerResponseSignal { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
		public override void OnRegister()
		{
			this.View.DialogDismissed += OnDismissed;
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
		{
			this.View.DialogDismissed -= OnDismissed;
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
              
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {

            }
        }
       
        private void OnDismissed(bool isConfirmed)
		{
            if (isConfirmed && this.View.IsWaitForResponse)
            {
                this.View.MessageLabel.text = I2.Loc.ScriptLocalization.WaitingForServer;
                this.View.YesButton.interactable = false;
                this.View.NoButton.interactable = false;
                ServerResponseSignal.AddOnce(OnServerResponse);
            }
            else
            {
			    this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
            }

			this.NewConfirmDialogDismissed.Dispatch(isConfirmed);
		}

        private void OnServerResponse(bool success)
        {
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
        }
	}
}
