using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class CardFilterView : AnimatedView
	{
		public CardFilterWidget FilterWidget;
	}
}

namespace NovaBlitz.UI.Mediators
{
	public class CardFilterMediator : Mediator
	{
		[Inject] public CardFilterView View {get;set;}

		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public FilterCardsSignal FilterCardsSignal {get;set;}
		[Inject] public CardAddedToDeckSignal CardAddedToDeckSignal {get;set;}
		[Inject] public CardRemovedFromDeckSignal CardRemovedFromDeckSignal {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }

		/// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
		public override void OnRegister()
		{
			this.View.FilterWidget.EventApplyClicked += OnFilterApplyClicked;
			this.CardAddedToDeckSignal.AddListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.AddListener(OnCardRemovedFromDeck);
			this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);

			// Match the filter state at startup
			this.MatchFilterState();
		}

		/// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
		{
			this.View.FilterWidget.EventApplyClicked -= OnFilterApplyClicked;
			this.CardAddedToDeckSignal.RemoveListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.RemoveListener(OnCardRemovedFromDeck);
			this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
		}


		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
				this.MatchFilterState();
			}
		}

		protected void MatchFilterState()
		{
			if (null != this.PlayerProfile.CurrentCardFilter)
				this.View.FilterWidget.MatchState(this.PlayerProfile.CurrentCardFilter);
		}

		#region View Listeners

		protected void OnFilterApplyClicked(CardFilterParams filterParams)
		{
			if (null != this.PlayerProfile.CurrentCardFilter)
				filterParams.SearchPhrase = this.PlayerProfile.CurrentCardFilter.SearchPhrase; //ignore search field
			this.FilterCardsSignal.Dispatch(filterParams);
		}
		#endregion

		#region Signal Listeners

		private void OnCardAddedToDeck(CardData card, DeckData deck)
		{
			this.View.FilterWidget.LockInAspects(deck.Aspects, this.PlayerProfile, card.Aspect);
		}

		private void OnCardRemovedFromDeck(CardData card, DeckData deck)
		{
			this.View.FilterWidget.LockInAspects(deck.Aspects, this.PlayerProfile, CardAspect.NoAspect);
		}
        #endregion
    }
}
