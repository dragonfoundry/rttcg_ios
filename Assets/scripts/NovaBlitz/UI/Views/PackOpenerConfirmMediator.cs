﻿using UnityEngine;
using LinqTools;
using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;
using Messages;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class PackOpenerConfirmMediator : Mediator
    {
		[Inject] public PackOpenerConfirmView View { get; set; }
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public ShowPurchaseOffersSignal ShowPurchaseOffersSignal { get; set; }

        [Inject] public GameData GameData { get; set; }
		[Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public VirtualPurchaseSignal VirtualPurchaseSignal { get; set; }
		[Inject] public InventoryLoadedSignal InventoryLoadedSignal { get; set; }
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public SaveUiHintDataSignal SaveUiHintDataSignal { get; set; }
		[Inject] public ViewReferences ViewReferences {get;set;}

        public const string OWNED_CARDS_FORMAT = "{0}<size=-15><color=#666666>/{1}</color></size>";


        public int commonOwned = 0;
        public int rareOwned = 0;
        public int epicOwned = 0;
        public int legendaryOwned = 0;

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>

        public override void OnRegister()
        {
            this.View.Reinitialize += Initialize;
            this.View.BuyBasicPack += OnOpenPack;
            this.View.BuySmartPack += OnOpenPack;
            this.View.BuyMegaPack += OnOpenPack;
            this.View.OpenSmartPack += OnOpenPack;
            this.View.OpenSmartPack += CheckSmartPackHint;
            this.View.DialogDismissed += OnDialogDismissed;
            this.InventoryLoadedSignal.AddListener(OnInventoryLoaded);
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.MoveAnimatedViewToCanvasSignal.AddListener(this.OnMoveAnimatedViewToCanvas);

            this.Initialize();
            this.UpdateUiHints();
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.Reinitialize -= Initialize;
            this.View.BuyBasicPack -= OnOpenPack;
            this.View.BuySmartPack -= OnOpenPack;
            this.View.BuyMegaPack -= OnOpenPack;
            this.View.OpenSmartPack -= OnOpenPack;
            this.View.OpenSmartPack -= CheckSmartPackHint;
            this.View.DialogDismissed -= OnDialogDismissed;
            this.InventoryLoadedSignal.RemoveListener(OnInventoryLoaded);
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.MoveAnimatedViewToCanvasSignal.RemoveListener(this.OnMoveAnimatedViewToCanvas);

            Timer.Instance.StopCoroutine(PlayFxCoroutine());
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                this.Initialize();
                this.UpdateUiHints();
            }
        }


        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
      
            }
        }

        public void Initialize()
        {
            Dictionary<string, StoreData> store;
            if (this.GameData.StoreCache == null || !this.GameData.StoreCache.TryGetValue(NBEconomy.CATALOG_BOOSTER, out store) || store.Count== 0)
            {
                Debug.LogError("store cache doesn't contain booster store");
                this.View.BuyBasicButton.interactable = false;
                View.BuySmartButton.interactable = false;
                View.OpenSmartButton.gameObject.SetActive(false);
                View.BuyMegaButton.interactable = false;
                return;
            }

            // Set the title based on the setId - this is a little
            this.View.PackStore = store;
 
            uint BPcreditPrice = 0;
            uint SPnovaGemPrice = 0;
            uint SPsmartPackPrice = 0;
            uint MPnovaGemPrice = 0;

            StoreData basicPack;
            if (null != store && store.TryGetValue(NBEconomy.ITEMID_BASIC_PACK, out basicPack))
            {
                this.View.BasicPackData = basicPack;
                if (basicPack.VirtualPrices.TryGetValue(CurrencyType.CR.ToString(), out BPcreditPrice))
                {
                    View.BasicPackPrice.text = BPcreditPrice.ToString();
                    this.View.BuyBasicButton.interactable = true;
                }
                else
                {
                    View.BasicPackPrice.text = string.Empty;
                    this.View.BuyBasicButton.interactable = false;
                }
            }
            StoreData smartPack;
            if (null != store && store.TryGetValue(NBEconomy.ITEMID_SMART_PACK, out smartPack))
            {
                this.View.SmartPackData = smartPack;
                if (smartPack.VirtualPrices.TryGetValue(CurrencyType.NG.ToString(), out SPnovaGemPrice))
                {
                    View.SmartPackPrice.text = SPnovaGemPrice.ToString();
                    View.BuySmartButton.interactable = true;
                }
                else
                {
                    View.SmartPackPrice.text = string.Empty;
                    View.BuySmartButton.interactable = false;
                }
                if (smartPack.VirtualPrices.TryGetValue(CurrencyType.SP.ToString(), out SPsmartPackPrice))
                {
                    View.OpenSmartButton.interactable = true;
                }
                else
                {
                    View.OpenSmartButton.interactable = false;
                }
            }
            StoreData megaPack;
            if (null != store && store.TryGetValue(NBEconomy.ITEMID_MEGA_PACK, out megaPack))
            {
                this.View.MegaPackData = megaPack;
                if (megaPack.VirtualPrices.TryGetValue(CurrencyType.NG.ToString(), out MPnovaGemPrice))
                {
                    View.MegaPackPrice.text = MPnovaGemPrice.ToString();
                    View.BuyMegaButton.interactable = true;
                }
                else
                {
                    View.MegaPackPrice.text = string.Empty;
                    View.BuyMegaButton.interactable = false;
                }
            }

            // Set the buy button text & price/packs owned info based on the player's inventory
            int smartPacks;
            int gems;
            if (PlayerProfile.Wallet != null 
                && PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.SP, out smartPacks) 
                && SPsmartPackPrice > 0 
                && smartPacks >= (int)SPsmartPackPrice)
            {
                PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.NG, out gems);
                View.PacksOwned.text = (smartPacks).ToString();
                View.OpenSmartButton.gameObject.SetActive(true);
                View.PacksOwnedImage.gameObject.SetActive(true);
            }
            else
            {
                View.OpenSmartButton.gameObject.SetActive(false);
            }
            
            // Set the owned numbers based on player inventory
            SetOwnedNumber();
        }


        private void SetOwnedNumber()
        {

            int commonTotal = 0;
            int newCommonOwned = 0;
            int rareTotal = 0;
            int newRareOwned = 0;
            int epicTotal = 0;
            int newEpicOwned = 0;
            int legendaryTotal = 0;
            int newLegendaryOwned = 0;
            //var cards = this.GameData.CardProductCache.Keys.Where(p => p.ItemClass == NBEconomy.ITEMCLASS_CARD);
            //var setList = this.GameData.CardProductCache.Where(c => c.Value.Tags.Contains(View.SetId));
            int owned = 0;
            CardData card;
            foreach (var cardData in this.GameData.CardProductCache) // Need to tag all the cards with their set, or find some easy way of going back this way...
            {
                PlayerProfile.OwnedCards.TryGetValue(cardData.Key, out owned);
                GameData.CardDictionary.TryGetValue(cardData.Key, out card);
                if (card.Rarity == CardRarity.Common)
                {
                    commonTotal += 3;
                    if (owned > 0)
                        newCommonOwned += Math.Min(3, owned);
                }
                else if (card.Rarity == CardRarity.Rare)
                {
                    rareTotal += 3;
                    if (owned > 0)
                        newRareOwned += Math.Min(3, owned);
                }
                else if (card.Rarity == CardRarity.Epic)
                {
                    epicTotal += 3;
                    if (owned > 0)
                        newEpicOwned += Math.Min(3, owned);
                }
                else if (card.Rarity == CardRarity.Legendary)
                {
                    legendaryTotal += 3;
                    if (owned > 0)
                        newLegendaryOwned += Math.Min(3, owned);
                }
            }
            if (newCommonOwned <= commonOwned)
                View.CommonNew.gameObject.SetActive(false);
            else
                View.CommonNew.text = "+" + (newCommonOwned - commonOwned);
            if (newRareOwned <= rareOwned)
                View.RareNew.gameObject.SetActive(false);
            else
                View.RareNew.text = "+" + (newRareOwned - rareOwned);
            if (newEpicOwned <= epicOwned)
                View.EpicNew.gameObject.SetActive(false);
            else
                View.EpicNew.text = "+" + (newEpicOwned - epicOwned);
            if (newLegendaryOwned <= legendaryOwned)
                View.LegendaryNew.gameObject.SetActive(false);
            else
                View.LegendaryNew.text = "+" + (newLegendaryOwned - legendaryOwned);
            
            View.CommonNew.gameObject.SetActive(newCommonOwned - commonOwned > 0);
            View.RareNew.gameObject.SetActive(newRareOwned - rareOwned > 0);
            View.EpicNew.gameObject.SetActive(newEpicOwned - epicOwned > 0);
            View.LegendaryNew.gameObject.SetActive(newLegendaryOwned - legendaryOwned > 0);

            commonOwned = newCommonOwned;
            rareOwned = newRareOwned;
            epicOwned = newEpicOwned;
            legendaryOwned = newLegendaryOwned;

            View.CommonOwned.SetText(OWNED_CARDS_FORMAT, commonOwned, commonTotal);
            View.RareOwned.SetText(OWNED_CARDS_FORMAT, rareOwned, rareTotal);
            View.EpicOwned.SetText(OWNED_CARDS_FORMAT, epicOwned, epicTotal);
            View.LegendaryOwned.SetText(OWNED_CARDS_FORMAT, legendaryOwned, legendaryTotal);
        }
        
        protected void UpdateUiHints()
        {
            Timer.Instance.StopCoroutine(PlayFxCoroutine());
            Timer.Instance.StartCoroutine(PlayFxCoroutine());
        }

        private IEnumerator PlayFxCoroutine()
        {
            yield return null;
            if (/*this.PlayerProfile.UiHintData.OpenPacks <= 1
                && */this.View.OpenSmartButton.isActiveAndEnabled
                && this.View.OpenSmartButton.interactable)
            {
                this.View.ButtonFx.gameObject.SetActive(true);
                while (!this.View.ButtonFx.gameObject.activeInHierarchy)
                {
                    yield return null;
                }
                this.View.ButtonFx.StartEffect();
                IsPlaying = true;
            }
            else
            {
                this.View.ButtonFx.EndEffect();
                this.View.ButtonFx.gameObject.SetActive(false);
                IsPlaying = false;
            }
        }

        private bool IsPlaying { get; set; }
        protected void OnMoveAnimatedViewToCanvas(AnimatedView view, NovaCanvas canvas, float delayTime)
        {
            if (view == this.View)
            {
                if (canvas == NovaCanvas.Main)
                {
                    View.ButtonFx.EndEffect();
                    View.ButtonFx.gameObject.SetActive(false);
                }
                else
                {
                    View.ButtonFx.gameObject.SetActive(true);
                    View.ButtonFx.StartEffect();
                }
            }
        }

        private IEnumerator WaitThenFixFx(bool isOverlay)
        {
            yield return null;
            if (IsPlaying)
            {
                if (!isOverlay)
                {
                    View.ButtonFx.EndEffect();
                    View.ButtonFx.gameObject.SetActive(false);
                }
                else
                {
                    View.ButtonFx.gameObject.SetActive(true);
                    View.ButtonFx.StartEffect();
                }
            }
        }

        public void OnInventoryLoaded()
        {
            this.Initialize();
        }
        
        private void OnOpenPack(CurrencyType currency, StoreData pack)
        { 
            int balance;
            this.PlayerProfile.Wallet.Currencies.TryGetValue(currency, out balance);
            uint price;
            if (!pack.VirtualPrices.TryGetValue(currency.ToString(), out price) || price <= 0)
            {
                Debug.LogErrorFormat("Pack {0} price not found for {1}", pack.ItemId, currency.ToString());
                return;
            }
            else if (balance < price)
            {
                if (currency == CurrencyType.CR)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.CreditsOffer);
                }
                else if (currency == CurrencyType.NG)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.GemsOffer);
                }
                /*if (currency == CurrencyType.CR || currency == CurrencyType.NG)
                {
                    this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(StoreView));
                    var StoreView = ViewReferences.Get<StoreView>();
                    if (StoreView != null)
                    {
                        if (currency == CurrencyType.NG)
                            StoreView.ScrollRect.verticalNormalizedPosition = 0.0f;
                        else
                            StoreView.ScrollRect.verticalNormalizedPosition = 0.5f;
                    }
                }*/
            }
            else
            {
                this.View.TurnOffButtons();
                this.VirtualPurchaseSignal.Dispatch(new VirtualPurchaseRequest { StoreId = pack.StoreId, ItemId = pack.ItemId, Currency = currency, ExpectedPrice = (int)price });
                Debug.LogFormat("Opening 1 pack with {0}", currency);
            }
        }

        private void CheckSmartPackHint(CurrencyType currency, StoreData pack)
        {
            if(this.PlayerProfile.UiHintData.OpenPacks <= 1)
            {
                this.PlayerProfile.UiHintData.OpenPacks = 2;
                this.SaveUiHintDataSignal.Dispatch();
                this.UpdateUiHints();
            }
        }

        private void OnDialogDismissed(bool dialogResult)
        {
            Debug.Log("Pack Opening canceled");
            this.CloseAnimatedViewSignal.Dispatch(typeof(PackOpenerConfirmView));
        }
    }
}