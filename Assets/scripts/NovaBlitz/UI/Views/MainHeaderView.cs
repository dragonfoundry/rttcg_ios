﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;
using TMPro;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class MainHeaderView : AnimatedView
	{
		public TextMeshProUGUI UsernameLabel;
        public TextMeshProUGUI VersionLabel;
        public Image DevVersionIdentifier;
        public NovaButton BackButton;
        public NovaButton ProfileButton;

		[SerializeField]
		protected TextMeshProUGUI CreditsLabel;
		[SerializeField]
		protected TextMeshProUGUI GemsLabel;
		//[SerializeField]
		//protected TextMeshProUGUI PacksLabel;
        //[SerializeField]
        //public Slider RankSlider;
        //[SerializeField]
        //protected TextMeshProUGUI RankLabel;


        public Image TopFrame;
        public Image BottomFrame;
        public Image[] RatingPips;
        public Image RankDisplay;
        public TextMeshProUGUI RankTotal;
        public Image LeaderboardPositionDisplay;
        public TextMeshProUGUI LeaderboardPositionTotal;

        public GameObject FriendsArea;
        public TextMeshProUGUI FriendsLabel;
        public Image FriendRequestNotification;
        public Image ChatNotification;
        public Image ChallengeNotification;
        public TextMeshProUGUI OnlineFriendCount;

        public event Action EventFriendsClicked;

        public event Action EventProfileClicked;
		public event Action EventSettingsClicked;
		public event Action EventCreditsClicked;
		public event Action EventSmartPacksClicked;
		public event Action EventGemsClicked;
        public event Action EventBackClicked;

        private int _LeaderboardPositionAmount;
        public int LeaderboardPositionAmount { get { return _LeaderboardPositionAmount; } set { _LeaderboardPositionAmount = value;
                LeaderboardPositionTotal.text = value.ToString();
                LeaderboardPositionDisplay.gameObject.SetActive(value > 0);
                RankDisplay.gameObject.SetActive(value <= 0);
            } }

        private int _Rank;
        public int RankAmount {
            get { return _Rank; }
            set {
                _Rank = value;
                RankTotal.text = value.ToString();
                //this.RankLabel.text = value.ToString();
                // Rating and frame

                // Rating and frame
                int pips;
                int frame;

                if (value == 0 || value > 30 || value < -6)
                {
                    pips = -1;
                    frame = 0;
                }
                else if (value > 0)
                {
                    frame = Math.Max(0, Math.Min(4, 4 - Math.DivRem(value-1, 6, out pips)));
                    pips = 5 - pips;
                }
                else // value.rank < 0 - you're in NOVA rank!!!
                {
                    pips = -value-1;
                    frame = 5;
                }

                Sprite frameSprite;
                if (playerFrames.Count > 0)
                {
                    if (!playerFrames.TryGetValue(frame, out frameSprite))
                    {
                        frameSprite = playerFrames[0];
                    }
                    TopFrame.sprite = frameSprite;
                    BottomFrame.sprite = frameSprite;
                }
                for (int i = 0; i < RatingPips.Length; i++)
                {
                    RatingPips[i].gameObject.SetActive(i <= pips);
                }
            }
        }

        public string UserName {
			get { return this.UsernameLabel.text; }
			set { this.UsernameLabel.text = value; }
		}

        private int _CreditsAmount;
		public int CreditsAmount
		{
			get { return _CreditsAmount; }
			set { if (_CreditsAmount == 0)
                    CreditsLabel.text = value.ToString();
                else
                    DOTween.To(x => CreditsLabel.text = ((int)x).ToString(), _CreditsAmount, value, 2.0f);
                _CreditsAmount = value; }
		}

        private int _GemsAmount;
        public int GemsAmount
		{
			get { return _GemsAmount; }
            set { if (_GemsAmount == 0)
                    GemsLabel.text = value.ToString();
                else
                    DOTween.To(x => GemsLabel.text = ((int)x).ToString(), _GemsAmount, value, 2.0f);
                _GemsAmount = value; }
		}
		
		public int PacksAmount
		{
            get;// { return Int32.Parse(PacksLabel.text); }
            set;//{ PacksLabel.text = value.ToString(); }
		}

        public string VersionText
        {
            get { return this.VersionLabel.text; }
            set { this.VersionLabel.text = value; }
        }

        public bool IsTargetingDev
        {
            set { this.DevVersionIdentifier.gameObject.SetActive(value); }
        }

        public bool IsTutorial
        {
            set { this.FriendsArea.SetActive(!value); this.GemsLabel.gameObject.SetActive(!value); this.CreditsLabel.gameObject.SetActive(!value); this.ProfileButton.interactable = !value; }
            get { return !this.FriendsArea.activeSelf; }
        }
        
        [System.Serializable]
        public struct PlayerFramesClass
        {
            public Sprite Bronze;
            public Sprite Silver;
            public Sprite Gold;
            public Sprite Platinum;
            public Sprite Diamond;
            public Sprite Nova;
        }
        public PlayerFramesClass PlayerFrames = new PlayerFramesClass();
        private Dictionary<int, Sprite> playerFrames = new Dictionary<int, Sprite>();

        public void SetFrames()
        {
            playerFrames.Clear();
            playerFrames[0] = PlayerFrames.Bronze;
            playerFrames[1] = PlayerFrames.Silver;
            playerFrames[2] = PlayerFrames.Gold;
            playerFrames[3] = PlayerFrames.Platinum;
            playerFrames[4] = PlayerFrames.Diamond;
            playerFrames[5] = PlayerFrames.Nova;
        }

        protected override void Awake()
        {
            base.Awake();
            SetFrames();
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            this.ChallengeNotification.transform.DOKill();
            this.ChatNotification.transform.DOKill();
            this.FriendRequestNotification.transform.DOKill();
        }

        #region uGUI
        public void ClickBack() {
            if (null != this.EventBackClicked)
                this.EventBackClicked.Invoke();
        }
        public void ClickProfile() { if (null != this.EventProfileClicked) this.EventProfileClicked.Invoke(); }
		public void ClickSettings() { if (null != this.EventSettingsClicked) this.EventSettingsClicked.Invoke(); }
		public void ClickCredits() { if (null != this.EventCreditsClicked) this.EventCreditsClicked.Invoke(); }
		public void ClickSmartPacks() { if (null != this.EventSmartPacksClicked) this.EventSmartPacksClicked.Invoke(); }
		public void ClickGems() { if (null != this.EventGemsClicked) this.EventGemsClicked.Invoke(); }
        public void ClickFriends() { if (null != this.EventFriendsClicked) this.EventFriendsClicked.Invoke(); }


        #endregion
    }
}
