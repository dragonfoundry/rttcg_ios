﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using DragonFoundry.Fx;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class OpenPrizeMediator : Mediator
    {
        [Inject] public OpenPrizeView View { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public ShareScreenShotSignal ShareScreenShotSignal { get; set; }
        [Inject] public PrizeOpenedSignal PrizeOpenedSignal { get; set; }

        public override void OnRegister()
        {
            View.OnDoneButton += OnDoneButton;
            OpenAnimatedViewSignal.AddListener(OnThisViewOpened);
            this.View.EventShareClicked += OnShareClicked;

        }
        public override void OnRemove()
        {
            View.OnDoneButton -= OnDoneButton;
            OpenAnimatedViewSignal.RemoveListener(OnThisViewOpened);
            this.View.EventShareClicked -= OnShareClicked;
        }
        private void OnDoneButton()
        {
            Debug.Log("Mediator:OnDoneButton");
            this.View.RecyclePrizes();
            this.View.DoneButtonCanvasGroup.alpha = 0;
            this.View.DoneButtonCanvasGroup.interactable = false;
            this.View.DoneButtonCanvasGroup.blocksRaycasts = false;
            this.View.ShareButtonCanvasGroup.alpha = 0;
            this.View.ShareButtonCanvasGroup.interactable = false;
            this.View.ShareButtonCanvasGroup.blocksRaycasts = false;
            PrizeOpenedSignal.Dispatch();
            if(this.View.PrizeViewType == PrizeViewType.BasicPack 
                || this.View.PrizeViewType == PrizeViewType.SmartPack 
                || this.View.PrizeViewType == PrizeViewType.MegaPack)
                this.CloseAnimatedViewSignal.Dispatch(typeof(OpenPrizeView));
        }

        private void OnThisViewOpened(System.Type type)
        {
            if (type == typeof(OpenPrizeView))
            {
                View._Pack.gameObject.SetActive(true);
                ParticleEffectsTrigger[] particleFx = this.GetComponentsInChildren<ParticleEffectsTrigger>();
                foreach (var fx in particleFx)
                {
                    fx.StartEffect();
                }
            }
        }

        private void OnShareClicked(ShareScreenShotParams Params)
        {
            this.ShareScreenShotSignal.Dispatch(Params);
        }

    }
}