﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class ProgressLoadingScreenView : AnimatedView
    {
		public Image ProgressBarBG;
		public Image ProgressBar;
        public Button CancelButton;
        public TextMeshProUGUI StatusLabel;
		public NovaAudioPlayer MenuMusic;
		
		public System.Action CancelClicked;

        IEnumerator enumerator;
        private string _StatusText = string.Empty;
        public string StatusText { get { return _StatusText; } set { _StatusText = value; StatusLabel.text = value; } }


        private Dictionary<AsyncProgress, float> progressItems = new Dictionary<AsyncProgress,float>();
		private Dictionary<AsyncProgress, string> progressLabels = new Dictionary<AsyncProgress, string>();

        private float _targetBarFill = 0;
        public float TargetBarFill { get { return _targetBarFill; } set
            {
                if (value > _targetBarFill || value == 0)
                {
                    if (progressBarSequence != null)
                        progressBarSequence.Kill();
                    // 5s to fill the whole bar
                    float duration = Math.Abs(_targetBarFill - value) * 5f;
                    _targetBarFill = value;
                    if (value == 1f)
                    {
                        progressBarSequence = DOTween.Sequence();
                        progressBarSequence.Append(ProgressBar.rectTransform.DOScaleX(value, 3.5f).SetEase(Ease.InOutSine));
                        progressBarSequence.Append(ProgressBar.rectTransform.DOScaleX(0.5f, 0.0f));
                        progressBarSequence.SetLoops(-1);
                    }
                    else
                    {
                        progressBarSequence.Append(ProgressBar.rectTransform.DOScaleX(value, duration).SetEase(Ease.InOutSine));
                    }
                    progressBarSequence.Play();
                }
                else if(value < 0)
                {
                    if (progressBarSequence != null)
                        progressBarSequence.Kill();
                    progressBarSequence = DOTween.Sequence();
                    progressBarSequence.Append(ProgressBar.rectTransform.DOScaleX(0.0f, 0.0f));
                    progressBarSequence.Play();
                }
            }
        }
        private Sequence progressBarSequence;

        protected override void Update()
        {
            base.Update();
            // if we're loading assets, show the asset load status as an override to any other text
            if(AssetBundleManager.Instance != null && !AssetBundleManager.Instance.IsAllBundlesLoaded && AssetBundleManager.Instance.TotalDownloads > 0)
            {
                StatusLabel.text = AssetBundleManager.Instance.ProgressString;
            }
            else
            {
                StatusLabel.text = StatusText;
            }

        }

		public void OnCancelClicked()
		{
			if(this.CancelClicked != null)
			{
				this.CancelClicked.Invoke();
			}
		}

		override protected void OnDestroy()
		{
			base.OnDestroy();

			foreach (var kvp in this.progressLabels)
			{
				//unfinishedLoadingTasks.Add(kvp.Value);
				Debug.LogWarningFormat("Unfinished Loading Task:{0} t:{1}", kvp.Value, this.progressItems[kvp.Key]);
            }
            if (progressBarSequence != null)
            {
                progressBarSequence.Kill();
                progressBarSequence = null;
            }
        }
		
		public void InitializeProgressItems(Dictionary<AsyncProgress, string> progressLabels)
		{
            if (progressBarSequence != null)
            {
                progressBarSequence.Kill();
                progressBarSequence = null;
            }
            _targetBarFill = 0;
            ProgressBar.rectTransform.DOScaleX(0, 0);
            this.StatusText = string.Empty;
			//Vector2 size = this.ProgressBar.rectTransform.sizeDelta;
			//size.x = 0;
			//this.ProgressBar.rectTransform.sizeDelta = size;
			this.progressLabels = progressLabels;
			this.progressItems.Clear();
			foreach(AsyncProgress progressType in progressLabels.Keys)
			{
				this.progressItems[progressType] = 0.0f;
			}
            //SetProgress(AsyncProgress.NoProgress, 0.0f);
		}
        
		public void SetProgress(AsyncProgress progressType, float progress)
		{
			if(this.progressItems.ContainsKey(progressType))
			{
				this.progressItems[progressType] = progress;
			}
            else
            {
                Debug.LogFormat("ProgressItems doesn't contain type {0}", progressType);
            }

            TargetBarFill = progress;
		}

		/// <summary>
		/// Called by the animation timeline 
		/// </summary>
		public void LogoScreenClosed()
        {
            Debug.LogFormat("Logo Screen Closed: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            foreach (var kvp in this.progressLabels)
			{
				if(this.progressItems[kvp.Key] < 1f)
				{
					//Debug.LogWarning("Unfinishied Loading Task:" + kvp.Value);
				}
			}

            this.CancelButton.gameObject.SetActive(false);
			this.StatusText = string.Empty;


            if (progressBarSequence != null)
            {
                progressBarSequence.Kill();
                progressBarSequence = null;
            }
            _targetBarFill = 0;
            ProgressBar.rectTransform.DOScaleX(0, 0);
            /*
            Vector2 size = this.ProgressBar.rectTransform.sizeDelta;
			size.x = 0;
			this.ProgressBar.rectTransform.sizeDelta = size;*/
		}
	}   
}