﻿using UnityEngine;
using System;
using System.Collections;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	public class AnimatedView : View, IAnimatedUI
	{
		public Animator Animator {get; private set;}
		private CanvasGroup canvasGroup;
        public bool isIntendedForOverlayCanvas { get; set; }
		
		public bool IsCanvasInteractive 
		{
			get{ return this.isCanvasInteractive;}
			set
			{
				if(value != this.IsCanvasInteractive)
				{
					this.canvasGroup.blocksRaycasts = this.canvasGroup.interactable = value;
					this.isCanvasInteractive = value;
				}
			}
		}
		private RectTransform rect;
		private bool isCanvasInteractive;

		public virtual bool IsOpen
		{
			get { 
                if(this == null)
                {

                }
				else if (this.Animator == null)
                {
                    this.Animator = this.GetComponent<Animator> ();
                }
				return this != null && this.isActiveAndEnabled && this.Animator.GetBool("IsOpen");
            }
			set { 
				if (this.Animator == null) { this.Animator = this.GetComponent<Animator> ();}
				this.Animator.SetBool("IsOpen", value); }
		}

		public event Action EventAnimOpen;
		public event Action EventAnimClose;
        public bool IsShouldAwakeOpen = false;

		override protected void Awake()
		{
			base.Awake();
			this.Animator = this.GetComponent<Animator>();
			this.canvasGroup = this.GetComponentInChildren<CanvasGroup>();
			this.rect = this.GetComponent<RectTransform>();
			if(this.rect != null)
			{
				rect.anchorMin = Vector2.zero;
				rect.anchorMax = Vector2.one;
			}
            if(this.canvasGroup == null)
            {
                Debug.Log("fail");
            }
			this.isCanvasInteractive = this.canvasGroup.interactable;

			var notifiers = this.Animator.GetBehaviours<AnimClipEndNotifier>();
			foreach (var n in notifiers) {
				if (n.Tag == AnimStateTag.Open)
					n.EventClipEnd += OnAnimOpen;
				if (n.Tag == AnimStateTag.Close)
					n.EventClipEnd +=OnAnimClose;
			}
            //if(!IsShouldAwakeOpen)
            //    this.gameObject.SetActive(false);
		}

		protected virtual void Update()
		{
			bool isOpen = this.Animator.GetCurrentAnimatorStateInfo(0).IsName("Open");
			this.IsCanvasInteractive = isOpen;
		}

		protected virtual void OnAnimOpen(AnimStateNotification asn)
		{
			if (null != this.EventAnimOpen)
				this.EventAnimOpen.Invoke();
		}

		protected virtual void OnAnimClose(AnimStateNotification asn)
		{
			if (null != this.EventAnimClose)
				this.EventAnimClose.Invoke();
		}
	}
}

