﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;
using DragonFoundry.Fx;
using LinqTools;
using UnityEngine.UI;
using TMPro;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
    public class PackOpenerConfirmView : AnimatedView
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public GameData GameData {get; set;}

        public Action Reinitialize;
        public Action<CurrencyType, StoreData> OpenSmartPack; // bools isConfirmed
        public Action<CurrencyType, StoreData> BuySmartPack; // bools isConfirmed
        public Action<CurrencyType, StoreData> BuyBasicPack; // bools isConfirmed
        public Action<CurrencyType, StoreData> BuyMegaPack; // bools isConfirmed
        public Action<bool> DialogDismissed; // bools isConfirmed
        public TextMeshProUGUI CommonOwned;
        //public TextMeshProUGUI CommonPerPack;
        public TextMeshProUGUI CommonNew;
        public TextMeshProUGUI RareOwned;
        //public TextMeshProUGUI RarePerPack;
        public TextMeshProUGUI RareNew;
        public TextMeshProUGUI EpicOwned;
        //public TextMeshProUGUI EpicPerPack;
        public TextMeshProUGUI EpicNew;
        public TextMeshProUGUI LegendaryOwned;
        //public TextMeshProUGUI LegendaryPerPack;
        public TextMeshProUGUI LegendaryNew;

        public TextMeshProUGUI BasicPackPrice;
        public TextMeshProUGUI SmartPackPrice;
        public TextMeshProUGUI MegaPackPrice;
        public TextMeshProUGUI PacksOwned;
        public Button BuyBasicButton;
        public Button BuySmartButton;
        public Button BuyMegaButton;
        public Button OpenSmartButton;
        public ParticleEffectsTrigger ButtonFx;
        public Image PacksOwnedImage;

		public StoreData BasicPackData {get; set; }
        public StoreData SmartPackData { get; set; }
        public StoreData MegaPackData { get; set; }
        public Dictionary<string, StoreData> PackStore;

        public void TurnOffButtons()
        {
            OpenSmartButton.interactable = false;
            BuyBasicButton.interactable = false;
            BuySmartButton.interactable = false;
            BuyMegaButton.interactable = false;
        }

        public void OnClickOpen()
        {
            //TurnOffButtons();
            if (this.OpenSmartPack != null) {
                this.OpenSmartPack.Invoke(CurrencyType.SP, this.SmartPackData);
            }
        }

        public void OnClickBuyBasicPack()
        {
            //TurnOffButtons();
            if (this.BuyBasicPack != null) {
                this.BuyBasicPack.Invoke(CurrencyType.CR, this.BasicPackData);
            }
        }
        
        public void OnClickBuySmartPack()
        {
            //TurnOffButtons();
            if (this.BuySmartPack != null) {
                this.BuySmartPack.Invoke(CurrencyType.NG, this.SmartPackData);
            }
        }

        public void OnClickBuyMegaPack()
        {
            //TurnOffButtons();
            if (this.BuyMegaPack != null) {
                this.BuyMegaPack.Invoke(CurrencyType.NG, this.MegaPackData);
            }
        }
        
        public void OnClickCancel()
        {
            if (this.DialogDismissed != null) {
                this.DialogDismissed.Invoke(false);
            }
        }
    }
}
