﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.mediation.impl;
using strange.extensions.context.impl;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using UnityEngine.UI;

namespace NovaBlitz.UI.Mediators
{
	public class CardBrowserMediator : Mediator
	{
		[Inject] public CardBrowserView View {get;set;}
		[Inject] public FilterCardsSignal FilterCardsSignal {get;set;}
		[Inject] public CardsFilteredSignal CardsFilteredSignal {get;set;}
		[Inject] public CatalogLoadedSignal CatalogLoadedSignal {get;set;}
		[Inject] public RemoveCardFromDeckSignal RemoveCardFromDeckSignal {get;set;}
		[Inject] public CardAddedToDeckSignal CardAddedToDeckSignal {get;set;}
		[Inject] public CardRemovedFromDeckSignal CardRemovedFromDeckSignal {get;set;}
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public LogEventSignal LogEventSignal {get;set;}

		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public GameData GameData {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
        
		[Inject] public InventoryLoadedSignal InventoryLoadedSignal {get;set;}
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }

		protected bool _ReceivedCatalog;
		protected bool isFiltersLockedIn = false;

		/// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister ()
		{
			this.View.CardPrefab = this.UIConfiguration.CardPrefab;
			this.View.DraggableCardPrefab = this.UIConfiguration.DraggableCardPrefab;

			this.View.EventCardListItemDropped += OnView_ListItemDropped;
			this.View.EventFilterChangeApplied += OnFilterApplyClicked;
			this.View.FilterBarWidget.EventSearchChanged += OnFilterBar_Search;
			this.View.FilterBarWidget.EventSearchClearClicked += OnFilterBar_SearchClear;
            this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
            this.InventoryLoadedSignal.AddListener(OnInventoryLoaded);
			this.FilterCardsSignal.AddListener(OnFilterCards);
			this.CardsFilteredSignal.AddListener(OnCardsFiltered);
			this.CardAddedToDeckSignal.AddListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.AddListener(OnCardRemovedFromDeck);
			this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);

			//if (this.GameData._catalogCached)
			//	OnCatalogLoaded(NBEconomy.CATALOG_MAIN);
		}

		/// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove ()
		{
			this.View.EventCardListItemDropped -= OnView_ListItemDropped;
			this.View.EventFilterChangeApplied -= OnFilterApplyClicked;
			this.View.FilterBarWidget.EventSearchChanged -= OnFilterBar_Search;
			this.View.FilterBarWidget.EventSearchClearClicked -= OnFilterBar_SearchClear;
            this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
            this.InventoryLoadedSignal.RemoveListener(OnInventoryLoaded);
			this.FilterCardsSignal.RemoveListener(OnFilterCards);
            this.CardsFilteredSignal.RemoveListener(OnCardsFiltered);
			this.CardAddedToDeckSignal.RemoveListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.RemoveListener(OnCardRemovedFromDeck);
			this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
		}

		protected void MatchFilterState()
		{
			//if (null != this.PlayerProfile.CurrentCardFilter)
			//	this.View.FilterPanelWidget.MatchState(this.PlayerProfile.CurrentCardFilter);
		}

		#region View Listeners
		protected void OnFilterApplyClicked(CardFilterParams filterParams)
		{
			if (null != this.PlayerProfile.CurrentCardFilter)
				filterParams.SearchPhrase = this.PlayerProfile.CurrentCardFilter.SearchPhrase; //ignore search field
			this.FilterCardsSignal.Dispatch(filterParams);
		}
		#endregion

		#region Signal Listeners

		private void OnCardAddedToDeck(CardData card, DeckData deck)
		{
			bool isSecondAspectAdded = false;

			if(deck.Aspects.Count >= 1)
			{
				if(deck.Aspects.Count == 1)
				{
					isSecondAspectAdded = card.Aspect != deck.Aspects.First();
				}
				else
				{
					isSecondAspectAdded = true;
				}
			}

			if(isSecondAspectAdded && this.isFiltersLockedIn == false)
			{
				//this.View.FilterPanelWidget.LockInAspects(deck.Aspects, this.PlayerProfile, card.Aspect);
				this.isFiltersLockedIn = true;
			}
			this.UpdateCollectionTotals();
		}

		private void OnCardRemovedFromDeck(CardData card, DeckData deck)
		{
			this.isFiltersLockedIn = deck.Aspects.Count < 2;
			this.UpdateCollectionTotals();
		}
        #endregion

		protected void UpdateCollectionTotals()
		{
			this.View.UpdateCollectionTotals(this.PlayerProfile);
		}

		#region View Listeners

		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
				this.UpdateCollectionTotals();
				this.LogEventSignal.Dispatch (new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_CARD_BROWSER, 
					IsTimedEvent = true, 
					IsTimedEventOver = false
				});

				// Hide the filter dropdowns if they are open
				this.View.HideFilterDropdowns();

				this.View.InitializeFilterBarSize();
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
			{
                //this.View.Recycle();
				this.LogEventSignal.Dispatch (new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_CARD_BROWSER, 
					IsTimedEvent = true, 
					IsTimedEventOver = true
				});
                if(this.View.TwoTouchOverlay != null)
                   this.View.TwoTouchOverlay.HideKeywords();
            }
        }

		protected void OnView_ListItemDropped(CardData card)
		{
			this.RemoveCardFromDeckSignal.Dispatch(card, this.PlayerProfile.CurrentDeck);
		}
		protected void OnFilterBar_Search(string phrase)
		{
			
			var filterParams = this.PlayerProfile.CurrentCardFilter;
			if (phrase != filterParams.SearchPhrase) {
				filterParams.SearchPhrase = phrase;
				//Debug.Log("Filter bar search: " + phrase);
				this.FilterCardsSignal.Dispatch(filterParams);
			}
		}

		protected void OnFilterBar_SearchClear()
		{
			Debug.Log("FilterBar Clear");
			var filterParams = this.PlayerProfile.CurrentCardFilter;
			if (!string.IsNullOrEmpty(filterParams.SearchPhrase)) {
				filterParams.SearchPhrase = string.Empty;
				this.FilterCardsSignal.Dispatch(filterParams);
			}
		}
		#endregion

		#region Signal Listeners
        private void OnCanceled()
        {
            //this.View.Initialize();
        }

        private void OnStartGameSession(StartGameSessionObject sessionData)
		{
			
        }

        private void OnProfileUpdated(ProfileSection section)
        {
            if (section != ProfileSection.Inventory)// && section != ProfileSection.Currency)
                return;
            OnInventoryLoaded();
        }

        private void OnInventoryLoaded()
        {
            UpdateCollectionTotals();
        }

		private void OnFilterCards(CardFilterParams filterParams)
		{
			filterParams = CardFilterParams.Expand(filterParams);
			this.View.FilterBarWidget.SearchPhrase = filterParams.SearchPhrase;
			this.View.FilterBarWidget.CostsIndicator.ShowIcons(filterParams.Costs);
			this.View.FilterBarWidget.TypesIndicator.ShowIcons(filterParams.Types);
			this.View.FilterBarWidget.AspectsIndicator.ShowIcons(filterParams.Aspects);
			this.View.FilterBarWidget.RaritiesIndicator.ShowIcons(filterParams.Rarities);
		}

		private void OnCardsFiltered(List<int> results)
		{
			List<CardData> filteredCards = new List<CardData>();
			foreach(int cardID in results)
			{
				filteredCards.Add(GameData.CardDictionary[cardID]);
			}	
			this.View.Initialize(filteredCards);
			
			//Debug.LogFormat("OnCardsFiltered:{0}", results.Count);
            this.View.ScrollView.verticalNormalizedPosition = 1f;

			// Get the scroll view to re-layout and render
			LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)this.View.ScrollView.transform);
			
			this.View.InitFilterPanel();
		}
        
		#endregion
	}
}
