﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using LinqTools;
using TMPro;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
	public class StoreItemView : PoolableMonoBehaviour
    {

        public override PooledObjectType PooledObjectType { get { return PooledObjectType.StoreItem; } }

		public CurrencyType Currency = CurrencyType.RM;
        [SerializeField] protected Image ItemDisplay;
        [SerializeField] protected TextMeshProUGUI _NameDisplay;
		[SerializeField] protected Image _SpriteDisplay;
        [SerializeField] protected NovaButton BuyButton;
		[SerializeField] protected TextMeshProUGUI _PriceDisplay;
        [SerializeField] protected NovaButton BuyButtonVC;
		[SerializeField] protected TextMeshProUGUI _PriceDisplayVC;
        [SerializeField] protected GameObject ProductHolder;
        [SerializeField] protected Image Header;
        [SerializeField] protected TextMeshProUGUI HeaderText;
        public RectTransform ProductScaler;

        public PoolableMonoBehaviour Product { get; set; }
        //public string ItemName { set { _NameDisplay.text = value; } }
        private uint _ItemPrice;
		public uint ItemPrice { set { _PriceDisplay.text = CurrencyUtils.FormatPrice(this.Currency, value); _ItemPrice = value; } get { return _ItemPrice; } }
		public Sprite ItemSprite { set { _SpriteDisplay.sprite = value; } }
        
        public bool IsVisible { get { return ItemDisplay.gameObject.activeSelf; } set { ItemDisplay.gameObject.SetActive(value); } }
        public bool IsPurchasable { get { return ActiveButton.interactable; } set {
                //if (value != ActiveButton.gameObject.activeSelf)
                //    ActiveButton.gameObject.SetActive(value);
                ActiveButton.interactable = value;
                ItemDisplay.sprite = value == true ? ActiveBackground : InactiveBackground; } }

		protected StoreData _StoreData;
		public StoreData StoreData {
			get { return _StoreData; }
			set { _StoreData = value; RefreshData(); }
		}
        private bool isPopular;
        private bool isBestValue;
        private bool isAnnuity;
        private bool isTimed;
        private bool isProductFound;
        public bool IsVirtualPurchase { get; set; }

        public NovaButton ActiveButton { get { return IsVirtualPurchase ? BuyButtonVC : BuyButton; } }
        public NovaButton InactiveButton { get { return IsVirtualPurchase ? BuyButton : BuyButtonVC ; } }

        public const string SMALL_SIZE = "<size=-10>";


        public Sprite ActiveBackground;
        public Sprite InactiveBackground;
        public Sprite annuity_nanobot;
        public Sprite annuity_credit;
        public Sprite annuity_smartpack;
        public Sprite gems_01;
        public Sprite gems_02;
        public Sprite gems_03;
        public Sprite gems_04;
        public Sprite gems_05;
        public Sprite gems_06;
        public Sprite credits_01;
        public Sprite credits_02;
        public Sprite credits_03;
        public Sprite nanobots_01;
        public Sprite nanobots_02;
        public Sprite nanobots_03;
        public Sprite smartpacks_01;
        public Sprite drafts_01;
        public Sprite offer_01;
        public Sprite offer_02;
        public Sprite offer_03;
        public Sprite offer_04;
        public Sprite offer_05;
        public Sprite offer_06;
        public Sprite offer_07;
        public Sprite offer_08;
        public Sprite offer_09;
        public Sprite offer_10;

        private static Dictionary<string, Sprite> SpriteLookup;

        public void Update()
        {
            SetTimedLabels();
        }

        private void SetTimedLabels()
        {
            IsPurchasable = isProductFound;
            if (isAnnuity && _StoreData != null)
            {
                int daysLeft = (int)(_StoreData.ExpiryTime - DateTime.UtcNow).TotalDays;
                if (_StoreData.NumberOwned == 0)
                {
                    HeaderText.color = Color.white;
                    Header.color = Color.red;
                    HeaderText.text = I2.Loc.ScriptLocalization.Great_Value;
                    //   Header.gameObject.SetActive(false);
                    //IsPurchasable = isProductFound;
                }
                else if (daysLeft > 0)
                {
                    HeaderText.color = new Color(0.04f, 0.26f, 0.29f);
                    Header.color = Color.green;
                    //I2.Loc.ScriptLocalization.day
                    HeaderText.text = string.Format(daysLeft > 1 ? I2.Loc.ScriptLocalization.Days_Left : I2.Loc.ScriptLocalization.Day_Left, daysLeft, 0, 0, 0);
                    IsPurchasable = daysLeft < 15 && isProductFound;
                }
                else
                {
                    HeaderText.color = Color.red;
                    Header.color = Color.yellow;
                    HeaderText.text = I2.Loc.ScriptLocalization.Annuity_Expired;
                    //IsPurchasable = isProductFound;
                }
                // three states - never owned, currently running, used to own
            }
            else if (isTimed && _StoreData != null)
            {
                if (_StoreData.ExpiryTime > Server.Time)
                {
                    HeaderText.color = new Color(0.04f, 0.26f, 0.29f);
                    Header.color = Color.green;
                    var countdown = _StoreData.ExpiryTime - Server.Time;
                    // countdown
                    if (_StoreData.IsWeeklyCard)
                        HeaderText.text = string.Format("{4}\n{0}: {1}:{2:00}:{3:00}", I2.Loc.ScriptLocalization.Expires, countdown.Hours, countdown.Minutes, countdown.Seconds,
                            I2.Loc.ScriptLocalization.Weekly_Card);
                    else
                        HeaderText.text = string.Format("{0}: {1}:{2:00}:{3:00}", I2.Loc.ScriptLocalization.Expires, countdown.Hours, countdown.Minutes, countdown.Seconds);
                }
                else
                {
                    HeaderText.color = Color.red;
                    Header.color = Color.yellow;
                    HeaderText.text = I2.Loc.ScriptLocalization.Last_Chance;
                }
            }
            else if(_StoreData != null && Product != null)
            {
                IsPurchasable = _StoreData.NumberOwned < 3;
                if(!IsPurchasable)
                {
                    Header.gameObject.SetActive(false);
                }
            }
        }

        private void CreateSpriteLookup()
        {
            SpriteLookup = new Dictionary<string, Sprite>
            {
                { "annuity_nanobot", annuity_nanobot },
                { "annuity_credit", annuity_credit },
                { "annuity_smartpack", annuity_smartpack },
                { "gems_01", gems_01 },
                { "gems_02", gems_02 },
                { "gems_03", gems_03 },
                { "gems_04", gems_04 },
                { "gems_05", gems_05 },
                { "gems_06", gems_06 },
                { "credits_01", credits_01 },
                { "credits_02", credits_02 },
                { "credits_03", credits_03 },
                { "nanobots_01", nanobots_01 },
                { "nanobots_02", nanobots_02 },
                { "nanobots_03", nanobots_03 },
                { "smartpacks_01", smartpacks_01 },
                { "drafts_01", drafts_01 },
                { "offer_01", offer_01 },
                { "offer_02", offer_02 },
                { "offer_03", offer_03 },
                { "offer_04", offer_04 },
                { "offer_05", offer_05 },
                { "offer_06", offer_06 },
                { "offer_07", offer_07 },
                { "offer_08", offer_08 },
                { "offer_09", offer_09 },
                { "offer_10", offer_10 },
            };
        }

        public virtual void RefreshData()
		{
            //CleanData();
            if (SpriteLookup == null)
                CreateSpriteLookup();
			if (null == _StoreData || _StoreData.VirtualPrices == null)// || null == _StoreData.ProductData)
            {
                Debug.LogErrorFormat("{0} is null", _StoreData == null ? "Store Data" : string.Format("Product data for {0}", _StoreData.ItemId));
                this.IsVisible = false;
                return;
            }
            else
            {
                this.IsVisible = true;
            }

            Header.rectTransform.sizeDelta = new Vector2(Header.rectTransform.sizeDelta.x, 60);
            // find the item. It could be a for sale product, in which case we'll have a specific image.
            // It could be a card, cardback, or avatar, in which case we should be using that.

            Sprite itemSprite;
            SpriteLookup.TryGetValue(StoreData.ItemId, out itemSprite);
            ItemSprite = itemSprite;
            _SpriteDisplay.gameObject.SetActive(true);
            ProductHolder.gameObject.SetActive(false);

            var price = _StoreData.VirtualPrices.FirstOrDefault();
			this.Currency = CurrencyUtils.GetCode(price.Key);

			StringBuilder builder = new StringBuilder();
            builder.Append(I2.Loc.ScriptLocalization.Get(NovaIAPService.STORE_ITEM_NAME + _StoreData.ItemId));
            builder.AppendLine ().Append (SMALL_SIZE);
			builder.Append(I2.Loc.ScriptLocalization.Get(NovaIAPService.STORE_ITEM_SHORT + _StoreData.ItemId));
			_NameDisplay.text = builder.ToString ();
			this.ItemPrice = price.Value;
            //_PriceDisplay.SetText ("wrong");
            //this.ItemSprite = ...


            IsVirtualPurchase = (this.Currency != CurrencyType.RM && this.Currency != CurrencyType.xx);
            ActiveButton.gameObject.SetActive(true);
            InactiveButton.gameObject.SetActive(false);
            IsPurchasable = true;
            InactiveButton.interactable = false;
            if (IsVirtualPurchase)
            {
                int currency = this.Currency == CurrencyType.CR ? 0 : this.Currency == CurrencyType.NG ? 1 : 2;
                _PriceDisplayVC.text = string.Format("<voffset=-1em><size=50><sprite={0}></size><voffset=-.78em>{1}", currency, ItemPrice);
            }

            isAnnuity = _StoreData.tags != null && _StoreData.tags.Contains("annuity");
            isBestValue = _StoreData.tags != null && _StoreData.tags.Contains("value");
            isPopular = _StoreData.tags != null && _StoreData.tags.Contains("popular");
            isTimed = _StoreData.ExpiryTime > DateTime.MinValue;

            // set colours
            this.Header.gameObject.SetActive(isPopular || isBestValue || isAnnuity || isTimed);
            if(isPopular)
            {
                this.Header.color = Color.blue;
                this.HeaderText.color = Color.white;
                this.HeaderText.text = I2.Loc.ScriptLocalization.Popular;
            }
            else if (isBestValue)
            {
                this.Header.color = Color.red;
                this.HeaderText.color = Color.white;
                this.HeaderText.text = I2.Loc.ScriptLocalization.Great_Value;
            }
            else if (isAnnuity)
            {
                this.Header.color = Color.yellow;
                this.HeaderText.color = Color.blue;
            }
            else if (isTimed)
            {
                this.Header.color = Color.green;
                this.HeaderText.color = Color.yellow;
            }

            // name colours based on product
            // gems, packs = green
            // card = orange
            // credits = orange
            // draft = orange?
            // nanobots = purple
            /*if(_StoreData.tags.Contains("gem") || _StoreData.tags.Contains("pack") || _StoreData.tags.Contains("credit") || _StoreData.tags.Contains("draft"))
            {
                this._NameDisplay.color = new Color(1.0f,1.0f,0.2f);
            }
            else if ()
            {
                this._NameDisplay.color = Color.yellow;
            }
            else if (_StoreData.tags.Contains("nanobot"))
            {
                this._NameDisplay.color = Color.magenta;
            }
            else if ()
            {
                this._NameDisplay.color = Color.green;
            }
            else if ()
            {
                this._NameDisplay.color = Color.cyan;
            }
            else if (_StoreData.tags.Contains("offer"))
            {
                this._NameDisplay.color = Color.yellow;
            }
            else // it's a card or similar
            {
                this._NameDisplay.color = Color.yellow;
            }*/


#if UNITY_STEAM
            isProductFound = StoreData != null && !string.IsNullOrEmpty(StoreData.ItemId);
            //int cents = 0;
            uint realprice;
            if(_StoreData.VirtualPrices.TryGetValue(CurrencyType.RM.ToString(), out realprice))
            {
                //int dollars = Math.DivRem((int)realprice, 100, out cents);
                //var text = CurrencyUtils.FormatPrice(CurrencyType.RM, realprice);
                //var text = string.Format("${0}.{1}", dollars, cents);
                //Debug.LogFormat("Item {0} price text '{1}'", _StoreData.ItemID, text);
                _PriceDisplay.text = CurrencyUtils.FormatPrice(CurrencyType.RM, realprice);
            }
#else
            isProductFound = StoreData.UnityPurchasingProduct != null || Currency != CurrencyType.RM;
            if(StoreData.UnityPurchasingProduct != null)
            {
                var data = _StoreData.UnityPurchasingProduct.metadata;
				if(data != null && !string.IsNullOrEmpty(data.localizedPriceString))
				{
					//Debug.LogFormat("Item {0} price text '{1}'", _StoreData.ItemId, data.localizedPriceString);
					_PriceDisplay.SetText(data.localizedPriceString);
				}
				else
				{
					Debug.LogFormat("Item {0} no price text found", _StoreData.ItemId);
				}
                //_NameDisplay.SetText(data.localizedTitle);
                //_OpenDisplay.SetText(data.localizedDescription);
            }
            /*else
            {
			    _PriceDisplay.text = "wrong";
            }*/
#endif
            if (isProductFound)
            {
                //Debug.LogFormat("Product FOUND: {0}", StoreData.ItemID);
            }
            else
            {
                Debug.LogFormat("Product not found: {0}", StoreData.ItemId);
            }
            IsPurchasable = isProductFound;

            if (Product != null && Product.PooledObjectType == PooledObjectType.CardView)
                InitializeToCard(Product as CardView);
            SetTimedLabels();
        }

        public void InitializeToPurchaseConfirm()
        {
            this.BuyButton.gameObject.SetActive(false);
            this.BuyButtonVC.gameObject.SetActive(false);
        }

        public void InitializeToCard(CardView card)
        {
            Product = card;
            card.transform.AttachToParent(this.ProductHolder.transform, 0, null, false, false);
            card.RectTransform.anchorMax = Vector2.one / 2;
            card.RectTransform.anchorMin = Vector2.one / 2;
            card.transform.localScale = Vector3.one * 2;
            card.CardCanvasGroup.transform.localScale = Vector3.one;
            card.RectTransform.anchoredPosition3D = Vector3.zero;
            _SpriteDisplay.gameObject.SetActive(false);
            ProductHolder.gameObject.SetActive(true);
            if (_StoreData != null && _StoreData.IsWeeklyCard)
            {
                this._NameDisplay.text = string.Format("<size=-10>\n{0}: {1}/{2}\n{3}</size>", I2.Loc.ScriptLocalization.Owned, _StoreData.NumberOwned, 3,
                    I2.Loc.ScriptLocalization.Weekly_Card);
                Header.rectTransform.sizeDelta = new Vector2(Header.rectTransform.sizeDelta.x, 80);
            }
            else
                this._NameDisplay.text = string.Format("<size=-10>\n{0}: {1}/{2}</size>", I2.Loc.ScriptLocalization.Owned, _StoreData.NumberOwned, 3);
        }

        public void InitializeOfferItem(PlayerProfile playerProfile, List<string> productList)
        {
            NovaInventoryItem item;
            if (StoreData.ItemId.Contains("annuity_") && playerProfile.Inventory.TryGetValue(StoreData.ItemId + "_tracker", out item))
            {
                StoreData.NumberOwned = Math.Max(1, item.RemainingUses.HasValue ? item.RemainingUses.Value : 1);
            }

            if (playerProfile.Inventory.TryGetValue(StoreData.ItemId, out item))
            {
                if (StoreData.StoreId == NBEconomy.STORE_MAIN)
                    StoreData.ExpiryTime = item.Expiration.HasValue ? item.Expiration.Value : DateTime.MinValue;
                StoreData.NumberOwned = Math.Max(1, item.RemainingUses.HasValue ? item.RemainingUses.Value : 1);
            }


            gameObject.transform.SetAsLastSibling();

            if (productList.Contains(StoreData.ItemId))
            {
                switch (StoreData.ItemId)
                {
                    case NovaIAPService.ANNUITY_SMARTPACK:
                    case NovaIAPService.ANNUITY_CREDIT:
                    case NovaIAPService.ANNUITY_NANOBOT:
                        //bool hasPurchased = playerProfile.BoughtStoreItems.Contains(StoreData.ItemId);
                        // set display based on purchased or not
                        break;
                    case NovaIAPService.GEMS_01:
                    case NovaIAPService.GEMS_02:
                    case NovaIAPService.GEMS_03:
                    case NovaIAPService.GEMS_04:
                    case NovaIAPService.GEMS_05:
                    case NovaIAPService.GEMS_06:
                        break;
                    case NovaIAPService.OFFER_01:
                    case NovaIAPService.OFFER_02:
                    case NovaIAPService.OFFER_03:
                    case NovaIAPService.OFFER_04:
                    case NovaIAPService.OFFER_05:
                    case NovaIAPService.OFFER_06:
                    case NovaIAPService.OFFER_07:
                    case NovaIAPService.OFFER_08:
                    case NovaIAPService.OFFER_09:
                    case NovaIAPService.OFFER_10:
                        break;
                    case NovaIAPService.CREDITS_01:
                    case NovaIAPService.CREDITS_02:
                    case NovaIAPService.CREDITS_03:
                    case NovaIAPService.NANOBOTS_01:
                    case NovaIAPService.NANOBOTS_02:
                    case NovaIAPService.NANOBOTS_03:
                    case NovaIAPService.SMARTPACKS_01:
                    case NovaIAPService.DRAFTS_01:
                        break;
                    default:
                        IsVisible = false;
                        Debug.LogErrorFormat("Store Item not found: {0}", StoreData.ItemId);
                        break;
                }
            }
            else
            {
                switch (StoreData.StoreId)
                {
                    case NBEconomy.ITEMCLASS_CARD:
                        int cardId;
                        CardData cardData;
                        if (int.TryParse(StoreData.ItemId, out cardId))
                        {
                            int owned;
                            playerProfile.OwnedCards.TryGetValue(cardId, out owned);
                            StoreData.NumberOwned = owned;

                            if (GameData.CardDictionary.TryGetValue(cardId, out cardData))
                            {
                                var card = ObjectPool.SpawnPrefab<CardView>(PooledObjectType.CardView);
                                card.DataBind(cardData);
                                InitializeToCard(card);
                            }
                        }
                        else
                        {
                            IsVisible = false;
                            Debug.LogErrorFormat("Store ItemClass not found (product): {0}", StoreData.ItemId);
                        }
                        break;
                    case NBEconomy.ITEMCLASS_CARDBACK:
                    case NBEconomy.ITEMCLASS_AVATAR:
                    default:
                        IsVisible = false;
                        Debug.LogErrorFormat("Store ItemClass not found (product): {0}", StoreData.ItemId);
                        break;

                }
                // this is a product - a card or avatar, or cardback
            }
        }

        public void SwitchCurrency(CurrencyType code)
		{
			if (null == _StoreData)
				return;
			this.Currency = code;
			uint price; _StoreData.VirtualPrices.TryGetValue(code.ToString(), out price);
			this.ItemPrice = price;
		}

		//public void Click() { if (null != this.EventClicked) this.EventClicked.Invoke(this); }

        public void Click()
        {
            var NovaIAPService = FindObjectOfType<NovaIAPService>();

            if (IsVirtualPurchase && Currency != CurrencyType.RM && Currency != CurrencyType.xx)
            {
                int balance;
                uint price;
                NovaIAPService.PlayerProfile.Wallet.Currencies.TryGetValue(Currency, out balance);
                _StoreData.VirtualPrices.TryGetValue(Currency.ToString(), out price);
                if (Currency == CurrencyType.CR && price > balance)
                {
                    NovaIAPService.ShowPurchaseOffersSignal.Dispatch(OfferType.CreditsOffer);
                }
                else if (Currency == CurrencyType.NG && price > balance)
                {
                    NovaIAPService.ShowPurchaseOffersSignal.Dispatch(OfferType.GemsOffer);
                }
                else if (Currency == CurrencyType.NC && price > balance)
                {
                    NovaIAPService.ShowPurchaseOffersSignal.Dispatch(OfferType.NanoBotsOffer);
                }
                else
                {
                    // show the purchase confirm window
                    var newView = Product != null ? Product.Spawn() : this.Spawn();// View.Instantiate(view, Vector3.zero, Quaternion.identity) as CardBackView;
                    if (newView.PooledObjectType == PooledObjectType.StoreItem)
                    {
                        StoreItemView storeItem = newView as StoreItemView;
                        storeItem.StoreData = StoreData;
                        storeItem.InitializeToPurchaseConfirm();
                    }
                    else if (newView.PooledObjectType == PooledObjectType.CardBackView)
                    {
                        CardBackView cbv = newView as CardBackView;
                        cbv.UpdateCardBackView((Product as CardBackView).CardBackData);
                        cbv._BuyButton.gameObject.SetActive(false);
                        cbv._NotOwnedScrim.gameObject.SetActive(false);
                        newView = cbv;
                    }
                    else if (newView.PooledObjectType == PooledObjectType.CardView)
                    {
                        CardView cbv = newView as CardView;
                        cbv.DataBind((Product as CardView).CardData);
                        newView = cbv;
                    }
                    NovaIAPService.BeginVirtualPurchase(StoreData, newView);
                }
            }
            else
            {
                // set to waiting to purchase

                //this.CurrentStoreData = item.StoreData;

                NovaIAPService.BeginCashPurchase(StoreData);
            }
        }

        private void CleanData()
        {
            this.IsVisible = true;
            this._SpriteDisplay.sprite = null;
            this.Header.gameObject.SetActive(false);
            this.BuyButton.interactable = true;
            this.BuyButtonVC.interactable = true;
            this.isAnnuity = false;
            this.isBestValue = false;
            this.isPopular = false;
            this.isProductFound = false;
            this.IsVirtualPurchase = false;
            this.isTimed = false;
        }

        public void OnLocalize()
        {
            if(_StoreData != null)
                RefreshData();
        }

        public override void OnRecycle()
        {
			if(Header != null && Header.rectTransform != null)
	            Header.rectTransform.sizeDelta = new Vector2(Header.rectTransform.sizeDelta.x, 60);
			if(ProductScaler != null && ProductScaler.transform != null)
	            this.ProductScaler.transform.localScale = Vector3.one;
            this._StoreData = null;
            (this.transform as RectTransform).sizeDelta = new Vector2(280, 500);
            CleanData();
            if(Product != null)
            {
                Product.Recycle();
                Product = null;
            }
        }
    }
}
