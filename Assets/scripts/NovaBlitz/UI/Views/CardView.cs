using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System;
using System.Text;
using DragonFoundry.Configuration;
using DG.Tweening;

namespace NovaBlitz.UI
{

	public class CardView : BaseCardView, IRectTransform
    {
        public CardAbilityController CardAbilities;
        public I2.Loc.Localize TitleLocalizer;
        public TextMeshProUGUI TitleLabel;
        public TextMeshProUGUI EnergyCostLabel;
        public TextMeshProUGUI AttackLabel;
        public TextMeshProUGUI HealthLabel;
		public TextMeshProUGUI RulesLabel;
		public TextMeshProUGUI SubtypeLabel;
        public Image UnitDropShadow;
        public Image PowerDropShadow;
        public Image CardFrame;
		public Image titleHolder;
		public CardArtImage CardArt;
		public Image artMask;
        public Image RarityIcon;
        public Image LoadingSpinner;
        public GameObject Canvas { get; private set; }
        public CanvasGroup CanvasGroup { get; private set; }
        public int NumCardsInStack {get; protected set;}
        public CanvasGroup CardCanvasGroup;
        public CanvasGroup ContentCanvasGroup;
		public Material GrayscaleMaterial;
        public Image CardCountFrame;
        public TextMeshProUGUI CardCountLabel;
        public RectTransform CardNodes;
        public Image[] CardNodeArray;
        public GameObject NewCardTag;
        public Color DisabledColor;
        public Color DefaultColor;
        public Color RedColor;
		public int DraftPickNumber;

		public RectTransform RectTransform {get; protected set;}

		private const string Number = "{0}";


        public CardAspectConfiguration Arcane;
        public CardAspectConfiguration Tech;
        public CardAspectConfiguration Divine;
        public CardAspectConfiguration Nature;
        public CardAspectConfiguration Chaos;
        public Sprite[] Rarities;
        public Sprite PowerMask;
        public Sprite UnitMask;
        public GameObject WhitePowerOutline;
        public GameObject WhiteUnitOutline;

        /*
        [System.Serializable]
        public struct UnitFramesClass
        {
            public Sprite Arcane;
            public Sprite Tech;
            public Sprite Divine;
            public Sprite Nature;
            public Sprite Chaos;
        }
        public UnitFramesClass UnitFrames = new UnitFramesClass();

        [System.Serializable]
        public struct ItemFramesClass
        {
            public Sprite Arcane;
            public Sprite Tech;
            public Sprite Divine;
            public Sprite Nature;
            public Sprite Chaos;
        }
        public ItemFramesClass ItemFrames = new ItemFramesClass();
        
        [System.Serializable]
        public struct PowerFramesClass
        {
            public Sprite Arcane;
            public Sprite Tech;
            public Sprite Divine;
            public Sprite Nature;
            public Sprite Chaos;
        }
		public PowerFramesClass PowerFrames = new PowerFramesClass();

		[System.Serializable]
		public struct TitleHolderClass
		{
			public Sprite Arcane;
			public Sprite Tech;
			public Sprite Divine;
			public Sprite Nature;
			public Sprite Chaos;
		}
		public TitleHolderClass TitleHolders = new TitleHolderClass();*/

        /*[System.Serializable]
        public struct RarityIconClass
        {
            public Sprite Common;
            public Sprite Uncommon;
            public Sprite Rare;
            public Sprite Legendary;
        }
        public RarityIconClass RarityIcons = new RarityIconClass();*/

        /*[System.Serializable]
		public struct CardMasksClass
		{
			public Sprite Power;
			public Sprite Unit;
		}
		public CardMasksClass CardMasks = new CardMasksClass();*/

        //private Dictionary<CardAspect, Sprite> titleHolders = new Dictionary<CardAspect, Sprite>();
        private Dictionary<CardType, Sprite> cardMasks = new Dictionary<CardType, Sprite>();
        //private Dictionary<CardAspect, Sprite> unitFrames = new Dictionary<CardAspect, Sprite>();
        //private Dictionary<CardAspect, Sprite> itemFrames = new Dictionary<CardAspect, Sprite>();
        //private Dictionary<CardAspect, Sprite> powerFrames = new Dictionary<CardAspect, Sprite>();
        //private Dictionary<CardAspect, Sprite> energySprites = new Dictionary<CardAspect, Sprite>();
        private Dictionary<CardRarity, Sprite> raritySprites = new Dictionary<CardRarity, Sprite>();
        private Dictionary<CardAspect, CardAspectConfiguration> aspects = new Dictionary<CardAspect, CardAspectConfiguration>();

        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.CardView; } }

        /// <summary>
        /// Set up a bunch of dictionaries that organize assets by aspect so we have an easier time 
        /// initializing the card.
        /// </summary>
        void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
            
            this.aspects[CardAspect.Arcane] = Arcane;
            this.aspects[CardAspect.Tech] = Tech;
            this.aspects[CardAspect.Divine] = Divine;
            this.aspects[CardAspect.Nature] = Nature;
            this.aspects[CardAspect.Chaos] = Chaos;

            this.WhitePowerOutline.SetActive(false);
            this.WhiteUnitOutline.SetActive(false);
            /*
            this.unitFrames[CardAspect.Arcane] = this.UnitFrames.Arcane;
            this.unitFrames[CardAspect.Tech] = this.UnitFrames.Tech;
            this.unitFrames[CardAspect.Divine] = this.UnitFrames.Divine;
            this.unitFrames[CardAspect.Nature] = this.UnitFrames.Nature;
            this.unitFrames[CardAspect.Chaos] = this.UnitFrames.Chaos;

            this.itemFrames[CardAspect.Arcane] = this.ItemFrames.Arcane;
            this.itemFrames[CardAspect.Tech] = this.ItemFrames.Tech;
            this.itemFrames[CardAspect.Divine] = this.ItemFrames.Divine;
            this.itemFrames[CardAspect.Nature] = this.ItemFrames.Nature;
            this.itemFrames[CardAspect.Chaos] = this.ItemFrames.Chaos;
            
            this.powerFrames[CardAspect.Arcane] = this.PowerFrames.Arcane;
            this.powerFrames[CardAspect.Tech] = this.PowerFrames.Tech;
            this.powerFrames[CardAspect.Divine] = this.PowerFrames.Divine;
            this.powerFrames[CardAspect.Nature] = this.PowerFrames.Nature;
            this.powerFrames[CardAspect.Chaos] = this.PowerFrames.Chaos;

			this.titleHolders[CardAspect.Arcane] = this.TitleHolders.Arcane;
			this.titleHolders[CardAspect.Tech] = this.TitleHolders.Tech;
			this.titleHolders[CardAspect.Divine] = this.TitleHolders.Divine;
			this.titleHolders[CardAspect.Nature] = this.TitleHolders.Nature;
			this.titleHolders[CardAspect.Chaos] = this.TitleHolders.Chaos;*/

            this.raritySprites[CardRarity.Common] = Rarities[0];// this.RarityIcons.Common;
            this.raritySprites[CardRarity.Rare] = Rarities[1];//this.RarityIcons.Uncommon;
            this.raritySprites[CardRarity.Epic] = Rarities[2];//this.RarityIcons.Rare;
            this.raritySprites[CardRarity.Legendary] = Rarities[3];//this.RarityIcons.Legendary;

            this.cardMasks [CardType.Power] = this.PowerMask;
			this.cardMasks [CardType.Unit] = this.UnitMask;

            this.Canvas = this.transform.GetChild(0).gameObject;
            this.CardCountFrame.gameObject.SetActive(false);
			this.NewCardTag.gameObject.SetActive(false);
        }

		/*void OnDestroy()
		{
			this.TitleLabel = null;
			this.AttackLabel = null;
			this.EnergyCostLabel = null;
			this.HealthLabel = null;
			this.RulesLabel = null;
			this.SubtypeLabel = null;
			this.CardArt = null;
			this.CardData = null;
			this.CardAbilities = null;
			//this.CardListItemPrefab = null;
			this.DeckLimitFrame = null;
			this.DeckLimitLabel = null;
			this.unitFrames = null;
			this.powerFrames = null;
			this.itemFrames = null;
			this.raritySprites = null;
			this.draggableItem = null;
			this.Canvas = null;
			this.CardCountFrame = null;
		}*/

        public override RectTransform GetRectTransform()
        {
            return (RectTransform)this.CardCanvasGroup.transform;
        }
        
		public override void RefreshIsVisible()
		{
			SetVisible(this.IsVisible);
		}
        
        public override void SetCardArt(Texture cardArtTexture)
		{
            if(cardArtTexture != null)
            {
                this.CardArt.texture = cardArtTexture;
            }
            else
            {
                this.ApplyAspectArt();
            }
            this.LoadingSpinner.gameObject.SetActive(false);
		}
        
        public void LateUpdate()
        {
            if(this.LoadingSpinner.gameObject.activeSelf)
            {
                this.LoadingSpinner.transform.Rotate(new Vector3(0,0, -Time.deltaTime * 100f));
            }
        }

        public void SetVisible(bool isVisible)
        {
            if (this.CanvasGroup != null)
            {
                this.CanvasGroup.alpha = isVisible ? 1.0f : 0;
                this.CanvasGroup.blocksRaycasts = this.CanvasGroup.interactable = isVisible;
            }
        }
        
        IEnumerator fadeCoRoutine = null;
        public void FadeOut(float duration = 1.0f)
        {
           if(this.fadeCoRoutine != null)
           {
                Timer.Instance.StopCoroutine(this.fadeCoRoutine);
           }
           if(this.IsVisible && this.CanvasGroup.alpha > 0f)
           {
                this.fadeCoRoutine = FadeCardCoRoutine(duration, 0f);
                Timer.Instance.StartCoroutine(this.fadeCoRoutine);
           }
        }
        
        public void FadeIn(float duration = 1.0f)
        {
           if(this.fadeCoRoutine != null)
           {
                Timer.Instance.StopCoroutine(this.fadeCoRoutine);
           }
           this.fadeCoRoutine = FadeCardCoRoutine(duration, 1.0f);
           Timer.Instance.StartCoroutine(this.fadeCoRoutine);
        }
        
        IEnumerator FadeCardCoRoutine(float duration, float alpha)
        {
            float deltaTime = Time.deltaTime;
            float startingAlpha = this.CanvasGroup.alpha;
            float range = alpha - startingAlpha;
            while(deltaTime < duration)
            {
                this.CanvasGroup.alpha = startingAlpha + (deltaTime/duration) * range;
                yield return null;
               // Debug.LogFormat("alpha:{0}", this.CanvasGroup.alpha);
                deltaTime += Time.deltaTime;
            }
            this.CanvasGroup.alpha = alpha;
        }

        public void ShowWhiteOutline(bool isVisible)
        {
            this.WhitePowerOutline.SetActive(false);
            this.WhiteUnitOutline.SetActive(false);

            if(isVisible && this.CardData != null)
            {
                switch(this.CardData.CardType)
                {
                    case CardType.Unit:
                        this.WhiteUnitOutline.SetActive(true);
                    break;
                    case CardType.Power:
                        this.WhitePowerOutline.SetActive(true);
                    break;
                }
            }
        }

		public override void RefreshCardData()
		{
			this.DataBind(this.CardData);
		}
        
        public void StartDelayDatabind(CardData cardData, int frames)
        {
            Timer.Instance.StartCoroutine(this.DelayDataBind(cardData,frames));
        }
        
        IEnumerator DelayDataBind(CardData cardData, int frames)
        {
            for(int i=0;i<frames;i++)
            {
                yield return null;
            }
            
            this.DataBind(cardData);
        }

		public void DataBind(CardData cardData)
		{
			if(this.CardAbilities != null)
			{
				this.CardAbilities.HideAbilities();
			}

			this.CardData = cardData;
			this.TitleLocalizer.Term = this.CardData.NameLocId;
			this.name = cardData.Name;
			
         
            if(this.CardCountFrame.gameObject.activeSelf)
                this.CardCountFrame.gameObject.SetActive(false);
            if(this.NewCardTag.gameObject.activeSelf)
                this.NewCardTag.gameObject.SetActive(false);
                

            if(this.CanvasGroup==null)
            {
                this.CanvasGroup = this.Canvas.GetComponent<CanvasGroup>();
            }

            // Load common elements
            this.RulesLabel.text = cardData.GenerateCardText();
			this.SubtypeLabel.text = cardData.Subtype;
			this.EnergyCostLabel.SetText (Number, cardData.Cost);// text = cardData.Cost.ToString();
                                                                 //this.CardArt.texture = Resources.Load(cardData.ArtID) as Texture;
                                                                 //this.CardArt.texture = null;
            
            // Set the bolding for languages
            bool isBold = true;
            if (I2.Loc.LocalizationManager.CurrentLanguage == "Chinese")
                isBold = false;
            TitleLabel.fontStyle = isBold ? FontStyles.Bold | FontStyles.SmallCaps : FontStyles.Normal | FontStyles.SmallCaps;
            RulesLabel.fontStyle = isBold ? FontStyles.Bold : FontStyles.Normal;
            SubtypeLabel.fontStyle = isBold ? FontStyles.Bold | FontStyles.SmallCaps : FontStyles.Normal | FontStyles.SmallCaps;

            Texture tex = AssetBundleManager.Instance.LoadAsset<Texture>(_CardData.ArtID);// Resources.Load(_CardData.ArtID) as Texture;
            this.SetCardArt(tex);
            this.CardArt.gameObject.SetActive(true);
			//Resources.Load("CardListArt/" + cardData.ArtID);
			this.RarityIcon.sprite = this.raritySprites[cardData.Rarity];

            this.UnitDropShadow.gameObject.SetActive(cardData.CardType == CardType.Unit);
            this.PowerDropShadow.gameObject.SetActive(cardData.CardType == CardType.Power);


			this.AttackLabel.gameObject.SetActive (cardData is UnitCard);
			this.HealthLabel.gameObject.SetActive (cardData is UnitCard);

            var aspectData = aspects[CardData.Aspect];

			switch (cardData.CardType)
			{
			case CardType.Item:
				//ItemCard item = (ItemCard)cardData;
                this.CardFrame.sprite = aspectData.itemTexture;// this.itemFrames[item.Aspect];
				break;
			case CardType.Power:
				//PowerCard power = (PowerCard)cardData;
				this.CardFrame.sprite = aspectData.powerTexture;// this.powerFrames [power.Aspect];
                    break;
			case CardType.Unit:
				UnitCard unit = (UnitCard)cardData;
				this.CardFrame.sprite = aspectData.unitTexture;//  this.unitFrames [unit.Aspect];
                this.AttackLabel.SetText (Number, unit.AttackPower);//.text = unit.AttackPower.ToString();
				this.HealthLabel.SetText(Number, unit.Health);//.text = unit.Health.ToString();
				break;
			}
			this.titleHolder.sprite = aspectData.titleHolder;// this.titleHolders [cardData.Aspect];
            this.artMask.sprite = this.cardMasks [cardData.CardType];
		}

        override public void ShowNewCardTag(bool isVisible)
        {
            //Debug.LogFormat("ShowNewCardTag: {0}", isVisible );
            this.NewCardTag.SetActive(isVisible);
        }

        public void LoadCardArt(string assetPath)
        {
			if(this.CardArt.UpdateCardArt(assetPath, this.CardData != null ? this.CardData.Aspect : CardAspect.NoAspect, OnTextureLoaded))
            {
                // Only apply aspect art if the art asset isn't already loaded
                if(!string.IsNullOrEmpty(assetPath))
                {
                    this.LoadingSpinner.gameObject.SetActive(true);
                }
                else
                    this.OnTextureLoaded();
            }
            else
                this.OnTextureLoaded();
        }
        
        private void OnTextureLoaded()
        {
            this.LoadingSpinner.gameObject.SetActive(false);
        }
        
        public void ApplyAspectArt()
        {
            if(this.CardData == null) return;

            string cardArtId = null;
            switch (CardData.Aspect)
            {
                case CardAspect.Arcane: cardArtId = "DC0353"; break;
                case CardAspect.Tech: cardArtId = "DC0351"; break;
                case CardAspect.Divine: cardArtId = "DC0352"; break;
                case CardAspect.Nature: cardArtId = "DC0354"; break;
                case CardAspect.Chaos: cardArtId = "DC0355"; break;
                default: cardArtId = "DC0348"; break;
            }
            CardArt.texture = AssetBundleManager.Instance.LoadAsset<Texture>(cardArtId);
        }

		public void OnPointerClick(PointerEventData eventData)
		{
			//Debug.Log("CardView clicked");
		}
        
        /// <summary>
        /// Initializes the appearance of the card quantity tab that appears below the card in the DeckBuilder
        /// </summary>
        public void SetQuantities(int collectionQuantity, int deckQuantity, int maxOfEachCard = 3, bool isTwoTouch = false)
 		{
            // Make the card greyscale or color depending on if it's owned or not
			foreach (Graphic gfx in this.ContentCanvasGroup.GetComponentsInChildren<Graphic>(true))
            {
                if(gfx is TextMeshProUGUI)
                {
                    ((TextMeshProUGUI)gfx).alpha = collectionQuantity > 0 ? 1f : 0.75f;
                }
				gfx.material = collectionQuantity > 0|| isTwoTouch ? gfx.defaultMaterial : GrayscaleMaterial;
            }

            this.CardFrame.DOFade(collectionQuantity > 0 ? 1f : 0.75f, 0f);

			this.CardCountLabel.color = isTwoTouch || (deckQuantity < maxOfEachCard && deckQuantity < collectionQuantity) ? new Color (37/255f,243/255f,1): new Color(25/255f,120/255f,135/255f) ;
			this.CardCountFrame.color = !isTwoTouch && (deckQuantity < maxOfEachCard && deckQuantity < collectionQuantity) ? new Color (37/255f,243/255f,1,0.8f): new Color(25/255f,120/255f,135/255f,0.8f) ;
			//this.CardCountFrame.DOFade (0.8f, 0.0f);
            for(int i = 0; i< CardNodeArray.Length; i++)
            {
				CardNodeArray[i].gameObject.SetActive(i < deckQuantity);
				//CardNodeArray [i].color = this.CardCountLabel.color;
            }
			//this.CardNodes.gameObject.SetActive (false);
            //this.CardCanvasGroup.alpha = isTwoTouch || (deckQuantity < maxOfEachCard && deckQuantity < collectionQuantity) ? 1.0f : 0.6f;
            //for(int i=0;i<maxOfEachCard;i++)
            //{
                //this.CardNodes.GetChild(i).gameObject.SetActive(i<deckQuantity);
            //}
            // If you have some cards left
            this.CardCountFrame.gameObject.SetActive(true);
            this.CardCountLabel.gameObject.SetActive(true);
			this.CardCountLabel.SetText("x{0}",collectionQuantity);
			//this.CardCountLabel.text = deckQuantity == 0
			//	? collectionQuantity.ToString()
			//	: string.Format("{0}/{1}", deckQuantity, collectionQuantity); 
 		}

        public void MakeFullbright()
        {
             // Make the card greyscale or color depending on if it's owned or not
			foreach (Graphic gfx in this.ContentCanvasGroup.GetComponentsInChildren<Graphic>(true))
            {
                if(gfx is TextMeshProUGUI)
                {
                    ((TextMeshProUGUI)gfx).alpha = 1f;
                }
            }

            this.CardFrame.DOFade(1f, 0f);
            this.CardCountFrame.DOFade(1f,0);
        }

		public void ScaleToWidth(float width)
		{
			var rx = (RectTransform)this.Canvas.transform;
			float ratio = width / rx.rect.width;
			rx.localScale = Vector3.one * ratio;
        }

        public void OnLocalize()
        {
            if(CardData != null)
                DataBind(CardData);
        }

        public void OnDestroy()
        {
            if(this != null && CardArt != null)
               CardArt.texture = null;
        }

        public override void OnRecycle()
        {
            base.OnRecycle();
            CardCanvasGroup.alpha = 1.0f;
            Canvas.transform.DOKill();
            Canvas.transform.localScale = Vector3.one;
            Canvas.transform.localPosition = Vector3.zero;
            CardArt.ResetArt();
            CardData = null;
            ShowWhiteOutline(false);
            IsVisible = true;
            SetVisible(true);
        }
    }

	public interface ICardView
	{
		CardData CardData {get;set;}
		bool IsVisible {get;set;}
	}

	public abstract class BaseCardView : PoolableMonoBehaviour, ICardView
	{
    
		protected CardData _CardData;
		public CardData CardData {
			get { return _CardData; }
			set { bool changed = _CardData != value;
				_CardData = value;
				if (changed && null != value) RefreshCardData(); }
		}

        protected bool isToBeMadeVisible = false;
		protected bool _IsVisible = true;
		public bool IsVisible {
			get { return _IsVisible; }
			set { bool changed = _IsVisible != value;
				_IsVisible = value;
                if(!_IsVisible) this.isToBeMadeVisible = false;
				if (changed) RefreshIsVisible();
			}
		}
        public bool IsSelected {get; set;}
        public virtual RectTransform GetRectTransform() { return (RectTransform)this.transform;}
		public virtual void RefreshCardData() {}
		public virtual void RefreshIsVisible() {}
        public virtual void ShowCollectionTotal(bool isVisible, int collectionTotal = 0, int deckTotal=0) {}
        public virtual void SelectCard(bool isSelected) {}
        public virtual void SetCardArt(Texture cardArtTexture) {}
        public virtual void ShowNewCardTag(bool isVisible) {
            //Debug.LogFormat("ShowNewCardTag BASE: {0}", isVisible );
        }
	}
}
