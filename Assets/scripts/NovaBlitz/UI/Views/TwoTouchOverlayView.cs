﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.Economy;
using Messages;
using DG.Tweening;
using DragonFoundry.Fx;

namespace NovaBlitz.UI
{
	public class TwoTouchOverlayView : AnimatedView, IRectTransform
	{

		[SerializeField] protected RectTransform _BoundingRectXF;
		[SerializeField] protected CardTwoTouchView _TwoTouchPrefab;
		[SerializeField] protected float _DesktopScaleFactor = 0.67f;
		[SerializeField] protected float _PhoneScaleFactor = 1f;
		[Inject] public INovaContext NovaContext {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public CardTwoTouchOpenedSignal CardTwoTouchOpenedSignal {get;set;}
		[Inject] public CardTwoTouchClosedSignal CardTwoTouchClosedSignal {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
	[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public ViewStateStack ViewStateStack {get;set;}
		[Inject] public ViewReferences ViewReferences { get;set;}

		
		public CardAbilityController CardAbilities;
		public CanvasGroup CardAbilitiesCanvasGroup;
		public RectTransform CardTotalController;
		public RectTransform CardCraftingController;
		public CanvasGroup CardTotalCircuit;
		public CanvasGroup CardCraftingCircuit;
		public RectTransform CardTotalsSpark;
		public Image CardTotalBurst;
		public Image CountCapsuel;
		public RectTransform CardNodes;
		public TextMeshProUGUI GemCountLabel;
		public TextMeshProUGUI CraftPriceLabel;
		public TextMeshProUGUI ScrapBonusLabel;
        public TextMeshProUGUI CraftButtonLabel;
        public TextMeshProUGUI ScrapButtonLabel;
        public TextMeshProUGUI ArtistNameLabel;
		public CanvasGroup ArtistNameCanvasGroup;
		public NovaButton AddOneButton;
		public NovaButton RemoveOneButton;
		public NovaButton CraftButton;
		public NovaButton ScrapButton;
        public NovaButton UndoScrapButton;
        public NovaButton UndoCraftButton;
		public Image LoadingOverlay;
		public Image LoadingSpinner;
		public ParticleEffectsTrigger CraftFx;
        public Image TopScrim;
        public Image ShortScrim;
        public Image FullScrim;
        public ParticleEffectsTrigger ButtonFx;

        public Color Blue;
		public Color Green;
		public Color Red;
		public Color Gray;

		public Image CraftNanitesIcon;
		public Image ScrapNanitesIcon;

		public RectTransform RectTransform {get; protected set;}
		public CanvasGroup CanvasGroup {get; protected set;}
		public CardTwoTouchView TwoTouchInstance {get; protected set;}
		public bool IsSmallScreen {get;set;}

		public System.Action AddOneClicked;
		public System.Action RemoveOneClicked;
		public System.Action CraftClicked;
		public System.Action ScrapClicked;
        public System.Action UndoCraftClicked;
        public System.Action UndoScrapClicked;
        public System.Action CloseTwoTouch;
        public System.Action OpenTwoTouch;

        private Vector3 originalPosition;
		private Sequence sequence;
		private RectTransform craftRectTransform;
		private RectTransform totalsRectTransform;
		
		private DraggableCardView draggableCardView;
        private bool _isOpened = false;
        private bool _IsEditable = false;

        public int NumberToCraft { get; set; }

		override public bool IsOpen 
		{
			get 
			{ 
				return this.CanvasGroup != null && this.CanvasGroup.blocksRaycasts; 
			}
			set 
			{ 
				base.IsOpen = value;
				if (this.CanvasGroup != null) {
					this.CanvasGroup.blocksRaycasts = value; 
					this.CanvasGroup.alpha = value ? 1f : 0f; 
				}
				
                if (!value)
                {
					//Debug.Log("CloseCalled bitches!");
                    ClearTwoTouch();

                    if( this.CloseTwoTouch != null)
                    {
                        this.CloseTwoTouch.Invoke();
                    }
                }
			}
		}

		public void LateUpdate()
        {
            if(this.LoadingSpinner.gameObject.activeSelf)
            {
                this.LoadingSpinner.transform.Rotate(new Vector3(0,0, -Time.deltaTime * 100f));
            }
        }

		protected override void OnDestroy()
		{
			this.TwoTouchInstance.CardClicked += this.OnTwoTouchCardClicked;
			base.OnDestroy();
		}

		public void HideKeywords()
		{
			this.CardAbilitiesCanvasGroup.alpha = 0;
			this.CardAbilities.HideAbilities();
		}

		public void InitializeAndOpenTwoTouch(PlayerProfile playerProfile, TwoTouchParams twoTouchParams)
		{
            this.gameObject.SetActive(true);
            //this.TwoTouchInstance.transform.DOKill();

            //Debug.LogFormat("PlaceTwoTouch: isEditable={0}", twoTouchParams.IsEditable);
			CardData cardData = twoTouchParams.CardData;
			bool isEditable = twoTouchParams.IsEditable;
			float scale = twoTouchParams.DestCardScale;
			RectTransform sourceTransform = twoTouchParams.SourceTransform;
			bool isFullScrim = twoTouchParams.IsFullScrim;
			bool isBlurred = twoTouchParams.IsBlurred;

			this.draggableCardView = twoTouchParams.DraggableCardView;

			int numOwnedCards;
			playerProfile.OwnedCards.TryGetValue(cardData.CardID, out numOwnedCards);

			//int deckCount = playerProfile.CurrentDeck.Cards.Where(cd=>cd.CardID==cardData.CardID).Count();

			this.RectTransform.SetAsLastSibling();
			
			this.TwoTouchInstance.CardData = cardData;
			this.TwoTouchInstance.ShowNewCardTag(playerProfile.NewCards.Contains(cardData.CardID));
			
			if(isEditable == false)
			{
				this.TwoTouchInstance.HideCollectionTotal();
			}

            this.CardTotalCircuit.DOFade(0f,0f);
			this.CardCraftingCircuit.DOFade(0f,0f);
			this.ArtistNameCanvasGroup.alpha = 0;
			
			this.TwoTouchInstance.IsEditable = false;

			StartCoroutine(ConstrainTwoTouch(cardData, sourceTransform, scale, isEditable, isFullScrim, isBlurred));

			if(cardData.NewHotnessKeywords.Count > 0)
			{
				this.CardAbilitiesCanvasGroup.alpha = 0;
				this.CardAbilities.Initialize(cardData.NewHotnessKeywords);
			}
			else
			{
				this.CardAbilitiesCanvasGroup.alpha = 0;
				this.CardAbilities.HideAbilities();
			}

			this.CardCraftingController.SetAsLastSibling();
			this.CardAbilities.transform.parent.SetAsLastSibling();
		}

		protected void ClearTwoTouch()
		{
			if (null != this.TwoTouchInstance) {
				this.TwoTouchInstance.IsVisible = false;
				this.TwoTouchInstance.CardData = null;
				this.CardTotalController.gameObject.SetActive(false);
				this.CardCraftingController.gameObject.SetActive(false);
				this.ArtistNameCanvasGroup.alpha = 0;
			}
		}

		protected IEnumerator ConstrainTwoTouch(CardData cardData, RectTransform rectTrans, float scale, bool isEditable, bool isFullScrim=false, bool isBlurred=true)
        {
            if (this.sequence != null) { this.sequence.Kill(); }
            yield return null;

			this.CardTwoTouchOpenedSignal.Dispatch(cardData);

			if(isBlurred)
			{
				this.OpenBlurredAnimatedViewSignal.Dispatch(this.GetType());
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this, NovaCanvas.Overlay, 0.0f);
            }
			else
			{
				this.OpenAnimatedViewSignal.Dispatch(this.GetType());
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this, NovaCanvas.Overlay, 0.0f);
            }

            this.TwoTouchInstance.GetComponent<CanvasGroup>().DOFade(1.0f, 0.0f);
            this.TwoTouchInstance.gameObject.SetActive(true);
            this.TwoTouchInstance.IsVisible = true;
			Rect boundingRect = _BoundingRectXF.rect;
			Rect twoTouchRect = this.TwoTouchInstance.RectTransform.rect;

			this.CardTotalController.gameObject.SetActive(isEditable);
			this.CardCraftingController.gameObject.SetActive(isEditable && PlayerProfile.HasUnlockedAllDecks);

			// Full scrim or short scrim?
            this.FullScrim.gameObject.SetActive(isFullScrim);
            this.ShortScrim.gameObject.SetActive(!isFullScrim);
            MainHeaderView header = ViewReferences.Get<MainHeaderView>();
            this.TopScrim.gameObject.SetActive(!isFullScrim && header != null && header.IsOpen);

            // Adjust the size of the bounding rect to match the scrim
            /*
			if(isFullScrim)
			{
				this._BoundingRectXF.offsetMin = this.FullScrim.rectTransform.offsetMin;
				this._BoundingRectXF.offsetMax = this.FullScrim.rectTransform.offsetMax;
			}
			else
			{
				this._BoundingRectXF.offsetMin = this.ShortScrim.rectTransform.offsetMin;
				this._BoundingRectXF.offsetMax = this.ShortScrim.rectTransform.offsetMax;
			}
			*/

            if (_isOpened && isEditable == this._IsEditable && isEditable == false)
                yield break;

            if (isEditable == false)
			{
				// scale
				float scale2 = this.IsSmallScreen ? _PhoneScaleFactor : _DesktopScaleFactor;
				float hFactor = scale2 * boundingRect.height / twoTouchRect.height;
				this.TwoTouchInstance.RectTransform.localScale = Vector3.one * hFactor;
				twoTouchRect.size *= hFactor;
			}
			else
			{
				// Position the Totals Box Offscreen and in the correct xPos
				float xPos = boundingRect.width * 0.25f + 18;
				this.CardTotalController.DOAnchorPosX(xPos, 0f);
				this.CardTotalController.DOAnchorPosY(700f,0f);

				// Position the Crafting Box offscreen and in the correct XPos
				this.CardCraftingController.DOAnchorPosX(xPos, 0f);
				this.CardCraftingController.DOAnchorPosY(-700,0f);

				// Position the card totals circuit
				this.totalsRectTransform.DOAnchorPosX(xPos,0f);
				//((RectTransform)this.CardTotalCircuit.transform).DOSizeDelta(new Vector2(xPos*2f, 100f),0,false);
				this.totalsRectTransform.DOSizeDelta(new Vector2(35f,100f),0,false);

				// Position the crafting circuit
				this.craftRectTransform.DOAnchorPosX(-xPos - 200f,0f);
				((RectTransform)this.CardCraftingCircuit.transform).DOSizeDelta(new Vector2(xPos+160f, 45f),0,false);
				this.craftRectTransform.DOSizeDelta(new Vector2(35f,350f),0,false);

				// Tween in the UI Boxes
				this.sequence = DOTween.Sequence();
				this.sequence.AppendInterval(0.1f);
				this.sequence.Append(this.CardTotalController.DOAnchorPosY(180f, 0.5f).SetEase(Ease.OutQuart));
				this.sequence.Insert(0.1f, this.CardCraftingController.DOAnchorPosY(-165f,0.5f).SetEase(Ease.OutQuart));
                this.sequence.Insert(0.0f, this.TwoTouchInstance.GetComponent<CanvasGroup>().DOFade(1.0f, 0.3f));
                this.sequence.Insert(0.45f, this.CardTotalCircuit.DOFade(1f, 0.2f));
				this.sequence.Join(this.CardCraftingCircuit.DOFade(1f, 0.2f));
				this.sequence.Join(this.ArtistNameCanvasGroup.DOFade(1f,0.4f));
				this.sequence.Insert(0.2f, this.craftRectTransform.DOSizeDelta(new Vector2(xPos+250,350f),1f, false).SetEase(Ease.OutExpo));
				this.sequence.Insert(0.5f, this.totalsRectTransform.DOSizeDelta(new Vector2(xPos*2,100f),0.2f, false).SetEase(Ease.OutExpo));
				//this.sequence.Play();
			}

			// assume center pivot
			twoTouchRect.center = (Vector2)_BoundingRectXF.InverseTransformPoint(rectTrans.position); // "parent" the 2d rect
			twoTouchRect.x = Mathf.Max(twoTouchRect.x, boundingRect.xMin);
			twoTouchRect.x = Mathf.Min(twoTouchRect.x, boundingRect.xMax - twoTouchRect.width);
			twoTouchRect.y = Mathf.Max(twoTouchRect.y, boundingRect.yMin);
			twoTouchRect.y = Mathf.Min(twoTouchRect.y, boundingRect.yMax - twoTouchRect.height);
			this.TwoTouchInstance.RectTransform.anchoredPosition = twoTouchRect.center;
			this.originalPosition = twoTouchRect.center;

			if(isEditable)
            {
                this.TwoTouchInstance.transform.DOScale(Vector3.one, 0f);
				this.sequence.Insert(0.0f,this.TwoTouchInstance.transform.DOScale(Vector3.one * scale, 0.7f).SetEase(Ease.OutQuart));
                this.sequence.Insert(0.0f, ((RectTransform)this.TwoTouchInstance.transform).DOAnchorPos(new Vector3(-220f, -15f), 0.7f).SetEase(Ease.OutQuart));
                /*this.sequence.InsertCallback(0.85f, ()=>{
						if(this.CardAbilities != null)
						{
							this.TwoTouchInstance.CardAbilityCanvasGroup.DOFade(1.0f, 0.15f);
						}
					});*/
                //this.sequence.Play();
            }
            this._isOpened = true;
            this._IsEditable = isEditable;
        }

		#region uGUI
		public void ClickScrim()
        {
            this._isOpened = false;

            if (this.draggableCardView != null)
			{
				this.draggableCardView.SelectCard(false);
			}

			Debug.Log("ClickScrim: Closing TwoTouch containing CardData " +this.TwoTouchInstance.CardData );
			this.CardTwoTouchClosedSignal.Dispatch(this.TwoTouchInstance.CardData);
			if(this.ViewStateStack.Count <= 2)
			{
				// Coopting the default ViewStateStack behavior, if there are two or less states
				// in the view stack we know that the camera will unblur. Asuuming the top state was opened by the Two touch.
				// in order to get a nice looking tween, we trigger the unblur a little early so that the closing animation coinsides
				// with the unblur. -DMac
				this.NovaContext.BlurMainCanvas(false, this);
			}

            if (this.sequence != null) { this.sequence.Kill(); }
            sequence = DOTween.Sequence();
            sequence.Insert(0.0f, ((RectTransform)this.TwoTouchInstance.transform).DOAnchorPos(this.originalPosition, 0.3f));
            sequence.InsertCallback(0.3f,()=>this.CloseAnimatedViewSignal.Dispatch(this.GetType()));
            sequence.Insert(0.0f, this.TwoTouchInstance.transform.DOScale(Vector3.one, 0.4f));
            sequence.Insert(0.0f, this.TwoTouchInstance.GetComponent<CanvasGroup>().DOFade(0f, 0.3f));
            sequence.InsertCallback(0.25f, ()=>this.TwoTouchInstance.gameObject.SetActive(false));
            sequence.Insert(0.0f, this.CardTotalController.DOAnchorPosY(700f, 0.2f));
            sequence.Insert(0.0f, this.CardCraftingController.DOAnchorPosY(-700f,0.2f));
            sequence.Insert(0.0f, this.CardTotalCircuit.DOFade(0f, 0.1f));
            sequence.Insert(0.0f, this.CardCraftingCircuit.DOFade(0f, 0.1f));
            sequence.InsertCallback(0.0f, ()=>this.CanvasGroup.blocksRaycasts = false);
            this.ArtistNameCanvasGroup.alpha = 0;
			this.HideKeywords();
            this.FullScrim.gameObject.SetActive(false);
            this.ShortScrim.gameObject.SetActive(false);

            ClearTwoTouch();

            if (this.CloseTwoTouch != null)
            {
                this.CloseTwoTouch.Invoke();
            }
        }
		#endregion

		private Sequence addOneSpark;
		public void PlayAddOneSparkAnimation(Action addOneAction)
		{
			float parentWidth = ((RectTransform)this.CardTotalsSpark.parent).rect.width;
			//Debug.LogFormat("Parent Width:{0}", parentWidth);

			if(this.addOneSpark != null && this.addOneSpark.IsPlaying())
			{
				this.addOneSpark.Kill(true);
			}

			this.addOneSpark = DOTween.Sequence();
			
			this.addOneSpark.Append(this.CardTotalsSpark.DOAnchorPosX(parentWidth/-1.4f, 0.01f));
			this.addOneSpark.Append(this.CardTotalsSpark.DOAnchorPosX(parentWidth/1.4f, 0.5f));
			this.addOneSpark.Append(this.CardTotalsSpark.DOAnchorPosX(0, 0));
			this.addOneSpark.Play();
			this.StartCoroutine(this.WaitToAddOne(0.2f, addOneAction));
		}

		private IEnumerator WaitToAddOne(float delay, Action addOneAction)
		{
			yield return new WaitForSecondsRealtime(delay);
			Sequence seq = DOTween.Sequence();
			seq.Append(this.CardTotalBurst.DOFade(1f,0));
			seq.AppendInterval(0.1f);
			seq.Append(this.CardTotalBurst.DOFade(0f, 0.3f));
			seq.Play();
			addOneAction.Invoke();
		}

		public void PlayRemoveOneSparkAnimation()
		{
			if(this.addOneSpark != null)
			{
				this.addOneSpark.Kill();
			}
			float parentWidth = ((RectTransform)this.CardTotalsSpark.parent).rect.width;
			Sequence seq = DOTween.Sequence();
			seq.Append(this.CardTotalsSpark.DOAnchorPosX(parentWidth/1.5f, 0.01f));
			seq.Append(this.CardTotalsSpark.DOAnchorPosX(parentWidth/-1.5f, 0.5f));
			seq.Append(this.CardTotalsSpark.DOAnchorPosX(0, 0));
			seq.Play();

			Sequence seq2 = DOTween.Sequence();
			seq2.Append(this.CardTotalBurst.DOFade(1f,0));
			seq2.AppendInterval(0.1f);
			seq2.Append(this.CardTotalBurst.DOFade(0f, 0.3f));
			seq2.Play();

		}

		#region Mono
		protected override void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
			this.CanvasGroup = GetComponent<CanvasGroup>();
			this.IsOpen = false;

			this.TwoTouchInstance = GameObject.Instantiate(_TwoTouchPrefab);
			this.TwoTouchInstance.RectTransform.SetParent(_BoundingRectXF);
			this.TwoTouchInstance.RectTransform.localScale = Vector3.one;
			this.TwoTouchInstance.RectTransform.localPosition = Vector3.zero;
			this.TwoTouchInstance.CardAbilityCanvasGroup = this.CardAbilitiesCanvasGroup;
			this.TwoTouchInstance.CardClicked += this.OnTwoTouchCardClicked;
			this.CardTotalController.gameObject.SetActive(false);
			this.CardCraftingController.gameObject.SetActive(false);

			this.craftRectTransform = (RectTransform)this.CardCraftingCircuit.transform.parent;
			this.totalsRectTransform = (RectTransform)this.CardTotalCircuit.transform.parent;

			this.CardTotalsSpark.DOAnchorPosX(-1000f,0);
			this.CardTotalBurst.DOFade(0,0f);

			base.Awake();
		}

        private void OnTwoTouchCardClicked()
        {
            this.ClickScrim();
        }

        protected override void Start()
		{
			UITools.MaximizeRect(this.RectTransform);
			base.Start();
		}
		#endregion

		public void ButtonAddOneClicked()
		{
			if(this.AddOneClicked != null)
			{
				this.PlayAddOneSparkAnimation(this.AddOneClicked);
			}
		}

		public void ButtonRemoveOneClicked()
		{
			if(this.RemoveOneClicked != null)
			{
				this.RemoveOneClicked.Invoke();
				this.PlayRemoveOneSparkAnimation();
			}
		}

		public void ButtonCraftClicked()
		{
			if(this.CraftClicked != null)
			{
				this.CraftClicked.Invoke();
			}
		}

		public void ButtonSellClicked()
		{
			if(this.ScrapClicked != null)
			{
				this.ScrapClicked.Invoke();
			}
        }
        public void ButtonUndoCraftClicked()
        {
            if (this.UndoCraftClicked != null)
            {
                this.UndoCraftClicked.Invoke();
            }
        }
        public void ButtonUndoScrapClicked()
        {
            if (this.UndoScrapClicked != null)
            {
                this.UndoScrapClicked.Invoke();
            }
        }


        internal void OnCraftingComplete()
        {
            //this.LoadingOverlay.gameObject.SetActive(false);
			this.CraftFx.StartEffect();
			this.TwoTouchInstance.PlaySettleIntoPlaceTween();
        }

        internal void OnScrapComplete()
        {
            // this.LoadingOverlay.gameObject.SetActive(false);
            this.CraftFx.StartEffect();
            this.TwoTouchInstance.PlaySettleIntoPlaceTween();
        }
    }

	public interface ITwoTouchOverlay : IViewOpen
	{
		void PlaceTwoTouch(CardData cardData, float scale, RectTransform rectTrans, bool isEditable, DraggableCardView draggableCardView = null, bool isFullScrim = false, bool isBlurred = true);
		void ClickScrim();
		void HideKeywords();
	}
}

namespace NovaBlitz.UI.Mediators
{
	public class TwoTouchOverlayMediator : Mediator
	{
		[Inject] public TwoTouchOverlayView View {get;set;}

		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}

		[Inject] public AddCardToDeckSignal AddCardToDeckSignal {get;set;}
		[Inject] public RemoveCardFromDeckSignal RemoveCardFromDeckSignal {get; set;}
		[Inject] public CraftItemSignal CraftItemSignal {get;set;}
		[Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal {get; set;}

		[Inject] public CardAddedToDeckSignal CardAddedToDeckSignal {get;set;}
		[Inject] public CardRemovedFromDeckSignal CardRemovedFromDeckSignal {get; set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
        [Inject] public SaveUiHintDataSignal SaveUiHintDataSignal { get; set; }
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
        [Inject] public ShowPurchaseOffersSignal ShowPurchaseOffersSignal { get; set; }

        //private const string ARTIST = I2.Loc.ScriptLocalization.Artist_Name;

        // crafting data storage
        [Inject] public CraftResponseSignal CraftResponseSignal { get; set; }
        private int _NumberToCraft;
        protected int NumberToCraft { get { return _NumberToCraft; } set { _NumberToCraft = value; this.View.NumberToCraft = value; } }
        protected int NumberOfCardInDeck { get; set; }

        protected int _CardID;
		protected string _CardItemInstanceID;
		protected int _CardNumOwned;
		protected uint _CardCraftPrice;
		protected uint _CardDisenchantPrice;
		protected bool _GotCraftPrice;
		protected bool _GotDisenchantPrice;
		protected int _Nanites;
        protected CardData _CraftingCard;

		protected int _CraftingCardsAdded = 0;

        //private const string CRAFT = "Craft";
        //private const string SCRAP = "Scrap";
        //private const string UNDO = "Undo";

        public override void OnRegister()
		{
			this.View.IsSmallScreen = this.UIConfiguration.IsSmallScreen;

			this.View.TwoTouchInstance.EventCardChanged += OnView_CardChanged;
			this.View.AddOneClicked += OnView_DeckAddClicked;
			this.View.RemoveOneClicked += OnView_DeckRemoveClicked;
			this.View.CraftClicked += OnView_CraftClicked;
			this.View.ScrapClicked += OnView_ScrapClicked;
            this.View.UndoScrapClicked += OnView_CraftClicked;
            this.View.UndoCraftClicked += OnView_ScrapClicked;
            this.View.CloseTwoTouch += OnView_CloseTwoTouch;
            this.CardAddedToDeckSignal.AddListener(OnSignal_CardAdded);
			this.CardRemovedFromDeckSignal.AddListener(OnSignal_CardRemoved);
			this.ProfileUpdatedSignal.AddListener(OnSignal_ProfileUpdated);
            this.CraftResponseSignal.AddListener(OnSignal_CraftResponse);
            this.MoveAnimatedViewToCanvasSignal.AddListener(this.OnMoveAnimatedViewToCanvas);

            if (null != this.View.TwoTouchInstance.CardData && null != this.PlayerProfile.CurrentDeck)
				OnView_CardChanged(this.View.TwoTouchInstance.CardData);
		}

		public override void OnRemove()
		{
			this.View.TwoTouchInstance.EventCardChanged -= OnView_CardChanged;
			this.View.AddOneClicked -= OnView_DeckAddClicked;
			this.View.RemoveOneClicked -= OnView_DeckRemoveClicked;
			this.View.CraftClicked -= OnView_CraftClicked;
			this.View.ScrapClicked -= OnView_ScrapClicked;
            this.View.UndoScrapClicked -= OnView_CraftClicked;
            this.View.UndoCraftClicked -= OnView_ScrapClicked;
            this.View.CloseTwoTouch -= OnView_CloseTwoTouch;
            this.CardAddedToDeckSignal.RemoveListener(OnSignal_CardAdded);
			this.CardRemovedFromDeckSignal.RemoveListener(OnSignal_CardRemoved);
			this.ProfileUpdatedSignal.RemoveListener(OnSignal_ProfileUpdated);
            this.CraftResponseSignal.RemoveListener(OnSignal_CraftResponse);
            this.MoveAnimatedViewToCanvasSignal.RemoveListener(this.OnMoveAnimatedViewToCanvas);
            if (_PlayFxCoroutine != null)
            {
                Timer.Instance.StopCoroutine(_PlayFxCoroutine);
                _PlayFxCoroutine = null;
            }
            if (_WaitThenFixFx != null)
            {
                Timer.Instance.StopCoroutine(_WaitThenFixFx);
                _WaitThenFixFx = null;
            }
        }


        protected void UpdateUiHints()
        {
            Timer.Instance.StartCoroutine(UpdateUiHintsCoroutine());
        }
        protected IEnumerator UpdateUiHintsCoroutine()
        {
            yield return null;
            if(this.PlayerProfile.UiHintData.DeckBuilder == 0 && this.View.AddOneButton.interactable)
            {
                //Debug.LogWarning("UI hints updated: ADD");
                PlayFx(this.View.AddOneButton);
                //SetGlow(this.View.AddGlow);
            }
            else if (this.PlayerProfile.UiHintData.DeckBuilder == 1 && this.View.RemoveOneButton.interactable)
            {
                //Debug.LogWarning("UI hints updated: REMOVE");
                PlayFx(this.View.RemoveOneButton);
                //SetGlow(this.View.RemoveGlow);
            }
            else if (this.PlayerProfile.UiHintData.Crafting == 0
                && NumberToCraft == 0
                && !this.View.UndoScrapButton.gameObject.activeInHierarchy
                && this.View.CraftButton.gameObject.activeInHierarchy
                && this.View.CraftButton.interactable) // can craft
            {
                //Debug.LogWarning("UI hints updated: CRAFT");
                PlayFx(this.View.CraftButton);
                //SetGlow(this.View.CraftGlow);
            }
            else if (this.PlayerProfile.UiHintData.Scrapping == 0 && this.PlayerProfile.UiHintData.Crafting > 0
                && NumberToCraft == 0
                && !this.View.UndoCraftButton.gameObject.activeInHierarchy
                && this.View.ScrapButton.gameObject.activeInHierarchy
                && this.View.ScrapButton.interactable) // can scrap
            {
                //Debug.LogWarning("UI hints updated: SCRAP");
                PlayFx(this.View.ScrapButton);
                //SetGlow(this.View.ScrapGlow);
            }
            else
            {
                //Debug.LogWarningFormat("UI hints updated: NONE; {0}", this.View.AddOneButton.interactable);
                PlayFx(null);
                //SetGlow(null);
            }
        }

        /*public void SetGlow(CanvasGroup glowCanvas)
        {
            this.View.AddGlow.DOFade(0.0f, 1.0f);
            this.View.RemoveGlow.DOFade(0.0f, 1.0f);
            this.View.CraftGlow.DOFade(0.0f, 1.0f);
            this.View.ScrapGlow.DOFade(0.0f, 1.0f);
            if (glowCanvas != null)
                glowCanvas.DOFade(1.0f, 1.0f);
        }*/

        public void PlayFx(Button button)
        {
            _PlayFxCoroutine = PlayFxCoroutine(button);
            Timer.Instance.StartCoroutine(_PlayFxCoroutine);
        }
        private IEnumerator _PlayFxCoroutine;
        private IEnumerator PlayFxCoroutine(Button button)
        {
            yield return null;
            if (View == null)
                yield break;
            if (button == null)
            {
                this.View.ButtonFx.EndEffect();
                this.View.ButtonFx.gameObject.SetActive(false);
                IsPlaying = false;
            }
            else
            {
                this.View.ButtonFx.transform.AttachToParent(button.transform, button.transform.childCount);
                this.View.ButtonFx.transform.localPosition = Vector3.zero;
                yield return null;
                if (View == null || button == null)
                    yield break;
                View.ButtonFx.gameObject.SetActive(true);
                while (View != null && !View.ButtonFx.gameObject.activeInHierarchy)
                {
                    yield return null;
                }
                if (View == null || button == null)
                    yield break;
                View.ButtonFx.StartEffect();
                IsPlaying = true;
            }
        }

        private bool IsPlaying { get; set; }
        private IEnumerator _WaitThenFixFx;
        protected void OnMoveAnimatedViewToCanvas(AnimatedView view, NovaCanvas canvas, float delayTime)
        {
            if (view == this.View && this.View.IsOpen && _WaitThenFixFx == null)
            {
                _WaitThenFixFx = WaitThenFixFx();
                Timer.Instance.StartCoroutine(_WaitThenFixFx);
            }
        }

        private IEnumerator WaitThenFixFx()
        {
            yield return null;
            if (this.View.IsOpen && IsPlaying)
            {
                if (this.NovaContext.IsAlreadyBlurred)
                {
                    View.ButtonFx.EndEffect();
                    View.ButtonFx.gameObject.SetActive(false);
                }
                else
                {
                    View.ButtonFx.gameObject.SetActive(true);
                    View.ButtonFx.StartEffect();
                }
            }
            _WaitThenFixFx = null;
        }

        protected void RefreshDeckEditing(CardData card)
		{
			var deck = this.PlayerProfile.CurrentDeck;
            if (deck == null)
            {
                Debug.LogErrorFormat("RefreshDeckEditing: current deck is null");
                return;
            }

            NumberOfCardInDeck = deck.Cards.Where(c => c.CardID == card.CardID).Count();
			int numOwned; this.PlayerProfile.OwnedCards.TryGetValue(card.CardID, out numOwned);

            // in-progress crafting:
            if (card.CardID == _CardID)
                numOwned += NumberToCraft;

            for (int i=0;i<deck.MaxOfEachCard;i++)
            {
                this.View.CardNodes.GetChild(i).gameObject.SetActive(i< NumberOfCardInDeck);
            }

            //if (numOwned < 3) numOwned = 3; //HACK: make the deckbuilder not care about card ownership
            string artistCredit;
            GameData.ArtistCredits.TryGetValue(card.ArtID, out artistCredit);
            this.View.ArtistNameLabel.SetText(I2.Loc.ScriptLocalization.Artist_Name + artistCredit);


            this.View.AddOneButton.interactable = 
                deck.DeckList.Count < deck.MaxDeckSize 
                && NumberOfCardInDeck < deck.MaxOfEachCard 
                && (numOwned - NumberOfCardInDeck) > 0
                && (deck.Aspects.Contains(card.Aspect) || deck.Aspects.Count < 2);
			this.View.RemoveOneButton.interactable = deck.Cards.Where(c => c.CardID == card.CardID).FirstOrDefault() != null;
			this.RefreshCollectinTotal(card);
		}

		protected void RefreshCrafting(CardData card)
		{
            _CraftingCard = card;
			CacheCraftingData(card.CardID);

            bool canUndoScrap = this.NumberToCraft < 0;
            bool canUndoCraft = this.NumberToCraft > 0;

            //this.View.CraftPriceLabel.text = "-" + _CardCraftPrice;
			//this.View.ScrapBonusLabel.text = _CardDisenchantPrice.ToString();
			this.View.GemCountLabel.text = _Nanites.ToString();

            uint price;
            NovaCatalogItem item;
            bool isCraftable = GameData.CardProductCache.TryGetValue(card.CardID, out item) && item.VirtualCurrencyPrices != null && item.VirtualCurrencyPrices.TryGetValue(CurrencyType.NC, out price) && price > 0;
            bool isScrappable = this.CanScrap && isCraftable && item.Container != null && item.Container.VirtualCurrencyContents != null && item.Container.VirtualCurrencyContents.TryGetValue(CurrencyType.NC, out price) && price > 0;
            isCraftable = this.CanCraft && isCraftable;

            // If gemsOwned < crafting price, can't craft.
            this.View.CraftButton.interactable = isCraftable;
            this.View.UndoScrapButton.interactable = isCraftable;

            this.View.UndoScrapButton.gameObject.SetActive(canUndoScrap);
            this.View.CraftPriceLabel.text = canUndoScrap ? string.Format("-{0}", _CardDisenchantPrice) : string.Format("-{0}", _CardCraftPrice);
            this.View.CraftPriceLabel.color = canUndoScrap ? Color.cyan : Color.red;

            if (isCraftable || canUndoScrap)
			{
				this.View.CraftPriceLabel.alpha = 1f;
				this.View.CraftNanitesIcon.DOFade(1f,0f);
			}
			else
			{
				this.View.CraftPriceLabel.alpha = 0.4f;
				this.View.CraftNanitesIcon.DOFade(0.4f,0f);
			}

			// If Cards Owned = 0, or there's no instance ID logged, can't sell.
			this.View.ScrapButton.interactable = isScrappable;
            this.View.UndoCraftButton.interactable = isScrappable;

            this.View.UndoCraftButton.gameObject.SetActive(canUndoCraft);
            this.View.ScrapBonusLabel.text = canUndoCraft ? string.Format("+{0}", _CardCraftPrice) : string.Format("+{0}", _CardDisenchantPrice);
            this.View.ScrapBonusLabel.color = canUndoCraft ? Color.cyan : Color.magenta;

            if (isScrappable || canUndoCraft)
			{
				this.View.ScrapBonusLabel.alpha = 1f;
				this.View.ScrapNanitesIcon.DOFade(1f,0f);
			}
			else
			{
				this.View.ScrapBonusLabel.alpha = 0.4f;
				this.View.ScrapNanitesIcon.DOFade(0.4f,0f);
			}

        }

		protected void RefreshCollectinTotal(CardData cardData)
		{
			int numOwnedCards;
			this.PlayerProfile.OwnedCards.TryGetValue(cardData.CardID, out numOwnedCards);
			int numCardsInDeck = this.PlayerProfile.CurrentDeck.Cards.Where(c => c.CardID == cardData.CardID).Count();
            if(cardData.CardID == _CardID)
                numOwnedCards += NumberToCraft;
			this.View.TwoTouchInstance.ShowCollectionTotal(true, numOwnedCards,numCardsInDeck);

		}

		#region Crafting Data
		protected void CacheCraftingData(int cardID)
		{
			_GotCraftPrice = false;
			_GotDisenchantPrice = false;
			_CardID = cardID;
			_Nanites = GetNanites();
            _CardNumOwned = GetNumberOwned(cardID);

			var cardProduct = GetCardProduct(cardID);
			if (null != cardProduct) {
				_CardCraftPrice = GetCraftPrice(cardProduct);
				_CardDisenchantPrice = GetDisenchantPrice(cardProduct);
			}
		}

		protected int GetNanites() 
		{
			int nanites;
			this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.NC, out nanites);
            var nanitesToAdd = NumberToCraft > 0 ? -NumberToCraft * (int)_CardCraftPrice : -NumberToCraft * (int)_CardDisenchantPrice;
			return nanites + nanitesToAdd;
		}

        protected int GetNumberOwned(int cardID)
        {
            int numberOwned;
            this.PlayerProfile.OwnedCards.TryGetValue(cardID, out numberOwned);
            return numberOwned + this.NumberToCraft;
        }

		protected NovaCatalogItem GetCardProduct(int cardID)
		{
            NovaCatalogItem cardProduct;
			if (!this.GameData.ProductCache.TryGetValue(cardID.ToString(), out cardProduct)) {
				Debug.LogErrorFormat("card {0} not found in card catalog", cardID);
				return null;
			}
			return cardProduct;
		}

		protected uint GetCraftPrice(NovaCatalogItem cardProduct)
		{
			if (null != cardProduct.VirtualCurrencyPrices) {
				uint craftPrice;
                _GotCraftPrice = cardProduct.VirtualCurrencyPrices.TryGetValue(CurrencyType.NC, out craftPrice);
				return craftPrice;
			}
			return 0;
		}

		protected uint GetDisenchantPrice(NovaCatalogItem cardProduct)
		{
			if (null != cardProduct.Container && null != cardProduct.Container.VirtualCurrencyContents) {
				uint disenchantPrice;
                _GotDisenchantPrice = cardProduct.Container.VirtualCurrencyContents.TryGetValue(CurrencyType.NC, out disenchantPrice);
				return disenchantPrice;
			}
			return 0;
		}

		protected bool CanCraft { get { return NumberToCraft < 0 || (_CardNumOwned < 3); } }// && _Nanites > 0 && _CardCraftPrice <= _Nanites); } }
		protected bool CanScrap { get { return NumberToCraft > 0 || _CardNumOwned > 0; } }
		#endregion

		#region View Listeners
		protected void OnView_CardChanged(CardData card)
        {
            CraftAndReset();
            RefreshDeckEditing(card);
			RefreshCrafting(card);
            UpdateUiHints();
        }

		protected void OnView_DeckAddClicked()
		{
			this.AddCardToDeckSignal.Dispatch(this.View.TwoTouchInstance.CardData, this.PlayerProfile.CurrentDeck, true);
            if (this.PlayerProfile.UiHintData.DeckBuilder == 0)
            {
                this.PlayerProfile.UiHintData.DeckBuilder = 1;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }

        }

		protected void OnView_DeckRemoveClicked()
		{
			this.RemoveCardFromDeckSignal.Dispatch(this.View.TwoTouchInstance.CardData, this.PlayerProfile.CurrentDeck);
            if(this.PlayerProfile.UiHintData.DeckBuilder == 1)
            {
                this.PlayerProfile.UiHintData.DeckBuilder = 2;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }
        }

		protected void OnView_CraftClicked()
        {
            if (this.CanCraft)
            {
                if (this.PlayerProfile.UiHintData.Crafting == 0)
                {
                    this.PlayerProfile.UiHintData.Crafting = 1;
                    this.SaveUiHintDataSignal.Dispatch();
                    UpdateUiHints();
                }
                if (_CardCraftPrice > _Nanites && NumberToCraft >= 0)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.NanoBotsOffer);
                    return;
                }
                NumberToCraft++;
                if (NumberToCraft <= 0)
                {
                    this.CraftItemSignal.Dispatch(new CraftItemData(_CardID, CraftRequestType.UndoScrap, NumberToCraft, _CardDisenchantPrice, _CardCraftPrice));
                    // is an undo
                    if (_CraftingCardsAdded < 0)
					{
						_CraftingCardsAdded++;
						OnView_DeckAddClicked();
					}
                }
                else
                {
                    this.CraftItemSignal.Dispatch(new CraftItemData(_CardID, CraftRequestType.Craft, NumberToCraft, _CardDisenchantPrice, _CardCraftPrice));
					// is a craft
					if (_CardNumOwned + 1 >= NumberOfCardInDeck && NumberOfCardInDeck < 3)
					{
						_CraftingCardsAdded++;
						OnView_DeckAddClicked ();
					}
                }

                /*var card = this.View.TwoTouchInstance.CardData;
				var titleText = "Craft 1 " + card.Name;
				var messageText = "Craft " + card.Name + " for " + _CardCraftPrice + " Nanites? \n You have " + _Nanites + " Nanites, and will be left with " + (_Nanites - _CardCraftPrice) + " Nanites.";
				this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(titleText, messageText, OnCraftDialog_Dismissed, false));*/
                this.View.FullScrim.GetComponent<Button>().interactable = false;
				this.View.FullScrim.gameObject.SetActive(true);
				this.View.ShortScrim.gameObject.SetActive(false);
				this.View.CraftButton.interactable = false;
                this.View.ScrapButton.interactable = false;
                this.View.UndoCraftButton.interactable = false;
                this.View.UndoScrapButton.interactable = false;
                StartCoroutine(AllowButtonActionAgain());
                this.View.OnCraftingComplete();
            }
        }

		protected void OnView_ScrapClicked()
		{
			if (this.CanScrap)
            {
                if (this.PlayerProfile.UiHintData.Scrapping == 0)
                {
                    this.PlayerProfile.UiHintData.Scrapping = 1;
                    this.SaveUiHintDataSignal.Dispatch();
                    UpdateUiHints();
                }
                NumberToCraft--;
                if (NumberToCraft >= 0)
                {
                    this.CraftItemSignal.Dispatch(new CraftItemData(_CardID, CraftRequestType.UndoCraft, NumberToCraft, _CardDisenchantPrice, _CardCraftPrice));
                    // is an undo
					if (_CraftingCardsAdded > 0)
					{
						_CraftingCardsAdded--;
						OnView_DeckRemoveClicked ();
					}
                }
                else
                {
                    this.CraftItemSignal.Dispatch(new CraftItemData(_CardID, CraftRequestType.Scrap, NumberToCraft, _CardDisenchantPrice, _CardCraftPrice));
                    // is a scrap
					if (_CardNumOwned - 1 < NumberOfCardInDeck)
					{
						_CraftingCardsAdded--;
						OnView_DeckRemoveClicked ();
					}
                }
                /*
                var card = this.View.TwoTouchInstance.CardData;
				var titleText = "Disenchant 1 " + card.Name;
				var messageText = "Disenchant " + card.Name + " for " + _CardDisenchantPrice + " Nanites? \n You currently own " + _CardNumOwned + " " + card.Name + ".";
				this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(titleText, messageText, OnDisenchantDialog_Dismissed, false))*/;
                this.View.CraftButton.interactable = false;
                this.View.ScrapButton.interactable = false;
                this.View.UndoCraftButton.interactable = false;
                this.View.UndoScrapButton.interactable = false;
				this.View.FullScrim.GetComponent<Button>().interactable = false;
				this.View.FullScrim.gameObject.SetActive(true);
				this.View.ShortScrim.gameObject.SetActive(false);
                StartCoroutine(AllowButtonActionAgain());
                this.View.OnScrapComplete();
            }
        }


        internal IEnumerator AllowButtonActionAgain()
        {
            this.View.LoadingOverlay.gameObject.SetActive(true);
            yield return null;
            RefreshCrafting(_CraftingCard);
            this.View.LoadingOverlay.gameObject.SetActive(false);
            RefreshDeckEditing(_CraftingCard);
			this.View.FullScrim.GetComponent<Button>().interactable = true;
			this.View.FullScrim.gameObject.SetActive(false);
            this.View.ShortScrim.gameObject.SetActive(true);
        }

        protected void OnView_CloseTwoTouch()
        {
            CraftAndReset();
        }
        #endregion

        protected void CraftAndReset()
        {
            if (this.NumberToCraft != 0 && GameData.CardDictionary.ContainsKey(this._CardID))
            {
                this.CraftItemSignal.Dispatch(new CraftItemData(_CardID, CraftRequestType.Confirm, NumberToCraft, _CardDisenchantPrice, _CardCraftPrice));
                // send a craft/scrap request
            }
            this.NumberToCraft = 0;
			this._CraftingCardsAdded = 0;
        }

        #region Signal Listeners
        protected void OnSignal_CardAdded(CardData card, DeckData deck)
		{
			if (this.View.IsOpen)
				RefreshDeckEditing(card);
		}

		protected void OnSignal_CardRemoved(CardData card, DeckData deck)
		{
			if (this.View.IsOpen)
				RefreshDeckEditing(card);
		}

		private void OnSignal_ProfileUpdated(ProfileSection section)
        {
			if(this.View.IsOpen)
			{
				/*if(section == ProfileSection.Currency)
				{
					this.View.OnScrapComplete();
				}*/

				CardData cardData = null;
				if(GameData.CardDictionary.TryGetValue(this._CardID, out cardData))
				{
					this.RefreshCrafting(cardData);
					this.RefreshCollectinTotal(cardData);
				}

                UpdateUiHints();
            }
        }

        private void OnSignal_CraftResponse(CraftResponse response)
        {
            if (response.Error != ErrorCode.NoError || response.RequestType == CraftRequestType.Cancel || response.RequestType == CraftRequestType.Confirm)
            {
                // 
            }
            if (response.RequestType == CraftRequestType.Craft || response.RequestType == CraftRequestType.UndoScrap)
            { 
            }
            else if (response.RequestType == CraftRequestType.Scrap || response.RequestType == CraftRequestType.UndoCraft)
            {
                
            }
        }
        #endregion
        /*
        #region Async
        protected void OnCraftDialog_Dismissed(bool confirmed)
		{
			if (confirmed && this.CanCraft)
			{
				this.CraftItemSignal.Dispatch(new CraftItemData(1, _CardCraftPrice, _CardID, null));
				this.View.LoadingOverlay.gameObject.SetActive(true);
				this.View.LoadingSpinner.gameObject.SetActive(true);
			}
		}

		protected void OnDisenchantDialog_Dismissed(bool confirmed)
		{
			if (confirmed && this.CanDisenchant)
			{
				this.CraftItemSignal.Dispatch(new CraftItemData(-1, _CardDisenchantPrice, null, _CardID));
				this.View.LoadingOverlay.gameObject.SetActive(true);
				this.View.LoadingSpinner.gameObject.SetActive(true);
			}
		}
		#endregion*/
	}
}
