﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.Economy;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
    public class LeaderboardItemView : PoolableMonoBehaviour
    {

        public override PooledObjectType PooledObjectType { get { return PooledObjectType.LeaderboardItemView; } }
        public Action<LeaderboardItemView> EventClicked;

        // Displays as: #1 - Playername - rank;
        public LeaderboardType type { get; set; }
        public Dictionary<int, IntRange> rankBrackets { get; set; }
        private PlayerLeaderboardEntry _LeaderboardEntry;
        public PlayerLeaderboardEntry LeaderboardEntry
        {
            get { return _LeaderboardEntry; }
            set { _LeaderboardEntry = value; RefreshData(); }
        }

        public TextMeshProUGUI Username;
        public TextMeshProUGUI Position;
        //public GameObject RankObject;
        //public GameObject EventObject;
        public TextMeshProUGUI Rank;
        public TextMeshProUGUI Rating;
        public Image RankBadge;
        public Image Highlight;
        //private bool _IsEvent { set { RankObject.gameObject.SetActive(!value); EventObject.gameObject.SetActive(value); } }
        
        public void RefreshData()
        {
            if (LeaderboardEntry == null)
                return;
            Username.SetText(LeaderboardEntry.DisplayName);
            Position.SetText((LeaderboardEntry.Position +1).ToString());
            //_IsEvent = type == LeaderboardType.Event;

            if (type == LeaderboardType.Ranking)
            {
                int mmr = EngineUtils.FindBracket(LeaderboardEntry.StatValue, rankBrackets);
                int rank = 30 - mmr;
                Rank.SetText(rank > 0 ? rank.ToString() : I2.Loc.ScriptLocalization.Nova);
                Rating.SetText(LeaderboardEntry.StatValue.ToString());
            }
            else if (type == LeaderboardType.Event)
            {
                Rank.SetText("{0}/{1}", LeaderboardEntry.StatValue / 100, 99 - LeaderboardEntry.StatValue % 100); // Assuming data is stored as "1599" = 15 wins, 0 losses.
                RankBadge.gameObject.SetActive(false);
            }
        }

        public void Click() { if (null != this.EventClicked) this.EventClicked.Invoke(this); }

        public override void OnRecycle()
        {
            LeaderboardEntry = null;
            EventClicked = null;
        }
    }
}