﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class ReconnectDialogMediator : Mediator
    {
        [Inject] public ReconnectDialogView View { get; set; }
        [Inject] public ServerConnectedSignal ServerConnectedSignal { get; set; }
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }
        [Inject] public MatchmakeToServerSignal MatchmakeToServerSignal { get; set; }

        public override void OnRegister()
        {
            this.View.DialogDismissed += OnDismissed;
            this.ServerConnectedSignal.AddListener(OnServerConnected);
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
#if UNITY_IOS
            this.View.ExitButton.gameObject.SetActive(false);
#endif
        }

        public override void OnRemove()
        {
            this.View.DialogDismissed -= OnDismissed;
            this.ServerConnectedSignal.RemoveListener(OnServerConnected);
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {

            }
        }

        private void OnServerConnected(bool isConnected, string reason)
        {
            if(isConnected)
            {
                this.CloseAnimatedViewSignal.Dispatch(typeof(ReconnectDialogView));
            }
        }

        private void OnDismissed(ReconnectAction reconnectAction, string url)
        {
            switch(reconnectAction)
            {
                case ReconnectAction.OpenExternalLink:
                    Debug.LogFormat("Opening URL - {0}", url);
                    Application.OpenURL(url);
                    break;
                case ReconnectAction.Reconnect:
                    this.View.ReconnectButtonLabel.text = I2.Loc.ScriptLocalization.Reconnecting;
                    this.View.ReconnectButton.interactable = false;
                    this.View.ExitButton.interactable = false;
                    StartCoroutine(ReconnectCoroutine());
                    break;
                case ReconnectAction.ReconnectToServerOnly:
                    this.MatchmakeToServerSignal.Dispatch();
                    break;
                case ReconnectAction.Exit:
                    Application.Quit();
#if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
#endif
                    break;
                case ReconnectAction.NoAction:
                default:
                    break;
            }
        }
        private IEnumerator ReconnectCoroutine()
        {
            yield return new WaitForSecondsRealtime(3);
            this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect);
        }
    }

    public enum ReconnectAction
    {
        NoAction,
        Reconnect,
        ReconnectToServerOnly,
        OpenExternalLink,
        Exit,
    }
}
