﻿using System;
using NovaBlitz.Economy;
using strange.extensions.mediation.impl;
using TMPro;

namespace NovaBlitz.UI
{
    public class QuestAndStatsView : View
    {
        public RewardWidgetView RewardWidget;
        
        public TextMeshProUGUI ArcaneGrants;
        public TextMeshProUGUI TechGrants;
        public TextMeshProUGUI DivineGrants;
        public TextMeshProUGUI NatureGrants;
        public TextMeshProUGUI ChaosGrants;

        
    }

    public class QuestAndStatsMediator : Mediator
    {
        [Inject] public QuestAndStatsView View { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public PlayerDataLoadedSignal PlayerDataLoadedSignal { get; set; }
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }


        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            SetData(this.PlayerProfile.OnboardingProgress);

            this.PlayerDataLoadedSignal.AddListener(OnData);
            this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.PlayerDataLoadedSignal.RemoveListener(OnData);
            this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
        }

        private void OnData()
        {
            SetData(this.PlayerProfile.OnboardingProgress);
        }

        private void OnProfileUpdated(ProfileSection section)
        {
            if (section == ProfileSection.Statistics || section == ProfileSection.Inventory)
            {
                SetData(this.PlayerProfile.OnboardingProgress);
            }
        }

        public void SetData(Messages.OnboardingProgress progress)
        {
            this.View.RewardWidget.Initialize(progress, false, false);

            this.View.ArcaneGrants.text = string.Format(NBEconomy.GRANT_TEXT,
                Math.Min(NBEconomy.MAX_ASPECT_REWARDS, progress != null ? progress.ArcaneRewardGranted : 0),
                NBEconomy.MAX_ASPECT_REWARDS, I2.Loc.ScriptLocalization.Arcane);
            this.View.TechGrants.text = string.Format(NBEconomy.GRANT_TEXT,
                Math.Min(NBEconomy.MAX_ASPECT_REWARDS, progress != null ? progress.TechRewardGranted : 0),
                NBEconomy.MAX_ASPECT_REWARDS, I2.Loc.ScriptLocalization.Tech);
            this.View.DivineGrants.text = string.Format(NBEconomy.GRANT_TEXT,
                Math.Min(NBEconomy.MAX_ASPECT_REWARDS, progress != null ? progress.DivineRewardGranted : 0),
                NBEconomy.MAX_ASPECT_REWARDS, I2.Loc.ScriptLocalization.Divine);
            this.View.NatureGrants.text = string.Format(NBEconomy.GRANT_TEXT,
                Math.Min(NBEconomy.MAX_ASPECT_REWARDS, progress != null ? progress.NatureRewardGranted : 0),
                NBEconomy.MAX_ASPECT_REWARDS, I2.Loc.ScriptLocalization.Nature);
            this.View.ChaosGrants.text = string.Format(NBEconomy.GRANT_TEXT,
                Math.Min(NBEconomy.MAX_ASPECT_REWARDS, progress != null ? progress.ChaosRewardGranted : 0),
                NBEconomy.MAX_ASPECT_REWARDS, I2.Loc.ScriptLocalization.Chaos);
        }
    }
}
