﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;
using GameFormat = Messages.GameFormat;
using TournamentStatus = Messages.TournamentStatus;
//using EntryMethod = Messages.EntryMethod;
using Messages;
using DG.Tweening;


namespace NovaBlitz.UI.Mediators
{
	public class PlayScreenColumnMediator : Mediator
	{
		[Inject] public PlayScreenColumnView View {get;set;}
        
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public GameData GameData { get; set; }
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public ViewReferences ViewReferences { get; set; }

        [Inject] public PlayerDataLoadedSignal PlayerDataLoadedSignal {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		[Inject] public DeckSelectedSignal DeckSelectedSignal {get;set;}
		[Inject] public DeckDeselectedSignal DeckDeselectedSignal {get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        [Inject] public DeleteDeckSignal DeleteDeckSignal {get;set;}
        [Inject] public CloneDeckSignal CloneDeckSignal {get;set;}

		[Inject] public DropFromEventSignal DropFromEventSignal {get;set;}
		[Inject] public StartGameSessionSignal StartGameSessionSignal {get;set;}
		[Inject] public StartPaidGameSessionSignal StartPaidGameSessionSignal {get;set;}

		[Inject] public ChallengeFriendSignal ChallengeFriendSignal {get;set;}
		[Inject] public AcceptFriendChallengeSignal AcceptFriendChallengeSignal {get;set;}
		[Inject] public FriendChallengeCanceledSignal FriendChallengeCanceledSignal {get;set;}
		[Inject] public FriendChallengeDeclinedSignal FriendChallengeDeclinedSignal {get;set;}

		[Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal { get; set; }
		[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public ServerResponseSignal ServerResponseSignal { get; set; }
        [Inject] public ServerErrorRaisedSignal ServerErrorRaisedSignal { get; set; }
        [Inject] public ShowPurchaseOffersSignal ShowPurchaseOffersSignal { get; set; }

        //[Inject] public ProcessDraftPickEventsSignal ProcessDraftPickEventsSignal { get; set; }
        [Inject] public DraftSessionLoadedSignal DraftSessionLoadedSignal { get; set; }
        [Inject] public UIConfiguration UIConfiguration { get; set; }

        [Inject] public SaveDeckSignal SaveDeckSignal {get;set;}
        
        private const string ENTRY_DRAFT = "entry_draft";
        private const string ENTRY_LEAGUE = "entry_league";

        public override void OnRegister()
		{
            if(this.View.PlayerCard == null)
            {
                this.View.PlayerCard = GameObject.Instantiate(this.UIConfiguration.PlayerCardPrefab);
                this.View.PlayerCard.transform.SetParent(this.View.PlayerCardParent.transform, false);
                this.View.PlayerCard.StatsPanel.gameObject.SetActive(false);
                this.View.PlayerCard.QuestPanel.gameObject.SetActive(false);
                this.View.PlayerCard.ButtonArea.gameObject.SetActive(false);
                this.View.PlayerCard.CardBack.gameObject.SetActive(false);
                this.View.PlayerCard.PlayStatusPanel.SetActive(false);
                this.View.PlayerCard.VerticalStatusPanel.SetActive(true);
                this.View.PlayerCard.isVertical = true;

            }
			this.PlayerDataLoadedSignal.AddListener(OnPlayerDataLoaded);
			this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
			this.DeckSelectedSignal.AddListener(OnDeckSelected);
			this.DeckDeselectedSignal.AddListener(OnDeckDeselected);
			this.ChallengeFriendSignal.AddListener(OnChallengeFriend);
			this.FriendChallengeCanceledSignal.AddListener(OnChallengeCanceled);
			this.FriendChallengeDeclinedSignal.AddListener(OnChallengeDeclined);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);

            this.View.EventPlayScreenOpened += OnPlayScreenOpened;
            this.View.EventFormatChanged += OnViewFormatChanged;
			this.View.EventDeckRenamed += OnDeckRenamed;
            this.View.LauncherWidget.EventPracticeClicked += OnPracticeClicked;
            this.View.LauncherWidget.EventPlayClicked += OnPlayClicked;
			this.View.LauncherWidget.EventRetireClicked += OnRetireClicked;
            this.View.LauncherWidget.EventPurchaseClicked += OnPurchaseClicked;
            this.View.LauncherWidget.EventFreePlayClicked += OnFreePlayClicked;
            this.View.EventDeleteClicked += OnDeckDeleted;
            this.View.EventCloneClicked += OnDeckCloned;
            this.View.PointerClickDeckImage += OnDeckImageClicked;
            this.View.PointerClickDeckNameInput += OnDeckNameInputClicked;

            this.View.EditButtonsCanvasGroup.alpha = 0;
            this.View.EditButtonsCanvasGroup.interactable = false;
            this.View.EditButtonsCanvasGroup.blocksRaycasts = false;

            OnPlayerDataLoaded();
            //if (this.View.EventPlayScreenOpened != null)
            //    this.View.EventPlayScreenOpened.Invoke();
            //UpdateUiHints();
		}

		public override void OnRemove()
		{
			this.PlayerDataLoadedSignal.RemoveListener(OnPlayerDataLoaded);
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
			this.DeckSelectedSignal.RemoveListener(OnDeckSelected);
			this.DeckDeselectedSignal.RemoveListener(OnDeckDeselected);
			this.ChallengeFriendSignal.RemoveListener(OnChallengeFriend);
			this.FriendChallengeCanceledSignal.RemoveListener(OnChallengeCanceled);
			this.FriendChallengeDeclinedSignal.RemoveListener(OnChallengeDeclined);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);

            //this.View.EventPlayScreenOpened -= UpdateUiHints;
            this.View.EventFormatChanged -= OnViewFormatChanged;
			this.View.EventDeckRenamed -= OnDeckRenamed;
            this.View.LauncherWidget.EventPracticeClicked -= OnPracticeClicked;
            this.View.LauncherWidget.EventPlayClicked -= OnPlayClicked;
			this.View.LauncherWidget.EventRetireClicked -= OnRetireClicked;
            this.View.LauncherWidget.EventPurchaseClicked -= OnPurchaseClicked;
            this.View.LauncherWidget.EventFreePlayClicked -= OnFreePlayClicked;
            this.View.EventDeleteClicked -= OnDeckDeleted;
            this.View.EventCloneClicked -= OnDeckCloned;
            this.View.PointerClickDeckImage -= OnDeckImageClicked;
            this.View.PointerClickDeckNameInput -= OnDeckNameInputClicked;
        }

        private void OnDeckImageClicked()
        {
            if (View.Format == GameFormat.Tutorial || this.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests.Any(q => q > -5) || View.IsFormatInProgress)
                return;
            else if (View.DeckNameInput.gameObject.activeSelf == false)
            {
                if (Application.platform != RuntimePlatform.Android)
                    OnDeckNameInputClicked();
                View.EditButtonsCanvasGroup.alpha = 1;
                View.EditButtonsCanvasGroup.interactable = true;
                View.EditButtonsCanvasGroup.blocksRaycasts = true;
            }
            else if (this.CurrentViewDeck != null && View.DeckNameInput.text == this.CurrentViewDeck.Name)
            {
                Debug.Log("Closed edit field. Same name. Do nothing. Bye.");
                Timer.Instance.StopCoroutine(View.DelayHideEditButtons(true));
                Timer.Instance.StartCoroutine(View.DelayHideEditButtons(true));
            }
            else if (string.IsNullOrEmpty(View.DeckNameInput.text))
            {
                View.DeckNameInput.text = View.CurrentViewDeck.Name;
                Debug.Log("Tried to enter a null deck name.");
                Timer.Instance.StopCoroutine(View.DelayHideEditButtons(true));
                Timer.Instance.StartCoroutine(View.DelayHideEditButtons(true));
            }
            else
            {
                View.OnDeckNameEndEdit(View.DeckNameInput.text);
            }
        }

        private void OnDeckNameInputClicked()
        {
            if (View.Format == GameFormat.Tutorial || this.PlayerProfile.OnboardingProgress.RemainingOnboardingQuests.Any(q => q > -5) || View.IsFormatInProgress)
                return;
            View.DeckNameInput.gameObject.SetActive(true);
            View.DeckNameInput.ActivateInputField();
            View.DeckNameInput.Select();
        }

        private void OnDeckCloned()
		{
            if (this.PlayerProfile.DeckList.Count < this.PlayerProfile.NumDeckSlots)
            {
                this.CloneDeckSignal.Dispatch(this.CurrentViewDeck);
                this.CloseAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
            }
            else
            {
                NewMessageDialogParams dlgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.OutOfDeckSlots,
                    string.Format(I2.Loc.ScriptLocalization.Error.OutOfDeckSlotsMessage, this.PlayerProfile.NumDeckSlots, 0, 0));// + " decks, please remove an existing deck before cloning a new one.");
                this.ShowNewMessageDialogSignal.Dispatch(dlgParams);
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
                RefreshChallenge(null);
                this.View.IsFormatInProgress = false;
            }

        }

        private void RefreshFormat()
        {
            if(this.View.QuestView != null)
            {
                Debug.LogFormat("Recycling quest");
                this.View.QuestView.Recycle();
                this.View.QuestView = null;
            }
            Debug.LogFormat("Refreshing format to :{0}", this.format);
            switch (this.format) {
                case GameFormat.Draft:
                    RefreshDraft();
                    break;
                case GameFormat.Constructed:
                    RefreshLeague();
                    break;
                case GameFormat.Tournament:
                case GameFormat.Monthly:
                case GameFormat.Annual:
                    RefreshTournamentEvent();
                    break;
                case GameFormat.Tutorial:
                    RefreshTutorial();
                    break;
                default:
                    RefreshCasualPlay();
                    break;
                }

                this.View.LauncherWidget.Format = format;
                string formatName;
                if(TournamentUtils.TournamentFormatNames.TryGetValue(format, out formatName))
                {
                    View._HeaderDisplay.text = formatName;
                }

                /*if (format == GameFormat.Casual)// || format == GameFormat.Draft)
                    View.LauncherWidget.IsPracticeEnabled = true;
                else*/
                    View.LauncherWidget.IsPracticeEnabled = false;

                RefreshChallenge(format == GameFormat.Challenge ? this.PlayerProfile.SelectedChallengeID : null);
        }

        private void OnDeckDeleted()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
            this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(string.Format(I2.Loc.ScriptLocalization.DeleteDeck, this.CurrentViewDeck.Name, 0, 0),
                I2.Loc.ScriptLocalization.DeleteDeckCantBeUndone, OnConfirmDialogDismissed, false));
		}

		private void OnConfirmDialogDismissed(bool isConfirmed)
        {
            if(isConfirmed)
                this.DeleteDeckSignal.Dispatch(this.CurrentViewDeck);
        }

        private void OnDeckRenamed(string deckName)
        {
            if (this.PlayerProfile.CurrentDeck == null)
                Debug.Log("current deck == null");
            if(deckName != this.PlayerProfile.CurrentDeck.Name)
            {
                Debug.LogFormat("Renaming deck {0}", deckName);
                this.PlayerProfile.CurrentDeck.Name = deckName;
                this.SaveDeckSignal.Dispatch(this.PlayerProfile.CurrentDeck);
            }
            else
            {
                Debug.LogFormat("Deck name is already {0}", deckName);
            }
        }

        protected void GetEntryFees(string itemId)
        {
            //ProductData entryItem;
            TournamentEventData eventData;
            /*if (GameData.ProductCache.TryGetValue(itemId, out entryItem) && entryItem.VirtualPrices != null)
            {
                View.entryFees = entryItem.VirtualPrices;

            }
            else */
            if ((this.View.Format == GameFormat.Tournament && this.View.TournamentEventData != null && GameData.TournamentDataCache.TryGetValue(this.View.TournamentEventData.ID, out eventData))
                || ((View.Format == GameFormat.Draft || View.Format == GameFormat.Constructed) && GameData.TournamentDataCache.TryGetValue(View.Format.ToString().ToLower(), out eventData)))
                View.EntryFees = eventData.Costs;
            else
                View.EntryFees = new Dictionary<CurrencyType, int>();

            View.LauncherWidget.SetButtons(View.EntryFees, PlayerProfile.Wallet.Currencies, this.View.Format);
        }

		protected void RefreshDraft()
		{
			var tStatus = this.PlayerProfile.DraftData.event_status;
			bool isPlaying = tStatus == TournamentStatus.playing;
            bool isDrafting = tStatus == TournamentStatus.drafting;
            this.View.IsExpanded = !isPlaying;
			this.View.IsFormatInProgress = isPlaying || isDrafting;
			this.View.EventInfo = isPlaying || isDrafting ? null : I2.Loc.ScriptLocalization.DraftInfo;
            this.View.EventArt.gameObject.SetActive(false);
            this.View.LauncherWidget.PlayLabel = isDrafting
				? I2.Loc.ScriptLocalization.Resume
                : I2.Loc.ScriptLocalization.Play;
            this.View.LauncherWidget.IsTournamentEntry = !isPlaying && !isDrafting;

            this.View.LeftTournamentInfo.gameObject.SetActive(false);
            this.View.CenterTournamentInfo.gameObject.SetActive(false);

            GetEntryFees(ENTRY_DRAFT);

            //this.View.EntryFee = isPlaying ? 0 : PRICE_ENTRY;
            
			this.View.DeckInfoToggle.IsOn = isPlaying || isDrafting;
			var deck = this.PlayerProfile.DraftDeck;
            if(deck == null)
            {
                deck = new DeckData(GameFormat.Draft);
            }
			if (null != deck) {
				string dname = I2.Loc.ScriptLocalization.Draft_Deck;
				if (deck.Cards.Count < deck.MinDeckSize)
					dname = string.Format("{0} {1}/{2}", dname, deck.Cards.Count, deck.MinDeckSize);
                else if (deck.Cards.Count > deck.MaxDeckSize)
                    dname = string.Format("{0} {1}/{2}", dname, deck.Cards.Count, deck.MaxDeckSize);
                this.View.DeckMini.DeckName = dname;
				this.View.DeckMini.Aspects = deck.Aspects;
				//this.View.EnergyGraph.DeckData = deck;
				//this.View.EnergyGraph.Refresh();
			}
            CurrentViewDeck = null;
            this.DeckSelectedSignal.Dispatch(deck);
            this.RefreshPlayerCard(this.PlayerProfile.DraftData, GameFormat.Draft);
		}

        private void RefreshPlayerCard(TournamentData data, GameFormat format)
        {

            // Set up the data for the PlayerCard.
            AvatarDataContract avContract;
            string avatarArtId = string.Empty;
            string cardBackArtId = string.Empty;
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.AvatarID) && this.GameData.AvatarByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.AvatarID, out avContract))
            {
                avatarArtId = avContract.ArtId;
            }
            CardBackDataContract cbContract;
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.CardBackID) && this.GameData.CardBackByItemIdDictionary.TryGetValue(this.PlayerProfile.MetaProfile.CardBackID, out cbContract))
            {
                cardBackArtId = cbContract.ArtId;
            }
            int qps;
            this.PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.QP, out qps);

            var Data = new PlayerData(this.PlayerProfile.MetaProfile.PlayFabID, this.PlayerProfile.MetaProfile.Name, this.PlayerProfile.UserStatistics, this.GameData.RankBrackets,
                avatarArtId, cardBackArtId, qps,
                data, format, 0, 0, 0, -1, 0, 0, null, null, 0);

            /*if (!this.PlayerProfile.UserStatistics.TryGetValue(NBEconomy.PLAYER_STATS_RATING, out Data.rating))
            {
                StringBuilder ratingBuilder = new StringBuilder();
                ratingBuilder.Append(DateTime.UtcNow.Year).Append('_').Append(DateTime.UtcNow.Month).Append('_').Append(NBEconomy.PLAYER_STATS_RATING);
                if (!this.PlayerProfile.UserStatistics.TryGetValue(ratingBuilder.ToString(), out Data.rating))
                {
                    this.PlayerProfile.UserStatistics.TryGetValue(NBEconomy.PLAYER_STATS_MMR, out Data.rating);
                }
            }*/
            
            this.View.PlayerCard.Initialize(Data);

        }

		protected void RefreshLeague()
		{
			var tStatus = this.PlayerProfile.ConstructedData.event_status;
			bool isPlaying = tStatus == TournamentStatus.playing;
			this.View.IsExpanded = false;
			this.View.IsFormatInProgress = isPlaying;
			this.View.EventInfo = isPlaying ? null : I2.Loc.ScriptLocalization.LeagueInfo;
            this.View.EventArt.gameObject.SetActive(true);
			//this.View.EntryFee = isPlaying ? 0 : PRICE_ENTRY;
			this.View.LauncherWidget.PlayLabel = isPlaying ? I2.Loc.ScriptLocalization.Play : I2.Loc.ScriptLocalization.Enter;

            this.View.LeftTournamentInfo.gameObject.SetActive(false);
            this.View.CenterTournamentInfo.gameObject.SetActive(false);

            GetEntryFees(ENTRY_LEAGUE);
            
            this.View.DeckInfoToggle.IsOn = isPlaying;
            var deck = this.PlayerProfile.LeagueDeck;
            if (null != deck)
            {
                string dname = deck.Name;
                if (deck.Cards.Count < deck.MinDeckSize)
                    dname = string.Format("{0} {1}/{2}", dname, deck.Cards.Count, deck.MinDeckSize);
                else if (deck.Cards.Count > deck.MaxDeckSize)
                    dname = string.Format("{0} {1}/{2}", dname, deck.Cards.Count, deck.MaxDeckSize);
            }
            if (isPlaying && this.PlayerProfile.LeagueDeck != null)
            {
                CurrentViewDeck = null;
                this.DeckSelectedSignal.Dispatch(this.PlayerProfile.LeagueDeck);
            }
            else if (this.PlayerProfile.CurrentDeck != null)
            {
                CurrentViewDeck = null;
                this.DeckSelectedSignal.Dispatch(this.PlayerProfile.CurrentDeck);
            }

            this.RefreshPlayerCard(this.PlayerProfile.ConstructedData, GameFormat.Constructed);
        }

        protected void RefreshTournamentEvent()
        {
            var eStatus = this.PlayerProfile.TournamentData.event_status;
            bool isPlaying = eStatus == TournamentStatus.playing;
            this.View.IsExpanded = false;
            this.View.IsFormatInProgress = isPlaying;
            this.View.EventInfo = isPlaying ? string.Empty : I2.Loc.ScriptLocalization.TournamentInfo;
            this.View.EventArt.gameObject.SetActive(true);
            //this.View.EntryFee = isPlaying ? 0 : PRICE_ENTRY;
            this.View.LauncherWidget.PlayLabel = isPlaying ? I2.Loc.ScriptLocalization.Play : I2.Loc.ScriptLocalization.Enter;

            this.View.LeftTournamentWidget.Init(PlayerProfile.CurrentTournament);
            this.View.CenterTournamentWidget.Init(PlayerProfile.CurrentTournament);
            this.View.LeftTournamentInfo.gameObject.SetActive(true);
            this.View.CenterTournamentInfo.gameObject.SetActive(true);

            GetEntryFees("FIGURE THIS OUT");
            
            this.View.DeckInfoToggle.IsOn = isPlaying;
            var deck = this.PlayerProfile.EventDeck;
            if (null != deck)
            {
                string dname = deck.Name;
                if (deck.Cards.Count < deck.MinDeckSize)
                    dname = string.Format("{0} {1}/{2}", dname, deck.Cards.Count, deck.MinDeckSize);
                else if (deck.Cards.Count > deck.MaxDeckSize)
                    dname = string.Format("{0} {1}/{2}", dname, deck.Cards.Count, deck.MaxDeckSize);
            }
            if (isPlaying && this.PlayerProfile.EventDeck != null)
            {
                CurrentViewDeck = null;
                this.DeckSelectedSignal.Dispatch(this.PlayerProfile.EventDeck);
            }
            else if (this.PlayerProfile.CurrentDeck != null)
            {
                CurrentViewDeck = null;
                this.DeckSelectedSignal.Dispatch(this.PlayerProfile.CurrentDeck);
            }


            this.RefreshPlayerCard(this.PlayerProfile.TournamentData, GameFormat.Tournament);
        }

        protected void RefreshTutorial()
        {
            // set decks based on tutorial number
            int currentTutorial = Math.Min(this.GameData.TutorialDictionary.Count, Math.Max(1, this.PlayerProfile.OnboardingProgress.CurrentTutorial));
            TutorialDataContract tutorialData;
            if(this.GameData.TutorialDictionary.TryGetValue(currentTutorial, out tutorialData))
            {
                this.PlayerProfile.CurrentDeck = tutorialData.TutorialDeck;
                CurrentViewDeck = null;
                this.DeckSelectedSignal.Dispatch(this.PlayerProfile.CurrentDeck);
            }

            /*if (null != this.PlayerProfile.CurrentDeck)
            {
                currentViewDeck = null;
                this.DeckSelectedSignal.Dispatch(this.PlayerProfile.CurrentDeck);
            }*/
            else OnDeckDeselected(null);
            this.View.IsExpanded = false;
            this.View.IsFormatInProgress = false;
            this.View.EventInfo = null;
            //this.View.EntryFee = 0;
            this.View.LauncherWidget.PlayLabel = I2.Loc.ScriptLocalization.Play;
            this.View.LauncherWidget.IsTournamentEntry = false;
            this.View.DeckInfoToggle.IsOn = true;

            this.View.LeftTournamentInfo.gameObject.SetActive(false);
            this.View.CenterTournamentInfo.gameObject.SetActive(false);
        }
        private DeckData CurrentViewDeck { get { return this.View.CurrentViewDeck; } set { this.View.CurrentViewDeck = value; } }
        protected void RefreshCasualPlay()
        {
            if (null != this.PlayerProfile.CurrentDeck)
            {
                CurrentViewDeck = null;
                this.DeckSelectedSignal.Dispatch(this.PlayerProfile.CurrentDeck);
            }
            else OnDeckDeselected(null);
            this.View.IsExpanded = false;
            this.View.IsFormatInProgress = false;
            this.View.EventInfo = null;
            //this.View.EntryFee = 0;
            this.View.LauncherWidget.PlayLabel = I2.Loc.ScriptLocalization.Play;
            this.View.LauncherWidget.IsTournamentEntry = false;
            this.View.DeckInfoToggle.IsOn = true;

            this.View.LeftTournamentInfo.gameObject.SetActive(false);
            this.View.CenterTournamentInfo.gameObject.SetActive(false);

            if(format == GameFormat.Casual)
                UpdateQuest();
        }

        protected void RefreshChallenge(string playfabID)
		{
			var friend = null == playfabID ? null : this.PlayerProfile.GetFriend(playfabID);
            if (friend != null)
            {
                this.View.isChallengeIncoming = this.PlayerProfile.IsChallengeIncoming(friend.FriendData.PlayFabId);
                this.View.ChallengeOpponent = friend;
            }
        }
        
        private void UpdateQuest()
        {
            if (this.View.QuestView != null)
                this.View.QuestView.Recycle();
            Debug.LogFormat("Updating quest");
            if (this.PlayerProfile.OnboardingProgress.CurrentQuest != null)
            {
                var quest = UIConfiguration.QuestPrefab.Spawn();
                quest.Initialize(this.PlayerProfile.OnboardingProgress.CurrentQuest);
                quest.transform.AttachToParent(this.View.QuestParent.transform, 0);
                quest.transform.localPosition = Vector3.zero;
                quest.transform.localRotation = Quaternion.identity;
                this.View.QuestView = quest;
            }
        }

        #region Signal Listeners
        protected void OnPlayerDataLoaded()
		{
			OnViewFormatChanged(this.View.Format);
		}

        protected void OnLanguageChanged()
        {
            OnViewFormatChanged(this.View.Format);
        }


        protected void OnProfileUpdated(ProfileSection section)
		{
            if (this.View.IsOpen)
            {
                switch (section)
                {
                    case ProfileSection.DeckList:
                        if (null != this.PlayerProfile.CurrentDeck)
                            OnDeckSelected(this.PlayerProfile.CurrentDeck);
                        break;
                    case ProfileSection.DraftDeck:
                        RefreshDraft();
                        break;
                }
            }
            OnViewFormatChanged(View.Format);
        }

		protected void OnDeckSelected(DeckData selectedDeck)
		{
			//this.View.SubHeader = string.Empty;
            if (selectedDeck == null)
            {
                this.View.IsDeckLegal = this.View.Format == GameFormat.Draft ? true : false;
                return;
            }
			
			bool selectedDeckChanged = this.CurrentViewDeck != selectedDeck;
			this.CurrentViewDeck = selectedDeck;
			
            this.View.IsDeckLegal = this.View.Format == GameFormat.Draft ? true : selectedDeck.IsLegalDeck;
			this.View.DeckMini.DeckName = selectedDeck.Name;
			this.View.DeckNameInput.text = selectedDeck.Name;
			this.View.DeckMini.Aspects = selectedDeck.Aspects;
			//this.View.EnergyGraph.DeckData = selectedDeck;
			//this.View.EnergyGraph.Refresh();
			
			if(selectedDeckChanged)
			{
				this.View.ShineDeckInfo(0.1f);
				this.View.Animator.SetTrigger("SelectDeck");
			}

            Texture cardArtTexture = null;
            if (!string.IsNullOrEmpty(selectedDeck.DeckArt))
            {
                cardArtTexture = AssetBundleManager.Instance.LoadAsset<Texture>(selectedDeck.DeckArt); //Resources.Load(selectedDeck.DeckArt) as Texture;
            }

            if (cardArtTexture == null)
            {
                string cardArtId = null;
                var aspect = selectedDeck.Aspects.FirstOrDefault();
                switch (aspect)
                {
                    case CardAspect.Arcane: cardArtId = "DC0353"; break;
                    case CardAspect.Tech: cardArtId = "DC0351"; break;
                    case CardAspect.Divine: cardArtId = "DC0352"; break;
                    case CardAspect.Nature: cardArtId = "DC0354"; break;
                    case CardAspect.Chaos: cardArtId = "DC0355"; break;
                    default: cardArtId = "DC0348"; break;
                }
                cardArtTexture = AssetBundleManager.Instance.LoadAsset<Texture>(cardArtId);
            }
            this.View.DeckArt.texture = cardArtTexture;
            this.View.EventArt.texture = cardArtTexture;

        }

		protected void OnDeckDeselected(DeckData deselectedDeck)
		{
			this.View.IsDeckLegal = this.View.Format == GameFormat.Draft ? true : false;
			this.View.DeckMini.DeckName = string.Empty;
			this.View.DeckNameInput.text = string.Empty;
			this.View.DeckMini.Aspects = null;
			//this.View.EnergyGraph.DeckData = null;
			//this.View.EnergyGraph.Refresh();
		}

		protected void OnChallengeFriend(string playfabID)
		{
			RefreshChallenge(playfabID);
		}

		protected void OnChallengeCanceled(string playfabID)
		{
			if (!this.View.IsOpen)
				return;
			if (null != this.View.ChallengeOpponent && playfabID == this.View.ChallengeOpponent.FriendData.PlayFabId)
            {
				this.View.ClickBack();
				var msgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Challenge, I2.Loc.ScriptLocalization.IncomingChallengeCanceled);
				this.ShowNewMessageDialogSignal.Dispatch(msgParams);
			}
		}

		protected void OnChallengeDeclined(string playfabID)
		{
			if (!this.View.IsOpen)
				return;
			if (null != this.View.ChallengeOpponent && playfabID == this.View.ChallengeOpponent.FriendData.PlayFabId) {
				var msgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Challenge, I2.Loc.ScriptLocalization.ChallengeDeclined);
				this.ShowNewMessageDialogSignal.Dispatch(msgParams);
			}
		}
		#endregion

		#region View Listeners
        private GameFormat format = GameFormat.Casual;
		protected void OnViewFormatChanged(GameFormat format)
		{
            //this.View.gameObject.SetActive(true);
            var canvas = this.View.DeckImageRoot.GetComponentInChildren<CanvasGroup>();
            if (canvas != null)
            {
                canvas.interactable = format != GameFormat.Tutorial;
            }
			this.format = format;
            //Debug.LogError("OnViewFormatChanged:" + this.format);
			//this.View.SubHeader = format != GameFormat.Draft && null == this.PlayerProfile.CurrentDeck ? "Choose Your Deck" : string.Empty;
		}

        protected IEnumerator WaitToRefreshFormat()
        {
            PlayScreenColumnView playScreenColumnView = (PlayScreenColumnView)this.ViewReferences.Get(typeof(PlayScreenColumnView));
            while(playScreenColumnView.IsOpen == false)
            {
                yield return null;
            }
            RefreshFormat();
        }
        
        protected void OnPracticeClicked()
        {
            var startGameSessionData = new StartGameSessionObject
            {
                GameFormat = GameFormat.Practice,
                PlayFabIdToChallenge = string.Empty
            };
            this.StartGameSessionSignal.Dispatch(startGameSessionData);
        }

        protected void OnFreePlayClicked(GameFormat format)
        {
            int price = 1;
            int freeBalance;
            if (format == GameFormat.Draft && PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.FD, out freeBalance) && freeBalance >= price)
            {
                EnterEvent(format, CurrencyType.FD, price);
            }
            else if (format == GameFormat.Constructed && PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.FC, out freeBalance) && freeBalance >= price)
            {
                EnterEvent(format, CurrencyType.FC, price);
            }
            else
            {
                var message = new Messages.ErrorResponse() { Error = Messages.ErrorCode.UnknownPaymentMethodProvided };
                ServerErrorRaisedSignal.Dispatch(message.Error, message);
            }
        }

        protected void OnPurchaseClicked(GameFormat format, CurrencyType currency, int price)
        {
            int balance;
            if(!PlayerProfile.Wallet.Currencies.TryGetValue(currency, out balance) || balance < price)
            {
                if (currency == CurrencyType.CR)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.CreditsOffer);
                }
                else if (currency == CurrencyType.NG)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.GemsOffer);
                }
                else if (currency == CurrencyType.NC)
                {
                    this.ShowPurchaseOffersSignal.Dispatch(OfferType.GemsOffer);
                }
                /*if (currency == CurrencyType.NG || currency == CurrencyType.CR || currency == CurrencyType.NC)
                {
                    this.View.OpenStore();
                    // go to store

                    var StoreView = ViewReferences.Get<StoreView>();
                    if (StoreView != null)
                    {
                        if (currency == CurrencyType.NG)
                            StoreView.ScrollRect.verticalNormalizedPosition = 0.0f;
                        else
                            StoreView.ScrollRect.verticalNormalizedPosition = 0.5f;
                    }
                }*/
                else
                {
                    var message = new Messages.ErrorResponse() { Error = Messages.ErrorCode.InsufficientFunds };
                    ServerErrorRaisedSignal.Dispatch(message.Error,  message);
                    // pop up warning: insufficient funds
                }
            }
            else
            {
                if (price == 0)
                    MetricsTrackingService.SavedException = string.Format("Price is 0. Find out why.");
                // Make the payment & enter
                EnterEvent(format, currency, price);
            }
        }

        protected void EnterEvent(GameFormat format, CurrencyType currency, int price)
        {
            var eventParams = new StartPaidGameSessionParams()
            {
                Format = format,
                Currency = currency,
                ExpectedPrice = price,
                DeckToPlay = format == GameFormat.Draft ? null : this.PlayerProfile.CurrentDeck.Clone(),
            };
            if (format == GameFormat.Tournament)
                eventParams.TournamentToPlay = View.CenterTournamentWidget.CurrentEvent.Clone();
            this.StartPaidGameSessionSignal.Dispatch(eventParams);
        }

        protected void OnPlayScreenOpened()
        {
            StartCoroutine(this.WaitToRefreshFormat());
        }

        protected void OnPlayClicked(GameFormat format)
		{
			var startGameSessionData = new StartGameSessionObject{
				GameFormat = format,
				PlayFabIdToChallenge = string.Empty
			};

			switch (format)
            {
                case GameFormat.Tutorial:
                case GameFormat.Casual:
			    case GameFormat.Practice:
				    this.StartGameSessionSignal.Dispatch(startGameSessionData);
				    break;
			    case GameFormat.Challenge:
				    string id = this.View.ChallengeOpponent.FriendData.PlayFabId;
				    bool isAccepting = this.PlayerProfile.IsChallengeIncoming(id);
				    if (isAccepting)
					    this.AcceptFriendChallengeSignal.Dispatch(id);
				    else this.ChallengeFriendSignal.Dispatch(id);
				    break;
			    case GameFormat.Draft:
				    switch (this.PlayerProfile.DraftData.event_status) {
				    case TournamentStatus.drafting:
                                //this.ProcessDraftPickEventsSignal.Dispatch();
                                this.DraftSessionLoadedSignal.Dispatch();
                                //EnterEvent(format, CurrencyType.CR, 0, true);
					    break;
				    case TournamentStatus.playing:
					    this.StartGameSessionSignal.Dispatch(startGameSessionData);
					    break;
				    }
				    break;
			    case GameFormat.Constructed:
                    switch (this.PlayerProfile.ConstructedData.event_status)
                    {
                        case TournamentStatus.playing:
                            this.StartGameSessionSignal.Dispatch(startGameSessionData);
                            break;
                    }
                    break;
                case GameFormat.Tournament:
			    case GameFormat.Monthly:
			    case GameFormat.Annual:
                        // TODO - ensure time is legal to play!!!
                    switch (this.PlayerProfile.ConstructedData.event_status)
                    {
                        case TournamentStatus.playing:
                            this.StartGameSessionSignal.Dispatch(startGameSessionData);
                            break;
                    }
                    break;
                default:
                    break;
			}
		}

		protected void OnRetireClicked(GameFormat format)
		{
			var title = string.Empty;
			var prompt = string.Empty;
			DialogDismissedDelegate onDismiss = isConfirmed => {
				if (isConfirmed) {
					this.DropFromEventSignal.Dispatch(format);
				}
				//this.NovaContext.BlurMainCanvas(false,null);
			};

            prompt = I2.Loc.ScriptLocalization.RetireEventMessage;

            switch (format) {
			case GameFormat.Draft:
				title = I2.Loc.ScriptLocalization.RetireDraft;
				break;
			case GameFormat.Constructed:
                    title = I2.Loc.ScriptLocalization.RetireLeague;
                    break;
                case GameFormat.Tournament:
			case GameFormat.Monthly:
			case GameFormat.Annual:
				title = I2.Loc.ScriptLocalization.RetireTournament;
				break;
			default:
				return;
			}

			this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(title, prompt, onDismiss, false));
		}
		#endregion
	}
}
