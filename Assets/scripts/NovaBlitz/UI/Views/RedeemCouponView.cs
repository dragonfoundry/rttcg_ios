﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;

namespace NovaBlitz.UI
{
    public class RedeemCouponView : AnimatedView
    {
        public Action<bool> DialogDismissed; // bools isConfirmed
        public TextMeshProUGUI MessageLabel;
        public TextMeshProUGUI TitleLabel;
        public Button RedeemButton;
        public TMP_InputField CouponInput;

        private bool IsCouponComplete { get { return CouponInput.text.Length == couponLength && regex.IsMatch(CouponInput.text); } }

        protected override void Awake()
        {
            //this.IsOpen = false;
            base.Awake();
        }

        public void Initialize()
        {
            RedeemButton.interactable = false;
            CouponInput.text = string.Empty;
        }
        private const string regexString = "[0-9a-zA-Z]{4}?-[0-9a-zA-Z]{3}?-[0-9a-zA-Z]{4}?-[0-9a-zA-Z]{3}$";
        private const int couponLength = 17;
        private Regex regex = new Regex(regexString);

        protected override void Update()
        {
            if (this.IsOpen)
            {
                if (IsCouponComplete)
                    this.RedeemButton.interactable = true;
                else
                    this.RedeemButton.interactable = false;
            }
        }

        public void OnEndEditCoupon()
        {
            if ((Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !CouponInput.wasCanceled)) && IsCouponComplete)
            {
                OnClickRedeem();
            }
        }

        public void OnClickRedeem()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(true);
            }
        }

        public void OnClickClose()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(false);
            }
        }
    }

    public class RedeemCouponMediator : Mediator
    {
        [Inject] public RedeemCouponView View { get; set; }
        [Inject] public RedeemCouponSignal RedeemCouponSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.DialogDismissed += OnDismissed;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.DialogDismissed -= OnDismissed;
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
                Timer.Instance.StartCoroutine(DelaySelectEntryField());
            }
        }

        private IEnumerator DelaySelectEntryField()
        {
            yield return new WaitForSecondsRealtime(0.3f);
            this.View.CouponInput.ActivateInputField();
            this.View.CouponInput.Select();
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
            }
        }

        private void OnDismissed(bool isConfirmed)
        {
            if (isConfirmed && !string.IsNullOrEmpty(this.View.CouponInput.text))
            {
                RedeemCouponSignal.Dispatch(this.View.CouponInput.text);
            }
            this.View.RedeemButton.interactable = false;
            this.View.CouponInput.text = string.Empty;
            this.CloseAnimatedViewSignal.Dispatch(typeof(RedeemCouponView));
        }
    }
}
