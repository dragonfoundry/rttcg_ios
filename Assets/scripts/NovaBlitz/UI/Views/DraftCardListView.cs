﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

namespace NovaBlitz.UI
{
	public enum DraftDropTarget
	{
		DeckList,
		UnusedList
	}

	public class DraftCardListView : AnimatedView, IPoolView
	{
		public event Action<CardData> EventCardDropped;
		public event Action EventDeckButtonClicked;
		public Action EventDoneClicked;

		public DropTarget DraftDeckListDropTarget;
		public ScrollRect DeckListScrollRect;
		public TextMeshProUGUI DoneButtonLabel;
		public TextMeshProUGUI DeckListLabel;
        public NovaAudioPlayer DropSound;

		[HideInInspector] public CardListItemView CardListItemPrefab;
		[HideInInspector] public DraggableCardView DraggableCardPrefab;
		[SerializeField] protected GridLayoutGroup _Grid;
		protected RectTransform _GridRX;

		[Inject] public IDragOverlay DragOverlay {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public ShowTwoTouchSignal ShowTwoTouchSignal {get;set;}
		[Inject] public CloseTwoTouchViewSignal CloseTwoTouchViewSignal {get;set;}
		public TwoTouchOverlayView TwoTouchOverlay {get;set;}

		protected List<DraggableCardView> _CardInstances = new List<DraggableCardView>();
		public IEnumerable<DraggableCardView> Cards { get { return _CardInstances.ToArray(); } }

		public Action<CardData,int> CardsDrafted; // First CardData, Second CardData, pickNumber
		public Action<CardData,DraftDropTarget> CardListItemDraggedTo;
		
		private CardData firstCardDrafted;
		
		#region uGUI
		public void ClickDeckButton() { if (null != this.EventDeckButtonClicked) this.EventDeckButtonClicked.Invoke(); }
		public void ClickDone() { if (null != this.EventDoneClicked) this.EventDoneClicked.Invoke(); }
		#endregion

		protected override void Awake()
		{
			_GridRX = (RectTransform)_Grid.transform;
			this.DraftDeckListDropTarget.EventDragItemHovered += OnDropTarget_Hover;
			this.DraftDeckListDropTarget.EventDragItemDropped += OnDropTarget_Drop;
			base.Awake();
		}

		protected override void Start()
		{
			base.Start();
			var cardListItemRX = this.CardListItemPrefab.GetComponent<RectTransform>();
			_Grid.cellSize = cardListItemRX.sizeDelta;
		}
		public void Recycle()
		{
			if (null != _GridRX) {
				foreach (var dc in _CardInstances.Where(i => null != i)) {
					dc.DespawnCardDisplay();
					dc.Recycle ();
					//GameObject.Destroy(dc.gameObject);
					//ObjectPool.Recycle<DraggableCardView>(dc);
				}
			}
			_CardInstances.Clear();
		}
		
		public void UpdateHeaders(int deckListCount, int deckSize)
		{
			this.DeckListLabel.text = String.Format("{2} {0}/{1}", deckListCount, deckSize, I2.Loc.ScriptLocalization.Draft_Deck);
		}
		
		public void Initialize(DeckData deckData, ScrollRect scrollRect, DropTarget draggedFrom, int maxItemsVisible)
		{
			this.Recycle();
			List<CardListItem> cardListItems = CardListTransform.GetList(deckData);
			this.firstCardDrafted = null;
			
			Debug.LogWarningFormat("Initializing DraftCardList with {0} cards", cardListItems.Count);

			for(int i=0;i<cardListItems.Count; i++)
			{
				CardListItem cardListItem = cardListItems[i];
				CardListItemView cardListItemView = this.AddCardItemToList(cardListItem, i, deckData.MaxOfEachCard);
                cardListItemView.Overlay.DOFade(0.0f, 0.0f);
                //TweenAlpha.Begin(cardListItemView.Overlay.gameObject, 0f, 0f);
			}
		}
		
		public CardListItemView AddCardItemToList(CardListItem cardListItem, int index, int maxOfEachCard)
		{
			if(this.TwoTouchOverlay == null)
			{
				this.TwoTouchOverlay = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			}

			var cardData = GameData.CardDictionary[cardListItem.CardID];
			DraggableCardView draggable = UITools.PoolSpawnRect(this.DraggableCardPrefab, Vector3.zero, _GridRX);
			_CardInstances.Add(draggable);
			draggable.RectTransform.localPosition = Vector3.zero;
			draggable.DragOverlay = this.DragOverlay;
			draggable.ShowTwoTouchCallback = OnShowTwoTouch;
			draggable.DismissTwoTouchCallback = OnDismissTwoTouch;
			draggable.IsLocked = true;
			draggable.IsEditable = false;
            draggable.DragItem.Constraint = DragConstraint.Horizontal;

			CardListItemView itemView = draggable.InitCardDisplay(this.CardListItemPrefab, cardData);
			if (index < _Grid.transform.childCount) { draggable.RectTransform.SetSiblingIndex(index); }
			itemView.Initialize(cardListItem, cardData, maxOfEachCard);

			return itemView;
		}

		private void OnShowTwoTouch(TwoTouchParams twoTouchParams)
		{
			this.ShowTwoTouchSignal.Dispatch(twoTouchParams);
		}

		private void OnDismissTwoTouch()
		{
			this.CloseTwoTouchViewSignal.Dispatch();
		}

		protected void OnDropTarget_Hover(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard && null != draggableCard.DraggedCard) 
			{
				draggableCard.DraggedCard.SwitchDisplay(this.CardListItemPrefab);
                BaseCardView cardListItem;
                if(draggableCard.DraggedCard.ChildViews.TryGetValue(typeof(CardListItemView), out cardListItem) && cardListItem != null && cardListItem is CardListItemView)
                {
                    var cardListItemView = cardListItem as CardListItemView;
                    cardListItemView.CountLabel.text = "1";
                    cardListItemView.CountLabel.gameObject.SetActive(true);
                    cardListItemView.CountLabel.color = cardListItemView.DefaultTextColor;
                }
                var draftPackBrowser = GameObject.FindObjectOfType<DraftPackBrowserView>();
                if(draftPackBrowser != null)
                {
                    draftPackBrowser.HideKeywords();
                }
                /*if(draggableCard.CardDisplay is CardView)
				{
					CardView cardView = (CardView)draggableCard.CardDisplay;
					cardView.CardCanvasGroup.DOFade(0f, 0.1f);
				}*/
            }
		}

		protected void OnDropTarget_Drop(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard) {
				var cardView = draggableCard.CardDisplay as CardView;
				if (null != cardView) {
					// Is the draftPick Number greater than zero?
					if (cardView.DraftPickNumber != 0)
					{
						this.firstCardDrafted = cardView.CardData;
						this.CardsDrafted(this.firstCardDrafted, cardView.DraftPickNumber);
						this.firstCardDrafted = null;	
						cardView.DraftPickNumber = 0;
					}
					cardView.SetQuantities(1,0,-1);
                    cardView.CardCountFrame.gameObject.SetActive(false);
                    cardView.NewCardTag.gameObject.SetActive(false);
                    NotifyCardDropped(cardView);
                    DropSound.Play();
				}
			}
		}

		protected void NotifyCardDropped(CardView cardView) { if (null != this.EventCardDropped) this.EventCardDropped.Invoke(cardView.CardData); }
    }
}