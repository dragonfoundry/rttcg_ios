﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TMPro;

namespace NovaBlitz.UI
{
    public class NewMessageDialogView : AnimatedView
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public NovaIAPService NovaIAPService { get; set; }

		public Action DialogDismissed; // bools isConfirmed
        public Action InitializeWaitForPurchase;
		public TextMeshProUGUI MessageLabel;
		public TextMeshProUGUI TitleLabel;

        public TextMeshProUGUI ButtonLabel;
        public NovaButton OkButton;
        public NovaButton CloseButton;
        public Button Scrim;
        public Image LoadingSpinner;
        public RectTransform DialogPanel;
        public RectTransform InnerArea;

        public PoolableMonoBehaviour SubjectItem { get; set; }
        public bool IsWaitingForPurchase { get; set; }
        private bool _IsButtonsActive;
        public bool IsButtonsActive { get { return _IsButtonsActive; } set { _IsButtonsActive = value; OkButton.gameObject.SetActive(value); CloseButton.gameObject.SetActive(value); Scrim.interactable = value; } }

        public void LateUpdate()
        {
            if (this.LoadingSpinner.gameObject.activeSelf)
            {
                this.LoadingSpinner.transform.Rotate(new Vector3(0, 0, -Time.deltaTime * 135f));
                if (!IsWaitingForPurchase)
                    this.LoadingSpinner.gameObject.SetActive(false);
            }
        }

        public void Initialize(NewMessageDialogParams Params)
		{
            StopWaitCoroutine();
            DialogPanel.sizeDelta = new Vector2(700, 500);
            if (Params.IsWaitingForPurchase)
            {
                InitializeAsWaitingForPurchase();
            }
            else
            {
                InitializeAsMessageDialog(Params);
            }
        }

        private void InitializeAsMessageDialog(NewMessageDialogParams Params)
        {
            this.LoadingSpinner.gameObject.SetActive(false);
            this.TitleLabel.text = Params.Title;
            this.MessageLabel.text = Params.Message;
            this.ButtonLabel.text = I2.Loc.ScriptLocalization.OK;
            IsButtonsActive = true;
            IsWaitingForPurchase = false;

            // if the PArams.StoreData isn't null, we've got a purchase confirm
            if (Params.StoreData != null)
            {
                if (SubjectItem != null)
                    SubjectItem.Recycle();
                var storeItem = ObjectPool.SpawnPrefab<StoreItemView>(PooledObjectType.StoreItem);
                storeItem.StoreData = Params.StoreData;
                if (this.PlayerProfile != null && this.NovaIAPService != null)
                    storeItem.InitializeOfferItem(this.PlayerProfile, NovaIAPService.ProductList);
                else
                    Debug.LogErrorFormat("Message Dialog: PlayerProfile is {0} and NovaIAPService is {1}", PlayerProfile == null ? "null" : "ok", NovaIAPService == null ? "null" : "ok");
                storeItem.ActiveButton.gameObject.SetActive(false);
                storeItem.InactiveButton.gameObject.SetActive(false);
                storeItem.transform.AttachToParent(InnerArea, InnerArea.childCount);
                storeItem.transform.localScale = Vector3.one;
                storeItem.transform.localPosition = Vector3.zero;
                SubjectItem = storeItem;
                // dialog size 700
                DialogPanel.sizeDelta = new Vector2(900, 700);
            }
        }
		
        private void InitializeAsWaitingForPurchase()
        {
            this.LoadingSpinner.gameObject.SetActive(true);
            IsWaitingForPurchase = true;

            _WaitForStoreResponseCoroutine = WaitForStoreResponseCoroutine ();
			Timer.Instance.StartCoroutine(_WaitForStoreResponseCoroutine);
        }
        public void StopWaitCoroutine() { if (_WaitForStoreResponseCoroutine != null) Timer.Instance.StopCoroutine(_WaitForStoreResponseCoroutine); }
		public IEnumerator _WaitForStoreResponseCoroutine { get; set; }
        private IEnumerator WaitForStoreResponseCoroutine()
        {
            this.IsButtonsActive = false;
            this.TitleLabel.text = I2.Loc.ScriptLocalization.Waiting_for_Server;
            this.MessageLabel.text = I2.Loc.ScriptLocalization.Purchase_Submitted;
            // would be good to add a spinner here too
            yield return new WaitForSecondsRealtime(3.0f);
            this.IsButtonsActive = true;
            this.ButtonLabel.text = I2.Loc.ScriptLocalization.Cancel;
			_WaitForStoreResponseCoroutine = null;
        }

        public void OnClickOk()
		{
			if(this.DialogDismissed != null)
			{
				this.DialogDismissed.Invoke();
			}
		}

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (SubjectItem != null)
                SubjectItem.Recycle();
        }
    }
}
