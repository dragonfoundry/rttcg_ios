using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System;
using LinqTools;
using System.Collections.Generic;
using DG.Tweening;
using NovaBlitz.UI;

namespace NovaBlitz.UI
{
	public class CardListView : AnimatedView, IPoolView
	{
		public event Action<CardData> EventCardDropped;

		public TextMeshProUGUI DeckName;
		public TextMeshProUGUI CardStatus;
		public DropTarget DropTarget;
		public DeckData DeckData {get;private set;}
		public ScrollRect ScrollRect;
		public Scrollbar Scrollbar;

		[HideInInspector] public CardListItemView CardListItemPrefab;
		[HideInInspector] public DraggableCardView DraggableCardPrefab;
		[HideInInspector] public CardListItemProxyView CardListItemProxyPrefab;
		[SerializeField] protected GridLayoutGroup _Grid;
		protected RectTransform _GridRX;

		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public IDragOverlay DragOverlay {get;set;}
		[Inject] public ShowTwoTouchSignal ShowTwoTouchSignal {get;set;}
		[Inject] public CloseTwoTouchViewSignal CloseTwoTouchViewSignal {get;set;}
		[Inject] public CardListItemProxyService CardListItemProxyService {get;set;}
		public TwoTouchOverlayView TwoTouchOverlay {get;set;}

		protected List<DraggableCardView> _CardInstances = new List<DraggableCardView>();
		public IEnumerable<DraggableCardView> Cards { get { return _CardInstances.ToArray(); } }

        public CardAspectsIndicator AspectsIndicator;

        public Action<DeckData> DoneDeckBuilding;
		public event Action EventDeckButtonClicked;
        
		public override bool IsOpen {
			get { return base.IsOpen; }
			set { base.IsOpen = value;
				if (value) {
					if (null != this.DeckData)
                        Initialize(this.DeckData, null, null);
				}
			}
		}

		#region Mono
		protected override void Awake()
		{
			_GridRX = (RectTransform)_Grid.transform;
			this.DropTarget.EventDragItemHovered += OnDropTarget_Hover;
			this.DropTarget.EventDragItemDropped += OnDropTarget_Drop;
			base.Awake();
		}

		protected override void Start()
		{
			base.Start();
			var cardListItemRX = this.CardListItemPrefab.GetComponent<RectTransform>();
			_Grid.cellSize = cardListItemRX.sizeDelta;
		}
		#endregion

		public void Recycle()
		{
			if (null != _GridRX) {
				foreach (var dc in _CardInstances.Where(i => null != i)) {
					//dc.DespawnCardDisplay();
					if(dc.CardDisplay is CardListItemProxyView)
					{
						CardListItemProxyView proxy = (CardListItemProxyView)dc.CardDisplay;
						CardListItemProxyService.CancelProxyImageRequest(proxy.CardListItem);
					}
					dc.Recycle ();
					
					//GameObject.Destroy(dc.gameObject);
					//ObjectPool.Recycle<DraggableCardView>(dc);
				}
			}
			_CardInstances.Clear();
		}

		public void InitializeDeckButton()
		{
			this.DeckName.text = this.DeckData.Name;
            SetCardStatus();
			
            this.DeckData.QueryAspects();
            this.DeckData.SetDeckArt();
            this.AspectsIndicator.ShowIcons(this.DeckData.Aspects);

            //CardBrowserView cardBrowserView = FindObjectOfType<CardBrowserView>();
        }

        public void SetCardStatus()
        {
            if (!this.DeckData.IsLegalDeck)
            {
                this.CardStatus.color = new Color(242 / 256f, 89 / 256f, 53 / 256f);
                this.CardStatus.text = string.Format("{0}/{1} {2}", this.DeckData.Cards.Count, this.DeckData.Cards.Count > this.DeckData.MaxDeckSize ? this.DeckData.MaxDeckSize : this.DeckData.MinDeckSize, I2.Loc.ScriptLocalization.NotLegal);
            }
            else
            {
                this.CardStatus.color = new Color(158 / 256f, 251 / 256f, 255 / 256f);
                this.CardStatus.text = string.Format("{0}/{1}", this.DeckData.Cards.Count, this.DeckData.MinDeckSize);
            }
        }

		public void Initialize(DeckData deckData, Dictionary<int, int> ownedCards, HashSet<int> legalCards)
        {
            if (deckData == null)
                return;
			// PB - Repeated the cell sizing here, as this doesn't initialize correctly on returning from a game
			var cardListItemRX = this.CardListItemPrefab.GetComponent<RectTransform>();
			_Grid.cellSize = cardListItemRX.sizeDelta;

            if (ownedCards != null && legalCards != null)
            {
                deckData.TrimToInventory(ownedCards, legalCards);
            }
            this.DeckData = deckData;
			this.InitializeDeckButton();
			this.Recycle();
			
			List<CardListItem> cardListItems = CardListTransform.GetList(deckData);

			for(int i=0;i<cardListItems.Count; i++)
			{
				CardListItem cardListItem = cardListItems[i];
				this.AddCardItemProxyToList(cardListItem, i, deckData.MaxOfEachCard);
            }
		}

		public void ClickDeckButton()
		{
			if (null != this.EventDeckButtonClicked)
				this.EventDeckButtonClicked.Invoke();
		}

		public void OnDoneButtonClicked()
		{
			if(this.DoneDeckBuilding != null)
				this.DoneDeckBuilding.Invoke(DeckData);
		}

		public CardListItemView AddCardItemToList(CardListItem cardListItem, int index, int maxOfEachCard)
		{
			if(this.TwoTouchOverlay == null)
			{
				this.TwoTouchOverlay = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			}

			var cardData = GameData.CardDictionary[cardListItem.CardID];
			DraggableCardView draggable = UITools.PoolSpawnRect(this.DraggableCardPrefab, Vector3.zero, _GridRX);
			_CardInstances.Add(draggable);
			//draggable.RectTransform.localPosition = Vector3.zero;
			draggable.DragOverlay = this.DragOverlay;
			draggable.ShowTwoTouchCallback = OnShowTwoTouch;
			draggable.DismissTwoTouchCallback = OnDismissTwoTouch;
			draggable.IsEditable = true;
            draggable.DragItem.Constraint = DragConstraint.Horizontal;

            CardListItemView itemView = draggable.InitCardDisplay(this.CardListItemPrefab, cardData);
			if (index < _CardInstances.Count) { draggable.RectTransform.SetSiblingIndex(index); }
			itemView.Initialize(cardListItem, cardData, maxOfEachCard);

			return itemView;
		}

		public CardListItemProxyView AddCardItemProxyToList(CardListItem cardListItem, int index, int maxOfEachCard)
		{
			if(this.TwoTouchOverlay == null)
			{
				this.TwoTouchOverlay = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			}

			var cardData = GameData.CardDictionary[cardListItem.CardID];
			DraggableCardView draggable = UITools.PoolSpawnRect(this.DraggableCardPrefab, Vector3.zero, _GridRX);
			_CardInstances.Add(draggable);
			//draggable.RectTransform.localPosition = Vector3.zero;
			draggable.DragOverlay = this.DragOverlay;
			draggable.ShowTwoTouchCallback = OnShowTwoTouch;
			draggable.DismissTwoTouchCallback = OnDismissTwoTouch;
			draggable.IsEditable = true;
            draggable.DragItem.Constraint = DragConstraint.Horizontal;

            CardListItemProxyView itemView = draggable.InitCardDisplay(this.CardListItemProxyPrefab, cardData);
			if (index < _CardInstances.Count) { draggable.RectTransform.SetSiblingIndex(index); }
			itemView.Initialize(this,cardListItem, cardData, maxOfEachCard, (p)=>((RectTransform)p.transform).DOAnchorPosX(250f,0.1f).From());
			return itemView;
		}

		private void OnShowTwoTouch(TwoTouchParams twoTouchParams)
		{
			this.ShowTwoTouchSignal.Dispatch(twoTouchParams);
		}

		private void OnDismissTwoTouch()
		{
			this.CloseTwoTouchViewSignal.Dispatch();
		}

		protected DraggableCardView FindDraggableListItem(int cardID)
		{
			return _CardInstances.FirstOrDefault(c => c.CardData.CardID == cardID);
		}

		public CardListItemProxyView GetSelectedListItem()
		{
			var draggable = _CardInstances.Where(c => c.CardDisplay.IsSelected).FirstOrDefault();
			if(draggable != null)
			{
				return draggable.CardDisplay as CardListItemProxyView;
			}
			return null;
		}

		public CardListItemProxyView FindListItem(int cardID)
		{
			var existing = FindDraggableListItem(cardID);
			if (null != existing)
				return existing.CardDisplay as CardListItemProxyView;
			return null;
		}

		public void RemoveListItem(int cardID)
		{
			var existing = FindDraggableListItem(cardID);
			if (null != existing) {
				_CardInstances.Remove(existing);
				existing.DespawnCardDisplay();
				existing.Recycle ();
			}
		}

		protected void OnDropTarget_Hover(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard && null != draggableCard.DraggedCard) 
			{
				draggableCard.DraggedCard.SwitchDisplay(this.CardListItemProxyPrefab);
			}
		}

		protected void OnDropTarget_Drop(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard) {
				var cardView = draggableCard.CardDisplay as CardView;
				if (null != cardView) NotifyCardDropped(cardView);
			}
		}

		protected void NotifyCardDropped(CardView cardView) { if (null != this.EventCardDropped) this.EventCardDropped.Invoke(cardView.CardData); }


        protected override void OnDestroy()
        {
            if (!_IsAppQuitting)
            {
                base.OnDestroy();
                this.Recycle();
            }
        }

        protected bool _IsAppQuitting;
        protected void OnApplicationQuit() { _IsAppQuitting = true; }
    }
}
