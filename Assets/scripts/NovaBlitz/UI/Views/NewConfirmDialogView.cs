﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TMPro;

namespace NovaBlitz.UI
{
    public class NewConfirmDialogView : AnimatedView
    {
		public Action<bool> DialogDismissed; // bools isConfirmed
		public TextMeshProUGUI MessageLabel;
		public TextMeshProUGUI TitleLabel;
        public bool IsWaitForResponse;
        public Button YesButton;
        public Button NoButton;

        public void Initialize(string title, string prompt, bool isWaitForResponse)
        {
            YesButton.interactable = true;
            NoButton.interactable = true;
            this.TitleLabel.text = title;
			this.MessageLabel.text = prompt;
            this.IsWaitForResponse = isWaitForResponse;
		}
		
		public void OnClickYes()
		{
			if(this.DialogDismissed != null)
			{
				this.DialogDismissed.Invoke(true);
			}
		}

		public void OnClickNo()
		{
			if(this.DialogDismissed != null)
			{
				this.DialogDismissed.Invoke(false);
			}
        }

        protected override void Awake()
        {
            //this.IsOpen = false;
            base.Awake();
        }
    }
}
