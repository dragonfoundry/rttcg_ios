﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;

namespace NovaBlitz.UI
{
    public class MobileTermsView : AnimatedView
    {
        public Action CreditsClicked;
        public Action HelpClicked;
        public Action DialogDismissed;
        public Action EulaClicked; // bools isConfirmed
        public Action PrivacyClicked; // bools isConfirmed
        public Action TermsClicked; // bools isConfirmed
        /*public TextMeshProUGUI EULALabel;
        public TextMeshProUGUI PrivacyPolicyLabel;
        public TextMeshProUGUI TermsLabel;
        public TextMeshProUGUI HeaderLabel;
        public TextMeshProUGUI FooterLabel;*/

        public void Initialize()
        {
        }

        public void OnClickEULA()
        {
            if (this.EulaClicked != null)
            {
                this.EulaClicked.Invoke();
            }
        }

        public void OnClickPrivacy()
        {
            if (this.PrivacyClicked != null)
            {
                this.PrivacyClicked.Invoke();
            }
        }

        public void OnClickTerms()
        {
            if (this.TermsClicked != null)
            {
                this.TermsClicked.Invoke();
            }
        }

        public void OnClickHelp()
        {
            if (this.HelpClicked != null)
            {
                this.HelpClicked.Invoke();
            }
        }

        public void OnClickCredits()
        {
            if (this.CreditsClicked != null)
            {
                this.CreditsClicked.Invoke();
            }
        }

        public void OnClickDismiss()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke();
            }
        }
    }

    public class MobileTermsMediator : Mediator
    {
        [Inject] public MobileTermsView View { get; set; }
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public INovaContext NovaContext { get; set; }
        [Inject] public GameData GameData { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.EulaClicked += OnEulaClicked;
            this.View.PrivacyClicked += OnPrivacyClicked;
            this.View.TermsClicked += OnTermsClicked;
            this.View.DialogDismissed += OnDialogDismissed;
            this.View.HelpClicked += OnHelpClicked;
            this.View.CreditsClicked += OnCreditsClicked;
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.EulaClicked -= OnEulaClicked;
            this.View.PrivacyClicked -= OnPrivacyClicked;
            this.View.TermsClicked -= OnTermsClicked;
            this.View.DialogDismissed -= OnDialogDismissed;
            this.View.HelpClicked -= OnHelpClicked;
            this.View.CreditsClicked -= OnCreditsClicked;
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {

            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {
                //this.NovaContext.BlurMainCanvas(false, this.View);
            }
        }

        private void OnDialogDismissed()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(MobileTermsView));
        }

        private void OnEulaClicked()
        {
            /*string url = Terms.urlForEULA();
            if (url == null)
            {
                url = this.GameData.url_EULA;
            }*/
			string url = this.GameData.url_EULA;
            Application.OpenURL(url);
            this.CloseAnimatedViewSignal.Dispatch(typeof(MobileTermsView));
        }

        private void OnPrivacyClicked()
        {
            /*string url = Terms.urlForPrivacyPolicy();
            if(url == null)
            {
                url = this.GameData.url_Privacy;
            }*/
			string url = this.GameData.url_Privacy;
            Application.OpenURL(url);
            this.CloseAnimatedViewSignal.Dispatch(typeof(MobileTermsView));
        }

        private void OnTermsClicked()
        {
			/*string url = Terms.urlForTOS();
            if (url == null)
            {
                url = this.GameData.url_Terms;
			}*/
			string url = this.GameData.url_Terms;
            Application.OpenURL(url);
            this.CloseAnimatedViewSignal.Dispatch(typeof(MobileTermsView));
        }

        private void OnHelpClicked()
        {
            Application.OpenURL(this.GameData.url_Help);
            this.CloseAnimatedViewSignal.Dispatch(typeof(MobileTermsView));
        }

        private void OnCreditsClicked()
        {
            Application.OpenURL(this.GameData.url_Credits);
            this.CloseAnimatedViewSignal.Dispatch(typeof(MobileTermsView));
        }
    }
}
