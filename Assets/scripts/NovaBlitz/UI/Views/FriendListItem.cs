﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using ExitGames.Client.Photon.Chat;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
    public class FriendListItem : PoolableMonoBehaviour
    {
        public TextMeshProUGUI REQUEST;
        public TextMeshProUGUI PlayerNameLabel;
		public GameObject OfflineRoot;
		public GameObject ChatAlertIcon;
		public GameObject Glow;
        public CardArtImage FriendAvatarArt;
		public Button ItemButton;
		public NovaAudioPlayer ClickSound;
        public Image SteamIcon;
		private string playfabId;
		private string playerName = string.Empty;
		[SerializeField]

		protected GameObject _ChallengeAlert;

		public string PlayfabId { get { return this.playfabId; } }

		public string PlayerName 
		{
			get { return this.playerName; }
			private set {
				this.PlayerNameLabel.text = value;
				//this.OfflinePlayerNameLabel.text = value;
				this.playerName = value;
			}
		}
		
		public bool IsOnline
		{
			get { return !this.OfflineRoot.activeSelf; }
			set {
				//this.OnlineRoot.SetActive(value);
				this.OfflineRoot.SetActive(!value);	
			}
		}
		
		public bool HasUnreadMessages
		{
			get { return this.ChatAlertIcon.activeSelf; }
			set { this.ChatAlertIcon.SetActive(value); }
		}

        private bool _isChallenging;
		public bool IsChallenging { get { return _isChallenging; }  set { _isChallenging = value; _ChallengeAlert.SetActive(value); } }
        
        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.FriendListItem; } }

        public void Initialize(FriendListDockView friendList, NovaBlitzFriend friend, GameData gameData)
        {
            this.Glow.SetActive(false);
            this.PlayerName = friend.FriendData.DisplayName;
            this.IsOnline = friend.ChatUserStatus == ChatUserStatus.Online && !friend.IsBlocked && !friend.FriendData.IsPending && !friend.FriendData.HasBlockedYou; // don't show online status for blocked, or for 
            this.playfabId = friend.FriendData.PlayFabId;
            this.SteamIcon.gameObject.SetActive(friend.FriendData.IsSteamFriend);
            //this.HasUnreadMessages = false;
            //this.IsChallenging = false;

            // Set the art based on the avatar id
            this.FriendAvatarArt.UpdateCardArt(string.IsNullOrEmpty(friend.AvatarArtId) ? gameData.DefaultAvatarArtId : friend.AvatarArtId, CardAspect.NoAspect);



            REQUEST.gameObject.SetActive(friend.FriendData.IsRequest || friend.FriendData.IsPending || friend.IsBlocked);
            REQUEST.text = friend.IsBlocked ? I2.Loc.ScriptLocalization.Blocked : friend.FriendData.IsPending ? I2.Loc.ScriptLocalization.Friend_Requested : I2.Loc.ScriptLocalization.Request;

            this.ItemButton.onClick.RemoveAllListeners();
			this.ItemButton.onClick.AddListener( () => {
				//this.ClickSound.Play();
				if (this.Glow.activeSelf == false)
                {
                    //Debug.LogFormat("ACTIVATE: Clicked Friend: {0}", this.playerName);
                    if (friend.IsBlocked)
                        friendList.DoFriendRequestConfim(this.playfabId, this.PlayerName, FriendRequestConfirmType.Blocked);
                    else if(friend.FriendData.IsRequest)
                        friendList.DoFriendRequestConfim(this.playfabId, this.PlayerName, FriendRequestConfirmType.Request);
                    else if(friend.FriendData.IsPending)
                        friendList.DoFriendRequestConfim(this.playfabId, this.PlayerName, FriendRequestConfirmType.Pending);
                    else
                        friendList.DoFriendChatLog(this.playfabId, this.PlayerName, (friendList.SelectedFriend != null && friendList.SelectedFriend == this.PlayfabId));
                    friendList.SelectedFriend = this.PlayfabId;
                }
				else
                {
                    //Debug.LogFormat("Deactivate: Clicked Friend: {0}", this.playerName);
                    friendList.SelectedFriend = null;
					friendList.HideFriendChatLog();
                    friendList.HideConfirmPanel();
					this.Glow.SetActive(false);
				}
			});
		}

        public override void OnRecycle()
        {
            FriendAvatarArt.ResetArt();
        }
    }
}