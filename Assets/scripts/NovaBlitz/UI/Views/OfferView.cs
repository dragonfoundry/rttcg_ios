﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.mediation.impl;
using Messages;
using NovaBlitz.Economy;
using TMPro;

namespace NovaBlitz.UI
{
    public class OfferView : AnimatedView
    {
        public Action<OfferType> Initialize;
        public Action Close;

        public TextMeshProUGUI Title;
        public MultiStoreItem Row1;
        public GridLayoutGroup OfferHolder;
        public OfferType OfferType { get; set; }
        public List<OfferType> OldOfferTypes = new List<OfferType>();

        public bool HasPurchasedSomething { get; set; }
        
        public void InitializeView(OfferType offerType)
        {
            if (OfferType != OfferType.NoType && offerType != OfferType.NoType && offerType != OfferType)
            { 
                 OldOfferTypes.Add(OfferType);
            }
            else
                OldOfferTypes.Clear();
            OfferType = offerType;

            if (Initialize != null)
                Initialize.Invoke(offerType);
            else
                Timer.Instance.StartCoroutine(InitializeCoroutine(offerType));
        }

        private IEnumerator InitializeCoroutine(OfferType offerType)
        {
            yield return null;
            if (Initialize != null)
                Initialize.Invoke(offerType);
        }
        
        public void OnScrimClicked() { if (this.Close != null) Close.Invoke(); }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            this.Row1.Purge();
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class OfferMediator : Mediator
	{
		[Inject] public OfferView View {get;set;}

		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public NovaIAPService NovaIAPService { get; set; }
        
		[Inject] public MoneyPurchaseSignal MoneyPurchaseSignal {get;set;}
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
        [Inject] public BeginVirtualPurchaseSignal BeginVirtualPurchaseSignal { get; set; }

        //public StoreData CurrentStoreData { get; set;}
        public Dictionary<string, StoreItemView> StoreItems = new Dictionary<string, StoreItemView>();
        
        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
		{
            this.View.Initialize += PopulateOfferView;
            this.View.Close += OnCloseClicked;
            this.ProfileUpdatedSignal.AddListener(OnSignal_ProfileUpdated);
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
        {
            this.View.Initialize -= PopulateOfferView;
            this.View.Close -= OnCloseClicked;
            this.ProfileUpdatedSignal.RemoveListener(OnSignal_ProfileUpdated);
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                //Timer.Instance.StartCoroutine(this.View.SetSize());
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
			{
				this.View.HasPurchasedSomething = false;
				//this.CurrentStoreData = null;
                ClearOfferView();
            }
        }

        public void OnCloseClicked()
        {
            if (this.View.OldOfferTypes.Count > 0)
            {
                var type = this.View.OldOfferTypes.Last();
                this.View.OldOfferTypes.RemoveAt(this.View.OldOfferTypes.Count - 1);
                this.View.OfferType = type;
                PopulateOfferView(type);
            }
            else
            {
                this.View.OldOfferTypes.Clear();
                this.View.OfferType = OfferType.NoType;
                this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
            }
        }

        /*
        protected void UpdateUiHints()
        {
            if (this.PlayerProfile.UiHintData.Store <= 1 && this.View.annuity_credit.isActiveAndEnabled)
            {
                UiHint hint = UiHint.Store;
                int hintNumber = 1;
                bool showCloseButton = true;
                this.ShowUiHintSignal.Dispatch(new ShowUiHintData(this.View.annuity_credit.BuyButton, hint, hintNumber, NovaContext.MainCanvas, showCloseButton));
            }
        }*/

        public void PopulateOfferView(OfferType offerType)
        {
            List<StoreData> offers = null;
            switch(offerType)
            {
                case OfferType.SpecialOffer:
                    View.Title.text = I2.Loc.ScriptLocalization.Special_Offer;
                    offers = PlayerProfile.GetOffers(3, true);
                    break;
                case OfferType.CreditsOffer:
                    View.Title.text = I2.Loc.ScriptLocalization.Not_Enough_Credits;
                    offers = PlayerProfile.GetCurrencyOffers(3, CurrencyType.CR);
                    break;
                case OfferType.GemsOffer:
                    View.Title.text = I2.Loc.ScriptLocalization.Not_Enougn_Gems;
                    offers = PlayerProfile.GetCurrencyOffers(3, CurrencyType.NG);
                    break;
                case OfferType.NanoBotsOffer:
                    View.Title.text = I2.Loc.ScriptLocalization.Not_Enough_NanoBots;
                    offers = PlayerProfile.GetCurrencyOffers(3, CurrencyType.NC);
                    break;
                default:
                    break;
            }


            this.View.IsCanvasInteractive = false;
            
            var r1 = this.View.Row1.Populate(offers).ToList();

            /*
            this.View.StoreItem = ObjectPool.SpawnPrefab<StoreItemView>(PooledObjectType.StoreItem);
            var view = this.View.StoreItem;
            view.transform.AttachToParent(this.View.OfferHolder.transform, 0);
            view.transform.localPosition = Vector3.zero;
            view.StoreData = offers.FirstOrDefault();*/
            
            // Sort the store
            foreach (var view in r1)
            {
                view.ProductScaler.transform.localScale = Vector3.one * 1.3f;
                view.InitializeOfferItem(this.PlayerProfile, NovaIAPService.ProductList);
                //view.EventClicked = OnView_ItemClicked;
            }
            
            this.View.IsCanvasInteractive = true;
        }

        #region Signal Listeners
        private IEnumerator _WaitToPopulateCoroutine;
        private IEnumerator WaitToPopuateCoroutine(OfferType offerType)
        {
            yield return null;// new WaitForSecondsRealtime(0.1f);
            PopulateOfferView(offerType);
            _WaitToPopulateCoroutine = null;
        }

        protected void OnSignal_ProfileUpdated(ProfileSection section)
        {
            if (this.View.IsOpen && _WaitToPopulateCoroutine == null)
            {
                _WaitToPopulateCoroutine = WaitToPopuateCoroutine(this.View.OfferType);
                Timer.Instance.StartCoroutine(_WaitToPopulateCoroutine);
            }
        }
        #endregion
        protected void ClearOfferView()
        {
            View.Row1.Purge();
        }

        protected void OnDestroy()
        {
            ClearOfferView();
        }
	}
}
