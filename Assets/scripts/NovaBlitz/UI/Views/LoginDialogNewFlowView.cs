﻿using UnityEngine;
using System.Text;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using I2.Loc;
using DG.Tweening;
using System;
using Crosstales.BadWord;
using System.Text.RegularExpressions;
#if UNITY_STEAM
using Steamworks;
#endif

namespace NovaBlitz.UI
{
    public class LoginDialogSettings
    {
        public bool? IsAgeGatePassed;
        public bool IsAddUsernameAndPassword;
        public bool IsCancelable;
        public string SteamName;
    }

    public class LoginDialogNewFlowView : AnimatedView
    {
        //[Inject] public ChillingoService ChillingoService { get; set; }
        public bool IsExistingUser;
        public System.Action<string> EmailEntered;
        public System.Action<string> EmailConfirmed;
        public System.Action<UserCredentials> LoginRequested;
        public System.Action<UserCredentials> RegisterUserRequested;
        public System.Action<string> PasswordResetRequested;
        public System.Action LanguageButtonClicked;
        public Action BackClicked;
        public Action EulaClicked;
        public Action PrivacyClicked;
        public Action TermsClicked;
        public Action CloseClicked;
        public TMP_InputField EmailTextBox;
        public TMP_InputField PasswordTextBox;
        public TMP_InputField NewPasswordTextBox;
        public TMP_InputField UsernameTextBox;
        public TextMeshProUGUI MaskedPassword;
        public TextMeshProUGUI MaskedNewPassword;
        public Button SmallBackButton;
        public NovaButton CloseButton;
        public Button ScrimButton;
		public Toggle MailingListToggle;
        
        // Confirm Email step buttons
        public Button ConfirmButton;
        public Button BackButton;
        public TextMeshProUGUI EnterEmailTitle;
        public TextMeshProUGUI EnterEmailPlaceholder;
        public TextMeshProUGUI EnterEmailExplanation;
        public TextMeshProUGUI ConfirmEmailTitle;
        public TextMeshProUGUI ConfirmEmailLabel;
        public TextMeshProUGUI ConfirmEmailExplanation;
        
        // Error Popup properties
        public CanvasGroup ErrorPopup;
        public TextMeshProUGUI ErrorPopupLabel;
        public TextMeshProUGUI EnterEmailLabel;
		public TextMeshProUGUI Under13LoginText;
        
        // More Buttons
		public Button DoneButton;
		public Button RegisterButton;
        public Button LoginButton;
		public Button PasswordReset;

        //private bool _isAgeGatePassed { get; set; }
        //private bool _isAddUsernameAndPassword { get; set; }
        public LoginDialogSettings Settings = new LoginDialogSettings();

        //public bool isAgeGatePassed { get; set; }
        public string Email {
            get { return this.EmailTextBox.text; }
            set { this.EmailTextBox.text = value; }
		}
		public string Username { 
			get { return this.UsernameTextBox.text; }
			set { this.UsernameTextBox.text = value; }
		}

        protected override void Update()
        {
            base.Update();
            if(Input.GetKeyUp(KeyCode.Return))
            {
                if (this.ErrorPopup.interactable == true)
                {
                    OnErrorPopupOk();
                }
                else if (this.ConfirmButton.interactable && this.Animator.GetInteger("TabIndex") == 1)
                {
                    OnConfirmEmailButtonPress();
				}
            }
            else if (Input.GetKeyUp(KeyCode.Escape))
            {
                this.BackClicked();
            }
        }

        /// <summary>
        /// helper property that retrieves the password input from the correct password text box
        /// based on the current user flow
        /// </summary>
        private string Password
        {
            get
            {
                if(this.Animator.GetBool("IsExistingUser"))
                {
                    return this.PasswordTextBox.text;
                }
                else
                {
                    return this.NewPasswordTextBox.text;
                }
			}
			set
            {
                this.PasswordTextBox.text = value;
                this.NewPasswordTextBox.text = value;
                OnPasswordModified();
            }
        }

        public void SetText(LoginDialogSettings settings)
        {
            Settings = settings;

            if(Settings.IsAddUsernameAndPassword)
            {
                EnterEmailTitle.text = ScriptLocalization.Protect_Your_Account;
                EnterEmailPlaceholder.text = ScriptLocalization.Email_Address;
                if (Settings.IsAgeGatePassed == true)
                {
                    this.EnterEmailExplanation.text = ScriptLocalization.Add_Email_Explanation;
                }
                else
                {
                    this.EnterEmailExplanation.text = ScriptLocalization.Add_Email_COPPA;
                }
            }
            else
            {
                EnterEmailTitle.text = ScriptLocalization.Welcome_To_Nova_Blitz;
                EnterEmailPlaceholder.text = ScriptLocalization.Enter_Your_NovaBlitz_Email;
                if (Settings.IsAgeGatePassed == true)
                {
                    this.EnterEmailExplanation.text = ScriptLocalization.New_Account_Explanation;
                }
                else
                {
                    this.EnterEmailExplanation.text = ScriptLocalization.New_Account_COPPA;
                }
            }
        }
        
        /// <summary>
        /// Utility method to reset the dialog to its default state
        /// </summary>
        public void Initialize(LoginDialogSettings settings)
        {
            Debug.Log("Initializing");
            this.CloseButton.gameObject.SetActive(settings.IsCancelable);
            this.ScrimButton.interactable = settings.IsCancelable;
            this.BackButton.interactable = true;
            this.ConfirmButton.interactable = false;
            this.LoginButton.interactable = true;
            this.DoneButton.interactable = true;
            this.RegisterButton.interactable = true;

            //OnBackButtonPress();
            
            this.Password = string.Empty;
			this.Email = string.Empty;
#if UNITY_STEAM
            this.Username = string.IsNullOrEmpty(settings.SteamName) ? string.Empty : settings.SteamName;
#else
            this.Username = string.Empty;
#endif

            this.SetText(settings);
            this.Animator.SetInteger("TabIndex", 0);
            this.Animator.SetBool("IsExistingUser", false);
            
            this.ConfirmEmailTitle.text = I2.Loc.ScriptLocalization.Confirm_Email;
        }

        public void ActivateUsernameField()
        {
            this.UsernameTextBox.ActivateInputField();
            this.UsernameTextBox.Select();
        }

        public void ActivateEmailField()
        {
            this.EmailTextBox.ActivateInputField();
            this.EmailTextBox.Select();
        }

        public void ActivatePasswordField()
        {
            this.PasswordTextBox.ActivateInputField();
            this.PasswordTextBox.Select();
        }

        public void ActivateNewPasswordField()
        {
            this.NewPasswordTextBox.ActivateInputField();
            this.NewPasswordTextBox.Select();
        }



        /// <summary>
        /// Tells the dialog to run the returning / known user wizard flow
        /// </summary>
        public void SetEmailAsKnownAccount()
        {
            this.Animator.SetBool("IsExistingUser", true);
        }

        /// <summary>
        /// The first step in the login flow is the user entering their email
        /// and hitting connect, this is that buttons onClick handler.
        /// </summary>
        public void OnConnectButtonPress()
        {
            Debug.LogError("Connect button press");
            // Default to the new user flow
            this.Animator.SetBool("IsExistingUser", false);
            
            //Do some very basic email validation on whatever the users typed in
            if(string.IsNullOrEmpty(this.EmailTextBox.text) || 
				!(this.EmailTextBox.text.Contains("@") && this.EmailTextBox.text.Contains("."))) // deliberately using the entered text here
            {
                this.ShowErrorMessage(ScriptLocalization.Error.InvalidEmail);
            }
            else
            {
                // If the email looks good go ahead and do the tasks associated
                // with moving the teh email confirm step
                this.Animator.SetInteger("TabIndex",1);
				this.ConfirmEmailLabel.text = String.Format("'{0}'",this.EmailTextBox.text); // deliberately using the entered text here

                this.MailingListToggle.gameObject.SetActive(false);
                this.MailingListToggle.isOn = false;
                this.Under13LoginText.gameObject.SetActive(true);
                this.PasswordReset.gameObject.SetActive(false);
                this.ConfirmButton.interactable = false;

                if(this.EmailEntered != null)
                {
                    this.EmailEntered.Invoke(this.Email);
                }
            }
        }

        /// <summary>
        /// Users whose email address we recognize are prompted to login. This method
        /// is that steps button click handler.
        /// </summary>
        public void OnLoginButtonPress()
        {
            Debug.LogError("Login button press");
            // Validate the password field and display any error messages
            if (string.IsNullOrEmpty(this.PasswordTextBox.text) || this.PasswordTextBox.text.Length < 6 || this.PasswordTextBox.text.Length > 30)
			{
                this.ShowErrorMessage(ScriptLocalization.Error.InvalidPassword);
            }
            else
            {
                // If everything looks ok, go ahead with the login
                if(this.LoginRequested != null)
                {
                    this.LoginButton.interactable = false;
                    UserCredentials creds = new UserCredentials(this.Email);
                    creds.Password = this.PasswordTextBox.text;
                    this.LoginRequested.Invoke(creds);
                }
            }
        }
        
        /// <summary>
        /// After entering an email, the login flow shows a confirmation. While
        /// this confirmation is up, the mediator is checking the email to see if we recognize
        /// it with a playfab call. 
        /// </summary>
        public void OnConfirmEmailButtonPress()
        {
            if(this.EmailConfirmed != null)
            {
                this.EmailConfirmed.Invoke(this.Email);
            }
        }
        
        /// <summary>
        /// On the email confirmation step, if the user doesn't like what they input
        /// they can press the back button and return to the email entry step. This method
        /// is the onClick handler for that button.
        /// </summary>
        public void OnBackButtonPress()
        {
            this.Initialize(this.Settings);
            //this.Animator.SetBool("IsExistingUser", false);
            //this.Animator.SetInteger("TabIndex",0);
            //ResetView();
            if (this.BackClicked != null)
            {
                this.BackClicked.Invoke();
            }
        }

        private void ResetView()
        {
            this.Password = String.Empty;
            this.BackButton.interactable = true;
            this.ConfirmButton.interactable = false;
            this.LoginButton.interactable = true;
            this.DoneButton.interactable = true;
            this.RegisterButton.interactable = true;
            this.ConfirmEmailTitle.text = I2.Loc.ScriptLocalization.Confirm_Email;
        }
        
        /// <summary>
        /// When a user confirms an Email address we don't know, we prompt them to entering
        /// a password for their new account. This method is the handler for the "Register"
        /// button on that step.
        /// </summary>
        public void OnRegisterButtonPress()
        {
            Debug.LogError("Register button press");
            // Double check the new password entered by the user on this step
            if (string.IsNullOrEmpty(this.Password) || this.Password.Length < 6 || this.Password.Length > 30)
			{
                this.ShowErrorMessage(ScriptLocalization.Error.InvalidPassword);
            }
            else
            {
				if (Settings.IsAgeGatePassed == true) {
                    Debug.LogError("Moving to username");
                    Timer.Instance.StartCoroutine(MoveToUsername());
                } else {
					this.RegisterButton.interactable = false;
					RegisterUser ();
				}
            }
        }

        private IEnumerator MoveToUsername()
        {
            yield return null;
            this.Animator.SetInteger("TabIndex", 3);
        }
        
        /// <summary>
        /// In the new user flow, after entering a password we ask them for a nickname /username.
        /// Clicking the done button on this step completes account creation and ends the flow. 
        /// This method is the OnClick handler for the Done button on this step.
        /// </summary>
        public void OnDoneButtonPress()
        {
            Debug.LogError("Done button press");
            // Do some basic validation on the username / nickname provided by the user
            if (string.IsNullOrEmpty (this.Username)
			            || this.Username.Length < 3
			            || this.Username.Length > 20)
			{
				this.ShowErrorMessage (ScriptLocalization.Error.InvalidUsername);
			} 
			else if (BWFManager.Contains (this.Username)) 
			{
				this.Username = string.Empty;
				this.ShowErrorMessage (ScriptLocalization.Error.BadWord);
			}
            else
			{
				string sanitized = Regex.Replace (this.Username, @"[^0-9a-zA-Z]", string.Empty);
                //sanitized = Regex.Replace(sanitized, "_", string.Empty);
                if (string.Compare(this.Username, sanitized) != 0)
				{
					this.Username = sanitized;
					this.ShowErrorMessage (ScriptLocalization.Error.InvalidCharacter);
				}
                else if(this.RegisterUserRequested != null)
                {
                    this.DoneButton.interactable = false;
					RegisterUser ();
                }
            }
        }
        
		public void RegisterUser()
		{
			UserCredentials creds = new UserCredentials(Settings.IsAgeGatePassed == true ? this.Username : null, this.Password, this.Email);
			Debug.LogWarningFormat("Creds - user: {0} password: REDACTED len{1} email: {2}", creds.Username, creds.Password.Length, creds.Email);
			this.RegisterUserRequested.Invoke(creds);
		}

        /// <summary>
        /// For returning users, the password entry step also has a button to resolve forgotten passwords.
        /// </summary>
        public void OnForgotPasswordPress()
        {
			if(this.PasswordResetRequested != null && Settings.IsAgeGatePassed == true)
            {
                this.PasswordResetRequested.Invoke(this.Email);
            }
        }
        
        /// <summary>
        /// Checks for editing ending by pressing enter on Desktop platforms
        /// </summary>
        public void OnEmailEndEdit(string email)
		{
			Debug.Log ("Email End Edit");
            if(Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !EmailTextBox.wasCanceled))
            {
                this.OnConnectButtonPress();
            }
        }
        
        /// <summary>
        /// Checks for editing ending by pressing enter on Desktop platforms
        /// </summary>
        public void OnPasswordEndEdit(string password)
        {
			Debug.Log ("Password End Edit");
            if(Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !PasswordTextBox.wasCanceled))
            {
                if(this.Animator.GetBool("IsExistingUser"))
                {
                    this.OnLoginButtonPress();
                }
                else
                {
                    this.OnRegisterButtonPress();
                }
            }
        }
        /// <summary>
        /// Checks for editing ending by pressing enter on Desktop platforms
        /// </summary>
        public void OnNewPasswordEndEdit(string password)
		{
			Debug.Log ("New Password End Edit");
            if (Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !NewPasswordTextBox.wasCanceled))
            {
                if (this.Animator.GetBool("IsExistingUser"))
                {
                    this.OnLoginButtonPress();
                }
                else
                {
                    this.OnRegisterButtonPress();
                }
            }
        }

        public void OnPasswordModified()
        {
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < PasswordTextBox.text.Length; i++)
            {
                builder.Append('*');
            }
            MaskedPassword.text = builder.ToString();
        }

        public void OnNewPasswordModified()
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < NewPasswordTextBox.text.Length; i++)
            {
                builder.Append('*');
            }
            MaskedNewPassword.text = builder.ToString();
        }

        /// <summary>
        /// Checks for editing ending by pressing enter on Desktop platforms
        /// </summary>
        public void OnUsernameEndEdit(string username)
		{
			Debug.Log ("Username End Edit");
			if((Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !UsernameTextBox.wasCanceled)) && Username.Length > 0)
            {
                this.OnDoneButtonPress();
            }
        }
        
        /// <summary>
        /// Helper method to display an error poup wiht the specified message
        /// </summary>
        public void ShowErrorMessage(string message)
        {
            this.ErrorPopupLabel.text = message;
            this.ErrorPopup.DOFade(1.0f,0.5f).OnComplete(() =>
            {
                this.ErrorPopup.blocksRaycasts = true;
                this.ErrorPopup.interactable = true;
            });
        }
        
        /// <summary>
        /// When validating input fields sometimes the user has made an error. When 
        /// this happens we pop up the ErrorMessage frame. This method is the 
        /// OnClick handler for that popup.
        /// </summary>
        public void OnErrorPopupOk()
        {
            this.ErrorPopup.blocksRaycasts = false;
            this.ErrorPopup.interactable = false;
            this.ErrorPopup.DOFade(0.0f, 0.5f);
            int animatorValue = Animator.GetInteger("TabIndex");
            switch(animatorValue)
            {
                case 0:
                    ActivateEmailField();
                    break;
                case 1:
                    // no input field here
                    break;
                case 2:
                    if (this.Animator.GetBool("IsExistingUser"))
                    {
                        ActivatePasswordField();
                    }
                    else
                    {
                        ActivateNewPasswordField();
                    }
                    break;
                case 3:
                    ActivateUsernameField();
                    break;
            }
        }

        /// <summary>
        /// Allows the player to change langauge from the login screen
        /// </summary>
        public void OnLanguageButonClick()
        {
            if (this.LanguageButtonClicked != null)
            {
                this.LanguageButtonClicked.Invoke();
            }
        }


        public void OnClickEULA()
        {
            if (this.EulaClicked != null)
            {
                this.EulaClicked.Invoke();
            }
        }

        public void OnClickPrivacy()
        {
            if (this.PrivacyClicked != null)
            {
                this.PrivacyClicked.Invoke();
            }
        }

        public void OnClickTerms()
        {
            if (this.TermsClicked != null)
            {
                this.TermsClicked.Invoke();
            }
        }

        public void OnClickClose()
        {
            if (this.CloseClicked != null)
            {
                this.CloseClicked.Invoke();
            }
        }

    }
}
