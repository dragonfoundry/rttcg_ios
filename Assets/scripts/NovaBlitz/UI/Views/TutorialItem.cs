﻿using UnityEngine;
using UnityEngine.UI;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class TutorialItem : MonoBehaviour
    {
        public TutorialDataContract Data;
        public Button playButton;
        public RawImage Artwork;
        public Image Darken;
        public Image Complete;
        public Image TitleBlinder;
        public TextMeshProUGUI Title;
    }
}