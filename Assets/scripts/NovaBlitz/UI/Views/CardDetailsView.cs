﻿using UnityEngine;
using System.Collections;


namespace NovaBlitz.UI
{
    public class CardDetailsView : AnimatedView
    {
		public CardView CardDetails;
		
		public System.Action CloseDetails;

        public void Initialize(RectTransform parent, CardData cardData)
		{
			this.CardDetails.CardData = cardData;
			this.CardDetails.CardAbilities.Initialize(cardData.NewHotnessKeywords);
			this.transform.SetParent(parent);
		}
		
		public void OnScrimClicked()
		{
			if(this.CloseDetails != null)
			{
				this.CloseDetails.Invoke();	
			}
		}
    }
}