﻿using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;

namespace NovaBlitz.UI
{
    public class ForgotPasswordView : AnimatedView
    {
        public Action<bool> DialogDismissed; // bools isConfirmed
        public TextMeshProUGUI MessageLabel;
        public TextMeshProUGUI TitleLabel;
        public Button SendButton;
        public TMP_InputField EmailInput;


        private bool IsEmailComplete { get { return this.EmailInput.text.Contains("@") && this.EmailInput.text.Contains("."); } }
        public string Email { get { return this.EmailInput.text.Replace("\t", string.Empty).Trim(); } }

        protected override void Awake()
        {
            //this.IsOpen = false;
            base.Awake();
        }

        protected void LateUpdate()
        {
            if (this.IsOpen)
            {
                this.SendButton.interactable = IsEmailComplete;
            }
        }

        public void Initialize()
        {
            SendButton.interactable = false;
            EmailInput.text = string.Empty;
            EmailInput.ActivateInputField();
            EmailInput.Select();
        }
        
        public void OnEndEditEmail()
        {
            if ((Input.GetKey(KeyCode.Return) || (TouchScreenKeyboard.isSupported && !EmailInput.wasCanceled)) && IsEmailComplete)
            {
                OnClickSend();
            }
        }

        public void OnClickSend()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(true);
            }
        }

        public void OnClickNo()
        {
            if (this.DialogDismissed != null)
            {
                this.DialogDismissed.Invoke(false);
            }
        }
    }
}
