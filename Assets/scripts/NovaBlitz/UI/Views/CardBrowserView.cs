using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using LinqTools;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using NovaBlitz.Economy;
using NovaBlitz.Game;
using DG.Tweening;

namespace NovaBlitz.UI
{
	[System.Serializable]
	public class CardCollectionFilterItem
	{
		public CardCollectionType CollectionType;
		public Toggle FilterToggle;
	}

	[System.Serializable]
	public class AspectFilterItem
	{
		public CardAspect Aspect;
		public Toggle Toggle;
		public GameObject SmallIcon;
	}

	[System.Serializable]
	public class DropDownButtonItem
	{
		public Button DropDownButton;
		public Transform DropDownRoot;
	}

	public class CardBrowserView : AnimatedView, IPoolView
	{
		[Inject] public GameData GameData{get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ShowTwoTouchSignal ShowTwoTouchSignal {get;set;}
		[Inject] public CloseTwoTouchViewSignal CloseTwoTouchViewSignal {get;set;}
		
		//public Button ClearAllButton;
		public Toggle[] CostFilterToggles;
		public AspectFilterItem[] AspectFilterToggles;
		public CardCollectionFilterItem[] CardCollectionToggles;
		public DropDownButtonItem[] DropDownButtons;
		public NovaButton CostClearButton;
		public NovaButton AspectClearButton;
		public TextMeshProUGUI CollectionTypeButtonLabel;
		public TextMeshProUGUI AspectButtonLabel;
		public TextMeshProUGUI CostButtonLabel;
		public TextMeshProUGUI CostButtonFilterLabel;
		public CanvasGroup CurrentAspectsRoot;
		public GameObject DropDownScrim;
		public NovaAudioPlayer ItemSelectSound;

		public Color DefaultTextColor;
		public Color SelectedTextColor;
		public event Action<CardData> EventCardListItemDropped;
		public Action<CardFilterParams> EventFilterChangeApplied;
		//public RectTransform FilterBox;
		public RectTransform ScrollArea;
		public GridLayoutGroup Grid;
		public DropTarget DropTarget;
		public ScrollRect ScrollView;
		public CardFilterBarWidget FilterBarWidget;
		//public CardFilterWidget FilterPanelWidget;
		public int CardColumns_4x3 = 4;
		public int CardColumns_16x9 = 5;
		public int CardRows = 4;
		public float CardRatio = 1.6f;
		protected int numOnScreenCardRows;
		protected int numOffscreenCardRows;

		public int CardColumns
		{
			get 
			{ 
				if(this.gridRows == null || this.gridRows.Count == 0)
				{
					return this.Is16x9AspectRatio ? this.CardColumns_16x9 : this.CardColumns_4x3; 
				}
				else
				{
					return this.gridRows[0].Grid.transform.childCount;
				}
			}
		}
		
		private bool Is16x9AspectRatio
		{
			get 
			{
				return Camera.main.aspect >= 1.5f;
				
				//16:9
				// if (Camera.main.aspect >= 1.7) 
				
				//3:2
				//else if (Camera.main.aspect >= 1.5)
				
				//4:3
				//else
			}
		}

		[HideInInspector] public CardView CardPrefab;
		[HideInInspector] public DraggableCardView DraggableCardPrefab;

		[Inject] public IDragOverlay DragOverlay {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		
		public TwoTouchOverlayView TwoTouchOverlay {get;set;}

		protected List<CardData> cardDataList;
		protected List<GridRow> gridRows = new List<GridRow>();
		protected GridRow bottomMostGridRow = null;
		protected GridRow topMostGridRow = null;

	
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// INTERNAL CLASS GRID ROW
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		protected class GridRow
		{
			public float YOffset
			{	
				get { return -this.gridRect.anchoredPosition.y; }
				set {
					Vector2 anchor = this.gridRect.anchoredPosition;
					anchor.y = -value;
					this.gridRect.anchoredPosition = anchor;
				}
			}
			public float Height { get { return this.gridRect.rect.height;} }
			public int RowIndex {get;private set;}
			public GridLayoutGroup Grid { get; private set;}
			public List<CardView> CardViews = new List<CardView>();
			public List<DraggableCardView> Draggables = new List<DraggableCardView>();
			private RectTransform gridRect;
			
			public GridRow(GridLayoutGroup grid)
			{
				this.Grid = grid;
				this.gridRect = (RectTransform)grid.transform;
			}
			
			public void BindCardData(List<CardData> listOfCards, int startIndex)
			{
				int cardsPerRow = this.CardViews.Count;
				this.RowIndex = (int)Mathf.Floor(startIndex / cardsPerRow);
                //Debug.LogFormat("startindex:{0} count:{1} rowIndex:{2};{3}", startIndex, listOfCards.Count, this.RowIndex, this.Grid.gameObject);

                for (int i =0;i<cardsPerRow;i++)
				{
					CardView cardView = this.CardViews[i];
					
					if(startIndex +i < listOfCards.Count)
					{
						
						CardData cardData = listOfCards[startIndex +i];
						this.Draggables[i].CardData = cardData;
						this.Draggables[i].name = string.Format("Draggable {0}", cardData.Name);
						
						cardView.DataBind(cardData);
						cardView.IsVisible = true;
						cardView.SetVisible(true);
						cardView.LoadCardArt(cardData.ArtID);
					}
					else
					{
						cardView.IsVisible = false;
						cardView.transform.parent.name = "Draggable Hidden";
					}
				}
				
				
				//Debug.LogError("RowINdex:" + this.RowIndex + " startIndex:" + startIndex + " cpr:" + cardsPerRow);
				this.YOffset = this.RowIndex * this.Grid.cellSize.y;
			}
			
			public void ResetRowPosition()
			{
				this.YOffset = this.RowIndex * this.Grid.cellSize.y;
			}
			
			public void Initialize(int numColumns, DraggableCardView draggablePrefab, CardView cardPrefab, Action<TwoTouchParams> OnShowTwoTouch, Action OnDismissTwoTouch, IDragOverlay dragOverlay )
			{

				if(this.Grid.transform.childCount == 0)
				{
					for(int i=0;i<numColumns;i++)
					{
                        DraggableCardView draggable = UITools.PoolSpawnRect(draggablePrefab, Vector3.zero, (RectTransform)this.Grid.transform);
						draggable.DragOverlay = dragOverlay;
						draggable.ShowTwoTouchCallback = OnShowTwoTouch;
						draggable.DismissTwoTouchCallback = OnDismissTwoTouch;
						draggable.IsEditable = true;
                        draggable.DragItem.Constraint = DragConstraint.Horizontal;
                        CardView cardView = draggable.InitCardDisplay(cardPrefab, null);
						cardView.ScaleToWidth(this.Grid.cellSize.x);
						
						this.Draggables.Add(draggable);
						this.CardViews.Add(cardView);
					}
				}
				else
				{
					for(int i=0;i<this.Draggables.Count;i++)
					{
						if(i<numColumns)
						{
							this.Draggables[i].IsVisible = true;
						}
						else
						{
							this.Draggables[i].IsVisible = false;
						}
					}
				}
			}
			
			public void UpdateCollectionTotals(PlayerProfile playerProfile)
			{
				//Debug.LogError("update collection totals");
				DeckData deck = playerProfile.CurrentDeck;
				
				foreach(CardView cardView in this.CardViews)
				{
					int quantity;
					if(cardView.CardData == null) continue;
					playerProfile.OwnedCards.TryGetValue(cardView.CardData.CardID, out quantity);
                    
					if(deck != null)
					{
						int deckCount = deck.Cards.Where(cd=>cd.CardID==cardView.CardData.CardID).Count();
						cardView.SetQuantities(quantity,deckCount, deck.MaxOfEachCard);
					}
					else
					{
						cardView.SetQuantities(quantity,0);
					}

					// Toggle visiblity of the "New" badge
					cardView.NewCardTag.gameObject.SetActive(playerProfile.NewCards.Contains(cardView.CardData.CardID));
				}
			}
		}

        internal void InitializeFilterBarSize()
        {
#if UNITY_IOS || UNITY_ANDROID

			RectTransform filterBarRect = ((RectTransform)this.FilterBarWidget.transform);
			if(this.Is16x9AspectRatio == true)
			{
				filterBarRect.DOSizeDelta(new Vector2(filterBarRect.sizeDelta.x, 80f),0f);
			}
			else
			{
				filterBarRect.DOSizeDelta(new Vector2(filterBarRect.sizeDelta.x, 66f),0f);
			}
#endif
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // END GridRow class
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

		private void OnShowTwoTouch(TwoTouchParams twoTouchParams)
		{
			this.ShowTwoTouchSignal.Dispatch(twoTouchParams);
		}

		private void OnDismissTwoTouch()
		{
			this.CloseTwoTouchViewSignal.Dispatch();
		}

        public void OnFilterSrimClicked()
		{
			this.HideFilterDropdowns();
			this.DropDownScrim.SetActive(false);
		}

		public void FilterDropdownClicked(Button dropDownButton)
		{
			DropDownButtonItem item = this.DropDownButtons.Where(i=>i.DropDownButton == dropDownButton).FirstOrDefault();
			if(item != null)
			{
				item.DropDownRoot.gameObject.SetActive(!item.DropDownRoot.gameObject.activeSelf);
			}
			
			bool isAnyDropDownActive = this.DropDownButtons.Where(i=>i.DropDownRoot.gameObject.activeSelf).Select(j=>j.DropDownRoot.gameObject.activeSelf).FirstOrDefault();

			if(isAnyDropDownActive && this.DropDownScrim.activeSelf == false)
			{
				this.DropDownScrim.SetActive(isAnyDropDownActive);
				this.DropDownScrim.GetComponent<Image>().DOFade(0,0f);
				this.DropDownScrim.GetComponent<Image>().DOFade(0.5f,0.2f);
			}
			else
			{
				this.DropDownScrim.SetActive(isAnyDropDownActive);
			}
		}
        			
		public override bool IsOpen
		{
			get { return base.IsOpen; }
			set { base.IsOpen = value;
				if (value) {
					this.ScrollView.verticalNormalizedPosition = 1f;
				}
			}
		}

		#region Mono
		protected override void Awake()
		{
			var rectNotifier = this.Grid.GetComponent<RectNotifier>();
			rectNotifier.EventRectChanged += CalcGridScroll;
			this.DropTarget.EventDragItemHovered += OnDropTarget_Hover;
			this.DropTarget.EventDragItemDropped += OnDropTarget_Drop;
			this.ScrollView.onValueChanged.AddListener(OnScrollView_Scrolled);
			this.FilterBarWidget.EventFilterClicked += OnFiltersClicked;
			base.Awake();

		#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
			this.ScrollView.scrollSensitivity = 80;
		#else
			this.ScrollView.scrollSensitivity = 20;
		#endif
		}

        #endregion

		bool isSearchSeleted = false;
		float lasScrollTime = 0;
		protected override void Update()
		{
            base.Update();
			if(this.isSearchSeleted == false && this.FilterBarWidget.SearchField.isFocused)
			{
				this.HideFilterDropdowns();
				this.DropDownScrim.SetActive(false);
			}
		
			if(Time.unscaledTime - this.lasScrollTime > 0.5f && DOTween.IsTweening(this.ScrollView.verticalScrollbar) == false)
			{
				this.ScrollView.verticalScrollbar.targetGraphic.DOFade(0, 0.3f);
			}
			

			this.isSearchSeleted = this.FilterBarWidget.SearchField.isFocused;

		}

		public void OnClearAllClicked()
		{
			//this.FilterPanelWidget.ClearAll();
			//this.FilterPanelWidget.ClickApply();
			//this.ClearAllButton.interactable = false;
		}
		
        private void OnFiltersClicked()
        {
			//this.ClearAllButton.interactable = false;
			Sequence seq = DOTween.Sequence();
			seq.Append(((RectTransform)this.FilterBarWidget.transform).DOAnchorPosY(70,0.15f).SetEase(Ease.OutQuint));
			seq.AppendCallback(()=>{this.InitFilterPanel();});
			//seq.Join(((RectTransform)this.ScrollArea.parent.parent).DOAnchorPosY(-this.FilterBox.rect.height, 0.4f)).SetEase(Ease.OutQuint);
			seq.Play();
        }

		public void HideFilterDropdowns()
		{
			foreach(var dropDownItem in this.DropDownButtons)
			{
				dropDownItem.DropDownRoot.gameObject.SetActive(false);
			}
		}

		public void CostItemToggled(int costVal)
		{
			this.ItemSelectSound.Play();
			this.ApplyFilters();
		}

		public void AspectItemToggled(int aspectVal)
		{
			this.ItemSelectSound.Play();
			this.ApplyFilters();
		}

		public void CollectionFilterSelected()
		{
			this.ItemSelectSound.Play();
			this.ApplyFilters();
		}

		public void ClearAspectFiltersClicked()
		{
			for(int i=0;i<this.AspectFilterToggles.Length;i++)
			{
				this.AspectFilterToggles[i].Toggle.isOn = false;
			}
			this.ApplyFilters();
		}

		public void ClearCostFiltersClicked()
		{
			for(int i=0;i<this.CostFilterToggles.Length;i++)
			{
				this.CostFilterToggles[i].isOn = false;
			}
			this.ApplyFilters();
		}

		private void ApplyFilters()
		{
			// Don't apply filters if we are currently having filters be applied by the InitFilterPanel() function -DMac
			if(this.isDuringFilterPanelInitialization) { return; }

			CardFilterParams filters = this.PlayerProfile.CurrentCardFilter;
			List<CardAspect> aspects = new List<CardAspect>();
			List<int> costs = new List<int>();
			bool isFilterSet = false;

			// Aspects
			for(int i=0;i<this.AspectFilterToggles.Length;i++)
			{
				Toggle aspectToggle = this.AspectFilterToggles[i].Toggle;
				this.AspectFilterToggles[i].SmallIcon.SetActive(false);
				if(aspectToggle.isOn)
				{
					aspects.Add(this.AspectFilterToggles[i].Aspect);
					this.AspectFilterToggles[i].SmallIcon.SetActive(true);
					isFilterSet = true;
				}
				aspectToggle.GetComponentInChildren<TextMeshProUGUI>().color = aspectToggle.isOn ? this.SelectedTextColor : this.DefaultTextColor;
			}

			// Enable the "Clear All" button at the bottom of the aspect group box dropdown
			this.AspectClearButton.interactable = isFilterSet;

			// Enable the aspect preview on the aspect dropdown button if there are aspect filters applied
			this.CurrentAspectsRoot.alpha = isFilterSet ? 1.0f : 0f;
			this.AspectButtonLabel.gameObject.SetActive(!isFilterSet);
			LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)this.CurrentAspectsRoot.transform);

			// Get a list of all the cost filters currently applied
			isFilterSet = false;
			for(int i=0;i<this.CostFilterToggles.Length;i++)
			{
				if(this.CostFilterToggles[i].isOn)
				{
					costs.Add(i);
					isFilterSet = true;
				}
			}

			// Enable the "Clear All" button at the bottom of the aspect group box dropdown
			this.CostClearButton.interactable = isFilterSet;

			// Build a filter string based on the costs list and use it as the dropdown label
			string filterString = this.GetCostFilterLabel(costs);
			this.CostButtonFilterLabel.text = filterString;
			this.CostButtonLabel.gameObject.SetActive(filterString == null);
			this.CostButtonFilterLabel.gameObject.SetActive(filterString != null);
	
			// Initialize the card collection toggles
			for(int i=0;i<this.CardCollectionToggles.Length;i++)
			{
				CardCollectionFilterItem cardCollection = this.CardCollectionToggles[i];
				cardCollection.FilterToggle.GetComponentInChildren<TextMeshProUGUI>().color = cardCollection.FilterToggle.isOn ? this.SelectedTextColor : this.DefaultTextColor;
			}

			// Color the CollectionType toggels correctly
			for(int i=0;i<this.CardCollectionToggles.Length;i++)
			{
				CardCollectionFilterItem cardCollection = this.CardCollectionToggles[i];
				TextMeshProUGUI label = cardCollection.FilterToggle.GetComponentInChildren<TextMeshProUGUI>();
				label.color = cardCollection.FilterToggle.isOn ? this.SelectedTextColor : this.DefaultTextColor;
				if(cardCollection.FilterToggle.isOn)
				{
					filters.CollectionType = cardCollection.CollectionType;
					this.CollectionTypeButtonLabel.GetComponent<I2.Loc.Localize>().Term = label.GetComponent<I2.Loc.Localize>().Term;
				}
			}

			filters.Aspects = aspects.ToArray();
			filters.Costs = costs.ToArray();

			this.StartCoroutine(this.ApplyFiltersDelay(filters));
		}

		private IEnumerator ApplyFiltersDelay(CardFilterParams filters)
		{
			// Give the UI elemetns a second to update before doing computationally intense filtering
			yield return new WaitForSecondsRealtime(0.2f);

			if(this.EventFilterChangeApplied != null)
			{
				EventFilterChangeApplied.Invoke(filters);
			}
		}

		private bool isDuringFilterPanelInitialization = false;
		public void InitFilterPanel()
		{
			this.isDuringFilterPanelInitialization = true;

			if(this.PlayerProfile.CurrentCardFilter != null)
			{
				//Debug.Log("Applying currentCardFilter");
				// Commented out initialization of the Widget for now because we aren't using it and it was causing the search input to lose focus during typing -DMac
				//this.FilterPanelWidget.MatchState(this.PlayerProfile.CurrentCardFilter);
				//this.ClearAllButton.interactable = this.FilterPanelWidget.IsFiltersApplied;

				CardFilterParams filters = this.PlayerProfile.CurrentCardFilter;

				// Initialize the Aspect Filters
				List<CardAspect> aspectFilters = filters.Aspects.ToList<CardAspect>();
				bool isFilterSet = false;
				for(int i=0;i<this.AspectFilterToggles.Length;i++)
				{
					// Take advantage of being able to cast integers to aspectEnum
					CardAspect aspect = this.AspectFilterToggles[i].Aspect;
				
					// If the aspect is present in the filters list, toggle the item on, otherwise toggle it off
					this.AspectFilterToggles[i].Toggle.isOn = aspectFilters.Contains(aspect);
					isFilterSet = isFilterSet || this.AspectFilterToggles[i].Toggle.isOn;
					this.AspectFilterToggles[i].SmallIcon.SetActive(this.AspectFilterToggles[i].Toggle.isOn);
				}

				// Enable the "Clear All" button at the bottom of the aspect group box dropdown
				this.AspectClearButton.interactable = isFilterSet;

				this.CurrentAspectsRoot.alpha = isFilterSet ? 1.0f : 0f;
				this.AspectButtonLabel.gameObject.SetActive(!isFilterSet);
				LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)this.CurrentAspectsRoot.transform);

				// Reset the filter state traking variables
				isFilterSet = false;

				// Initialize the Cost Filters
				List<int> costFilters = filters.Costs.ToList<int>();
				for(int i=0;i<=7;i++)
				{
					this.CostFilterToggles[i].isOn = costFilters.Contains(i);
					isFilterSet = isFilterSet || this.CostFilterToggles[i].isOn;
				}

				// Enable the "Clear All" button at the bottom of the aspect group box dropdown
				this.CostClearButton.interactable = isFilterSet;

				// Build a filter string based on the costs list and use it as the dropdown label
				string filterString = this.GetCostFilterLabel(costFilters);
				this.CostButtonFilterLabel.text = filterString;
				this.CostButtonLabel.gameObject.SetActive(filterString == null);
				this.CostButtonFilterLabel.gameObject.SetActive(filterString != null);

				// Initialize the Collection type filters
				for(int i=0;i<this.CardCollectionToggles.Length;i++)
				{
					CardCollectionFilterItem cardCollection = this.CardCollectionToggles[i];
					cardCollection.FilterToggle.isOn = cardCollection.CollectionType == filters.CollectionType;
					TextMeshProUGUI filterToggleLabel = cardCollection.FilterToggle.GetComponentInChildren<TextMeshProUGUI>();
					if(cardCollection.FilterToggle.isOn)
					{
						this.CollectionTypeButtonLabel.GetComponent<I2.Loc.Localize>().Term = filterToggleLabel.GetComponent<I2.Loc.Localize>().Term;
                    }
					filterToggleLabel.color = cardCollection.FilterToggle.isOn ? this.SelectedTextColor : this.DefaultTextColor;
				}
			}
			else
			{
				Debug.Log("Current filter is null");
				//this.FilterPanelWidget.ClearAll();
				//this.ClearAllButton.interactable = false;
			}

			this.isDuringFilterPanelInitialization = false;
		}

		private string GetCostFilterLabel(List<int> costs)
		{
			if(costs.Count > 0)
			{
				int previousCost = costs.First(); 
				System.Text.StringBuilder sb = new System.Text.StringBuilder();
				sb.Append(previousCost);
				bool isHyphenated = false;

				// Loop though the cost array staring after the first cost and stopping before the last
				for(int i=1;i<costs.Count-1;i++)
				{
					if(isHyphenated == false)
					{
						// Check for the start of a hyphenated run
						if(costs[i] == previousCost + 1 && costs[i+1] == previousCost + 2)
						{
							// Begin hyphenated rus with a "-" instead of the cost
							isHyphenated = true;
							sb.Append("-");
						}
						else
						{
							// When not hyphenated, add costs with a prepended ","
							sb.Append(",");
							sb.Append(costs[i]);
						}
					}
					else
					{
						// Check for the end of a hyphenated run
						if(costs[i+1] != costs[i] + 1)
						{
							isHyphenated = false;
							sb.Append(costs[i]);
						}

						// By default we don't add anything to a hyphenated run until it ends
					}

					// Store current cost as the previousCost
					previousCost = costs[i];
				}

				if(costs.Count > 1)
				{
					if(isHyphenated == false) sb.Append(","); 
					sb.Append(costs.Last());
					if(costs.Last() == 7)
					{
						sb.Append("+");
					}
				}
                string str = sb.ToString();
				//Debug.LogFormat("label:{0}{1}", str, costs.Count);
				return str;
			}

			return null;
		}
		
		public void OnFiltersClosedClicked()
		{
			Sequence seq = DOTween.Sequence();
			//seq.Append(this.FilterBox.DOAnchorPosY(this.FilterBox.rect.height+5f,0.3f).SetEase(Ease.OutQuint));
			//seq.Join(((RectTransform)this.ScrollArea.parent.parent).DOAnchorPosY(-64f,0.3f).SetEase(Ease.OutQuint));
			seq.Insert(0.05f,(((RectTransform)this.FilterBarWidget.transform).DOAnchorPosY(0f,0.15f)));
			
			//seq.AppendCallback(()=>this.FilterBox.gameObject.SetActive(false));
			seq.Play();
			
		}

        public void Recycle()
		{
			/*
			this.currentGridRow = 0;
			this.gridCards.Clear();
			
			if (null != _GridRX) {
				foreach (var dc in _CardInstances.Where(i => null != i)) {
					dc.DespawnCardDisplay();
					GameObject.Destroy(dc.gameObject);
					//ObjectPool.Recycle<DraggableCardView>(dc);
				}
				_CardInstances.Clear();
            }
            else Debug.Log("No grid to recycle");
			*/

		}
		
		[ContextMenu("Scroll down one row")]
		public void IncrementRow()
		{
			
		}
		
		public void UpdateCollectionTotals(PlayerProfile playerProfile)
		{
			for(int i=0;i<this.gridRows.Count;i++)
			{
				this.gridRows[i].UpdateCollectionTotals(playerProfile);
			}
		}
		

		public void Initialize(List<CardData> listOfCards)
		{
			//Debug.Log("Initializing CardBrowser View");

			if(this.TwoTouchOverlay == null)
			{
				this.TwoTouchOverlay = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			}
			
			this.cardDataList = listOfCards;
		
			//Debug.LogError("Initialize Browser View :" + listOfCards.Count);

			float columnWidth = UITools.CalcGridColumnWidth(this.Grid, this.CardColumns);
			this.Grid.cellSize = new Vector2(Mathf.Floor(columnWidth), Mathf.Floor(columnWidth*this.CardRatio));
			
			// Calculate the number of rows of cards the card grid should store
			// this is based on the visible rows plus two extra offscreen rows to support scaling
			float viewportHeight = this.ScrollView.viewport.rect.height;
			this.numOnScreenCardRows = (int) Mathf.Ceil(viewportHeight / this.Grid.cellSize.y);
			this.numOffscreenCardRows = this.CardRows - this.numOnScreenCardRows;

			this.lasScrollTime = Time.unscaledTime;
			this.ScrollView.verticalScrollbar.targetGraphic.DOKill();
			this.ScrollView.verticalScrollbar.targetGraphic.DOFade(1.0f, 0f);
			
			if(this.gridRows.Count == 0)
			{
				for(int i=0;i<this.CardRows;i++)
				{
					GridLayoutGroup newGrid = UITools.SpawnRect(this.Grid, this.ScrollArea);
					GridRow newGridRow = new GridRow(newGrid);
					newGridRow.Initialize(this.CardColumns,this.DraggableCardPrefab,this.CardPrefab, this.OnShowTwoTouch, this.OnDismissTwoTouch, this.DragOverlay);
					newGridRow.Grid.name = "GridRow" + i;
					this.gridRows.Add(newGridRow);
					newGridRow.YOffset = i * this.Grid.cellSize.y;
					this.gridRows[i].BindCardData(this.cardDataList, i * this.CardColumns);
					// We dont update collection totals here because often they aren't loaded yet!
				}
			}
			else
			{
				for(int i=0;i<this.gridRows.Count;i++)
				{
					this.gridRows[i].BindCardData(this.cardDataList, i * this.CardColumns);
					this.gridRows[i].UpdateCollectionTotals(this.PlayerProfile);
				}
			}

			// Disable the template grid row
			this.Grid.gameObject.SetActive(false);
			
			this.topMostGridRow = this.gridRows[0];
			this.bottomMostGridRow = this.gridRows[this.gridRows.Count-1];
			this.ResizeScrollArea(this.cardDataList.Count);
		}
		
		private void ResizeScrollArea(int numCards)
		{
			int numRows = (int) Mathf.Ceil(numCards / (float)this.CardColumns);
			int paddingBottom = 75;
			Vector2 size = this.ScrollArea.sizeDelta;
			size.y = numRows * this.Grid.cellSize.y;
			size.y += paddingBottom;
			this.ScrollArea.sizeDelta = size;
		}
		
		private GridRow GetFurthestAwayRow(GridRow row)
		{
			float distance = 0f;
			GridRow furthestRow = null;
			for(int i=0;i<this.gridRows.Count;i++)
			{
				GridRow testRow = this.gridRows[i];
				if(testRow != row)
				{
					float testDistance = Mathf.Abs(row.YOffset - testRow.YOffset);
					if(testDistance > distance)
					{
						furthestRow = testRow;
						distance = testDistance;
					}
				}
			}
			
			if(furthestRow == null)
			{
				for(int i=0;i<this.gridRows.Count;i++)
				{
					if(this.gridRows[i] != row) return this.gridRows[i];
				}
			}
			
			return furthestRow;
		}
		
		private void UpdateTopAndBottommostRows()
		{
			for(int i=0;i<this.gridRows.Count;i++)
			{
				GridRow testRow = this.gridRows[i];
				if(testRow.YOffset > this.bottomMostGridRow.YOffset)
				{
					this.bottomMostGridRow = testRow;
				}
				
				if(testRow.YOffset < this.topMostGridRow.YOffset)
				{
					this.topMostGridRow = testRow;
				}
			}
		}

		public void OnCloseAnimationFinished()
		{
			this.gameObject.SetActive(false);
		}

		protected Rect _ScrollRect;



		protected void CalcGridScroll()
		{
			
			//Debug.Log("Recalculating grid scroll");
			RectTransform gridRect = (RectTransform)this.Grid.transform;
			
			if(this.cardDataList == null) return;
			
			float columnWidth = UITools.CalcGridColumnWidth(this.Grid, this.CardColumns);
			this.Grid.cellSize = new Vector2(Mathf.Floor(columnWidth), Mathf.Floor(columnWidth*this.CardRatio));
			this.ResizeScrollArea(this.cardDataList.Count);

			for(int i=0;i<this.gridRows.Count;i++)
			{
				this.gridRows[i].Grid.cellSize = this.Grid.cellSize;
				CardView[] cardViews = this.gridRows[i].Grid.GetComponentsInChildren<CardView>();
				for(int j=0;j<cardViews.Length;j++)
				{
					cardViews[j].ScaleToWidth(this.Grid.cellSize.x);
				}
				this.gridRows[i].ResetRowPosition();
			}
			
			this.UpdateTopAndBottommostRows();
			
		}

		Vector2 lastScroll = Vector2.zero;
		protected void OnScrollView_Scrolled(Vector2 scroll)
		{
			if (!this.IsOpen) return;

			// Working variables
			float deltaY = scroll.y - this.lastScroll.y;
			this.lastScroll = scroll;
			float yOffset = (deltaY * this.ScrollArea.sizeDelta.y);
			float padding =  this.Grid.cellSize.y * 1;
			RectTransform viewport = this.ScrollView.viewport;
			RectTransform gridRect = ((RectTransform)this.Grid.transform);

			if(Mathf.Abs(yOffset) > 5)
			{
				this.lasScrollTime = Time.unscaledTime;
				this.ScrollView.verticalScrollbar.targetGraphic.DOKill();
				this.ScrollView.verticalScrollbar.targetGraphic.DOFade(1.0f, 0f);
			}
			
			
			if(yOffset < 0)
			{
				// Scrolling down cheks
				
				//Debug.LogFormat("scrolling down:{0}", deltaY);
				float viewportBottomInScrollArea = viewport.rect.height + this.ScrollArea.anchoredPosition.y;
				float cardGridBottomInScrollArea = this.bottomMostGridRow.YOffset + this.bottomMostGridRow.Height;

                //Debug.LogFormat("ViewportBottom: {0} gridBottom:{1} padding:{2} scrollAreaLocaly:{3}", viewportBottomInScrollArea, cardGridBottomInScrollArea, padding, this.ScrollArea.anchoredPosition.y);
                if ( (viewportBottomInScrollArea + padding) > cardGridBottomInScrollArea)
				{
					int deltaGridRows = Mathf.Max(this.numOffscreenCardRows - 1,(int)Mathf.Ceil(yOffset / -this.bottomMostGridRow.Height));
					int rowIndex = this.bottomMostGridRow.RowIndex;
					
					for(int i=0;i<deltaGridRows;i++)
					{
						// Get the furthest away row
						GridRow row = GetFurthestAwayRow(this.bottomMostGridRow);
						
						// Databaind it to the row index
						row.BindCardData(this.cardDataList, (rowIndex + 1 + i) * this.CardColumns);
						row.UpdateCollectionTotals(this.PlayerProfile);
						
						// Update top and bottom most rows
						this.UpdateTopAndBottommostRows();
					}
				}
			}
			else if(yOffset >= 0)
			{
				
				// Scrolling up checks
				float viewportTopInScrollArea = this.ScrollArea.anchoredPosition.y;
				float cardGridTopInScrollArea = this.topMostGridRow.YOffset;

                //Debug.LogFormat("ViewportTop: {0} cardGridTop:{1} padding:{2} yOffset:{3}", viewportTopInScrollArea, cardGridTopInScrollArea, padding, yOffset);

                if ( ((viewportTopInScrollArea - padding) > 0) && (viewportTopInScrollArea-padding) < cardGridTopInScrollArea)
				{
					int deltaGridRows = Mathf.Min(-this.numOffscreenCardRows +1,(int)Mathf.Ceil(yOffset / -this.topMostGridRow.Height));
					int rowIndex = this.topMostGridRow.RowIndex;

					if(deltaGridRows + rowIndex < 0)
					{
						Debug.LogFormat("Previously deltaGridRows:{0}", deltaGridRows);
						deltaGridRows = -rowIndex;
					}

                    //Debug.LogFormat("DeltaGridRows:{0} rowIndex:{1}", deltaGridRows, rowIndex);

                    for (int i=0;i<Mathf.Abs(deltaGridRows);i++)
					{
						// Get the furthest away row
						GridRow row = GetFurthestAwayRow(this.topMostGridRow);
						
						// Databaind it to the row index
						row.BindCardData(this.cardDataList, (rowIndex - 1 - i) * this.CardColumns);
						row.UpdateCollectionTotals(this.PlayerProfile);
						
						// Update top and bottom most rows
						this.UpdateTopAndBottommostRows();
					}
				}
			}
		}

		protected void OnDropTarget_Hover(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard && null != draggableCard.DraggedCard) {
				draggableCard.DraggedCard.SwitchDisplay(this.CardPrefab);
			}
		}

		protected void OnDropTarget_Drop(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard) {
				var listItem = draggableCard.CardDisplay as CardListItemView;
				if (null != listItem) 
				{
					NotifyCardListItemDropped(listItem);
					return;
				}
			}

			if (null != draggableCard) {
				var listItem = draggableCard.CardDisplay as CardListItemProxyView;
				if (null != listItem) NotifyCardListItemProxyDropped(listItem);
			}
		}

		protected void NotifyCardListItemDropped(CardListItemView listItem) { if (null != this.EventCardListItemDropped) this.EventCardListItemDropped.Invoke(listItem.CardData); }

		protected void NotifyCardListItemProxyDropped(CardListItemProxyView listItem) { if (null != this.EventCardListItemDropped) this.EventCardListItemDropped.Invoke(listItem.CardData); }
	}
}
