﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.Game;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
    public class GameLogBrowserView : AnimatedView
    {
        public Action Dismiss;
        public Action<int> ClickLog;

        public VerticalLayoutGroup VerticalLayout;
        public GameLogListItem GameLogListItemPrefab;

        protected override void Awake()
        {
            //this.IsOpen = false;
            base.Awake();
        }

        public void OnClickClose()
        {
            if(Dismiss != null)
            {
                Dismiss.Invoke();
            }
        }

        public void OnClickLogElement(int logNumber)
        {
            if (ClickLog != null)
            {
                ClickLog.Invoke(logNumber);
            }
        }

        public void Initialize(List<GameLog> gameLogs)
        {
            var elements = this.VerticalLayout.GetComponentsInChildren<GameLogListItem>();
            int existingElements = elements.Count();
            for (int i = gameLogs.Count; i < existingElements; i++)
            {
                elements[i].gameObject.SetActive(false);
            }
            // need to populate the log!!
            for (int i = 0; i < gameLogs.Count ; i++)
            {
                GameLogListItem element;
                if (i < existingElements)
                    element = elements[i];
                else
                    element = (GameLogListItem)GameObject.Instantiate(this.GameLogListItemPrefab);
                element.transform.AttachToParent(this.VerticalLayout.transform, 0);
                element.transform.localPosition = Vector3.zero;
                var log = gameLogs[i];
                element.Opponent.text = log.OpponentPlayerData != null ? log.OpponentPlayerData.displayName : string.Empty;
                element.Format.text = log.Format.ToString();
                var last = log.LogElements.LastOrDefault();
                element.Turns.text = last == null ? "0" : last.Turn.ToString();
                element.Result.text = log.IsPlayerWinner == true ? I2.Loc.ScriptLocalization.Victory : I2.Loc.ScriptLocalization.Defeat;
                element.LogNumber = i;
                element.Date.text = string.Format("{0} {1}",
                    (log.StartTime.DayOfYear == DateTime.Now.DayOfYear && log.StartTime.Year == DateTime.Now.Year)
                        ? I2.Loc.ScriptLocalization.Today
                        : (log.StartTime.AddDays(1).DayOfYear == DateTime.Now.DayOfYear && log.StartTime.AddDays(1).Year == DateTime.Now.Year)
                            ? I2.Loc.ScriptLocalization.Yesterday
                            : log.StartTime.ToString("M"),
                    log.StartTime.ToString("t"));
                element.Background.sprite = log.IsPlayerWinner == true ? element.winSprite : element.loseSprite;
                element.ClickLog -= OnClickLogElement;
                element.ClickLog += OnClickLogElement;
            }
        }
    }

}

namespace NovaBlitz.UI.Mediators
{
    public class GameLogBrowserMediator : Mediator
    {
        [Inject] public GameLogBrowserView View { get; set; }
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }
        [Inject] public GameLogsExportedSignal GameLogsExportedSignal { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.GameLogsExportedSignal.AddListener(this.OnGameLogsUpdated);
            this.View.ClickLog += OnClickLog;
            this.View.Dismiss += OnDismiss;
        }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.AnimatedViewOpenedSignal.RemoveListener(OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.GameLogsExportedSignal.RemoveListener(this.OnGameLogsUpdated);
            this.View.ClickLog -= OnClickLog;
            this.View.Dismiss -= OnDismiss;
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
                // Refresh the view databinding
                //this.RefreshView();
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {

            }
        }

        private void OnGameLogsUpdated()
        {
            this.View.Initialize(this.PlayerProfile.GameLogs);
        }

        private void OnDismiss()
        {
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
        }

        private void OnClickLog(int logNumber)
        {
            if(this.PlayerProfile.GameLogs.Count > logNumber && logNumber >= 0)
            {
                var log = this.PlayerProfile.GameLogs[logNumber];
                if (log != null)
                {
                    this.InitializeAnimatedViewSignal.Dispatch(typeof(GameLogView),
                        (v) =>
                        {
                            ((GameLogView)v).Initialize(log, logNumber, true);
                        });
                    this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(GameLogView));
                }
            }
        }
    }
}
