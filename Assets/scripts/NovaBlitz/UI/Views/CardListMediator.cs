﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using strange.extensions.context.impl;
using LinqTools;
using System.Collections.Generic;
using UnityEngine.UI;
using NovaBlitz.UI.DataContracts;
using Messages;
using System;

namespace NovaBlitz.UI.Mediators
{
	public class CardListMediator : Mediator 
	{
		[Inject] public CardListView View {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public GameData GameData { get; set; }

		[Inject] public AddCardToDeckSignal AddCardToDeckSignal {get;set;}
		[Inject] public SaveDeckSignal SaveDeckSignal {get;set;}

		[Inject] public CardAddedToDeckSignal CardAddedToDeckSignal {get;set;}
		[Inject] public CardRemovedFromDeckSignal CardRemovedFromDeckSignal {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		[Inject] public DeckSelectedSignal DeckSelectedSignal {get;set;}
		[Inject] public FilterCardsSignal FilterCardsSignal {get;set;}
        [Inject] public DecksModifiedSignal DecksModifiedSignal { get; set; }

        [Inject] public INovaContext ContextView {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
		[Inject] public CardTwoTouchOpenedSignal CardTwoTouchOpenedSignal {get;set;}
		[Inject] public CardTwoTouchClosedSignal CardTwoTouchClosedSignal {get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }

        private bool isEdited = false;
		private List<CardAspect> _WorkingAspects = new List<CardAspect>();

		/// <summary>
		/// Add event listeners
		/// </summary>
		public override void OnRegister()
		{
			this.View.CardListItemProxyPrefab = this.UIConfiguration.CardListItemProxyPrefab;
			this.View.CardListItemPrefab = this.UIConfiguration.CardListItemPrefab;
			this.View.DraggableCardPrefab = this.UIConfiguration.DraggableCardPrefab;
			this.View.DoneDeckBuilding += OnDoneDeckBuilding;
			this.View.EventCardDropped += OnView_CardDropped;
			this.CardAddedToDeckSignal.AddListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.AddListener(OnCardRemovedFromDeck);
			this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
			this.DeckSelectedSignal.AddListener(OnDeckSelected);
            this.DecksModifiedSignal.AddListener(OnDecksExternallyModified);
			this.CardTwoTouchOpenedSignal.AddListener(OnCardTwoTouchOpened);
			this.CardTwoTouchClosedSignal.AddListener(OnCardTwoTouchClosed);
			this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
		}

		/// <summary>
		/// Remove event listeners
		/// </summary>
		public override void OnRemove()
		{
			this.View.DoneDeckBuilding -= OnDoneDeckBuilding;
			this.View.EventCardDropped -= OnView_CardDropped;
			this.CardAddedToDeckSignal.RemoveListener(OnCardAddedToDeck);
			this.CardRemovedFromDeckSignal.RemoveListener(OnCardRemovedFromDeck);
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
			this.DeckSelectedSignal.RemoveListener(OnDeckSelected);
            this.DecksModifiedSignal.RemoveListener(OnDecksExternallyModified);
			this.CardTwoTouchOpenedSignal.RemoveListener(OnCardTwoTouchOpened);
			this.CardTwoTouchClosedSignal.RemoveListener(OnCardTwoTouchClosed);
			this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
        }

		private void OnCardTwoTouchClosed(CardData cardData)
        {
            if (cardData != null)
            {
                CardListItemProxyView cardListItem = this.View.FindListItem(cardData.CardID);
                if (cardListItem != null && cardListItem.IsSelected)
                {
                    cardListItem.SelectCard(false);
                }
            }
		}

        private void OnCardTwoTouchOpened(CardData cardData)
        {
			CardListItemProxyView selectedListItem = this.View.GetSelectedListItem();
			if(selectedListItem != null && selectedListItem.IsSelected)
			{
				selectedListItem.SelectCard(false);
			}

			CardListItemProxyView cardListItem = this.View.FindListItem(cardData.CardID);
			if(cardListItem != null && !cardListItem.IsSelected)
			{
				cardListItem.SelectCard(true);
			}
        }


        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
				DeckData deck = this.PlayerProfile.CurrentDeck;
				if ( deck != this.View.DeckData) {
					_WorkingAspects = deck.Aspects;
					this.isEdited = false;
					this.View.Initialize(deck, PlayerProfile.OwnedCards, GameData.LegalCards);
				}
            }
			else if(viewType == typeof(TwoTouchOverlayView))
            {
				NovaBlitz.Game.GameLogView gameLogView = GameObject.FindObjectOfType<NovaBlitz.Game.GameLogView>();

				if(gameLogView == null || gameLogView.IsOpen == false)
				{
					// Make sure we go to the overlay layer with the TwoTouch
					this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
				}
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                this.View.Recycle();
			}
		}

        private void OnLanguageChanged()
        {
            this.View.Initialize(this.View.DeckData, PlayerProfile.OwnedCards, GameData.LegalCards);
        }

        #region View Listeners
        /// <summary>
        /// User clicked the done button
        /// </summary>
        private void OnDoneDeckBuilding(DeckData deckData)
        {
            Debug.Log("On done deckbuilding from deckbuilder");
            // If the two-touch is open, close it
            this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));

			//this.NovaContext.BlurMainCanvas(false, null);

			// If there were edits, save changes
			if (this.isEdited) {
				this.isEdited = false;
				this.SaveDeckSignal.Dispatch(deckData);
			}
            else
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.DeckList);
        }

		/// <summary>
		/// When a card is dragged and dropped onto the card list...
		/// </summary>
		protected void OnView_CardDropped(CardData card)
		{
			// Trigger the command to add a card to the deck
			this.AddCardToDeckSignal.Dispatch(card, this.PlayerProfile.CurrentDeck, false);
		}
		#endregion

		#region Signal Listeners

        private void OnDecksExternallyModified()
        {
            this.isEdited = true;
        }

        private void OnProfileUpdated(ProfileSection section)
		{
            if (!this.View.IsOpen)
                return;
            
			// Can't find a case where we need to refresh the CardList when the profile is updated
			// The Crafting / Scrapping already manages the state of the list on it's own
			// and opening the CardBrowser always loads the latest DeckList data -DMac
		}

		/// <summary>
		/// Removes a list item from the list, or if there's more than one of the card, decrements the counter
		/// </summary>
		private void OnCardRemovedFromDeck(CardData card, DeckData deck)
		{
			this.isEdited = true;
			this.View.InitializeDeckButton();

			// Get a reference to the CardListItem if it still exists in the cardList
			List<CardListItem> updatedCardList = CardListTransform.GetList(deck);
			CardListItem updateCardListItem = updatedCardList.Where( o=>o.CardID == card.CardID).FirstOrDefault();

			// loop though the list and try to find the removed item
			if (updateCardListItem == null) {
				this.View.RemoveListItem(card.CardID);
			}
			else {
				// Best way to refresh the view is to just re-initialize it with the current state
				var listItem = this.View.FindListItem(card.CardID);
				if(listItem != null)
				{
					listItem.DataBind(updateCardListItem, card, deck.MaxOfEachCard);
					listItem.TweenHighlight(false);
				}
			}

			if (!deck.Aspects.Contains(card.Aspect)) {
				_WorkingAspects.Remove(card.Aspect);
				if (_WorkingAspects.Count < 2) {
					var filterParams = this.PlayerProfile.CurrentCardFilter ?? new CardFilterParams();
					filterParams.Aspects = new CardAspect[0];
					this.FilterCardsSignal.Dispatch(filterParams);
				}
			}
		}

		/// <summary>
		/// Waits for the list to resize itself before setting the desired scrollbar value
		/// </summary>
		IEnumerator SetScrollBarValue(float value) 
		{
			yield return null;
			yield return null;
			this.View.Scrollbar.value = value;
		}

		/// <summary>
		/// Appends a new deckListItem to the end of the cardList
		/// </summary>
		private void OnCardAddedToDeck(CardData card, DeckData deck)
		{
			this.isEdited = true;
			this.View.InitializeDeckButton();

			if (_WorkingAspects.Count == 1 && !_WorkingAspects.Contains(card.Aspect))
            {
				_WorkingAspects.Add(card.Aspect);
				var filterParams = this.PlayerProfile.CurrentCardFilter ?? new CardFilterParams();
				filterParams.Aspects = _WorkingAspects.ToArray();
				this.FilterCardsSignal.Dispatch(filterParams);
			}
			if (_WorkingAspects.Count == 0)
				_WorkingAspects.Add(card.Aspect);

			// Get the index of the card in the transformed cardList
			CardListItem listItemData = null;
			CardListItemProxyView cardListItemView = null;
			int indexInView = CardListTransform.IndexOfCard(deck,card, ref listItemData);
			
			// Get a reference to the existing item
			if(indexInView < this.View.Cards.Count())
			{
				cardListItemView = this.View.FindListItem(card.CardID);
			}
			
			// If one is found... does it match the card being added to the deck?
			if(cardListItemView != null && cardListItemView.CardData.CardID == card.CardID)
			{
				// Initialize the View with the correct ListItem Data
				cardListItemView.DataBind(listItemData, card, deck.MaxOfEachCard);
			}
			else
			{
				// Create a new item at the specified index
				cardListItemView = this.View.AddCardItemProxyToList(listItemData, indexInView, deck.MaxOfEachCard);
			}

			// Play a tween affect on the affected item in the list
			cardListItemView.TweenHighlight(true);

			// Scroll the list so that the added card is visible
			if(this.View.Cards.Count() > 1)
			{
				float scrollbarValue = 1.0f - (indexInView / (float)(this.View.Cards.Count()-1));
				StartCoroutine(SetScrollBarValue(scrollbarValue));
			}
		}

		private void OnDeckSelected(DeckData deck)
		{
			if (!this.View.IsOpen)
                return;
				
			if (deck != this.View.DeckData) {
				_WorkingAspects = deck.Aspects;
				this.isEdited = false;
				this.View.Initialize(deck, PlayerProfile.OwnedCards, GameData.LegalCards);
			}
		}
		#endregion
	}
}
