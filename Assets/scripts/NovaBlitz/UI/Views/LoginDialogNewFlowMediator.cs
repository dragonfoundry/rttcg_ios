﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using PlayFab.ClientModels;
using System;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class LoginDialogNewFlowMediator : Mediator
    {
        [Inject] public LoginDialogNewFlowView View { get; set; }
        [Inject] public LoginUserWithEmailSignal LoginUserWithEmailSignal { get; set; }
        [Inject] public AccountEmailFoundSignal AccountEmailFoundSignal { get; set; }
        [Inject] public LinkDeviceOrSteamSignal LinkDeviceOrSteamSignal { get; set; }
        [Inject] public AccountRecoverySignal AccountRecoverySignal { get; set; }
        [Inject] public UserLoggedInSignal UserLoggedInSignal { get; set; }
        [Inject] public UsernameAndPasswordAddedSignal UsernameAndPasswordAddedSignal { get; set; }
        [Inject] public RegisterUserSignal RegisterUserSignal { get; set; }
        [Inject] public AddUsernameAndPasswordSignal AddUsernameAndPasswordSignal { get; set; }
        [Inject] public UsernameNotAvailable UsernameNotAvailable { get; set; }
        [Inject] public InvalidEmailOrPasswordSignal InvalidEmailOrPasswordSignal { get; set; }
        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }
		[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        //[Inject] public ChillingoService ChillingoService { get; set; }
        [Inject] public EmailListSignupSignal EmailListSignupSignal { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public ShowMainMenuSignal ShowMainMenuSignal { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

        private bool isAccountChecked = false;
        private bool isEmailConfirmed = false;

        override public void OnRegister()
        {
            this.View.EmailEntered += this.OnEmailEntered;
            this.View.EmailConfirmed += this.OnEmailConfirmed;
            this.View.LoginRequested += this.OnLoginRequested;
            this.View.BackClicked += this.OnBackButtonClicked;
            this.View.PasswordResetRequested += this.OnPasswordResetRequested;
            this.View.RegisterUserRequested += this.OnRegisterUserRequested;
            this.View.LanguageButtonClicked += this.OnLanguageButtonPress;
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            this.View.EulaClicked += OnEulaClicked;
            this.View.PrivacyClicked += OnPrivacyClicked;
            this.View.TermsClicked += OnTermsClicked;
            this.View.CloseClicked += OnCloseClicked;
            if(this.View.IsOpen)
                this.View.ActivateEmailField();
            //OnOpenAnimatedView(this.View.GetType());
            //this.View.Reset(this.PlayerProfile.isAgeGatePassed, this.ClientPrefs.SteamName, !string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.PlayFabID));
        }

        override public void OnRemove()
        {
            this.View.EmailEntered -= this.OnEmailEntered;
            this.View.EmailConfirmed -= this.OnEmailConfirmed;
            this.View.LoginRequested -= this.OnLoginRequested;
            this.View.BackClicked -= this.OnBackButtonClicked;
            this.View.PasswordResetRequested -= this.OnPasswordResetRequested;
            this.View.RegisterUserRequested -= this.OnRegisterUserRequested;
            this.View.LanguageButtonClicked -= this.OnLanguageButtonPress;
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
            this.View.EulaClicked -= OnEulaClicked;
            this.View.PrivacyClicked -= OnPrivacyClicked;
            this.View.TermsClicked -= OnTermsClicked;
            this.View.CloseClicked -= OnCloseClicked;

            // ensure we remove added listeners when we destroy the view
            this.AccountEmailFoundSignal.RemoveListener(OnAccountEmailFound);
            this.UserLoggedInSignal.RemoveListener(OnUserLogged);
            this.UsernameAndPasswordAddedSignal.RemoveListener(OnUsernameAdded);
            this.UsernameNotAvailable.RemoveListener(NotifyUsernameNotAvailable);
            this.InvalidEmailOrPasswordSignal.RemoveListener(NotifyInvalidPassword);
        }

        public void OnOpenAnimatedView(Type view)
        {
            if (view == typeof(LoginDialogNewFlowView))
            {
                MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.StartFlow, true);
                this.View.ActivateEmailField();
                //this.View.Reset(this.PlayerProfile.isAgeGatePassed, this.ClientPrefs.SteamName, !string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.PlayFabID));
                //this.AsyncProgressSignal.Dispatch(AsyncProgress.NoProgress, -1.0f);
            }
        }

        public void OnCloseAnimatedView(Type view)
        {
            if (view == typeof(LoginDialogNewFlowView))
            {
                //this.View.Reset(this.ChillingoService.IsAgeGatePassed, this.ClientPrefs.SteamName);
                //this.NovaContext.BlurMainCanvas(false, this.View);
            }
        }

        private void OnPasswordResetRequested(string email)
        {
            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.PasswordReset, true);
            this.AccountRecoverySignal.Dispatch(email);
        }

        private void OnRegisterUserRequested(UserCredentials creds)
        {
            this.UsernameNotAvailable.RemoveListener(this.NotifyUsernameNotAvailable);
            this.UsernameNotAvailable.AddListener(this.NotifyUsernameNotAvailable);

            if (string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.PlayFabID))
            {
                Debug.LogError("Registering User");
                this.UserLoggedInSignal.RemoveListener(OnUserLogged);
                this.UserLoggedInSignal.AddListener(OnUserLogged);
                //this.UserRegisteredSignal.RemoveListener(OnUserRegistered);
                //this.UserRegisteredSignal.AddListener(OnUserRegistered);
                this.RegisterUserSignal.Dispatch(creds);
            }
            else
            {
                Debug.LogError("Adding username and password");
                this.UsernameAndPasswordAddedSignal.RemoveListener(OnUsernameAdded);
                this.UsernameAndPasswordAddedSignal.AddListener(OnUsernameAdded);
                this.AddUsernameAndPasswordSignal.Dispatch(creds);
            }
            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.Password, true);
        }

        private void OnLoginRequested(UserCredentials creds)
        {
            this.UserLoggedInSignal.RemoveListener(OnUserLogged);
            this.InvalidEmailOrPasswordSignal.RemoveListener(this.NotifyInvalidPassword);
            this.InvalidEmailOrPasswordSignal.AddListener(this.NotifyInvalidPassword);
            this.UserLoggedInSignal.AddListener(OnUserLogged);
            this.LoginUserWithEmailSignal.Dispatch(creds);
            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.LoggedIn, true);
        }

        private void OnEmailConfirmed(string email)
        {
            this.isEmailConfirmed = true;
            //this.View.BackButton.interactable = false;
            this.View.ConfirmButton.interactable = false;
            if (this.isAccountChecked)
            {
                this.View.Animator.SetInteger("TabIndex", 2);
            }
        }

        private void OnEmailEntered(string email)
        {
            this.isAccountChecked = false;
            this.isEmailConfirmed = false;
            this.AccountEmailFoundSignal.AddListener(this.OnAccountEmailFound);
            UserCredentials creds = new UserCredentials(email);
            creds.Password = "LordDeliverMeFromMyWickedWaysTempPassword";
            this.LoginUserWithEmailSignal.Dispatch(creds);
        }

        private void OnAccountEmailFound(bool isFound)
        {
            Debug.LogFormat("Account Email found. View:{0} Animator:{1} Profile:{2}",
                View == null ? "null" : "ok", 
                View == null || View.Animator == null ? "null" : "ok",
                PlayerProfile == null ? "null" : "ok");
            this.AccountEmailFoundSignal.RemoveListener(this.OnAccountEmailFound);
            this.UserLoggedInSignal.RemoveListener(OnUserLogged);
            if (isFound)
            {
                this.View.SetEmailAsKnownAccount();
                this.UserLoggedInSignal.AddListener(OnUserLogged);
                this.View.ConfirmEmailTitle.text = I2.Loc.ScriptLocalization.Account_Found;
            }
            else
            {
                this.View.ConfirmEmailTitle.text = I2.Loc.ScriptLocalization.Confirm_Email;
                MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.Email, true);
            }

            if (this.PlayerProfile.isAgeGatePassed != true)
            { 
                // Turn off the 
                this.View.MailingListToggle.gameObject.SetActive(false);
                this.View.MailingListToggle.isOn = false;
                this.View.ConfirmEmailExplanation.gameObject.SetActive(true);
                this.View.Under13LoginText.gameObject.SetActive(true);
                this.View.PasswordReset.gameObject.SetActive(false);
            }
            else if(this.PlayerProfile.isAgeGatePassed == true)
            {
                this.View.MailingListToggle.gameObject.SetActive(true);
                this.View.MailingListToggle.isOn = !isFound;
                this.View.ConfirmEmailExplanation.gameObject.SetActive(false);
                this.View.Under13LoginText.gameObject.SetActive(false);
                this.View.PasswordReset.gameObject.SetActive(true);
            }
            this.View.ConfirmButton.interactable = true;
            
            this.isAccountChecked = true;  
            
            if(this.isEmailConfirmed)
            {
                this.View.Animator.SetInteger("TabIndex",2);
            } 
        }

        private void OnBackButtonClicked()
        {
            this.UserLoggedInSignal.RemoveListener(OnUserLogged);
            this.AccountEmailFoundSignal.RemoveListener(OnAccountEmailFound);
            this.InvalidEmailOrPasswordSignal.RemoveListener(this.NotifyInvalidPassword);
            this.UserLoggedInSignal.RemoveListener(OnUserLogged);
            this.UsernameAndPasswordAddedSignal.RemoveListener(OnUsernameAdded);
        }

        private void RegisterForEmail()
        {
            if (this.PlayerProfile.isAgeGatePassed != true || !this.View.MailingListToggle.isOn)
            {
                MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.MailingList, false);
                return;
            }
            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.MailingList, true);

            // Add to mailing list if they've agreed to do so; only do this when they register.
            string email = this.View.Email;
            string username = this.View.Username;
            Platform platform = Platform.Steam;
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                platform = Platform.IOS;
            }
            else if (Application.platform == RuntimePlatform.Android)
            {
                platform = Platform.Android;
            }
            else if(Application.platform == RuntimePlatform.WSAPlayerX86 
                || Application.platform == RuntimePlatform.WSAPlayerARM 
                || Application.platform == RuntimePlatform.WSAPlayerX64)
            {
                platform = Platform.Windows;
            }
            
            Debug.Log("signing up for email list");
            var emailParams = new EmailListParams(email, username, this.PlayerProfile.MetaProfile.PlayFabID, platform);
            EmailListSignupSignal.Dispatch(emailParams);
        }
        
        private void OnUserLogged(LoginResult result)
		{
            Debug.Log("OnUserLoggedInOrRegistered: Logged in");
            this.UserLoggedInSignal.RemoveListener(OnUserLogged);
            this.UsernameNotAvailable.RemoveListener(this.NotifyUsernameNotAvailable);
            this.InvalidEmailOrPasswordSignal.RemoveListener(this.NotifyInvalidPassword);
            
            // Link the device/steam account to the playfab account
            this.LinkDeviceOrSteamSignal.Dispatch();

            RegisterForEmail();

            // Return the view to its default state
            ResetView();
            this.AsyncProgressSignal.Dispatch(AsyncProgress.Authenticating, 1.0f);
        }


        /// <summary>
        /// In the returning user flow, this handler is called when the user types in an
        /// invalid password for their Email.
        /// </summary>
        internal void NotifyInvalidPassword()
        {
            this.View.ShowErrorMessage(I2.Loc.ScriptLocalization.Error.InvalidPassword);
            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.Password, false);
            this.View.LoginButton.interactable = true;
        }

        /// <summary>
        /// When the mediator receives a signal from the register user command that the name 
        /// is taken, we need to tell the user to pick a new nickname /username
        /// </summary>
        internal void NotifyUsernameNotAvailable(string username)
        {
            this.View.ShowErrorMessage(I2.Loc.ScriptLocalization.Error.UsernameTaken);
            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.Username, false);
            this.View.DoneButton.interactable = true;
        }

        private void OnUsernameAdded()
        {
            Debug.Log("OnUserLoggedInOrRegistered: username added");
            this.UsernameAndPasswordAddedSignal.RemoveListener(OnUsernameAdded);
            this.UsernameNotAvailable.RemoveListener(this.NotifyUsernameNotAvailable);
            this.InvalidEmailOrPasswordSignal.RemoveListener(this.NotifyInvalidPassword);


            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.Username, true);
            MetricsTrackingService.TrackAccountCreationStep(AccountCreationStep.CompleteFlow, true);
            RegisterForEmail();
            this.ShowMainMenuSignal.Dispatch(new ShowMainMenuParams());
            // Return the view to its default state
            ResetView();
            this.AsyncProgressSignal.Dispatch(AsyncProgress.Authenticating, 1.0f);
        }


        private void ResetView() 
        {
            this.AccountEmailFoundSignal.RemoveListener(OnAccountEmailFound);
            this.UserLoggedInSignal.RemoveListener(OnUserLogged);
            this.UsernameAndPasswordAddedSignal.RemoveListener(OnUsernameAdded);
            this.UsernameNotAvailable.RemoveListener(this.NotifyUsernameNotAvailable);
            this.InvalidEmailOrPasswordSignal.RemoveListener(this.NotifyInvalidPassword);
            //this.View.Reset();
            //this.NovaContext.BlurMainCanvas(false, this.View);
            this.CloseAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));
        }

        /// <summary>
        /// Allows the user to change the langauge on the login screen, if it's not in the right langauge for them
        /// </summary>
        public void OnLanguageButtonPress()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LanguageSelectorView));
        }

        public void OnLanguageChanged()
        {
            this.View.SetText(this.View.Settings);
        }


        private void OnEulaClicked()
        {
            Application.OpenURL(this.GameData.url_EULA);
        }

        private void OnPrivacyClicked()
        {
            Application.OpenURL(this.GameData.url_Privacy);
        }

        private void OnTermsClicked()
        {
            Application.OpenURL(this.GameData.url_Terms);
        }

        private void OnCloseClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
            // if the player's logged in, just close. If not, open first view again
            if(!this.ClientService.IsConnected)
            {
                this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(FirstDialogView));
            }
        }
    }
}
