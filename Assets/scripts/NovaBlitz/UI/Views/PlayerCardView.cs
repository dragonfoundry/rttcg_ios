﻿using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using Messages;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class PlayerCardView : View
    {
        public Action ClickAvatar { get; set; }
        public Action ClickCardBack { get; set; }
        public Action ClickGameLog { get; set; }
        public Action ClickRedeem { get; set; }
        public Action AnimationsComplete { get; set; }

        public CanvasGroup CanvasGroup;
        public TextMeshProUGUI NameDisplay;
        public CardArtImage AvatarArt;
        public RawImage CardBack;
        public Image Frame;
        public CanvasGroup NewRankFrame;
        public Image NewRankFrameImage;
        public Image NewTierBackground;
        public TextMeshProUGUI NewTierText;

        public GameObject ButtonArea; // turn this off unless in the profile view.
        public Button AvatarButton; // used in the profile menu to set your avatar
        public Button DeckButton; // Used in the profile menu to set your deck
        public NovaButton GameLogButton;

        //public GameObject LeaderboardPositionObject;
        public Canvas RankOffset;
        public Image LeaderboardPositionDisplay;
        public TextMeshProUGUI LeaderboardPositionAmount;
        public TextMeshProUGUI NovaRankLabel;
        //public TextMeshProUGUI NovaRankUp;
        public GameObject RankObject;
        public Image RankDisplay;
        public TextMeshProUGUI RankAmount;
        public TextMeshProUGUI RankUp;
        public GameObject WinStreakObject;
        public Image WinStreakDisplay;
        public TextMeshProUGUI WinStreakAmount;
        public TextMeshProUGUI WinStreakUp;
        public GameObject QPObject;
        public Image QPDisplay;
        public TextMeshProUGUI QPAmount;
        public TextMeshProUGUI QPUp;
        public GameObject TotalWinsObject;
        public Image TotalWinsDisplay;
        public TextMeshProUGUI TotalWinsAmount;
        public TextMeshProUGUI TotalWinsUp;

        public Image[] RatingPips;

        public Image RatingSlider;
        public TextMeshProUGUI RatingIncrease;
        public TextMeshProUGUI RatingText;
        private IntRange RatingBracket;

        //public ComboSlider ComboSlider;
        public GameObject SliderArea;
        protected int _Rating;
        public int Rating { get { return _Rating; } set {
                _Rating = value;
            } }

        public GameObject StatsPanel;
        public GameObject QuestPanel;
        public GameObject PlayStatusPanel;
        public PlayStatusWidget PlayStatusWidget;
        public Canvas WinsBackground;
        public GameObject VerticalStatusPanel;
        public PlayStatusWidget VerticalStatusWidget;
        public TextMeshProUGUI EventFormat;

        public QuestView Quest { get; set; }
        
        public bool isVertical = false;

        [System.Serializable]
        public struct PlayerFramesClass
        {
            public Sprite Bronze;
            public Sprite Silver;
            public Sprite Gold;
            public Sprite Platinum;
            public Sprite Diamond;
            public Sprite Nova;
        }
        public PlayerFramesClass PlayerFrames = new PlayerFramesClass();
        private Dictionary<int, Sprite> playerFrames = new Dictionary<int, Sprite>();

        public NovaAudioPlayer RankUpSound;
        public NovaAudioPlayer RatingGainSound;
        public NovaAudioPlayer StatGainSound;
        public NovaAudioPlayer EventLossSound;
        public NovaAudioPlayer EventWinSound;

        //private string AvatarArtId;

        //public string AvatarID { get; set; }
        

        /*
         * Flow is: 
         * Show the panel with its original data
         * animate the panel as it's coming in:
         *  - wobble the portrait
         *  - fade in whichever data is being faded in, one at a time
         * Add the animations for:
         *  - Rating(XP?)
         *  - Rank up (possible but unlikely to be able to do 2)
         *  - Win streak
         *  - # wins
         *  - QPs won
         */

        private PlayerData _PlayerData;
        public PlayerData PlayerData { get { return _PlayerData; }
            set
            {
                _PlayerData = value;
                isFirstSliderComplete = false;
                // Display name
                NameDisplay.text = value.displayName;

                // update art, name, display, etc.
                AvatarArt.UpdateCardArt(value.avatarArtId, CardAspect.NoAspect);
                SetCardBackArt(value.cardBackArtId);


                if (seq != null)
                {
                    seq.Kill();
                }
                if(bonusSequence != null)
                {
                    bonusSequence.Kill();
                    bonusSequence = null;
                }

                ResetNotifiers();

                /*
                // testing
                value.hasWon = true;
                //value.oldRating = 38000;
                //value.rating = 49000;
                value.oldRating = 19500;
                value.rating = 21000;
                value.oldWinStreak = 1;
                value.winStreak = 2;
                value.oldWins = 100;
                value.wins = 1005;
                value.oldQps = 20;
                value.qps = 200;
                value.oldRank = 13;
                //value.oldRank = -1;
                */

                if (value.oldRating == 0)
                    value.oldRating = value.rating;

                int startingRank = value.RankBrackets != null ? GetRankFromRating(value.oldRating, value.oldRank) : value.rank;
                
                this.NovaRankLabel.text = GetNovaRankLabel(startingRank);
                this.RankOffset.transform.localPosition = Vector3.zero;
                this.RankOffset.transform.localScale = Vector3.one;
                this.RankOffset.overrideSorting = false; 
                /*
                 * either just show the value, or show the original value & tween up to thenew value (or away)
                 * win streak: 
                 */
                bool isTournament = SetPlayStatus(value.tournamentData, value.format);

                if (value.Quest != null && (value.format == GameFormat.Casual || value.format == GameFormat.Practice))//(value.Quest.ID < 0 && value.Quest.ID > -5)))
                {
                    var quest = ObjectPool.SpawnPrefab<QuestView>(PooledObjectType.QuestView);
                    quest.transform.AttachToParent(this.QuestPanel.transform, 0);
                    quest.transform.localPosition = new Vector3(70, -10);
                    quest.transform.localRotation = Quaternion.identity;
                    quest.Initialize(value.Quest);
                    quest.QuestActive.color = Color.grey;
                    Quest = quest;

                    QuestPanel.SetActive(true);
                    Debug.LogFormat("Quest Activated: from {0}/{1} to {2}/{1} for {3}", quest.QuestData.CurrentScore, quest.QuestData.TargetScore, value.NewQuestValue, quest.QuestDetails.text);
                }
                else
                    QuestPanel.SetActive(false);
                SetUpBonusSequence(isTournament);




                // Either show Rating, or show Leaderboard Position (a big deal!)
                LeaderboardPositionDisplay.gameObject.SetActive(startingRank < 0);
                LeaderboardPositionAmount.text = Math.Max(0, value.leaderboardPosition).ToString();

                RankObject.SetActive(startingRank > 0 || startingRank < 0);
                RankDisplay.gameObject.SetActive(startingRank > 0);
                RankAmount.text = startingRank > 0 ? startingRank.ToString() : I2.Loc.ScriptLocalization.Nova;



                // Start by displaying the data based on the old rating (and rank).
                // If there's a new rating to display, animate it.
                // Animate the new player portrait frame
                // animate the pip gain for a rank up
                // animate the rank icon for a rank up
                // animate the change in Nova Rank
                // Add a "rank up" bug to make ranking up cooler

                this.RatingIncrease.gameObject.SetActive(false);

                // rank goes from 30 to -6, skipping 0
                int startingPips;
                int startingFrame;
                GetPipsAndFrame(startingRank, out startingPips, out startingFrame);
                SetFrame(startingFrame, startingFrame, null);
                SetPips(startingPips, startingPips, null);

                if (value.RankBrackets != null)
                {
                    TweenPlayerCard(value.oldRating, value.rating);
                }
                else
                {
                    SliderArea.gameObject.SetActive(false);
                }

            }
        }

        private void TweenPlayerCard(int oldRating, int rating)
        {
            var bracketId = EngineUtils.FindBracket(oldRating, PlayerData.RankBrackets);
            IntRange bracket;
            if (!PlayerData.RankBrackets.TryGetValue(bracketId, out bracket))
            {
                bracket = new IntRange { Min = -1, Max = rating };
            }

            this.RatingSlider.transform.DOScaleX(GetScale(oldRating, bracket), 0f);
            RatingText.text = bracket.Min == -1 ? oldRating.ToString() : string.Format("{0}/{1}", oldRating, bracket.Max + 1);

            this.CanvasGroup.alpha = 0;
            // Fade in the card view
            Sequence showCardSequence = DOTween.Sequence();
            showCardSequence.Append(this.CanvasGroup.DOFade(1.0f, FADE_IN_PLAYER_CARD));
            if (oldRating != rating)
            {
                showCardSequence.Insert(0.0f, this.CanvasGroup.transform.DOShakeRotation(FADE_IN_PLAYER_CARD, new Vector3(15, 0, 0), 1, 0, true));
            }
            showCardSequence.AppendCallback(() => PlayRatingSliderTweens(oldRating, rating));
        }


        public void ResetNotifiers()
        {
            RankDisplay.transform.localScale = Vector3.one;
            LeaderboardPositionDisplay.transform.localScale = Vector3.one;
            RankAmount.alpha = 1;
            LeaderboardPositionAmount.alpha = 1;
            RankUp.alpha = 0;
            RankUp.transform.localPosition = Vector3.zero;
            RankUp.transform.localScale = Vector3.one;
            WinStreakAmount.alpha = 1;
            WinStreakUp.alpha = 0;
            WinStreakUp.transform.localPosition = Vector3.zero;
            WinStreakUp.transform.localScale = Vector3.one;
            TotalWinsAmount.alpha = 1;
            TotalWinsUp.alpha = 0;
            TotalWinsUp.transform.localPosition = Vector3.zero;
            TotalWinsUp.transform.localScale = Vector3.one;
            QPAmount.alpha = 1;
            QPUp.alpha = 0;
            QPUp.transform.localPosition = Vector3.zero;
            QPUp.transform.localScale = Vector3.one;
        }

        public float GetScale(int rating, IntRange bracket)
        {
            if (bracket.Min == -1)
                return 1;
            float scale = 0.05f + 0.90f * (Math.Max(0f, (rating - bracket.Min)) / Math.Max(1.0f, (bracket.Max - bracket.Min))) + ((rating > bracket.Max) ? 0.05f : 0.0f);
            return scale;
        }
        private const string RATING_FORMAT = "{0}/{1}";

        public static Color DecreaseColor = new Color { r = 1f, g = 0.3f, b = 0.3f, a = 1 };
        public static Color IncreaseColor = new Color { r = 0.0f, g = 1.0f, b = 0.0f, a = 1 };

        private const float FADE_IN_PLAYER_CARD = 0.8f;
        private const float FADE_TIME = 0.25f;
        private const float INSERT_OFFSET = 0.4f;
        private const float FADE_HALFLIFE = 0.4f;
        private const float RATING_HALFLIFE = 0.4f;
        private const float BONUS_HALFLIFE = 0.4f;
        private Sequence seq;
        private Sequence bonusSequence;

        public void SetUpBonusSequence(bool isTournament)
        {
            var playerData = PlayerData;

            bonusSequence = DOTween.Sequence();
            bonusSequence.Pause();
            //bonusSequence.AppendInterval(5);
            //bonusSequence.

            if (!isTournament)
            {
                float length = 0f;
                if (playerData.oldWins == 0 || playerData.oldWins >= playerData.wins)
                {
                    TotalWinsDisplay.transform.localScale = Vector3.one;
                    TotalWinsObject.SetActive(playerData.wins > 0);
                    TotalWinsAmount.text = Math.Max(0, playerData.wins).ToString();
                }
                else
                {
                    TotalWinsDisplay.transform.localScale = Vector3.one;
                    TotalWinsObject.SetActive(true);
                    TotalWinsAmount.text = Math.Max(0, playerData.oldWins).ToString();
                    bonusSequence.Append(TotalWinsDisplay.transform.DOScale(1.5f, BONUS_HALFLIFE));
                    length = bonusSequence.Duration();
                    Debug.LogFormat("bonus length = {0}", length);
                    bonusSequence.InsertCallback(length, () => this.StatGainSound.Play());
                    bonusSequence.Insert(length - FADE_TIME, DOTween.To(x => TotalWinsAmount.alpha = x, 1f, 0f, FADE_TIME).SetEase(Ease.InOutCubic));
                    bonusSequence.Insert(length, DOTween.To(x => TotalWinsAmount.text = playerData.wins.ToString(), 0f, 0f, 0f));
                    bonusSequence.Insert(length, DOTween.To(x => TotalWinsAmount.alpha = x, 0f, 1f, FADE_TIME).SetEase(Ease.InOutCubic));
                    bonusSequence.Insert(length, TotalWinsDisplay.transform.DOScale(1.0f, BONUS_HALFLIFE));
                    // we're adding a win

                    // add the rank up popup
                    this.TotalWinsUp.SetText("+{0}", playerData.wins - playerData.oldWins);
                    this.TotalWinsUp.gameObject.SetActive(true);
                    this.TotalWinsUp.color = IncreaseColor;
                    TotalWinsUp.alpha = 0;
                    bonusSequence.Insert(length, DOTween.To(x => TotalWinsUp.alpha = x, 0f, 1f, BONUS_HALFLIFE));
                    bonusSequence.Insert(length, this.TotalWinsUp.transform.DOBlendableLocalMoveBy(new Vector3(0, 300), BONUS_HALFLIFE * 6));
                    bonusSequence.Insert(length, this.TotalWinsUp.transform.DOScale(2.0f, BONUS_HALFLIFE * 6));
                    bonusSequence.Insert(length + BONUS_HALFLIFE * 2, DOTween.To(x => TotalWinsUp.alpha = x, 1f, 0f, BONUS_HALFLIFE * 2));
                    //bonusSequence.AppendInterval(BONUS_HALFLIFE);

                }

                if(Quest != null)
                {

                    bonusSequence.InsertCallback(length, () => this.StatGainSound.Play());
                    Debug.LogFormat("Quest Tweening: from {0} to {1}", Quest.QuestData.CurrentScore, playerData.NewQuestValue);
                    bonusSequence.InsertCallback(length, () => Quest.TweenQuestAmount(playerData.NewQuestValue));
                }

                if (playerData.oldWinStreak <= 0 || playerData.winStreak == 0 || playerData.winStreak == 1 || playerData.format == GameFormat.Practice || playerData.format == GameFormat.Tutorial || playerData.format == GameFormat.Challenge)
                {
                    WinStreakDisplay.transform.localScale = Vector3.one;
                    WinStreakObject.SetActive(playerData.winStreak > 1);
                    WinStreakAmount.text = Math.Max(0, playerData.winStreak).ToString();
                }
                else if (playerData.winStreak < 0 && playerData.oldWinStreak > 1)
                {
                    WinStreakObject.SetActive(true);
                    WinStreakDisplay.transform.localScale = Vector3.one;
                    WinStreakAmount.text = Math.Max(0, playerData.oldWinStreak).ToString();
                    bonusSequence.Append(WinStreakDisplay.transform.DOScale(1.2f, BONUS_HALFLIFE));
                    length = bonusSequence.Duration();
                    Debug.LogFormat("bonus length = {0}", length);
                    bonusSequence.Append(WinStreakDisplay.transform.DOScale(0.0f, BONUS_HALFLIFE));
                    // win streak is broken
                    // tween out, and 
                    // add the rank up popup
                    this.WinStreakUp.SetText(I2.Loc.ScriptLocalization.Streak_Broken, playerData.oldWinStreak, 0, 0);
                    this.WinStreakUp.gameObject.SetActive(true);
                    this.WinStreakUp.color = DecreaseColor;
                    WinStreakUp.alpha = 0;
                    bonusSequence.Insert(length, DOTween.To(x => WinStreakUp.alpha = x, 0f, 1f, BONUS_HALFLIFE));
                    bonusSequence.Insert(length, this.WinStreakUp.transform.DOBlendableLocalMoveBy(new Vector3(0, 300), BONUS_HALFLIFE * 6));
                    bonusSequence.Insert(length, this.WinStreakUp.transform.DOScale(2.0f, BONUS_HALFLIFE * 6));
                    bonusSequence.Insert(length + BONUS_HALFLIFE * 2, DOTween.To(x => WinStreakUp.alpha = x, 1f, 0f, BONUS_HALFLIFE * 2));
                    bonusSequence.AppendCallback(() => WinStreakObject.SetActive(false));
                    bonusSequence.AppendInterval(BONUS_HALFLIFE);
                }
                else if (playerData.winStreak > 1)
                {
                    WinStreakDisplay.transform.localScale = Vector3.one;
                    WinStreakAmount.text = Math.Max(0, playerData.winStreak - 1).ToString();
                    bonusSequence.Append(WinStreakDisplay.transform.DOScale(1.5f, BONUS_HALFLIFE));
                    length = bonusSequence.Duration();
                    Debug.LogFormat("bonus length = {0}", length);
                    bonusSequence.InsertCallback(length, () => this.StatGainSound.Play());
                    bonusSequence.Insert(length - FADE_TIME, DOTween.To(x => WinStreakAmount.alpha = x, 1f, 0f, FADE_TIME).SetEase(Ease.InOutCubic));
                    bonusSequence.Insert(length, DOTween.To(x => WinStreakAmount.text = playerData.winStreak.ToString(), 0f, 0f, 0f));
                    bonusSequence.Insert(length, DOTween.To(x => WinStreakAmount.alpha = x, 0f, 1f, FADE_TIME).SetEase(Ease.InOutCubic));
                    bonusSequence.Insert(length, WinStreakDisplay.transform.DOScale(1.0f, BONUS_HALFLIFE));
                    // win streak is above 1, and increased by 1
                    // tween the winstreak value up
                    // tween the notification
                    this.WinStreakUp.SetText("+1");
                    this.WinStreakUp.gameObject.SetActive(true);
                    this.WinStreakUp.color = IncreaseColor;
                    WinStreakUp.alpha = 0;
                    bonusSequence.Insert(length, DOTween.To(x => WinStreakUp.alpha = x, 0f, 1f, BONUS_HALFLIFE));
                    bonusSequence.Insert(length, this.WinStreakUp.transform.DOBlendableLocalMoveBy(new Vector3(0, 300), BONUS_HALFLIFE * 6));
                    bonusSequence.Insert(length, this.WinStreakUp.transform.DOScale(2.0f, BONUS_HALFLIFE * 6));
                    bonusSequence.Insert(length + BONUS_HALFLIFE * 2, DOTween.To(x => WinStreakUp.alpha = x, 1f, 0f, BONUS_HALFLIFE * 2));
                    //bonusSequence.AppendInterval(BONUS_HALFLIFE);
                }
            }
            else
            {

                var length = bonusSequence.Duration();
                if(playerData.hasWon == true)
                {
                    bonusSequence.InsertCallback(length, () => this.EventWinSound.Play());
                    bonusSequence.InsertCallback(length,() => { WinsBackground.overrideSorting = true; WinsBackground.sortingOrder = 1; });
                    bonusSequence.Insert(length, WinsBackground.transform.DOPunchScale(Vector3.one * 2.0f, BONUS_HALFLIFE * 2, vibrato: 1, elasticity: 0.5f));
                    bonusSequence.InsertCallback(length + BONUS_HALFLIFE, () => this.PlayStatusWidget._WinsDisplay.text = (playerData.tournamentData.event_wins + 1).ToString());
                    bonusSequence.AppendCallback(() => { WinsBackground.overrideSorting = false; WinsBackground.sortingOrder = 0; });
                }
                else if(playerData.hasWon == false)
                {
                    bonusSequence.InsertCallback(length, () => this.EventLossSound.Play());
                    var loss = this.PlayStatusWidget.LossArray[Math.Max(0, Math.Min(this.PlayStatusWidget.LossArray.Length - 1, playerData.tournamentData.event_losses))];
                    bonusSequence.Insert(length, DOTween.To(x => loss.alpha = x, 0f, 1f, BONUS_HALFLIFE));
                    bonusSequence.Insert(length, loss.rectTransform.DOPunchScale(Vector3.one * 2.0f, BONUS_HALFLIFE * 2, vibrato: 1, elasticity: 0.5f));
                }

            }

            // set stat values & on/off
            if (playerData.oldQps >= playerData.qps || playerData.oldQps < 0)
            {
                QPDisplay.transform.localScale = Vector3.one;
                QPObject.SetActive(playerData.qps > 0);
                QPAmount.text = Math.Max(0, playerData.qps).ToString();
            }
            else
            {
                // tween the QPs value up
                // tween the notification
                QPDisplay.transform.localScale = Vector3.one;
                QPObject.SetActive(true);
                QPAmount.text = Math.Max(0, playerData.oldQps).ToString();
                var length = bonusSequence.Duration();
                bonusSequence.Append(QPDisplay.transform.DOScale(1.5f, BONUS_HALFLIFE));
                var length2 = bonusSequence.Duration();
                bonusSequence.InsertCallback(length, () => this.StatGainSound.Play());
                bonusSequence.Insert(length, DOTween.To(x => QPAmount.text = Math.Max(0, (int)x).ToString(), playerData.oldQps, playerData.qps, BONUS_HALFLIFE * 2));
                bonusSequence.Insert(length2, QPDisplay.transform.DOScale(1.0f, BONUS_HALFLIFE));
                //length = bonusSequence.Duration();
                //bonusSequence.Insert(length - FADE_TIME, DOTween.To(x => QPAmount.alpha = x, 1f, 0f, FADE_TIME).SetEase(Ease.InOutCubic));
                //bonusSequence.Insert(length, DOTween.To(x => QPAmount.alpha = x, 0f, 1f, FADE_TIME).SetEase(Ease.InOutCubic));
                // notification

                this.QPUp.SetText("+{0}", Math.Max(0, playerData.qps) - playerData.oldQps);
                this.QPUp.gameObject.SetActive(true);
                this.QPUp.color = IncreaseColor;
                QPUp.alpha = 0;
                bonusSequence.Insert(length, DOTween.To(x => QPUp.alpha = x, 0f, 1f, BONUS_HALFLIFE));
                bonusSequence.Insert(length, this.QPUp.transform.DOBlendableLocalMoveBy(new Vector3(0, 300), BONUS_HALFLIFE * 6));
                bonusSequence.Insert(length, this.QPUp.transform.DOScale(2.0f, BONUS_HALFLIFE * 6));
                bonusSequence.Insert(length + BONUS_HALFLIFE * 2, DOTween.To(x => QPUp.alpha = x, 1f, 0f, BONUS_HALFLIFE * 2));
                //bonusSequence.AppendInterval(BONUS_HALFLIFE);
            }
            var length3 = bonusSequence.Duration();
            bonusSequence.InsertCallback(Math.Max(0.0f,length3-1.0f), () => { if (AnimationsComplete != null) AnimationsComplete.Invoke(); });
            //bonusSequence.OnComplete(() => { if (AnimationsComplete != null) AnimationsComplete.Invoke(); });
        }

        private bool isFirstSliderComplete = false;

        public void PlayRatingSliderTweens(int oldRating, int newRating)
        {
            var bracketId = EngineUtils.FindBracket(oldRating, PlayerData.RankBrackets);
            IntRange bracket;
            if (!PlayerData.RankBrackets.TryGetValue(bracketId, out bracket))
            {
                if(bracketId >= PlayerData.RankBrackets.Count)
                {
                    bracket = new IntRange { Min = - 1, Max = newRating };
                }
                else
                {
                    bonusSequence.Play();
                    return;
                }
            }

            this.RatingSlider.transform.DOScaleX(GetScale(oldRating, bracket), 0f);
            RatingText.text = bracket.Min == -1 ? oldRating.ToString() : string.Format("{0}/{1}", oldRating, bracket.Max + 1);

            SliderArea.gameObject.SetActive(true);
            // set everything to the start point

            if (oldRating == newRating)
            {
                bonusSequence.Play();
                return;
            }
            if (!isFirstSliderComplete)
            {
                isFirstSliderComplete = true;
                if (newRating > oldRating)
                {
                    this.RatingIncrease.SetText("+{0}", newRating - oldRating);
                    this.RatingIncrease.gameObject.SetActive(true);
                    this.RatingIncrease.color = IncreaseColor;
                }
                else if (newRating < oldRating)
                {
                    this.RatingIncrease.SetText("-{0}", oldRating - newRating);
                    this.RatingIncrease.gameObject.SetActive(true);
                    this.RatingIncrease.color = DecreaseColor;
                }
            }
            else
            {
                this.RatingIncrease.gameObject.SetActive(false);
            }
            this.RatingIncrease.alpha = 0;
            this.RatingIncrease.transform.localPosition = Vector3.zero;
            //this.RatingIncrease.transform.DOBlendableLocalMoveBy(new Vector3(0,-200), 0f);
            //this.RatingIncrease.transform.DOBlendableLocalMoveBy(new Vector3(0, 0), 0f);

            if (seq != null)
            {
                seq.Kill();
            }
            seq = DOTween.Sequence();
            int end = Math.Min(newRating, bracket.Max + 1);
            float sliderTweenLength = RATING_HALFLIFE * Math.Min(4, Math.Max(2, Math.Abs(end-oldRating)/25f));
            seq.Insert(0f, this.RatingSlider.transform.DOScaleX(GetScale(end, bracket), sliderTweenLength));
            seq.Insert(0f, DOTween.To(x =>
                RatingText.text = bracket.Min == -1 ? ((int)x).ToString() : string.Format(RATING_FORMAT, (int)x, bracket.Max + 1),
                oldRating, end, sliderTweenLength));
            seq.Insert(0f, DOTween.To(x => RatingIncrease.alpha = x, 0f, 1f, RATING_HALFLIFE));
            seq.Insert(0f, this.RatingIncrease.transform.DOBlendableLocalMoveBy(new Vector3(0, 300), RATING_HALFLIFE * 6));
            seq.InsertCallback(0.0f, () => this.RatingGainSound.Play());
            seq.Insert(RATING_HALFLIFE, this.RatingIncrease.transform.DOScale(2.0f, RATING_HALFLIFE * 6));
            seq.Insert(RATING_HALFLIFE * 2, DOTween.To(x => RatingIncrease.alpha = x, 1f, 0f, RATING_HALFLIFE * 2));
            //seq.Insert(ratingHalfLife * 3, this.RatingIncrease.transform.DOBlendableLocalMoveBy(new Vector3(0, -200), 0.1f));
            //seq.Insert(ratingHalfLife, this.RatingText.transform.DOScale(1.5f, ratingHalfLife).SetEase(Ease.InOutCubic));
            //seq.Insert(ratingHalfLife*2, this.RatingText.transform.DOScale(1.0f, ratingHalfLife).SetEase(Ease.InOutCubic));
            seq.PrependInterval(1f); // gap before we start playing.
            var length = seq.Duration();
            if ((oldRating < newRating && end > bracket.Max) || (oldRating > newRating && end <= bracket.Min))
            {
                seq.AppendCallback(() => PlayRankUpTweens(oldRating, newRating, bracketId));
            }
            else
                seq.InsertCallback(Math.Max(0.0f, length - 1.5f),() => bonusSequence.Play());
            //seq.Play();
        }

        public int GetRankFromRating(int rating, int oldRank)
        {
            // need to ensure this doesn't drop below Nova 1 if
            
            int bracket = EngineUtils.FindBracket(rating, PlayerData.RankBrackets);
            int rank = 30 - bracket;
            if (rank <= 0)
                rank--;
            if (rank == 0 || rank > 30)
                rank = 30;

            // Set the minimum rank
            //int minRank = oldRank <= -1 ? -1 : oldRank <= 6 ? 6 : oldRank <= 12 ? 12 : oldRank <= 18 ? 18 : 3000000;
            int minRank = oldRank <= -1 ? 0 : 300000;
            if (oldRank < 0)
                rank = Math.Min(minRank, rank);
            return rank;
        }

        public void PlayRankUpTweens(int oldRating, int newRating, int bracketId)
        {
            int thresholdRank = this.PlayerData.oldRank;
            int calcedOldRank = GetRankFromRating(oldRating, this.PlayerData.oldRank);// EngineUtils.FindBracket(oldRating, PlayerData.RankBrackets);
            int newRank = Math.Max(calcedOldRank == 1 ? -1 : calcedOldRank - 1, GetRankFromRating(newRating, this.PlayerData.oldRank));

            // handle dropping below NOVA rank (you shouldn't...)
            if (thresholdRank < 0)
                newRank = Math.Min(-1, newRank);

            if (newRank == calcedOldRank)
            {
                bonusSequence.Play();
                return;
            }
            if (oldRating > newRating)
            {
                bracketId = EngineUtils.FindBracket(newRating, PlayerData.RankBrackets);
            }

            IntRange bracket;
            if (!PlayerData.RankBrackets.TryGetValue(bracketId + 1, out bracket))
            {
                IntRange lastBracket;
                PlayerData.RankBrackets.TryGetValue(bracketId, out lastBracket);
                bracket = new IntRange { Min = lastBracket.Max + 1, Max = newRating };
            }

            if (oldRating > newRating)
            {
                RatingText.text = bracket.Min == -1 ? newRating.ToString() : string.Format(RATING_FORMAT, newRating, bracket.Max + 1);
            }

            if (seq != null)
            {
                seq.Kill();
            }
            seq = DOTween.Sequence();
            Image rankObject = calcedOldRank > 0 ? this.RankDisplay : this.LeaderboardPositionDisplay;
            TextMeshProUGUI rankAmount = calcedOldRank > 0 ? this.RankAmount : this.LeaderboardPositionAmount;

            // we're going to move the RankOffset to center screen, zoom, it, play the rank animations, zoom it back

            var pos = this.RatingText.transform.TransformPoint(this.RatingText.transform.position);
            seq.AppendCallback(() => { this.RankOffset.overrideSorting = true; this.RankOffset.sortingOrder = 5; this.RankOffset.sortingLayerName = "Overlay"; });
            seq.Append(this.RankOffset.transform.DOMove(pos, BONUS_HALFLIFE));
            seq.Join(this.RankOffset.transform.DOScale(3.0f, BONUS_HALFLIFE));
            if (newRank < calcedOldRank)
                seq.AppendCallback(() => this.RankUpSound.Play());
            var length = seq.Duration();
            // zoom the object
            // going from regular to Nova
            if (newRank < 0 && calcedOldRank > 0)
            {
                seq.Insert(length, rankObject.transform.DOScale(0.0f, FADE_HALFLIFE));
                //seq.Insert(FADE_HALFLIFE, DOTween.To(x => { rankAmount.alpha = x; rankObject.gameObject.SetActive(false); }, 1.0f, 0f, FADE_HALFLIFE));
                rankObject = this.LeaderboardPositionDisplay;
                rankAmount = this.LeaderboardPositionAmount;
                seq.Insert(length, DOTween.To(x => { rankAmount.alpha = x; rankObject.gameObject.SetActive(true); }, 0f, 1.0f, FADE_HALFLIFE));
                //seq.Insert(FADE_HALFLIFE, DOTween.To(x => rankObject.gameObject.SetActive(true),0,0,0));
                seq.Insert(length, rankObject.transform.DOScale(0.0f, 0f));
                seq.Insert(length, rankObject.transform.DOScale(1.5f, FADE_HALFLIFE));
                seq.Insert(length+FADE_HALFLIFE, rankObject.transform.DOScale(1.0f, FADE_HALFLIFE).SetEase(Ease.OutBack));
                //rankAmount.alpha = 0;
                //rankObject.transform.DOScale(1.5f, 0f);
                //rankObject.SetActive(true);
            }
            else if (newRank > 0 && calcedOldRank < 0)
            {
                seq.Insert(length, rankObject.transform.DOScale(0.0f, FADE_HALFLIFE));
                //seq.Insert(FADE_HALFLIFE, DOTween.To(x => { rankAmount.alpha = x; rankObject.gameObject.SetActive(false); }, 1.0f, 0f, FADE_HALFLIFE));
                rankObject = this.RankDisplay;
                rankAmount = this.RankAmount;
                seq.Insert(length, DOTween.To(x => { rankAmount.alpha = x; rankObject.gameObject.SetActive(true); }, 0f, 1.0f, FADE_HALFLIFE));
                //seq.Insert(FADE_HALFLIFE, DOTween.To(x => rankObject.gameObject.SetActive(true),0,0,0));
                seq.Insert(length, rankObject.transform.DOScale(0.0f, 0f));
                seq.Insert(length, rankObject.transform.DOScale(1.5f, FADE_HALFLIFE));
                seq.Insert(length + FADE_HALFLIFE, rankObject.transform.DOScale(1.0f, FADE_HALFLIFE).SetEase(Ease.OutBack));
                //rankAmount.alpha = 0;
                //rankObject.transform.DOScale(1.5f, 0f);
                //rankObject.SetActive(true);
            }
            else
            {
                seq.Insert(length, rankObject.transform.DOScale(1.5f, FADE_HALFLIFE).SetEase(Ease.OutBack));
                // quick fade out the number
                // increment the number
                seq.Insert(length+FADE_HALFLIFE - FADE_TIME, DOTween.To(x => rankAmount.alpha = x, 1f, 0f, FADE_TIME));
                // start the pip and frame animation here, so they lag.
                // quick fade in the number
                seq.Insert(length+FADE_HALFLIFE, DOTween.To(x => rankAmount.alpha = x, 0f, 1f, FADE_TIME).SetEase(Ease.InOutCubic));
                // zoom in the object
                seq.Insert(length+FADE_HALFLIFE, rankObject.transform.DOScale(1.0f, FADE_HALFLIFE).SetEase(Ease.OutBack));
            }

            // Insert the "Rank Up/Nova Rank {X}/Rank Down" text
            string novaRankText = GetNovaRankLabel(newRank);
            seq.Insert(length+FADE_HALFLIFE, DOTween.To(x => rankAmount.text = newRank > 0 ? newRank.ToString() : PlayerData.leaderboardPosition.ToString(), 0f, 0f, 0f));
            seq.InsertCallback(length + FADE_HALFLIFE, () => this.NovaRankLabel.text = novaRankText);

            int oldPips;
            int oldFrame;
            GetPipsAndFrame(calcedOldRank, out oldPips, out oldFrame);
            int newPips;
            int newFrame;
            GetPipsAndFrame(newRank, out newPips, out newFrame);
            SetPips(oldPips, newPips, seq);
            SetFrame(oldFrame, newFrame, seq);

            
            bool isRankUp = newRank < calcedOldRank;
            this.RankUp.SetText(newRank > 0 ? (isRankUp ? I2.Loc.ScriptLocalization.Rank_Up : I2.Loc.ScriptLocalization.Rank_Down) : novaRankText);
            this.RankUp.gameObject.SetActive(true);
            this.RankUp.color = isRankUp ? IncreaseColor : DecreaseColor;
            RankUp.alpha = 0;
            seq.InsertCallback(length, () => this.RankUp.transform.localPosition = Vector3.zero);
            seq.Insert(length, DOTween.To(x => this.RankUp.alpha = x, 0f, 1f, BONUS_HALFLIFE));
            seq.Insert(length, this.RankUp.transform.DOBlendableLocalMoveBy(new Vector3(0, 170), BONUS_HALFLIFE * 5));
            seq.Insert(length, this.RankUp.transform.DOScale(1.5f, BONUS_HALFLIFE * 5));
            seq.Insert(length + BONUS_HALFLIFE * 4, DOTween.To(x => this.RankUp.alpha = x, 1f, 0f, BONUS_HALFLIFE));
            
            // scale back down
            seq.Insert(length + BONUS_HALFLIFE * 5, this.RankOffset.transform.DOLocalMove(Vector3.zero, BONUS_HALFLIFE));
            seq.Join(this.RankOffset.transform.DOScale(1.0f, BONUS_HALFLIFE));
            seq.AppendCallback(() => { this.RankOffset.overrideSorting = false; });

            length = seq.Duration();
            // check for more rank ups
            if (newRating > oldRating)
                seq.AppendCallback(() => PlayRatingSliderTweens(bracket.Min, newRating));
            else
                seq.InsertCallback(Math.Max(0.0f, length-1.5f),() => bonusSequence.Play());
            //seq.Play();
        }

        public string GetNovaRankLabel(int rank)
        {
            string novaRankText;

            switch (rank)
            {
                case -1:
                    novaRankText = "I";
                    break;
                case -2:
                    novaRankText = "II";
                    break;
                case -3:
                    novaRankText = "III";
                    break;
                case -4:
                    novaRankText = "IV";
                    break;
                case -5:
                    novaRankText = "V";
                    break;
                case -6:
                    novaRankText = "VI";
                    break;
                default:
                    novaRankText = string.Empty;
                    break;
            }
            return I2.Loc.ScriptLocalization.Nova_Rank + ' ' + novaRankText;
        }

        public bool SetPlayStatus(TournamentData data, GameFormat format)
        {
            bool isTournament = data != null && (data.event_status == TournamentStatus.playing || data.event_status == TournamentStatus.drafting);
            PlayStatusPanel.SetActive(isTournament && !isVertical);
            PlayStatusWidget.Wins = data == null ? 0 : data.event_wins;
            PlayStatusWidget.Losses = data == null ? 0 : data.event_losses;

            string formatName;
            TournamentUtils.TournamentFormatNames.TryGetValue(format, out formatName);
            EventFormat.text = string.IsNullOrEmpty(formatName) ? string.Empty : formatName;


            VerticalStatusPanel.SetActive(isTournament && isVertical);
            VerticalStatusWidget.Wins = data == null ? 0 : data.event_wins;
            VerticalStatusWidget.Losses = data == null ? 0 : data.event_losses;

            // if the play status panel is active, turn some of the stats panel off.
            if(isTournament && !isVertical)
            {
                WinStreakObject.SetActive(false);
                TotalWinsObject.SetActive(false);
            }
            return isTournament;
        }



        public void GetPipsAndFrame(int rank, out int pips, out int frame)
        {
            if (rank == 0 || rank > 30 || rank < -6)
            {
                pips = -1;
                frame = 0;
            }
            else if (rank > 0)
            {
                frame = Math.Max(0, Math.Min(4, 4 - Math.DivRem(rank - 1, 6, out pips)));
                pips = 5 - pips;
            }
            else // value.rank < 0 - you're in NOVA rank!!!
            {
                pips = -rank - 1;
                frame = 5;
            }
        }


        public void SetFrame(int oldFrame, int newFrame, Sequence seq)
        {
            Sprite frameSprite;
            if (!playerFrames.TryGetValue(oldFrame, out frameSprite))
            {
                frameSprite = playerFrames[0];
            }
            Frame.sprite = frameSprite;


            // set the regular frame to on, new frame to invisible
            Frame.DOFade(1f, 0f);
            Frame.gameObject.SetActive(true);
            NewRankFrame.gameObject.SetActive(true);
            NewRankFrame.DOFade(0f, 0f);
            NewTierBackground.gameObject.SetActive(false);
            if (newFrame != oldFrame && seq != null) // NOTE - sequence is played elsewhere
            {
                // animate the frame change
                Sprite newframeSprite;
                if (!playerFrames.TryGetValue(newFrame, out newframeSprite))
                {
                    newframeSprite = playerFrames[0];
                }
                NewRankFrameImage.sprite = newframeSprite;
                NewRankFrame.transform.DOScale(2f, 0f);
                // zoom the new frame in; fade out the old frame
                seq.Insert(INSERT_OFFSET+FADE_HALFLIFE, Frame.DOFade(0f, FADE_HALFLIFE).SetEase(Ease.InOutCubic));
                seq.Insert(INSERT_OFFSET+FADE_HALFLIFE, NewRankFrame.transform.DOScale(1f, FADE_HALFLIFE *2).SetEase(Ease.OutBounce));
                seq.Insert(INSERT_OFFSET, NewRankFrame.DOFade(1f, FADE_HALFLIFE * 2.0f).SetEase(Ease.InOutCubic));

                // Animate a "Rank up" banner
                if(newFrame > oldFrame)
                {
                    NewTierBackground.gameObject.SetActive(true);
                    Color background;
                    switch(newFrame)
                    {
                        case 5:
                            background = new Color(111/255f, 69/255f, 128/255f);
                            NewTierText.text = I2.Loc.ScriptLocalization.Nova_Achieved;
                            break;
                        case 4:
                            background = new Color(28/255f, 108/255f, 120/255f);
                            NewTierText.text = I2.Loc.ScriptLocalization.Diamond_Achieved;
                            break;
                        case 3:
                            background = new Color(92/255f, 186/255f, 61/255f);
                            NewTierText.text = I2.Loc.ScriptLocalization.Platinum_Achieved;
                            break;
                        case 2:
                            background = new Color(208/255f, 146/255f, 0);
                            NewTierText.text = I2.Loc.ScriptLocalization.Gold_Achieved;
                            break;
                        case 1:
                            background = new Color(150/255f, 150/255f, 150/255f);
                            NewTierText.text = I2.Loc.ScriptLocalization.Silver_Achieved;
                            break;
                        default:
                            background = new Color(255/255f, 150/255f, 0);
                            NewTierText.text = I2.Loc.ScriptLocalization.Bronze_Tier;
                            break;
                    }
                    NewTierBackground.color = background;
                    seq.AppendInterval(1.5f);
                }
            }
        }


        public void SetPips(int oldPips, int newPips, Sequence seq)
        {
            if (oldPips == 5 && newPips == 0)
                oldPips = -1;
            if (newPips == 5 && oldPips == 0)
                newPips = -1;
            for (int i = 0; i < RatingPips.Length; i++)
            {
                RatingPips[i].gameObject.SetActive(i <= oldPips);
            }
            if (newPips > oldPips && seq != null) // NOTE - sequence is played elsewhere
            {
                float fadeTime = 0.3f;
                for (int i = Math.Max(0, Math.Min(RatingPips.Length - 1, oldPips+1)); i <= Math.Min(RatingPips.Length, newPips); i++)
                {
                    var pip = RatingPips[i];
                    pip.gameObject.SetActive(true);
                    seq.Insert(0f, pip.DOFade(0f, 0f));
                    seq.Insert(0f, pip.DOFade(1.0f, fadeTime));
                    seq.Insert(0f, pip.transform.DOScale(2.0f, FADE_HALFLIFE).SetEase(Ease.OutBack));
                    seq.Insert(FADE_HALFLIFE, pip.transform.DOScale(1.0f, FADE_HALFLIFE).SetEase(Ease.OutBack));
                }
                // tween the new pip(s)
            }
            else if (newPips < oldPips && seq != null)
            {
                if (newPips == -1)
                    newPips = 5;
                float fadeTime = 0.3f;
                for (int i = 0; i < RatingPips.Length; i++)
                {
                    var pip = RatingPips[i];
                    seq.Insert(0f, pip.DOFade(i > newPips ? 0.0f : 1.0f, fadeTime));
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            playerFrames[0] = PlayerFrames.Bronze;
            playerFrames[1] = PlayerFrames.Silver;
            playerFrames[2] = PlayerFrames.Gold;
            playerFrames[3] = PlayerFrames.Platinum;
            playerFrames[4] = PlayerFrames.Diamond;
            playerFrames[5] = PlayerFrames.Nova;
        }

        public void Initialize(PlayerData data)
        {
            PlayerData = data;
        }

        public void SetCardBackArt(string artId)
        {
            Texture tex = AssetBundleManager.Instance.LoadAsset<Texture>(artId);
            //Texture tex = (Texture)Resources.Load(artId);
            if (tex == null)
            {
                tex = AssetBundleManager.Instance.LoadAsset<Texture>("BasicCardBack");
                //tex = (Texture)Resources.Load("BasicCardBack");
            }
            CardBack.texture = tex;
        }

        public void OnCardBackClick()
        {
            if(ClickCardBack != null)
            {
                ClickCardBack.Invoke();
            }
        }

        public void OnAvatarClick()
        {
            if (ClickAvatar != null)
            {
                ClickAvatar.Invoke();
            }
        }

        public void OnGameLogClick()
        {
            if (ClickGameLog != null)
            {
                ClickGameLog.Invoke();
            }
        }

        public void OnRedeemClick()
        {
            if(ClickRedeem != null)
            {
                ClickRedeem.Invoke();
            }
        }
        
        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Quest != null)
            {
                Quest.Recycle();
                Quest = null;
            }
            AvatarArt.texture = null;
        }
    }

    public class PlayerData
    {
        public string playFabId { get; set; }
        public string avatarArtId { get; set; }
        public string cardBackArtId { get; set; }
        public string displayName { get; set; }
        public int oldRating;
        public int rating;
        public int qps;
        public int oldQps;
        public int winStreak;
        public int oldWinStreak;
        public int rank;
        public int oldRank;
        public int wins;
        public int oldWins;
        public int leaderboardPosition;
        public int oldLeaderboardPosition;
        public bool? hasWon;
        public TournamentData tournamentData { get; set; }
        public GameFormat format { get; set; }
        public Dictionary<int,IntRange> RankBrackets { get; set; }
        public QuestData Quest;
        public int NewQuestValue;

        public PlayerData() { }

        public PlayerData(string playFabId, string name, Dictionary<string,int> userStats, Dictionary<int, IntRange> rankBrackets,
            string avatarArtId, string cardBackArtId, int qps, TournamentData data, GameFormat format, int oldRating, int oldRank, int oldLeaderboardPosition, int oldQps, int oldWins,
            int oldWinStreak, bool? hasWon, QuestData quest, int newQuestValue)
        {
            this.RankBrackets = rankBrackets;
            this.playFabId = playFabId;
            this.displayName = name;
            this.avatarArtId = avatarArtId;
            this.cardBackArtId = cardBackArtId;
            this.qps = qps;
            this.tournamentData = data;
            this.format = format;
            this.oldLeaderboardPosition = oldLeaderboardPosition;
            this.oldWinStreak = oldWinStreak;
            this.oldWins = oldWins;
            this.oldQps = oldQps;
            this.hasWon = hasWon;
            this.Quest = quest;
            this.NewQuestValue = newQuestValue;

            userStats.TryGetValue(NovaAchievements.CURRENT_WIN_STREAK, out this.winStreak);
            userStats.TryGetValue(NovaAchievements.GAMES_WON, out this.wins);
            userStats.TryGetValue(NovaAchievements.LEADERBOARD, out this.leaderboardPosition);
            userStats.TryGetValue(NBEconomy.PLAYER_STATS_RANK, out rank);
            userStats.TryGetValue(NBEconomy.PLAYER_STATS_RATING, out rating);

            this.oldRating = oldRating;
            this.oldRank = oldRank;
            
        }
    }
}
