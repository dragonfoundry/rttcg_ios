﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using System;
using LinqTools;
using TMPro;
using DragonFoundry.Fx;
using GameFormat = Messages.GameFormat;

namespace NovaBlitz.UI
{
	public class DeckListDockView : AnimatedView, IPoolView
	{
		[Inject] public ViewReferences ViewReferences {get; set;}
		[Inject] public ShowTwoTouchSignal ShowTwoTouchSignal {get;set;}
		[Inject] public CloseTwoTouchViewSignal CloseTwoTouchViewSignal {get;set;}
		public event Action<GameFormat> EventFormatChanged;

		public DeckData DeckData {get;private set;}
		public ScrollRect ScrollRect;
		public Scrollbar Scrollbar;
		public TextMeshProUGUI DeckNameLabel;
        public Button DeckButton;
		public Button EditButton;
        public ParticleEffectsTrigger PracticeButtonFx;
        public ParticleEffectsTrigger EditButtonFx;
		public Action OnCloseAnimationFinished;
		public event Action EventEditClicked;
		public event Action EventStatsClicked;

		public TextMeshProUGUI CardStatus;
		public CardAspectsIndicator AspectsIndicator;

        [HideInInspector] public CardListItemView CardListItemPrefab;
		[HideInInspector] public DraggableCardView DraggableCardPrefab;
		[SerializeField] protected GridLayoutGroup _Grid;
		protected RectTransform _GridRX;

		[Inject] public IDragOverlay DragOverlay {get;set;}
		public TwoTouchOverlayView TwoTouchOverlay {get;set;}

		protected List<DraggableCardView> _CardInstances = new List<DraggableCardView>();
		public IEnumerable<DraggableCardView> Cards { get { return _CardInstances.ToArray(); } }
        
		public bool IsEditable { set { this.EditButton.gameObject.SetActive(value); } }

		protected GameFormat _Format = GameFormat.Casual;
		public GameFormat Format {
			get { return _Format; }
			set { _Format = value; SwitchFormat(value); }
		}

		protected void SwitchFormat(GameFormat format)
		{
			if (null != this.EventFormatChanged)
				this.EventFormatChanged.Invoke(format);
		}

		public void Recycle()
		{
			if (null != _GridRX) {
				foreach (var dc in _CardInstances.Where(i => null != i)) {
					dc.DespawnCardDisplay();
					dc.Recycle ();
					//GameObject.Destroy(dc.gameObject);
					//ObjectPool.Recycle<DraggableCardView>(dc);
				}
			}
			_CardInstances.Clear();
		}

		#region Mono
		protected override void Awake()
		{
			_GridRX = (RectTransform)_Grid.transform;
			base.Awake();
        }

		protected override void Start()
		{
			base.Start();
			var cardListItemRX = this.CardListItemPrefab.GetComponent<RectTransform>();
			_Grid.cellSize = cardListItemRX.sizeDelta;
		}
		#endregion


		public void ClickStatsButton()
		{
			if (null != this.EventStatsClicked)
				this.EventStatsClicked.Invoke();
		}

		public void ClickEdit()
		{
			if (null != this.EventEditClicked)
				this.EventEditClicked.Invoke();
		}

		///<summary>
		/// Initializes the Dock to display the decks cards
		///</summary>
		public void Initialize(DeckData deckData, Dictionary<int, int> ownedCards, HashSet<int> legalCards)
		{
            if (ownedCards != null && legalCards != null)
            {
                deckData.TrimToInventory(ownedCards, legalCards);
            }
            this.DeckData = deckData;
			this.DeckNameLabel.text = this.DeckData.Name;
            
            SetCardStatus();

            this.DeckData.QueryAspects();
            this.DeckData.SetDeckArt();
			this.AspectsIndicator.ShowIcons(this.DeckData.Aspects);

			// Clear out any existing cardList items
			this.Recycle();

			// Get the decklist
			List<CardListItem> cardListItems = CardListTransform.GetList(deckData);

			// Loop though te decklist adding it to the deck
			for(int i=0;i<cardListItems.Count; i++)
			{
				CardListItem cardListItem = cardListItems[i];
				CardListItemView cardListItemView = this.AddCardItemToList(cardListItem, i, deckData.MaxOfEachCard);
                /*var data = cardListItemView.CardData;
                cardListItemView.CardData = null;
                cardListItemView.CardData = data;*/
                cardListItemView.Overlay.DOFade(0.0f, 0.0f);
                //TweenAlpha.Begin(cardListItemView.Overlay.gameObject, 0f, 0f);
			}
		}

        public void SetCardStatus()
        {
            if (this.DeckData == null)
                return;

            if (!this.DeckData.IsLegalDeck)
            {
                this.CardStatus.color = new Color(242 / 256f, 89 / 256f, 53 / 256f);
                this.CardStatus.text = string.Format("{0}/{1} {2}", this.DeckData.Cards.Count, this.DeckData.Cards.Count > this.DeckData.MaxDeckSize ? this.DeckData.MaxDeckSize : this.DeckData.MinDeckSize, I2.Loc.ScriptLocalization.NotLegal);
            }
            else
            {
                this.CardStatus.color = new Color(158 / 256f, 251 / 256f, 255 / 256f);
                this.CardStatus.text = string.Format("{0}/{1}", this.DeckData.Cards.Count, this.DeckData.MinDeckSize);
            }
        }
		
		///<summary>
		/// Adds a card list item to tohe end of the list
		///</summary>
		public CardListItemView AddCardItemToList(CardListItem cardListItem, int index, int maxOfEachCard)
		{
			if(this.TwoTouchOverlay == null)
			{
				this.TwoTouchOverlay = (TwoTouchOverlayView) this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			}

			var cardData = GameData.CardDictionary[cardListItem.CardID];
			DraggableCardView draggable = UITools.PoolSpawnRect(this.DraggableCardPrefab, Vector3.zero, _GridRX);
			_CardInstances.Add(draggable);
			//draggable.RectTransform.localPosition = Vector3.zero;
			draggable.DragOverlay = this.DragOverlay;
			draggable.ShowTwoTouchCallback = OnShowTwoTouch;
			draggable.DismissTwoTouchCallback = OnDismissTwoTouch;
			draggable.IsLocked = true;
			draggable.IsEditable = false;
            draggable.DragItem.Constraint = DragConstraint.Horizontal;

            CardListItemView itemView = draggable.InitCardDisplay(this.CardListItemPrefab, cardData);
			itemView.CardArtImage.texture = null;
			if (index < _CardInstances.Count) { draggable.RectTransform.SetSiblingIndex(index); }
			itemView.Initialize(cardListItem, cardData, maxOfEachCard);

			return itemView;
		}

		private void OnShowTwoTouch(TwoTouchParams twoTouchParams)
		{
			this.ShowTwoTouchSignal.Dispatch(twoTouchParams);
		}

		private void OnDismissTwoTouch()
		{
			this.CloseTwoTouchViewSignal.Dispatch();
		}
		
		public void OnDeckListCloseAnimationFinished()
		{
			if(this.OnCloseAnimationFinished != null)
			{
				this.OnCloseAnimationFinished.Invoke();
			}
		}

	}
}
