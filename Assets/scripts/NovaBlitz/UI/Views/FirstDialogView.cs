﻿using System;
using UnityEngine;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
    public class FirstDialogView : AnimatedView
    {
        public Action ClickLogin;
        public Action ClickNewAccount;
        public Action LanguageButtonClicked;
        public Action EulaClicked;
        public Action PrivacyClicked;
        public Action TermsClicked;

        public void OnClickLogin()
        {
            if (this.ClickLogin != null)
            {
                this.ClickLogin.Invoke();
            }
        }

        public void OnClickNewAccount()
        {
            if (this.ClickNewAccount != null)
            {
                this.ClickNewAccount.Invoke();
            }
        }

        /// <summary>
        /// Allows the player to change langauge from the login screen
        /// </summary>
        public void OnLanguageButonClick()
        {
            if (this.LanguageButtonClicked != null)
            {
                this.LanguageButtonClicked.Invoke();
            }
        }



        public void OnClickEULA()
        {
            if (this.EulaClicked != null)
            {
                this.EulaClicked.Invoke();
            }
        }

        public void OnClickPrivacy()
        {
            if (this.PrivacyClicked != null)
            {
                this.PrivacyClicked.Invoke();
            }
        }

        public void OnClickTerms()
        {
            if (this.TermsClicked != null)
            {
                this.TermsClicked.Invoke();
            }
        }

    }

    public class FirstDialogMediator : Mediator
    {
        [Inject] public FirstDialogView View { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }
        [Inject] public LoginWithDeviceOrSteamSignal LoginWithDeviceOrSteamSignal { get; set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.ClickLogin += OnLogin;
            this.View.ClickNewAccount += OnNewAccount;
            this.View.EulaClicked += OnEulaClicked;
            this.View.PrivacyClicked += OnPrivacyClicked;
            this.View.TermsClicked += OnTermsClicked;
            this.View.LanguageButtonClicked += this.OnLanguageButtonPress;
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.ClickLogin -= OnLogin;
            this.View.ClickNewAccount -= OnNewAccount;
            this.View.EulaClicked -= OnEulaClicked;
            this.View.PrivacyClicked -= OnPrivacyClicked;
            this.View.TermsClicked -= OnTermsClicked;
            this.View.LanguageButtonClicked -= this.OnLanguageButtonPress;
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {

            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {

            }
        }

        private void OnLogin()
        {
            // go to the login dialog - enter email, password, etc.

            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());

            this.InitializeAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView), (v) => {
                ((LoginDialogNewFlowView)v).Initialize(new LoginDialogSettings
                {
                    IsAddUsernameAndPassword = false,
                    IsAgeGatePassed = this.PlayerProfile.isAgeGatePassed,
                    IsCancelable = true,
                    SteamName = this.ClientPrefs.SteamName,
                });
            });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));
        }

        private void OnNewAccount()
        {
            // create account behind the scenes; choose a displayname

            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
            this.LoginWithDeviceOrSteamSignal.Dispatch(true);
        }
        
        /// <summary>
        /// Allows the user to change the langauge on the login screen, if it's not in the right langauge for them
        /// </summary>
        public void OnLanguageButtonPress()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LanguageSelectorView));
        }


        private void OnEulaClicked()
        {
            Application.OpenURL(this.GameData.url_EULA);
        }

        private void OnPrivacyClicked()
        {
            Application.OpenURL(this.GameData.url_Privacy);
        }

        private void OnTermsClicked()
        {
            Application.OpenURL(this.GameData.url_Terms);
        }
    }
}


