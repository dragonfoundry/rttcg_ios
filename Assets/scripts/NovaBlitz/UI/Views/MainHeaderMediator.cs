﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.UI;
using LinqTools;
using NovaBlitz.Economy;
using Messages;
using DG.Tweening;

namespace NovaBlitz.UI.Mediators
{
	public class MainHeaderMediator : Mediator
	{
		[Inject] public MainHeaderView View {get;set;}
		[Inject] public AccountInfoLoadedSignal AccountInfoLoadedSignal {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
        [Inject] public FriendNotificationSignal FriendNotificationSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public GameData GameData { get; set; }
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
		[Inject] public MetagameSettings MetagameSettings {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}


		public override void OnRegister()
		{
            this.View.SetFrames();
            View.EventProfileClicked += OnHeader_ProfileClicked;
            View.EventSettingsClicked += OnHeader_SettingsClicked;
            View.EventCreditsClicked += OnHeader_CreditsClicked;
            View.EventSmartPacksClicked += OnHeader_SmartPacksClicked;
            View.EventGemsClicked += OnHeader_GemsClicked;

            this.AccountInfoLoadedSignal.AddListener(OnAccountInfoLoaded);
			this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
            RefreshMainHeader();
            RefreshFriendInfo(new FriendNotification { numberOnlineFriends = 0, isChallengeRequest = false, isChatMessage = false, isFriendRequest = false });
            this.FriendNotificationSignal.AddListener(RefreshFriendInfo);
            this.LanguageChangedSignal.AddListener(RefreshMainHeader);
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

		public override void OnRemove()
        {
            View.EventProfileClicked -= OnHeader_ProfileClicked;
            View.EventSettingsClicked -= OnHeader_SettingsClicked;
            View.EventCreditsClicked -= OnHeader_CreditsClicked;
            View.EventSmartPacksClicked -= OnHeader_SmartPacksClicked;
            View.EventGemsClicked -= OnHeader_GemsClicked;
		
			this.AccountInfoLoadedSignal.RemoveListener(OnAccountInfoLoaded);
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
            this.FriendNotificationSignal.RemoveListener(RefreshFriendInfo);
            this.LanguageChangedSignal.RemoveListener(RefreshMainHeader);
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            if (viewType == typeof(OfferView) || viewType == typeof(StoreView) || viewType == typeof(PackOpenerConfirmView) || viewType == typeof(AvatarSelectorView) || viewType == typeof(CardBackSelectorView) || viewType == typeof(ConfirmVirtualPurchaseDialogView))
            {
                //Debug.LogFormat("Header to overlay due to: {0}", viewType);
                // Make sure we go to the overlay layer with the TwoTouch
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
            }
            else
            {

                //Debug.LogFormat("Header to main due to: {0}", viewType);
                this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Main, 0.0f);
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if (viewType == this.View.GetType())
            {

            }
            
        }

        private void RefreshMainHeader()
		{
			if (null != this.PlayerProfile.MetaProfile)
				this.View.UserName = this.PlayerProfile.MetaProfile.Name;
            if (null != this.PlayerProfile.UserStatistics)
            {

                int leaderboardPosition;
                this.PlayerProfile.UserStatistics.TryGetValue(NovaAchievements.LEADERBOARD, out leaderboardPosition);
                this.View.LeaderboardPositionAmount = leaderboardPosition;
                //this.PlayerProfile.UpdateRankAndRating();
                this.View.RankAmount = this.PlayerProfile.CurrentRank;
                //var bracket = EngineUtils.GenerateLevelUpSteps(PlayerProfile.CurrentRating, PlayerProfile.CurrentRating, this.GameData.RankBrackets).Last();
                //this.View.RankSlider.minValue = bracket.BracketRange.Min - bracket.BracketRange.Max * 0.1f;
                //this.View.RankSlider.maxValue = bracket.BracketRange.Max;
                //this.View.RankSlider.value = PlayerProfile.CurrentRating;
            }
			if (null != this.PlayerProfile.Wallet) {
				this.View.CreditsAmount = this.PlayerProfile.Wallet.Currencies[CurrencyType.CR];
				this.View.GemsAmount = this.PlayerProfile.Wallet.Currencies[CurrencyType.NG];
				this.View.PacksAmount = this.PlayerProfile.Wallet.Currencies[CurrencyType.SP];
            }
            if(null != this.MetagameSettings)
            {
                this.View.IsTargetingDev = this.MetagameSettings.IsTargetingDev;
            }
            this.View.VersionLabel.text = this.GameData.VersionLabel;
		}


        protected void RefreshFriendInfo(FriendNotification notification)
        {
            // # online

            // chat notifications

            // challenge notifications

            // friend requests
            if (notification.numberOnlineFriends != null)
                this.View.OnlineFriendCount.SetText("{0}/{1}",notification.numberOnlineFriends.Value, notification.numberOfFriends);
            
            if(notification.isChallengeRequest != null)
            { 
                this.View.ChallengeNotification.gameObject.SetActive((bool)notification.isChallengeRequest);
                this.View.ChallengeNotification.transform.DOKill();
                this.View.ChallengeNotification.transform.localScale = Vector3.one;
                this.View.ChallengeNotification.transform.DOPunchScale(Vector3.one * 0.3f, 3.7f, vibrato: 1, elasticity: 0.25f).SetLoops(-1);
            }

            if (notification.isChatMessage != null)
            { 
				this.View.ChatNotification.gameObject.SetActive((bool)notification.isChatMessage && this.PlayerProfile.isAgeGatePassed == true);

                this.View.ChatNotification.transform.DOKill();
                this.View.ChatNotification.transform.localScale = Vector3.one;
                this.View.ChatNotification.transform.DOPunchScale(Vector3.one * 0.3f, 3.7f, vibrato: 1, elasticity: 0.25f).SetLoops(-1);
            }

            if (notification.isFriendRequest != null)
            {
                this.View.FriendRequestNotification.gameObject.SetActive((bool)notification.isFriendRequest);

                this.View.FriendRequestNotification.transform.DOKill();
                this.View.FriendRequestNotification.transform.localScale = Vector3.one;
                this.View.FriendRequestNotification.transform.DOPunchScale(Vector3.one * 0.3f, 3.7f, vibrato: 1, elasticity: 0.25f).SetLoops(-1);
            }

            this.View.FriendsLabel.gameObject.SetActive(
                !this.View.ChallengeNotification.gameObject.activeSelf
                && !this.View.ChatNotification.gameObject.activeSelf
                && !this.View.FriendRequestNotification.gameObject.activeSelf);
        }

        protected void OnAccountInfoLoaded()
		{
			RefreshMainHeader();
		}

		void OnProfileUpdated(ProfileSection section)
		{
			if (section == ProfileSection.Currency || section == ProfileSection.Statistics)
				RefreshMainHeader();
		}

        protected void OnHeader_ProfileClicked()
        {
            ToProfile();
        }
        protected void ToProfile()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ProfileMenuView));
            var profileMenu = (ProfileMenuView)ViewReferences.Get(typeof(ProfileMenuView));
            if (profileMenu != null)
                profileMenu.OpenPlayerTab();
        }

        protected void OnHeader_SettingsClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(SettingsView));
            /*var profileMenu = (ProfileMenuView)ViewReferences.Get(typeof(ProfileMenuView));
            if (profileMenu != null)
    			profileMenu.OpenSettingsTab();*/
        }

        protected void OnHeader_CreditsClicked()
        {
            OpenStore(CurrencyType.CR);
        }

        protected void OnHeader_SmartPacksClicked()
        {
            OpenStore(CurrencyType.SP);
        }

        protected void OnHeader_GemsClicked()
        {
            OpenStore(CurrencyType.NG);
        }

        protected void OpenStore(CurrencyType currency)
        {
            var twoTouch = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
            if (twoTouch != null)
            {
                this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
            }
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(StoreView));
            var StoreView = ViewReferences.Get<StoreView>();
            if (StoreView != null)
            {
                if (currency == CurrencyType.NG)
                    StoreView.ScrollRect.verticalNormalizedPosition = 0.0f;
                else
                    StoreView.ScrollRect.verticalNormalizedPosition = 0.5f;
            }
        }
    }
}
