﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.mediation.impl;
using TMPro;
using NovaBlitz.Economy;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
    public class LeaderboardView : AnimatedView
    {
        public Action RefreshEvent;
        public Action CloseEvent;
        public MultiLeaderboardItem OverallLeaderboardDisplay;
        public MultiLeaderboardItem FriendLeaderboardDisplay;

        public List<PlayerLeaderboardEntry> OverallLeaderboard { get; set; }
        public List<PlayerLeaderboardEntry> FriendLeaderboard { get; set; }
        public LeaderboardType LeaderboardType { get; set; }

        protected override void Awake()
        {
            //this.IsOpen = false;
            base.Awake();
        }

        public void OnScrimClicked()
        {
            if (CloseEvent != null)
                CloseEvent.Invoke();
        }

        public void Initialize(List<PlayerLeaderboardEntry> overallLeaderboard, List<PlayerLeaderboardEntry> friendLeaderboard, LeaderboardType type)
        {

            ClearLeaderboards();
            LeaderboardType = type;
            OverallLeaderboard = overallLeaderboard;
            FriendLeaderboard = friendLeaderboard;
            if(RefreshEvent != null)
                RefreshEvent.Invoke();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ClearLeaderboards();
        }

        public void ClearLeaderboards()
        {
            OverallLeaderboardDisplay.Purge();
            FriendLeaderboardDisplay.Purge();
            if(OverallLeaderboard != null)
                OverallLeaderboard.Clear();
            if(FriendLeaderboard != null)
                FriendLeaderboard.Clear();
        }
    }

    [Serializable]
    public class MultiLeaderboardItem : MultiRect<LeaderboardItemView, PlayerLeaderboardEntry>
    {
        public LeaderboardType type { get; set; }
        public Dictionary<int, IntRange> rankBrackets { get; set; }
        protected override void Initialize(LeaderboardItemView instance, PlayerLeaderboardEntry data) { instance.type = type; instance.rankBrackets = rankBrackets; instance.LeaderboardEntry = data;   }
    }


    public enum LeaderboardType
    {
        NoType = 0,
        Event = 1,
        Ranking =2,
    }
}

namespace NovaBlitz.UI.Mediators
{
    public class LeaderboardMediator : Mediator
    {
        [Inject] public LeaderboardView View { get; set; }
        [Inject] public UIConfiguration UIConfiguration { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            View.RefreshEvent += RefreshView;
            View.CloseEvent += CloseView;
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            //this.RefreshView();
        }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            View.RefreshEvent -= RefreshView;
            View.CloseEvent -= CloseView;
            this.AnimatedViewOpenedSignal.RemoveListener(OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                // Refresh the view databinding
                //this.RefreshView();
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {

            }
        }

        private void RefreshView()
        {
            if (View.OverallLeaderboard != null)
            {
                View.OverallLeaderboardDisplay.type = View.LeaderboardType;
                View.OverallLeaderboardDisplay.rankBrackets = GameData.RankBrackets;
                View.OverallLeaderboardDisplay.Populate(View.OverallLeaderboard);
                foreach (var view in View.OverallLeaderboardDisplay.Instances)
                {
                    view.EventClicked = OnView_ItemClicked;
                    if (view.LeaderboardEntry.PlayFabId == this.PlayerProfile.MetaProfile.PlayFabID)
                        view.Highlight.gameObject.SetActive(true);
                }
            }
            if (View.FriendLeaderboard != null)
            {
                View.FriendLeaderboardDisplay.type = View.LeaderboardType;
                View.FriendLeaderboardDisplay.rankBrackets = GameData.RankBrackets;
                View.FriendLeaderboardDisplay.Populate(View.FriendLeaderboard);
                foreach (var view in View.FriendLeaderboardDisplay.Instances)
                {
                    view.EventClicked = OnView_ItemClicked;
                    if (view.LeaderboardEntry.PlayFabId == this.PlayerProfile.MetaProfile.PlayFabID)
                        view.Highlight.gameObject.SetActive(true);
                }
            }
        }


        public void CloseView()
        {
            this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
        }

        protected void OnView_ItemClicked(LeaderboardItemView item)
        {
            // get the decklist for the player

            // Show the decklist for the player.

            // Make the cards in the decklist clickable.
        }
    }
}
