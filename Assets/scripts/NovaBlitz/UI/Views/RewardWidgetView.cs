﻿using System;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;

namespace NovaBlitz.UI
{
    public class RewardWidgetView : View
    {
        public Action CollectAction;

        public CanvasGroup ThisCanvasGroup;
        public LayoutElement[] DailyRewards;
        public Image[] DailyRewardCheckMarks { get; set; }
        public Image[] DailyRewardFrames { get; set; }
        public Image[] DailyRewardIcons { get; set; }
        public TextMeshProUGUI[] DailyRewardAmounts { get; set; }
        public TextMeshProUGUI[] DailyRewardDays { get; set; }
        public TextMeshProUGUI HeaderText;
        public TextMeshProUGUI CollectPlaceholder;
        public NovaButton CollectButton;

        public Material GreyScaleMaterial;
        public Sprite ActiveBox;
        public Sprite InactiveBox;

        
        public static Color grey = new Color { r = 0.8f, g = 0.8f, b = 0.8f, a = 1 };
        public static Color orange = new Color { r = 1f, g = 0.6f, b = 0.0f, a = 1 };
        public static Color green = new Color { r = 0.0f, g = 1.0f, b = 0.25f, a = 1 };
        public static Color purple = new Color { r = 1.0f, g = 0.5f, b = 1.0f, a = 1 };
        public const int SMALL = 150;
        public const int LARGE = 200;

        private bool IsInitialized = false;
        private bool IsCollect;

        public void OnClickCollect()
        {
            if(CollectAction != null)
            {
                CollectAction.Invoke();
            }
        }

        public void Initialize(Messages.OnboardingProgress progress, bool isCollect, bool isDLC)
        {
            CollectPlaceholder.text = isDLC ? I2.Loc.ScriptLocalization.DLC_Explanation : I2.Loc.ScriptLocalization.Daily_Rewards_Placeholder;
            HeaderText.text = isDLC ? I2.Loc.ScriptLocalization.DLC_Unlocked : I2.Loc.ScriptLocalization.Login_Rewards;
            IsCollect = isCollect;
            if (!IsInitialized)
            {
                int rewardCount = DailyRewards.Count();
                DailyRewardCheckMarks = new Image[rewardCount];
                DailyRewardFrames = new Image[rewardCount];
                DailyRewardIcons = new Image[rewardCount];
                DailyRewardAmounts = new TextMeshProUGUI[rewardCount];
                DailyRewardDays = new TextMeshProUGUI[rewardCount];
                for (int i = 0; i < rewardCount; i++)
                {
                    var images = DailyRewards[i].GetComponentsInChildren<Image>();
                    var text = DailyRewards[i].GetComponentsInChildren<TextMeshProUGUI>();
                    DailyRewardFrames[i] = images[0];
                    DailyRewardIcons[i] = images[1];
                    DailyRewardCheckMarks[i] = images[2];
                    DailyRewardAmounts[i] = text[0];
                    DailyRewardDays[i] = text[1];
                }
                IsInitialized = true;
            }
            SetDailyRewardLevel(progress != null ? progress.DailyLoginStreak : 0);
            CollectButton.gameObject.SetActive(isCollect);
        }

        public void SetDailyRewardLevel(int level)
        {
            // Hack to show you the current, not next, one.
            if (IsCollect)
            {
                level = level - 1;
            }
            else if(level > 6)
            {
                level = 0;
            }

            for (int i = 0; i < DailyRewards.Count(); i++)
            {
                var element = DailyRewards[i];
                if (i < level)
                {
                    DailyRewardCheckMarks[i].gameObject.SetActive(true);
                    DailyRewardFrames[i].sprite = InactiveBox;
                    DailyRewardFrames[i].material = null;
                    DailyRewardIcons[i].material = null;
                    DailyRewardDays[i].color = green;
                    DailyRewardAmounts[i].color = i == 0 || i == 2 || i == 4 ? purple : orange;
                    element.minHeight = SMALL;
                    element.minWidth = SMALL;
                    DailyRewardDays[i].SetText(I2.Loc.ScriptLocalization.Reward_Day, i + 1);
                }
                else if (i == level)
                {
                    DailyRewardCheckMarks[i].gameObject.SetActive(false);
                    DailyRewardFrames[i].sprite = ActiveBox;
                    DailyRewardFrames[i].material = null;
                    DailyRewardIcons[i].material = null;
                    DailyRewardDays[i].color = green;
                    DailyRewardAmounts[i].color = i == 0 || i == 2 || i == 4 ? purple : orange;
                    element.minHeight = LARGE;
                    element.minWidth = LARGE;
                    DailyRewardDays[i].text = IsCollect ? I2.Loc.ScriptLocalization.Today : I2.Loc.ScriptLocalization.Tomorrow;
                }
                else
                {
                    DailyRewardCheckMarks[i].gameObject.SetActive(false);
                    DailyRewardFrames[i].sprite = InactiveBox;
                    DailyRewardFrames[i].material = GreyScaleMaterial;
                    DailyRewardIcons[i].material = GreyScaleMaterial;
                    DailyRewardDays[i].color = grey;
                    DailyRewardAmounts[i].color = grey;
                    element.minHeight = SMALL;
                    element.minWidth = SMALL;
                    DailyRewardDays[i].SetText(I2.Loc.ScriptLocalization.Reward_Day, i + 1);
                }
            }
        }
    }
}
