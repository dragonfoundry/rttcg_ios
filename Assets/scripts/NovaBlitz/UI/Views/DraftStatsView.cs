﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;

namespace NovaBlitz.UI
{
    public class DraftStatsView : View
    {
        public CardAspectsIndicator AspectsIndicator;
        public DeckEnergyGraphWidget EnergyGraph;

        public DeckData DeckData { get; set; }
                
        public void Refresh()
        {
            if (null == this.DeckData)
                return;
            
            this.AspectsIndicator.ShowIcons(this.DeckData.Aspects);

            this.EnergyGraph.DeckData = this.DeckData;
            this.EnergyGraph.Refresh();
            
        }
    }
}

namespace NovaBlitz.UI.Mediators
{
    public class DraftStatsMediator : Mediator
    {
        [Inject]
        public DraftStatsView View { get; set; }

        [Inject]
        public PlayerProfile PlayerProfile { get; set; }
        [Inject]
        public DeckSelectedSignal DeckSelectedSignal { get; set; }
        [Inject]
        public CardAddedToDeckSignal CardAddedToDeckSignal { get; set; }
        [Inject]
        public CardRemovedFromDeckSignal CardRemovedFromDeckSignal { get; set; }
        

        [Inject]
        public ViewReferences ViewReferences { get; set; }
        [Inject]
        public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject]
        public ShowNewMessageDialogSignal ShowNewMessageDialogSignal { get; set; }

        public override void OnRegister()
        {
        }

        public override void OnRemove()
        {
        }
    }
}
