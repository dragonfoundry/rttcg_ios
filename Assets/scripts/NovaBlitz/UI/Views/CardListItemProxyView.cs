﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using DG.Tweening;
using I2.Loc;

namespace NovaBlitz.UI
{

    public class CardListItemProxyView : BaseCardView
    {
		// Publics
		public CanvasGroup CanvasGroup;
		public CanvasGroup SelectedOverlay;
		public RawImage ProxyImage;
		public CardListItem CardListItem { get {return this.cardListItem;} }

		// Privates
		private CardListView cardListView;
		private CardListItem cardListItem;
		

		// Pool state
        override public PooledObjectType PooledObjectType { get { return PooledObjectType.CardListItemProxy; } }

		override public void OnRecycle()
		{
			base.OnRecycle();
			this.gameObject.transform.DOKill();
			this.gameObject.transform.localScale = Vector3.one;
			DestroyImmediate(this.ProxyImage.texture);
			this.IsSelected = false;
			this.ProxyImage.texture = null;
			this.cardListView = null;
			this.cardListItem = null;
			this.IsVisible = false;
		}

		override public void SetCardArt(Texture cardArtTexture) 
		{
			this.isToBeMadeVisible = true;
			this.RefreshCardData();
		}

		override public void RefreshCardData() 
		{
			if(this.cardListView == null)
				this.cardListView = GameObject.FindObjectOfType<CardListView>();

			if(this.cardListItem == null)
				this.cardListItem = new CardListItem(this.CardData.CardID,null);
			
			if(this.cardListView != null)
				this.DataBind(this.cardListItem, this.CardData, 1);
		}

		override public void RefreshIsVisible()
		{
			this.CanvasGroup.DOKill();
			this.CanvasGroup.alpha = _IsVisible ? 1f : 0f;
			this.CanvasGroup.blocksRaycasts = _IsVisible;
			if(_IsVisible == false)
			{
				this.isToBeMadeVisible = false;
			}
			else
			{
				//Debug.Log("IsVisible:true");
			}
		}

		public void Initialize(CardListView cardListView, CardListItem cardListItem, CardData cardData, int maxOfEachCard, System.Action<CardListItemProxyView> callback = null)
		{
			// Cache some of the parameter data
			this.cardListView = cardListView;
			this.cardListItem = cardListItem;
			this._CardData = cardData; // Set the protected field so we don't trigger RefreshCardData()
			this.SelectedOverlay.alpha = this.IsSelected ? 1.0f : 0f;
			this.IsVisible = false;
			this.CanvasGroup.blocksRaycasts = true;

			// Request a proxy texture from the CardListItemProxyService
			this.cardListView.CardListItemProxyService.GetProxyImage(cardListItem, cardData, maxOfEachCard, 
			(proxyTexture)=>{
				this.ProxyImage.texture = proxyTexture;
				if(callback != null)
				{
					callback.Invoke(this);
				}
				//Debug.Log("Made Visible in Initialize");
				this.IsVisible = true;
			});
		}

		public void DataBind(CardListItem cardListItem, CardData cardData, int maxOfEachCard)
		{
			// Request a proxy texture from the CardListItemProxyService
			bool isPriority = true;
			this.cardListView.CardListItemProxyService.GetProxyImage(cardListItem, cardData, maxOfEachCard, 
			(proxyTexture)=>{
				this.ProxyImage.texture = proxyTexture;
				this.SelectedOverlay.alpha = this.IsSelected ? 1.0f : 0f;
				if(this.isToBeMadeVisible)
				{
					//Debug.Log("Made Visible in data bind");
					this.IsVisible = true;
					this.isToBeMadeVisible = false;
				}
			}, isPriority);
		}

		public void TweenHighlight(bool isIncrementing )
		{
			Vector3 scaleSize = new Vector3(1.15f, 1.15f, 1.15f);
            this.SelectedOverlay.DOFade(0.4f, 0.0f).OnComplete(() => this.SelectedOverlay.DOFade(1.0f, 0.6f).OnComplete(()=> this.SelectedOverlay.DOFade(this.IsSelected ? 1.0f:0f, 0.2f)));
			if(isIncrementing)
			{
                this.gameObject.transform.DOScale(isIncrementing ? scaleSize : Vector3.one,0.0f).OnComplete(() => this.gameObject.transform.DOScale(Vector3.one, 0.2f));
			}
		}

		public override void SelectCard(bool isSelected)
		{
			//Debug.LogFormat("CardListItemView:{0} isSelected:{1}", this.transform.parent.gameObject.name, isSelected);
			this.SelectedOverlay.DOKill();
			this.SelectedOverlay.DOFade(isSelected ? 1f : 0f, 0.1f);
			this.IsSelected = isSelected;
		}
    }
}