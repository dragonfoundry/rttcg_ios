﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;
using TMPro;
using Messages;

namespace NovaBlitz.UI
{
    public class TournamentListView : View
    {
        private const string TODAY = "Today";
        private const string TOMORROW = "Tomorrow";
        private const string YESTERDAY = "Yesterday";
        private const string TIMEFORMAT = " {1}:{2}";

        public TextMeshProUGUI TournamentNameLabel;
        public TextMeshProUGUI TournamentStatusLabel;
        public TextMeshProUGUI TournamentCostLabel;
        public TextMeshProUGUI TournamentStartTimeLabel;
        public Toggle TournamentButton;
        public CanvasGroup CanvasGroup;

        public Image Complete;
        public Image Future;
        public Image Active;

        public Action<TournamentEventData> OnClickAction;

        protected TournamentEventData _Data;
        public TournamentEventData Data
        {
            get { return _Data; }
            set { _Data = value; RefreshData(); }
        }
                
        public bool IsVisible
        {
            set
            {
                this.CanvasGroup.alpha = value ? 1f : 0f;
                this.CanvasGroup.interactable = value;
                this.CanvasGroup.blocksRaycasts = value;
            }
        }

        void RefreshData()
        {
            if (null == this.Data)
                return;
            
            DateTime localStart = Data.StartTimeUTC.ToLocalTime();
            TournamentStartTimeLabel.SetText(localStart.ToString("ddd d MMM h:mm"));
            
            TournamentNameLabel.text = this.Data.ID;
            TournamentStatusLabel.text = DateTime.UtcNow >= Data.LastEntryTimeUTC ? I2.Loc.ScriptLocalization.Leaderboard : DateTime.UtcNow >= Data.StartTimeUTC ? I2.Loc.ScriptLocalization.Enter : String.Empty;
            if(Data.Costs == null || Data.Costs.Count == 0)
                TournamentCostLabel.text = I2.Loc.ScriptLocalization.Free;
            else
            {
                foreach(var kvp in Data.Costs)
                {
                    TournamentCostLabel.text = kvp.Key.ToString() + " " + kvp.Value.ToString();
                }
            }

            // Wire up more data here
            this.TournamentButton.gameObject.SetActive(true);
            this.TournamentButton.onValueChanged.RemoveListener(this.OnTournamentToggled);
            this.TournamentButton.onValueChanged.AddListener(this.OnTournamentToggled);
        }

        public void SetToggleGroup(ToggleGroup group)
        {
            this.TournamentButton.group = group;
        }

        public void OnClick()
        {
            if(OnClickAction != null)
            {
                OnClickAction.Invoke(Data);
            }
        }

        public void OnTournamentToggled(bool isActive)
        {
            if (!isActive)
                return;
            // populate the data in the eventscreencolumnview

            // If it's completed, get the leaderboard

            // if it's upcoming, view its info

            // if it's current, let you enter it
        }
    }


    [System.Serializable]
    public class MultiTournamentView : MultiRect<TournamentListView, TournamentEventData>
    {
        public Action<TournamentEventData> OnTournamentItemClicked;

        public ToggleGroup group;
        protected override void Initialize(TournamentListView instance, TournamentEventData data)
        {
            instance.Data = data;
            instance.TournamentButton.group = group;
            instance.OnClickAction += OnItemClicked;
        }

        public TournamentListView SetCurrent(TournamentEventData data)
        {
            TournamentListView view = null;
            foreach(var item in this.Instances)
            {
                item.TournamentButton.isOn = false;
                if(item.Data.StartTimeUTC == data.StartTimeUTC)
                {
                    view = item;
                    item.TournamentButton.isOn = true;
                }
            }
            return view;
        }

        public void OnItemClicked(TournamentEventData data)
        {
            if(OnTournamentItemClicked != null)
            {
                OnTournamentItemClicked.Invoke(data);
            }
            Debug.Log("Clicked for data: " + data.ID);        }

        public void SnapTo(TournamentListView target)
        {
            Canvas.ForceUpdateCanvases();

            (this._Container.transform as RectTransform).anchoredPosition =
                (Vector2)this._Container.transform.InverseTransformPoint((this._Container.transform as RectTransform).position)
                - (Vector2)this._Container.transform.InverseTransformPoint((target.transform as RectTransform).position);
        }
    }
}
