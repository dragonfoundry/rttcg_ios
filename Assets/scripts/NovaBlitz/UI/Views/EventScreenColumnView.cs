﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using LinqTools;
using UnityEngine.EventSystems;
using TMPro;
using NovaBlitz.Economy;
using strange.extensions.mediation.impl;
using Messages;

namespace NovaBlitz.UI
{
    public class EventScreenColumnView : AnimatedView
    {
        // Header on the top, as normal
        // one column on the left, listing past & future events.
        // one column on the right, listing the current event.
        // Enter button on the right, below pricing.

        // Information popup: Leaderboard & event info (duplicate of current event one.

        public MultiTournamentView MultiTournamenView;
        public MultiTournamentView ActiveTournament;

        public event Action EventBackClicked;
        public event Action<TournamentEventData> EventEnterClicked;
        public event Action<string> EventTournamentClicked;
        public event Action<string> EventTournamentEntryClicked;

        public Action<TournamentListView> TournamentSelected;
        public Action<TournamentListView> TournamentDeselected;

        public TextMeshProUGUI StatusHeader;
        public GridLayoutGroup ItemsGrid;
        public ToggleGroup ToggleGroup;
        public ScrollRect ScrollRect;

        public TextMeshProUGUI StatusText;
        public TextMeshProUGUI CountDownText;
        
        public TournamentInfoWidget TournamentInfo;
        
        private const string ANIM_EXPAND = "IsExpanded";

        public bool IsExpanded
        {
            get { return this.Animator.GetBool(ANIM_EXPAND); }
            set { this.Animator.SetBool(ANIM_EXPAND, value); }
        }

        protected override void Update()
        {
            base.Update();
            if (!TournamentInfo.IsPlaying && TournamentInfo.CurrentEvent == null || DateTime.UtcNow > TournamentInfo.CurrentEvent.LastEntryTimeUTC)
            {
                TournamentInfo.Init(null);
            }
            TournamentInfo.UpdateStatusAndTime();
        }

        #region uGUI
        public void ClickBack() { if (null != this.EventBackClicked) this.EventBackClicked.Invoke(); }
        public void ClickEnter() { if (null != this.EventEnterClicked) this.EventEnterClicked.Invoke(this.TournamentInfo.CurrentEvent); }
        #endregion
    }
}

namespace NovaBlitz.UI.Mediators
{
    public class EventScreenColumnMediator : Mediator
    {
        [Inject]
        public EventScreenColumnView View { get; set; }

        [Inject]
        public GameData GameData { get; set; }
        [Inject]
        public PlayerProfile PlayerProfile { get; set; }
        [Inject]
        public TitleDataLoadedSignal TitleDataLoadedSignal { get; set; }
        [Inject]
        public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject]
        public LanguageChangedSignal LanguageChangedSignal { get; set; }

        [Inject] public ShowLeaderboardSignal ShowLeaderboardSignal { get; set; }

        public override void OnRegister()
        {
            this.TitleDataLoadedSignal.AddListener(OnTitleDataLoaded);
            this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
            this.View.TournamentInfo.EventCurrentTournamentUpdated += OnCurrentTournamentUpdated;
            this.View.MultiTournamenView.OnTournamentItemClicked += this.OnTournamentItemClicked;
            this.View.ActiveTournament.OnTournamentItemClicked += this.OnTournamentItemClicked;
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);
            PopulateView();
            RefreshView();
        }

        public override void OnRemove()
        {
            this.TitleDataLoadedSignal.RemoveListener(OnTitleDataLoaded);
            this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
            this.View.TournamentInfo.EventCurrentTournamentUpdated -= OnCurrentTournamentUpdated;
            this.View.MultiTournamenView.OnTournamentItemClicked -= this.OnTournamentItemClicked;
            this.View.ActiveTournament.OnTournamentItemClicked -= this.OnTournamentItemClicked;
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);
        }

        protected void OnLanguageChanged()
        {
            PopulateView();
        }

        protected void OnCurrentTournamentUpdated()
        {
            if (View.TournamentInfo.CurrentEvent == null)
                return;
            View.ActiveTournament.Populate(new List<TournamentEventData> { View.TournamentInfo.CurrentEvent });
            View.ActiveTournament.SetCurrent(View.TournamentInfo.CurrentEvent);

            View.MultiTournamenView.SnapTo(View.MultiTournamenView.SetCurrent(View.TournamentInfo.CurrentEvent));
        }
        
        protected void PopulateView()
        {
            if (GameData.TournamentSchedule != null)
            {
                this.View.MultiTournamenView.Populate(GameData.TournamentSchedule.Values.Reverse());
            }
            RefreshView();
        }

        protected void RefreshView()
        {
            this.View.TournamentInfo.Init((PlayerProfile.CurrentTournament == null && PlayerProfile.TournamentData.event_status != Messages.TournamentStatus.not_playing) ? PlayerProfile.CurrentTournament : null);
            this.View.IsExpanded = true;
            this.View.TournamentInfo.UpdateStatusAndTime();
        }

        protected void OnTitleDataLoaded()
        {
            PopulateView();
        }

        protected void OnProfileUpdated(ProfileSection section)
        {
            RefreshView();
        }

        protected void OnTournamentItemClicked(TournamentEventData data)
        {
            if(data != null && data.Leaderboard != null)
                this.ShowLeaderboardSignal.Dispatch(data.Leaderboard, LeaderboardType.Event);
        }
    }
}
