﻿using UnityEngine;
using UnityEngine.UI;
using System;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	public class DragOverlay : MonoBehaviour, IDragOverlay, IRectTransform
	{
		public RectTransform RectTransform {get; protected set;}

		public void Place(Transform child)
		{
			this.RectTransform.SetAsLastSibling();
			child.SetParent(this.RectTransform);
			child.SetAsLastSibling();
			child.localPosition = Vector3.zero;
		}

		public void Move(Transform child, Vector2 pos)
		{
			Vector2 newPos; RectTransformUtility.ScreenPointToLocalPointInRectangle(this.RectTransform, pos, Camera.main, out newPos);
			child.localPosition = newPos;
		}

		void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
		}

		void Start()
		{
			UITools.MaximizeRect(this.RectTransform);
		}
	}

	public interface IDragOverlay
	{
		void Place(Transform child);
		void Move(Transform child, Vector2 pos);
	}
}
