﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System;
using LinqTools;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using System.Collections.Generic;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class DraftPackBrowserView : AnimatedView, IPoolView
    {
		public TextMeshProUGUI HeaderLabel;
		//public DraftStatsView DraftStats;
		public GridLayoutGroup Grid;
		public DropTarget DropTarget;
		public CardAbilityController CardAbilities;
		public CanvasGroup CardAbilitiesCanvasGroup;
        public GameObject[] Spacers;
        public NovaAudioPlayer[] SlotAudio;

        protected RectTransform _GridRX;

		[HideInInspector] public CardView CardPrefab;
		[HideInInspector] public DraggableCardView DraggableCardPrefab;


        public int CardColumns = 3;
        public float CardRatio = 1.6f;
        private float ColumnWidth = 0f;

		[Inject] public ViewReferences ViewReferences {get;set;}
        [Inject] public IDragOverlay DragOverlay {get;set;}
		public TwoTouchOverlayView TwoTouchOverlay {get;set;}
		[Inject] public ShowTwoTouchSignal ShowTwoTouchSignal {get;set;}
		[Inject] public CloseTwoTouchViewSignal CloseTwoTouchViewSignal {get;set;}

		protected List<DraggableCardView> _CardInstances = new List<DraggableCardView>();

        [Inject] public PlayerProfile PlayerProfile {get;set;}

		public override bool IsOpen {
			get { return base.IsOpen; }
			set { base.IsOpen = value;
				if (!value) Recycle(); }
		}

        public void Initialize()
        {
            //DraftStats.DeckData = this.PlayerProfile.DraftDeck;
            //DraftStats.Refresh();
			this.DropTarget.EventDragItemHovered += OnDropTarget_Hover;
			this.DropTarget.EventDragItemDropped += OnDropTarget_Drop;

            ColumnWidth = Math.Min(500,UITools.CalcGridColumnWidth(this.Grid, this.CardColumns));
            this.Grid.cellSize = new Vector2(Mathf.Floor(ColumnWidth), Mathf.Floor(ColumnWidth * this.CardRatio));
        }

        protected override void Update()
        {
            base.Update();
            float columnWidth = Math.Min(500, UITools.CalcGridColumnWidth(this.Grid, this.CardColumns));
            if(ColumnWidth != columnWidth)
            {
                Debug.LogFormat("Column width from {0} to {1}", ColumnWidth, columnWidth);
                ColumnWidth = columnWidth;
                this.Grid.cellSize = new Vector2(Mathf.Floor(ColumnWidth), Mathf.Floor(ColumnWidth * this.CardRatio));

                var cardViews = _CardInstances.Select(dc => (CardView)dc.CardDisplay);
                foreach(var cardView in cardViews)
                {
                    cardView.ScaleToWidth(this.Grid.cellSize.x);
                }

                LayoutRebuilder.ForceRebuildLayoutImmediate(this._GridRX);
            }
            // if the screen's changed size, resize the cards
        }

        protected void OnDropTarget_Hover(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard && null != draggableCard.DraggedCard) 
			{
				draggableCard.DraggedCard.SwitchDisplay(this.CardPrefab);

				if(draggableCard.CardDisplay is CardView)
				{
					//Debug.LogFormat("Target drag Over :{0}", draggableCard.gameObject.name);
					CardView cardView = (CardView)draggableCard.CardDisplay;
					cardView.CardCanvasGroup.DOFade(0.25f, 0.1f);
					BaseCardView instance;
					if (draggableCard.DraggedCard.ChildViews.TryGetValue(this.CardPrefab.GetType(), out instance)) 
					{
                        var cardViewInstance = instance as CardView;
                        if(cardViewInstance != null)
                            cardViewInstance.ScaleToWidth(this.Grid.cellSize.x / 1.25f);
                        instance.transform.localScale = Vector3.one;
                        ShowKeywords(cardViewInstance.CardData.NewHotnessKeywords);
					}
				}
			}
		}

		protected void OnDropTarget_Drop(DraggableItem dragItem)
		{
			var draggableCard = dragItem.GetComponent<DraggableCardView>();
			if (null != draggableCard) 
			{
				StartCoroutine(this.ShowCardAtEndOfFrame(draggableCard));
			}
		}

		protected IEnumerator ShowCardAtEndOfFrame(DraggableCardView draggableCard)
		{
			yield return 0;
			CardView[] cardViews = this.Grid.GetComponentsInChildren<CardView>();	
			Debug.LogFormat("Target Drop:{0} CardViews:{1} DraggableCard:{2}",
                draggableCard.gameObject.name, cardViews == null ? "Null" : cardViews.Count().ToString(), draggableCard == null ? "null" : draggableCard.name);
			foreach(var cardView in cardViews)
			{
				//if(cardView.CardData.CardID == draggableCard.CardData.CardID)
				//{
					cardView.CanvasGroup.DOKill();
					cardView.CanvasGroup.alpha = 1;
					//break;
				//}
			}
		}
        
        public CardView SpawnCard(CardData card)
		{
			if(this.TwoTouchOverlay == null)
			{
				this.TwoTouchOverlay = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			}

			DraggableCardView draggable = UITools.PoolSpawnRect(this.DraggableCardPrefab, Vector3.zero);
            draggable.transform.AttachToParent(_GridRX, 3);
            draggable.transform.localPosition = Vector3.zero;
			_CardInstances.Add(draggable);
			//draggable.transform.localPosition = Vector3.zero;
			draggable.DragOverlay = this.DragOverlay;
			draggable.ShowTwoTouchCallback = OnShowTwoTouch;
			draggable.DismissTwoTouchCallback = OnDismissTwoTouch;
			draggable.CardDraggedCallback = OnCardDragged;
			draggable.CardHoveredCallback = OnCardHovered;
			draggable.CardUnhoveredCallback = OnCardUnhovered;
			draggable.IsTwoTouchable = false;
			draggable.IsEditable = false;
            draggable.DragItem.Constraint = DragConstraint.None;
			CardView cardView = draggable.InitCardDisplay(this.CardPrefab, card);
			cardView.LoadCardArt(card.ArtID);
            cardView.ScaleToWidth(this.Grid.cellSize.x);
            cardView.SetQuantities(1, 0, -1);
            cardView.CardCountFrame.gameObject.SetActive(false);
            cardView.NewCardTag.gameObject.SetActive(false);
			return cardView;
		}

		public void DealCardPack()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(this._GridRX);

			for(int i=0;i<_CardInstances.Count;i++)
			{
				CardView cardView = (CardView)_CardInstances[i].CardDisplay;
				RectTransform cardRect = (RectTransform)cardView.Canvas.transform;
				float startDelay = (i+1) * 0.25f;
                var seq = DOTween.Sequence();
                var j = i;
                seq.InsertCallback(startDelay + i*0.05f, () => SlotAudio[j].Play());
				seq.Insert(startDelay, cardRect.DOAnchorPosX(-900f,0.3f).From());
                seq.Insert(startDelay, cardRect.DOScale(1.0f,0.35f).From());
                seq.Insert(startDelay, cardView.CanvasGroup.DOFade(1.0f,0.2f));
			}
		}

		private void OnCardHovered(DraggableCardView draggableCardView)
        {
            //Debug.Log("Card Hovered " + draggableCardView);
			if(draggableCardView.CardDisplay is CardView)
			{
				CardView cardView = (CardView)draggableCardView.CardDisplay;
				cardView.ShowWhiteOutline(true);

                ShowKeywords(draggableCardView.CardData.NewHotnessKeywords);
            }
        }

        private void ShowKeywords(Dictionary<int,double> keywords)
        {
            if (keywords.Count > 0)
            {
                this.CardAbilitiesCanvasGroup.DOKill();
                this.CardAbilitiesCanvasGroup.DOFade(1.0f, 0.2f).SetDelay(0.3f);
                this.CardAbilities.Initialize(keywords);
            }
            else
            {
                HideKeywords();
            }
        }

        public void HideKeywords()
        {
            this.CardAbilitiesCanvasGroup.DOKill();
            this.CardAbilitiesCanvasGroup.alpha = 0;
            this.CardAbilities.HideAbilities();
        }

        private void OnCardUnhovered(DraggableCardView draggableCardView)
        {
            //Debug.Log("Card Unhovered " + draggableCardView);
			if(draggableCardView.CardDisplay is CardView)
			{
				CardView cardView = (CardView)draggableCardView.CardDisplay;
                HideKeywords();
                cardView.ShowWhiteOutline(false);
			}
        }

        private void OnShowTwoTouch(TwoTouchParams twoTouchParams)
		{
            ShowKeywords(twoTouchParams.CardData.NewHotnessKeywords);
		}

		private void OnDismissTwoTouch()
		{
			this.CloseTwoTouchViewSignal.Dispatch();
		}

		private void OnCardDragged(DraggableCardView draggableCardView)
		{
			if(draggableCardView.CardDisplay is CardView)
			{
				CardView cardView = (CardView)draggableCardView.CardDisplay;
				cardView.CardCanvasGroup.alpha =  0.25f;
				cardView.ShowWhiteOutline(false);
				//this.CardAbilitiesCanvasGroup.DOKill();
				//this.CardAbilitiesCanvasGroup.alpha = 0;
				cardView.ShowWhiteOutline(false);
				BaseCardView instance;
				if (draggableCardView.DraggedCard.ChildViews.TryGetValue(this.CardPrefab.GetType(), out instance)) 
				{
                    instance.transform.localScale = Vector3.one;// * 1.5f;
				}
			}
		}
		
		public void OnTextureLoaded(CardView cardView, Texture texture)
		{
			if(texture != null)
			{
				cardView.CardArt.texture = texture;
			}
			
			cardView.LoadingSpinner.gameObject.SetActive(false);
		}
		
		public void ClearPack(float fadeDuration = 0f, int pickedCard = 0)
		{
			var cardViews = _CardInstances.Select(dc => (CardView)dc.CardDisplay);
			Debug.Log("Fade out cards...");
			
			if (fadeDuration > 0f) {
				foreach (var cv in cardViews)
                {
					if (cv != null && (cv.CardData == null || cv.CardData.CardID != pickedCard))
						cv.FadeOut(fadeDuration);
				}
				StartCoroutine(this.RecycleAfterDelay(fadeDuration));
			}
			else
			{
				this.Recycle();	
			}
		}
		
		IEnumerator RecycleAfterDelay(float fadeDuration)
		{
			yield return new WaitForSecondsRealtime(fadeDuration);
			Recycle();
        }
		
		public void Recycle()
		{
			if (null != _GridRX) {
				foreach (var dc in _CardInstances.Where(i => null != i)) {
					//dc.DespawnCardDisplay();
					dc.Recycle ();
					//GameObject.Destroy(dc.gameObject);
					//ObjectPool.Recycle<DraggableCardView>(dc);
				}
				_CardInstances.Clear();

                foreach (var spacer in Spacers)
                {
                    spacer.gameObject.SetActive(true);
                }
            }
			else Debug.Log("No grid to recycle");
		}

		protected override void Awake()
		{
			_GridRX = (RectTransform)this.Grid.transform;
			base.Awake();
		}
    }
}