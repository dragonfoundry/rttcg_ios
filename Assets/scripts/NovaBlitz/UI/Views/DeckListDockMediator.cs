﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using LinqTools;
using System.Collections.Generic;
using UnityEngine.UI;
using GameFormat = Messages.GameFormat;
using NovaBlitz.UI;

namespace NovaBlitz.UI.Mediators
{
	public class DeckListDockMediator : Mediator 
	{
		[Inject] public DeckListDockView View {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;} 
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public GameData GameData { get; set; }
		[Inject] public DeckSelectedSignal DeckSelectedSignal {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public DeckDeletedSignal DeckDeletedSignal { get; set; }
		[Inject] public DeckClonedSignal DeckClonedSignal { get; set; }
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }


		/// <summary>
		/// Add event listeners
		/// </summary>
		public override void OnRegister()
        {
            this.View.CardListItemPrefab = this.UIConfiguration.CardListItemPrefab;
			this.View.DraggableCardPrefab = this.UIConfiguration.DraggableCardPrefab;
			this.View.EventFormatChanged += OnViewFormatChanged;
			this.ProfileUpdatedSignal.AddListener(OnProfileUpdated);
			this.DeckSelectedSignal.AddListener(OnDeckSelected);
			this.DeckClonedSignal.AddListener (OnDeckCloned);
			this.DeckDeletedSignal.AddListener (OnDeckDeleted);
			this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.AddListener(this.OnLanguageChanged);

            // By default have this view on the overlay Canvas
			
			//RefreshDeck(this.View.Format);
		}
		
		/// <summary>
		/// Remove event listeners
		/// </summary>
		public override void OnRemove()
        {
            this.View.EventFormatChanged -= OnViewFormatChanged;
			this.ProfileUpdatedSignal.RemoveListener(OnProfileUpdated);
			this.DeckSelectedSignal.RemoveListener(OnDeckSelected);
			this.DeckClonedSignal.RemoveListener (OnDeckCloned);
			this.DeckDeletedSignal.RemoveListener (OnDeckDeleted);
			this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.LanguageChangedSignal.RemoveListener(this.OnLanguageChanged);

        }

		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            if(viewType == typeof(TwoTouchOverlayView))
            {
				NovaBlitz.Game.GameLogView gameLogView = GameObject.FindObjectOfType<NovaBlitz.Game.GameLogView>();

				if(gameLogView == null || gameLogView.IsOpen == false)
				{
					// Make sure we go to the overlay layer with the TwoTouch
					this.MoveAnimatedViewToCanvasSignal.Dispatch(this.View, NovaCanvas.Overlay, 0.0f);
				}
            }
            else if(viewType == typeof(DeckListDockView))
            {
                //RefreshDeck(this.View.Format);
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
				if(this.View.TwoTouchOverlay != null)
				{
					this.View.TwoTouchOverlay.HideKeywords();
                }
                this.View.Recycle();
            }
        }

		private void OnProfileUpdated(ProfileSection section)
		{
			switch (section) {
			case ProfileSection.DeckList:
			case ProfileSection.DraftDeck:
				RefreshDeck(this.View.Format);
				break;
			}
		}

        private void OnLanguageChanged()
        {
            RefreshDeck(this.View.Format);
        }

		private void OnDeckSelected(DeckData deckData)
		{
			//this.View.Initialize(deckData);
            RefreshDeck(this.View.Format);
			/*if(this.View.Format != GameFormat.Draft && this.View.Format != GameFormat.Tutorial)
			{
				this.View.IsEditable = true;
			}*/
		}

		protected void OnViewFormatChanged(GameFormat format)
		{
			RefreshDeck(format);
		}

		protected void OnDeckDeleted(int index)
		{
			this.RefreshDeck (this.View.Format);
		}

		protected void OnDeckCloned(DeckData deckData, string deckName)
		{
			this.RefreshDeck (this.View.Format);
        }
        
        protected void RefreshDeck(GameFormat format)
		{
            Debug.LogFormat("Refreshing deck for {0}", format);
			switch (format) {
			case GameFormat.Draft:
				if (null != this.PlayerProfile.DraftDeck)
					this.View.Initialize(this.PlayerProfile.DraftDeck,null,null);
                else
                {
                    var deck = new DeckData(GameFormat.Draft);
                    this.View.Initialize(deck, null,null);
                }
				this.View.IsEditable = false;
				break;
            case GameFormat.Constructed:
                if (null != this.PlayerProfile.LeagueDeck)
                {
                    this.View.Initialize(this.PlayerProfile.LeagueDeck,null,null);
                    this.View.IsEditable = false;
                }
                else
                {
                    this.View.Initialize(this.PlayerProfile.CurrentDeck, PlayerProfile.OwnedCards, GameData.LegalCards);
                    this.View.IsEditable = true;
                }
                break;
            case GameFormat.Tournament:
            case GameFormat.Monthly:
            case GameFormat.Annual:
                if (null != this.PlayerProfile.EventDeck)
                {
                    this.View.Initialize(this.PlayerProfile.EventDeck, null,null);
                    this.View.IsEditable = false;
                }
                else
                {
                    this.View.Initialize(this.PlayerProfile.CurrentDeck, PlayerProfile.OwnedCards, GameData.LegalCards);
                        this.View.IsEditable = true;
                }
                break;
            case GameFormat.Tutorial:
                if (null != this.PlayerProfile.CurrentDeck)
                {
                    this.View.Initialize(this.PlayerProfile.CurrentDeck, null,null);
                    this.View.IsEditable = false;
                }
                else
                {
                    this.View.Recycle();
                    this.View.IsEditable = false;
                }
                break;
            default:
				if (null != this.PlayerProfile.CurrentDeck) {
					this.View.Initialize(this.PlayerProfile.CurrentDeck, PlayerProfile.OwnedCards, GameData.LegalCards);
                        this.View.IsEditable = true;
				}
				else {
					this.View.Recycle();
					this.View.IsEditable = false;
				}
				break;
			}
            View.DeckButton.interactable = format != GameFormat.Tutorial;

        }
	}
}
