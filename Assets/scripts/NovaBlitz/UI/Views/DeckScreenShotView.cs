﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;

namespace NovaBlitz.UI
{
	public class DeckScreenShotView : AnimatedView
	{
		[SerializeField]
		protected TextMeshProUGUI _NameDisplay;
		[SerializeField]
		protected TextMeshProUGUI _CountDisplay;

		public CardAspectsIndicator AspectsIndicator;

        public RectTransform WholeScreen;
        public RectTransform ScreenShotPanel;
        public Button ShareButton;
        public GameObject ShareButtonPanel;
        public NativeShare SharingPlugin;

        public Action<ShareScreenShotParams> EventShareClicked;
        public Action EventCloseClicked;

        [Inject] public IDragOverlay DragOverlay {get;set;}
		protected List<DraggableCardView> _CardInstances = new List<DraggableCardView>();
		[SerializeField]
		public GridLayoutGroup _CardGrid;
		protected RectTransform _GridRX;

        private DeckData _Deck;

		[HideInInspector] public CardListItemView CardListItemPrefab;
		//public DeckData DeckData {get;set;}

		public string DeckName {
            get { return _NameDisplay.text; }
			set { if (null != _NameDisplay) _NameDisplay.text = value; }
		}

		public int CardCount {
			set { _CountDisplay.SetText("{0}/{1}", value, _Deck.MinDeckSize); }
		}

		protected override void Awake()
		{
			_GridRX = (RectTransform)_CardGrid.transform;
            //this.IsOpen = false;
			base.Awake();
		}

        public void OnShareClick()
        {
            if (this.EventShareClicked != null)
            {
                this.EventShareClicked.Invoke(new ShareScreenShotParams(SharingPlugin, this.DeckName, ShareButton, ShareButtonPanel));
            }
        }

        public void OnScrimClicked()
		{
            if(this.EventCloseClicked != null)
            {
                this.EventCloseClicked.Invoke();
            }
		}

        public void Refresh(DeckData deck)
        {
            if (null == deck)
                return;
            _Deck = deck;

            this.DeckName = deck.Name;
            this.CardCount = deck.Cards.Count;
            this.AspectsIndicator.ShowIcons(deck.Aspects);
            foreach (CardListItemView cliv in _CardGrid.GetComponentsInChildren(typeof(CardListItemView)))
            {
                //UnityEngine.Object.Destroy(cliv);
                cliv.Recycle();
            }
            List<CardListItem> cardListItems = CardListTransform.GetList(deck);

            for (int i = 0; i < cardListItems.Count; i++)
            {
                CardListItem cardListItem = cardListItems[i];
                this.AddCardItemToGrid(cardListItem, i, deck.MaxOfEachCard);
                //CardListItemView cardListItemView = this.AddCardItemToGrid(cardListItem, i, deck.MaxOfEachCard);
                //TweenAlpha.Begin(cardListItemView.Overlay.gameObject, 0f, 0f);
            }

            Timer.Instance.StartCoroutine(SetSize());

#if UNITY_IOS || UNITY_ANDROID
#else
            this.ShareButtonPanel.gameObject.SetActive(false);
#endif
        }

        public IEnumerator SetSize()
        {
            yield return null;
            // zoom in to match screen size
            var screenWidth = WholeScreen.rect.width;
            var screenHeight = WholeScreen.rect.height;
            var width = ScreenShotPanel.rect.width;
            var height = ScreenShotPanel.rect.height;


            float targetHeight = 0.9f;
            float targetWidth = 1.0f;
            // zoom to 0.9
            var widthRatio = (screenWidth / width) * targetWidth; // how much bigger the screen is
            var heightRatio = (screenHeight / height) * targetHeight; // how much bigger the screen is
            float zoomamount = Math.Min(1.5f, Math.Min(widthRatio, heightRatio));
            ScreenShotPanel.localScale = new Vector3(zoomamount, zoomamount, zoomamount);
        }

		public CardListItemView AddCardItemToGrid(CardListItem cardListItem, int index, int maxOfEachCard)
		{
			var cardData = GameData.CardDictionary[cardListItem.CardID];
            
            var itemView = UITools.PoolSpawnRect(CardListItemPrefab, Vector3.zero, _CardGrid.transform);

            //var itemView = (CardListItemView)UnityEngine.Object.Instantiate(CardListItemPrefab, Vector3.zero, Quaternion.identity);

			itemView.Initialize(cardListItem, cardData, maxOfEachCard);
            itemView.transform.localPosition = Vector3.zero;
            itemView.RefreshCardData ();
			itemView.CountLabel.gameObject.SetActive(true);
			itemView.transform.SetParent (_CardGrid.transform, false);
            itemView.Overlay.gameObject.SetActive(false);


            if (itemView.transform is RectTransform)
            {
                _CardGrid.cellSize = new Vector2(
                    (itemView.transform as RectTransform).rect.width,
                    (itemView.transform as RectTransform).rect.height);
            }
			return itemView;
		}

        public void Recycle()
        {
            var children = this.GetComponentsInChildren<CardListItemView>();
            foreach(var child in children)
            {
                child.Recycle();
            }
        }

        protected override void OnDestroy()
        {
            Recycle();
            base.OnDestroy();

        }
    }
}

namespace NovaBlitz.UI.Mediators
{
	public class DeckScreenShotMediator : Mediator
	{
		[Inject] public DeckScreenShotView View {get;set;}

		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}

		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal {get;set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal { get; set; }
        [Inject] public INovaContext NovaContext { get; set; }
        [Inject] public ShareScreenShotSignal ShareScreenShotSignal { get; set; }

		/// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
		{
			this.View.CardListItemPrefab = this.UIConfiguration.CardListItemPrefab;
			this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            this.View.EventCloseClicked += OnCloseClicked;
            this.View.EventShareClicked += OnShareClicked;

            //this.View.Refresh (PlayerProfile.CurrentDeck);;
		}

		/// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
        {
			this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
            this.View.EventCloseClicked -= OnCloseClicked;
            this.View.EventShareClicked -= OnShareClicked;
        }

		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == typeof(DeckScreenShotView))
            {
                //View.Refresh(PlayerProfile.CurrentDeck);
                //Timer.Instance.StartCoroutine(this.View.SetSize());
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                this.View.Recycle();  
            }
        }

        private void OnCloseClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(DeckScreenShotView));
        }
        private void OnShareClicked(ShareScreenShotParams Params)
        {
            this.ShareScreenShotSignal.Dispatch(Params);
        }

        #region Signal Listeners
        /*protected void OnViewOpen(System.Type type)
		{
			if (type != typeof(DeckScreenShotView))
				return;
			View.Refresh (PlayerProfile.CurrentDeck);
            //Timer.Instance.StartCoroutine(this.View.SetSize());
        }*/
		#endregion
	}
}
