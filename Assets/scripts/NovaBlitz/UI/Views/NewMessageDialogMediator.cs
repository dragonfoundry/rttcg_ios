﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class NewMessageDialogMediator : Mediator 
	{
		[Inject] public NewMessageDialogView View {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public UnityIapCompletePurchaseSignal UnityIapCompletePurchaseSignal { get; set; }
        [Inject] public UnityIapPurchaseProcessSignal UnityIapPurchaseProcessSignal { get; set; }

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
		{
			this.View.DialogDismissed += OnDismissed;
            this.UnityIapPurchaseProcessSignal.AddListener(this.OnPurchaseComplete);

            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

		 /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
		{
			this.View.DialogDismissed -= OnDismissed;
            this.UnityIapPurchaseProcessSignal.RemoveListener(this.OnPurchaseComplete);
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

		/// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {

            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                //this.NovaContext.BlurMainCanvas(false, this.View);

                if (View.SubjectItem != null)
                    View.SubjectItem.Recycle();
            }
        }

        private void OnDismissed()
		{
			this.CloseAnimatedViewSignal.Dispatch(this.View.GetType());
		}

        protected void OnStoreOpened()
        {
            if (!View.IsWaitingForPurchase)
                return;
            View.StopWaitCoroutine();
            this.View.TitleLabel.text = I2.Loc.ScriptLocalization.Waiting_For_Confirmation;
            this.View.MessageLabel.text = string.Empty;
            // thank them
            this.View.IsButtonsActive = true;
            this.View.ButtonLabel.text = I2.Loc.ScriptLocalization.OK;
        }

        protected void OnPurchaseComplete(UnityEngine.Purchasing.Product purchasedProduct, bool isSuccess, string note)
        {
            if(!isSuccess)
            {
                OnPurchaseFailed(note);
            }
        }

        protected void OnSteamPurchaseComplete(bool isSuccessful, string note)
        {

        }

        protected void OnPurchaseSuccess(string note)
        {
            if (!View.IsWaitingForPurchase)
                return;
            View.StopWaitCoroutine();
            this.View.TitleLabel.text = I2.Loc.ScriptLocalization.Purchase_Successful;
            this.View.MessageLabel.text = I2.Loc.ScriptLocalization.Purchase_Successful;
            // thank them
            this.View.IsButtonsActive = true;
            this.View.ButtonLabel.text = I2.Loc.ScriptLocalization.OK;
        }

        protected void OnPurchaseFailed(string note)
        {
            if (!View.IsWaitingForPurchase)
                return;
            View.StopWaitCoroutine();
            this.View.TitleLabel.text = I2.Loc.ScriptLocalization.Error.Failed;
            this.View.MessageLabel.text = I2.Loc.ScriptLocalization.Error.Purchase_Failed;
            this.View.IsButtonsActive = true;
            this.View.ButtonLabel.text = I2.Loc.ScriptLocalization.OK;
        }
        // move the waiting panel to its own view - could be used elsewhere.
        // overload the message dialog panel.
        protected void OnPurchaseCanceled()
        {
            if (!View.IsWaitingForPurchase)
                return;
            View.StopWaitCoroutine();
            OnDismissed();
        }
    }
}
