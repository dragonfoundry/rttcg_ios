﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.mediation.impl;
using Messages;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class StoreView : AnimatedView
	{
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		public event Action<StoreData> EventItemClicked;
        public MultiStoreItem Row1;
        public MultiStoreItem Row2;
        public MultiStoreItem Row3;

        public RectTransform WholeScreen;
        public RectTransform StorePanel;
        public RectTransform ViewPort;
        public RectTransform StoreVert;
        public ScrollRect ScrollRect;

        private float scale;

        public bool hasPurchasedSomething { get;set;}

        //public MultiStoreItem MultiBooster;
		//public MultiStoreItem MultiCreditBundle;
		//public MultiStoreItem MultiPackBundle;
		//public MultiStoreItem MultiDailyDeal;

        //public Image PacksToOpen;
        //public TextMeshProUGUI NumberOfPacksToOpen;

		protected void OnItem_Clicked(StoreItemView item) { if (null != this.EventItemClicked) this.EventItemClicked.Invoke(item.StoreData); }

        public void OnScrimClicked()
        {
            this.CloseAnimatedViewSignal.Dispatch(this.GetType());
        }

        protected override void Update()
        {
            base.Update();
            // scale rows based on screen size, with a max scale of 1.
            var newScale = Math.Min(1.0f, ViewPort.rect.width / 1470);
            if (scale != newScale)
            {
                StoreVert.localScale = Vector3.one * newScale;
                scale = newScale;
            }
            /*if (ViewPort.rect.width < 1450 && scale == 1.0f)
            {
                scale = 0.7f;
                StoreVert.localScale = Vector3.one * scale;
            }
            else if (ViewPort.rect.width >= 1450 && scale == 0.7f)
            {
                scale = 1.0f;
                StoreVert.localScale = Vector3.one * scale;
            }*/
        }

        public IEnumerator SetSize()
        {
            yield return null;
            // zoom in to match screen size
            var screenWidth = WholeScreen.rect.width;
            var screenHeight = WholeScreen.rect.height;
            var width = StorePanel.rect.width;
            var height = StorePanel.rect.height;


            float targetHeight = 0.75f;
            float targetWidth = 0.9f;
            // zoom to 0.9
            var widthRatio = (screenWidth / width) * targetWidth; // how much bigger the screen is
            var heightRatio = (screenHeight / height) * targetHeight; // how much bigger the screen is
            float zoomamount = Math.Max(1f, Math.Min(1.5f, Math.Min(widthRatio, heightRatio)));
            StorePanel.localScale = new Vector3(zoomamount, zoomamount, zoomamount);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            this.Row1.Purge();
            this.Row2.Purge();
            this.Row3.Purge();
        }
    }


    [Serializable]
    public class MultiStoreItem : MultiRect<StoreItemView, StoreData>
    {
		protected override void Initialize(StoreItemView instance, StoreData data) { instance.StoreData = data; }
	}
}

namespace NovaBlitz.UI.Mediators
{
	public class StoreMediator : Mediator
	{
		[Inject] public StoreView View {get;set;}

		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public NovaIAPService NovaIAPService { get; set; }
        [Inject] public ViewReferences ViewReferences {get;set;}
        
		[Inject] public VirtualPurchaseSignal VirtualPurchaseSignal {get;set;}
		[Inject] public MoneyPurchaseSignal MoneyPurchaseSignal {get;set;}
		[Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal {get;set; }
        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal { get; set; }
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
		[Inject] public LogEventSignal LogEventSignal {get;set;}
        [Inject] public BeginVirtualPurchaseSignal BeginVirtualPurchaseSignal { get; set; }

        public StoreData CurrentStoreData { get; set;}
        public Dictionary<string, StoreItemView> StoreItems = new Dictionary<string, StoreItemView>();
        
        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
		{
			this.ProfileUpdatedSignal.AddListener(OnSignal_ProfileUpdated);
            this.AnimatedViewOpenedSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
            //OnOpenAnimatedView(this.View.GetType());
            //this.InitializeStoreItems(NBEconomy.STORE_MAIN);
            //this.RefreshAllStoreItems();
            //this.UpdateUiHints();
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
		public override void OnRemove()
		{
			this.ProfileUpdatedSignal.RemoveListener(OnSignal_ProfileUpdated);
            this.AnimatedViewOpenedSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(Type viewType)
        {
            // Is the view that was opened our view?
            if(viewType == this.View.GetType())
            {
                var twoTouch = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
                if (twoTouch != null)
                {
                    this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
                    //twoTouch.IsOpen = false;
				}
				this.View.hasPurchasedSomething = false;
				this.LogEventSignal.Dispatch (new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_STORE, 
					IsTimedEvent = true, 
					IsTimedEventOver = false
				});
                //Timer.Instance.StartCoroutine(this.View.SetSize());
                this.PopulateStoreView();
            }
        }

        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
			{
				var eventParams = new LogEventParams {
					EventName = LogEventParams.EVENT_TIME_STORE, 
					IsTimedEvent = true, 
					IsTimedEventOver = true,
				};
				if (this.View.hasPurchasedSomething && this.CurrentStoreData != null)
					eventParams.ItemId = this.CurrentStoreData.ItemId;
                this.LogEventSignal.Dispatch (eventParams);

				this.View.hasPurchasedSomething = false;
				this.CurrentStoreData = null;
                ClearStoreView();
            }
        }


        /*
        protected void UpdateUiHints()
        {
            if (this.PlayerProfile.UiHintData.Store <= 1 && this.View.annuity_credit.isActiveAndEnabled)
            {
                UiHint hint = UiHint.Store;
                int hintNumber = 1;
                bool showCloseButton = true;
                this.ShowUiHintSignal.Dispatch(new ShowUiHintData(this.View.annuity_credit.BuyButton, hint, hintNumber, NovaContext.MainCanvas, showCloseButton));
            }
        }*/

        public void ClearStoreView()
        {
            this.View.Row1.Purge();
            this.View.Row2.Purge();
            this.View.Row3.Purge();
        }

        public void PopulateStoreView()
        {
            this.View.IsCanvasInteractive = false;
            ClearStoreView();
            if (null != this.GameData.StoreCache)
            {

                Dictionary<string, StoreData> store;
                GameData.StoreCache.TryGetValue(NBEconomy.STORE_MAIN, out store);

                var row1 = store.Values.Where(v => v.tags != null && v.tags.Contains("r1")).OrderBy(v => v.tags).ToList();
                var row2 = store.Values.Where(v => v.tags != null && v.tags.Contains("r2")).OrderBy(v => v.tags);
                var row3 = store.Values.Where(v => v.tags != null && v.tags.Contains("r3")).OrderBy(v => v.tags);


                // prepend offers to row 1.

                var offers = PlayerProfile.GetOffers(3, false);
            

                if (row1.Count() != 2) Debug.LogErrorFormat("Row 1 contains {0} items", row1.Count());

                row1.InsertRange(0, offers.Take(5 - row1.Count));

                if (row1.Count() > 5) Debug.LogErrorFormat("Row 1 contains more than 5 items. Contains {0} items", row1.Count());
                if (row2.Count() != 5) Debug.LogErrorFormat("Row 2 contains {0} items", row1.Count());
                if (row3.Count() != 5) Debug.LogErrorFormat("Row 3 contains {0} items", row1.Count());

                // row 1: OFFERS: Onboarding offer, if any (0-1)(RM). Card offers, if any (0-2)(VC). Smart Pack & Smart Pack Annuity. (2)(RM)
                // If there's no onboarding offer, this row is row 3.

                // row 2: ANNUITIES: The credits and nanobots annuities (RM), plus Credits & Nanobots (VC), plus Draft pack sales (1)(RM)
                // snap to row 2 if the player clicked credits or Nanobots & failed.

                // row 3: GEMS: 5 Gems purchases ($5-$100) (RM)
                // snap to row 3 if the player clicked Gems


                // Populate the store
                //var views = this.View.MultiView.Populate(store.Values).ToList();
                var r1 = this.View.Row1.Populate(row1).ToList();
                var r2 = this.View.Row2.Populate(row2).ToList();
                var r3 = this.View.Row3.Populate(row3).ToList();

                // Sort the store
                var views = r1.Concat(r2).Concat(r3).ToList();
                foreach (var view in views)
                {
                    NovaInventoryItem item;
                    if (view.StoreData.ItemId.Contains("annuity_") && this.PlayerProfile.Inventory.TryGetValue(view.StoreData.ItemId + "_tracker", out item))
                    {
                        view.StoreData.NumberOwned = Math.Max(1, item.RemainingUses.HasValue ? item.RemainingUses.Value : 1);
                    }

                    if (this.PlayerProfile.Inventory.TryGetValue(view.StoreData.ItemId, out item))
                    {
                        if(view.StoreData.StoreId == NBEconomy.STORE_MAIN)
                            view.StoreData.ExpiryTime = item.Expiration.HasValue ? item.Expiration.Value : DateTime.MinValue;
                        view.StoreData.NumberOwned = Math.Max(1, item.RemainingUses.HasValue ? item.RemainingUses.Value : 1);
                    }
                    
                    view.gameObject.transform.SetAsLastSibling();
                    
                    if(NovaIAPService.ProductList.Contains(view.StoreData.ItemId))
                    {
                        switch (view.StoreData.ItemId)
                        {
                            case NovaIAPService.ANNUITY_SMARTPACK:
                            case NovaIAPService.ANNUITY_CREDIT:
                            case NovaIAPService.ANNUITY_NANOBOT:
                                //bool hasPurchased = PlayerProfile.BoughtStoreItems.Contains(view.StoreData.ItemId);
                                // set display based on purchased or not
                                break;
                            case NovaIAPService.GEMS_01:
                            case NovaIAPService.GEMS_02:
                            case NovaIAPService.GEMS_03:
                            case NovaIAPService.GEMS_04:
                            case NovaIAPService.GEMS_05:
                            case NovaIAPService.GEMS_06:
                                break;
                            case NovaIAPService.OFFER_01:
                            case NovaIAPService.OFFER_02:
                            case NovaIAPService.OFFER_03:
                            case NovaIAPService.OFFER_04:
                            case NovaIAPService.OFFER_05:
                            case NovaIAPService.OFFER_06:
                            case NovaIAPService.OFFER_07:
                            case NovaIAPService.OFFER_08:
                            case NovaIAPService.OFFER_09:
                            case NovaIAPService.OFFER_10:
                                break;
                            case NovaIAPService.CREDITS_01:
                            case NovaIAPService.CREDITS_02:
                            case NovaIAPService.CREDITS_03:
                            case NovaIAPService.NANOBOTS_01:
                            case NovaIAPService.NANOBOTS_02:
                            case NovaIAPService.NANOBOTS_03:
                            case NovaIAPService.SMARTPACKS_01:
                            case NovaIAPService.DRAFTS_01:
                                break;
                            default:
                                view.IsVisible = false;
                                Debug.LogErrorFormat("Store Item not found: {0}", view.StoreData.ItemId);
                                break;
                        }
                    }
                    else
                    {
                        switch (view.StoreData.StoreId)
                        {
                            case NBEconomy.ITEMCLASS_CARD:
                                int cardId;
                                CardData cardData;
                                if(int.TryParse(view.StoreData.ItemId, out cardId))
                                {
                                    int owned;
                                    PlayerProfile.OwnedCards.TryGetValue(cardId, out owned);
                                    view.StoreData.NumberOwned = owned;

                                    if (GameData.CardDictionary.TryGetValue(cardId, out cardData))
                                    {
                                        var card = (UIConfiguration.CardPrefab.Spawn());
                                        card.DataBind(cardData);
                                        view.InitializeToCard(card);
                                    }
                                }
                                else
                                {
                                    view.IsVisible = false;
                                    Debug.LogErrorFormat("Store ItemClass not found (product): {0}", view.StoreData.ItemId);
                                }
                                break;
                            case NBEconomy.ITEMCLASS_CARDBACK:
                            case NBEconomy.ITEMCLASS_AVATAR:
                            default:
                                view.IsVisible = false;
                                Debug.LogErrorFormat("Store ItemClass not found (product): {0}", view.StoreData.ItemId);
                                break;

                        }
                        // this is a product - a card or avatar, or cardback
                    }

                    view.ProductScaler.transform.localScale = Vector3.one;
                }
            }
            this.View.IsCanvasInteractive = true;
        }
        
        #region Signal Listeners
        private IEnumerator _WaitToPopulateCoroutine;
        private IEnumerator WaitToPopuateCoroutine()
        {
            yield return null;
            PopulateStoreView();
            _WaitToPopulateCoroutine = null;
        }

		protected void OnSignal_ProfileUpdated(ProfileSection section)
		{
            if(this.View.IsOpen && _WaitToPopulateCoroutine == null)
            {
                _WaitToPopulateCoroutine = WaitToPopuateCoroutine();
                Timer.Instance.StartCoroutine(WaitToPopuateCoroutine());
            }
		}
        #endregion
    }
}
