﻿using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
    public class ForgotPasswordMediator : Mediator
    {
        [Inject] public ForgotPasswordView View { get; set; }
        //[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal { get; set; }
        [Inject] public AccountRecoverySignal AccountRecoverySignal { get; set; }
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}

        /// <summary>
        /// Called on register with the context to register signals and event handlers
        /// </summary>
        public override void OnRegister()
        {
            this.View.DialogDismissed += OnDismissed;
            this.OpenAnimatedViewSignal.AddListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.AddListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Called on Remove from the context to unrgister signals and event handlers
        /// </summary>
        public override void OnRemove()
        {
            this.View.DialogDismissed -= OnDismissed;
            this.OpenAnimatedViewSignal.RemoveListener(this.OnOpenAnimatedView);
            this.CloseAnimatedViewSignal.RemoveListener(this.OnCloseAnimatedView);
        }

        /// <summary>
        /// Inovked when a view is OPENED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnOpenAnimatedView(System.Type viewType)
        {
            // Is the view that was opened our view?
            if (viewType == this.View.GetType())
            {
                Timer.Instance.StartCoroutine(DelaySelectEntryField());
            }
        }

        private IEnumerator DelaySelectEntryField()
        {
            yield return new WaitForSecondsRealtime(0.1f);
            this.View.EmailInput.ActivateInputField();
            this.View.EmailInput.Select();
        }


        /// <summary>
        /// Inovked when a view is CLOSED with the OpenAnimatedViewSignal
        /// </summary>
        private void OnCloseAnimatedView(System.Type viewType)
        {
            // Is the view thatw s closed our view?
            if(viewType == this.View.GetType())
            {
                //this.NovaContext.BlurMainCanvas(false, this.View);
            }
        }

        private void OnDismissed(bool isConfirmed)
        {
            if (isConfirmed)
            {
                AccountRecoverySignal.Dispatch(this.View.Email);
                //ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams { Message = "Check your email for more instructions", Title = "Password Reset Sent" });
                this.View.SendButton.interactable = false;
                this.View.EmailInput.text = string.Empty;
            }

            this.CloseAnimatedViewSignal.Dispatch(typeof(ForgotPasswordView));
        }
    }
}
