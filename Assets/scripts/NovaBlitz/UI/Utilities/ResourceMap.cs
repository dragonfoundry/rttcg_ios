using UnityEngine;
using UnityEngine.Audio;
using System;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using NovaBlitz.UI;

[Serializable]
public class ResourceMap : ScriptableObject
{
	[NonSerialized]
	public Dictionary<string, Sprite> AvatarSprites;
	[NonSerialized]
	public Dictionary<string, Sprite> CardBackSprites;
	
	// Assign via ResourceTool
	public string[] WidgetPaths;
	public string[] AvatarTexturePaths;
	public string[] CardBackTexturePaths;
	
	// Assign via Inspector
	public AudioMixerGroup MasterChannel;
	public AudioMixerGroup SFXChannel;
	public AudioMixerGroup MusicChannel;
	
	void OnEnable()
	{
		PreCacheSprites(AvatarTexturePaths, ref AvatarSprites);
		PreCacheSprites(CardBackTexturePaths, ref CardBackSprites);
		Debug.Log("Resources cached");
	}
	
	void PreCache<TKey, TValue>(string[] paths, ref Dictionary<TKey, TValue> dict, Func<TValue, TKey> selector) where TValue : Component
	{
		if (null == paths)
			return;
		
		dict = new Dictionary<TKey, TValue>();
		foreach (string path in paths)
		{
			TValue prefab = Resources.Load<TValue>(path);
			TKey k = selector.Invoke(prefab);
			if (!dict.ContainsKey(k))
				dict.Add(k, prefab);
			else Debug.LogWarningFormat(@"Duplicate key '{0}' in ResourceMap!", k);
		}
	}
	
	void PreCacheSprites(string[] paths, ref Dictionary<string, Sprite> dict)
	{
		if (null == paths)
			return;
		
		dict = new Dictionary<string, Sprite>();
		foreach (string path in paths)
		{
			Sprite[] sprites = Resources.LoadAll<Sprite>(path);
			foreach (Sprite sprite in sprites)
			{
				string name = sprite.name;
				if (!dict.ContainsKey(name))
					dict.Add(name, sprite);
				else Debug.LogWarningFormat(@"Duplicate key '{0}' in ResourceMap!", name);
			}
		}
	}
}
