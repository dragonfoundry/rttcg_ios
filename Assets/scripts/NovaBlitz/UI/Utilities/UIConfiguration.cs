﻿using UnityEngine;

[System.Serializable]
public class UXValues
{
	public UXConfig ConfigType;
	public float ArenaScale;
	public float HoverDistanceY;
	public float HoverScale;
	public float DragUpThreshold;
	public float DragArrowThreshold;
	public float ZoomHandHoverDistanceY;
	public float ZoomHandHoverScale;
	public float ZoomHandDragUpThreshold;
	public float ZoomHandDragArrowThreshold;
}

[System.Serializable]
public enum UXConfig
{
	MobileWideScreenWideArena = 0,
	MobileWideScreenSquareArena = 1,
	MobileSquareScreenSquareArena = 2,
	DesktopWideScreenWideArena = 3,
	DesktopWideScreenSquareArena = 4,
	DesktopSquareScreenSquareArena = 5
}



namespace NovaBlitz.UI
{
	[CreateAssetMenu]
	public class UIConfiguration : ScriptableObject
	{
		[SerializeField] protected UXValues[] UXValues;

		public UXValues GetUXValues(UXConfig config)
		{
			return this.UXValues[(int)config];
		}

		public CardView CardPrefab;

		[SerializeField] protected CardListItemView _Standard_CardListItemPrefab;
		[SerializeField] protected CardListItemView _Phone_CardListItemPrefab;
		public CardListItemView CardListItemPrefab { get { return this.IsSmallScreen ? _Phone_CardListItemPrefab : _Standard_CardListItemPrefab; } }

		[SerializeField] protected CardListItemProxyView _Standard_CardListItemProxyPrefab;
		[SerializeField] protected CardListItemProxyView _Phone_CardListItemProxyPrefab;
		[SerializeField] public CardListItemProxyView CardListItemProxyPrefab { get { return this.IsSmallScreen ? _Phone_CardListItemProxyPrefab : _Standard_CardListItemProxyPrefab; } }

		[SerializeField] protected DeckListItem _Standard_DeckListItemPrefab;
		[SerializeField] protected DeckListItem _Phone_DeckListItemPrefab;

		[SerializeField] protected FxTrigger _PooledFxPrefab;
		[SerializeField] protected FxTrailTrigger _PooledTrailFxPrefab;
		public DeckListItem DeckListItemPrefab { get { return this.IsSmallScreen ? _Phone_DeckListItemPrefab : _Standard_DeckListItemPrefab; } }
        public FriendListItem FriendListItemPrefab;
        public FxTrigger PooledFxPrefab { get {return this._PooledFxPrefab;} }
        public FxTrailTrigger PooledTrailFxPrefab { get { return this._PooledTrailFxPrefab; } }
		[SerializeField] protected ChatLogEntry _GlobalChatPrefab;
        public ChatLogEntry GlobalChatPrefab { get { return this._GlobalChatPrefab; } }
        public StoreItemView StoreItemPrefab;

        public DraggableCardView DraggableCardPrefab;
        public DraggedCardView DraggedCardPrefab;

        public DragOverlay DragOverlayPrefab;
		public TwoTouchOverlayView TwoTouchOverlayPrefab;
        public AchievementView AchievementPrefab;
        public QuestView QuestPrefab;
        public LeaderboardItemView LeaderboardPrefab;

		public int CardPoolSize = 10;
        public int UIObjectPoolSize = 30;
        public int FXObjectPoolSize = 6;

        public AvatarView AvatarItemPrefab;
        public PlayerCardView PlayerCardPrefab;
        public CardBackView CardBackViewPrefab;

        public Material GrayscaleMaterial;

        [HideInInspector]
		public bool IsSmallScreen;
		public float SmallScreenThreshold = 7.5f;

		public ViewReferences ViewReferences;

		protected void OnEnable()
		{
			Vector2 screenInches = UITools.GetScreenSizeInches();
			this.IsSmallScreen = screenInches.magnitude < this.SmallScreenThreshold;
		}
	}
}
