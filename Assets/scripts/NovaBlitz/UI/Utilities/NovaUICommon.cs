using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using TMPro;

namespace NovaBlitz.UI
{
	public interface IRectTransform
	{
		RectTransform RectTransform {get;}
	}

	public interface IViewOpen
	{
		bool IsOpen {get;set;}
	}

	public static class UITools
	{
		public static T PoolSpawnRect<T>(T prefab, Vector3 pos, Transform parent = null) where T : PoolableMonoBehaviour
		{
            var inst = ObjectPool.SpawnPrefab<T>(prefab.PooledObjectType, pos, parent);
			var irx = (RectTransform)inst.transform;
			irx.localScale = Vector3.one;
			return inst;
		}

		public static T SpawnRect<T>(T prefab, Transform parent = null) where T : Component
		{
			var inst = GameObject.Instantiate(prefab);
			var irx = (RectTransform)inst.transform;
			var prx = (RectTransform)prefab.transform;

			irx.SetParent(parent);
			MatchRect(prx, irx);
			return inst;
		}

		public static void MatchRect(RectTransform source, RectTransform target)
		{
			target.localScale = Vector3.one;
			target.localPosition = source.localPosition;
			target.anchoredPosition = source.anchoredPosition;
			target.sizeDelta = source.sizeDelta;
		}

		public static void MaximizeRect(RectTransform rx)
		{
			rx.localScale = Vector3.one;
			rx.localPosition = Vector3.zero;
			rx.anchorMin = Vector2.zero;
			rx.anchorMax = Vector2.one;
			rx.sizeDelta = Vector2.zero;
		}

		public static Vector2 GetScreenSizeInches()
		{
			return new Vector2(Screen.width/Screen.dpi, Screen.height/Screen.dpi);
		}

		public static float CalcGridColumnWidth(GridLayoutGroup grid, int columns)
		{
			float padWidth = ((RectTransform)grid.transform).rect.width - grid.padding.left - grid.padding.right;
			float spacing = (columns-1) * grid.spacing.x;
			return (padWidth - spacing) / columns;
		}
        
        public static bool ShouldMoveAnimatedViewToCanvas(Type viewType)
        {
            if (viewType == typeof(TwoTouchOverlayView) || viewType == typeof(PlayScreenColumnView) || viewType == typeof(ProgressLoadingScreenView))
                return false;
            else
                return true;
        }
	}

	[Serializable]
	public class MultiRect<T, TData> where T : Component
	{
		[SerializeField]
		protected T _Template;
		[SerializeField]
		protected Transform _Container;
        public Transform Container { get { return _Container; } }
		protected List<T> _Instances = new List<T>();

		public IEnumerable<T> Instances { get { return _Instances.ToArray(); } }

		public T Add(TData data) { T inst = Clone(data); _Instances.Add(inst); return inst; }
		public void Remove(T instance) 
		{ 
			_Instances.Remove(instance); 
			var poolInstance = instance as PoolableMonoBehaviour;
			if (poolInstance != null)
				poolInstance.Recycle ();
			else
				GameObject.Destroy(instance.gameObject); 
		}
		
		public IEnumerable<T> Populate(IEnumerable<TData> data)
		{
			Purge();
			_Instances = data.Select(i => Clone(i)).ToList();
			return this.Instances;
		}
		
		public void Purge()
		{
			foreach (var inst in _Instances)
				if (null != inst)
				{
					var poolInstance = inst as PoolableMonoBehaviour;
					if (poolInstance != null)
						poolInstance.Recycle ();
					else
						GameObject.Destroy(inst.gameObject);
				}
			_Instances.Clear();
		}

		protected T Clone(TData data)
		{
			if (null == _Container)
				_Container = _Template.transform.parent;
            var poolInstance = _Template as PoolableMonoBehaviour;
            T inst;
            if (poolInstance != null)
                inst = UITools.PoolSpawnRect(poolInstance, Vector3.zero, _Container) as T;
            else
                inst = UITools.SpawnRect(_Template, _Container);
			inst.gameObject.SetActive(true);
			Initialize(inst, data);
			return inst;
		}
		
		protected virtual void Initialize(T instance, TData data) {}
	}
	
	[Serializable]
	public class MultiButton : MultiRect<Button, ButtonInfo>
	{
		public event Action<Button> EventClick;
		
		protected override void Initialize(Button instance, ButtonInfo data)
		{
			Text label = instance.GetComponentsInChildren<Text>(true).FirstOrDefault();
			if (null != label)
				label.text = data.Label;
			if (null != data.OnClick)
				instance.onClick.AddListener(data.OnClick);
			instance.onClick.AddListener( ()=> {
				if (null != EventClick)
					EventClick.Invoke(instance);
			});
		}
	}
	
	public class ButtonInfo
	{
		public string Label;
		public UnityEngine.Events.UnityAction OnClick;
	}

	[Serializable]
	public class ComboSlider
	{
		[SerializeField]
		protected AnimatedSlider _Slider;
		[SerializeField]
		protected TextMeshProUGUI _LevelDisplay;
		
		protected Queue<LevelUpInfo> _AnimQueue = new Queue<LevelUpInfo>();

		public bool Descending;
		public int Level { set { _LevelDisplay.text = value.ToString(); } }
        public string DigitalDisplayText { set { _Slider.DigitalDisplay.text = value; } }
		public int Progress { set { _Slider.Progress = value; } }

		public event Action EventAnimsComplete;
		public event Action<int> EventLevelReached;

		public void SetRange(int empty, int full)
		{
			_Slider.Empty = empty;
			_Slider.Full = full;
		}

		public void QueueAnimation(LevelUpInfo anim) { _AnimQueue.Enqueue(anim); }
		public void QueueAnimations(IEnumerable<LevelUpInfo> anims) { foreach (var anim in anims) QueueAnimation(anim); }
		
		public void PlayAnimations()
		{
			if (_AnimQueue.Count > 0) {
				Timer.Instance.StartCoroutine(EngineUtils.ActionOnceTimeout(_Slider.FillDuration, PlayAnimations, a => _Slider.EventAnimComplete += a, a => _Slider.EventAnimComplete -= a));
				var anim = _AnimQueue.Dequeue();
				int start = this.Descending ? -anim.DeltaRange.Max : anim.DeltaRange.Min;
				int end = this.Descending ? -anim.DeltaRange.Min : anim.DeltaRange.Max;
				this.Level = anim.Level;
                SetRange(this.Descending ? anim.BracketRange.Max -1 : anim.BracketRange.Min, this.Descending ? anim.BracketRange.Min : anim.BracketRange.Max +1);
				_Slider.Animate(start, end);
				if (start == _Slider.Empty && null != this.EventLevelReached)
					this.EventLevelReached.Invoke(anim.Level);
			}
			else if (null != EventAnimsComplete)
				EventAnimsComplete.Invoke();
		}
	}

	/// <summary>
	/// This base class is needed for Unity Inspector.
	/// </summary>
	public abstract class DataToggle : MonoBehaviour
	{
		protected Toggle _Toggle;
		public Toggle ToggleComponent { get { return _Toggle; } }
		
		public bool ToggleState {
			get { return _Toggle == null ? false : _Toggle.isOn; }
			set { if(_Toggle == null) { this.Awake(); }_Toggle.isOn = value; }
		}
		
		protected void Awake()
		{
			_Toggle = GetComponent<Toggle>();
			_Toggle.onValueChanged.AddListener(OnToggleChanged);
		}
		
		protected virtual void OnToggleChanged(bool isOn) {}
	}

	/// <summary>
	/// Stores a serializable value that can be set via inspector.
	/// Fires an event containing the value and state of toggle upon change.
	/// </summary>
	public abstract class DataToggle<T> : DataToggle
	{
		[SerializeField]
		protected T _Data;
		public T DataValue { get { return _Data; } }

		public event Action<T, bool> EventToggled;

		protected override void OnToggleChanged(bool isOn)
		{
			if (null != this.EventToggled)
				this.EventToggled.Invoke(_Data, isOn);
		}
	}

	/// <summary>
	/// Manages a set of DataToggles. Derived classes should be [Serializable] for the Unity Inspector.
	/// </summary>
	public abstract class MultiDataToggle<T>
	{
		[SerializeField]
		protected DataToggle[] _BaseToggles;
		protected DataToggle<T>[] _Toggles;

		public event Action EventChanged;
		public IEnumerable<T> ActiveValues { get { return _Toggles.Where(t => t.ToggleState).Select(t => t.DataValue); } }
		
		public void Init()
		{
			_Toggles = _BaseToggles.Cast<DataToggle<T>>().ToArray();
			foreach (var tog in _Toggles)
				tog.EventToggled += OnToggleChanged;
		}
		
		public void SetAll(bool isOn)
		{
			foreach (var tog in _Toggles)
				tog.ToggleState = isOn;
		}

        public bool SetToSpecificState(IEnumerable<T> toSet)
        {
			bool isFiltered = false;
            foreach(var tog in _Toggles)
            {
                if (toSet.Contains(tog.DataValue))
				{
                    tog.ToggleState = true;
					isFiltered = true;
				}
                else
                    tog.ToggleState = false;
            }

			return isFiltered;
        }
		
		protected void OnToggleChanged(T data, bool state) {
			if (null != this.EventChanged)
				EventChanged.Invoke();
		}
	}
}