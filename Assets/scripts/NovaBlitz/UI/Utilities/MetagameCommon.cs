using System;
using System.Collections.Generic;
using Messages;

namespace NovaBlitz.Economy
{
	public interface IWallet
	{
		Dictionary<CurrencyType, int> Currencies {get;}
	}

	public interface IProduct
	{
        NovaCatalogItem Product {get;set;}
	}
	
	public interface IPurchaser
	{
		event Action<NovaCatalogItem, string> EventBeginPurchase;
		
		IProduct Current {get;}
		string CurrencyCode {get;set;}
		
		void BeginPurchase();
	}

	public class Wallet : IWallet
	{
		public Dictionary<CurrencyType, int> Currencies {get; protected set;}

		public Wallet()
		{
			this.Currencies = new Dictionary<CurrencyType, int>();
			foreach (var code in (CurrencyType[])Enum.GetValues(typeof(CurrencyType)))
				this.Currencies.Add(code, 0);
		}
	}

	public class PurchaseRequest
	{
		public string ItemID;
		public string StoreID;
		public string CatalogVersion;
		public string CurrencyCode;
		public uint Price;
	}

	public class SmartPackInfo
	{
		public string PackID;
		public int[] CardIDs;
	}
}