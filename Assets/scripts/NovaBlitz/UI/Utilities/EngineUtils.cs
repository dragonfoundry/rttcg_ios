using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

//#if UNITY_ANDROID
using Ionic.Zlib;
/*#else
     using System.IO.Compression;
#endif*/


public static class EngineUtils
{
	public static TResult ComponentCast<TSource, TResult>(TSource input) where TSource : class where TResult : class
	{
		TResult result = input as TResult;
		if (null != result)
			return result;

		Component comp = input as Component;
		return comp.GetComponent<TResult>();
	}

	public static IEnumerable<TResult> ComponentCast<TSource, TResult>(IEnumerable<TSource> input) where TSource : class where TResult : class
	{
		return input.Select(x => ComponentCast<TSource, TResult>(x)).OfType<TResult>();
	}

	public static IEnumerable<T> MultiConcat<T>(params IEnumerable<T>[] multi)
	{
		var result = multi[0];
		for (int i=1; i<multi.Length; ++i)
			result = result.Concat(multi[i]);
		return result;
	}

	/// <summary>
	/// Convenient way to apply the same logical operation to a set of anonymous conditions.
	/// </summary>
	public static bool MultiCondition(LogicalOp operation, params Func<bool>[] evaulators)
	{
		bool result = evaulators[0].Invoke();
		int end = evaulators.Length;
		
		switch (operation)
		{
		case LogicalOp.AND:
			for (int i=1; i<end; ++i) {
				result = result && evaulators[i].Invoke();
				if (!result) break;
			}
			break;
		case LogicalOp.OR:
			for (int i=1; i<end; ++i) {
				result = result || evaulators[i].Invoke();
				if (result) break;
			}
			break;
		}
		return result;
	}
	
	/// <summary>
	/// Convenient way to apply the same logical operation to a set of anonymous conditions.
	/// </summary>
	public static bool MultiPredicate<T>(T subject, LogicalOp operation, params Func<T, bool>[] predicates)
	{
		return MultiCondition(operation, predicates.Select<Func<T, bool>, Func<bool>>(p => () => p.Invoke(subject)).ToArray());
	}

	/// <summary>
	/// Advances a coroutine iterator by one step, nulling it when finished.
	/// </summary>
	/// <returns>The iterator's next value.</returns>
	/// <param name="iter">A reference to an iterator, passed by ref to allow nullage.</param>
	public static object AdvanceCoroutine(ref IEnumerator iter)
	{
		if (null == iter)
			return null;

		if (iter.MoveNext())
			return iter.Current;
		return iter = null;
	}

	public static IEnumerator Tween(float duration, System.Action<float> callback)
	{
		float startTime = Time.time;
		float endTime = startTime + duration;
		while (Time.time < endTime) {
			float progress = Mathf.InverseLerp(startTime, endTime, Time.time);
			callback.Invoke(progress);
			yield return null;
		}
		callback.Invoke(1f);
	}

	public static IEnumerator ActionTimeout(Action callback, float duration)
	{
		yield return new WaitForSecondsRealtime(duration);
		callback.Invoke();
	}

	/// <summary>
	/// Perturb a normalized value toward an exponential curve. A negative power draws the curve in reverse.
	/// </summary>
	/// <param name="nvalue">Value to adjust.</param>
	/// <param name="power">Power by which to raise. Negative values </param>
	/// <param name="weight">Interpolation between original and adjusted value.</param>
	public static float Sensitivity(float nvalue, float power, float weight = 1f)
	{
		// Negative values must be converted to positive and then back.
		float output;
		float sign = Mathf.Sign(nvalue);
		
		// Only normalized values will produce expected results.
		nvalue = Mathf.Clamp01(Mathf.Abs(nvalue));
		
		// Don't actually calculate negative powers.
		float absPower = Mathf.Abs(power);
		
		// Perform exponentiation. A negative power means "flip it."
		if (power < 0f)
			output = 1f - Mathf.Pow(1f - nvalue, absPower);
		else output = Mathf.Pow(nvalue, absPower);
		
		// Apply weighting.
		if (!Mathf.Approximately(weight, 1f))
		{
			weight = Mathf.Clamp01(weight);
			output = Mathf.Lerp(nvalue, output, weight);
		}
		
		// Revert sign.
		return output * sign;
	}

	/// <summary>
	/// Converts a linear range into a logarithmic decibel range used by UnityEngine.Audio.
	/// Input smaller than 0.01 returns min value instead of unknown number.
	/// </summary>
	/// <returns>Decibel value from -80db to 0db.</returns>
	/// <param name="linear">Linear value in 0-1 range.</param>
	public static float LinearToDecibel(float linear)
	{
		return linear < 0.01f ? -80f : Mathf.Log(linear)*20f;
	}

	/// <summary>
	/// Uses a proxy delegate to auto-unsubscribe upon receiving event.
	/// </summary>
	public static void ActionOnce(Action callback, Action<Action> sub, Action<Action> unsub)
	{
		Action proxy = null;
		proxy = () => {
			unsub.Invoke(proxy);
			proxy = null;
			callback.Invoke();
		};
		sub.Invoke(proxy);
	}

	/// <summary>
	/// Uses a proxy delegate to auto-unsubscribe upon receiving event.
	/// </summary>
	public static void ActionOnce<T>(Action<T> callback, Action<Action<T>> sub, Action<Action<T>> unsub)
	{
		Action<T> proxy = null;
		proxy = x => {
			unsub.Invoke(proxy);
			proxy = null;
			callback.Invoke(x);
		};
		sub.Invoke(proxy);
	}

	/// <summary>
	/// Uses a proxy delegate to auto-unsubscribe upon receiving event. Invokes callback if event is not received in time.
	/// </summary>
	public static IEnumerator ActionOnceTimeout(float duration, Action callback, Action<Action> sub, Action<Action> unsub)
	{
		bool received = false;
		Action proxy = null;
		proxy = () => {
			unsub.Invoke(proxy);
			proxy = null;
			received = true;
			callback.Invoke();
		};
		sub.Invoke(proxy);

		float end = Time.time + duration;
		while (!received && Time.time < end)
			yield return null;
		if (!received)
			proxy.Invoke();
	}

	/// <summary>
	/// Uses a proxy delegate to auto-unsubscribe upon receiving event. Invokes callback if event is not received in time.
	/// </summary>
	public static IEnumerator ActionOnceTimeout<T>(float duration, Action<T> callback, Action<Action<T>> sub, Action<Action<T>> unsub)
	{
		bool received = false;
		Action<T> proxy = null;
		proxy = x => {
			unsub.Invoke(proxy);
			proxy = null;
			received = true;
			callback.Invoke(x);
		};
		sub.Invoke(proxy);

		float end = Time.time + duration;
		while (!received && Time.time < end)
			yield return null;
		if (!received)
			proxy.Invoke(default(T));
	}

	public static IntRange[] GenerateBrackets(int[] steps, bool descending = false)
	{
		// Require first bracket zero for ascending, max for descending.
		if (descending)
			steps = (new int[]{int.MaxValue}).Concat(steps).ToArray();
		else if (steps[0] != 0)
			steps = (new int[]{0}).Concat(steps).ToArray();

		int len = steps.Length-1;
		var brackets = new IntRange[len];
		for (int i=0; i<len; ++i) {
			int min = Mathf.Min(steps[i], steps[i+1]);
			int max = Mathf.Max(steps[i], steps[i+1]);
			if (descending) ++min;
			else --max;
			brackets[i] = new IntRange(min, max);
		}
		return brackets;
	}

	public static Dictionary<int, IntRange> BracketsToDictionary(IntRange[] brackets, int offset = 0)
	{
		var dict = new Dictionary<int, IntRange>();
		for (int i=0; i<brackets.Length; ++i)
			dict[i+offset] = brackets[i];
		return dict;
	}

	public static int FindBracket(int value, IEnumerable<IntRange> brackets)
	{
		int i = 0;
		foreach (var range in brackets) {
			++i;
			if (value >= range.Min && value <= range.Max)
				return i;
		}
		return brackets.Count();
	}

	public static int FindBracket(int value, Dictionary<int, IntRange> brackets)
	{
        if (brackets == null || brackets.Count == 0)
            return 0;
        int highvalue = 0;
		foreach (var kvp in brackets) {
			if (value >= kvp.Value.Min && value <= kvp.Value.Max)
				return kvp.Key;
            highvalue = kvp.Key > highvalue ? kvp.Key : highvalue;
		}
		return highvalue+1;
	}

	public static IEnumerable<LevelUpInfo> GenerateLevelUpSteps(int startValue, int endValue, Dictionary<int, IntRange> brackets)
	{
		var q = new Queue<LevelUpInfo>();
		int startLevel = EngineUtils.FindBracket(startValue, brackets);
		int endLevel = EngineUtils.FindBracket(endValue, brackets);
		int min = Mathf.Min(startValue, endValue);
		int max = Mathf.Max(startValue, endValue);
        if (startLevel >= brackets.Count)
        {
            q.Enqueue(new LevelUpInfo()
            {
                Level = brackets.Count - 1,
                BracketRange = new IntRange()
                {
                    Min = startLevel,
                    Max = endValue,
                },
                DeltaRange = new IntRange()
                {
                    Min = startLevel,
                    Max = endValue,
                }
            });
            return q;
        }
        else if (endLevel >= brackets.Count)
            endLevel = brackets.Count - 1;
        for (int i = startLevel; i <= endLevel; ++i)
        {
            var bracket = brackets[i];
            q.Enqueue(new LevelUpInfo()
            {
                Level = i,
                BracketRange = bracket,
                DeltaRange = new IntRange()
                {
                    Min = Mathf.Max(min, bracket.Min),
                    Max = Mathf.Min(max, bracket.Max)
                }
            });
        }
		return q;
    }


    public static void CopyTo(Stream src, Stream dest)
    {
        try
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public static byte[] Zip(string str)
    {
        try
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    //msi.CopyTo(gs);
                    CopyTo(msi, gs);
                }

                return mso.ToArray();
            }
        }
        catch(Exception ex)
        {
            Debug.LogException(ex);
            return new byte[0];
        }
    }

    public static string Unzip(byte[] bytes)
    {
        try
        {
            using (var msi = new MemoryStream(bytes))
            {
                using (var mso = new MemoryStream())
                {
                    using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                    //using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                    {
                        //gs.CopyTo(mso);
                        CopyTo(gs, mso);
                    }
                    return Encoding.UTF8.GetString(mso.ToArray());
                }
            }
        }
        catch(Exception ex)
        {
            Debug.LogException(ex);
            return string.Empty;
        }
    }
}

/// <summary>
/// Observable wrapper for a list.
/// </summary>
public class ObservableList<T> : ICollection<T>
{
	private List<T> _List;
	public event System.Action<T> EventAdd;
	public event System.Action<T> EventRemove;
	public event System.Action EventClear;
	
	public int Count { get { return _List.Count; } }
	public bool IsReadOnly { get { return false; } }
	
	public bool Contains(T item) { return _List.Contains(item); }
	public void CopyTo(T[] array, int arrayIndex) { _List.CopyTo(array, arrayIndex); }
	IEnumerator<T> IEnumerable<T>.GetEnumerator() { return _List.GetEnumerator(); }
	IEnumerator IEnumerable.GetEnumerator() { return _List.GetEnumerator(); }
	
	public void Add(T item)
	{
		_List.Add(item);
		if (null != EventAdd)
			EventAdd.Invoke(item);
	}
	
	public bool Remove(T item)
	{
		bool result = _List.Remove(item);
		if (result && null != EventRemove)
			EventRemove.Invoke(item);
		return result;
	}
	
	public void Clear()
	{
		_List.Clear();
		if (null != EventClear)
			EventClear.Invoke();
	}
	
	public ObservableList()
	{
		_List = new List<T>();
	}
	
	public ObservableList(IEnumerable<T> members)
	{
		_List = new List<T>(members);
	}
}

public enum LogicalOp
{
	AND,
	OR
}

//Used to run Coroutines from
public class Timer : MonoBehaviour 
{
	private static Timer instance;
	public static Timer Instance 
	{
		get 
		{
			if(instance == null) 
			{
				var gB = new GameObject("Timer");
				instance = gB.AddComponent<Timer>();
			}
			return instance;
		}
	}

    private static Timer gameInstance;
    public static Timer GameInstance
    {
        get
        {
            if (gameInstance == null)
            {
                var gB = new GameObject("GameTimer");
                gameInstance = gB.AddComponent<Timer>();
            }
            return gameInstance;
        }
    }
}

[Serializable]
public struct IntRange : IEquatable<IntRange>, IComparable<IntRange>
{
	public int Min;
	public int Max;
	
	public IntRange(int min, int max)
	{
		this.Min = min;
		this.Max = max;
	}
	
	public int Magnitude { get { return Mathf.Abs(this.Max - this.Min); } }
	public int CompareTo(IntRange other) { return this.Magnitude.CompareTo(other.Magnitude); }
	public bool Equals(IntRange other) { return this.Min == other.Min && this.Max == other.Max; }
	public override bool Equals(object obj) { return this.Equals((IntRange)obj); }
	public override int GetHashCode() { return Min^Max; }
	public override string ToString() { return string.Format("IntRange: {0} to {1}", Min, Max); }
	
	public static bool operator ==(IntRange r1, IntRange r2) { return r1.Equals(r2); }
	public static bool operator !=(IntRange r1, IntRange r2) { return !r1.Equals(r2); }
}

public class LevelUpInfo
{
	public int Level;
	public IntRange BracketRange;
	public IntRange DeltaRange;
}

