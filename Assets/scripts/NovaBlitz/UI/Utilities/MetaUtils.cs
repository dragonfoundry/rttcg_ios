using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using System.Collections.Generic;
using NovaBlitz.UI;
using Messages;

namespace NovaBlitz.UI
{
    public static class Server
    {
        public static DateTime Time { get { return DateTime.UtcNow.AddHours(-8); } }
    }

}

namespace NovaBlitz.Economy
{
	public static class NBEconomy
	{
		public const string CATALOG_MAIN = 		"1";
		public const string CATALOG_PUBLIC = 	"store_catalog";
		public const string CATALOG_KEY = 		"op_catalog";
		public const string CATALOG_BOOSTER = 	"openable_booster";
		public const string CATALOG_CARD = 		"card_catalog";
		public const string CATALOG_SMARTPACK = "smartpack";
        public const string CATALOG_BOOSTER_SUFFIX = "_booster";
        public const string CATALOG_AVATAR_PREFIX = "avatar_";
        public const string CATALOG_CARDBACK_PREFIX = "cardback_";

        public const string STORE_BOOSTER = "openable_booster";
		public const string STORE_MAIN = "main_store";
        public const string STORE_AVATAR = "avatar_store";
        public const string STORE_CARDBACK = "cardback_store";
        public const string STORE_OFFER = "offer_store";

        public const string ITEMCLASS_STORE_ITEM = "store_item";
		public const string ITEMCLASS_CARD = 	"card";
		public const string ITEMCLASS_BOOSTER = "openable_booster";
		public const string ITEMCLASS_BOOSTER_DROPS = 	"booster";
		public const string ITEMCLASS_KEY = 	"key";
		public const string ITEMCLASS_AVATAR = 	"avatar";
		public const string ITEMCLASS_CARDBACK = "cardback";
		public const string ITEMCLASS_PLAYMAT = 	"playmat";
        
		public const string ITEMID_KEY_CLEAGUE = "constr_key";
		public const string ITEMID_KEY_DLEAGUE = "draft_key";
		public const string ITEMID_KEY_TOURNEY = "tourney_key";
		public const string ITEMID_KEY_SEASON = "season_key";
		public const string ITEMID_KEY_ANNUAL = "annual_key";
		public const string ITEMID_KEY_AVATAR1 = "avatar_0";
		public const string ITEMID_KEY_CARDBACK1 = "cardback_0";
        public const string ITEMID_BASIC_PACK = "pack_basic";
        public const string ITEMID_SMART_PACK = "pack_smart";
        public const string ITEMID_MEGA_PACK = "pack_mega";


        public const string PLAYER_STATS_RANK = "rank";
        public const string PLAYER_STATS_RATING = "rating";
        public const string PLAYER_STATS_MMR = "mmr";


        public const int MAX_ASPECT_REWARDS = 30;
        public const string GRANT_TEXT = "{0}/{1}\n{2}";

        public static readonly string[] STORE_ALL = {
			NBEconomy.STORE_BOOSTER,
			NBEconomy.STORE_MAIN, 
            NBEconomy.STORE_AVATAR,
            NBEconomy.STORE_CARDBACK
        };
	}

    public static class NovaAchievements
    {

        public const string BEST_DRAFT = "best_result_draft";
        public const string BEST_LEAGUE = "best_result_constr";
        public const string BEST_EVENT = "best_result_tourney";
        public const string GAMES_WON = "games_won";
        public const string CURRENT_WIN_STREAK = "current_win_streak";
        public const string QPS_WON = "qps_won";
        public const string LIFETIME_LEVEL = "lifetime_level";
        public const string LEADERBOARD = "leaderboard";

        public const string WINS_CASUAL = "casual_wins";
    }

	public static class CurrencyUtils
	{
        private static Dictionary<CurrencyType, string> SingularNameMap = new Dictionary<CurrencyType, string>(){
			{ CurrencyType.RM, string.Empty },
			{ CurrencyType.CR, I2.Loc.ScriptLocalization.Credit },
            { CurrencyType.NG, I2.Loc.ScriptLocalization.Gem },
			{ CurrencyType.SP, I2.Loc.ScriptLocalization.Smart_Pack },
			{ CurrencyType.NC, I2.Loc.ScriptLocalization.NanoBot },
			{ CurrencyType.QP, I2.Loc.ScriptLocalization.Qualifier_Point },
			{ CurrencyType.SQ, string.Empty },
			{ CurrencyType.FC, I2.Loc.ScriptLocalization.Free_League },
			{ CurrencyType.FD, I2.Loc.ScriptLocalization.Free_Draft },
			{ CurrencyType.FT, I2.Loc.ScriptLocalization.Free_Tournament }
		};

        private static Dictionary<CurrencyType, string> PluralNameMap = new Dictionary<CurrencyType, string>(){
            { CurrencyType.RM, string.Empty },
            { CurrencyType.CR, I2.Loc.ScriptLocalization.Credits },
            { CurrencyType.NG, I2.Loc.ScriptLocalization.Gems },
            { CurrencyType.SP, I2.Loc.ScriptLocalization.Smart_Packs },
            { CurrencyType.NC, I2.Loc.ScriptLocalization.NanoBots },
            { CurrencyType.QP, I2.Loc.ScriptLocalization.Qualifier_Points },
            { CurrencyType.SQ, string.Empty },
            { CurrencyType.FC, I2.Loc.ScriptLocalization.Free_Leagues },
            { CurrencyType.FD, I2.Loc.ScriptLocalization.Free_Drafts },
            { CurrencyType.FT, I2.Loc.ScriptLocalization.Free_Tournaments }
        };


        public static CurrencyType GetCode(string str)
		{
            CurrencyType code;
            GameData.CurrencyDictionary.TryGetValue(str, out code);
            return code;
		}

		public static string GetName(CurrencyType code, int amount)
		{
			string name;
			if (amount == 1 && SingularNameMap.TryGetValue(code, out name))
				return name;
            else if (amount != 1 && PluralNameMap.TryGetValue(code, out name))
                return name;
            return null;
		}

		public static Sprite GetSprite(CurrencyType code)
		{
            Sprite sprite = null;
			switch (code)
            {
            case CurrencyType.FC:
                sprite = Resources.Load("gtrgsgf") as Sprite;
                return sprite;
            case CurrencyType.FD:
                return sprite;
            case CurrencyType.FT:
                return sprite;
			case CurrencyType.QP:
				return sprite;
			case CurrencyType.SP:
			case CurrencyType.NG:
				return sprite;
			case CurrencyType.RM:
				return sprite;
			case CurrencyType.CR:
				return sprite;
			case CurrencyType.NC:
			case CurrencyType.NR:
			case CurrencyType.NE:
			case CurrencyType.NL:
				return sprite;
            default:
                return null;
			}
		}

		public static string FormatPrice(CurrencyType code, uint amount)
		{
			string price;
			switch (code) {
			case CurrencyType.RM:
                price = string.Format("${0:N2}", amount / 100m);
                //price = string.Format("${0:N0}.{1:F2}", amount/100, amount%100); //TODO localize
                break;
			default:
				price = string.Format("{0:N0} {1}", amount, code);
				break;
			}
			return price;
		}
	}

    public static class TimeUtils
    {
        public static string FormatDateTime(DateTime dateTime)
        {
            // Long-term - format by language & country time format
            return dateTime.ToString("ddd d MMM h:mm");
        }

        public static string FormatTime(DateTime time)
        {
            // Long-term - format by language & country time format
            return time.ToString("h:mm");
        }

        public static string FormatTimeDifference(DateTime earlierTime, DateTime laterTime)
        {
            var span = laterTime - earlierTime;
            StringBuilder builder = new StringBuilder();
            builder.Append(span.Hours + span.Days * 24);
            builder.Append(':');
            if (span.Minutes < 10)
                builder.Append('0');
            builder.Append(span.Minutes);
            builder.Append(':');
            if (span.Seconds < 10)
                builder.Append('0');
            builder.Append(span.Seconds);
            // Long-term - format by language & country time format
            return builder.ToString();
        }
    }
    public class PlayerProfileUtils
    {
        private const string _RATING = "_rating";
        private const int PLAT_FLOOR = 20000;
        private const int GOLD_FLOOR = 10000;

        public static string RatingKey
        {
            get
            {
                var time = Server.Time;
                return new StringBuilder()
                    .Append(time.Year)
                    .Append('_')
                    .Append(time.Month)
                    .Append(_RATING)
                    .ToString();
            }
        }
        public static string PrevRatingKey
        {
            get
            {
                var time = Server.Time;
                return new StringBuilder()
                    .Append(time.Month == 1 ? time.Year - 1 : time.Year)
                    .Append('_')
                    .Append(time.Month == 1 ? 12 : time.Month)
                    .Append(_RATING)
                    .ToString();
            }
        }
        public static string RankKey
        {
            get
            {
                var time = Server.Time;
                return new StringBuilder()
                    .Append(time.Year)
                    .Append('_')
                    .Append(time.Month)
                    .Append("_rank")
                    .ToString();
            }
        }


        public static int GetRank(Dictionary<string, int> stats)
        {
            int rank;
            int mmr;
            if (stats.TryGetValue(NBEconomy.PLAYER_STATS_RANK, out rank))
            {
            }
            else if (stats.TryGetValue(RankKey, out rank))
            {
            }
            else
            {
                stats.TryGetValue(NBEconomy.PLAYER_STATS_MMR, out mmr);
                rank = mmr <= 0 ? 30 : 31 - mmr;
                rank = rank <= 0 ? -1 : rank;
            }
            return rank;
        }


        public static int GetRating(Dictionary<string, int> stats)
        {
            int rating;
            if (!stats.TryGetValue(RatingKey, out rating))
            {
                if (stats.TryGetValue(PrevRatingKey, out rating))
                {
                    rating = Math.Max(PLAT_FLOOR, Math.Min(GOLD_FLOOR, rating / 2));
                }
                else if (stats.TryGetValue(NBEconomy.PLAYER_STATS_RATING, out rating))
                {
                    rating = Math.Min(rating, GOLD_FLOOR);
                }
            }
            return rating;
        }

        public static int CalcRankFromRating(Dictionary<int, IntRange> rankLevels, int rating)
        {
            int brack = EngineUtils.FindBracket(rating, rankLevels);
            return brack + 1;
            /*for (int i = 0; i < PlayFabDataStore.Instance.rankThresholds.Length; i++)
            {
                if (rating < PlayFabDataStore.Instance.rankThresholds[i])
                    return i;
            }
            return PlayFabDataStore.Instance.rankThresholds.Length;*/
        }
    }
    
}
