using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using NovaBlitz.Economy;
using Messages;
using I2.Loc;

namespace NovaBlitz.UI
{
	public static class QueryUtils
	{
        private const StringComparison scomp = StringComparison.CurrentCultureIgnoreCase;
        private const string COLON = ":";
        private const string PLUS = "+";
        private const string MINUS = "-";
        public static IEnumerable<CardData> FilterCardsByString(IEnumerable<CardData> cards, string phrase, Dictionary<int, int> ownedCards)
		{
			if (string.IsNullOrEmpty(phrase))
				return cards;

            // lowercase phrase

            // set localizedsearchablecardtext to the right value
            string[] split = phrase.Split();
            if (split == null)
                return cards;

            for (int i = 0; i < split.Count(); i++)
            {
                var searchTerm = split[i];
                if (string.IsNullOrEmpty(searchTerm))
                {
                }
                else if (searchTerm.IndexOf(COLON, scomp) >= 0)
                {
                    // it's a special search
                    var subsplit = searchTerm.Split(':');
                    int count = subsplit.Count();
                    IntRange range = new IntRange();
                    bool rangeSet = false;
                    if (count < 1)
                    {

                    }
                    else if (count == 1)
                    {
                        // do nothing. null is good.
                    }
                    else
                    {
                        //string value = subsplit[1];
                        IntRange activateRange;
                        if (TryGetIntRange(subsplit[0], out activateRange))
                        {
                            cards = cards.Where(c => c.ActivationCost != null && c.ActivationCost.Value <= activateRange.Max && c.ActivationCost.Value >= activateRange.Min);
                            continue;
                        }
                        else
                        {
                            rangeSet = TryGetIntRange(subsplit[1], out range);
                        }
                    }
                    // regex subsplit 1 into a number or range
                    string key = subsplit[0];
                    if(rangeSet == false)
                    {
                        if (GameData.SearchArtist.IndexOf(key, scomp) >= 0 && !string.IsNullOrEmpty(subsplit[1]))
                        {
                            cards = cards.Where(c => c.ArtistCredit.IndexOf(subsplit[1], scomp) >= 0);
                        }
                        if (GameData.SearchExtra.IndexOf(key, scomp) >= 0)
                        {
                            cards = cards.Where(c => { int owned; ownedCards.TryGetValue(c.CardID, out owned); return owned > 3; });
                            // TODO - extra cards
                            //cards = cards.Where(c => c.ArtistCredit.IndexOf(subsplit[1], scomp) >= 0);
                        }
                    }
                    else if (GameData.SearchEnergy.IndexOf(key, scomp) == 0 || GameData.SearchMana.IndexOf(key, scomp) == 0 || GameData.SearchCost.IndexOf(key, scomp) == 0)
                    {
                        cards = cards.Where(c => c.Cost <= range.Max && c.Cost >= range.Min);
                    }
                    else if (GameData.SearchAttack.IndexOf(key, scomp) == 0 || GameData.SearchStrength.IndexOf(key, scomp) == 0)
                    {
                        cards = cards.Where(c => c.CardType == CardType.Unit && c.AttackPower <= range.Max && c.AttackPower >= range.Min);
                    }
                    else if (GameData.SearchHealth.IndexOf(key, scomp) == 0)
                    {
                        cards = cards.Where(c => c.CardType == CardType.Unit && c.Health <= range.Max && c.Health >= range.Min);
                    }
                    else if (GameData.SearchOwned.IndexOf(key, scomp) == 0)
                    {
                        cards = cards.Where(c => { int owned; ownedCards.TryGetValue(c.CardID, out owned); return owned >= range.Min && owned <= range.Max; });
                        // TODO
                    }
                    else if (GameData.SearchActivated.IndexOf(key, scomp) == 0)
                    {
                        // TO DO
                        cards = cards.Where(c => c.ActivationCost != null && c.ActivationCost.Value <= range.Max && c.ActivationCost.Value >= range.Min);
                    }
                }
                else
                {
                    cards = cards.Where(c => c.LocalizedSearchableCardText.IndexOf(searchTerm, scomp) >= 0);
                }
            }

            // set the search terms by localized language

            // First, break up by spaces into an array of strings
            // then add a search for each string


            // second, look for keywords, by localized version.
            // cost/mana/energy:
            // strength/attack:
            // health:
            // activate: /activated:
            // owned:
            // artist:
            // extra:

            // 1
            // 1-3
            // 1+
            // 5-

            // append to searchable text
            // power
            // unit
            // item
            // subtype
            // common
            // rare
            // epic
            // legendary

            return cards;
		}

        private static bool TryGetIntRange(string value, out IntRange range)
        {
            int firstValue;
            int secondValue;
            int plus = value.IndexOf(PLUS, scomp);
            int minus = value.IndexOf(MINUS, scomp);
            if (plus >= 0)
            {
                var highLow = value.Split('+');
                if (int.TryParse(highLow[0], out firstValue))
                {
                    range= new IntRange { Min = firstValue, Max = int.MaxValue };
                    return true;
                }
            }
            else if (minus >= 0)
            {
                var highLow = value.Split('-');
                if (int.TryParse(highLow[0], out firstValue))
                {
                    if (int.TryParse(highLow[1], out secondValue))
                    {
                        range = new IntRange { Min = firstValue, Max = secondValue };
                        return true;
                    }
                    else
                    {
                        range = new IntRange { Min = int.MinValue, Max = firstValue };
                        return true;
                    }
                }
            }
            else if (int.TryParse(value, out firstValue))
            {
                range = new IntRange { Max = firstValue, Min = firstValue };
                return true;
            }
            range = new IntRange();
            return false;
        }


        public static int GetIdNumber(string itemId)
        {
            int avatarNumber = 0;
            if (itemId == null)
                return avatarNumber;
            try
            {
                var resultString = Regex.Match(itemId, @"\d+").Value;
                int.TryParse(resultString, out avatarNumber);
            }
            catch { }
            return avatarNumber;
        }
    }


    public static class StringUtils
    {
        public static string HashEmail(string rawEmail)
        {
            char[] charArray = rawEmail.ToCharArray();
            {
                for (int i = 0; i < charArray.Length; i++)
                {
                    var character = charArray[i];
                    int num = (int)character;
                    num += i;

                    while (num < 'a' || num > 'z')
                    {
                        if (num < 'a')
                        {
                            num += 26;
                        }
                        else if (num > 'z')
                        {
                            num -= 26;
                        }
                    }
                    if (num > 'm')
                    {
                        num -= 13;
                    }
                    else
                    {
                        num += 13;
                    }

                    charArray[i] = (char)num;
                }

            }
            string hash = new string(charArray) + NO_EMAIL;
            return hash;
        }
        private const string NO_EMAIL = "@noemail.novablitz";
    }

    public static class TournamentUtils
    {
        public static Dictionary<GameFormat, string> TournamentFormatNames = new Dictionary<GameFormat, string>{
            { GameFormat.Casual, I2.Loc.ScriptLocalization.Casual },
            { GameFormat.Practice, I2.Loc.ScriptLocalization.Practice },
            { GameFormat.Challenge, I2.Loc.ScriptLocalization.Challenge },
            { GameFormat.Draft, I2.Loc.ScriptLocalization.Draft },
            { GameFormat.Constructed, I2.Loc.ScriptLocalization.League },
            { GameFormat.Tournament, I2.Loc.ScriptLocalization.Tournament },
            { GameFormat.Monthly, I2.Loc.ScriptLocalization.Tournament },
            { GameFormat.Annual, I2.Loc.ScriptLocalization.Tournament }
        };
    }

	internal enum PrefabMain
	{
		BackgroundMainMenu,
		HomeButtonsPanel,
		HeaderPanel,
		ProfilePanel,
		FriendListDockPanel,
		PlayScreenColumn,
		DeckListPanel,
		DeckListDockPanel,
		CardBrowserPanel,
		CardListPanel,
		DeckStatsPanel,
		DraftPackBrowserPanel,
		DraftCardListPanel,
		NewMessageDialogPanel,
		ConfirmDialogPanel,
		ProgressLoadingScreenPanel,
		TutorialMenuPanel,
		StorePanel,
		OpenPackWindow,
		AvatarSelectorPanel,
		CardBackSelectorPanel,
		InGameMenuPanel,
		PackOpenerPanel,
		ForgotPasswordPanel,
		DeckScreenShotPanel,
		ConfirmPurchaseDialogPanel,
		LeaderboardPanel,
		LoginDialogNewFlowPanel,
		EventScreenColumn,
		ReconnectDialogPanel,
		EndGameDialogPanel,
		LanguageSelector,
		MobileTermsPanel,
		TwoTouchOverlay,
		HintPopup,
		RatingPanel,
		RedeemCouponPanel,
		SettingsPanel,
		GameLog,
		GameLogList,
		GameEndPrizeWindow,
		ReportBugDialog,
	}

	internal enum PrefabSession
	{
		StartingHandPanel,
		EndGameDialogPanel,
		TutorialPopUpDialogPanel,
		InGameMenuPanel,
		StartGameDialogPanel,
		GameLog,
		TwoTouchOverlay,
	}

    internal static class NovaConfig
	{
		public const string PATH_UICFG = "NovaUIConfig";
        public const string PATH_SESSCFG = "NovaSessionUIConfig";
        public const string PATH_RESMAP = "ResourceMap";

		public const string KEY_SFXVOL = "SFXVolume";
		public const string KEY_MUSICVOL = "MusicVolume";
		public const string KEY_SVIP = "ServerIP";
		public const string KEY_SVPORT = "ServerPort";
		public const string KEY_AVATAR = "selected_avatar";
		public const string KEY_CARDBACK = "selected_card_back";
        public const string KEY_DECKLIST = "deck_list";
        //public const string KEY_ANDROID_ID = "android_unique_id";
        public const string KEY_UI_HINTS = "ui_hints";
        public const string KEY_TRACKING_DATA_CACHE = "tracking_data_cache";
        public const string KEY_WINDOWED_RESOLUTION_X = "win_res_x";
        public const string KEY_WINDOWED_RESOLUTION_Y = "win_res_y";

        public const string GAME_LOG_FILE_NAME = "game_log_file.json";
        public const string CHAT_LOG_FILE_NAME = "_chat_log_file.json";

        public const string RATE_GAME_SUFFIX = "_rate_game_data";

        public const int 	DEF_VOLUME = 60;
        public const int    DEF_WINDOWED_RESOLUTION_X = 1600;
        public const int    DEF_WINDOWED_RESOLUTION_Y = 900;

        public const int CHAT_LOG_MAX_SIZE = 200;
    }

	public class MetaProfile
	{
		public string ID = null;
		public string _Name = string.Empty;
        public string Name { get { return !string.IsNullOrEmpty(_Name) ? _Name : ScriptLocalization.Player; } set { _Name = value; } }
        public bool IsNameSet { get { return !string.IsNullOrEmpty(_Name); } }
		public string AvatarID = NBEconomy.ITEMID_KEY_AVATAR1;
		public string CardBackID = NBEconomy.ITEMID_KEY_CARDBACK1;
		public string PlayFabID = null;
		public string PlayFabSessionToken = null;
		public string PhotonAuthToken = null;
		public int CurrentRating = 0;
        public MetaProfile() { }
	}

	[Serializable] public class MultiIntToggle : MultiDataToggle<int> {}
	[Serializable] public class MultiCardTypeToggle : MultiDataToggle<CardType> {}
	[Serializable] public class MultiCardAspectToggle : MultiDataToggle<CardAspect> {}
	[Serializable] public class MultiCardRarityToggle : MultiDataToggle<CardRarity> {}
}
