﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class SelectDeckSignal : Signal<DeckData> {}
	public class SelectDeckCommand : Command
	{
		// Signal Parameters
		[Inject] public DeckData DeckData {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public DeckSelectedSignal DeckSelectedSignal {get;set;}
		
		public override void Execute()
		{
			this.PlayerProfile.CurrentDeck = this.DeckData;
			this.DeckSelectedSignal.Dispatch(this.DeckData);
		}
	}
}