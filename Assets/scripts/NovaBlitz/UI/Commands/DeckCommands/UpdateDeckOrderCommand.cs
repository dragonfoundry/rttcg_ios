﻿using UnityEngine;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class UpdateDeckOrderSignal : Signal<List<string>> {}
	public class UpdateDeckOrderCommand : Command
	{
		// Signal Parameters
		[Inject] public List<string> DeckOrderList {get;set;}
		
		// Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		
		public override void Execute ()
		{
			// Initialize a new deckList using the DeckOrderList and the existing DeckData instances in the profile
			List<DeckData> newDeckList = new List<DeckData>();
			for(int i=0;i<this.DeckOrderList.Count; i++)
			{
				DeckData existingDeckData = this.PlayerProfile.DeckList.Where(d=>d.ID == this.DeckOrderList[i]).FirstOrDefault();
				newDeckList.Add(existingDeckData);
			}
			
			// Find any decks in the existing deckList that aren't in the new one
			var leftoverDecks = this.PlayerProfile.DeckList.Except(newDeckList);
			
			// Add the decks not listed in the DeckOrderList to the newDeckList so it wont ever be destructive
			foreach(DeckData deck in leftoverDecks)
			{
				newDeckList.Add(deck);
			}
			
			// Replace the deckList in the profile with the newly sorted one
			this.PlayerProfile.DeckList = newDeckList;
		}
	}
}
