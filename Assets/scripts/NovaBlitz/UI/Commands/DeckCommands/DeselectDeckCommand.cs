﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI.DataContracts;
using LinqTools;

namespace NovaBlitz.UI
{
	public class DeselectDeckSignal : Signal<DeckData> {}
	public class DeselectDeckCommand : Command
	{
		// Signal Parameters
		[Inject] public DeckData DeckData {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public DeckDeselectedSignal DeckDeselectedSignal {get;set;}
        [Inject] public SelectDeckSignal SelectDeckSignal { get; set; }


        public override void Execute()
		{
            DeckData deck = null;
            for( int i = 0; i < this.PlayerProfile.DeckList.Count; i++)
            {
                if(DeckData.ID == this.PlayerProfile.DeckList[i].ID)
                {
                    if (i > 0)
                        deck = this.PlayerProfile.DeckList[i - 1];
                    else if (i == 0 && this.PlayerProfile.DeckList.Count > 1)
                        deck = this.PlayerProfile.DeckList[i + 1];
                    break;
                }
            }
            if (deck != null)
            { 
                this.SelectDeckSignal.Dispatch(deck);
            }
            else
            {
                this.PlayerProfile.CurrentDeck = null;
                this.DeckDeselectedSignal.Dispatch(this.DeckData);
            }

        }
	}
}