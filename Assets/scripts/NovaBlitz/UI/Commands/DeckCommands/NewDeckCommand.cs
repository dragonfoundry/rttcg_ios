﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class NewDeckSignal : Signal {}
	public class NewDeckCommand : Command
	{
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public SelectDeckSignal SelectDeckSignal {get;set;}
		
		public override void Execute ()
		{
			var newDeck = new DeckData(Messages.GameFormat.Casual);
			newDeck.Name = I2.Loc.ScriptLocalization.New_Deck;
			this.SelectDeckSignal.Dispatch(newDeck);
		}
	}
}
