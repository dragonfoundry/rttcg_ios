﻿using System;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using NovaBlitz.Economy;
using NovaBlitz.Game;
using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;

namespace NovaBlitz.UI
{
    public class CraftItemSignal : Signal<CraftItemData> { }
    public class CraftItemCommand : Command
    {
        [Inject] public CraftItemData CraftItemData { get; set; }
        [Inject] public ClientService ClientService { get; set; }

        public override void Execute()
        {
            if (CraftItemData == null) // missing data
                Debug.LogError("Craft Item Data is null");
            /*else if (CraftItemData.ExpectedCraftPrice == 0)
                Debug.LogErrorFormat("Craft Item Data: Price is null");
            else if (CraftItemData.ExpectedScrapReturn == 0)
                Debug.LogErrorFormat("Craft Item Data: Scrap return is null");*/
            else
            {
                ClientService.SendMessage(new CraftRequest {
                    CardId = CraftItemData.CardId,
                    RequestType = CraftItemData.RequestType,
                    NumberToCraft = CraftItemData.NumberToCraft,
                    ExpectedCraftPrice = (int)CraftItemData.ExpectedCraftPrice,
                    ExpectedScrapReturn = (int)CraftItemData.ExpectedScrapReturn });
            }

            /*
            else if (CraftItemData.NumberToCraft < 0 && CraftItemData.ItemId == null && CraftItemData.ItemInstanceId != null)
            {
                // This is a DISENCHANT request
                var request = new OpenContainerRequest();
                request.ItemID = CraftItemData.ItemInstanceId;
                OpenContainerSignal.Dispatch(request);
            }
            else if (CraftItemData.Amount > 0 && CraftItemData.ItemId != null && CraftItemData.ItemInstanceId == null)
            {
                // This is a CRAFT request
                var request = new PurchaseRequest();
                request.CurrencyCode = CurrencyType.NC.ToString();
                request.Price = CraftItemData.Price;
                request.ItemID = CraftItemData.ItemId;
                VirtualPurchaseSignal.Dispatch(request);
            }
            else // malformed request
                Debug.LogErrorFormat("Craft Signal malformed. ItemId = {0}, InstanceId = {1}", CraftItemData.ItemId, CraftItemData.ItemInstanceId);*/
        }
    }

    public class CraftItemData
    {
        public uint ExpectedScrapReturn { get; private set; }
        public uint ExpectedCraftPrice { get; private set; }
        public int NumberToCraft { get; private set; }
        public int CardId { get; private set; }
        public CraftRequestType RequestType { get; private set; }
        public CraftItemData(int cardId, CraftRequestType requestType, int numberToCraft, uint expectedScrapReturn, uint expectedCraftPrice)
        {
            ExpectedScrapReturn = expectedScrapReturn;
            ExpectedCraftPrice = expectedCraftPrice;
            NumberToCraft = numberToCraft;
            CardId = cardId;
            RequestType = requestType;
        }
    }
}
