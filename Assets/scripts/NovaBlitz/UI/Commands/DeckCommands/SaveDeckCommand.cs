using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class SaveDeckSignal : Signal<DeckData> {}
	public class SaveDeckCommand : Command
	{
		// Signal Parameters
		[Inject] public DeckData DeckData {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public UpdateDeckListSignal UpdateDeckListSignal {get;set;}
		
		public override void Execute()
		{
			this.DeckData.QueryAspects();
            this.DeckData.SetDeckArt();
            if (!this.PlayerProfile.DeckList.Contains(this.DeckData)) {
				this.DeckData.ID =  System.Guid.NewGuid().ToString();
				this.PlayerProfile.DeckList.Insert(0, this.DeckData);
            }

            DeckListDataContract dataContract = this.PlayerProfile.GetDeckListDataContract();
            if (dataContract != null)
            {
                this.UpdateDeckListSignal.Dispatch(dataContract, true);
            }
			Debug.LogFormat("DECK SAVED: {0}", DeckData.ToString());
		}
	}
}
