﻿using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public enum CardCollectionType
	{
		MyCards = 0,
		AllCards = 1,
		NewCards = 2,
		UnownedCards = 3,
		DeckCards = 4
	}
	public class CardFilterParams
	{
		public CardCollectionType CollectionType;
		public string SearchPhrase;
		public int[] Costs = new int[0];
		public CardType[] Types = new CardType[0];
		public CardAspect[] Aspects = new CardAspect[0];
		public CardRarity[] Rarities = new CardRarity[0];

		public static CardFilterParams All {
			get { return new CardFilterParams{
					CollectionType = CardCollectionType.MyCards,
					SearchPhrase = string.Empty,
					Costs = Enumerable.Range(0,8).ToArray(),
					Types = (CardType[])Enum.GetValues(typeof(CardType)),
					Aspects = (CardAspect[])Enum.GetValues(typeof(CardAspect)),
					Rarities = (CardRarity[])Enum.GetValues(typeof(CardRarity))
				}; }
		}

		public static CardFilterParams Expand(CardFilterParams filterParams)
		{
			var all = CardFilterParams.All;
			all.CollectionType = filterParams.CollectionType;
			all.SearchPhrase = filterParams.SearchPhrase;
			if (null != filterParams.Costs && filterParams.Costs.Length > 0) all.Costs = filterParams.Costs;
			if (null != filterParams.Types && filterParams.Types.Length > 0) all.Types = filterParams.Types;
			if (null != filterParams.Aspects && filterParams.Aspects.Length > 0) all.Aspects = filterParams.Aspects;
			if (null != filterParams.Rarities && filterParams.Rarities.Length > 0) all.Rarities = filterParams.Rarities;
			return all;
		}
	}

	public class FilterCardsSignal : Signal<CardFilterParams> {};
	public class FilterCardsCommand : Command
	{
		[Inject] public CardFilterParams SignalParams {get;set;}
		
		[Inject] public GameData GameData {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public CardsFilteredSignal CardsFilteredSignal {get;set;}
		
		private const int HI_COST = 7;
		
		public override void Execute()
		{
			this.PlayerProfile.CurrentCardFilter = this.SignalParams;
			var allCards = GameData.CardDictionary.Values.AsEnumerable();
			var filtered = allCards;

			switch(this.SignalParams.CollectionType)
			{
				case CardCollectionType.MyCards:
					//filtered = filtered.Where(c => this.PlayerProfile.OwnedCards.ContainsKey(c.CardID));
				break;
				case CardCollectionType.NewCards:
                    //filtered = filtered.Where(c => this.PlayerProfile.OwnedCards.ContainsKey(c.CardID));
				break;
				case CardCollectionType.DeckCards:
					if(this.PlayerProfile.CurrentDeck != null)
					{
						filtered = filtered.Where(c=>this.PlayerProfile.CurrentDeck.DeckList.Contains(c.CardID));
					}
				break;
				case CardCollectionType.UnownedCards:
					filtered = filtered.Where(c => this.PlayerProfile.OwnedCards.ContainsKey(c.CardID)==false);
				break;
			}
			
			if (this.SignalParams.Costs.Any() && this.SignalParams.Costs.Count() <= HI_COST) {
				bool includeHi = this.SignalParams.Costs.Contains(HI_COST);
				filtered = filtered.Where(c => {
					if (c.Cost < HI_COST)
						return this.SignalParams.Costs.Contains(c.Cost);
					return includeHi;
				});
			}
			if (this.SignalParams.Aspects.Any() && this.SignalParams.Aspects.Length < Enum.GetValues(typeof(CardAspect)).Length)
				filtered = filtered.Where(c => this.SignalParams.Aspects.Contains(c.Aspect));
			if (this.SignalParams.Rarities.Any() && this.SignalParams.Rarities.Length < Enum.GetValues(typeof(CardRarity)).Length)
				filtered = filtered.Where(c => this.SignalParams.Rarities.Contains(c.Rarity));
			if (this.SignalParams.Types.Any() && this.SignalParams.Types.Length < Enum.GetValues(typeof(CardType)).Length)
				filtered = filtered.Where(c => this.SignalParams.Types.Contains(c.CardType));
			filtered = QueryUtils.FilterCardsByString(filtered, this.SignalParams.SearchPhrase, this.PlayerProfile.OwnedCards);

			// Always make sure we filter cards agains thte CardProductCache
			if(this.SignalParams.CollectionType == CardCollectionType.NewCards)
			{
				// Apply a custom sort order when we are looking at new cards
				 filtered = filtered
				 	.Where(c => this.GameData.LegalCards.Contains(c.CardID))
                    .OrderBy(c => this.PlayerProfile.OwnedCards.ContainsKey(c.CardID) ? 0 : 1)
                    .ThenBy(card => this.PlayerProfile.NewCards.Contains(card.CardID) ? 0 : 1)
                    .ThenBy(card => card.Cost).ThenBy(cx => cx.Name);
			}
			else
			{
				filtered = filtered
					.Where(c => this.GameData.LegalCards.Contains(c.CardID))
					.OrderBy(c => this.PlayerProfile.OwnedCards.ContainsKey(c.CardID) ? 0 : 1).ThenBy(card => card.Cost).ThenBy(cx => cx.Name);
			}
			
			this.CardsFilteredSignal.Dispatch(filtered.Select(c => c.CardID).ToList());
		}
	}
}
