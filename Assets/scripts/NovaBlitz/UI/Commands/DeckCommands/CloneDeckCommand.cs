﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using LinqTools;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class CloneDeckSignal : Signal<DeckData>{}
	public class CloneDeckCommand : Command
	{
		// Signal Properties
		[Inject] public DeckData DeckData {get;set;}

		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public DeckClonedSignal DeckClonedSignal {get;set;}
		[Inject] public UpdateDeckListSignal UpdateDeckListSignal {get;set;}
		[Inject] public SelectDeckSignal SelectDeckSignal {get;set;}

		public override void Execute ()
		{
			Debug.Log ("Cloning deck!");

			// Create a cloned deck and copy the carditems of the source Deck
			DeckData clonedDeck = new DeckData(this.DeckData.Format);
			foreach(var card in this.DeckData.Cards)
			{
				clonedDeck.Cards.Add(new CardItem(card.CardID, card.DeckIndex, card.Aspect, card.CardType, card.Cost));
			}

			// Fill out the cloned deck's properties
			clonedDeck.ID = System.Guid.NewGuid().ToString();
			clonedDeck.Name = "Cloned " + this.DeckData.Name;
			clonedDeck.QueryAspects();
            clonedDeck.SetDeckArt();
            DeckData.QueryAspects();
            DeckData.SetDeckArt();

			// Find the index of the source Deck and add the cloned deck immediately after it
			int sourceDeckIndex = this.PlayerProfile.DeckList.IndexOf(this.DeckData);
			this.PlayerProfile.DeckList.Insert(sourceDeckIndex +1, clonedDeck);

			// Dispatch a signal letting the app know a deck has been cloned into the decklist
			this.DeckClonedSignal.Dispatch(this.DeckData, clonedDeck.ID);
            this.SelectDeckSignal.Dispatch(clonedDeck);

            // Persist the change to playfab
            DeckListDataContract dataContract = this.PlayerProfile.GetDeckListDataContract();
            if (dataContract != null)
            {
                this.UpdateDeckListSignal.Dispatch(dataContract, true);
            }
		}
	}
}
