﻿using UnityEngine;
using System;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using LinqTools;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
    public class CraftResponseSignal : Signal<CraftResponse> { }
    public class CraftResponseCommand :Command
    {
        // Signal properties
        [Inject] public CraftResponse Response { get; set; }

        // other injections
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public DecksModifiedSignal DecksModifiedSignal { get; set; }
		[Inject] public LogEventSignal LogEventSignal {get;set;}
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

        public override void Execute()
        {
            // craft, undocraft, scrap, undoscrap : update the ui

            // confirm: update inventory, wallet, UI
            int cardId = Response.CardId;
            if(Response.Error != ErrorCode.NoError)
            {
                Debug.LogWarningFormat("craft failed: {0}, {1} reason:{2}", Response.CardId, Response.NumberToCraft, Response.Error);
            }
            else if(Response.Error == ErrorCode.NoError && Response.RequestType == CraftRequestType.Confirm && cardId != 0)
            {

                Debug.LogWarningFormat("craft complete: {0}, {1}", Response.CardId, Response.NumberToCraft);
                int currentValue = 0;
                PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.NC, out currentValue);
				var newBalance = Math.Max(0, currentValue + Response.NanitesToAdd);
				PlayerProfile.Wallet.Currencies [CurrencyType.NC] = newBalance;

				if (Response.NumberToCraft > 0)
				{
					this.LogEventSignal.Dispatch (new LogEventParams {
						EventName = LogEventParams.EVENT_VIRTUAL_PURCHASE, 
						CurrencyType = CurrencyType.NC, 
						CurrencyBalance = newBalance,
						EventValue = -Response.NanitesToAdd,
						ItemId = Response.CardId.ToString(),
						ItemClass = NBEconomy.ITEMCLASS_CARD});
                    this.MetricsTrackingService.TrackCrafting(Response.CardId, true, Response.NumberToCraft, -Response.NanitesToAdd);

                }
				else
				{
					this.LogEventSignal.Dispatch (new LogEventParams {
						EventName = LogEventParams.EVENT_CURRENCY_CHANGE, 
						CurrencyType = CurrencyType.NC, 
						CurrencyBalance = newBalance,
						EventValue = Response.NanitesToAdd});
                    this.MetricsTrackingService.TrackCrafting(Response.CardId, false, -Response.NumberToCraft, -Response.NanitesToAdd);
                }

                int previousOwned = 0;
                PlayerProfile.OwnedCards.TryGetValue(cardId, out previousOwned);
                int newOwned = Math.Max(0, previousOwned + Response.NumberToCraft);
                if (newOwned > 0)
                {
                    PlayerProfile.OwnedCards[cardId] = newOwned;
                }
                else
                { 
                    PlayerProfile.OwnedCards.Remove(cardId);
                }

                if(previousOwned <= 2 && newOwned > previousOwned)
                {
                    PlayerProfile.NewCards.Add(cardId);
                }

                if(Response.NumberToCraft < 0 && newOwned < 3)
                {
                    CheckAndRemoveCardFromDecks();
                    Debug.LogWarningFormat("checking decks: {0}, {1}", Response.CardId, newOwned);
                }

                ProfileUpdatedSignal.Dispatch(ProfileSection.Inventory);
                ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);
            }
        }

        public void CheckAndRemoveCardFromDecks()
        {
            if (!this.PlayerProfile.HasUnlockedAllDecks || PlayerProfile.OwnedCards.Count == 0 || GameData.LegalCards.Count == 0)
                return;
            bool decksUpdated = false;
            foreach(var deck in PlayerProfile.DeckList)
            {
                if (deck.TrimToInventory(PlayerProfile.OwnedCards, GameData.LegalCards))
                    decksUpdated = true;
            }
            if(decksUpdated)
            {
                DecksModifiedSignal.Dispatch();

                this.ProfileUpdatedSignal.Dispatch(ProfileSection.DeckList);
                //DeckListDataContract dataContract = this.PlayerProfile.GetDeckListDataContract();
                //this.UpdateDeckListSignal.Dispatch(dataContract);
            }
        }
    }
    public class DecksModifiedSignal : Signal { }
}
