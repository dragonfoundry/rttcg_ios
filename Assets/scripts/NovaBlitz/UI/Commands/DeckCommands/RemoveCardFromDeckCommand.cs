﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class RemoveCardFromDeckSignal : Signal<CardData, DeckData>{} // CardData, reference to Deck 
	public class RemoveCardFromDeckCommand : Command
	{
		// Signal Params
		[Inject] public CardData CardData {get;set;}
		[Inject] public DeckData DeckData {get;set;}
		
		// Inject other Resources
		[Inject] public CardRemovedFromDeckSignal CardRemovedFromDeckSignal {get;set;}
		
		public override void Execute ()
		{
			for(int i=this.DeckData.Cards.Count-1; i>=0; i--)
			{
				if(this.DeckData.Cards[i].CardID == this.CardData.CardID)
				{
					this.DeckData.Cards.RemoveAt(i);
					this.DeckData.QueryAspects();
                    this.DeckData.SetDeckArt();
                    this.CardRemovedFromDeckSignal.Dispatch(this.CardData, this.DeckData);
                    Debug.LogFormat("Removed card {0} from deck", this.CardData.CardID);
                    return;
				}
			}
		}
	}
}
