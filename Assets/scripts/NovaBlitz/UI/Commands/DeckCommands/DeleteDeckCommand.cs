﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class DeleteDeckSignal : Signal<DeckData> {}
	public class DeleteDeckCommand : Command
	{
		// Signal Parameters
		[Inject] public DeckData DeckData {get;set;}

		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public DeckDeletedSignal DeckDeletedSignal {get;set;}
		[Inject] public DeselectDeckSignal DeselectDeckSignal {get;set;}
		[Inject] public UpdateDeckListSignal UpdateDeckListSignal {get;set;}

		public override void Execute ()
        {
            this.DeselectDeckSignal.Dispatch(this.DeckData);

            int listIndex = this.PlayerProfile.DeckList.IndexOf(this.DeckData);
			this.PlayerProfile.DeckList.Remove(this.DeckData);
			this.DeckDeletedSignal.Dispatch(listIndex);

			DeckListDataContract dataContract = this.PlayerProfile.GetDeckListDataContract();
            if (dataContract != null)
            {
                this.UpdateDeckListSignal.Dispatch(dataContract, true);
            }
        }
    }
}
