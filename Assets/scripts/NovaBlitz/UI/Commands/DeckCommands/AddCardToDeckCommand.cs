﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class AddCardToDeckSignal : Signal<CardData, DeckData, bool>{}  // reference to CardData, reference to Deck
	public class AddCardToDeckCommand : Command
	{
		// Signal Params
		[Inject] public CardData CardData {get;set;}
		[Inject] public DeckData DeckData {get;set;}
        [Inject] public bool IsFromTwoTouch { get; set; }

        // Inject other Resources
        [Inject] public CardAddedToDeckSignal CardAddedToDeckSignal {get;set;}
		[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ViewReferences ViewReferences {get;set;}
		
		/// <summary>
		/// Execute the command
		/// </summary>
		public override void Execute ()
		{
			CardListItem cardListItem = null;
			CardListTransform.IndexOfCard(this.DeckData, this.CardData, ref cardListItem);
			
			this.DeckData.QueryAspects();
			
			if(this.DeckData.Aspects.Contains(this.CardData.Aspect) == false && DeckData.Aspects.Count == 2)
			{                
				if (!IsFromTwoTouch)
				{
                	NewMessageDialogParams dlgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.AspectLimit,
                    	string.Format(I2.Loc.ScriptLocalization.Error.AspectLimitMessage, this.CardData.Aspect, DeckData.Aspects[0], DeckData.Aspects[1]));
                	// "Decks in Nova Blitz are capped at two aspects.\n\n To add this " + this.CardData.Aspect + " card you'll have to remove all cards from your deck belonging to either the " + DeckData.Aspects[0] + " or " + DeckData.Aspects[1] + " aspect.");
                	// Show a dialog informing the user that they can't add more than 2 aspects to a deck

                    this.ShowNewMessageDialogSignal.Dispatch(dlgParams);
                }
                return;
			}
            TwoTouchOverlayView TwoTouch = (TwoTouchOverlayView) this.ViewReferences.Get(typeof(TwoTouchOverlayView));

            int owned;
            PlayerProfile.OwnedCards.TryGetValue(this.CardData.CardID, out owned);
            owned += (TwoTouch as TwoTouchOverlayView).NumberToCraft;

            if (this.DeckData.DeckList.Count >= this.DeckData.MaxDeckSize)
            {
				if (!IsFromTwoTouch)
				{
                	NewMessageDialogParams dlgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.MaxDeckSize,
                    	string.Format(I2.Loc.ScriptLocalization.Error.MaxDeckSizeMessage, this.DeckData.MaxDeckSize, 0, 0));// "MAX DECK SIZE REACHED", "A maximum of " + this.DeckData.LegalDeckSize + " cards can be added to a deck.\n\n Remove cards before adding new ones.");
                // Show a dialog informing the user that they can't add any more of that type of card

                    this.ShowNewMessageDialogSignal.Dispatch(dlgParams);
                }
            }
            else if (cardListItem != null && cardListItem.Count >= this.DeckData.MaxOfEachCard)
			{
				if (!IsFromTwoTouch)
				{
                	NewMessageDialogParams dlgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.CardLimit,
                    	string.Format(I2.Loc.ScriptLocalization.Error.CardLimitMessage, this.DeckData.MaxOfEachCard, 0, 0));// "CARD LIMIT REACHED", "A maximum of " + this.DeckData.MaxOfEachCard + " of each card can be added to a deck.\n\n Try adding some other cards.");
                	// Show a dialog informing the user that they can't add any more of that type of card
                    this.ShowNewMessageDialogSignal.Dispatch(dlgParams);
                }
            }
            else if (owned <= 0 || (cardListItem != null && cardListItem.Count >= owned))
			{      
				if (!IsFromTwoTouch)
				{
                	NewMessageDialogParams dlgParams = new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.AlreadyInDeck,
                    	string.Format(I2.Loc.ScriptLocalization.Error.AlreadyInDeckMessage, owned, 0, 0));//"ALREADY IN DECK", "You own " + owned + " of this card.\n\n Craft another if you want to add one to your deck.");
                	// Show a dialog informing the user that they can't add any more of that type of card

                    this.ShowNewMessageDialogSignal.Dispatch(dlgParams);
                }
            }
			else
			{
				// Add the new card to the Deck
				CardItem newCard = new CardItem(this.CardData.CardID, -1, this.CardData.Aspect, this.CardData.CardType, this.CardData.Cost);
				this.DeckData.Cards.Add(newCard);
				newCard.DeckIndex = this.DeckData.Cards.IndexOf(newCard);
                Debug.LogFormat("Added card {0} to deck at {1}. Deck:{2}", this.CardData.CardID, newCard.DeckIndex, DeckData.ToString());

                // Inform any listeners that a card has been added to the deck at the given index in the ViewList
                this.CardAddedToDeckSignal.Dispatch(this.CardData, this.DeckData);
			}
		}
	}
}
