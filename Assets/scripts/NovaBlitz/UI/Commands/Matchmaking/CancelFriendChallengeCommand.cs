﻿using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using UnityEngine;

namespace NovaBlitz.UI
{
	public class FriendChallengeCancledMessage
	{
		public FriendChallengeCancledMessage(string friendPlayfabId)
		{
			this.FriendPlayfabId = friendPlayfabId;
		}
		
		public string FriendPlayfabId {get; private set;}
		
		public string[] Serialize()
		{
			string[] data = new string[2];
			data[0] = PhotonMessageType.CancelChallenge.ToString();
			data[1] = this.FriendPlayfabId;
			return data;
		}
		
		static public FriendChallengeCancledMessage Deserialize(string[] data)
		{
			if(data.Length == 2 && data[0] == PhotonMessageType.CancelChallenge.ToString())
			{
				FriendChallengeCancledMessage retVal = new FriendChallengeCancledMessage(data[1]);
				return retVal;
			}
			else
			{
				Debug.LogError("Could not deserialize FriendChallengeCanceled");
			}
			return null;
		}
	}
	
	public class CancelFriendChallengeSignal : Signal<string>{} // Challenger PlayfabId
	
    public class CancelFriendChallengeCommand : Command
    {
		// Signal Parameters
		[Inject] public string FriendPlayfabId {get;set;}
		
		 // Injected Dependencies
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public PlayFabChatClient PlayFabChatClient {get;set;}
		
		public override void Execute()
		{
			// Send a cancel message to the friend waiting on the other side
			FriendChallengeCancledMessage cancel = new FriendChallengeCancledMessage(this.FriendPlayfabId);
            this.PlayFabChatClient.chatClient.SendPrivateMessage(cancel.FriendPlayfabId, cancel.Serialize(), true);
			
			Debug.LogWarning("Sent Cancel Friend Challenge");
		}
    }
}
