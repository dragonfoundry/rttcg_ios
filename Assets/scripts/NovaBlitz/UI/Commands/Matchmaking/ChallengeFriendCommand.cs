using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
	public class ChallengeFriendSignal : Signal<string> {}
    public class ChallengeFriendCommand : Command
    {
		// Signal Parameters
		[Inject] public string FriendPlayFabID {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public PlayFabChatClient PlayFabChatClient {get;set;}
		[Inject] public StartGameSessionSignal StartGameSessionSignal {get;set;}
		
		public override void Execute()
		{
			// Send the FriendChallenge over Photon Private messaging
			FriendChallenge friendChallenge = new FriendChallenge(this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.MetaProfile.Name);
			this.PlayFabChatClient.chatClient.SendPrivateMessage(this.FriendPlayFabID, friendChallenge.Serialize(), true);

			var startGameSessionData = new StartGameSessionObject{
				GameFormat = Messages.GameFormat.Challenge,
				PlayFabIdToChallenge = this.FriendPlayFabID,
				IsAcceptingChallenge = false
			};
			this.StartGameSessionSignal.Dispatch(startGameSessionData);
		}
    }
}