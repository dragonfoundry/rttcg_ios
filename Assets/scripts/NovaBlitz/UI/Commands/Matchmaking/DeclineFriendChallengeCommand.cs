﻿using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using UnityEngine;


namespace NovaBlitz.UI
{
    public class FriendChallengeDeclinedMessage
	{
		public FriendChallengeDeclinedMessage()
		{
		}
		
		public string[] Serialize()
		{
			string[] data = new string[2];
			data[0] = PhotonMessageType.DeclineChallenge.ToString();
			return data;
		}
		
		static public FriendChallengeDeclinedMessage Deserialize(string[] data)
		{
			if(data.Length == 1 && data[0] == PhotonMessageType.DeclineChallenge.ToString())
			{
				FriendChallengeDeclinedMessage retVal = new FriendChallengeDeclinedMessage();
				return retVal;
			}
			else
			{
				Debug.LogError("Could not deserilize FriendChallengeDeclined");
			}
			
			return null;
		}
	}

	public class FriendChallengeDeclinedSignal : Signal<string>{}

	public class DeclineFriendChallengeSignal : Signal<string>{}
    public class DeclineFriendChallengeCommand : Command
    {
        // Signal Parameters
		[Inject] public string FriendPlayFabID { get; set; }

        // Injected Dependencies
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public PlayFabChatClient PlayFabChatClient {get;set;}

        public override void Execute()
		{
			// Remove the challenge from the profile 
			this.PlayerProfile.FriendChallenges.Remove(this.FriendPlayFabID);
            
            // Send a decline message to the friend waiting on the other side
            FriendChallengeDeclinedMessage decline = new FriendChallengeDeclinedMessage();
            this.PlayFabChatClient.chatClient.SendPrivateMessage(this.FriendPlayFabID, decline.Serialize(), true);
			
			Debug.LogWarning("Sent Decline Friend CHallenge");
		}
    }
}