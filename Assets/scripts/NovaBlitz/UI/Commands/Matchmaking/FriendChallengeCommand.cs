﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using NovaBlitz.Game;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
	public class FriendChallenge
	{
		public FriendChallenge(string playfabUserId, string titleDisplayName)
		{
			this.FriendPlayFabId = playfabUserId;
			this.FriendTitleDisplayName = titleDisplayName;
			this.TimeChallenged = Time.realtimeSinceStartup;
		}
		public string FriendPlayFabId {get; private set;}
		public string FriendTitleDisplayName {get; private set;}
		public float TimeChallenged {get; private set;}
		
		public string[] Serialize()
		{
			string[] data = new string[3];
			data[0] = PhotonMessageType.Challenge.ToString();
			data[1] = this.FriendPlayFabId;
			data[2] = this.FriendTitleDisplayName;
			return data;
		}
		
		static public FriendChallenge Deserialize(string[] data)
		{
			if(data.Length == 3 && data[0] == PhotonMessageType.Challenge.ToString())
			{
				FriendChallenge retVal = new FriendChallenge(data[1], data[2]);
				return retVal;
			}
			else
			{
				Debug.LogError("Could not deserilize FriendChallenge");
			}
			
			return null;
		}
	}
	public class FriendChallengeSignal : Signal<FriendChallenge> {}
	public class FriendChallengeCommand : Command 
	{
		// Signal Params
		[Inject] public FriendChallenge FriendChallenge {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		
		public override void Execute()
		{
			this.PlayerProfile.FriendChallenges[this.FriendChallenge.FriendPlayFabId] = this.FriendChallenge;
		}
	}
}