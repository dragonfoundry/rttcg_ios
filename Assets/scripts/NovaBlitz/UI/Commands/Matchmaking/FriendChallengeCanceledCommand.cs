﻿using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using UnityEngine;

namespace NovaBlitz.UI
{
    public class FriendChallengeCanceledSignal : Signal<string> { } // Challenger PlayfabId
    public class FriendChallengeCanceledCommand : Command
    {
		// Signal Parameters
		[Inject] public string FriendPlayfabId {get;set;}
		
		 // Injected Dependencies
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		
		public override void Execute()
		{
			// Remove the challenge from the profile
			this.PlayerProfile.FriendChallenges.Remove(this.FriendPlayfabId);
		}
    }
}
