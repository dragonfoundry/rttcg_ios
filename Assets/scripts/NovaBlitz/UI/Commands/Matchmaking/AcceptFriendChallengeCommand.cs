﻿using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System.Collections.Generic;
using LinqTools;

namespace NovaBlitz.UI
{
    public class AcceptFriendChallengeSignal : Signal<string>{}
    public class AcceptFriendChallengeCommand : Command
    {
		// Signal Parameters
		[Inject] public string FriendPlayFabID {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public StartGameSessionSignal StartGameSessionSignal {get;set;}
		[Inject] public DeclineFriendChallengeSignal DeclineFriendChallengeSignal {get;set;}
		
		public override void Execute()
		{
			// Remove the challenge from the profile 
			this.PlayerProfile.FriendChallenges.Remove(this.FriendPlayFabID);
			this.PlayerProfile.SelectedChallengeID = null;

			// Decline all others
			foreach (var id in this.PlayerProfile.FriendChallenges.Keys.ToArray())
				this.DeclineFriendChallengeSignal.Dispatch(id);
			
			var startGameSessionData = new StartGameSessionObject{
				GameFormat = Messages.GameFormat.Challenge,
				PlayFabIdToChallenge = this.FriendPlayFabID,
				IsAcceptingChallenge = true,
			};
			this.StartGameSessionSignal.Dispatch(startGameSessionData);
		}
    }
}
