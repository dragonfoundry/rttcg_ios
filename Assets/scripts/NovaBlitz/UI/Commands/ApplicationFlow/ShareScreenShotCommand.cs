﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using System.Text;

namespace NovaBlitz.UI
{
    public class ShareScreenShotParams
    {
        public NativeShare sharingPlugin;
        public string text;
        public Button shareButton;
        public GameObject shareButtonPanel;

        public ShareScreenShotParams(NativeShare SharingPlugin, string Text, Button ShareButton, GameObject ShareButtonPanel)
        {
            sharingPlugin = SharingPlugin;
            text = Text;
            shareButton = ShareButton;
            shareButtonPanel = ShareButtonPanel;
        }
    }

    public class ShareScreenShotSignal : Signal<ShareScreenShotParams>  { }
    public class ShareScreenShotCommand : Command
    {
        [Inject] public ShareScreenShotParams Params { get; set; }
        
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

        public override void Execute()
        {
            this.Retain();
            Timer.Instance.StartCoroutine(ShareScreenshotWithTextCoroutine());
        }

        public const string SCREENSHOT_NAME = "screenshot.png";

        public IEnumerator ShareScreenshotWithTextCoroutine()
        {
            Params.shareButtonPanel.SetActive(false);
            Params.shareButton.interactable = false;// .gameObject.SetActive(false);

			string screenShotPath = System.IO.Path.Combine(Application.persistentDataPath,SCREENSHOT_NAME);

            if (System.IO.File.Exists(screenShotPath))
            {
				//Debug.Log ("Sharing: deleting existing file");
                System.IO.File.Delete(screenShotPath);
            }

#if UNITY_IOS || UNITY_ANDROID
			ScreenCapture.CaptureScreenshot (SCREENSHOT_NAME);
#else
            ScreenCapture.CaptureScreenshot(screenShotPath);
			#endif

			//Debug.Log ("Sharing: capturing screenshot to " + screenShotPath);
            // wait until screen is captured
            float startTime = Time.time;
            while (false == System.IO.File.Exists(screenShotPath))
            {
                // tried too long to save the file ?
                if (Time.time - startTime > 10.0f)
				{
					Params.shareButtonPanel.SetActive(true);
					Params.shareButton.interactable = true;// .gameObject.SetActive(true);
                    yield break;
                }
                yield return null;
            }

            yield return null;
            yield return null;

#if UNITY_IOS || UNITY_ANDROID
			//Debug.Log("Starting Sharing");
            Params.sharingPlugin.Share(Params.text, screenShotPath, string.Empty);
            this.MetricsTrackingService.TrackSocialShare();
#else
            Application.OpenURL(screenShotPath);
#endif
            Params.shareButtonPanel.SetActive(true);
            Params.shareButton.interactable = true;// .gameObject.SetActive(true);
            this.Release();
        }
    }
}
