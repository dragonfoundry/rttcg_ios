﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using DragonFoundry.Networking;


namespace NovaBlitz.UI
{
    public class TeardownUISignal : Signal { }
    public class TeardownUICommand : Command
    {
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public ViewStateStack ViewStateStack { get; set; }
		[Inject] public GameData GameData {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
        [Inject] public UnloadUnusedResourcesSignal UnloadUnusedResourcesSignal { get; set; }

        public override void Execute()
        {
            Timer.Instance.StartCoroutine(TeardownUICoroutine());
        }

        private IEnumerator TeardownUICoroutine()
        {
            yield return null;
			int objectPoolSize = 	this.UIConfiguration.UIObjectPoolSize;
            int cardPoolSize = this.UIConfiguration.CardPoolSize;
            int fxPoolSize = this.UIConfiguration.FXObjectPoolSize;
            var cardBrowser = 	FindView<CardBrowserView>();
			var cardList = 		FindView<CardListView>();
			var deckListDock = 	FindView<DeckListDockView>();
			var draftBrowser = 	FindView<DraftPackBrowserView>();
			var draftList = 	FindView<DraftCardListView>();

			IPoolView[] poolViews = new IPoolView[]{
				cardBrowser,
				cardList,
				deckListDock,
				draftBrowser,
				draftList
			};

			foreach (var pv in poolViews) {
				if (null != pv) pv.Recycle();
			}

            // Don't create more of these, but destroy them if there's too many
            if (ObjectPool.PoolExists(PooledObjectType.DraggedCardView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.DraggedCardPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.DraggableCardView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.DraggableCardPrefab, objectPoolSize * 3);
            if (ObjectPool.PoolExists(PooledObjectType.CardView))
				ObjectPool.LimitPoolSize(this.UIConfiguration.CardPrefab, objectPoolSize*2);
            if (ObjectPool.PoolExists(PooledObjectType.AchievementView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.AchievementPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.QuestView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.QuestPrefab, 3);
            if (ObjectPool.PoolExists(PooledObjectType.LeaderboardItemView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.LeaderboardPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.AvatarView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.AvatarItemPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.CardBackView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.CardBackViewPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.StoreItem))
                ObjectPool.LimitPoolSize(this.UIConfiguration.StoreItemPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.CardListItemView))
                ObjectPool.LimitPoolSize(this.UIConfiguration.CardListItemPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.CardListItemProxy))
                ObjectPool.LimitPoolSize(this.UIConfiguration.CardListItemProxyPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.UpdatedPrizeView))
                ObjectPool.LimitPoolSize(this.GameData.PrizePrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.CardBackController))
                ObjectPool.LimitPoolSize(this.GameData.CardBackPrefab, cardPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.CardFrontController))
                ObjectPool.LimitPoolSize(this.GameData.ArenaCardPrefab, cardPoolSize);

            if (ObjectPool.PoolExists(PooledObjectType.DeckListItem))
                ObjectPool.LimitPoolSize(this.UIConfiguration.DeckListItemPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.FriendListItem))
                ObjectPool.LimitPoolSize(this.UIConfiguration.FriendListItemPrefab, objectPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.GlobalChatItem))
                ObjectPool.LimitPoolSize(this.UIConfiguration.GlobalChatPrefab, objectPoolSize);

            // Resize the PooledFx pools, including setting them to
            if (ObjectPool.PoolExists(PooledObjectType.FxTrigger))
                ObjectPool.SetPoolSizeAsync(UIConfiguration.PooledFxPrefab, fxPoolSize, fxPoolSize);
            if (ObjectPool.PoolExists(PooledObjectType.FxTrailTrigger))
                ObjectPool.SetPoolSizeAsync(UIConfiguration.PooledTrailFxPrefab, fxPoolSize, fxPoolSize);
            Debug.LogFormat("Object Pools limited at {0}. Last Frame {1}", Time.realtimeSinceStartup, Time.deltaTime);
            this.ViewStateStack.Clear();
            this.UnloadUnusedResourcesSignal.Dispatch();
		}

		protected T FindView<T>() where T : AnimatedView { return this.ViewReferences.Get<T>(); }
    }
}
