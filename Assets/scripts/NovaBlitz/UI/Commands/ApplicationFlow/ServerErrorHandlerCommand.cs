﻿using UnityEngine;
using LinqTools;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;
using Messages;

namespace NovaBlitz.UI
{

    public class ServerErrorRaisedSignal : Signal<ErrorCode, MessageBase> { }

    public class ServerErrorHandlerCommand : Command
    {
        [Inject] public ErrorCode errorCode { get; set; }
        [Inject] public MessageBase Message { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }


        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}

        public override void Execute()
        {
            string title = I2.Loc.ScriptLocalization.Error.Unknown_Error;
            string message = I2.Loc.ScriptLocalization.Error.Unknown_Error_Message;
            switch (errorCode)
            {
                // Card play; ignore these and return.
                case ErrorCode.AbilityNotFound:
                case ErrorCode.AbilityNotLegal:
                case ErrorCode.CardIsNull:
                    return;
                case ErrorCode.ConnectionFailed:
                    title = I2.Loc.ScriptLocalization.Error.ConnectionFailed;
                    message = I2.Loc.ScriptLocalization.Error.ConnectionFailedMessage;
                    break;
                case ErrorCode.RequestFailed:
                    title = I2.Loc.ScriptLocalization.Error.Failed;
                    message = I2.Loc.ScriptLocalization.Error.GameRequestFailed;
                    break;
                case ErrorCode.AlreadyPlayingInThisEvent: // player is already in the event; reconnect to refresh data
                    title = I2.Loc.ScriptLocalization.Error.AlreadyPlayingInThisEvent;
                    message = I2.Loc.ScriptLocalization.Error.AlreadyPlayingInThisEventMessage;
                    break;
                case ErrorCode.UnknownPaymentMethodProvided:
                    title = I2.Loc.ScriptLocalization.Error.UnknownPaymentMethodProvided;
                    message = I2.Loc.ScriptLocalization.Error.UnknownPaymentMethodProvidedMessage;
                    break;
                case ErrorCode.DraftNotFound: // We couldn't find the draft for a draft pick.
                    title = I2.Loc.ScriptLocalization.Error.DraftNotFound;
                    message = I2.Loc.ScriptLocalization.Error.DraftNotFoundMessage;
                    break;
                case ErrorCode.GameNotFound: // Game action, concede, or other similar message sent, but the server couldn't find the game // Also for tutorials past 6.
                    title = I2.Loc.ScriptLocalization.Error.GameNotFound;
                    message = I2.Loc.ScriptLocalization.Error.GameNotFoundMessage;
                    break;
                case ErrorCode.NoEventToResume: // requested a resume draft; no draft to resume
                    title = I2.Loc.ScriptLocalization.Error.NoEventToResume;
                    message = I2.Loc.ScriptLocalization.Error.NoEventToResumeMessage;
                    break;
                case ErrorCode.EventStartFailure: // requested a resume draft; no draft to resume
                    title = I2.Loc.ScriptLocalization.Error.EventStartFailure;
                    message = I2.Loc.ScriptLocalization.Error.EventStartFailureMessage;
                    break;
                case ErrorCode.GameFormatNotSupported: // Requested a GameFormat that isn't allowed by the server
                    title = I2.Loc.ScriptLocalization.Error.GameFormatNotSupported;
                    message = I2.Loc.ScriptLocalization.Error.GameFormatNotSupportedMessage;
                    break;
                case ErrorCode.NotPlayingThatEventType: // Requested a GameFormat that the player isn't currently playing
                    title = I2.Loc.ScriptLocalization.Error.NotPlayingThatEventType;
                    message = I2.Loc.ScriptLocalization.Error.NotPlayingThatEventTypeMessage;
                    break;
                case ErrorCode.IllegalDeck: // Submitted deck is illegal
                    title = I2.Loc.ScriptLocalization.Error.IllegalDeck;
                    message = I2.Loc.ScriptLocalization.Error.IllegalDeckMessage;
                    break;
                case ErrorCode.CurrencyNotFound:
                case ErrorCode.InsufficientFunds: // Tried to pay, but 
                    title = I2.Loc.ScriptLocalization.Error.InsufficientFunds;
                    message = I2.Loc.ScriptLocalization.Error.InsufficientFundsMessage;
                    break;
                case ErrorCode.MessageVersionError: // Message library out of sync. This is fatal - need to update the client
                    title = I2.Loc.ScriptLocalization.Error.ClientOutOfDate;
                    message = I2.Loc.ScriptLocalization.Error.ClientOutOfDateMessage;
                    break;
                case ErrorCode.PlayerNotPartOfGame: // For some reason, this player isn't in this game
                    title = I2.Loc.ScriptLocalization.Error.PlayerNotPartOfGame;
                    message = I2.Loc.ScriptLocalization.Error.PlayerNotPartOfGameMessage;
                    break;
                case ErrorCode.SessionTokenInvalid: // Player's Session Token failed to authenticate
                    title = I2.Loc.ScriptLocalization.Error.SessionTokenInvalid;
                    message = I2.Loc.ScriptLocalization.Error.SessionTokenInvalidMessage;
                    break;
                case ErrorCode.CouponAlreadyUsed:
                    title = I2.Loc.ScriptLocalization.Error.CouponAlreadyRedeemed;
                    message = I2.Loc.ScriptLocalization.Error.CouponAlreadyRedeemedMessage;
                        break;
                case ErrorCode.CouponNotValid:
                    title = I2.Loc.ScriptLocalization.Error.InvalidCoupon;
                    message = I2.Loc.ScriptLocalization.Error.InvalidCouponMessage;
                    break;
                case ErrorCode.DuplicateCouponOnThisAccount:
                    title = I2.Loc.ScriptLocalization.Error.CouponAlreadyOnAccount;
                    message = I2.Loc.ScriptLocalization.Error.CouponAlreadyOnAccountMessage;
                    break;
                case ErrorCode.ServerDown:
                    title = I2.Loc.ScriptLocalization.Error.ServerDown;
                    message = I2.Loc.ScriptLocalization.Error.ServerDownText;
                    break;
                default:
                    break;
            }
            ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams { Message = message, Title = title });
        }
    }
}
