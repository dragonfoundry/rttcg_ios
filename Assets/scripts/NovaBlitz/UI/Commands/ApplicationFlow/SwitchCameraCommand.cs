﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;

namespace NovaBlitz.UI
{
	public enum CameraType
	{
		UI,
		Gameplay
	}
	public class SwitchCameraSignal : Signal<NovaBlitz.UI.CameraType> {}
    public class SwitchCameraCommand : Command
    {
       // Signal Parameters
	   [Inject] public NovaBlitz.UI.CameraType CameraType {get;set;}
	   
	   // Injected Dependencies
	   [Inject] public INovaContext ContextView {get;set;}
	   
	   public override void Execute()
	   {
		   MainContextView mainContextView = (MainContextView)this.ContextView;
		   mainContextView.CameraForUI.gameObject.SetActive(this.CameraType == CameraType.UI);
	   }
    }
}
