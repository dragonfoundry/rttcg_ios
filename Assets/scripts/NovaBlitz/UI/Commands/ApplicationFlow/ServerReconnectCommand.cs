﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using System.Collections.Generic;
using Messages;
using NovaBlitz.Game;

// Signal indicating that the client is out of date with the server
namespace NovaBlitz.UI
{
    public class ServerReconnectSignal : Signal<DisconnectCode> { }
    public class ServerReconnectCommand : Command
    {
        [Inject] public DisconnectCode disconnectCode { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public InitializeProgressScreenSignal InitializeProgressScreenSignal { get; set;}
		[Inject] public TeardownArenaSignal TeardownArenaSignal { get; set; }
		//[Inject] public CancelSignal CancelSignal { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }

        public override void Execute()
        {
            // If they logged out, do nothing here. The login dialog is already up.
            if (disconnectCode == DisconnectCode.UserLoggedOut)
            {
                return;
            }
            this.CloseAnimatedViewSignal.Dispatch(typeof(ReconnectDialogView));
            //this.CancelSignal.Dispatch();
            this.ViewReferences.DestroyAllViews();
            this.TeardownArenaSignal.Dispatch();

            // Start up a progress screen with a timer bar
            Dictionary<AsyncProgress, string> progressLabels = new Dictionary<AsyncProgress, string>();
            progressLabels[AsyncProgress.NoProgress] = I2.Loc.ScriptLocalization.AsyncProgress.StartingGame;
            progressLabels[AsyncProgress.CardArtPositions] = I2.Loc.ScriptLocalization.AsyncProgress.CardArtPositions;
            progressLabels[AsyncProgress.LoadingAudio] = I2.Loc.ScriptLocalization.AsyncProgress.LoadingAudio;
            progressLabels[AsyncProgress.Authenticating] = I2.Loc.ScriptLocalization.AsyncProgress.Authenticating;
            progressLabels[AsyncProgress.TitleData] = I2.Loc.ScriptLocalization.AsyncProgress.TitleData;
            this.InitializeProgressScreenSignal.Dispatch(progressLabels);

            // If they're being asked to reconnect, do the first loop silently.
            if (disconnectCode == DisconnectCode.PleaseLogOutAndBackInAgain)
            {
                this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect);
                return;
            }
            else
            {
                this.Retain();
                Timer.Instance.StartCoroutine(WaitOneFrame());
            }
        }

        public IEnumerator WaitOneFrame()
        {
            yield return null;
            this.OpenAnimatedViewSignal.Dispatch(typeof(ReconnectDialogView));
            yield return null;
            var reconnectView = this.ViewReferences.Get<ReconnectDialogView>();
            if (reconnectView == null)
            {
                reconnectView = (ReconnectDialogView)this.ViewReferences.Instantiate(typeof(ReconnectDialogView));
                this.OpenAnimatedViewSignal.Dispatch(typeof(ReconnectDialogView));
            }
            reconnectView.Initialize(disconnectCode, 0);
            this.Release();
        }
    }

    public enum ServerErrorReason
    {
        NoReason = 0,
        ClientOutOfDate = 1,
        ServerDownForMaintenance = 2,
        PlayFabDataParseFail = 3,
        NeedToRestartClient = 4,
    }
}