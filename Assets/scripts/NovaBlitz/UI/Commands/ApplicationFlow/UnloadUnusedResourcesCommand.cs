﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using PlayFab;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class UnloadUnusedResourcesSignal : Signal { }
    public class UnloadUnusedResourcesCommand : Command
    {
        [Inject] public AssetManagerService AssetManagerService { get; set; }
        public override void Execute()
        {
            AssetManagerService.UnlinkUnusedArtAssets();
            Resources.UnloadUnusedAssets();
            Debug.LogFormat("Assets Unloaded at {0}. Last Frame {1}", Time.realtimeSinceStartup, Time.deltaTime);
        }
    }
}
