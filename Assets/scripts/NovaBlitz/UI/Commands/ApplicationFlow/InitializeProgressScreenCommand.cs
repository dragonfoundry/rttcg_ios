﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class InitializeProgressScreenSignal : Signal<Dictionary<AsyncProgress,string>>{}
    public class SetProgressScreenStatusLabelSignal : Signal<string>{}
    public class ProgressScreenClosedSignal : Signal{}
    public class ShowCancelProgressButtonSignal : Signal<System.Action>{} // onCancelClicked
    public class HideCancelProgressButtonSignal : Signal {};
    
    public class InitializeProgressScreenCommand : Command
    {
		// Signal Parameters
		[Inject] public Dictionary<AsyncProgress, string> ProgressLabels {get;set;}
		
		// Injected Dependencies
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        //[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal {get;set;}
		
			
		override public void Execute()
        { 
            Debug.LogFormat("Initializing Progress Loading: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
			this.InitializeAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView), this.InitializeView);
            
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
		}

        private void InitializeView(AnimatedView view)
        {
            ProgressLoadingScreenView progressScreen = (ProgressLoadingScreenView)view;
            
            progressScreen.CancelButton.gameObject.SetActive(false);

            progressScreen.InitializeProgressItems(this.ProgressLabels);
            Debug.LogFormat("Progress Loading Initialized: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
        }
    }
}