﻿using UnityEngine;
using System.Collections.Generic;
using LinqTools;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Newtonsoft.Json;
//using TuneSDK;
using KHD;


namespace NovaBlitz.UI
{
	public class LogEventSignal :Signal<LogEventParams> {}
	public class LogEventCommand : Command
	{
		[Inject] public PlayerProfile PlayerProfile { get; set; }

		[Inject] public LogEventParams Params { get; set; }

		public override void Execute ()
		{
			if (PlayerProfile.isAgeGatePassed != true || string.IsNullOrEmpty(Params.EventName) || !PlayerProfile.isAdTrackingEnabled)
				return;

//#if UNITY_IPHONE || UNITY_ANDROID

			// Add details to the general params dictionary


			// Log the event to Fiksu & Tune
			switch(Params.EventName)
			{
				case LogEventParams.EVENT_LEVEL_UP:
					/*switch(Params.EventValue)
					{
						case 5:
							Tune.MeasureEvent(new TuneEvent(Params.EventName){level = Params.EventValue});
							//Fiksu.UploadRegistration(Fiksu.FiksuRegistrationEvent.EVENT3);
							break;
						case 10:
							Tune.MeasureEvent(new TuneEvent(Params.EventName){level = Params.EventValue});
							break;
						case 15:
							Tune.MeasureEvent(new TuneEvent(Params.EventName){level = Params.EventValue});
							break;
					}*/

                    FlurryAnalytics.Instance.LogEvent(string.Format("level_up_{0}", Params.EventValue), false);
                    //FuseService.RegisterLevel(Params.EventValue);
					Params.EventDetails["Level"] = Params.EventValue.ToString();
					break;
				case LogEventParams.EVENT_GAME_COMPLETED:
                    // Log specific tutorial completion
                    if (Params.Format == GameFormat.Tutorial && PlayerProfile.OnboardingProgress.TutorialsCompleted == false)
                    {
                        FlurryAnalytics.Instance.LogEvent(string.Format((Params.hasWon ? "tutorial_{0}" : "tutorial_loss_{0}"),PlayerProfile.OnboardingProgress.CurrentTutorial), false);
                        /*if (PlayerProfile.OnboardingProgress.CurrentTutorial == 5 && Params.hasWon)
                        {
                            Tune.MeasureEvent(new TuneEvent("TutorialComplete") { level = 5 });
                            //Fiksu.UploadRegistration(Fiksu.FiksuRegistrationEvent.EVENT2);
                        }*/
                    }
					// set up for logging all game completions in Flurry
					Params.EventDetails["Format"] = Params.Format.ToString();
					Params.EventDetails["HasWon"] = Params.hasWon.ToString();
					Params.EventDetails["Rating"] = Params.EventValue.ToString();
					break;
				case LogEventParams.EVENT_TOURNAMENT_COMPLETED:
					Params.EventDetails["Format"] = Params.Format.ToString();
					Params.EventDetails["Wins"] = Params.EventValue.ToString();
					Params.EventDetails["Losses"] = Params.EventValue2.ToString();
					break;
				case LogEventParams.EVENT_PURCHASE:
					//Fiksu.UploadPurchase(Fiksu.FiksuPurchaseEvent.EVENT1, Params.EventValue, Params.ISOCurrencyCode);

					/*TuneEvent tuneEvent = new TuneEvent("purchase");
					tuneEvent.eventItems = new TuneItem[]{
						new TuneItem(Params.ItemId){
							unitPrice = Params.EventValue, 
							quantity = 1}};
					tuneEvent.revenue = Params.EventValue;
					tuneEvent.currencyCode = Params.ISOCurrencyCode;
					//tuneEvent.advertiserRefId = "12999748531";

					Tune.MeasureEvent(tuneEvent);*/

					Params.EventDetails["CurrencyCode"] = Params.ISOCurrencyCode;
					Params.EventDetails["PurchasePrice"] = Params.EventValue.ToString();
					Params.EventDetails["ItemId"] = Params.ItemId;
					break;
				case LogEventParams.EVENT_VIRTUAL_PURCHASE:
					//FuseService.RegisterCurrencyBalances(Params.CurrencyType);
                    //FuseService.RegisterVirtualPurchase(Params.ItemId, Params.CurrencyType, Params.EventValue);
                    Params.EventDetails["Balance"] = Params.CurrencyBalance.ToString();
					Params.EventDetails["Currency"] = Params.CurrencyType.ToString();
					Params.EventDetails["CurrencyChange"] = (-Params.EventValue).ToString(); // -ve price because this is a purchase, so we need to track -ve currency changes
                    Params.EventDetails["ItemClass"] = Params.ItemClass;
					Params.EventDetails["ItemId"] = Params.ItemId;
					/*if(Params.ItemClass == "event_entry")
						Fiksu.UploadPurchase(Fiksu.FiksuPurchaseEvent.EVENT3, 0 , "USD");
					else if (Params.ItemClass == NBEconomy.ITEMCLASS_BOOSTER)
						Fiksu.UploadPurchase(Fiksu.FiksuPurchaseEvent.EVENT4, 0 , "USD");*/
					break;
				case LogEventParams.EVENT_CURRENCY_CHANGE:
					//FuseService.RegisterCurrencyBalances(Params.CurrencyType);
					Params.EventDetails["Balance"] = Params.CurrencyBalance.ToString();
					Params.EventDetails["Currency"] = Params.CurrencyType.ToString();
					Params.EventDetails["CurrencyChange"] = Params.EventValue.ToString();
					break;
				case LogEventParams.EVENT_SOCIAL_MEDIA_POST:
					string platform = Params.isTwitter ? "twitter" : "facebook";
					//Tune.MeasureEvent(new TuneEvent(Params.EventName){attribute1 = platform});
					Params.EventDetails["Platform"] = platform;
					break;
				case LogEventParams.EVENT_TIME_WAIT_FOR_GAME:
					if(Params.IsTimedEventOver)
					{
						Params.EventDetails["Format"] = Params.Format.ToString();
						int mmr;
						if(this.PlayerProfile.UserStatistics.TryGetValue("mmr", out mmr))
							Params.EventDetails["mmr"] = mmr.ToString();
					}
					break;
				case LogEventParams.EVENT_TIME_IN_GAME:
					if(Params.IsTimedEventOver)
					{
						Params.EventDetails["Format"] = Params.Format.ToString();
						Params.EventDetails["Rating"] = Params.EventValue.ToString();
						Params.EventDetails["HasWon"] = Params.hasWon.ToString();
					}
					break;
				case LogEventParams.EVENT_TIME_CARD_BROWSER:
					if(Params.IsTimedEventOver)
					{
						Params.EventDetails["CardsOwned"] = this.PlayerProfile.OwnedCards.Values.Sum().ToString();
						Params.EventDetails["DecksOwned"] = this.PlayerProfile.DeckList.Count.ToString();
					}
					break;
				case LogEventParams.EVENT_TIME_FRIENDS:
					if(Params.IsTimedEventOver)
						Params.EventDetails["NumberOfFriends"] = (this.PlayerProfile.FriendListData == null) ? "0" : this.PlayerProfile.FriendListData.Count.ToString();
					break;
				case LogEventParams.EVENT_TIME_STORE:
					if(Params.IsTimedEventOver && !string.IsNullOrEmpty(Params.ItemId))
						Params.EventDetails["ItemId"] = Params.ItemId;
					break;
			}
            
            

//#endif
            // Log the event to Flurry
            if (Params.EventDetails == null || Params.EventDetails.Count == 0)
			{
				if(Params.IsTimedEvent && Params.IsTimedEventOver)
					FlurryAnalytics.Instance.EndTimedEvent(Params.EventName);
				else
					FlurryAnalytics.Instance.LogEvent (Params.EventName, Params.IsTimedEvent);
                Debug.LogFormat("#LOGGING#Event {0}", Params.EventName);
			}
			else 
			{
                List<string> keys = Params.EventDetails.Keys.ToList<string>();

                foreach(var key in keys)
                {
                    if(string.IsNullOrEmpty(Params.EventDetails[key]))
                    {
                        //Debug.LogFormat("Null value in Params.EventDetails[{0}]", key);
                        Params.EventDetails.Remove(key);
                    }
                }
				if(Params.IsTimedEvent && Params.IsTimedEventOver)
					FlurryAnalytics.Instance.EndTimedEvent(Params.EventName, Params.EventDetails);
				else
					FlurryAnalytics.Instance.LogEventWithParameters (Params.EventName, Params.EventDetails, Params.IsTimedEvent);
                Debug.LogFormat("#LOGGING#Event {0}; Details: {1}", Params.EventName, JsonConvert.SerializeObject(Params.EventDetails, Formatting.None));

                //Debug.LogFormat("#LOGGING#json: {0}",FlurryAnalyticsIOS.ConvertParameters(Params.EventDetails));
            }


			// logging
			//Debug.LogFormat("Event logged: {0}", Params.EventName);
		}
	}

	public class LogEventParams
	{
		public string EventName;
		public Dictionary<string, string> EventDetails = new Dictionary<string, string>();
		public bool IsTimedEvent;
		public bool IsTimedEventOver;
		public int EventValue;
		public int EventValue2;
		public string ISOCurrencyCode;
		public string ItemId;
		public string ItemClass;
		public CurrencyType CurrencyType;
		public int CurrencyBalance;
		public bool isTwitter;
		public GameFormat Format;
		public bool hasWon;

		/*
		public LogEventParams(string eventName, Dictionary<string, string> eventDetails = null, bool isDuration = false)
		{
			EventName = eventName;
			EventDetails = eventDetails;
			IsTimedEvent = isDuration;
		}*/

		public const string EVENT_PURCHASE = "purchase";
		public const string EVENT_VIRTUAL_PURCHASE = "virtual_purchase";
		public const string EVENT_LEVEL_UP = "level_up";
		public const string EVENT_GAME_COMPLETED = "game_completed";
		public const string EVENT_TOURNAMENT_COMPLETED = "tournament_completed";
		public const string EVENT_CURRENCY_CHANGE = "currency_change";
		public const string EVENT_SOCIAL_MEDIA_POST = "social_post";

		// Timed events
		public const string EVENT_TIME_WAIT_FOR_GAME = "time_wait_for_game"; // time the player spends waiting for a game
		public const string EVENT_TIME_IN_GAME = "time_in_game"; // time the player is playing in
		public const string EVENT_TIME_CARD_BROWSER = "time_card_browser"; // time the player is in the card browser
		public const string EVENT_TIME_FRIENDS = "time_friends"; // time the friends list is open
		public const string EVENT_TIME_STORE = "time_in_store"; // time the user spends in the store
	}
}

