﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using NovaBlitz.UI;
using GameAnalyticsSDK;
using KHD;

namespace NovaBlitz.UI
{
	public class InitializeAnalyticsSignal :Signal {}
	public class InitializeAnalyticsCommand : Command
	{
		[Inject] public PlayerProfile PlayerProfile { get; set;}
		[Inject] public LogEventSignal LogEventSignal { get; set;}
		[Inject] public INovaContext MainContext { get; set; }
        [Inject] public GameData GameData { get; set; }

        // Flurry constants
        private const string FLURRY_IOS_API_KEY = "B2XHQWXSBYPRB4CCPYB6";//"5B2496QYVB5T23845GBD"; //"52KMY6XJ55ZKGNFPS8K5";// "B2XHQWXSBYPRB4CCPYB6";
        private const string FLURRY_ANDROID_API_KEY = "CFB62HDXJB45GYVXW8S7"; //"5PJH8RFB974BQNKJHHPK"; //"Q55DGGRN88XCWM7CXSYD";

		// Fiksu constants
		//private const string FIKSU_ITunesApplicationID = "com.dragonfoundry.novablitz";
		//private const bool FIKSU_DebugModeEnabled = false;

		// Tune constants
		//private const string TUNE_CONVERSION_KEY = "f6ef4ec9f9420f6238d25299a0aa90f2";
		//private const string TUNE_ADVERTISER_ID = "943";

		// Fuseboxxx constants


		public override void Execute ()
		{
			// No analytics in editor, or if the age gat isn't passed.
			if (PlayerProfile.isAgeGatePassed != true)// || Application.isEditor)
			{
				Debug.LogFormat ("Not initializing analytics. Age Gate: {0}, isEditor: {1}", 
					(PlayerProfile.isAgeGatePassed != null) ? PlayerProfile.isAgeGatePassed.ToString () : "null", Application.isEditor.ToString ());
				return;
			}

            this.PlayerProfile.isAdTrackingEnabled = true;

            Debug.Log("Initializing GameAnalytics");
            GameAnalytics.SetCustomId(this.PlayerProfile.PlayFabId);
            GameAnalytics.NewDesignEvent("AnalyticsInitialized");

            //this.Retain();

            Debug.Log("Initializing Flurry Analytics");
            //			KHD.FlurryAnalytics.Instance.SetDebugLogEnabled(true);
            
            // Set custom app version.
            KHD.FlurryAnalytics.Instance.SetAppVersion(this.GameData.VersionLabel);

            // Track unique user info.
#if (UNITY_5_2 || UNITY_5_3_OR_NEWER)
			//UnityEngine.Analytics.Analytics.enabled = true;
            FlurryAnalytics.Instance.replicateDataToUnityAnalytics = true;
#endif
            FlurryAnalytics.Instance.SetUserId(this.PlayerProfile.MetaProfile.PlayFabID);
			FlurryAnalytics.Instance.SetSessionContinueSeconds (5);
			FlurryAnalytics.Instance.StartSession (FLURRY_IOS_API_KEY, FLURRY_ANDROID_API_KEY, true);
            FlurryAnalyticsIOS.SetIAPReportingEnabled(true);
            FlurryAnalyticsIOS.SetSessionReportsOnPauseEnabled(true);
            FlurryAnalyticsIOS.SetSessionReportsOnCloseEnabled(true);
            this.LogEventSignal.Dispatch(new LogEventParams {
				EventName = "AnalyticsInitialized",
				EventDetails = null,
				IsTimedEvent = false});

			/*
            // TUNE
            Debug.Log("Initializing TUNE");
			Tune.Init(TUNE_ADVERTISER_ID, TUNE_CONVERSION_KEY);
			//Tune.AutomateIapEventMeasurement(true);
			Tune.SetUserId(this.PlayerProfile.MetaProfile.PlayFabID);
			// Track Steam players as existing users, not new users.
			if(PlayerProfile.LoginInfoResult != null 
				&& PlayerProfile.LoginInfoResult.AccountInfo != null
				&& (PlayerProfile.LoginInfoResult.AccountInfo.SteamInfo != null
					|| PlayerProfile.LoginInfoResult.AccountInfo.CustomIdInfo != null))
			{
				Tune.SetExistingUser(true);
			}
			Tune.MeasureSession();*/

			/*
            // We don't initialize FIKSU outside of IOS; we DO initialize Flurry, because it replicates events to Unity Analytics, and TUNE because it makes things easier
            // FIKSU
            Debug.Log("Initializing FIKSU");
			Fiksu.Initialize(new Dictionary<string, object>()
				{
					{ Fiksu.ITunesApplicationIDKey, FIKSU_ITunesApplicationID },
					{ Fiksu.DebugModeEnabledKey, FIKSU_DebugModeEnabled },
					{ Fiksu.ProductIdentifiersKey, new String[]{"test"} }
				});

			Fiksu.SetClientID("NovaBlitz");
			Fiksu.UploadRegistration(Fiksu.FiksuRegistrationEvent.EVENT1);
			Fiksu.UploadPurchase(Fiksu.FiksuPurchaseEvent.EVENT2, 0, "USD"); // tracks sessions;*/

			// TODO - UNCOMMENT THIS SO WE CAN USE IT (it crashes the app right now)
            // FUSE - just start the session. The object's already initialized.
            //this.FuseService.StartSession();

			Debug.LogFormat("Analytics Initialized: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);


			// Just one line of code to get wide range of metrics like:
			// Sessions
			// Active Users
			// New Users
			// Session Length
			// Frequency of Use
			// Custom User Segments
			// User Retention
			// Version Adoption
			// Devices
			// Carriers
			// Firmware Versions
		}
	}
}

