﻿using UnityEngine;
using System.Collections.Generic;
using Messages;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System;
using NovaBlitz.UI;


namespace NovaBlitz.UI
{
	public class EndTimedAnalyticsEventSignal :Signal<LogEventParams> {}
	public class EndTimedAnalyticsEventCommand : Command
	{
		[Inject] public PlayerProfile PlayerProfile { get; set; }

		[Inject] public LogEventParams Params { get; set; }

		public override void Execute ()
		{
			if (PlayerProfile.isAgeGatePassed != true || string.IsNullOrEmpty(Params.EventName))
				return;
			// End the event logging
			KHD.FlurryAnalytics.Instance.EndTimedEvent (Params.EventName, Params.EventDetails);
		}

	}
}

