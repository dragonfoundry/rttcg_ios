﻿using System;
using UnityEngine;
using System.Collections.Generic;
using LinqTools;
using Newtonsoft.Json;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
    public class RateGameSignal : Signal<RateGamePoint> { }
    public class RateGameCommand : Command
    {
        // Signal Params
        [Inject] public RateGamePoint RateGamePoint { get; set; }
        
        // data injections
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GameData GameData { get; set; }

        // Signal injectinos
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }

        //public const string LISTED_VERSION = "this version";
        public override void Execute()
        {
            if((Application.platform != RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.Android))
            {
                return; // no rating panel on steam
            }
            // don't let them see two rating prompts in the same calendar day
            if (PlayerProfile.RateGameTrackingData.LastTimeRatingPanelSeen > DateTime.UtcNow.AddHours(-1))
            {
                Debug.Log("#RateGame# 1: already seen the rating panel today; stopping");
                return;
            }

            Debug.Log("#RateGame# 2: not in same calendar day");
            // Reset the tracking if the version is different in any way (ios/android), or if the major version is different (steam)
            if (((Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
                && PlayerProfile.RateGameTrackingData.ListedVersion == this.GameData.VersionLabel)
                || PlayerProfile.RateGameTrackingData.ListedMajorVersion == this.GameData.VersionMajor)
            {
                if (PlayerProfile.RateGameTrackingData.HasRatedListedVersion || PlayerProfile.RateGameTrackingData.TimesSeen >= 3)
                {
                    Debug.Log("#RateGame# 3a: same version. Already rated, or seen too many times for this version. stopping.");
                    return;
                }
                Debug.Log("#RateGame# 3b: same version; good to go");
            }
            else
            {
                PlayerProfile.RateGameTrackingData = new RateGameTrackingData();
                PlayerProfile.RateGameTrackingData.ListedVersion = this.GameData.VersionLabel;
                PlayerProfile.RateGameTrackingData.ListedMajorVersion = this.GameData.VersionMajor;
                ExportToPlayerPrefs();
                Debug.Log("#RateGame# 3c: version updated; good to go");
            }
            
            switch (RateGamePoint)
            {
                case RateGamePoint.LevelUp:
                    if(!PlayerProfile.RateGameTrackingData.IsRankUpSeen)
                    {
                        Debug.Log("#RateGame# 5a: Rank Up");
                        PlayerProfile.RateGameTrackingData.IsRankUpSeen = true;
                        RateThisVersion();
                    }
                    break;
                case RateGamePoint.EventEnd:
                    if (!PlayerProfile.RateGameTrackingData.IsEventEndSeen)
                    {
                        Debug.Log("#RateGame# 5b: Event End");
                        PlayerProfile.RateGameTrackingData.IsEventEndSeen = true;
                        RateThisVersion();
                    }
                    break;
                case RateGamePoint.WinStreak:
                    if (!PlayerProfile.RateGameTrackingData.IsWinStreakSeen)
                    {
                        Debug.Log("#RateGame# 5c: Win Streak");
                        PlayerProfile.RateGameTrackingData.IsWinStreakSeen = true;
                        RateThisVersion();
                    }
                    break;
                case RateGamePoint.Purchase:
                    if (!PlayerProfile.RateGameTrackingData.IsPurchaseSeen)
                    {
                        Debug.Log("#RateGame# 5d: Purchase");
                        PlayerProfile.RateGameTrackingData.IsPurchaseSeen = true;
                        RateThisVersion();
                    }
                    break;
                case RateGamePoint.DraftQuest:
                    if (!PlayerProfile.RateGameTrackingData.IsDraftQuestSeen)
                    {
                        Debug.Log("#RateGame# 5e: DraftQuest");
                        PlayerProfile.RateGameTrackingData.IsDraftQuestSeen = true;
                        RateThisVersion();
                    }
                    break;
                default:
                    Debug.Log("#RateGame# 5f: FAILED");
                    return;
            }
        }

        private void RateThisVersion()
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(RatingsView), (v) =>
            {
                ((RatingsView)v).RateGamePoint = this.RateGamePoint;
            });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(RatingsView));
            PlayerProfile.RateGameTrackingData.TimesSeen++;
            PlayerProfile.RateGameTrackingData.LastTimeRatingPanelSeen = DateTime.UtcNow;
            ExportToPlayerPrefs();
            
            Debug.Log("#RateGame# 5: Rating view opened; data updated");
        }

        private void ExportToPlayerPrefs()
        {
            try
            {
                string converted = JsonConvert.SerializeObject(PlayerProfile.RateGameTrackingData);
                PlayerPrefs.SetString(this.PlayerProfile.MetaProfile.PlayFabID + NovaConfig.RATE_GAME_SUFFIX, converted);
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("#RateGame# Rate Game Exception {0}", e);
            }
        }
    }

    public enum RateGamePoint
    {
        NoPoint,
        LevelUp, // end of game
        EventEnd, // end of game
        WinStreak, // end of game
        Purchase, // store
        DraftQuest, // after completing the "earn a draft" quest
    }
}
