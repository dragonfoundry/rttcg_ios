﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using PlayFab.ClientModels;
using PlayFab;
using Messages;

namespace NovaBlitz.UI
{
    public class SmartPackOpenedSignal : Signal<OpenPackResponse> { }
    public class SmartPackOpenedCommand : Command
    {
        [Inject] public OpenPackResponse Response { get; set; }

        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject] public OpenPackSignal OpenPackSignal { get; set; }
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
		[Inject] public LogEventSignal LogEventSignal {get;set;}
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

        public override void Execute()
        {
            if (Response.Error != ErrorCode.NoError)
            {
                Debug.LogErrorFormat("Pack opening error: ", Response.Error.ToString());
                return;
            }
            if (Response.ContainedCardIds.Count != 5)
                Debug.LogErrorFormat("Pack wrong size. Should be 5, but is {0}", Response.ContainedCardIds.Count);

            Debug.Log("Purchased smart pack!");
            int balance;
			PlayerProfile.Wallet.Currencies.TryGetValue (Response.PaymentCurrency, out balance);
			var newBalance = Math.Max (0, balance - Response.ExpectedPrice);
			PlayerProfile.Wallet.Currencies [Response.PaymentCurrency] = newBalance;

            // Metrics & Logging
			this.LogEventSignal.Dispatch(new LogEventParams{
				EventName = LogEventParams.EVENT_VIRTUAL_PURCHASE, 
				CurrencyType = Response.PaymentCurrency, 
				CurrencyBalance = newBalance,
				EventValue = Response.ExpectedPrice,
				ItemId = Response.PackId.ToString(), 
				ItemClass = NBEconomy.ITEMCLASS_BOOSTER});
            this.MetricsTrackingService.TrackPackOpening(Response.PackName, Response.PaymentCurrency, Response.ExpectedPrice);

            List<int> items = new List<int>();
            foreach (var item in Response.ContainedCardIds)
            {
                items.Add(item);
            }

            PlayerProfile.UnopenedPack = new SmartPackInfo { PackID = Response.PackName, CardIDs = items.ToArray() };
            this.ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);
            OpenPackSignal.Dispatch(Response);
        }
    }
}
