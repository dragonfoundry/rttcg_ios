﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
	public class RegisterUserSignal : Signal<UserCredentials> {}
	public class UsernameNotAvailable : Signal<string> {}
	public class RegisterUserCommand : Command
	{
		// Signal Parameters
		[Inject] public UserCredentials UserCredentials {get;set;}

		// Dependency Injection
		[Inject] public ClientPrefs ClientPrefs {get;set;}
		[Inject] public MetagameSettings MetagameSettings {get;set;}
		[Inject] public UserRegisteredSignal UserRegisteredSignal {get;set;}
		[Inject] public UserLoggedInSignal UserLoggedInSignal {get;set;}
		[Inject] public UsernameNotAvailable UsernameNotAvailable {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public AddDisplayNameSignal AddDisplayNameSignal { get; set; }


		public override void Execute ()
		{
            this.Retain();
            Debug.LogFormat("Registering on playfab with {0}/{1}", this.UserCredentials.Username, "*Redacted*");
            RegisterPlayFabUserRequest req = new RegisterPlayFabUserRequest();
            req.TitleId = this.MetagameSettings.GameTitleId;
            req.Username = this.UserCredentials.Username;
            req.Password = this.UserCredentials.Password;
            req.Email = (this.PlayerProfile.isAgeGatePassed == true) ? this.UserCredentials.Email : StringUtils.HashEmail(this.UserCredentials.Email);
            if (this.PlayerProfile.isAgeGatePassed != true)
            {
                req.RequireBothUsernameAndEmail = false;
                req.Username = null;
            }

            PlayFabClientAPI.RegisterPlayFabUser(req, OnRegisterUser, OnLoginFailure);
           
		}

		void OnRegisterUser(RegisterPlayFabUserResult result) 
		{	
			Debug.Log ("Registered user");
            this.UserRegisteredSignal.Dispatch(result.SessionTicket);
			this.UserLoggedInSignal.Dispatch (new LoginResult { PlayFabId = result.PlayFabId, SessionTicket = result.SessionTicket });
			this.Release();
			if(string.IsNullOrEmpty(result.Username))
			{
				this.AddDisplayNameSignal.Dispatch (result.PlayFabId);
			}
            this.Release();
		}
        
        void OnLoginFailure(PlayFabError pfe) 
		{
			Debug.LogError("got register error " + pfe.Error + ": " + pfe.ErrorMessage);
			
			if(pfe.Error == PlayFabErrorCode.UsernameNotAvailable 
                || pfe.Error == PlayFabErrorCode.InvalidUsername 
                || pfe.Error == PlayFabErrorCode.InvalidUsernameOrPassword 
                || pfe.Error == PlayFabErrorCode.DuplicateUsername)
			{
				this.UsernameNotAvailable.Dispatch(this.UserCredentials.Username);
			}
			
			this.Fail();
            this.Release();
        }
	}
}
