﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
    public class UserCredentials
    {
        public UserCredentials(string username, string password, string email = null)
        {
            this.Username = username;
            this.Password = password;
            this.Email = email;
        }
        public UserCredentials(string email)
        {
            this.Email = email; ;
        }

        public string Username;
        public string Password;
        public string Email;
    }

    public class LoginUserWithEmailSignal : Signal<UserCredentials> {}
	public class AccountEmailFoundSignal : Signal<bool> {}
	public class InvalidEmailOrPasswordSignal : Signal {}
	
    public class LoginUserWithEmailCommand : Command
    {
		// Signal Parameters
		[Inject] public UserCredentials UserCredentials {get;set;}

		// Injected Dependencies
		[Inject] public MetagameSettings MetagameSettings {get;set;}
		[Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public UserLoggedInSignal UserLoggedInSignal {get;set;}
		[Inject] public AccountEmailFoundSignal AccountEmailFoundSignal {get;set;}
		[Inject] public InvalidEmailOrPasswordSignal InvalidEmailOrPasswordSignal {get;set;}
		[Inject] public LoginUserWithHashedEmailSignal LoginUserWithHashedEmailSignal { get; set; }

        private int Attempts = 0;

        /// <summary>
        /// Call the playfab
        /// </summary>
        public override void Execute ()
		{
			this.Retain();
            Timer.Instance.StartCoroutine(TryLogin());
		}

        private IEnumerator TryLogin()
        {
            yield return new WaitForSecondsRealtime(Attempts * Attempts);
            Attempts++;
            Debug.LogFormat("Logging into playfab ({0}) with {1}, password redacted", this.MetagameSettings.GameTitleId, this.UserCredentials.Email);
            LoginWithEmailAddressRequest req = new LoginWithEmailAddressRequest();
            req.TitleId = this.MetagameSettings.GameTitleId;
            req.Email = this.UserCredentials.Email;
            req.Password = this.UserCredentials.Password;
            req.InfoRequestParameters = new GetPlayerCombinedInfoRequestParams();
            req.InfoRequestParameters.GetUserAccountInfo = true;
            PlayFabClientAPI.LoginWithEmailAddress(req, OnLoginSuccess, OnLoginFailure);
        }
		
		void OnLoginSuccess(LoginResult result) 
		{
            this.PlayerProfile.isAgeGatePassed = true; // they've logged in a compliant account, so open everything up.
			this.PlayerProfile.LoginInfoResult = result.InfoResultPayload;
            PlayerPrefs.SetString("isAgeGatePassed", "true");
			Debug.LogFormat("got login result {0}", result.SessionTicket);
            this.UserLoggedInSignal.Dispatch(result);
            
            this.Release();
		}
        
        void OnLoginFailure(PlayFabError pfe) 
		{
			switch(pfe.Error)
			{
				case PlayFabErrorCode.AccountNotFound:
                    // if we're in Kid mode, follow on by trying the hashed email versionS
                    Debug.LogWarning("***No basic account; trying hashed version***");
                    LoginUserWithHashedEmailSignal.Dispatch(UserCredentials);
                    /*if (this.PlayerProfile.isAgeGatePassed != true)
                    {
                        Debug.LogWarning("***No basic account; trying hashed version***");
                        LoginUserWithHashedEmailSignal.Dispatch(UserCredentials);
                    }
                    else
                    {
                        Debug.LogWarning("***Plaintext Account not Found***");
                        this.AccountEmailFoundSignal.Dispatch(false);
                    }*/
                    break;
				case PlayFabErrorCode.InvalidEmailOrPassword:
					Debug.LogWarning("***Compliant Account Found***");
					this.AccountEmailFoundSignal.Dispatch(true);
					this.InvalidEmailOrPasswordSignal.Dispatch();
				    break;
                default:
                    if (Attempts < 3)
                    {
                        TryLogin();
                        return;
                    }
                    break;
			}
			this.Fail();
            this.Release();
        }

    }
	
	
}
