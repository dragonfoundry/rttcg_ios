﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using PlayFab;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class LogoutUserSignal : Signal<ShouldUnlinkDevice, ShouldAutoReconnect> {}
	public class LogoutUserCommand : Command
	{
		// Dependency Injection
		[Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public ClientService ClientService { get; set; }
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public PlayFabChatClient PlayfabChatClient {get;set;}
        [Inject] public UnlinkDeviceOrSteamSignal UnlinkDeviceOrSteamSignal { get; set; }
        [Inject] public DeviceOrSteamUnlinkedSignal UnlinkRequestSentSignal { get; set; }
		[Inject] public ViewReferences ViewReferences { get; set; }
		[Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public WaitForPlayerDataSignal WaitForPlayerDataSignal { get; set; }
        //[Inject] public ChillingoService ChillingoService { get; set; }
        [Inject] public UnloadUnusedResourcesSignal UnloadUnusedResourcesSignal { get; set; }
        [Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal { get; set; }

        // Parameter injection
        [Inject] public ShouldUnlinkDevice IsUnlinkRequest { get; set; }
        [Inject] public ShouldAutoReconnect IsAutoReconnect { get; set; }

		public override void Execute ()
        {
            this.CloseAnimatedViewSignal.Dispatch(typeof(ProfileMenuView));
            this.CloseAnimatedViewSignal.Dispatch(typeof(SettingsView));
            this.CloseAnimatedViewSignal.Dispatch(typeof(ReconnectDialogView));
            this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
			this.Retain();
            if (PlayfabChatClient != null && PlayfabChatClient.chatClient != null)
                this.PlayfabChatClient.chatClient.Disconnect();
//#if TEST_BUILD
//            CompleteLogout(false);
//#else
            if (IsUnlinkRequest == ShouldUnlinkDevice.UnlinkAccount)
            {
                UnlinkRequestSentSignal.AddOnce(CompleteLogout);
                UnlinkDeviceOrSteamSignal.Dispatch();
            }
            else
                CompleteLogout(false);
//#endif
        }

        private void CompleteLogout(bool unlinkSuccess)
        {
            this.ClientPrefs.ResetClientPrefs();
            PlayerProfile.ResetProfile();
            //PlayerProfile.isAgeGatePassed = ChillingoService.IsAgeGatePassed;
            //PlayFabClientAPI.AuthKey = null; // We're not going to turn off their session ticket; it's not used to re-log-in anyway.
            Debug.Log("Logged out user");
            ViewReferences.DestroyAllViews ();
            //StartSignal.Dispatch ();
            //this.ClientService.OnLogout();
            this.UnloadUnusedResourcesSignal.Dispatch();

			this.CloseAnimatedViewSignal.Dispatch (typeof(LoginDialogNewFlowView));
            this.ClientService.OnLogout();
            this.Retain();
            Timer.Instance.StartCoroutine(WaitToReconnectCoroutine());
        }

        private IEnumerator WaitToReconnectCoroutine()
        {
            yield return new WaitForSecondsRealtime(2.0f);
            if (IsAutoReconnect == ShouldAutoReconnect.AutomaticallyReconnect)
            {
                WaitForPlayerDataSignal.Dispatch();
                this.Release();
            }
            else
            {
                this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams
                {
                    IsWaitForResponse = false,
                    Prompt = I2.Loc.ScriptLocalization.Error.ServerDownText,
                    Title = I2.Loc.ScriptLocalization.Error.ServerDown,
                    DialogDismissedHandler = ConfirmDismissedHandler
                });
            }
            this.Release();
        }

        public void ConfirmDismissedHandler(bool isConfirmed)
        {
            WaitForPlayerDataSignal.Dispatch();
            this.Release();
        }
    }


    public enum ShouldUnlinkDevice
    {
        UnlinkAccount,
        DontUnlinkAccount,
    }
    public enum ShouldAutoReconnect
    {
        AutomaticallyReconnect,
        DontReconnect,
    }
}
