/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
	public class LoadCatalogSignal : Signal<string> {}
	public class LoadCatalogCommand : Command
	{
		[Inject] public string SignalVersion {get;set;}
		[Inject] public GameData GameData {get; set;}
		[Inject] public CatalogLoadedSignal CatalogLoadedSignal {get; set;}

		public override void Execute()
		{
			this.Retain();
			GetCatalogItemsRequest req = new GetCatalogItemsRequest();
			req.CatalogVersion = this.SignalVersion;
			PlayFabClientAPI.GetCatalogItems(req, OnGetCatalogItemsResult, OnPlayFabError);
		}

		void OnGetCatalogItemsResult(GetCatalogItemsResult result)
		{
			CacheData(result.Catalog);
			this.CatalogLoadedSignal.Dispatch(this.SignalVersion);

            Debug.Log("Got catalog items");
            // Make sure all the store items have product data attached
            foreach (var store in GameData.StoreCache)
            {
                foreach (var storeItem in store.Value)
                {
                    ProductData pdata;
                }
            }
            this.Release();
		}
			
		void OnPlayFabError(PlayFabError pfe) 
		{
			this.Release();
		}

		#region Conversion

		void CacheData(IEnumerable<CatalogItem> catalog)
		{
			this.GameData.ProductCache = catalog.Select(item => PlayFabToNova(item)).ToDictionary(prod => prod.ItemId);

            int cardId;
            foreach (var kvp in GameData.ProductCache)
            {
                if (kvp.Value.ItemClass == NBEconomy.ITEMCLASS_CARD)
                {
                    if (int.TryParse(kvp.Key, out cardId))
                    {
                        this.GameData.CardProductCache[cardId] = kvp.Value;
                    }
                    else
                        Debug.Log("card failed: " + kvp.Key);
                }
            }
			this.GameData._catalogCached = true;
		}

        NovaCatalogItem PlayFabToNova(CatalogItem item)
		{
            NovaCatalogItem pdata = new NovaCatalogItem();

			pdata.ItemId 			= item.ItemId;
			pdata.ItemClass 		= item.ItemClass;
			pdata.CatalogVersion	= item.CatalogVersion;
			pdata.DisplayName 		= item.DisplayName;
			pdata.Description 		= item.Description;
			pdata.CanBecomeCharacter = item.CanBecomeCharacter;
			pdata.IsStackable 		= item.IsStackable;
         //   pdata.ItemImageURL      = item.ItemImageURL;

			pdata.CustomData 		= item.CustomData;
			if (null != item.Tags)
				pdata.Tags 			= item.Tags.ToArray();
			//if (null != item.GrantedIfPlayerHas)
			//	pdata.GrantedIfHas 	= item.GrantedIfPlayerHas.ToArray();

			pdata.Consumable 		= PlayFabToNova(item.Consumable);
			pdata.Container 		= PlayFabToNova(item.Container);
			pdata.Bundle 			= PlayFabToNova(item.Bundle);

			pdata.VirtualCurrencyPrices = item.VirtualCurrencyPrices == null ? null : item.VirtualCurrencyPrices.ToDictionary(k => CurrencyUtils.GetCode(k.Key), v=> v.Value);

			return pdata;
		}

		ConsumableData PlayFabToNova(CatalogItemConsumableInfo consumable)
		{
			if (null == consumable)
				return null;

			ConsumableData cdata = new ConsumableData();
			cdata.UsageCount 		= consumable.UsageCount;
			cdata.UsagePeriod 		= consumable.UsagePeriod;
			cdata.UsagePeriodGroup 	= consumable.UsagePeriodGroup;
			return cdata;
		}
		
		ContainerData PlayFabToNova(CatalogItemContainerInfo container)
		{
			if (null == container)
				return null;

			return new ContainerData{
				KeyItemID 				= container.KeyItemId,
				ItemContents 			= container.ItemContents != null ? container.ItemContents.ToArray() : null,
				ResultTableContents 	= container.ResultTableContents != null ? container.ResultTableContents.ToArray() : null,
				VirtualCurrencyContents = container.VirtualCurrencyContents
			};
		}

		BundleData PlayFabToNova(CatalogItemBundleInfo bundle)
		{
			if (null == bundle)
				return null;

			BundleData bdata = new BundleData();
			if (null != bundle.BundledItems)
				bdata.Items 		= bundle.BundledItems.ToArray();
			if (null != bundle.BundledResultTables)
				bdata.ResultTables 	= bundle.BundledResultTables.ToArray();
			bdata.VirtualCurrencies = bundle.BundledVirtualCurrencies;
			return bdata;
		}

		#endregion
	}

    public class LoadFullCatalogSignal : Signal<string> { }
	public class LoadFullCatalogCommand : Command
	{
        [Inject] public string CatalogVersion { get; set; }

		[Inject] public LoadCatalogSignal LoadCatalogSignal {get;set;}
		[Inject] public CatalogLoadedSignal CatalogLoadedSignal {get; set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GameData GameData { get; set; }
        
		public override void Execute()
		{
            Debug.LogFormat("LoadFullCatalogCommand: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
			this.Retain();
			this.CatalogLoadedSignal.AddListener(OnCatalogLoaded);
            
			this.LoadCatalogSignal.Dispatch(CatalogVersion);
		}

		void OnCatalogLoaded(string version)
		{
			this.CatalogLoadedSignal.RemoveListener(OnCatalogLoaded);
            this.PlayerProfile.isAllCatalogsLoaded = true;
            Debug.LogFormat("Full Catalog Loaded: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Release();
			
		}
	}
}*/
