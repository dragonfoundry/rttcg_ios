﻿using System.Collections.Generic;
using LinqTools;
using UnityEngine;
using UnityEngine.Purchasing;
using strange.extensions.command.impl;
using Newtonsoft.Json;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
    public class LoadIAPProductsCommand : Command
    {
        [Inject] public GameData GameData { get; set; }
       // [Inject] public InitializePurchasingSignal InitializePurchasingSignal { get; set; }

        public override void Execute()
        {
            Dictionary<string, StoreData> store = GameData.StoreCache[NBEconomy.STORE_MAIN];
			Dictionary<string, NovaCatalogItem> catalog = GameData.ProductCache.Values.Where(prod => prod.ItemClass == NBEconomy.ITEMCLASS_STORE_ITEM).ToDictionary(prod => prod.ItemId);

            List<UnityIAPProduct> products = new List<UnityIAPProduct>();
            foreach (var item in store)
            {
                if (item.Value.ItemId != null && catalog.ContainsKey(item.Value.ItemId))
                {
                    var catItem = catalog[item.Value.ItemId];
                    var catCustomData = JsonConvert.DeserializeObject<Dictionary<string, string>>(catItem.CustomData);
                    var googleItemId = catCustomData["GoogleStoreIdentifier"];
                    var appleItemId = catCustomData["AppleStoreIdentifier"];
                    var productType = catCustomData["ProductType"];

                    var product = new UnityIAPProduct
                    {
                        ProductId = catItem.ItemId,
                        GoogleStoreIdentifier = googleItemId,
                        AppleStoreIdentifier = appleItemId
                    };
                    switch (productType)
                    {
                        case "consumable":
                            product.ProductType = ProductType.Consumable;
                            break;
                        case "non-consumable":
                            product.ProductType = ProductType.NonConsumable;
                            break;
                        case "subscription":
                            product.ProductType = ProductType.Subscription;
                            break;
                    }
                }
            }
            GameData.IAPProductCache = products;
       //     InitializePurchasingSignal.Dispatch(products);
        }
    }
}
