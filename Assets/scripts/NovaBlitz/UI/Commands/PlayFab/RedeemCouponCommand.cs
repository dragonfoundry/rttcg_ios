﻿using System;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class RedeemCouponSignal : Signal<string> { }
    public class RedeemCouponCommand : Command
    {
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public string CouponString { get; set; }
        public override void Execute()
        {
            if(CouponString.Length != 17)
            {
                return;
            }
            string itemId = CouponString.Substring(0, 4);
            string couponId = CouponString.Substring(5, 12);
            //this.Retain();
            this.ClientService.SendMessage(new Messages.RedeemCouponMessage { ItemId = itemId, Couponcode = couponId });
        }
    }
}
