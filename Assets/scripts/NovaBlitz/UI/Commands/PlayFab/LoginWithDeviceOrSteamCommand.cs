﻿using System;
using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using NovaBlitz.Game;
#if UNITY_STEAM
using Steamworks;
#endif
namespace NovaBlitz.UI
{
    public class LoginWithDeviceOrSteamSignal : Signal<bool> { }
    public class LoginWithDeviceOrSteamCommand : Command
    {
        // Injected Dependencies
        [Inject] public MetagameSettings MetagameSettings { get; set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public UserLoggedInSignal UserLoggedInSignal { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public LogInToServerSignal LogInToServerSignal { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public ServerReconnectSignal ServerReconnectSignal { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }
#if UNITY_STEAM
        [Inject] public SteamAuthTicketSignal SteamAuthTicketSignal {get;set; }
        [Inject] public GetSteamAuthTokenSignal GetSteamAuthTokenSignal { get; set; }
        [Inject] public NovaSteamService NovaSteamService { get; set; }
#endif

        // signal param
        [Inject] public bool IsCreateAccount { get; set; }

        private IEnumerator _waitForTimeout { get; set; }
        private IEnumerator _deviceLogin { get; set; }

        /// <summary>
        /// Call the playfab login function for the appropriate device
        /// </summary>
        public override void Execute()
        {
            this.Retain();
            this._waitForTimeout = WaitForTimeout();
            Timer.Instance.StartCoroutine(this._waitForTimeout);
#if UNITY_STEAM
            this.SteamAuthTicketSignal.AddListener(OnSteamAuthTicket);
            this.GetSteamAuthTokenSignal.Dispatch();
#else
            _deviceLogin = TryDeviceLogin();
            Timer.Instance.StartCoroutine(_deviceLogin);
#endif
        }
        private int Attempts = 0;
        private IEnumerator TryDeviceLogin()
        {
            yield return new WaitForSecondsRealtime(Attempts * Attempts);
            Attempts++;
#if UNITY_IOS
            Debug.Log("Logging into playfab with IOS: " + ClientPrefs.DeviceUniqueIdentifier);
            LoginWithIOSDeviceIDRequest iosRequest = new LoginWithIOSDeviceIDRequest();
            iosRequest.TitleId = this.MetagameSettings.GameTitleId;
            iosRequest.CreateAccount = IsCreateAccount;
            iosRequest.DeviceId = ClientPrefs.DeviceUniqueIdentifier;
			iosRequest.InfoRequestParameters = new GetPlayerCombinedInfoRequestParams();
			iosRequest.InfoRequestParameters.GetUserAccountInfo = true;
            PlayFabClientAPI.LoginWithIOSDeviceID(iosRequest, OnLoginSuccess, OnLoginFailure);
#elif UNITY_ANDROID
            var uniqueId = ClientPrefs.DeviceUniqueIdentifier;
            Debug.Log("Logging into Playfab with Android: " + uniqueId);
            LoginWithAndroidDeviceIDRequest androidRequest = new LoginWithAndroidDeviceIDRequest();
            androidRequest.TitleId = this.MetagameSettings.GameTitleId;
            androidRequest.CreateAccount = IsCreateAccount;
            androidRequest.AndroidDeviceId = uniqueId;
            androidRequest.InfoRequestParameters = new GetPlayerCombinedInfoRequestParams();
            androidRequest.InfoRequestParameters.GetUserAccountInfo = true;
            PlayFabClientAPI.LoginWithAndroidDeviceID(androidRequest, OnLoginSuccess, OnLoginFailure);
#else
            // Try Custom ID, which will fall back to simple login
            Debug.Log("Logging into playfab with Custom Id: " + ClientPrefs.DeviceUniqueIdentifier);
            LoginWithCustomIDRequest customRequest = new LoginWithCustomIDRequest();
            customRequest.TitleId = this.MetagameSettings.GameTitleId;
            customRequest.CreateAccount = IsCreateAccount;
			customRequest.CustomId = ClientPrefs.DeviceUniqueIdentifier;
			customRequest.InfoRequestParameters = new GetPlayerCombinedInfoRequestParams();
			customRequest.InfoRequestParameters.GetUserAccountInfo = true;
            PlayFabClientAPI.LoginWithCustomID(customRequest, OnLoginSuccess, OnLoginFailure);
#endif
        }

#if UNITY_STEAM
        void OnSteamAuthTicket(string authStr)
        {
            Debug.LogFormat("Steam ticket: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.SteamAuthTicketSignal.RemoveListener(OnSteamAuthTicket);
            if (string.IsNullOrEmpty(authStr))
            {
                Debug.LogError("Steam ticket is null - auth failed");
                Attempts = 3000;
                OnLoginFailure(new PlayFabError { Error = PlayFabErrorCode.AccountNotFound });
            }
            else
            {
                //Debug.LogFormat("Logging in with Steam: {0}", authStr);
				var loginWithSteam = new LoginWithSteamRequest () {
					CreateAccount = IsCreateAccount,
					TitleId = PlayFabSettings.TitleId,
					SteamTicket = authStr
				};
				loginWithSteam.InfoRequestParameters = new GetPlayerCombinedInfoRequestParams();
				loginWithSteam.InfoRequestParameters.GetUserAccountInfo = true;
				PlayFabClientAPI.LoginWithSteam(loginWithSteam, OnLoginSuccess, OnLoginFailure);
            }
        }
#endif

        // On success, continue the flow
        void OnLoginSuccess(LoginResult result)
        {
            Debug.LogFormat("Login Success: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            Timer.Instance.StopCoroutine(this._waitForTimeout);
            Debug.Log("got login result " + result.SessionTicket);
			this.PlayerProfile.LoginInfoResult = result.InfoResultPayload;

            if (ClientService.IsConnected)
            {
                string sessionTicket = result.SessionTicket;
                Debug.LogFormat("User Re-Logged in: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
                // Set the session ticket everywhere - this is too many places...
                this.PlayerProfile.MetaProfile.PlayFabSessionToken = sessionTicket;
                //PlayFabClientAPI.AuthKey = sessionTicket;
                this.ClientPrefs.SessionTicket = sessionTicket;

                // need to tell the server; server will refresh the player's cache
                this.LogInToServerSignal.Dispatch();
            }
            else
            {
                this.UserLoggedInSignal.Dispatch(result);
            }
            this.Release();
        }

        // On failure, go to the login or create account window
        void OnLoginFailure(PlayFabError pfe)
        {
#if !UNITY_STEAM
            if(Attempts < 3)
            {
                _deviceLogin = TryDeviceLogin();
                Timer.Instance.StartCoroutine(_deviceLogin);
            }
            else
#endif
            {
                Debug.LogFormat("Login Failure: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
                Timer.Instance.StopCoroutine(this._waitForTimeout);
                if (ClientService.IsConnected)
                {
                    Debug.LogError("Re-authentication failure. Something went wrong; disconnect & log out the user.");
                    this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect); // we're not un-linking the device here.
                }
                else
                {
                    Debug.LogFormat("Login with device id {0} failed because {1}", ClientPrefs.DeviceUniqueIdentifier, pfe.ErrorMessage);
                    if (pfe.Error == PlayFabErrorCode.AccountNotFound
                        || pfe.Error == PlayFabErrorCode.InvalidSteamTicket
                        || pfe.Error == PlayFabErrorCode.InvalidDeviceID) // This is a "good" error - allows us to progress the user to login or create an account.
                    {
                        Debug.Log("No account found. Directing to login or create account");
                        this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(FirstDialogView));
                    }
                    else if (pfe.Error == PlayFabErrorCode.DownstreamServiceUnavailable)
                    {
                        Debug.Log("Steam is down. Try email login instead");
                        this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));
                    }
                    else
                    {
                        //this.PlayfabErrorRaisedSignal.Dispatch(pfe);
                    }
                }

                this.Fail();
                this.Release();
            }
        }


        private IEnumerator WaitForTimeout()
        {
            yield return new WaitForSecondsRealtime(10);
            Debug.Log("#Login# Login timed out (no internet?)");
            this.ServerReconnectSignal.Dispatch(Messages.DisconnectCode.NotLoggedIn);
            if(_deviceLogin != null)
                Timer.Instance.StopCoroutine(_deviceLogin);
            this.Fail();
            this.Release();
        }
    }
}
