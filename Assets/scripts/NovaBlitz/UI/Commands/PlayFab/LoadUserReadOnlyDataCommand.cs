﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;

using PlayFab.ClientModels;
using PlayFab;
using PlayFab.Internal;

using NovaBlitz.Game;
using Newtonsoft.Json;
using Messages;

namespace NovaBlitz.UI
{
	public class LoadUserReadOnlyDataSignal : Signal {}
		
	public class LoadUserReadOnlyDataCommand : Command
	{
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}
        [Inject] public GameData GameData { get; set; }
        [Inject] public ClientService ClientService { get; set; }
		
		private const string basicDraftStatus = "basic_draft_status";
		private const string basicDraftCardsGranted = "basic_draft_cards_granted";
		private const string dailyRewardTimestamp = "daily_reward_timestamp";
		private const string basicDraftDraftedCards = "basic_draft_drafted_cards";
        private const string tutorialProgressKey = "tutorial_progress";
        private const string tutorialCompletedKey = "tutorial_completed";
        private const string leagueStatusKey = "constructed_status";
        private const string tournamentEventStatusKey = "event_status";

        public override void Execute ()
        {
            Debug.LogFormat("LoadUserReadOnlyData: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Retain();
			GetUserDataRequest request = new GetUserDataRequest();
			request.Keys = new List<string>()
			{
				basicDraftStatus,
				basicDraftCardsGranted,
				dailyRewardTimestamp,
				basicDraftDraftedCards,	
                tutorialProgressKey,
                tutorialCompletedKey,
                leagueStatusKey,
                tournamentEventStatusKey,
            };
			PlayFabClientAPI.GetUserReadOnlyData(request, onGetUserReadOnlyData, onPlayFabError);
		}

        private void onPlayFabError(PlayFabError error)
        {
			Debug.LogError("LoadUserReadOnlyDataCommand failed");
            this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams("Error Loading User Data", "We were unable to load your user data."));
            this.Fail();
            this.Release();
        }

        private void onGetUserReadOnlyData(GetUserDataResult result)
        {
            Debug.LogFormat("LoadUserReadOnlyData: got Data: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.ProcessReadOnlyData(result.Data);
            Debug.LogFormat("LoadUserReadOnlyData: processed data: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);

            this.Release();
        }
		
        private void ProcessReadOnlyData(Dictionary<string, UserDataRecord> data)
        {
			UserDataRecord draftStatus;
			UserDataRecord draftCardsGranted;
			UserDataRecord dailyRewardTime;
			UserDataRecord draftedCards;
            UserDataRecord tutorialProgress;
            UserDataRecord tutorialCompleted;
            UserDataRecord leagueStatus;
            UserDataRecord tournamentEventStatus;

            try
            {
                data.TryGetValue(basicDraftStatus, out draftStatus);
                data.TryGetValue(basicDraftCardsGranted, out draftCardsGranted);
                data.TryGetValue(dailyRewardTimestamp, out dailyRewardTime);
                data.TryGetValue(basicDraftDraftedCards, out draftedCards);
                data.TryGetValue(tutorialProgressKey, out tutorialProgress);
                data.TryGetValue(tutorialCompletedKey, out tutorialCompleted);
                data.TryGetValue(leagueStatusKey, out leagueStatus);
                data.TryGetValue(tournamentEventStatusKey, out tournamentEventStatus);
            } 
            catch
            {
                this.DataParseFailed("Unable to parse userData response.");
                return;
            }
            

            if (dailyRewardTime != null)
			{
                DateTime.TryParse(dailyRewardTime.Value, out PlayerProfile.DailyRewardTimestamp);
			}
            
            try
            {
                if(tutorialProgress != null)
                {
                    // Set the Current tutorial number. 
                    // If the reported one is exactly one lower than the stored one, don't update - it means the title data hasn't updated yet.
                    int currentTutorial;
                    if (!int.TryParse(tutorialProgress.Value, out currentTutorial))
                        PlayerProfile.OnboardingProgress.CurrentTutorial = 1;
                    else if (currentTutorial + 1 != PlayerProfile.OnboardingProgress.CurrentTutorial)
                        PlayerProfile.OnboardingProgress.CurrentTutorial = currentTutorial;

                }
                else
                {
                    PlayerProfile.OnboardingProgress.CurrentTutorial = 1;
                }
            }
            catch
            {
                this.DataParseFailed("Unable to prase tutorial progress");
                return;
            }
            
            try
            {
                if (tutorialCompleted != null)
                {
                    bool isCompleted;
                    if (!bool.TryParse(tutorialCompleted.Value, out isCompleted))
                        PlayerProfile.OnboardingProgress.TutorialsCompleted = false;
                }
                else
                {
                    PlayerProfile.OnboardingProgress.TutorialsCompleted = false;
                }
            }
            catch
            {
                this.DataParseFailed("Unable to parse tutorial completed data");
                return;
            }

            try
            {
                if (draftStatus != null)
                {
                    try
                    {
                        var statusData = Newtonsoft.Json.JsonConvert.DeserializeObject<Messages.TournamentData>(draftStatus.Value);
                        this.PlayerProfile.DraftData = statusData;
                    }
                    catch (Exception e)
                    {
                        Debug.LogFormat("Draft Data exception: {0}", e);
                        //string str = e.ToString();
                        this.PlayerProfile.DraftData = new TournamentData();
                    }
                }
                else
                {
                    this.PlayerProfile.DraftData = new TournamentData();
                }
            }
            catch
            {
                this.DataParseFailed("Unable to parse draft status data");
                return;
            }
            
            try
            {
                if (leagueStatus != null)
                {
                    try
                    {
                        var statusData = Newtonsoft.Json.JsonConvert.DeserializeObject<TournamentData>(leagueStatus.Value);
                        this.PlayerProfile.ConstructedData = statusData;
                        if (this.PlayerProfile.ConstructedData.event_status == TournamentStatus.playing)
                        {
                            this.PlayerProfile.LeagueDeck = new DeckData(GameFormat.Constructed) { Name = statusData.DeckName };
                            CardData cardData;
                            foreach (var cardId in statusData.DeckList)
                            {
                                if (GameData.CardDictionary.TryGetValue(cardId, out cardData))
                                {
                                    CardItem card = new CardItem(cardData, -2);
                                    this.PlayerProfile.LeagueDeck.Cards.Add(card);
                                }
                            }
                            this.PlayerProfile.LeagueDeck.QueryAspects();
                            this.PlayerProfile.LeagueDeck.SetDeckArt();
                        }
                        else
                        {
                            this.PlayerProfile.LeagueDeck = null;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogFormat("League Data exception: {0}", e);
                        //string str = e.ToString();
                        this.PlayerProfile.ConstructedData = new TournamentData();
                        this.PlayerProfile.LeagueDeck = null;
                    }
                }
                else
                {
                    this.PlayerProfile.ConstructedData = new TournamentData();
                }
            }
            catch
            {
                this.DataParseFailed("Unable to parse league status data");
                return;
            }
            
            try
            {

                if (tournamentEventStatus != null)
                {
                    try
                    {
                        // deserializes into a TournamentData AND an EventData. EventData includes start time, end time (both UTC),
                        // which let the client correctly show "collect Rewards" (drop from event)
                        var statusData = Newtonsoft.Json.JsonConvert.DeserializeObject<TournamentData>(tournamentEventStatus.Value);
                        this.PlayerProfile.TournamentData = statusData;
                        if(this.PlayerProfile.TournamentData.event_status == TournamentStatus.playing || PlayerProfile.TournamentData.event_status == TournamentStatus.drafting)
                        { 
                            this.PlayerProfile.EventDeck = new DeckData(GameFormat.Tournament) { Name = statusData.DeckName };

                            GameData.TournamentSchedule.TryGetValue(statusData.startTimeUTC, out this.PlayerProfile.CurrentTournament);

                            CardData cardData;
                            foreach (var cardId in statusData.DeckList)
                            {
                                if (GameData.CardDictionary.TryGetValue(cardId, out cardData))
                                {
                                    CardItem card = new CardItem(cardData, -2);
                                    this.PlayerProfile.EventDeck.Cards.Add(card);
                                }
                            }
                            this.PlayerProfile.EventDeck.QueryAspects();
                            this.PlayerProfile.EventDeck.SetDeckArt();
                        }
                        else
                        {
                            this.PlayerProfile.LeagueDeck = null;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogFormat("Tournament Data exception: {0}", e);
                        this.PlayerProfile.TournamentData = new TournamentData();
                    }
                }
                else
                {
                    this.PlayerProfile.TournamentData = new TournamentData();
                }
            }
            catch
            {
                this.DataParseFailed("Unable to parse tourniment data");
                return;
            }

            try
            {
                // Zero out the DraftDeck for players that havn't drafted
                if (this.PlayerProfile.DraftData.event_status == TournamentStatus.not_playing)
                {
                    this.PlayerProfile.DraftDeck = null;
                    this.PlayerProfile.DraftProgress.DraftedCards.Clear();
                }
                
                this.PlayerProfile.DraftDeck = new DeckData(GameFormat.Draft);

                if (draftedCards != null)
                {
                    object[] cards = JsonConvert.DeserializeObject<object[]>(draftedCards.Value);
                    this.PlayerProfile.DraftProgress.DraftedCards = new List<int>();
                    
                    for(int i=0;i<cards.Length;i++)
                    {
                        int cardID = (int)(double)cards[i];
                        CardData cardData = GameData.CardDictionary[cardID];
                        CardItem cardItem = new CardItem(cardData, -2);	
                        this.PlayerProfile.DraftProgress.DraftedCards.Add(cardItem.CardID);
                        this.PlayerProfile.DraftDeck.Cards.Add(cardItem);
                    }
                    
                    this.PlayerProfile.DraftDeck.QueryAspects();
                    this.PlayerProfile.DraftDeck.SetDeckArt();
                    Debug.LogWarningFormat("Drafted Cards: {0}", this.PlayerProfile.DraftProgress.DraftedCards.Count);
                }
            }
            catch
            {
                DataParseFailed("Unable to parse Draft deck");
                return;
            }
            
            Debug.LogWarningFormat("Draft status is:{0}", this.PlayerProfile.DraftData.event_status);
        }
        
        private void DataParseFailed(string message)
        {
            Debug.LogError(message);
            this.ClientService.ForceDisconnect(DisconnectCode.DataParseFailed);
            this.Fail();
            this.Release();
        }
    }
}