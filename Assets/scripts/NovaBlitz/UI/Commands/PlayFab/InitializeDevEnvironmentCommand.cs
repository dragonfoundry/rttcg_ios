﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class InitializeDevEnvironmentSignal: Signal {}

    public class InitializeDevEnvironmentCommand : Command
    {
		[Inject] public MetagameSettings MetagameSettings {get;set;}
		
		private string devTitleId = "721F";
		private string devChatAppId = "f4a27b89-e3c9-4cdb-9d25-fe6f5d2c9734";
		
		public override void Execute ()
		{
			//Debug.LogWarning("Initializing DEV Enviorment");
			this.MetagameSettings.GameTitleId = devTitleId;
			this.MetagameSettings.PhotonChatAppID = devChatAppId;
            this.MetagameSettings.IsTargetingDev = true;
		}
    }
}
