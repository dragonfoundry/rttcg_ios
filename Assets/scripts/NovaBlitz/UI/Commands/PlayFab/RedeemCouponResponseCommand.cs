﻿using System;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using System.Text;
using LinqTools;
using NovaBlitz.Economy;
using UnityEngine;

namespace NovaBlitz.UI
{
    public class RedeemCouponResponseSignal : Signal<RedeemCouponResponse> { }
    public class RedeemCouponResponseCommand : Command
    {
        [Inject] public RedeemCouponResponse Response { get; set; }

        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

        [Inject] public ServerErrorRaisedSignal ServerErrorRaisedSignal { get; set; }
        [Inject] public LogEventSignal LogEventSignal { get; set; }

        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }

        private const string BEGIN = "<nobr>";
        private const string END = "</nobr>";

        public override void Execute()
        {
            // either fire a gameoversignal (creating the game over message), or fire an error message.

            if(Response.Error != ErrorCode.NoError)
            {
                this.ServerErrorRaisedSignal.Dispatch(Response.Error, Response);
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(I2.Loc.ScriptLocalization.You_Gained).Append(":\n");
                // show the results.
                // update inventory
                bool first = true;
                if (Response.CurrencyResults != null)
                {
                    foreach (var kvp in Response.CurrencyResults)
                    {
                        int currentValue = 0;
                        this.PlayerProfile.Wallet.Currencies.TryGetValue(kvp.Key, out currentValue);
                        var newBalance = Math.Max(0, currentValue + kvp.Value);
                        PlayerProfile.Wallet.Currencies[kvp.Key] = newBalance;
                        Debug.LogFormat("Redeem Coupon: change by {0}{1} to {2} from {3}", kvp.Value, kvp.Key, newBalance, currentValue);

                        this.LogEventSignal.Dispatch(new LogEventParams
                        {
                            EventName = LogEventParams.EVENT_CURRENCY_CHANGE,
                            CurrencyType = kvp.Key,
                            CurrencyBalance = newBalance,
                            EventValue = kvp.Value
                        });
                        if(!first)
                            builder.Append(',').Append(' ');
                        builder.Append(BEGIN).Append(kvp.Value).Append(' ');
                        switch(kvp.Key)
                        {
                            case CurrencyType.SP:
                                builder.Append(kvp.Value == 1 ? I2.Loc.ScriptLocalization.Smart_Pack : I2.Loc.ScriptLocalization.Smart_Packs);
                                break;
                            case CurrencyType.CR:
                                builder.Append(kvp.Value == 1 ? I2.Loc.ScriptLocalization.Credit : I2.Loc.ScriptLocalization.Credits);
                                break;
                            case CurrencyType.NG:
                                builder.Append(kvp.Value == 1 ? I2.Loc.ScriptLocalization.Gem : I2.Loc.ScriptLocalization.Gems);
                                break;
                            case CurrencyType.NC:
                                builder.Append(kvp.Value == 1 ? I2.Loc.ScriptLocalization.NanoBot : I2.Loc.ScriptLocalization.NanoBots);
                                break;
                            case CurrencyType.QP:
                                builder.Append(kvp.Value == 1 ? I2.Loc.ScriptLocalization.Qualifier_Point : I2.Loc.ScriptLocalization.Qualifier_Points);
                                break;
                        }
                        builder.Append(END);
                        first = false;
                    }
                }
                if (Response.ItemResults != null)
                {
                    // need to gauge the size of the grant.
                    int avatars = Response.ItemResults.Count(item => item.ItemClass == NBEconomy.ITEMCLASS_AVATAR);
                    int cardbacks = Response.ItemResults.Count(item => item.ItemClass == NBEconomy.ITEMCLASS_CARDBACK);
                    int cards = Response.ItemResults.Count(item => item.ItemClass == NBEconomy.ITEMCLASS_CARD);

                    if(cards > 0)
                    {
                        if(!first)
                            builder.Append(',').Append(' ');
                        builder.Append(BEGIN).Append(cards).Append(' ').Append(cards == 1 ? I2.Loc.ScriptLocalization.Card : I2.Loc.ScriptLocalization.Cards).Append(END);
                        first = false;
                    }
                    if (cardbacks > 0)
                    {
                        if (!first)
                            builder.Append(',').Append(' ');
                        builder.Append(BEGIN).Append(cardbacks).Append(' ').Append(cardbacks == 1 ? I2.Loc.ScriptLocalization.Card_Back : I2.Loc.ScriptLocalization.Card_Backs).Append(END);
                        first = false;
                    }
                    if (avatars > 0)
                    {
                        if (!first)
                            builder.Append(',').Append(' ');
                        builder.Append(BEGIN).Append(avatars).Append(' ').Append(avatars == 1 ? I2.Loc.ScriptLocalization.Avatar : I2.Loc.ScriptLocalization.Avatars).Append(END);
                        first = false;
                    }
                    foreach(var item in Response.ItemResults)
                    {
                        PlayerProfile.Inventory[item.ItemId] = item;

                        switch (item.ItemClass)
                        {
                            case NBEconomy.ITEMCLASS_AVATAR:
                                PlayerProfile.OwnedAvatars.Add(item.ItemId);
                                break;
                            case NBEconomy.ITEMCLASS_CARD:
                                int cardId;
                                if (int.TryParse(item.ItemId, out cardId))
                                {
                                    int previousOwned = 0;
                                    PlayerProfile.OwnedCards.TryGetValue(cardId, out previousOwned);
                                    PlayerProfile.OwnedCards[cardId] = previousOwned + 1;
                                }
                                break;
                            case NBEconomy.ITEMCLASS_CARDBACK:
                                PlayerProfile.OwnedCardBacks.Add(item.ItemId);
                                break;
                        }
                    }
                }
                
                this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams
                {
                    Title = I2.Loc.ScriptLocalization.Coupon_Redeemed,
                    Message = builder.ToString()
                });
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Inventory);
            }
        }
    }
}
