﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using Messages;
using System.Collections.Generic;
using NovaBlitz.Game;
using LinqTools;

namespace NovaBlitz.UI
{
	public class RemoveFriendSignal : Signal<string> {} // friendPlayfabId
	
	public class RemoveFriendResponseSignal : Signal<string, bool>{} // friendPlayfabId, success / failed
    public class RemoveFriendCommand : Command
    {
        // Signal Parameters
        [Inject]
        public string PlayFabIdToDecline { get; set; }

        // Injected Dependencies
        [Inject] public DeclineFriendResponseSignal DeclineFriendResponseSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientService ClientService { get; set; }

        public override void Execute()
        {
            if (this.PlayFabIdToDecline == this.PlayerProfile.MetaProfile.PlayFabID)
            {
                Debug.LogError("#FRIENDS# Attempted to Remove yourself");
                //  Don't allow removing yourself to your friends list :)
                this.DeclineFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
                return;
            }

            //NovaBlitzFriend friend;

            /*if (this.PlayerProfile.FriendListData.TryGetValue(this.PlayFabIdToDecline, out friend))
            {
                // Get rid of the friend right now.
                //this.PlayerProfile.FriendListData.Remove(friend);
                this.DeclineFriendResponseSignal.Dispatch(friend.FriendInfo.PlayFabId, true);
            }*/

            Debug.LogError("#FRIENDS# Remove Friend request sent");
            this.ClientService.SendMessage(new ManageFriendRequest { FriendPlayFabId = PlayFabIdToDecline, RequestType = RequestType.RemoveRequest });

        }
    }
}
