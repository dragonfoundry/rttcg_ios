using UnityEngine;

using System;
using System.Text.RegularExpressions;
using LinqTools;
using System.Collections.Generic;

using strange.extensions.command.impl;
using strange.extensions.signal.impl;

using PlayFab.ClientModels;
using PlayFab;

using NovaBlitz.Game;
using NovaBlitz.UI.DataContracts;
using Messages;
using Newtonsoft.Json;

namespace NovaBlitz.UI
{

	public class LoadTitleDataSignal : Signal {}
	public class LoadTitleDataCommand : Command
	{		
		// Injected Dependencies
		[Inject] public GameData GameData {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public ClientService ClientService { get; set; }
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
        [Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}

        [Inject] public LoadPlayerSettingsSignal LoadPlayerSettingsSignal { get; set; }


        private const string testDnsKey = "test_server_dns";
        private const string dnsKey = "server_dns";
        private const string betaDnsKey = "beta_server_dns";
        private const string versionKey = "min_client_version";
		private const string maintenanceKey = "maintenance_status";
		//private const string mobileBetaKey = "mobile_beta_version"; 
		private const string mobileTestKey = "mobile_test_version";
        
        public override void Execute()
		{
			this.Retain();
            this.AsyncProgressSignal.Dispatch(AsyncProgress.TitleData, 0.4f);

            // Get the title data from PlayFab
            GetTitleDataRequest req = new GetTitleDataRequest();
            req.Keys = new List<string>()
            {
                testDnsKey,
                betaDnsKey,
                dnsKey, 
				versionKey,
                maintenanceKey,
				//mobileBetaKey,
				mobileTestKey,
            };

            PlayFabClientAPI.GetTitleData(req, OnGetTitleData, OnPlayFabError);
            this.LoadPlayerSettingsSignal.Dispatch();
        }

        private void OnGetTitleData(GetTitleDataResult result)
        {
            this.AsyncProgressSignal.Dispatch(AsyncProgress.TitleData, 0.5f);
            var titleDataResult = result;
            
            if(titleDataResult == null || titleDataResult.Data == null)
            {
                Debug.LogError("Title Data not found or not readable: data is null");
                TitleDataParseFailed(DisconnectCode.DataParseFailed);
                return;
            }
            try
            {
                var data = titleDataResult.Data;

                string dnsJSON;
                data.TryGetValue(dnsKey, out dnsJSON);
                string testDnsJSON;
                data.TryGetValue(testDnsKey, out testDnsJSON);
                string betaDnsJSON;
                data.TryGetValue(betaDnsKey, out betaDnsJSON);

                string maintenanceStatus;

				string versionString;
				//string mobileBetaString;
				string mobileTestString;
				data.TryGetValue(versionKey, out versionString);
				//data.TryGetValue(mobileBetaKey, out mobileBetaString);
				data.TryGetValue(mobileTestKey, out mobileTestString);
                data.TryGetValue(maintenanceKey, out maintenanceStatus);
                int major = int.MaxValue;
                int minor = int.MaxValue;
                int patch = int.MaxValue;
                try
                {
                    var matches = Regex.Matches(versionString, "[0-9]+");
                    major = int.Parse(matches[0].Value);
                    minor = int.Parse(matches[1].Value);
                    patch = int.Parse(matches[2].Value);
                }
                catch
                {
                    Debug.LogErrorFormat("Version parse failed for: {0}", versionString);
                }
                Debug.LogFormat("Playfab Version: {0}.{1}.{2}. ClientVersion: {3}. Maintenance: {4}", major, minor, patch, this.GameData.VersionLabel, maintenanceStatus);

                if (maintenanceStatus == "down")
                {
                    Debug.Log("Server is down for maintenance");
                    TitleDataParseFailed(DisconnectCode.ServerDownForMaintenance);
                    return;
                }

                if (major > this.GameData.VersionMajor
                    || (major == this.GameData.VersionMajor && minor > this.GameData.VersionMinor)
                    || (major == this.GameData.VersionMajor && minor == this.GameData.VersionMinor && patch > this.GameData.VersionPatch))
                {
                    Debug.LogError("Client is out of Date");
                    TitleDataParseFailed(DisconnectCode.ClientOutOfDate);
                    return;
                }

                //Debug.LogFormat("{0} Live: {1} Beta: {2} Test: {3}", PlayFabSettings.TitleId, dnsJSON, betaDnsJSON, testDnsJSON);

                string dnsString;
#if UNITY_STEAM && !UNITY_EDITOR
                string branchName;
                Steamworks.SteamApps.GetCurrentBetaName(out branchName, 16);
                Debug.Log("Branch:" + branchName + ".");
                switch(branchName)
                {
					case "staging":
						Debug.LogWarningFormat("Staging Test version. Min:{0}. Current{1}", branchName, GameData.VersionLabel);
                        dnsString = testDnsJSON;
                        break;
					case "beta":
						Debug.LogWarningFormat("Beta version. Min:{0}. Current{1}", branchName, GameData.VersionLabel);
                        dnsString = betaDnsJSON;
                        break;
                    default:
						Debug.LogWarningFormat("Release version. Min:{0}. Current{1}", branchName, GameData.VersionLabel);
                        dnsString = dnsJSON;
                        break;
                }
#else
                if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
				{
					try
					{
						var testMatches = Regex.Matches(mobileTestString, "[0-9]+");
						var tmj = int.Parse(testMatches[0].Value);
						var tmi = int.Parse(testMatches[1].Value);
						var tpa = int.Parse(testMatches[2].Value);


						/*var betaMatches = Regex.Matches(mobileBetaString, "[0-9]+");
						var bmj = int.Parse(betaMatches[0].Value);
						var bmi = int.Parse(betaMatches[1].Value);
						var bpa = int.Parse(betaMatches[2].Value);*/

						/*if(tmj < GameData.VersionMajor
							|| (tmj == GameData.VersionMajor && tmi < GameData.VersionMinor)
							|| (tmj == GameData.VersionMajor && tmi == GameData.VersionMinor && tpa <= GameData.VersionPatch))*/
                        if(tmj == GameData.VersionMajor && tmi == GameData.VersionMinor && tpa <= GameData.VersionPatch)
						{
							Debug.LogWarningFormat("Mobile Test version. Min:{0}. Current{1}", mobileTestString, GameData.VersionLabel);
							dnsString = testDnsJSON;
						}
						/*else if(bmj < GameData.VersionMajor
							|| (bmj == GameData.VersionMajor && bmi < GameData.VersionMinor)
							|| (bmj == GameData.VersionMajor && bmi == GameData.VersionMinor && bpa <= GameData.VersionPatch))
						{
							Debug.LogWarningFormat("Mobile Beta version. Min:{0}. Current{1}", mobileBetaString, GameData.VersionNumber);
							dnsString = betaDnsJSON;
						}*/
						else
						{
							dnsString = dnsJSON;
						}
					}
					catch
					{
						Debug.LogErrorFormat("Mobile beta/test parse failed for: {0}", versionString);
						dnsString = dnsJSON;
					}
				}
				else
                	dnsString = dnsJSON;
                /*dnsString = (PlayFabSettings.TitleId == "721F"
                    && ClientPrefs.UserAccountInfo != null && ClientPrefs.UserAccountInfo.PlayFabId == "9A7AE96546CEE8C4")
                    && !string.IsNullOrEmpty(testDnsJSON)
                    ? testDnsJSON
                    : dnsJSON;*/
#endif
                ProcessDNSrecords(dnsString);

                this.AsyncProgressSignal.Dispatch(AsyncProgress.TitleData, 1.0f);
            }
            catch (Exception e)
            {
                Debug.LogError("Title Data Exception " + e.ToString());
                TitleDataParseFailed(DisconnectCode.DataParseFailed);
                return;
            }
            this.Release();
        }
        
		private bool CompareVersionString(string versionString)
		{
			return false;
		}


		private void ProcessDNSrecords(string dnsDataJson)
		{
            Dictionary<string, List<DNSDataContract>> matchmakers = JsonConvert.DeserializeObject<Dictionary<string, List<DNSDataContract>>>(dnsDataJson);

            ClientPrefs.DNSoptions = matchmakers.Values.FirstOrDefault();
			Debug.LogFormat ("{0} servers, including: {1}:{2}", ClientPrefs.DNSoptions.Count, ClientPrefs.DNSoptions[0].Hostname, ClientPrefs.DNSoptions[0].Port);
		}

        private void OnPlayFabError(PlayFabError pfe)
        {

            Debug.LogError("Playfab error: " + pfe.Error + ": " + pfe.ErrorMessage);
            TitleDataParseFailed(DisconnectCode.DataParseFailed);
            this.Release();
            return;
        }

        private void TitleDataParseFailed(DisconnectCode code)
        {
            this.ClientService.ForceDisconnect(code);
            this.Fail();
            this.Release();
        }
    }
}
