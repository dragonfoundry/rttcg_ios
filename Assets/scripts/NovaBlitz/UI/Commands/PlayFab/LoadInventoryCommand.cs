using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using LinqTools;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
	public class LoadInventorySignal : Signal {}
	public class LoadInventoryCommand : Command
	{
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public GameData GameData {get;set;}

		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		[Inject] public InventoryLoadedSignal InventoryLoadedSignal {get;set;}
		
		public override void Execute()
        {
            Debug.LogFormat("LoadInventoryCommand: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Retain();
			GetUserInventoryRequest req = new GetUserInventoryRequest();
			PlayFabClientAPI.GetUserInventory(req, OnGetUserInventoryResult, OnPlayFabError);
		}
		
		void OnGetUserInventoryResult(GetUserInventoryResult result)
		{
			foreach (var kvp in result.VirtualCurrency) {
				var code = CurrencyUtils.GetCode(kvp.Key);
				this.PlayerProfile.Wallet.Currencies[code] = kvp.Value;
                Debug.LogFormat("Redeem Coupon: change for {0} to {1} by {2}", code, PlayerProfile.Wallet.Currencies[code], kvp.Value);
            }

            Dictionary<string, string> instanceIDs = new Dictionary<string, string>();
			Dictionary<int, int> cards = new Dictionary<int, int>();
			HashSet<string> keys = new HashSet<string>();
            HashSet<string> avatars = new HashSet<string>();
            HashSet<string> cardBacks = new HashSet<string>();
            HashSet<string> playmats = new HashSet<string>();
            HashSet<string> ownedItems = new HashSet<string>();

			foreach (ItemInstance item in result.Inventory)
			{
                ownedItems.Add(item.ItemId);
				switch (item.ItemClass)
				{
				case NBEconomy.ITEMCLASS_CARD:
					// This property works regardless of IsStackable.
					int amount = (int)(item.RemainingUses ?? 1);
                        int cardId;
                        int.TryParse(item.ItemId, out cardId);
                    // Increase quantity.
                    if (cards.ContainsKey(cardId))
                        cards[cardId] += amount;
                    else
                        cards.Add(cardId, amount);
                    // Track the instance ID for crafting - always update, even if there's a value stored. 
                    //(should only be one, but there's a chance a player's inventory could go to 0 and then back up)
                    instanceIDs[item.ItemId] = item.ItemInstanceId;
					break; 
				case NBEconomy.ITEMCLASS_KEY:
					keys.Add(item.ItemId);
                    break;
                case NBEconomy.ITEMCLASS_AVATAR:
                    avatars.Add(item.ItemId);
                    break;
                case NBEconomy.ITEMCLASS_CARDBACK:
                    cardBacks.Add(item.ItemId);
                    break;
                case NBEconomy.ITEMCLASS_PLAYMAT:
                    playmats.Add(item.ItemId);
                    break;
                }
			}
			this.PlayerProfile.OwnedCards = cards;
			this.PlayerProfile.OwnedKeys = keys;
            this.PlayerProfile.OwnedAvatars = avatars;
            this.PlayerProfile.OwnedCardBacks = cardBacks;
            this.PlayerProfile.OwnedPlaymats = playmats;

            this.ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);
            this.InventoryLoadedSignal.Dispatch();
			this.Release();
        }
		
		void OnPlayFabError(PlayFabError pfe) 
		{
			this.Release();
		}
	}
}
