﻿using UnityEngine;
using System.Collections;
using NovaBlitz.Game;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
    public class UnlinkDeviceOrSteamSignal : Signal { }
    public class DeviceOrSteamUnlinkedSignal : Signal<bool> { }
    public class UnlinkDeviceOrSteamCommand : Command
    {
        // Injected Dependencies
        [Inject] public MetagameSettings MetagameSettings { get; set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public DeviceOrSteamUnlinkedSignal DeviceOrSteamUnlinkedSignal { get; set; }
        [Inject] public ClientService ClientService { get; set; }
#if UNITY_STEAM
        [Inject] public SteamAuthTicketSignal SteamAuthTicketSignal { get; set; }
        [Inject] public GetSteamAuthTokenSignal GetSteamAuthTokenSignal { get; set; }
#endif

        /// <summary>
        /// Call the playfab login function for the appropriate device
        /// </summary>
        public override void Execute()
        {
            this.Retain();
            //#if TEST_BUILD
#if UNITY_STEAM
            this.SteamAuthTicketSignal.AddListener(OnSteamAuthTicket);
            this.GetSteamAuthTokenSignal.Dispatch();
#elif (UNITY_IOS)
			Debug.Log("Unlinking IOS device ID from account: " + ClientPrefs.DeviceUniqueIdentifier);
            UnlinkIOSDeviceIDRequest req = new UnlinkIOSDeviceIDRequest();
            req.DeviceId = ClientPrefs.DeviceUniqueIdentifier;
            PlayFabClientAPI.UnlinkIOSDeviceID(req, OnUnlinkIOSSuccess, OnUnlinkFailure);
#elif (UNITY_ANDROID)
            string uniqueId = ClientPrefs.DeviceUniqueIdentifier;
            Debug.Log("Unlinking ANDROID device ID from account: " + uniqueId);
            UnlinkAndroidDeviceIDRequest android = new UnlinkAndroidDeviceIDRequest();
            android.AndroidDeviceId = uniqueId;
            PlayFabClientAPI.UnlinkAndroidDeviceID(android, OnUnlinkAndroidSuccess, OnUnlinkFailure);
#else
            Debug.Log("Unlinking Custom ID to account");
            UnlinkCustomIDRequest custom = new UnlinkCustomIDRequest();
            custom.CustomId = ClientPrefs.DeviceUniqueIdentifier;
            PlayFabClientAPI.UnlinkCustomID(custom, OnUnlinkCustomSuccess, OnUnlinkFailure);
#endif
        }

#if UNITY_STEAM
        void OnSteamAuthTicket(string authStr)
        {
            this.SteamAuthTicketSignal.RemoveListener(OnSteamAuthTicket);
            if (string.IsNullOrEmpty(authStr))
            {
                Debug.LogError("Steam ticket is null - auth failed");
                this.Fail();
                this.Release();
            }
            else
            {
                Debug.Log("Unlinking STEAM ID from account (note that we don't use the auth ticket we just got, but if you can't get an auth ticket, you shouldn't be unlinking...");
                UnlinkSteamAccountRequest steam = new UnlinkSteamAccountRequest();
                PlayFabClientAPI.UnlinkSteamAccount(steam, OnUnlinkSteamSuccess, OnUnlinkFailure);
            }
        }
#endif

        // On success, continue the flow
        void OnUnlinkIOSSuccess(UnlinkIOSDeviceIDResult result)
        {
			Debug.Log("IOS Device Unlinked: " + ClientPrefs.DeviceUniqueIdentifier);
            DeviceOrSteamUnlinkedSignal.Dispatch(true);
            this.Release();
        }

        // On success, continue the flow
        void OnUnlinkSteamSuccess(UnlinkSteamAccountResult result)
        {
			Debug.Log("Steam Account Unlinked: " + ClientPrefs.DeviceUniqueIdentifier);
            DeviceOrSteamUnlinkedSignal.Dispatch(true);
            this.Release();
        }

        // On success, continue the flow
        void OnUnlinkAndroidSuccess(UnlinkAndroidDeviceIDResult result)
        {
            Debug.Log("Android Account Unlinked ");
            //PlayerPrefs.SetString(NovaConfig.KEY_ANDROID_ID, null);
            DeviceOrSteamUnlinkedSignal.Dispatch(true);
            this.Release();
        }

        // On success, continue the flow
        void OnUnlinkCustomSuccess(UnlinkCustomIDResult result)
        {
            Debug.Log("Custom Account Unlinked ");
            DeviceOrSteamUnlinkedSignal.Dispatch(true);
            this.Release();
        }


        // On failure, just handle the standard errors (
        void OnUnlinkFailure(PlayFabError pfe)
        {
            if (pfe.Error == PlayFabErrorCode.AccountNotLinked)
            {
                Debug.Log("Account wasn't linked in the first place.");
            }
            else
            {
                Debug.Log("Unlinking Device or Steam ID failed");
            }
            DeviceOrSteamUnlinkedSignal.Dispatch(false);
            this.Fail();
            this.Release();
        }
    }
}
