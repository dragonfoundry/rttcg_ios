﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
	public class AddUsernameAndPasswordSignal : Signal<UserCredentials> {}
    public class UsernameAndPasswordAddedSignal : Signal { }

    public class AddUsernameAndPasswordCommand : Command
	{
		// Signal Parameters
		[Inject] public UserCredentials UserCredentials {get;set;}

		// Dependency Injection
		[Inject] public UsernameAndPasswordAddedSignal UsernameAndPasswordAddedSignal { get;set;}
		[Inject] public UsernameNotAvailable UsernameNotAvailable {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public AddDisplayNameSignal AddDisplayNameSignal { get; set; }


		public override void Execute ()
		{
            this.Retain();
            Debug.LogFormat("Adding credentials to account {2} on playfab with {0}/{1}", this.UserCredentials.Username, "*Redacted*", this.PlayerProfile.MetaProfile.PlayFabID);
            AddUsernamePasswordRequest req = new AddUsernamePasswordRequest();
            req.Username = this.UserCredentials.Username;
            req.Password = this.UserCredentials.Password;
            req.Email = (this.PlayerProfile.isAgeGatePassed == true) ? this.UserCredentials.Email : StringUtils.HashEmail(this.UserCredentials.Email);
            if (this.PlayerProfile.isAgeGatePassed != true)
            {
                req.Username = this.PlayerProfile.MetaProfile.PlayFabID;
            }
            PlayFabClientAPI.AddUsernamePassword(req, OnAccountDetailsAdded, OnLoginFailure);
            
		}

        void OnAccountDetailsAdded(AddUsernamePasswordResult result)
        {
            Debug.Log("Added username and password to user");
            if (this.PlayerProfile.LoginInfoResult == null)
                this.PlayerProfile.LoginInfoResult = new GetPlayerCombinedInfoResultPayload();
            if (this.PlayerProfile.LoginInfoResult.AccountInfo == null)
                this.PlayerProfile.LoginInfoResult.AccountInfo = new UserAccountInfo();
            if (this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo == null)
                this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo = new UserPrivateAccountInfo();
            if (this.PlayerProfile.LoginInfoResult.AccountInfo.TitleInfo == null)
                this.PlayerProfile.LoginInfoResult.AccountInfo.TitleInfo = new UserTitleInfo();

            this.PlayerProfile.MetaProfile.Name = result.Username;
            this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo.Email = (result.Request as AddUsernamePasswordRequest).Email;
            this.PlayerProfile.LoginInfoResult.AccountInfo.Username = result.Username;
            this.PlayerProfile.LoginInfoResult.AccountInfo.TitleInfo.DisplayName = result.Username;
            this.UsernameAndPasswordAddedSignal.Dispatch();
            this.Release();
            if (result.Username == null)
            {
                this.AddDisplayNameSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID);
            }
            else
            {
                this.AddDisplayNameSignal.Dispatch(result.Username);
            }
        }

        void OnLoginFailure(PlayFabError pfe) 
		{
			Debug.LogError("got register error " + pfe.Error + ": " + pfe.ErrorMessage);
			
			if(pfe.Error == PlayFabErrorCode.UsernameNotAvailable 
                || pfe.Error == PlayFabErrorCode.InvalidUsername 
                || pfe.Error == PlayFabErrorCode.InvalidUsernameOrPassword 
                || pfe.Error == PlayFabErrorCode.DuplicateUsername)
			{
				this.UsernameNotAvailable.Dispatch(this.UserCredentials.Username);
			}
			
			this.Fail();
            this.Release();
        }
	}
}

