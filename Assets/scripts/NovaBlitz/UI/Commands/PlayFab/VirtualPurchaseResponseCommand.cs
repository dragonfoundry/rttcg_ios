﻿using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using Messages;
using UnityEngine;

namespace NovaBlitz.UI
{
    public class VirtualPurchaseResponseSignal : Signal<VirtualPurchaseResponse> { }

    public class VirtualPurchaseResponseCommand : Command
    {
        [Inject] public VirtualPurchaseResponse Response { get; set; }

        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GameData GameData { get; set; }
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
		[Inject] public LogEventSignal LogEventSignal {get;set;}
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }
        
        public override void Execute()
        {
            if (Response.Error != ErrorCode.NoError)
            {
                int balance;
                this.PlayerProfile.Wallet.Currencies.TryGetValue(Response.Currency, out balance);
                var error = string.Format("Insufficient funds error curr:{0} expectedPrice:{1} knownBalance:{2}", Response.Currency, Response.ActualPrice, balance);
                MetricsTrackingService.SavedException = error;
                Debug.Log(error);
                return;
            }
            else
            {
                NovaInventoryItem novaItem = null;
                if (Response.ContainedItems == null || Response.ContainedItems.Count == 0)
                {
                    if (PlayerProfile.Inventory.TryGetValue(Response.ItemId, out novaItem))
                    {
                        novaItem.RemainingUses++;
                    }
                    else
                    {
                        novaItem = new NovaInventoryItem { ItemId = Response.ItemId, RemainingUses = 1, };
                        PlayerProfile.Inventory.Add(Response.ItemId, novaItem);
                    }
                }
                else
                {
                    foreach (var item in Response.ContainedItems)
                    {
                        PlayerProfile.Inventory[item.ItemId] = item;
                    }
                }

                int balance;
                int cardId;
                CardData cardData;
                if (PlayerProfile.Wallet.Currencies.TryGetValue(Response.Currency, out balance))
                {
                    var newBalance = Math.Max(0, balance - Response.ActualPrice);
                    PlayerProfile.Wallet.Currencies[Response.Currency] = newBalance;
                    Debug.LogFormat("Spent {0}{1} on {2}. Balance: {3} from {4}", Response.ActualPrice, Response.Currency, Response.ItemId, newBalance, balance);

                    this.LogEventSignal.Dispatch(new LogEventParams
                    {
                        EventName = LogEventParams.EVENT_VIRTUAL_PURCHASE,
                        CurrencyType = Response.Currency,
                        CurrencyBalance = newBalance,
                        EventValue = Response.ActualPrice,
                        ItemId = Response.ItemId
                    });
                }

                if (GameData.AvatarByItemIdDictionary.ContainsKey(Response.ItemId))
                {
                    if (novaItem != null)
                        novaItem.ItemClass = NBEconomy.ITEMCLASS_AVATAR;
                    if (PlayerProfile.OwnedAvatars == null)
                        PlayerProfile.OwnedAvatars = new HashSet<string>();
                    PlayerProfile.OwnedAvatars.Add(Response.ItemId);
                }
                else if (GameData.CardBackByItemIdDictionary.ContainsKey(Response.ItemId))
                {
                    if (novaItem != null)
                        novaItem.ItemClass = NBEconomy.ITEMCLASS_CARDBACK;
                    if (PlayerProfile.OwnedCardBacks == null)
                        PlayerProfile.OwnedCardBacks = new HashSet<string>();
                    PlayerProfile.OwnedCardBacks.Add(Response.ItemId);
                }
                // Add other items here!
                else if (int.TryParse(Response.ItemId, out cardId) && GameData.CardDictionary.TryGetValue(cardId, out cardData))
                {
                    int owned;
                    if (PlayerProfile.OwnedCards == null)
                        PlayerProfile.OwnedCards = new Dictionary<int, int>();
                    PlayerProfile.OwnedCards.TryGetValue(cardId, out owned);
                    PlayerProfile.OwnedCards[cardId] = owned + 1;
                }


                if (Response.ContainedCurrencies != null)
                {
                    foreach (var kvp in Response.ContainedCurrencies)
                    {
                        int currentValue = 0;
                        this.PlayerProfile.Wallet.Currencies.TryGetValue(kvp.Key, out currentValue);
                        var newBalance = Math.Max(0, currentValue + (int)kvp.Value);
                        PlayerProfile.Wallet.Currencies[kvp.Key] = newBalance;
                        Debug.LogFormat("Gained {0}{1}. Balance: {2} from {3}", kvp.Value, kvp.Key, newBalance, currentValue);

                        this.LogEventSignal.Dispatch(new LogEventParams
                        {
                            EventName = LogEventParams.EVENT_CURRENCY_CHANGE,
                            CurrencyType = kvp.Key,
                            CurrencyBalance = newBalance,
                            EventValue = (int)kvp.Value
                        });
                    }
                }

                this.MetricsTrackingService.TrackVirtualPurchase(Response.Currency, Response.ActualPrice, Response.ItemId);

                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Inventory);
            }
        }
    }
}
