﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using System.Collections.Generic;
using LinqTools;
using NovaBlitz.UI.DataContracts;
using Messages;

namespace NovaBlitz.UI
{
    public class LoadFriendsListSignal : Signal { }
    public class FriendsListLoadedSignal : Signal { }
    public class LoadFriendsListCommand : Command
    {
        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public FriendsListLoadedSignal FriendsListLoadedSignal { get; set; }
        [Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
		[Inject] public ConnectToPhotonChatSignal ConnectToPhotonChatSignal {get;set;}
        [Inject] public LoadChatHistorySignal LoadChatHistorySignal { get; set; }

        private const int FriendsToGet = 100;
        private int StartPosition { get; set; }

        public override void Execute()
        {
            this.Retain();
            this.PlayerProfile.IsFriendsListInitialized = false;
            
            foreach(var friend in this.PlayerProfile.FriendListData)
            {
                friend.Value.IsBlocked = this.PlayerProfile.BlockList.Contains(friend.Key);
            }

            if (this.PlayerProfile.FriendListData.Count > 0)
                GetFriendLeaderboard(0);
            else
                InitializeFriends();
            // loads the chat in parallel. Initialization waits for chat to be loaded.
            this.LoadChatHistorySignal.Dispatch();
        }

        private void GetFriendLeaderboard(int startPosition)
        {
            GetFriendLeaderboardRequest req = new GetFriendLeaderboardRequest() { StartPosition = this.StartPosition, MaxResultsCount = FriendsToGet, StatisticName = "selected_avatar" };
            PlayFabClientAPI.GetFriendLeaderboard(req, OnGetFriendLeaderboard, OnPlayfabError);

        }

        void OnGetFriendLeaderboard(GetLeaderboardResult result)
        {
            if(result.Leaderboard != null)
            {
                foreach(var res in result.Leaderboard)
                {
                    NovaBlitzFriend friend;
                    if(this.PlayerProfile.FriendListData.TryGetValue(res.PlayFabId, out friend))
                    {
                        friend.AvatarId = res.StatValue;
                        AvatarDataContract avatar;
                        if (GameData.AvatarDictionary.TryGetValue(res.StatValue, out avatar) && !string.IsNullOrEmpty(avatar.ArtId))
                            friend.AvatarArtId = avatar.ArtId;
                    }
                }
                if(result.Leaderboard.Count == FriendsToGet)
                {
                    StartPosition = StartPosition + FriendsToGet;
                    GetFriendLeaderboard(StartPosition);
                }
                else
                    InitializeFriends();
            }
            else
                InitializeFriends();
        }

        void InitializeFriends()
        {
            Timer.Instance.StartCoroutine(InitializeFriendsCoroutine());
        }

        IEnumerator InitializeFriendsCoroutine()
        {
            FriendListDockView friendsList = this.ViewReferences.Get(typeof(FriendListDockView)) as FriendListDockView;
            MainHeaderView mainHeader = this.ViewReferences.Get(typeof(MainHeaderView)) as MainHeaderView;
            int tracker = 0;
            while (friendsList == null || mainHeader == null || !this.PlayerProfile.isChatLogLoaded)
            {
                yield return new WaitForSecondsRealtime(0.1f);
                tracker += 100;
                friendsList = this.ViewReferences.Get(typeof(FriendListDockView)) as FriendListDockView;
                mainHeader = this.ViewReferences.Get(typeof(MainHeaderView)) as MainHeaderView;
            }
            yield return null;
            Debug.LogFormat("Initializing Friends. Waited {0}ms", tracker);
            if (friendsList != null)
            {
                friendsList.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed, false);
                friendsList.transform.SetAsLastSibling();
            }
            this.PlayerProfile.IsFriendsListInitialized = true;
            this.FriendsListLoadedSignal.Dispatch();
            //this.AsyncProgressSignal.Dispatch(AsyncProgress.FriendsList, 1.0f);

            // Only connect to Photon if the user has friends.
            if (PlayerProfile.FriendListData != null && PlayerProfile.FriendListData.Count > 0)
                this.ConnectToPhotonChatSignal.Dispatch(string.Empty, new ChatMessage(),false);

            this.Release();
        }

        void OnPlayfabError(PlayFabError pfe)
        {
            Debug.LogError(pfe.ErrorMessage);
            InitializeFriends();
            this.Fail();
            this.Release();
        }

    }
}