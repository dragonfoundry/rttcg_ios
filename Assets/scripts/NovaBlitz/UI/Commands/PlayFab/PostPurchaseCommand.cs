using System.Collections.Generic;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using UnityEngine;
using Messages;

namespace NovaBlitz.UI
{
	public class PostPurchaseSignal : Signal<ItemInstance[]> {}
	public class PostPurchaseCommand : Command
	{
        [Inject] public ItemInstance[] SignalItems { get; set; }

		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public GameData GameData {get;set;}

		[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}
		[Inject] public DialogDismissedSignal DialogDismissedSignal {get;set;}

		public override void Execute()
		{

            var cards = this.SignalItems.Where(x => x.ItemClass == NBEconomy.ITEMCLASS_CARD);
            List<string> cardIDs = cards.Select(x => x.ItemId).ToList();
            string packID = string.Empty;

            foreach (var inst in this.SignalItems)
            {
                NovaInventoryItem item;
                if (PlayerProfile.Inventory.TryGetValue(inst.ItemId, out item))
                {
                    item.RemainingUses++;
                }
                else
                {
                    item = new NovaInventoryItem { ItemClass = inst.ItemClass, ItemId = inst.ItemInstanceId, RemainingUses = inst.RemainingUses, Expiration = inst.Expiration, CustomData = inst.CustomData, ItemInstanceId = inst.ItemInstanceId  };
                    PlayerProfile.Inventory.Add(inst.ItemId, item);
                }
                if(inst.ItemClass == NBEconomy.ITEMCLASS_BOOSTER && cards.Count() > 0)
                {
                    // This was an openable booster - show the pack opener
                    packID = inst.ItemId;
                }
                else if(cardIDs.Contains(inst.ItemId) && inst.UsesIncrementedBy > 1) // If there's a duplicate card, it won't show up as a duplicate cardID, because cards are Stackable.
                {
                    for (int i = 1; i < inst.UsesIncrementedBy; i++)
                    {
                        cardIDs.Add(inst.ItemId);
                        Debug.LogError("Added duplicate Card to pack: " + inst.ItemId);
                    }
                }
                else if(inst.ItemClass == NBEconomy.ITEMCLASS_AVATAR)
                {
                    if (PlayerProfile.OwnedAvatars == null)
                        PlayerProfile.OwnedAvatars = new HashSet<string>();
                    PlayerProfile.OwnedAvatars.Add(inst.ItemId);
                }
                else if (inst.ItemClass == NBEconomy.ITEMCLASS_PLAYMAT)
                {
                    if (PlayerProfile.OwnedPlaymats == null)
                        PlayerProfile.OwnedPlaymats = new HashSet<string>();
                    PlayerProfile.OwnedPlaymats.Add(inst.ItemId);
                }
                else if (inst.ItemClass == NBEconomy.ITEMCLASS_CARDBACK)
                {
                    if (PlayerProfile.OwnedCardBacks == null)
                        PlayerProfile.OwnedCardBacks = new HashSet<string>();
                    PlayerProfile.OwnedCardBacks.Add(inst.ItemId);
                }
                else if (inst.ItemClass == NBEconomy.ITEMCLASS_STORE_ITEM)
                {
                    if (PlayerProfile.BoughtStoreItems == null)
                        PlayerProfile.BoughtStoreItems = new HashSet<string>();
                    PlayerProfile.BoughtStoreItems.Add(inst.ItemId);
                    if(inst.ItemId == NovaIAPService.ANNUITY_CREDIT)
                    {
                        PlayerProfile.CreditAnnuityExpiration = inst.Expiration;
                    }
                    else if (inst.ItemId == NovaIAPService.ANNUITY_NANOBOT)
                    {
                        PlayerProfile.NanoBotAnnuityExpiration = inst.Expiration;
                    }
                    else if (inst.ItemId == NovaIAPService.ANNUITY_SMARTPACK)
                    {
                        PlayerProfile.SmartPackAnnuityExpiration = inst.Expiration;
					}
					else if (inst.ItemId == NovaIAPService.ANNUITY_GEM)
					{
						PlayerProfile.GemAnnuityExpiration = inst.Expiration;
					}
                }
                // TODO: Handle other item classes as needed.
            }
		}
	}
}
