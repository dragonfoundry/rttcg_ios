﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using NovaBlitz.Economy;
using PlayFab.ClientModels;
using PlayFab;
using Messages;

namespace NovaBlitz.UI
{
    public class OpenSmartPackParams
    {
        public string StoreId { get; set; }
        public string PackId { get; set; }
        public CurrencyType Currency { get; set; }
        public int ExpectedPrice { get; set; }
    }

    public class OpenSmartPackSignal : Signal<OpenSmartPackParams> { }

    public class OpenSmartPackCommand : Command
    {
        // signal parameters
        [Inject] public OpenSmartPackParams Params { get; set; }

        
        [Inject] public ClientService ClientService { get; set; }
                
        public override void Execute()
        {
            ClientService.SendMessage(new OpenPackRequest {
                StoreId = Params.StoreId,
                PackName = Params.PackId,
                ExpectedPrice = Params.ExpectedPrice,
                PaymentCurrency = Params.Currency });
        }
    }
}