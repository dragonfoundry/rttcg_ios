﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using Newtonsoft.Json;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class LoadAccountInfoSignal : Signal<string> {}
	public class LoadAccountInfoCommand : Command
	{
		// Signal Parampeters
		[Inject] public string SessionTicket {get;set;}
		
		// Dependency Injection
		[Inject] public ClientPrefs ClientPrefs {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public AccountInfoLoadedSignal AccountInfoLoadedSignal {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
		[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal{get;set;}
		[Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
		[Inject] public AddDisplayNameSignal AddDisplayNameSignal { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }


		private IEnumerator timoutCoRoutine;

		public override void Execute ()
		{
			this.Retain();
            GetAccountInfoRequest req = new GetAccountInfoRequest();
			this.timoutCoRoutine = WaitForTimeout();
			Timer.Instance.StartCoroutine(this.timoutCoRoutine);
			PlayFabClientAPI.GetAccountInfo(req, OnGetAccountInfo, OnPlayFabError);

            this.AsyncProgressSignal.Dispatch(AsyncProgress.Authenticating, 0.2f);
		}
        
        void OnGetAccountInfo(GetAccountInfoResult result) 
		{
            Debug.Log("Account info loaded");
			Timer.Instance.StopCoroutine (this.timoutCoRoutine);
			UserAccountInfo info = result.AccountInfo;
			this.ClientPrefs.SessionTicket = this.SessionTicket;
			this.ClientPrefs.UserAccountInfo = info;
			this.ClientPrefs.Save();
			this.PlayerProfile.MetaProfile.ID = info.PlayFabId;
			if (info.TitleInfo != null && !string.IsNullOrEmpty (info.TitleInfo.DisplayName)) {
				this.PlayerProfile.MetaProfile.Name = info.TitleInfo.DisplayName;
			}
			else if(this.PlayerProfile.LoginInfoResult != null 
                && this.PlayerProfile.LoginInfoResult.AccountInfo != null
                && this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo != null
                && !string.IsNullOrEmpty(this.PlayerProfile.LoginInfoResult.AccountInfo.PrivateInfo.Email))
			{
				this.PlayerProfile.MetaProfile.Name = info.PlayFabId;
				this.AddDisplayNameSignal.Dispatch (info.PlayFabId);
			}
			this.PlayerProfile.PlayFabId = info.PlayFabId;
			this.PlayerProfile.MetaProfile.PlayFabSessionToken = this.SessionTicket;

            // Log the session start in the metrics tracking service - need the playfab ID to do this!
            this.MetricsTrackingService.OnSessionStart();

            this.AsyncProgressSignal.Dispatch(AsyncProgress.Authenticating, 1.0f);
			this.AccountInfoLoadedSignal.Dispatch();


            Timer.Instance.StartCoroutine(LoadGameLogs());
            this.Release();
        }

        private IEnumerator LoadGameLogs()
        {
            yield return null;
            this.PlayerProfile.GameLogs.Clear();
            if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.PlayFabID))
            {
                try
                {
                    List<GameLog> gameLogs;
                    var directory = Path.Combine(Application.persistentDataPath, this.PlayerProfile.MetaProfile.PlayFabID);
                    var file = Path.Combine(directory, NovaConfig.GAME_LOG_FILE_NAME);
                    if (Directory.Exists(directory) && File.Exists(file))
                    {
                        string logString = File.ReadAllText(file);
                        gameLogs = JsonConvert.DeserializeObject<List<GameLog>>(logString);
                        if (gameLogs != null)
                        {
                            PlayerProfile.GameLogs = gameLogs;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Game Log Load Exception: {0}", ex.ToString());
                }
            }
        }

        IEnumerator WaitForTimeout()
		{
			yield return new WaitForSecondsRealtime(15f);
			ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.TimedOut, I2.Loc.ScriptLocalization.Error.TimedOutMessage));
			this.Fail();
            this.Release();
        }

		void OnPlayFabError(PlayFabError pfe) 
		{
			Timer.Instance.StopCoroutine (this.timoutCoRoutine);
			Debug.LogError("Playfab error: " + pfe.Error + ": " + pfe.ErrorMessage);
			ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.AccountInfo, I2.Loc.ScriptLocalization.Error.AccountInfoMessage));
			this.Fail();
            this.Release();
        }
	}
}
