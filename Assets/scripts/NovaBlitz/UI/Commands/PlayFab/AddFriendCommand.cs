﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using NovaBlitz.Game;
using PlayFab;
using Messages;
using LinqTools;

namespace NovaBlitz.UI
{
    public class AddFriendSignal : Signal<string,bool>{};
    public class AddFriendResponseSignal : Signal<string, bool>{}; // friend playFabId, is Successfully requested
    public class AddFriendCommand : Command
    {
        // Signal Parameters
        [Inject] public string TitleDisplayName { get; set; }
        [Inject] public bool IsPlayFabId { get; set; }

        // Injected Dependencies
        [Inject] public AddFriendResponseSignal AddFriendResponseSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public ConfirmFriendSignal ConfirmFriendSignal { get; set; }
        
        public override void Execute()
        {
            if (IsPlayFabId)
            {
                DoAddFriend(TitleDisplayName);
            }
            else
            {
                if (!PlayerProfile.MetaProfile.IsNameSet || this.TitleDisplayName == this.PlayerProfile.MetaProfile.Name)
                {
                    Debug.LogError("#FRIENDS# Attempted to Add yourself");
                    //  Don't allow adding yourself to your friends list :)
                    this.AddFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
                    return;
                }

                NovaBlitzFriend friend = this.PlayerProfile.FriendListData.Values.Where(f => f.FriendData.DisplayName == this.TitleDisplayName && !f.FriendData.IsRequest).FirstOrDefault();
                if (friend != null && PlayerProfile.FriendListData.ContainsKey(friend.FriendData.PlayFabId))
                {
                    // We're already friends; just confirm that this was successful?
                    Debug.LogWarning("Already friends!");
                    this.AddFriendResponseSignal.Dispatch(friend.FriendData.PlayFabId, true);
                    return;
                }
                else
                {
                    this.Retain();
                    PlayFab.PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest { TitleDisplayName = this.TitleDisplayName }, OnAccountInfo, OnPlayFabError);
                    // Begin the add friend process
                }
            }
        }

        private void DoAddFriend(string playFabId)
        {
            NovaBlitzFriend friend;
            if (PlayerProfile.FriendListData.TryGetValue(playFabId, out friend))
            {
                if (friend.FriendData.IsRequest)
                {
                    this.ConfirmFriendSignal.Dispatch(playFabId);
                }
                else
                    this.AddFriendResponseSignal.Dispatch(playFabId, false);
            }
            else
            {
                Debug.LogError("#FRIENDS# Add Friend request sent");
                this.ClientService.SendMessage(new ManageFriendRequest { FriendPlayFabId = playFabId, RequestType = RequestType.AddRequest });
            }
        }

        //cccc //get avatar info here
        private void OnAccountInfo(GetAccountInfoResult result)
        {
            if (result == null || result.AccountInfo == null || string.IsNullOrEmpty(result.AccountInfo.PlayFabId) || result.AccountInfo.PlayFabId == PlayerProfile.MetaProfile.PlayFabID)
            {
                this.AddFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
            }
            else
            { 
                DoAddFriend(result.AccountInfo.PlayFabId);
            }
            this.Release();
        }

        private void OnPlayFabError(PlayFabError pfe)
        {
            this.AddFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
            this.Fail();
            this.Release();
        }
    }

}