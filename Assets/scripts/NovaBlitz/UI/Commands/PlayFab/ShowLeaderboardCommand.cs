﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;
using PlayFab.ClientModels;
using PlayFab;
using System.Collections.Generic;
using LinqTools;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
    public class ShowLeaderboardSignal : Signal<string, LeaderboardType> { }
    public class ShowLeaderboardCommand : Command
    {
        [Inject] public string LeaderboardName { get; set; }
        [Inject] public LeaderboardType LeaderboardType { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get;set;}
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get;set;}
        [Inject] public ViewReferences ViewReferences { get; set; }
		[Inject] public INovaContext ContextView {get;set;}

        List<PlayerLeaderboardEntry> OverallLeaderboard { get; set; }
        List<PlayerLeaderboardEntry> FriendLeaderboard { get; set; }
        bool overallCompleted = false;
        bool friendCompleted = false;
        LeaderboardView view;

        public override void Execute()
        {
            view = this.ViewReferences.Get(typeof(LeaderboardView)) as LeaderboardView;
            if (view == null)
                view = this.ViewReferences.Instantiate(typeof(LeaderboardView), this.ContextView.MainCanvas.transform) as LeaderboardView;
            this.Retain();
            GetOverallLeaderboard();
            GetFriendLeaderboard();
        }

        private void GetOverallLeaderboard()
        {
            GetLeaderboardRequest req = new GetLeaderboardRequest() { StartPosition = 0, MaxResultsCount = 50, StatisticName = LeaderboardName };
            PlayFabClientAPI.GetLeaderboard(req, OnGetOverallLeaderboard, OnPlayfabError);
        }

        void OnGetOverallLeaderboard(GetLeaderboardResult result)
        {
            OverallLeaderboard = result.Leaderboard;
            overallCompleted = true;
            if (friendCompleted == true)
                ShowLeaderboard();
        }
        
        private void GetFriendLeaderboard()
        {
            GetFriendLeaderboardAroundPlayerRequest req = new GetFriendLeaderboardAroundPlayerRequest() { MaxResultsCount = 50, StatisticName = LeaderboardName };
            PlayFabClientAPI.GetFriendLeaderboardAroundPlayer(req, OnGetFriendLeaderboard, OnPlayfabError);
        }


        void OnGetFriendLeaderboard(GetFriendLeaderboardAroundPlayerResult result)
        {
            FriendLeaderboard = result.Leaderboard;

            friendCompleted = true;
            if (overallCompleted == true)
                ShowLeaderboard();
        }

        void ShowLeaderboard()
        {
            view.Initialize(OverallLeaderboard, FriendLeaderboard, LeaderboardType);

            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LeaderboardView));


            this.Release();
        }


        void OnPlayfabError(PlayFabError pfe)
        {
            Debug.LogError(pfe.ErrorMessage);
            this.Fail();
            this.Release();
        }
    }
}
