﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;
using PlayFab;
using LinqTools;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using Newtonsoft.Json;

namespace NovaBlitz.UI
{
    public class SaveUiHintDataSignal : Signal { }
    public class SaveUiHintDataCommand : Command
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        
        public override void Execute()
        {
            if (PlayerProfile.UiHintData == null || PlayerProfile.UiHintDataLastSaved.AddSeconds(5) > Server.Time)
                return;
            PlayerProfile.UiHintDataLastSaved = Server.Time;
            Timer.Instance.StartCoroutine(SaveHintsCouroutine());
        }

        private IEnumerator SaveHintsCouroutine()
        {
            yield return new WaitForSecondsRealtime(5);
            UpdateUserDataRequest req = new UpdateUserDataRequest();
            Dictionary<string, string> data = new Dictionary<string, string>();

            data[NovaConfig.KEY_UI_HINTS] = JsonConvert.SerializeObject(this.PlayerProfile.UiHintData, Formatting.None);
            req.Data = data;
            req.Permission = UserDataPermission.Private;

            PlayFabClientAPI.UpdateUserData(req, OnUpdateUserData, OnPlayFabError);
        }

        void OnUpdateUserData(UpdateUserDataResult result)
        {
            Debug.Log("UI hints Updated");
        }

        void OnPlayFabError(PlayFabError pfe)
        {
            Debug.LogErrorFormat("UI hints update failed: {0}", pfe.ErrorMessage);
        }
    }
}
