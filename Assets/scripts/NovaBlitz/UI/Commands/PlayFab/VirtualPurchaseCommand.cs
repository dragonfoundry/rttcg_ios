using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class VirtualPurchaseSignal : Signal<VirtualPurchaseRequest> {}
	public class VirtualPurchaseCommand : Command
	{
		[Inject] public VirtualPurchaseRequest SignalRequest {get;set;}
        [Inject] public ClientService ClientService { get; set;}
        [Inject] public PlayerProfile PlayerProfile { get; set;}

        public override void Execute()
		{
            int balance;
            PlayerProfile.Wallet.Currencies.TryGetValue(SignalRequest.Currency, out balance);
            Debug.LogFormat("Virtual Purchase Request for {3}: Curr:{0}, Price:{1} Balance:{2}", SignalRequest.Currency, SignalRequest.ExpectedPrice, balance, SignalRequest.ItemId);
            this.ClientService.SendMessage(this.SignalRequest);
		}
	}
}
