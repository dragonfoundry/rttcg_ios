﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using System.Collections.Generic;
using Newtonsoft.Json;

using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
	public class UpdateDeckListSignal : Signal<DeckListDataContract, bool> {}
	public class UpdateDeckListCommand : Command
	{
		// Signal Parameters
		[Inject] public DeckListDataContract deckList {get; set;}
        [Inject] public bool isShouldRefresh { get; set; }

		// Injected Dependencies
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
				
		public override void Execute ()
		{
            if (deckList == null)
            {
                return;
            }
			this.Retain();
			UpdateUserDataRequest req = new UpdateUserDataRequest();
			Dictionary<string,string> data = new Dictionary<string, string>();

			data[NovaConfig.KEY_DECKLIST] = JsonConvert.SerializeObject(this.deckList, Formatting.None);
			req.Data = data;
			req.Permission = UserDataPermission.Private;

			//Debug.Log ("Updating Decklist with: " + data[decklistKey]);
			
			PlayFabClientAPI.UpdateUserData(req, OnUpdateUserData, OnPlayFabError);
            if(isShouldRefresh)
    			this.ProfileUpdatedSignal.Dispatch(ProfileSection.DeckList);
		}

		void OnUpdateUserData(UpdateUserDataResult result) 
		{
			Debug.Log ("Deck List Updated");
			this.Release();
		}
		
		void OnPlayFabError(PlayFabError pfe)
        {
            Debug.Log("Deck List Update failed: " + pfe.ErrorMessage);
            this.Release();
		}
	}
}
