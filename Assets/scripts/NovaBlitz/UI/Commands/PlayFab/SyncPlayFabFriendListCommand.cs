﻿/*using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using System.Collections.Generic;
using LinqTools;
using NovaBlitz.UI.DataContracts;
using Messages;

namespace NovaBlitz.UI
{
    public class InitializeFriendListSignal : Signal { }
    public class SyncPlayFabFriendListCommand : Command
    {
        private IEnumerator manageFriendDifferences;
        private bool modified = false;

        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public LoadFriendsListSignal LoadFriendsListSignal { get; set; }

        public override void Execute()
        {
            this.Retain();
            // Make sure there aren't any differences between friend lists
            if (this.manageFriendDifferences != null)
                Timer.Instance.StopCoroutine(manageFriendDifferences);
            this.manageFriendDifferences = ManageFriendDifferences();
            Timer.Instance.StartCoroutine(manageFriendDifferences);
        }

        private IEnumerator ManageFriendDifferences()
        {
            int waitTime = 1;
            List<string> playFabIdsToAdd = new List<string>();
            List<string> playFabIdsToRemove = new List<string>();

            SetIdsToAddAndRemove(ref playFabIdsToAdd, ref playFabIdsToRemove);

            while (playFabIdsToAdd.Count > 0 || playFabIdsToRemove.Count > 0)
            {
                yield return new WaitForSecondsRealtime(waitTime);
                if (this.PlayerProfile.friendListPlayFabResult == null)
                    break;
                // Compare FriendList to FriendDataList
                SetIdsToAddAndRemove(ref playFabIdsToAdd, ref playFabIdsToRemove);

                
                if (playFabIdsToAdd.Count > 0)
                {
                    var toAdd = playFabIdsToAdd.FirstOrDefault();
                    if(playFabIdsToRemove.Contains(toAdd))
                    {
                        playFabIdsToAdd.Remove(toAdd);
                        playFabIdsToRemove.Remove(toAdd);
                    }
                    else
                        PlayFabClientAPI.AddFriend(new AddFriendRequest { FriendPlayFabId = toAdd }, OnAddFriend, OnManageFriendPlayFabError);
                }
                else if (playFabIdsToRemove.Count > 0)
                    PlayFabClientAPI.RemoveFriend(new RemoveFriendRequest { FriendPlayFabId = playFabIdsToRemove[0] }, OnRemoveFriend, OnManageFriendPlayFabError);
                waitTime++;
            }

            yield return new WaitForSecondsRealtime(2);

            if (modified)
            {
                this.LoadFriendsListSignal.Dispatch();
            }

            this.Release();

        }

        private void SetIdsToAddAndRemove(ref List<string> addList, ref List<string> removeList)
        {
            // Compare FriendList to FriendDataList
            if (this.PlayerProfile.friendListPlayFabResult != null && this.PlayerProfile.friendListPlayFabResult.Friends != null)
            {
                removeList = this.PlayerProfile.friendListPlayFabResult.Friends.Where(f => !this.PlayerProfile.FriendList.Contains(f.FriendPlayFabId)).Select(f => f.FriendPlayFabId).ToList();
                addList.Clear();
                foreach (var friend in this.PlayerProfile.FriendList)
                {
                    if (!this.PlayerProfile.friendListPlayFabResult.Friends.Any(f => f.FriendPlayFabId == friend))
                    {
                        addList.Add(friend);
                    }
                }
            }
            else
            {
                removeList = new List<string>();
                addList = this.PlayerProfile.FriendList.ToList();
            }
        }

        void OnAddFriend(AddFriendResult result)
        {
            this.modified = true;
            FriendInfo friendInfo = new FriendInfo() { FriendPlayFabId = (result.Request as AddFriendRequest).FriendPlayFabId };
            this.PlayerProfile.friendListPlayFabResult.Friends.Add(friendInfo);
        }

        void OnRemoveFriend(RemoveFriendResult result)
        {
            this.modified = true;
            var friend = this.PlayerProfile.friendListPlayFabResult.Friends.FirstOrDefault(f => f.FriendPlayFabId == (result.Request as RemoveFriendRequest).FriendPlayFabId);
            if (friend != null)
                this.PlayerProfile.friendListPlayFabResult.Friends.Remove(friend);
        }

        void OnManageFriendPlayFabError(PlayFabError pfe)
        {
            if (manageFriendDifferences != null)
                Timer.Instance.StopCoroutine(manageFriendDifferences);
            this.manageFriendDifferences = null;
            this.LoadFriendsListSignal.Dispatch();
            Fail();

            this.Release();
        }
    }
}
*/