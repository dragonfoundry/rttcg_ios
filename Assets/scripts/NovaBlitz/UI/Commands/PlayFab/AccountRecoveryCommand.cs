using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
	public class AccountRecoverySignal : Signal<string> {}
	public class AccountRecoveryCommand : Command
	{
        [Inject] public string EmailAddress { get; set; }

		[Inject] public ClientPrefs ClientPrefs {get;set;}
		[Inject] public MetagameSettings MetagameSettings {get;set;}
		
		[Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal {get;set;}
		[Inject] public ShowNewConfirmDialogSignal ShowNewConfirmDialogSignal {get;set;}
		
		private string _Email;
		
		public override void Execute()
		{
			this.Retain();
            if(EmailAddress != null && EmailAddress.Contains("@"))
            {
                _Email = EmailAddress;
                SendRequest();
            }
            else if (null != this.ClientPrefs.UserAccountInfo) {
				_Email = this.ClientPrefs.UserAccountInfo.PrivateInfo.Email;
				OnEmail (_Email);
			} else
				GetEmail ();
		}

		void GetEmail()
		{
			this.Release();
		}

		void OnEmail(string address)
		{
			string dialogText = string.Format(I2.Loc.ScriptLocalization.Reset_Password_Prompt, address, 0 ,0);
			
			this.ShowNewConfirmDialogSignal.Dispatch(new NewConfirmDialogParams(I2.Loc.ScriptLocalization.Reset_Password, dialogText, OnDialogDismissed, false ));
		}

        public void OnDialogDismissed(bool isConfirmed)
        {
            if (isConfirmed)
            {
                this.SendRequest();
            }
            else
            {
                this.Fail();
                this.Release();
            }
        }

		void SendRequest()
		{
			SendAccountRecoveryEmailRequest req = new SendAccountRecoveryEmailRequest();
			req.TitleId = this.MetagameSettings.GameTitleId;
			req.Email = _Email;
			PlayFabClientAPI.SendAccountRecoveryEmail(req, OnResponse, OnPlayFabError);
		}
		
		void OnResponse(SendAccountRecoveryEmailResult result)
		{
			this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Account_Recovery, string.Format(I2.Loc.ScriptLocalization.Account_Recovery_Message, _Email,0,0)));
			this.Release();
		}
		
		void OnPlayFabError(PlayFabError error)
        {
            this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Account_Recovery_Failed, string.Format(I2.Loc.ScriptLocalization.Account_Recovery_Failed_Message, _Email,0,0)));
            //this.PlayfabErrorRaisedSignal.Dispatch(error);
			this.Release();
		}
	}
}