﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class InitializeLiveEnvironmentSignal: Signal {}
    public class InitializeLiveEnvironmentCommand : Command
    {
		[Inject] public MetagameSettings MetagameSettings {get;set;}
		
		private string liveTitleId = "721F";
		private string liveChatAppId = "9a053bb4-2d30-4f07-94aa-94d3ae52b09f";
		
		public override void Execute ()
        {
            //Debug.LogWarning("Initializing LIVE Enviorment");
            this.MetagameSettings.GameTitleId = this.liveTitleId;
			this.MetagameSettings.PhotonChatAppID = this.liveChatAppId;
		}
    }
}
