﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using NovaBlitz.Game;
using System.Collections.Generic;
using Messages;
using LinqTools;

namespace NovaBlitz.UI
{
    public class DeclineFriendRequestSignal : Signal<string> { };
    public class DeclineFriendResponseSignal : Signal<string, bool> { }; // friend playFabId, is Successfully requested
    public class DeclineFriendRequestCommand : Command
    {
        // Signal Parameters
        [Inject] public string PlayFabIdToDecline { get; set; }

        // Injected Dependencies
        [Inject] public DeclineFriendResponseSignal DeclineFriendResponseSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientService ClientService { get; set; }

        public override void Execute()
        {
            if (this.PlayFabIdToDecline == this.PlayerProfile.MetaProfile.PlayFabID)
            {
                Debug.LogError("#FRIENDS# Attempted to Decline yourself");
                //  Don't allow removing yourself to your friends list :)
                this.DeclineFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
                return;
            }

            /*if (!this.PlayerProfile.PendingFriendRequests.ContainsKey(this.PlayFabIdToDecline))
            {
                this.DeclineFriendResponseSignal.Dispatch(this.PlayFabIdToDecline, true);
                return;
            }*/

            Debug.LogError("#FRIENDS# Decline Friend request sent");
            this.ClientService.SendMessage(new ManageFriendRequest { FriendPlayFabId = PlayFabIdToDecline, RequestType = RequestType.DeclineRequest });

        }
    }
}