﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using NovaBlitz.Game;
using System.Collections.Generic;
using Messages;
using LinqTools;

namespace NovaBlitz.UI
{
    public class ConfirmFriendSignal : Signal<string> { };
    public class ConfirmFriendResponseSignal : Signal<string, bool> { }; // friend playFabId, is Successfully requested
    public class ConfirmFriendCommand : Command
    {
        // Signal Parameters
        [Inject] public string PlayFabIdToConfirm { get; set; }

        // Injected Dependencies
        [Inject] public ConfirmFriendResponseSignal ConfirmFriendResponseSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientService ClientService { get; set; }

        public override void Execute()
        {
            if (this.PlayFabIdToConfirm == this.PlayerProfile.MetaProfile.PlayFabID)
            {
                Debug.LogError("#FRIENDS# Attempted to Confirm yourself");
                //  Don't allow adding yourself to your friends list :)
                this.ConfirmFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
                return;
            }

            NovaBlitzFriend friend;
            if (this.PlayerProfile.FriendListData.TryGetValue(this.PlayFabIdToConfirm, out friend) && friend.FriendData != null)
            {
                if (friend.FriendData.IsRequest) // can only confirm requests
                {
                    this.ClientService.SendMessage(new ManageFriendRequest { FriendPlayFabId = PlayFabIdToConfirm, RequestType = RequestType.ConfirmRequest });
                }
                else
                {
                    // We're already friends; just confirm that this was successful?
                    Debug.LogWarning("Already friends! Reconfirming");
                    this.ConfirmFriendResponseSignal.Dispatch(this.PlayFabIdToConfirm, true);
                }
            }
            else
            {
                Debug.LogError("#FRIENDS# Confirm Friend request can't be sent: not friends");
                this.ConfirmFriendResponseSignal.Dispatch(this.PlayFabIdToConfirm, false);
            }
        }
    }
}