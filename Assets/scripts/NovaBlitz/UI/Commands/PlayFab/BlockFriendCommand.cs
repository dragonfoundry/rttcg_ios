﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using Messages;
using System.Collections.Generic;
using NovaBlitz.Game;
using PlayFab;
using Newtonsoft.Json;

namespace NovaBlitz.UI
{
	public class BlockFriendSignal : Signal<string> {} //user playfabId
	public class BlockFriendResponseSignal : Signal<string,bool> {}
    public class BlockFriendCommand : Command
    {
		// Signal Parameters
		[Inject] public string PlayfabIdToBlock {get;set;}
		
		// Injected Dependency
		[Inject] public BlockFriendResponseSignal BlockFriendResponseSignal { get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
        [Inject] public ClientService ClientService { get; set; }

        public override void Execute()
        {
            if (this.PlayfabIdToBlock == this.PlayerProfile.MetaProfile.PlayFabID)
            {
                //  Don't allow blocking yourself :)
                this.BlockFriendResponseSignal.Dispatch(PlayfabIdToBlock,false);
            }
            else if(!this.PlayerProfile.BlockList.Contains(PlayfabIdToBlock))
            {
                this.ClientService.SendMessage(new ManageFriendRequest { FriendPlayFabId = PlayfabIdToBlock, RequestType = RequestType.BlockRequest });
            }
            else
            {
                this.BlockFriendResponseSignal.Dispatch(PlayfabIdToBlock,false);
            }
        }
    }
}
