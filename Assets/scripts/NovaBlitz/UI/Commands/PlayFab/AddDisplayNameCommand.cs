﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
	public class AddDisplayNameSignal : Signal<string> {}
	public class AddDisplayNameCommand : Command
	{
		[Inject] public string displayName { get; set; }
		[Inject] public PlayerProfile PlayerProfile { get; set; }

		public override void Execute()
		{
            this.Retain();
			UpdateUserTitleDisplayNameRequest req = new UpdateUserTitleDisplayNameRequest ();
			req.DisplayName = displayName;
			PlayFabClientAPI.UpdateUserTitleDisplayName (req, OnDisplayNameAdded, OnPlayFabError);
		}


		void OnDisplayNameAdded(UpdateUserTitleDisplayNameResult result)
		{
			this.PlayerProfile.MetaProfile.Name = result.DisplayName;
			this.Release();
		}

		void OnPlayFabError(PlayFabError pfe) 
		{
			Debug.LogError("got add display name error " + pfe.Error + ": " + pfe.ErrorMessage);

			this.Fail();
            this.Release();
        }
	}
}

