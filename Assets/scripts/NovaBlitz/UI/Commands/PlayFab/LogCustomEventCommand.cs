﻿using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace NovaBlitz.UI
{
	public class LogCustomEventSignal : Signal<string, Dictionary<string,object>>{}
	public class LogCustomEventCommand : Command 
	{
		// Signal Parameters
		[Inject] public string EventName {get;set;}
		[Inject] public Dictionary<string,object> ParamsDictionary {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }
        public override void Execute()
        {
            if (PlayFabClientAPI.IsClientLoggedIn())
            {
                Debug.Log("Can't log custom event: " + this.EventName + " because we're logged out");
                return; // Can't log with a null auth key.
            }

            ParamsDictionary["d_device"] = SystemInfo.deviceType;
            ParamsDictionary["d_model"] = SystemInfo.deviceModel;
            ParamsDictionary["d_platform"] = Application.platform.ToString();
            ParamsDictionary["d_resolution"] = Screen.currentResolution.ToString();
            ParamsDictionary["p_unique_cards"] = PlayerProfile.OwnedCards.Count;
            ParamsDictionary["p_rating"] = PlayerProfile.MetaProfile.CurrentRating;
            this.Retain();
			WriteClientPlayerEventRequest request = new WriteClientPlayerEventRequest();
            //PlayFabId was removed from the event, because it is detected server side ( i think )
			//request.PlayFabId = this.PlayerProfile.MetaProfile.PlayFabID;
			request.EventName = this.EventName;
			request.Body = this.ParamsDictionary;
			Debug.Log("Logging custom event: " + this.EventName);
			PlayFabClientAPI.WritePlayerEvent(request,LogEventCallback, ErrorCallback);
		}
		
		private void LogEventCallback(WriteEventResponse result)
		{
			this.Release();
		}
		
		private void ErrorCallback(PlayFabError error)
		{
			this.Fail();
            this.Release();
        }
	
	}
	
}
