using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;

public class CatalogLoadedSignal : Signal<string> {}