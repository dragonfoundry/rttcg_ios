﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using Messages;
using System.Collections.Generic;
using NovaBlitz.Game;
using PlayFab;
using Newtonsoft.Json;

namespace NovaBlitz.UI
{
    public class UnblockFriendSignal : Signal<string> { } //user playfabId
    public class UnblockFriendResponseSignal : Signal<string,bool> { }
    public class UnblockFriendCommand : Command
    {
        // Signal Parameters
        [Inject] public string PlayfabIdToUnblock { get; set; }

        // Injected Dependency
        [Inject] public UnblockFriendResponseSignal UnblockFriendResponseSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientService ClientService { get; set; }

        public override void Execute()
        {
            if (this.PlayfabIdToUnblock == this.PlayerProfile.MetaProfile.PlayFabID)
            {
                //  Don't allow blocking yourself :)
                this.UnblockFriendResponseSignal.Dispatch(PlayfabIdToUnblock,false);
            }
            else if (this.PlayerProfile.BlockList.Contains(PlayfabIdToUnblock))
            {

                this.ClientService.SendMessage(new ManageFriendRequest { FriendPlayFabId = PlayfabIdToUnblock, RequestType = RequestType.UnblockRequest });
            }
            else
            {
                this.UnblockFriendResponseSignal.Dispatch(PlayfabIdToUnblock,false);
            }
        }
    }
}
