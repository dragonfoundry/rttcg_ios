﻿/*using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using System.Collections.Generic;
using UnityEngine;
using Messages;
using LinqTools;

namespace NovaBlitz.UI
{
    // Use this command to add and remove friends from your friend list, after the server's told you what to do.


    public class ManagePlayFabFriendListSignal : Signal<AddFriendRequest, RemoveFriendRequest> { };
    public class ManagePlayFabFriendListCommand : Command
    {
        // Signal Parameters
        [Inject] public AddFriendRequest AddFriendRequest { get; set; }
        [Inject] public RemoveFriendRequest RemoveFriendRequest { get; set; }

        // Injected Dependencies
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public LoadFriendsListSignal LoadFriendsListSignal { get; set; }
        [Inject] public PlayFabChatClient PlayFabChatClient { get; set; }
        [Inject] public DeclineFriendResponseSignal DeclineFriendResponseSignal { get; set; }
        [Inject] public ConfirmFriendResponseSignal ConfirmFriendResponseSignal { get; set; }

        private string friendPlayfabId;

        public override void Execute()
        {
            if(!string.IsNullOrEmpty(AddFriendRequest.FriendPlayFabId))
            {
                var friend = this.PlayerProfile.GetFriend(AddFriendRequest.FriendPlayFabId);
                if (this.AddFriendRequest.FriendPlayFabId == this.PlayerProfile.MetaProfile.ID || (friend != null && !friend.FriendInfo.IsRequest))
                    return;

                // Add the friend to the friend list
                this.Retain();
                PlayFabClientAPI.AddFriend(this.AddFriendRequest, this.OnAddFriend, this.OnPlayfabError);
            }

            if (!string.IsNullOrEmpty(RemoveFriendRequest.FriendPlayFabId))
            {
                var friend = this.PlayerProfile.GetFriend(RemoveFriendRequest.FriendPlayFabId);
                FriendRequestData data;
                if (this.RemoveFriendRequest.FriendPlayFabId != this.PlayerProfile.MetaProfile.ID && (friend != null && !friend.FriendInfo.IsRequest))
                {
                    this.Retain();
                    PlayFabClientAPI.RemoveFriend(this.RemoveFriendRequest, this.OnRemoveFriend, this.OnPlayfabError);
                }
                else if (this.PlayerProfile.PendingFriendRequests.TryGetValue(this.RemoveFriendRequest.FriendPlayFabId, out data))
                {
                    // this was a request, not a current friend. wipe it now.
                    //this.PlayerProfile.PendingFriendRequests.Remove(this.RemoveFriendRequest.FriendPlayFabId);
                    //var request = this.PlayerProfile.FriendListData.FirstOrDefault(f => f.FriendInfo.FriendPlayFabId == this.RemoveFriendRequest.FriendPlayFabId && f.IsRequest);
                    //this.PlayerProfile.FriendListData.Remove(request);
                    RemoveFriend(this.RemoveFriendRequest.FriendPlayFabId);
                    return;
                }

                else
                    return;
            }
        }

        void OnAddFriend(AddFriendResult result)
        {
            var friend = (result.Request as AddFriendRequest);
            this.PlayerProfile.FriendList.Add(friend.FriendPlayFabId);
            this.PlayerProfile.BlockList.Remove(friend.FriendPlayFabId);
            this.PlayerProfile.PendingFriendRequests.Remove(friend.FriendPlayFabId);
            
            //var friendListItem = this.PlayerProfile.FriendListData.FirstOrDefault(f => f.FriendInfo.FriendPlayFabId == friend.FriendPlayFabId && f.IsRequest);
            //if (friendListItem != null)
            //    this.PlayerProfile.FriendListData.Remove(friendListItem);

            this.ConfirmFriendResponseSignal.Dispatch(friend.FriendPlayFabId, true);
            //    this.PlayerProfile.FriendListData.Remove(friendListItem);
            this.LoadFriendsListSignal.Dispatch();
            //this.PlayFabChatClient.UpdatePhotonFriends();
            this.Release();
        }

        void OnRemoveFriend(RemoveFriendResult result)
        {
            var friend = (result.Request as RemoveFriendRequest);
            RemoveFriend(friend.FriendPlayFabId);
            this.Release();
        }

        void RemoveFriend(string playFabId)
        {

            this.PlayerProfile.FriendList.Remove(playFabId);
            this.PlayerProfile.PendingFriendRequests.Remove(playFabId);
            //var friendListItem = this.PlayerProfile.FriendListData.FirstOrDefault(f => f.FriendInfo.FriendPlayFabId == playFabId);
            //if (friendListItem != null)
                this.PlayerProfile.FriendListData.Remove(playFabId);
            this.DeclineFriendResponseSignal.Dispatch(playFabId, true);
            this.LoadFriendsListSignal.Dispatch();
        }

        void OnPlayfabError(PlayFabError pfe)
        {
            this.LoadFriendsListSignal.Dispatch();
            this.Fail();
            this.Release();
        }
    }
}*/