﻿using System;
using UnityEngine;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;
using NovaBlitz.Economy;
using NovaBlitz.UI.DataContracts;
using Newtonsoft.Json;

namespace NovaBlitz.UI
{
	public class LoadPlayerSettingsSignal : Signal {}
	public class LoadPlayerSettingsCommand : Command
	{
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}
		[Inject] public GameData GameData {get;set;}
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }

		public override void Execute()
        {
            this.LogoutUserSignal.AddListener(OnLogout);
            Debug.LogFormat("LoadPlayerSettings: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Retain();
			var req = new GetUserDataRequest{
				Keys = new List<string>{
                    NovaConfig.KEY_AVATAR,
                    NovaConfig.KEY_CARDBACK,
                    NovaConfig.KEY_DECKLIST,
                    NovaConfig.KEY_UI_HINTS,
                    NovaConfig.KEY_TRACKING_DATA_CACHE
                }
			};
			PlayFabClientAPI.GetUserData(req, OnGetUserData, OnPlayFabError);

            LoadRateGameData();
		}

		private void OnGetUserData(GetUserDataResult result)
        {
            Debug.LogFormat("LoadPlayerSettings got settings: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            UserDataRecord avatarUDR;
			if (result.Data.TryGetValue(NovaConfig.KEY_AVATAR, out avatarUDR) && !string.IsNullOrEmpty(avatarUDR.Value))
				this.PlayerProfile.MetaProfile.AvatarID = avatarUDR.Value;
            else
                this.PlayerProfile.MetaProfile.AvatarID = NBEconomy.ITEMID_KEY_AVATAR1;

            UserDataRecord cardbackUDR;
            if (result.Data.TryGetValue(NovaConfig.KEY_CARDBACK, out cardbackUDR))
                this.PlayerProfile.MetaProfile.CardBackID = cardbackUDR.Value;
            else
                this.PlayerProfile.MetaProfile.CardBackID = NBEconomy.ITEMID_KEY_CARDBACK1;
            
            UserDataRecord uiHintsUDR;
            if (result.Data.TryGetValue(NovaConfig.KEY_UI_HINTS, out uiHintsUDR))
            {
                try
                {
                    UiHintData hintData = JsonConvert.DeserializeObject<UiHintData>(uiHintsUDR.Value);
                    this.PlayerProfile.UiHintData = hintData;
                }
                catch
                {
                    Debug.LogError("UI Hint exception. resetting hints");
                    this.PlayerProfile.UiHintData = new UiHintData();
                }
            }
            else
            {
                this.PlayerProfile.UiHintData = new UiHintData();
            }


            UserDataRecord trackingCacheUDR;
            if (result.Data.TryGetValue(NovaConfig.KEY_TRACKING_DATA_CACHE, out trackingCacheUDR))
            {
                try
                {
                    this.MetricsTrackingService.LoadTrackingFromPlayFabResponse(trackingCacheUDR.Value);
                }
                catch { }
            }

            UserDataRecord deckListUDR;
            if (result.Data.TryGetValue(NovaConfig.KEY_DECKLIST, out deckListUDR))
            {
                DeckListDataContract deckList = null;
                try
                {
                    deckList = JsonConvert.DeserializeObject<DeckListDataContract>(deckListUDR.Value);// this.ProcessUserData(result.Data);
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("LoadPlayerSettings: Decklist Parsing Failed");
                    Debug.LogException(ex);
                }
                this.PlayerProfile.DeckListDataContract = deckList;
                Debug.LogFormat("LoadPlayerSettings: {0} Decklists found", deckList != null && deckList.Decks != null ? deckList.Decks.Count : -1000);
            } else
            {
                Debug.LogFormat("LoadPlayerSettings: No Decklists found");
            }


            Debug.LogFormat("LoadPlayerSettings: Parsed settings: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.LogoutUserSignal.RemoveListener(OnLogout);
            this.PlayerProfile.isPlayerSettingsLoaded = true;
            this.Release();
        }

		private void OnPlayFabError(PlayFabError pferror)
        {
            this.LogoutUserSignal.RemoveListener(OnLogout);
            this.Release();
		}
        
        void OnLogout(ShouldUnlinkDevice shouldUnlink, ShouldAutoReconnect shouldReconnect)
        {
            this.LogoutUserSignal.RemoveListener(OnLogout);
            Debug.LogError("Logout. Cancelling LoadPlayerSettings.");
            this.Fail();
            this.Release();
        }

        private void LoadRateGameData()
        {
            string rateGameStatus = PlayerPrefs.GetString(this.PlayerProfile.MetaProfile.PlayFabID + NovaConfig.RATE_GAME_SUFFIX, string.Empty);
            if (!string.IsNullOrEmpty(rateGameStatus))
            {
                try
                {
                    PlayerProfile.RateGameTrackingData = JsonConvert.DeserializeObject<RateGameTrackingData>(rateGameStatus);
                }
                catch
                {
                    PlayerProfile.RateGameTrackingData = new RateGameTrackingData();
                }
            }
            else
                PlayerProfile.RateGameTrackingData = new RateGameTrackingData();

        }
    }
}
