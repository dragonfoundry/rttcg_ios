﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
	public class LoginUserWithHashedEmailSignal : Signal<UserCredentials> {}
	
    public class LoginUserWithHashedEmailCommand : Command
    {
		// Signal Parameters
		[Inject] public UserCredentials UserCredentials {get;set;}

		// Injected Dependencies
		[Inject] public MetagameSettings MetagameSettings {get;set;}
		[Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public UserLoggedInSignal UserLoggedInSignal {get;set;}
		[Inject] public AccountEmailFoundSignal AccountEmailFoundSignal {get;set;}
		[Inject] public InvalidEmailOrPasswordSignal InvalidEmailOrPasswordSignal {get;set;}

        private int Attempts = 0;

        /// <summary>
        /// Call the playfab
        /// </summary>
        public override void Execute ()
		{
			this.Retain();
            Timer.Instance.StartCoroutine(TryLogin());
        }

        private IEnumerator TryLogin()
        {
            yield return new WaitForSecondsRealtime(Attempts * Attempts);
            Attempts++;
            Debug.LogFormat("Logging into playfab ({0}) with HASHED {1}, password redacted", this.MetagameSettings.GameTitleId, this.UserCredentials.Email);
            LoginWithEmailAddressRequest req = new LoginWithEmailAddressRequest();
            req.TitleId = this.MetagameSettings.GameTitleId;
            req.Email = this.UserCredentials.Email;
            req.Password = this.UserCredentials.Password;
            req.InfoRequestParameters = new GetPlayerCombinedInfoRequestParams();
            req.InfoRequestParameters.GetUserAccountInfo = true;
            PlayFabClientAPI.LoginWithEmailAddress(req, OnLoginSuccess, OnLoginFailure);
        }

        void OnLoginSuccess(LoginResult result)
        {
            this.PlayerProfile.isAgeGatePassed = false; // they've logged in a non-compliant account, so lock everything down.
			this.PlayerProfile.LoginInfoResult = result.InfoResultPayload;
            PlayerPrefs.SetString("isAgeGatePassed", "false");
            Debug.Log("got login result with hashed email " + result.SessionTicket);
            this.UserLoggedInSignal.Dispatch(result);
			this.Release();
		}

		void OnLoginFailure(PlayFabError pfe) 
		{
			switch(pfe.Error)
			{
				case PlayFabErrorCode.AccountNotFound:
					Debug.LogWarning("***Hashed Account not Found***");
					this.AccountEmailFoundSignal.Dispatch(false);
				    break;
				case PlayFabErrorCode.InvalidEmailOrPassword:
					Debug.LogWarning("***Hashed Account Found***");
                    this.AccountEmailFoundSignal.Dispatch(true);
					this.InvalidEmailOrPasswordSignal.Dispatch();
				    break;
                default:
                    if (Attempts < 3)
                    {
                        TryLogin();
                        return;
                    }
                    break;
            }
			this.Fail();
            this.Release();
        }

    }
	
	
}
