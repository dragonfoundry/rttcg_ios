﻿using UnityEngine;
using System.Collections;
using NovaBlitz.Game;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
    public class LinkDeviceOrSteamSignal : Signal { }
    public class LinkDeviceOrSteamCommand : Command
    {
        // Injected Dependencies
        [Inject] public MetagameSettings MetagameSettings { get; set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }
        [Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal { get; set; }
        [Inject] public ClientService ClientService { get; set; }
#if UNITY_STEAM
        [Inject] public SteamAuthTicketSignal SteamAuthTicketSignal { get; set; }
        [Inject] public GetSteamAuthTokenSignal GetSteamAuthTokenSignal { get; set; }
#endif

        /// <summary>
        /// Call the playfab login function for the appropriate device
        /// </summary>
        public override void Execute()
        {
            this.Retain();
//#if TEST_BUILD
#if UNITY_STEAM
            this.SteamAuthTicketSignal.AddListener(OnSteamAuthTicket);
            this.GetSteamAuthTokenSignal.Dispatch();
#elif (UNITY_IOS)
			Debug.Log("Linking IOS device ID to account: " + ClientPrefs.DeviceUniqueIdentifier);
            LinkIOSDeviceIDRequest ios = new LinkIOSDeviceIDRequest();
            ios.DeviceId = ClientPrefs.DeviceUniqueIdentifier;
            PlayFabClientAPI.LinkIOSDeviceID(ios, OnLinkIOSSuccess, OnLinkFailure);
            
#elif (UNITY_ANDROID)
            Debug.Log("Linking ANDROID device ID to account: " + ClientPrefs.DeviceUniqueIdentifier);
            LinkAndroidDeviceIDRequest android = new LinkAndroidDeviceIDRequest();
            //string uniqueId = System.Guid.NewGuid().ToString();
            //PlayerPrefs.SetString(NovaConfig.KEY_ANDROID_ID, uniqueId);
            android.AndroidDeviceId = ClientPrefs.DeviceUniqueIdentifier;
            PlayFabClientAPI.LinkAndroidDeviceID(android, OnLinkAndroidSuccess, OnLinkFailure);

#else
            Debug.Log("Linking Custom ID to account");
            LinkCustomIDRequest custom = new LinkCustomIDRequest();
            custom.CustomId = ClientPrefs.DeviceUniqueIdentifier;
            PlayFabClientAPI.LinkCustomID(custom, OnLinkCustomSuccess, OnLinkFailure);
#endif
        }

#if UNITY_STEAM
        void OnSteamAuthTicket(string authStr)
        {
            steamAuthStr = authStr;
            this.SteamAuthTicketSignal.RemoveListener(OnSteamAuthTicket);
            if (string.IsNullOrEmpty(authStr))
            {
                Debug.LogError("Steam ticket is null - auth failed");
                this.Fail();
                this.Release();
            }
            else if(this.PlayerProfile.LoginInfoResult == null 
                || this.PlayerProfile.LoginInfoResult.AccountInfo == null 
                || this.PlayerProfile.LoginInfoResult.AccountInfo.SteamInfo == null)
            {
                LinkSteam();
            }
            else
            {
                UnlinkThenLinkSteam();
            }
        }
#endif
        void UnlinkThenLinkSteam()
        {
            Debug.Log("unlinking then linking steam");
            UnlinkSteamAccountRequest unlinkSteam = new UnlinkSteamAccountRequest();
            PlayFabClientAPI.UnlinkSteamAccount(unlinkSteam, OnUnlinkSteamSuccess, OnunlinkSteamFailure);
        }
        private string steamAuthStr = string.Empty;

        void OnUnlinkSteamSuccess(UnlinkSteamAccountResult result) { LinkSteam(); }
        void OnunlinkSteamFailure(PlayFabError error) { }


        void LinkSteam()
        {
            Debug.Log("Linking STEAM device ID to account");
            LinkSteamAccountRequest steam = new LinkSteamAccountRequest();
            steam.ForceLink = true;
            steam.SteamTicket = steamAuthStr;
            PlayFabClientAPI.LinkSteamAccount(steam, OnLinkSteamSuccess, OnLinkFailure);

        }

        // On success, continue the flow
        void OnLinkIOSSuccess(LinkIOSDeviceIDResult result)
        {
			Debug.Log("IOS Device Linked: " + ClientPrefs.DeviceUniqueIdentifier);
            this.Release();
        }

        // On success, continue the flow
        void OnLinkSteamSuccess(LinkSteamAccountResult result)
        {
            Debug.Log("Steam Account Linked ");
            this.Release();
        }

        // On success, continue the flow
        void OnLinkAndroidSuccess(LinkAndroidDeviceIDResult result)
        {
			Debug.Log("Android Device Linked: " + ClientPrefs.DeviceUniqueIdentifier);
            this.Release();
        }

        // On success, continue the flow
        void OnLinkCustomSuccess(LinkCustomIDResult result)
        {
            Debug.Log("Custom Device Linked: " + ClientPrefs.DeviceUniqueIdentifier);
            this.Release();
        }

        // On failure, just handle the standard errors (
        void OnLinkFailure(PlayFabError pfe)
        {
            Debug.Log("Linking Device or Steam ID failed");
            if (pfe.Error == PlayFabErrorCode.AccountAlreadyLinked)
            {
                // The account is already linked to this account. No need to worry.
                Debug.LogErrorFormat("account {0} is already linked", this.ClientPrefs.SessionTicket);
            }
            else if (pfe.Error == PlayFabErrorCode.LinkedAccountAlreadyClaimed)
            {
                // The Steam account is already claimed by another Nova Blitz account. Nothing can be done here.
                // It shouldn't, in theory, be possible to get here, because the flow is always to attempt to login with the device id before anything else.
                Debug.Log("CRITICAL: Linked Account Already Claimed.");
            }
            else if (pfe.Error == PlayFabErrorCode.LinkedDeviceAlreadyClaimed)
            {
                // The Device ID is already associated with another Nova Blitz account. Nothing can be done here.
                // It shouldn't, in theory, be possible to get here, because the flow is always to attempt to login with the device id before anything else.
                Debug.Log("CRITICAL: Linked Device Already Claimed.");
            }
            else
            {
                //this.PlayfabErrorRaisedSignal.Dispatch(pfe);
            }
            this.Fail();
            this.Release();
        }
    }
}
