using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
  public class MoneyPurchaseSignal : Signal<StoreData> { }
  public class MoneyPurchaseCommand : Command
  {
    [Inject] public StoreData storeData { get; set; }
    [Inject] public NovaIAPService NovaIAPService { get; set; }
    [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal { get; set; }

    public override void Execute()
    {
#if UNITY_IOS
      ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams { 
        Message = "In-app purchases are not active in this testflight build", 
        Title = "Cannot Purchase" 
        });
#else
      NovaIAPService.StartPurchaseFlow(storeData);
#endif
    }

  }
}
