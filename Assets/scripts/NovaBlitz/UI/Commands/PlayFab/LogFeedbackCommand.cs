﻿using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace NovaBlitz.UI
{
	public class LogFeedbackSignal : Signal<int, string> {} // Mood 1=happy 0=neutral -1=Frustrated, Comment=string
	public class LogFeedbackCommand : Command 
	{
		// Signal Parameters
		[Inject] public int Mood {get;set;}
		[Inject] public string Comment {get;set;}

		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		
		public override void Execute()
		{
			this.Retain();
			WriteClientPlayerEventRequest request = new WriteClientPlayerEventRequest();
            //PlayFabId was removed from the event, because it is detected server side ( i think )
			//request.PlayFabId = this.PlayerProfile.MetaProfile.PlayFabID;
			request.EventName = "NB_Feedback";
			request.Body = new Dictionary<string, object>();
			request.Body["Mood"] = this.Mood;
			request.Body["Comment"] = this.Comment;
            Debug.LogFormat("Mood:{0} Comment:{1}", this.Mood, this.Comment);
            PlayFabClientAPI.WritePlayerEvent(request,LogEventCallback, ErrorCallback);
		}
		
		private void LogEventCallback(WriteEventResponse result)
		{
			this.Release();
		}
		
		private void ErrorCallback(PlayFabError error)
		{
			this.Fail();
            this.Release();
        }
	}
}
