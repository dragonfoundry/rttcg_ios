using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;
using LinqTools;

namespace NovaBlitz.UI
{
	public class UserStatisticsLoadedSignal : Signal {}
	public class LoadUserStatisticsSignal : Signal {}
	public class LoadUserStatisticsCommand : Command
	{
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}

		public override void Execute()
        {
            Debug.LogFormat("LoadUserStatistics: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Retain();
			var req = new GetPlayerStatisticsRequest();
			PlayFabClientAPI.GetPlayerStatistics(req, OnResult, OnError);

		}

		protected void OnResult(GetPlayerStatisticsResult result)
        {
            Debug.LogFormat("LoadUserStatistics: got data: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            if (result.Statistics != null)
            {
                this.PlayerProfile.UserStatistics.Clear();
                foreach(var stat in result.Statistics)
                {
                    PlayerProfile.UserStatistics.Add(stat.StatisticName, stat.Value);
                }
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Statistics);
            }
            Debug.LogFormat("LoadUserStatistics: processed data: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Release();
		}
		
		protected void OnError(PlayFabError pfe) 
		{
			this.Release();
		}
	}
}
