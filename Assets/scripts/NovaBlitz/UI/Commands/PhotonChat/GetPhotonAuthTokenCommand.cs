﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;


namespace NovaBlitz.UI
{
    public class GetPhotonAuthTokenSignal : Signal<string,ChatMessage> {}
    public class GetPhotonAuthTokenCommand : Command
    {
        [Inject] public MetagameSettings MetagameSettings { get; set; }
        [Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public PlayFabChatClient PlayFabChatClient {get;set;}

        public override void Execute()
        {
            if (!this.PlayFabChatClient.IsChatClientConnected && !this.PlayFabChatClient.IsChatClientConnecting)
            {
                this.PlayFabChatClient.IsChatClientConnecting = true;
                //if (this.PlayFabChatClient != null && this.PlayFabChatClient.IsChatClientConnected)
                //    return;

                this.Retain();
                GetPhotonAuthenticationTokenRequest request = new GetPhotonAuthenticationTokenRequest();
                request.PhotonApplicationId = MetagameSettings.PhotonChatAppID;
                PlayFabClientAPI.GetPhotonAuthenticationToken(request, OnPhotonAuthenticationSuccess, OnPlayFabError);
            }
        }
		
		private void OnPhotonAuthenticationSuccess(GetPhotonAuthenticationTokenResult result)
		{
			this.PlayerProfile.MetaProfile.PhotonAuthToken = result.PhotonCustomAuthenticationToken;
            Debug.Log("Got photon auth token");
            this.Release();
		}
        
        void OnPlayFabError(PlayFabError pfe)
		{
			this.Fail();
            this.Release();
        } 
    }
}