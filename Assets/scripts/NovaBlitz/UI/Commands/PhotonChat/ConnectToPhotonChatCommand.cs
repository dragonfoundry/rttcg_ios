﻿using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using System.Collections;
using UnityEngine;

namespace NovaBlitz.UI
{
    public class ConnectedToGlobalChatSignal : Signal { }
    public class ConnectToPhotonChatSignal : Signal<string,ChatMessage,bool> {}
    public class ConnectToPhotonChatCommand : Command
    {
        [Inject] public MetagameSettings MetagameSettings { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public PlayFabChatClient PlayFabChatClient { get; set; }
        [Inject] public string channelId {get;set;}
        [Inject] public bool isForceConnect {get;set;}
		
		public override void Execute()
        {
            if(this.PlayerProfile.OnboardingProgress == null
                || !this.PlayerProfile.OnboardingProgress.TutorialsCompleted
                || (this.PlayerProfile.FriendListData.Count == 0 && !isForceConnect))
            {
                return;
            }

            if (this.PlayerProfile.isInitializingGlobalChat < 0) 
                this.PlayFabChatClient.WaitForConnection();

            if (!this.PlayFabChatClient.IsChatClientConnected 
                && (this.PlayerProfile.FriendListData.Count > 0 || isForceConnect))
            {
                this.PlayFabChatClient.ConnectToPhoton(this.MetagameSettings.PhotonChatAppID);
                this.Retain();
                Timer.Instance.StartCoroutine(WaitForPhotonConnection());
            }
            else if (this.PlayFabChatClient.IsChatClientConnected && string.IsNullOrEmpty(channelId))
            {
                this.PlayFabChatClient.UpdatePhotonFriends();
            }

        }

        private IEnumerator WaitForPhotonConnection()
        {
            while(this.PlayFabChatClient.IsChatClientConnecting)
            {
                yield return new WaitForSecondsRealtime(0.1f);
            }
            if (this.PlayFabChatClient.IsChatClientConnected)
            {
                Debug.Log("Connected to Photon via command");
                this.Release();
            }
            else
            {
                Debug.LogError("Failed to connect to Photon via command");
                this.Fail();
                this.Release();
            }
        }
    }
}
