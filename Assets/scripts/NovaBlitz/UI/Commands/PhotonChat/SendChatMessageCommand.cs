﻿using System;
using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
	public class SendChatMessageSignal : Signal<string,ChatMessage,bool>{} // FriendPlayfabID, chat message
    public class SendChatMessageCommand : Command
    {
		// Signal Parameters
		[Inject] public string ChannelOrPlayFabId {get;set;}
		[Inject] public ChatMessage ChatMessage {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public PlayFabChatClient PlayFabChatClient {get;set;}
		[Inject] public AddChatMessageSignal AddChatMessageSignal {get;set;}
        [Inject] public AddGlobalChatMessagesSignal AddGlobalChatMessagesSignal { get;set;}
		
		public override void Execute()
        {
            this.ChatMessage.SenderDisplayName = this.PlayerProfile.MetaProfile.Name;
            this.ChatMessage.SentTimeUTC = DateTime.UtcNow;

            if (ChannelOrPlayFabId.StartsWith("global"))
            {
                this.PlayFabChatClient.chatClient.PublishMessage(ChannelOrPlayFabId, new string[] 
                {
                    ChatMessage.SenderPlayFabId,
                    ChatMessage.AvatarArtId,
                    ChatMessage.SenderDisplayName,
                    ChatMessage.Message,
                    ChatMessage.TimeStampString
                });
                this.AddGlobalChatMessagesSignal.Dispatch(new List<ChatMessage> { this.ChatMessage });
            }
            else
            {
                // Send the Friend Chat message over Photon Private Messaging
                this.PlayFabChatClient.chatClient.SendPrivateMessage(this.ChannelOrPlayFabId, new string[]
                {
                    PhotonMessageType.DirectMessage.ToString(),
                    this.ChatMessage.AvatarArtId,
                    this.ChatMessage.SenderDisplayName,
                    this.ChatMessage.Message,
                    this.ChatMessage.TimeStampString
                }, true);

                // Also add the chat message to our local state and inform the UI of it
                this.AddChatMessageSignal.Dispatch(this.ChannelOrPlayFabId, this.ChatMessage);
            }
		}
    }
}