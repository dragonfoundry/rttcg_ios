﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using LinqTools;
using System;

namespace NovaBlitz.UI
{
	public class UpdateFriendStatusSignal : Signal <string,int> {} // <friendPlayfabId, ChatUserStatus>
	public class FriendStatusUpdatedSignal : Signal <string,int> {} // <friendPlayfabId, ChatUserStatus>
	public class UpdateFriendStatusCommand : Command
	{
		// Signal Parameters
		[Inject] public string FriendPlayfabId {get;set;}
		[Inject] public int ChatUserStatus {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public FriendStatusUpdatedSignal FriendStatusUpdatedSignal {get;set;} 
		
		public override void Execute()
		{
			if(this.PlayerProfile.IsFriendsListInitialized == false)
			{
				this.Retain();
				Timer.Instance.StartCoroutine(WaitForFriendsList());
			}
			else
			{
				DoWork();
			}
		}

        private IEnumerator WaitForFriendsList()
        {
            while(this.PlayerProfile.IsFriendsListInitialized == false)
			{
				yield return null;
			}
			
			this.DoWork();
			this.Release();
        }

        private void DoWork()
		{
            NovaBlitzFriend nbFriend;
			if(this.PlayerProfile.FriendListData.TryGetValue(this.FriendPlayfabId, out nbFriend))
			{
				nbFriend.ChatUserStatus = this.ChatUserStatus;
				this.FriendStatusUpdatedSignal.Dispatch(this.FriendPlayfabId, this.ChatUserStatus);
			}
		}
	}
}
