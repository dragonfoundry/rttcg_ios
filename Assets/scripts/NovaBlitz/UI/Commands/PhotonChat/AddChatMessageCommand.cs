﻿using System;
using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace NovaBlitz.UI
{
    [DataContract]
    public class ChatMessage
	{
        // CANNOT change the names
        [DataMember(Name = "P")] public string SenderPlayFabId { get; set; }
        [DataMember(Name = "N")] public string SenderDisplayName { get; set; }
        [DataMember(Name = "A")] public string AvatarArtId { get; set; }
        [DataMember(Name = "M")] public string Message { get; set; }
        [DataMember(Name = "T")] public double TimeSeconds { get; set; }
        [DataMember(Name = "R")] public bool isRead;
        [DataMember(Name = "Y")] public bool IsFromYou;
        
        public string TimeStampString { get { return TimeSeconds.ToString(); } set {
                double seconds = 0;
                double.TryParse(value, out seconds);
                TimeSeconds = seconds;
            } }

        public DateTime SentTimeUTC {
            get { return TimeSeconds <= 0 ? DateTime.MinValue : DateTime.MinValue.AddSeconds(TimeSeconds); }
            set { TimeSeconds = value.Subtract(DateTime.MinValue).TotalSeconds; } } 

        public DateTime SentTimeLocal { get { try
                {
                    return TimeSeconds <= 0 ? DateTime.MinValue : DateTime.MinValue.AddSeconds(TimeSeconds).ToLocalTime();
                }
                catch(Exception ex)
                {
                    Debug.LogErrorFormat("Timestamp is borked. Stamp is {0}s. LocalTime = {1}. Message from {2}: {3}, {4}, {5}", TimeSeconds, DateTime.UtcNow.ToLocalTime(), IsFromYou ? "you" : "other", SenderPlayFabId, SenderDisplayName, AvatarArtId, Message);
                    throw ex;
                }
            }
        }

        public ChatMessage() { }

		public ChatMessage(string senderPlayfabId, string senderAvatarArtId, string message, bool isFromYou)
		{
			this.SenderPlayFabId = senderPlayfabId;
			this.Message = message;
            this.IsFromYou = isFromYou;
            AvatarArtId = senderAvatarArtId;
        }
		
		public ChatMessage(string senderPlayfabId, string senderAvatarArtId, string senderDisplayName, string message, bool isFromYou)
        {
            this.SenderPlayFabId = senderPlayfabId;
			this.SenderDisplayName = senderDisplayName;
			this.Message = message;
            this.IsFromYou = isFromYou;
            AvatarArtId = senderAvatarArtId;
        }
        
        public bool AddChatMessageToThis(ChatMessage message)
        {
            if (message != null
                    && message.SenderPlayFabId == this.SenderPlayFabId
                    && message.TimeSeconds <= this.TimeSeconds + 120)
            {
                this.Message = string.Format("{0}\n{1}", this.Message, message.Message);
                return true;
            }
            else
            {
                return false;
            }
        }
	}
	
    public class AddChatMessageSignal : Signal<string,ChatMessage> { } // Friend playfab Id, ChatMessage
	public class ChatMessageAddedSignal : Signal<string,ChatMessage,bool> { } // Friend playfab Id, ChatMessage
    public class AddChatMessageCommand : Command
    {
		// Signal Parameters
		[Inject] public string FriendPlayfabId {get;set;}
		[Inject] public ChatMessage ChatMessage {get;set;}
		
		// Injected Dependencies
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ChatMessageAddedSignal ChatMessageAddedSignal {get;set;}
		
		/// <summary>
		/// Execute the command
		/// </summary>
		public override void Execute()
		{
			List<ChatMessage> chatLogForPlayer = null;
			if(!this.PlayerProfile.ChatLogs.TryGetValue(this.FriendPlayfabId, out chatLogForPlayer))
			{
                chatLogForPlayer = new List<ChatMessage>();
                this.PlayerProfile.ChatLogs[this.FriendPlayfabId] = chatLogForPlayer;
            }
            bool isCombined = false;
            if(chatLogForPlayer.Count > 0 && chatLogForPlayer.Last().AddChatMessageToThis(this.ChatMessage))
            {
                isCombined = true;
                // do nothing here - we've already combined the messages
            }
            else
                chatLogForPlayer.Add(this.ChatMessage);

            if (chatLogForPlayer.Count > NovaConfig.CHAT_LOG_MAX_SIZE)
            {
                chatLogForPlayer.RemoveRange(0, chatLogForPlayer.Count - NovaConfig.CHAT_LOG_MAX_SIZE);
            }

            if (string.IsNullOrEmpty(this.ChatMessage.SenderDisplayName))
			{
                NovaBlitzFriend nbFriend;
				if(this.PlayerProfile.FriendListData.TryGetValue(this.ChatMessage.SenderPlayFabId, out nbFriend))
				{
					// Look up the friends displayname if they are a friend.
					this.ChatMessage.SenderDisplayName = nbFriend.FriendData.DisplayName;
				}
				else if(this.PlayerProfile.MetaProfile.ID == this.FriendPlayfabId)
				{
					// Use the local players displayname if the message is a local one
					this.ChatMessage.SenderDisplayName = this.PlayerProfile.MetaProfile.Name;
				}
			}
			
			this.ChatMessageAddedSignal.Dispatch(this.FriendPlayfabId, this.ChatMessage, isCombined);
            this.PlayerProfile.isChatLogNeedsSaving = true;
		}
    }

    public class AddGlobalChatMessagesSignal : Signal<List<ChatMessage>> { } // Friend playfab Id, ChatMessage
    public class GlobalChatMessageAddedSignal : Signal<List<ChatMessage>, bool> { } // Friend playfab Id, ChatMessage
    public class AddGlobalChatMessagesCommand : Command
    {
        // Signal Parameters
        [Inject] public List<ChatMessage> ChatMessages { get; set; }

        // Injected Dependencies
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GlobalChatMessageAddedSignal GlobalChatMessageAddedSignal { get; set; }

        /// <summary>
        /// Execute the command
        /// </summary>
        public override void Execute()
        {
            bool isCombined = false;
            List<ChatMessage> globalChatLog = PlayerProfile.GlobalChatLog;
            ChatMessage LastMessage = globalChatLog.LastOrDefault();
            for(int i = 0; i<ChatMessages.Count; i++)
            {
                var msg = ChatMessages[i];
                if (LastMessage != null && LastMessage.AddChatMessageToThis(msg))
                {
                    ChatMessages.RemoveAt(i);
                    i--;
                    // do nothing here - we've already combined the messages
                    if (i < 0)
                        isCombined = true;
                }
                else
                {
                    globalChatLog.Add(msg);
                    LastMessage = msg;
                }
            }
            if (globalChatLog.Count > NovaConfig.CHAT_LOG_MAX_SIZE)
            {
                globalChatLog.RemoveRange(0, globalChatLog.Count - NovaConfig.CHAT_LOG_MAX_SIZE);
            }
            
            this.GlobalChatMessageAddedSignal.Dispatch(this.ChatMessages, isCombined);
        }
    }
}
