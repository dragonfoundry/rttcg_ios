﻿
using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace NovaBlitz.UI
{
    public class LoadChatHistorySignal : Signal { }
    public class LoadChatHistoryCommand : Command
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        
        public override void Execute()
        {
            try
            {
                var path = Path.Combine(Application.persistentDataPath, this.PlayerProfile.PlayFabId + NovaConfig.CHAT_LOG_FILE_NAME);
                if (File.Exists(path))
                {
                    var dataString = EngineUtils.Unzip(File.ReadAllBytes(path));
                    Dictionary<string, List<ChatMessage>> chatDict = JsonConvert.DeserializeObject<Dictionary<string, List<ChatMessage>>>(dataString);
                    if (chatDict != null)
                    {
                        this.PlayerProfile.ChatLogs = chatDict;
                        Debug.LogFormat("Loaded {0} chat logs", chatDict.Count);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            this.PlayerProfile.isChatLogLoaded = true;
        }

    }
}

/* BSON code

        var bsonString = File.ReadAllText(path);
        byte[] data = Convert.FromBase64String(bsonString);
        MemoryStream ms = new MemoryStream(data);
        using(BsonReader reader = new BsonReader(ms))
        {
            JsonSerializer serializer = new JsonSerializer();

            Dictionary<string, List<ChatMessage>> e = serializer.Deserialize<Dictionary<string, List<ChatMessage>>>(reader);

            // Movie Premiere
        } 


    MemoryStream ms = new MemoryStream();
    using (BsonWriter writer = new BsonWriter(ms))
    {
        JsonSerializer serializer = new JsonSerializer();
        serializer.Serialize(writer, this.PlayerProfile.ChatLogs);
    }
    string data = Convert.ToBase64String(ms.ToArray());
 * */

/* DEFLATE code
using (System.IO.Stream input = System.IO.File.OpenRead(fileToCompress))
{
    using (var raw = System.IO.File.Create(fileToCompress + ".deflated"))
    {
        using (Stream compressor = new System.IO.Compression.DeflateStream(raw, System.IO.Compression.CompressionMode.Compress))
        {
            byte[] buffer = new byte[WORKING_BUFFER_SIZE];
            int n;
            while ((n = input.Read(buffer, 0, buffer.Length)) != 0)
            {
                compressor.Write(buffer, 0, n);
            }
        }
    }
}*/
