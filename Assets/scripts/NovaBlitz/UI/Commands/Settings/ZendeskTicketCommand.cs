﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using LinqTools;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Zendesk;
using NovaBlitz.Game;

using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
    public class ZendeskTicket
    {
        public string ContactType;
        public string Description;
        public string Subject;
        public string EmailAddress;
        public int Happiness;
        public List<string> Tags = new List<string>();
        public List<string> Uploads = new List<string>();
    }
    public class ZendeskTicketSignal : Signal<ZendeskTicket, ReportType, List<ChatMessage>> { }
    public class ZendeskTicketCommand : Command
    {
        [Inject] public ZendeskTicket ZendeskTicket { get; set; }
        [Inject] public ReportType ReportType { get; set; }
        [Inject] public List<ChatMessage> ChatList { get; set; }

        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public GameData GameData { get; set; }

        protected string ZendeskUrl = "https://dragonfoundry.zendesk.com/api/v2/";
        
        private const string ApiToken = "XXXeOSINXZfud1XXBRDq4BBVimhasuN26VT8ZMK1VUX";
        private const string _ticketsJson = "requests.json";

        //private string emailAddress;

        public override void Execute()
        {
            try
            {
                this.Retain();
                ServicePointManager.ServerCertificateValidationCallback = (p1, p2, p3, p4) => true;

                ZendeskTicket.Tags.Add(this.GameData.VersionLabel);
                ZendeskTicket.Tags.Add(Application.platform.ToString());
                ZendeskTicket.Tags.Add(this.PlayerProfile.PlayFabId);
                ZendeskTicket.Tags.Add(string.Format("Rank{0}", this.PlayerProfile.CurrentRank));
                //ZendeskTicket.Tags.Add(string.Format("time:{0}",Server.Time.ToShortTimeString()));
                ZendeskTicket.Tags.Add(string.Format("proc:{0} {1}MHz", SystemInfo.processorType, SystemInfo.processorFrequency).Replace(' ','_'));
                ZendeskTicket.Tags.Add(string.Format("mem:{0} gpu{1}", SystemInfo.systemMemorySize, SystemInfo.graphicsMemorySize).Replace(' ', '_'));
                int spend;
                this.MetricsTrackingService.TrackingData.tracking_stats.TryGetValue((int)TrackingStat.Total_IAP_USD, out spend);
                if (spend > 0)
                {
                    //ZendeskTicket.Tags.Add("spend");
                    if (spend > 0 && spend <= 10)
                    {
                        ZendeskTicket.Tags.Add("spend10");
                    }
                    else if (spend > 10 && spend <= 20)
                    {
                        ZendeskTicket.Tags.Add("spend20");
                    }
                    else if (spend > 20 && spend <= 50)
                    {
                        ZendeskTicket.Tags.Add("spend50");
                    }
                    else if (spend > 50 && spend <= 100)
                    {
                        ZendeskTicket.Tags.Add("spend100");
                    }
                    else if (spend > 100 && spend <= 500)
                    {
                        ZendeskTicket.Tags.Add("spend500");
                    }
                    else if (spend > 500 && spend <= 1000)
                    {
                        ZendeskTicket.Tags.Add("spend1000");
                    }
                    else if (spend > 1000)
                    {
                        ZendeskTicket.Tags.Add("spend1000+");
                    }
                }
                //var ticketResponse = CreateTicket(ticket);
                //Timer.Instance.StartCoroutine(AttachScreenShot(ZendeskTicket));


                UploadLogFile(ZendeskTicket);
                /*switch (ReportType)
                {
                    case ReportType.ReportBug:
                        UploadGameLog(ZendeskTicket);
                        break;
                    case ReportType.ReportPlayerGlobal:
                    case ReportType.ReportPlayerPrivate:
                        UploadChatLog(ZendeskTicket);
                        break;
                }*/
                CreateTicket(ZendeskTicket);
                // pop up a "thanks" message
                this.Release();
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);

            }
        }

        public const string SCREENSHOT_NAME = "bug_screenshot.png";
        public const string LOG_NAME = "bug_log.txt";

        public IEnumerator AttachScreenShot(ZendeskTicket ticket)
        {
            if (ZendeskTicket.Happiness < 0)
            {
                Camera cam = Camera.main;
                Texture2D image = new Texture2D(Screen.width, Screen.height);

                RenderTexture currentRT = RenderTexture.active;

                RenderTexture.active = cam.targetTexture;
                cam.Render();

                yield return new WaitForEndOfFrame();

                var photoRect = new Rect(0, 0, Screen.width, Screen.height);
                image.ReadPixels(photoRect, 0, 0);

                //Resize the image. Useful if you don't need a 1:1 screenshot.
                //4 is just used as an example. You could use 10 to resize it
                //to a tenth of the original scale or whatever floats your boat.
                float scale = 800 / Screen.width;
                TextureScale.Bilinear(image, (int)(image.width / scale), (int)(image.height / scale));

                image.Apply();
                RenderTexture.active = currentRT;
                
                //Save it as PNG, but it could easily be changed to JPG
                byte[] screenshotBytes = image.EncodeToJPG();
                
                ZenFile file = new ZenFile { ContentType = "image/png", FileName = "screenShot", FileData = screenshotBytes };
                Upload upload = UploadAttachment(file, null);
                if(ticket.Uploads == null)
                    ticket.Uploads = new List<string>();
                if(upload != null)
                    ticket.Uploads.Add(upload.Token);
            }

            UploadLogFile(ticket);
            /*switch(ReportType)
            {
                case ReportType.ReportBug:
                    UploadGameLog(ticket);
                    break;
                case ReportType.ReportPlayerGlobal:
                case ReportType.ReportPlayerPrivate:
                    UploadChatLog(ticket);
                    break;
            }*/
            CreateTicket(ticket);
            // pop up a "thanks" message
            this.Release();
        }

        public void UploadLogFile(ZendeskTicket ticket)
        {
            if (ZendeskTicket.Happiness < 0)
            {
                var logList = this.MetricsTrackingService.LongLogQueue.ToList();
                string logFile = JsonConvert.SerializeObject(logList, Formatting.Indented);
                ticket.Description = string.Format("{0}\n\nUnity Log:\n{1}", ticket.Description, logFile);


                /*var bytes = Encoding.UTF8.GetBytes(logFile);

                ZenFile file = new ZenFile { ContentType = "text/plain", FileName = "UnityLog.txt", FileData = bytes };
                Upload upload = UploadAttachment(file, null);
                if (ticket.Uploads == null)
                    ticket.Uploads = new List<string>();
                if (upload != null)
                    ticket.Uploads.Add(upload.Token);*/
            }
        }

        public void UploadChatLog(ZendeskTicket ticket)
        {
            if (ZendeskTicket.Happiness < 0)
            {
                if (ChatList != null)
                {
                    StringBuilder builder = new StringBuilder();
                    for(int i = 0; i < ChatList.Count; i++)
                    {
                        var msg = ChatList[i];
                        builder.Append(msg.SentTimeUTC.ToShortDateString()).Append(' ').Append(msg.SentTimeUTC.ToShortTimeString()).Append(' ');
                        builder.Append(msg.SenderDisplayName).Append(':').Append(' ').Append(msg.Message);
                        builder.AppendLine();
                    }
                    var bytes = Encoding.UTF8.GetBytes(builder.ToString());
                    ZenFile file = new ZenFile { ContentType = "text/plain", FileName = "ChatLog.txt", FileData = bytes };
                    Upload upload = UploadAttachment(file, null);
                    if (ticket.Uploads == null)
                        ticket.Uploads = new List<string>();
                    if (upload != null)
                        ticket.Uploads.Add(upload.Token);
                }
            }
            //var resp = ticketResponse.Ticket;
        }

        public void UploadGameLog(ZendeskTicket ticket)
        {
            if(ZendeskTicket.Happiness < 0)
            {
                var log = this.PlayerProfile.GameLogs.LastOrDefault();
                if(log != null)
                {
                    string logJson = JsonConvert.SerializeObject(log, Formatting.Indented);
                    var bytes = Encoding.UTF8.GetBytes(logJson);
                    ZenFile file = new ZenFile { ContentType = "text/plain", FileName = "GameLog.txt", FileData = bytes };
                    Upload upload = UploadAttachment(file, null);
                    if (ticket.Uploads == null)
                        ticket.Uploads = new List<string>();
                    if (upload != null)
                        ticket.Uploads.Add(upload.Token);
                }
            }
            //var resp = ticketResponse.Ticket;
        }

        private bool CreateTicket(ZendeskTicket ticket)
        {
            var supportRequest = new Messages.SupportTicketRequest
            {
                AttachmentIds = ticket.Uploads,
                Subject = ticket.Subject,
                Description = ticket.Description,
                EmailAddress = ticket.EmailAddress,
                Tags = ticket.Tags,
            };
            this.ClientService.SendMessage(supportRequest);
            // create & send message to the server
            return false;
        }


        /// <summary>
        /// Uploads a file to zendesk and returns the corresponding token id.
        /// To upload another file to an existing token just pass in the existing token.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="token"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>       
        Upload UploadAttachment(ZenFile file, string token, int? timeout = null)
        {
            var resource = string.Format("uploads.json?filename={0}", file.FileName);
            if (!string.IsNullOrEmpty(token))
            {
                resource += string.Format("&token={0}", token);
            }
            var requestResult = RunRequest<UploadResult>(resource, RequestMethod.Post, file, timeout);
            return requestResult != null ? requestResult.Upload : null;
        }
        
        protected T GenericPost<T>(string resource, object body = null)
        {
            var res = RunRequest<T>(resource, RequestMethod.Post, body);
            return res;
        }
        
        public T RunRequest<T>(string resource, string requestMethod, object body = null, int? timeout = null, string formKey = null)
        {
            var response = RunRequest(resource, requestMethod, body, timeout, formKey);
            var obj = JsonConvert.DeserializeObject<T>(response.Content, jsonSettings);
            return obj;
        }
        
        public RequestResult RunRequest(string resource, string requestMethod, object body = null, int? timeout = null, string formKey = null)
        {
            try
            {
                string requestUrl = ZendeskUrl + resource;

                HttpWebRequest req = WebRequest.Create(requestUrl) as HttpWebRequest;


                req.Headers["Authorization"] = GetPasswordOrTokenAuthHeader();
                req.PreAuthenticate = true;

                req.Method = requestMethod; //GET POST PUT DELETE
                req.Accept = "application/json, application/xml, text/json, text/x-json, text/javascript, text/xml";
                req.Timeout = timeout ?? req.Timeout;

                if (body != null)
                {
                    byte[] data = null;
                    var zenFile = body as ZenFile;
                    if (zenFile == null)
                    {
                        req.ContentType = "application/json";
                        data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(body, jsonSettings));
                    }
                    else
                    {
                        req.ContentType = zenFile.ContentType;
                        data = String.IsNullOrEmpty(formKey) ? zenFile.FileData : GetFromData(zenFile, req, formKey);
                    }

                    req.ContentLength = data.Length;

                    using (var dataStream = req.GetRequestStream())
                    {
                        dataStream.Write(data, 0, data.Length);
                    }
                }

                var res = req.GetResponse();
                var response = res as HttpWebResponse;
                string responseFromServer = string.Empty;
                using (var responseStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseFromServer = reader.ReadToEnd();
                    }
                }

                return new RequestResult { Content = responseFromServer, HttpStatusCode = response.StatusCode };
            }
            catch (WebException ex)
            {
                WebException wException = GetWebException(resource, body, ex);
                MetricsTrackingService.SavedException = wException.ToString();
                Debug.Log("Zendesk Ticket Web Exception");
                return new RequestResult { Content = string.Empty, HttpStatusCode = HttpStatusCode.BadRequest };
            }
        }

        JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            DateParseHandling = DateParseHandling.DateTimeOffset,
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate

        };


        protected string GetPasswordOrTokenAuthHeader()
        {
            if (string.IsNullOrEmpty(this.ZendeskTicket.EmailAddress))
                return null;
            else
                return GetAuthHeader(this.ZendeskTicket.EmailAddress + "/token", ApiToken);
        }

        protected string GetAuthHeader(string userName, string password)
        {
            string auth = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", userName, password)));
            return string.Format("Basic {0}", auth);
        }

        private byte[] GetFromData(ZenFile zenFile, HttpWebRequest req, string formKey)
        {
            string boundaryString = "FEF3F395A90B452BB8BFDC878DDBD152";
            req.ContentType = "multipart/form-data; boundary=" + boundaryString;
            MemoryStream postDataStream = new MemoryStream();
            StreamWriter postDataWriter = new StreamWriter(postDataStream);

            // Include the file in the post data
            postDataWriter.Write("\r\n--" + boundaryString + "\r\n");
            postDataWriter.Write("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n",
                                    formKey, zenFile.FileName, zenFile.ContentType);
            postDataWriter.Flush();
            postDataStream.Write(zenFile.FileData, 0, zenFile.FileData.Length);
            postDataWriter.Write("\r\n--" + boundaryString + "--\r\n");
            postDataWriter.Flush();

            return postDataStream.ToArray();
        }


        private WebException GetWebException(string resource, object body, WebException originalWebException)
        {
            string error = string.Empty;
            WebException innerException = originalWebException.InnerException as WebException;

            if (originalWebException.Response != null || (innerException != null && innerException.Response != null))
            {
                using (Stream stream = (originalWebException.Response ?? innerException.Response).GetResponseStream())
                {
                    if (stream != null && stream.CanRead)
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            error = sr.ReadToEnd();
                        }
                    }
                    else
                    {
                        error = "Cannot read error stream.";
                    }
                }
            }
            Debug.LogException(originalWebException);
            Debug.LogErrorFormat(originalWebException.Message + " " + error);

            string headersMessage = string.Format("Error content: {0} \r\n Resource String: {1}  + \r\n", error, resource);
            string bodyMessage = string.Empty;

            if (body != null)
            {
                ZenFile zenFile = body as ZenFile;
                if (zenFile == null)
                {
                    bodyMessage = string.Format(" Body: {0}", JsonConvert.SerializeObject(body, Formatting.Indented, jsonSettings));
                }
                else
                {
                    bodyMessage = string.Format(" File Name: {0} \r\n File Length: {1}\r\n", zenFile.FileName,
                        (zenFile.FileData != null ? zenFile.FileData.Length.ToString() : "No Data"));
                }
            }

            headersMessage += bodyMessage;

            if (originalWebException.Response != null && originalWebException.Response.Headers != null)
            {
                headersMessage += originalWebException.Response.Headers;
            }

            var wException = new WebException(originalWebException.Message + headersMessage, originalWebException);
            wException.Data.Add("jsonException", error);

            return wException;
        }

    }
}
#region zendeskItems
namespace Zendesk
{ 

    public class ZenFile
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
    }
    public class RequestResult
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public string Content { get; set; }
    }
    
    public class Requester
    {
        /// <summary>
        /// Optional
        /// See ZendeskApi.LocaleValue for more info
        /// </summary>
        [JsonProperty("locale_id")]
        public long? LocaleId { get; set; }

        /// <summary>
        /// If the email already exists in the system this is optional
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }
    }
    

    public class Attachment
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("article_id")]
        public long ArticleId { get; set; }

        [JsonProperty("file_name")]
        public string FileName { get; set; }

        [JsonProperty("content_url")]
        public string ContentUrl { get; set; }

        [JsonProperty("content_type")]
        public string ContentType { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("inline")]
        public bool Inline { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("thumbnails")]
        public IList<Thumbnail> Thumbnails { get; set; }
    }

    public static class RequestMethod
    {
        public const string Get = "GET";
        public const string Put = "PUT";
        public const string Post = "POST";
        public const string Delete = "DELETE";
    }
    
    public class Thumbnail
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("file_name")]
        public string FileName { get; set; }

        [JsonProperty("content_url")]
        public string ContentUrl { get; set; }

        [JsonProperty("content_type")]
        public string ContentType { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }
    }

    public class Upload
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        //[JsonProperty("attachments")]
        //public IList<Attachment> Attachments { get; set; }
    }

    public class UploadResult
    {
        [JsonProperty("upload")]
        public Upload Upload { get; set; }
    }
#endregion
}
