﻿using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using System;
using System.IO;
using Newtonsoft.Json;
using LinqTools;

namespace NovaBlitz.UI
{
    public class SaveChatHistorySignal : Signal { }
    public class SaveChatHistoryCommand : Command
    {
        [Inject] public PlayerProfile PlayerProfile { get; set; }

        public override void Execute()
        {
            if (!this.PlayerProfile.isChatLogNeedsSaving)
                return;
            try
            {
                var LogLimitIsOneWeek = DateTime.Now.AddDays(-7);
                // Trim the chat logs to only friends
                foreach(var key in this.PlayerProfile.ChatLogs.Keys.ToList())
                {
                    if(!this.PlayerProfile.FriendListData.ContainsKey(key) 
                        || this.PlayerProfile.ChatLogs[key] == null 
                        || this.PlayerProfile.ChatLogs[key].Count == 0 
                        || this.PlayerProfile.ChatLogs[key].Last().SentTimeLocal < LogLimitIsOneWeek)
                    {
                        Debug.LogFormat("Chat Log {0} removed. Reason: {1}", key, !this.PlayerProfile.FriendListData.ContainsKey(key) 
                            ? "Not friends" 
                            : (this.PlayerProfile.ChatLogs[key] == null || this.PlayerProfile.ChatLogs[key].Count == 0)
                                ? "No messages" : "Too old");
                        this.PlayerProfile.ChatLogs.Remove(key);
                    }
                }
                Debug.LogFormat("Saving {0} chat logs", this.PlayerProfile.ChatLogs.Count);
                var chatLogs = EngineUtils.Zip(JsonConvert.SerializeObject(this.PlayerProfile.ChatLogs));
                File.WriteAllBytes(Path.Combine(Application.persistentDataPath, Path.Combine(Application.persistentDataPath, this.PlayerProfile.PlayFabId + NovaConfig.CHAT_LOG_FILE_NAME)), chatLogs);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            this.PlayerProfile.isChatLogNeedsSaving = false;
        }
    }
}