﻿using Messages;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using PlayFab.ClientModels;
using NovaBlitz.UI.DataContracts;

namespace NovaBlitz.UI
{
    public class ManageFriendResponseSignal : Signal<ManageFriendResponse> { }
    public class ManageFriendResponseCommand : Command
    {
        // signal parameters
        [Inject] public ManageFriendResponse Response { get; set; }

        
        [Inject] public FriendNotificationSignal FriendNotificationSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GameData GameData { get; set; }
        //[Inject] public ManagePlayFabFriendListSignal ManagePlayFabFriendListSignal { get; set; }
        //[Inject] public LoadFriendsListSignal LoadFriendsListSignal { get; set; }
        [Inject] public AddFriendResponseSignal AddFriendResponseSignal { get; set; }
        [Inject] public DeclineFriendResponseSignal DeclineFriendResponseSignal { get; set; }
        [Inject] public ConfirmFriendResponseSignal ConfirmFriendResponseSignal { get; set; }
		[Inject] public RemoveFriendResponseSignal RemoveFriendResponseSignal{get;set;}
		[Inject] public BlockFriendResponseSignal BlockFriendResponseSignal {get;set;}
		[Inject] public UnblockFriendResponseSignal UnblockFriendResponseSignal { get;set;}
		[Inject] public PlayFabChatClient PlayFabChatClient {get;set;}
		[Inject] public ConnectToPhotonChatSignal ConnectToPhotonChatSignal {get;set;}

        private const string SELECTED_AVATAR = "selected_avatar";

        public override void Execute()
        {
            if (Response == null)
                return;
            //Debug.LogErrorFormat("#FRIENDS# Manage friend response. {0}:{1}. Requestor:{2} Requested:{3}", Response.RequestType, Response.RequestStatus,Response.RequestorPlayFabId, Response.RequestedPlayFabId);

            switch(Response.RequestType)
            {
                case RequestType.AddRequest:
                    if (Response.RequestStatus == RequestStatus.Success)
                    {
                        //Debug.LogError("#FRIENDS# Add request success. Getting player info");
                        Retain();
                        // get info for the player
                        // create the friend list result
                        PlayFab.PlayFabClientAPI.GetPlayerCombinedInfo(new GetPlayerCombinedInfoRequest
                        {
                            PlayFabId = Response.RequestedPlayFabId != this.PlayerProfile.MetaProfile.PlayFabID ? Response.RequestedPlayFabId : Response.RequestorPlayFabId,
                            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
                            {
                                PlayerStatisticNames = new System.Collections.Generic.List<string> { SELECTED_AVATAR },
                                GetPlayerStatistics = true,
                                GetUserAccountInfo = true,
                                GetTitleData = true,
                            }
                        }, OnGetCombinedInfo, OnPlayFabError);
                    }
                    else
                    {
                        Debug.LogError("#FRIENDS# Add Request Failed");
                        this.AddFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, false);
                    }
                    break;
                case RequestType.ConfirmRequest:
                    // doesn't matter whether this is your confirm or their confirm - add the friend just the same
                    // Need to add the player to your friends list.

                    if (Response.RequestStatus == RequestStatus.Success)
                    {
                        string playFabId = (Response.RequestedPlayFabId == this.PlayerProfile.MetaProfile.PlayFabID) ? Response.RequestorPlayFabId : Response.RequestedPlayFabId;

                        var friend = this.PlayerProfile.GetFriend(playFabId);
                        if (friend != null && friend.FriendData.IsRequest || friend.FriendData.IsPending)
                        {
                            //Debug.LogError("#FRIENDS# Confirm Request success");
                            friend.FriendData.IsRequest = false;
                            friend.FriendData.IsPending = false;
                            //var add = new AddFriendRequest() { FriendPlayFabId = playFabId };
                            //ManagePlayFabFriendListSignal.Dispatch(add, new RemoveFriendRequest());
                            this.ConfirmFriendResponseSignal.Dispatch(playFabId, true);
                        }
                        if (friend == null)
                        {
                            Debug.LogError("#FRIENDS# Confirm Request Failed - no friend to confirm");
                            this.ConfirmFriendResponseSignal.Dispatch(playFabId, false);
                        }
                        else
                        {
                            //Debug.LogError("#FRIENDS# Confirm Request already confirmed");
                            this.ConfirmFriendResponseSignal.Dispatch(playFabId, true);
                        }
                        this.ConnectToPhotonChatSignal.Dispatch(string.Empty, new ChatMessage(),false);
                    }
                    else
                    {
                        Debug.LogError("#FRIENDS# Confirm Request Failed");
                        this.ConfirmFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, false);
                    }
                    break;
                case RequestType.RemoveRequest:
                case RequestType.DeclineRequest:
                    if (Response.RequestStatus == RequestStatus.Success)
                    {
                        string playFabId = (Response.RequestedPlayFabId == this.PlayerProfile.MetaProfile.PlayFabID) ? Response.RequestorPlayFabId : Response.RequestedPlayFabId;

                        if (!string.IsNullOrEmpty(playFabId))
                        {

                            var friend = this.PlayerProfile.GetFriend(playFabId);
                            if (friend != null && friend.FriendData != null && (friend.FriendData.IsRequest || friend.FriendData.IsPending))
                            {
                                //Debug.LogError("#FRIENDS# Cleaning up friend data");
                                friend.FriendData.IsRequest = false;
                                friend.FriendData.IsPending = false;
                                friend.IsBlocked = false;
                            }
                            PlayerProfile.FriendListData.Remove(playFabId);
                            if (Response.RequestType == RequestType.RemoveRequest)
                            {
                                //Debug.LogError("#FRIENDS# Remove Request success");
                                this.RemoveFriendResponseSignal.Dispatch(playFabId, true);
                            }
                            else
                            {
                                //Debug.LogError("#FRIENDS# Decline Request success");
                                this.DeclineFriendResponseSignal.Dispatch(playFabId, true);
                            }
                        }
                        this.ConnectToPhotonChatSignal.Dispatch(string.Empty, new ChatMessage(),false);
                    }
                    else
                    {
                        Debug.Log("Decline/Remove Request Failed");
                        if (Response.RequestType == RequestType.RemoveRequest)
                            this.RemoveFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, false);
                        else
                            this.DeclineFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, false);
                    }
                    break;
                case RequestType.BlockRequest:
                    if (Response.RequestStatus == RequestStatus.Success)
                    {
                        string playFabId = (Response.RequestedPlayFabId == this.PlayerProfile.MetaProfile.PlayFabID) ? Response.RequestorPlayFabId : Response.RequestedPlayFabId;
                        if (PlayerProfile.BlockList.Add(playFabId))
                        {
                            //Debug.Log("Block Request success");
                            var friend = this.PlayerProfile.GetFriend(playFabId);
                            if (friend != null)
                            {
                                friend.IsBlocked = true;
                            }
                            this.UnblockFriendResponseSignal.Dispatch(playFabId, true);
                        }
                        else
                        {
                            Debug.Log("Block Request Failed - already on list");
                            this.UnblockFriendResponseSignal.Dispatch(playFabId, false);
                        }
                    }
                    else
                    {
                        Debug.Log("Block Request Failed");
                        this.BlockFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
                    }
                    break;
                case RequestType.UnblockRequest:
                    if (Response.RequestStatus == RequestStatus.Success)
                    {
                        string playFabId = (Response.RequestedPlayFabId == this.PlayerProfile.MetaProfile.PlayFabID) ? Response.RequestorPlayFabId : Response.RequestedPlayFabId;
                        if (PlayerProfile.BlockList.Remove(playFabId))
                        {
                            //Debug.Log("Unblock Request success");
                            var friend = this.PlayerProfile.GetFriend(playFabId);
                            if (friend != null)
                            {
                                friend.IsBlocked = false;
                            }
                            this.UnblockFriendResponseSignal.Dispatch(playFabId, true);
                        }
                        else
                        {
                            Debug.Log("Unlock Request Failed - already off list");
                            this.UnblockFriendResponseSignal.Dispatch(playFabId, false);
                        }
                    }
                    else
                    {
                        Debug.Log("Unlock Request Failed");
                        this.UnblockFriendResponseSignal.Dispatch(this.PlayerProfile.MetaProfile.PlayFabID, false);
                    }
                    break;
                case RequestType.BlockedByFriend:
                    if (Response.RequestStatus == RequestStatus.Success)
                    {
                        string playFabId = (Response.RequestedPlayFabId == this.PlayerProfile.MetaProfile.PlayFabID) ? Response.RequestorPlayFabId : Response.RequestedPlayFabId;

                        if (!string.IsNullOrEmpty(playFabId))
                        {
                            var friend = this.PlayerProfile.GetFriend(playFabId);
                            if (friend != null)
                            {
                                //Debug.Log("Blocked by friend - success");
                                friend.FriendData.HasBlockedYou = true;
                                this.UnblockFriendResponseSignal.Dispatch(playFabId,true);
                            }
                            else
                                Debug.Log("Blocked by friend - no friend in list");
                        }
                        else
                            Debug.Log("Blocked by friend - something failed");
                    }
                    break;
                case RequestType.UnblockedByFriend:
                    if (Response.RequestStatus == RequestStatus.Success)
                    {
                        string playFabId = (Response.RequestedPlayFabId == this.PlayerProfile.MetaProfile.PlayFabID) ? Response.RequestorPlayFabId : Response.RequestedPlayFabId;
                        
                        if (!string.IsNullOrEmpty(playFabId))
                        {
                            var friend = this.PlayerProfile.GetFriend(playFabId);
                            if (friend != null)
                            {
                                //Debug.Log("Unlocked by friend - success");
                                friend.FriendData.HasBlockedYou = false;
                                this.UnblockFriendResponseSignal.Dispatch(playFabId,true);
                            }
                            else
                                Debug.Log("Unlocked by friend - no friend in list");
                        }
                        else
                            Debug.Log("Unlocked by friend - something failed");
                    }
                    break;
                default:
                    break;
            }
        }

        private void OnGetCombinedInfo(GetPlayerCombinedInfoResult result)
        {
            if (result.InfoResultPayload == null)
            {
                Debug.LogErrorFormat("#FRIENDS# Account info for Add:{0}:{1}. is NULL", result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName, result.PlayFabId);
                this.AddFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, false);
            }
            else
            {
                bool isRequestFromFriend = Response.RequestedPlayFabId == PlayerProfile.MetaProfile.PlayFabID;
                if (!isRequestFromFriend)
                    PlayerProfile.BlockList.Remove(Response.RequestedPlayFabId);
                //Debug.LogErrorFormat("#FRIENDS# Account info for Add:{0}:{1}.", result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName, result.PlayFabId);
                if (result.InfoResultPayload.AccountInfo != null && result.InfoResultPayload.AccountInfo.TitleInfo != null)
                {
                    var facebookId = result.InfoResultPayload.AccountInfo.FacebookInfo != null ? result.InfoResultPayload.AccountInfo.FacebookInfo.FacebookId : null;
                    var facebookRealName = result.InfoResultPayload.AccountInfo.FacebookInfo != null ? result.InfoResultPayload.AccountInfo.FacebookInfo.FullName : null;
                    var steamId = result.InfoResultPayload.AccountInfo.SteamInfo != null ? result.InfoResultPayload.AccountInfo.SteamInfo.SteamId : null;
                    var friend = new NovaBlitzFriend(new NovaFriend
                    {
                        PlayFabId = result.InfoResultPayload.AccountInfo.PlayFabId,
                        DisplayName = result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName,
                        FacebookId = facebookId,
                        FacebookRealName = facebookRealName,
                        IsFacebookFriend = result.InfoResultPayload.AccountInfo.FacebookInfo != null && !string.IsNullOrEmpty(facebookId),
                        IsSteamFriend = result.InfoResultPayload.AccountInfo.SteamInfo != null && !string.IsNullOrEmpty(steamId),
                        IsRequest = isRequestFromFriend,
                        IsPending = !isRequestFromFriend,
                    }, 0, GameData.DefaultAvatarArtId);
                    if (result.InfoResultPayload.PlayerStatistics == null)
                    {
                        var avatarresult = result.InfoResultPayload.PlayerStatistics.FirstOrDefault(st => st.StatisticName == SELECTED_AVATAR);
                        if (avatarresult != null)
                        {
                            friend.AvatarId = avatarresult.Value;
                            AvatarDataContract avatar;
                            if (GameData.AvatarDictionary.TryGetValue(friend.AvatarId, out avatar) && !string.IsNullOrEmpty(avatar.ArtId))
                                friend.AvatarArtId = avatar.ArtId;
                        }
                    }

                    PlayerProfile.FriendListData[friend.FriendData.PlayFabId] = friend;

                    Debug.LogErrorFormat("#FRIENDS# Add successful:{0}:{1}.", result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName, result.PlayFabId);
                    this.AddFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, true);
                    this.ConnectToPhotonChatSignal.Dispatch(string.Empty, new ChatMessage(),false);
                }
                else
                {
                    Debug.LogErrorFormat("#FRIENDS# Add failed:{0}:{1}.", result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName, result.PlayFabId);
                    this.AddFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, false);
                }
            }
            
            this.Release();
        }

        private void OnPlayFabError(PlayFab.PlayFabError error)
        {
            this.AddFriendResponseSignal.Dispatch(Response.RequestedPlayFabId, false);
            this.Release();
        }
    }
}
