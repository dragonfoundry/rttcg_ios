﻿using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
    public class BackupMetricsDataSignal : Signal<string> { }
    public class BackupMetricsDataCommand : Command
    {
        [Inject] public string MetricsData { get; set; }
        
        public override void Execute()
        {
            if (string.IsNullOrEmpty(MetricsData))
                return;
            this.Retain();
            var data = new Dictionary<string, string>{
                { NovaConfig.KEY_TRACKING_DATA_CACHE, this.MetricsData }
            };
            var req = new PlayFab.ClientModels.UpdateUserDataRequest
            {
                Data = data,
                Permission = UserDataPermission.Private
            };
            PlayFabClientAPI.UpdateUserData(req, OnUpdateUserDataResult, OnPlayFabError);
        }

        private void OnUpdateUserDataResult(UpdateUserDataResult result)
        {
            this.Release();
        }

        private void OnPlayFabError(PlayFabError pferror)
        {
            this.Release();
        }
    }
}
