using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class SaveSettingsSignal : Signal {}
	public class SaveSettingsCommand : Command
	{
		[Inject] public ClientPrefs ClientPrefs {get;set;}
		[Inject] public NovaAudio NovaAudio {get;set;}

		public override void Execute()
		{
			SaveAudio();
		}

		void SaveAudio()
		{
			Debug.Log ("#Settings#Saving Settings NovaAudio.SoundFxVolume:" + this.NovaAudio.SoundFxVolume + " NovaAudio.MusicVolume:" + this.NovaAudio.MusicVolume);
			PlayerPrefs.SetInt(NovaConfig.KEY_SFXVOL, this.NovaAudio.SoundFxVolume);
			PlayerPrefs.SetInt(NovaConfig.KEY_MUSICVOL, this.NovaAudio.MusicVolume);
		}
	}
}
