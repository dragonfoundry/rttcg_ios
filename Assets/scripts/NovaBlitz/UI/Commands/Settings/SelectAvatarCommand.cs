using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab;
using PlayFab.ClientModels;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class SelectAvatarSignal : Signal<string> {}
	public class SelectAvatarCommand : Command
	{
		[Inject] public string SignalID {get;set;}

		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ProfileUpdatedSignal ProfileUpdatedSignal {get;set;}

		public override void Execute()
		{
            // Do nothing if the avatar's the one the client already knows about.
            if (SignalID == this.PlayerProfile.MetaProfile.AvatarID)
                return;
			this.Retain();
			var data = new Dictionary<string, string>{
				{ NovaConfig.KEY_AVATAR, this.SignalID }
			};
			var req = new PlayFab.ClientModels.UpdateUserDataRequest{
				Data = data,
				Permission = UserDataPermission.Public
			};
			PlayFabClientAPI.UpdateUserData(req, OnUpdateUserDataResult, OnPlayFabError);
		}

		private void OnUpdateUserDataResult(UpdateUserDataResult result) 
		{
			this.PlayerProfile.MetaProfile.AvatarID = this.SignalID;
			this.ProfileUpdatedSignal.Dispatch(ProfileSection.Inventory);
			this.Release();
		}

		private void OnPlayFabError(PlayFabError pferror)
		{
			this.Release();
		}
	}
}
