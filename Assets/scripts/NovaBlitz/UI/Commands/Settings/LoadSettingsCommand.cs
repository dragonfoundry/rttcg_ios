using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
	public class SettingsLoadedSignal : Signal {}
	public class LoadSettingsSignal : Signal {}
	public class LoadSettingsCommand : Command
	{
		[Inject] public ClientPrefs ClientPrefs {get;set;}
		[Inject] public NovaAudio NovaAudio {get;set;}

		[Inject] public SettingsLoadedSignal SettingsLoadedSignal {get;set;}

		public override void Execute()
		{
			
			LoadAudio();
			this.SettingsLoadedSignal.Dispatch();
		}

		void LoadAudio()
		{
			this.NovaAudio.SoundFxVolume = PlayerPrefs.GetInt(NovaConfig.KEY_SFXVOL, NovaConfig.DEF_VOLUME);
			this.NovaAudio.MusicVolume = PlayerPrefs.GetInt(NovaConfig.KEY_MUSICVOL, NovaConfig.DEF_VOLUME);

			Debug.Log ("#Settings#Loading Settings NovaAudio.SoundFxVolume:" + this.NovaAudio.SoundFxVolume + " NovaAudio.MusicVolume:" + this.NovaAudio.MusicVolume);
		}
	}
}
