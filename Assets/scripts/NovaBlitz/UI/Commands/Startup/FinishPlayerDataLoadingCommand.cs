﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI.DataContracts;
using LinqTools;

namespace NovaBlitz.UI
{
	public class PlayerDataLoadedSignal : Signal {}
	public class FinishPlayerDataLoadingSignal : Signal {}
	public class FinishPlayerDataLoadingCommand : Command
	{
		[Inject] public PlayerDataLoadedSignal PlayerDataLoadedSignal {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public GameData GameData { get; set; }
		[Inject] public MetagameSettings MetagameSettings {get;set;}
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }

        [Inject] public DecksModifiedSignal DecksModifiedSignal { get; set; }
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
		[Inject] public ViewReferences ViewReferences {get;set;}
        [Inject] public UnloadUnusedResourcesSignal UnloadUnusedResourcesSignal { get; set; }

        private IEnumerator waitForServer;

		public override void Execute ()
        {
            this.Retain();
            Debug.LogFormat("Waiting To Finish Player Data Loading: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.LogoutUserSignal.AddListener(OnLogout);
            waitForServer = WaitForServerCoroutine();
            Timer.Instance.StartCoroutine(waitForServer);
		}

        public IEnumerator WaitForServerCoroutine()
        {
            // Wait for the data to come in
            float startTime = Time.realtimeSinceStartup;
            int startFrame = Time.frameCount;
            float waittime = 0;
            while(!this.PlayerProfile.IsAllDataLoaded)
            {
                Debug.LogFormat("Data not yet finished loading. Waiting for {0}s for {1} {2} {3} {4} {5} {6} {7} (realtime:{8:0.00}s) (frames:{9})", 
                    waittime,
                    PlayerProfile.isPlayerDataLoaded ? string.Empty : "PlayerData",
                    PlayerProfile.isClientCardFileLoaded ? string.Empty : "CardFile",
                    PlayerProfile.isClientDataFileLoaded ? string.Empty : "DataFile",
                    PlayerProfile.isClientTextFileLoaded ? string.Empty : "TextFile",
                    PlayerProfile.isPlayerSettingsLoaded ? string.Empty : "PlayerSettings",
                    PlayerProfile.isAllCatalogsLoaded ? string.Empty : "Catalogs",
                    PlayerProfile.isAllObjectPoolsCreated ? string.Empty : "ObjectPools",
                    Time.realtimeSinceStartup - startTime,
                    Time.frameCount - startFrame);

                yield return new WaitForSecondsRealtime(0.25f);
                waittime += 0.25f;
                if(waittime > 20.0f)
                {
                    Debug.LogFormat("Waited {0} for data. Too long. Try again.", waittime);
                    this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount,ShouldAutoReconnect.AutomaticallyReconnect);
                }
            }
            yield return null;
            Debug.LogFormat("Datafinished loading. Waited for a total of {2} seconds ({3}s realtime): {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond, waittime, Time.realtimeSinceStartup - startTime);

            if (PlayerProfile.DraftData != null && PlayerProfile.DraftData.DeckList != null && PlayerProfile.DraftData.DeckList.Count > 0)
            {
                SetDraftDeck(PlayerProfile.DraftData.DeckList);
            }
            else if (PlayerProfile.DraftProgress != null && PlayerProfile.DraftProgress.DraftedCards != null && PlayerProfile.DraftProgress.DraftedCards.Count > 0)
            {
                SetDraftDeck(PlayerProfile.DraftProgress.DraftedCards);
            }

            if (PlayerProfile.ConstructedData != null && PlayerProfile.ConstructedData.DeckList != null && PlayerProfile.ConstructedData.DeckList.Count > 0)
            {
                SetConstructedDeck(PlayerProfile.ConstructedData.DeckList);
            }
            yield return null;
            
            // Bit for dev server testing
            if(this.MetagameSettings.IsTargetingDev && GameData.LegalCards.Contains(int.MinValue))
            {
                int owned;
                foreach(var cardId in GameData.LegalCards.ToList())
                {
                    if(!PlayerProfile.OwnedCards.TryGetValue(cardId, out owned) || owned <3)
                    {
                        PlayerProfile.OwnedCards[cardId] = 3;
                    }
                }
            }

            // SET DECKS
            {
                this.PlayerProfile.DeckList.Clear();
                //this.PlayerProfile.CurrentDeck = null;
                if (PlayerProfile.DeckListDataContract != null)
                {
                    if (PlayerProfile.DeckListDataContract.Decks != null) { Debug.Log("DeckCount: " + PlayerProfile.DeckListDataContract.Decks.Count); }
                    foreach (DeckDataContract deck in PlayerProfile.DeckListDataContract.Decks)
                    {
                        // Parse the deck properties
                        DeckData newDeck = new DeckData(Messages.GameFormat.Casual) { ID = deck.Id, Name = deck.Name, DeckArt = deck.DeckArt };

                        // Parse the Cards
                        for (int deckIndex = 0; deckIndex < deck.Cards.Count; deckIndex++)
                        {
                            newDeck.AddCard(deck.Cards[deckIndex].Id, deckIndex);
                        }

                        // Determine how many aspects are present in the deck and store the results in newDeck
                        newDeck.QueryAspects();
                        newDeck.SetDeckArt();

                        this.PlayerProfile.DeckList.Add(newDeck);
                    }

                    if (PlayerProfile.OwnedCards.Count > 0 && GameData.LegalCards.Count > 0 && this.PlayerProfile.HasUnlockedAllDecks)
                    {

                        bool decksUpdated = false;
                        foreach (var deck in PlayerProfile.DeckList)
                        {
                            if (deck.TrimToInventory(PlayerProfile.OwnedCards, GameData.LegalCards))
                                decksUpdated = true;
                        }
                        if (decksUpdated)
                        {
                            this.DecksModifiedSignal.Dispatch();

                            this.ProfileUpdatedSignal.Dispatch(ProfileSection.DeckList);
                            //DeckListDataContract dataContract = this.PlayerProfile.GetDeckListDataContract();
                            //this.UpdateDeckListSignal.Dispatch(dataContract);
                            Debug.Log("Decks trimmed");
                        }
                    }
                }

                // No matter what, update the DeckList
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.DeckList);
            }
            // END SET DECKS

            yield return null;

            // set tutorial decks
            {
                if (this.GameData.TutorialDictionary != null)
                {
                    foreach(var tutorial in this.GameData.TutorialDictionary.Values)
                    {

                        // Parse the deck properties
                        DeckData newDeck = new DeckData(Messages.GameFormat.Casual) { ID = string.Format("tutorial_{0}", tutorial.TutorialId) };

                        // Parse the Cards
                        for (int deckIndex = 0; deckIndex < tutorial.Deck.Count; deckIndex++)
                        {
                            newDeck.AddCard(tutorial.Deck[deckIndex], deckIndex);
                        }

                        // Determine how many aspects are present in the deck and store the results in newDeck
                        newDeck.QueryAspects();
                        newDeck.SetDeckArt();
                        newDeck.SetPreconName();
                        newDeck.IsTutorialDeck = true;
                        newDeck.MinDeckSize = newDeck.Cards.Count;
                        //newDeck.MaxDeckSize = 60;

                        tutorial.TutorialDeck = newDeck;
                    }
                }
            }

            yield return null;
            
            // SET PRECON DECKS
            {

                GameData.PreconstructedDecks.Clear();
                if (this.GameData.PreconstructedDataContract != null)
                {
                    foreach (DeckDataContract deck in this.GameData.PreconstructedDataContract.Decks)
                    {
                        // Parse the deck properties
                        DeckData newDeck = new DeckData(Messages.GameFormat.Casual) { ID = deck.Id };

                        // Parse the Cards
                        for (int deckIndex = 0; deckIndex < deck.Cards.Count; deckIndex++)
                        {
                            newDeck.AddCard(deck.Cards[deckIndex].Id, deckIndex);
                        }

                        // Determine how many aspects are present in the deck and store the results in newDeck
                        newDeck.QueryAspects();
                        newDeck.SetDeckArt();
                        newDeck.SetPreconName();

                        GameData.PreconstructedDecks.Add(newDeck);
                    }
                }
            }

            yield return null;
            Debug.LogFormat("FinishedPlayerDataLoading: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.PlayerProfile.IsFinishedDataLoading = true;
            this.PlayerDataLoadedSignal.Dispatch();
            this.UnloadUnusedResourcesSignal.Dispatch();
            this.LogoutUserSignal.RemoveListener(OnLogout);
            /*if (!string.IsNullOrEmpty(this.PlayerProfile.MetaProfile._Name))
            {
                var loginView = (LoginDialogNewFlowView)this.ViewReferences.Get(typeof(LoginDialogNewFlowView));
                if (loginView != null)
                {
                    GameObject.Destroy(loginView.gameObject);
                }
            }*/
            this.Release();
        }

        void OnLogout(ShouldUnlinkDevice shouldUnlink, ShouldAutoReconnect shouldReconnect)
        {
            this.LogoutUserSignal.RemoveListener(OnLogout);
            Debug.LogError("Logout. Cancelling FinishPlayerDataLoading.");
            if (this.waitForServer != null)
                Timer.Instance.StopCoroutine(waitForServer);
            this.Fail();
            this.Release();
        }

        private void SetDraftDeck(List<int> deckList)
        {
            this.PlayerProfile.DraftDeck = new DeckData(Messages.GameFormat.Draft) { Name = I2.Loc.ScriptLocalization.Draft_Deck };
            for (int i = 0; i < deckList.Count; i++)
            {
                int cardID = deckList[i];
                CardData cardData = GameData.CardDictionary[cardID];
                CardItem cardItem = new CardItem(cardData, -2);
                this.PlayerProfile.DraftDeck.Cards.Add(cardItem);
            }

            this.PlayerProfile.DraftDeck.QueryAspects();
            this.PlayerProfile.DraftDeck.SetDeckArt();
            this.ProfileUpdatedSignal.Dispatch(ProfileSection.TournamentData);
        }

        private void SetConstructedDeck(List<int> deckList)
        {

            this.PlayerProfile.LeagueDeck = new DeckData(Messages.GameFormat.Constructed) { Name = I2.Loc.ScriptLocalization.League_Deck };
            for (int i = 0; i < deckList.Count; i++)
            {
                int cardID = deckList[i];
                CardData cardData = GameData.CardDictionary[cardID];
                CardItem cardItem = new CardItem(cardData, -2);
                this.PlayerProfile.LeagueDeck.Cards.Add(cardItem);
            }

            this.PlayerProfile.LeagueDeck.QueryAspects();
            this.PlayerProfile.LeagueDeck.SetDeckArt();
            this.ProfileUpdatedSignal.Dispatch(ProfileSection.TournamentData);
        }

        private void SetPreconstructedDecks()
        {
        }
    }
}
