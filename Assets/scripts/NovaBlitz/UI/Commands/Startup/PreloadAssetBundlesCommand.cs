﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;
using LinqTools;

namespace NovaBlitz.UI
{
    public class PreloadAssetBundlesCommand : Command
    {
        [Inject] public NovaAudio NovaAudio {get;set; }
        [Inject] public AssetBundleManager AssetBundleManager { get; set; }
        [Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
        public override void Execute()
        {
            Debug.Log("Pre-loading Asset Bundles");
            Application.backgroundLoadingPriority = ThreadPriority.Normal;
            //Timer.Instance.StartCoroutine(LoadNovaAudioBundle());
            NovaAudio.AssetBundleManager = AssetBundleManager;
            AssetBundleManager.PreloadIncludedAssetBundles();
        }
        /*
        IEnumerator LoadNovaAudioBundle()
        {
            //yield return new WaitForSecondsRealtime(0.5f);
            //this.AsyncProgressSignal.Dispatch(AsyncProgress.LoadingAudio, 0.5f);
            yield return null;
            string path = Application.streamingAssetsPath;
            path += path.EndsWith("/") ? string.Empty : "/";
			path += "novaaudio.unity3d";

            var assetBundle = AssetBundle.LoadFromFile(path);
            //this.NovaAudio.AssetBundle = assetBundle;

            Application.backgroundLoadingPriority = ThreadPriority.Normal;

            this.AsyncProgressSignal.Dispatch(AsyncProgress.LoadingAudio, 0.6f);
            foreach(NovaAudioGroup audioGroup in this.NovaAudio.AudioGroups)
            {
                if(audioGroup.IsPreloaded)
                {
                    foreach(NovaAudioItem audioItem in audioGroup.AudioItems)
                    {
                        yield return this.NovaAudio.PreloadAudioSource(audioItem);
                    }
                }
                if(audioGroup.Label == "music_theme")
                {
                    NovaAudio.MusicTheme = audioGroup;
                }
                if (audioGroup.Label == "music_menu")
                {
                    NovaAudio.MusicMenu = audioGroup;
                }
            }
            this.AsyncProgressSignal.Dispatch(AsyncProgress.LoadingAudio,1.0f);
            
			Debug.Log ("Nova Audio Initialized");
            this.NovaAudio.IsInitialized = true;
        }*/
    }
}