﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;

namespace NovaBlitz.UI
{
	public class InitializeClientPrefsSignal : Signal {}
	public class InitializeClientPrefsCommand : Command
	{
		[Inject] public ClientPrefs ClientPrefs {get;set;}

		public override void Execute ()
		{
			Debug.Log ("Loading Client Prefs");
			this.ClientPrefs.Load();
		}
	}
}
