﻿using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using Messages;
using System;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using LinqTools;
using Newtonsoft.Json;
using NovaBlitz.UI.DataContracts;
using NovaBlitz.Economy;
using NovaBlitz.Game;
using PlayFab;
using PlayFab.Internal;

namespace NovaBlitz.UI
{
    public enum DataFileType
    {
        NoType,
        CardFile,
        DataFile,
        TextFile,
    }

    public class LoadDataFileSignal : Signal<string, DataFileType> { }
    public class LoadDataFileCommand : Command
    {
        [Inject] public string DataFileName { get; set; }
        [Inject] public DataFileType DataFileType { get; set; }

        
		[Inject] public TitleDataLoadedSignal TitleDataLoadedSignal { get; set; }
        //[Inject] public LoadFullCatalogSignal LoadFullCatalogSignal { get; set; }

        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		[Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public LanguageChangedSignal LanguageChangedSignal { get; set; }
        [Inject] public NovaIAPService NovaIAPService { get; set; }

        private const string leagueUnlockedKey = "league_unlocked";
        private const string eventsUnlockedKey = "events_unlocked";
        private const string leagueRankKey = "league_rank_threshold";
        private const string eventsRankKey = "events_rank_threshold";
        private const string globalChatShardKey = "global_chat_shards";
        private const string clientCardFileKey = "card_file_client_test";    // Key value is used by PlayFab GetContentDownloadUrlS
        private const string catalogVersionKey = "catalog_version";
        private const string preconstructedDeckKey = "preconstructed_decks";
        private const string aiDeckKey = "client_ai_decks";
        private const string ratingRulesKey = "rating_rules";
        private const string rankThresholdsKey = "rank_thresholds";
        private const string achievementDataKey = "achievement_data";
        private const string tutorialDataKey = "tutorial_client";
        private const string avatarDataKey = "avatar_data";
        private const string cardBackDataKey = "cardback_data";
        private const string localizationDataKey = "localization_file";
        private const string tutorialSpecialAttackKey = "tutorial_special_attacks";
        //private const string eventScheduleKey = "event_schedule";
        //private const string eventTypesKey = "event_types";

        private const string PLAYERPREFS_CURRENT_DATA_FILE_NAME = "CurrentDataFileName";
        private const string PLAYERPREFS_CURRENT_CARD_FILE_NAME = "CurrentCardFileName";
        private const string PLAYERPREFS_CURRENT_TEXT_FILE_NAME = "CurrentTextFileName";
        

        public override void Execute()
        {
            this.Retain();


            /*try
            {

                PrivateHttp.DownloadBytes("http://store.steampowered.com/app/388370/Nova_Blitz/?snr=new_account_created", (r, e) => { this.OnGetBytes(r, e); });
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }*/

            // Check to see if there is a new file to be downloaded
            string path = Path.Combine(Application.persistentDataPath, DataFileName);
            if (File.Exists(path))
            {
                try
                {
                    Debug.LogFormat("Loading file from disk {0}: {1}:{2}", DataFileType, DateTime.Now.ToLongTimeString(), DateTime.Now.Millisecond);
                    // Read the local file and insert card data into the title data dictionary (replaces card data file name), to be parsed later
                    var dataString = File.ReadAllText(path);
                    OnGetFileString(dataString, true);
                }
                catch
                {
                    GetContentDownloadUrl();
                }
            }
            else
            {
                GetContentDownloadUrl();
            }
        }

        private void GetContentDownloadUrl()
        {
            Debug.LogFormat("Started loading file {0}: {1}:{2}", DataFileType, DateTime.Now.ToLongTimeString(), DateTime.Now.Millisecond);
            // Use the new filename as the key to requesting the url for the client card data file
            GetContentDownloadUrlRequest request = new GetContentDownloadUrlRequest();
            request.Key = DataFileName;
            request.HttpMethod = "GET";
            request.ThruCDN = true;
            PlayFabClientAPI.GetContentDownloadUrl(request, (r) => { this.OnGetFileUrl(r); }, OnPlayFabError);
        }
                
        private void OnGetFileUrl(GetContentDownloadUrlResult result)
        {
            try
            {
                // Download new card data from PlayFab storage
                //PrivateHttp.DownloadString(result.URL, (r, e) => { this.OnGetFile(r, e); });
                PrivateHttp.DownloadBytes(result.URL, (r, e) => { this.OnGetBytes(r, e); });
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
                TitleDataParseFailed(DisconnectCode.DataParseFailed);
            }
        }

        private void OnGetBytes(byte[] bytes, string error)
        {
            if (!String.IsNullOrEmpty(error))
            {
                Debug.LogError("Web request error on card file: " + error);
                TitleDataParseFailed(DisconnectCode.DataParseFailed);
                this.Release();
            }
            else if (bytes != null && bytes.Length > 0)
            {
                try
                {
                    string response = EngineUtils.Unzip(bytes);
                    string filePath;
                    switch (DataFileType)
                    {
                        case DataFileType.CardFile:
                            filePath = PLAYERPREFS_CURRENT_CARD_FILE_NAME;
                            break;
                        case DataFileType.DataFile:
                            filePath = PLAYERPREFS_CURRENT_DATA_FILE_NAME;
                            break;
                        case DataFileType.TextFile:
                            filePath = PLAYERPREFS_CURRENT_TEXT_FILE_NAME;
                            break;
                        default:
                            Debug.LogError("Title Data not found or not readable: Cardfile");
                            TitleDataParseFailed(DisconnectCode.DataParseFailed);
                            this.Release();
                            return;
                    }
                    // Save the card data file locally and delete the old file
                    File.WriteAllText(Path.Combine(Application.persistentDataPath, DataFileName), response);
                    // Delete the previous cached card file name
                    string currentFileName = PlayerPrefs.GetString(filePath, string.Empty);
                    if (!String.IsNullOrEmpty(currentFileName)
                        && currentFileName != DataFileName)
                    {
                        File.Delete(Path.Combine(Application.persistentDataPath, currentFileName));
                    }

                    // Update current card file name
                    PlayerPrefs.SetString(filePath, DataFileName);

                    OnGetFileString(response,false);
                }
                catch
                {
                    Debug.LogError("Title Data unzip exception");
                    TitleDataParseFailed(DisconnectCode.DataParseFailed);
                    this.Release();
                }
            }
            else
            {
                Debug.LogError("Title Data not found or not readable: Cardfile");
                TitleDataParseFailed(DisconnectCode.DataParseFailed);
                this.Release();
            }
        }

        /*private void OnGetFile(string response, string error)
        {
            if (!String.IsNullOrEmpty(error))
            {
                Debug.LogError("Web request error on card file: " + error);
                TitleDataParseFailed(DisconnectCode.DataParseFailed);
                this.Release();
            }
            else if (!String.IsNullOrEmpty(response))
            {
                string filePath;
                switch (DataFileType)
                {
                    case DataFileType.CardFile:
                        filePath = PLAYERPREFS_CURRENT_CARD_FILE_NAME;
                        break;
                    case DataFileType.DataFile:
                        filePath = PLAYERPREFS_CURRENT_DATA_FILE_NAME;
                        break;
                    case DataFileType.TextFile:
                        filePath = PLAYERPREFS_CURRENT_TEXT_FILE_NAME;
                        break;
                    default:
                        Debug.LogError("Title Data not found or not readable: Cardfile");
                        TitleDataParseFailed(DisconnectCode.DataParseFailed);
                        this.Release();
                        return;
                }

                // Save the card data file locally and delete the old file
                File.WriteAllText(Path.Combine(Application.persistentDataPath, DataFileName), response);
                
                // Delete the previous cached card file name
                string currentFileName = PlayerPrefs.GetString(filePath, string.Empty);
                if (!String.IsNullOrEmpty(currentFileName)
                    && currentFileName != DataFileName)
                {
                    File.Delete(Path.Combine(Application.persistentDataPath, currentFileName));
                }

                // Update current card file name
                PlayerPrefs.SetString(filePath, DataFileName);
                
                OnGetFileString(response);
            }
            else
            {
                Debug.LogError("Title Data not found or not readable: Cardfile");
                TitleDataParseFailed(DisconnectCode.DataParseFailed);
                this.Release();
            }
        }*/

        private void OnGetFileString(string fileString, bool isFromFile)
        {
            switch(DataFileType)
            {
                case DataFileType.CardFile:
                    ProcessCardFile(fileString, isFromFile);
                    break;
                case DataFileType.DataFile:
                    ProcessDataFile(fileString, isFromFile);
                    break;
                case DataFileType.TextFile:
                    ProcessTextFile(fileString, isFromFile);
                    break;
            }
            Debug.LogFormat("Finished loading file {0}: {1}:{2}", DataFileType, System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
        }

        private void OnPlayFabError(PlayFabError pfe)
        {
            Debug.LogError("Playfab error: " + pfe.Error + ": " + pfe.ErrorMessage);
            TitleDataParseFailed(DisconnectCode.DataParseFailed);
            this.Release();
        }
        
        private void ProcessCardFile(string cardDataJson, bool isFromFile)
        {
            try
            {
                CardFileDataContract cardFile = JsonConvert.DeserializeObject<CardFileDataContract>(cardDataJson);

                GameData.KeywordsById.Clear();
                GameData.KeywordsByName.Clear();
                GameData.SubtypesById.Clear();
                GameData.SubtypesByName.Clear();
                GameData.CardDictionary.Clear();


                foreach (var contract in cardFile.Keywords)
                {
                    KeywordData newKeyword = KeywordData.Create(contract);
                    if (!GameData.KeywordsById.ContainsKey(newKeyword.Id))
                    {
                        GameData.KeywordsById.Add(newKeyword.Id, newKeyword);
                    }
                    if (!GameData.KeywordsByName.ContainsKey(newKeyword.Name))
                    {
                        GameData.KeywordsByName.Add(newKeyword.Name, newKeyword);
                    }
                    if (Enum.IsDefined(typeof(KeywordEnum), newKeyword.Name))
                    {
                        var keywordEnum = (KeywordEnum)Enum.Parse(typeof(KeywordEnum), newKeyword.Name, true);
                        GameData.KeywordsByEnum[keywordEnum] = newKeyword;
                        if (keywordEnum == KeywordEnum.OneShot)
                            GameData.OneShotId = newKeyword.Id;
                    }
                    else
                    {
                        //Debug.LogFormat("Keyword not in Enum: {0}", newKeyword.Name);
                    }
                }
                foreach (var keywordEnum in Enum.GetValues(typeof(KeywordEnum)).Cast<KeywordEnum>())
                {
                    KeywordData data;
                    if (!GameData.KeywordsByEnum.TryGetValue(keywordEnum, out data))
                    {
                        Debug.LogFormat("Keyword in Enum, but not data: {0}", keywordEnum.ToString());
                    }
                }

                string[] subtypes = cardFile.Subtypes;
                int subtypeCount = subtypes.Count();
                for (int i = 0; i < subtypeCount; i++)
                {
                    var subtype = subtypes[i];
                    GameData.SubtypesById.Add(i, subtype);
                    GameData.SubtypesByName.Add(subtype, i);
                }

                GameData.ArtistCredits = new Dictionary<string, string>(cardFile.ArtCredits, StringComparer.OrdinalIgnoreCase);

                List<CardDataContract> cardDataContracts = cardFile.Cards;// JsonConvert.DeserializeObject < List<CardDataContract>>(jObject["Cards"]);// DataUtils.GetObjectList<CardDataContract>(rawResult, "Cards");

                // Populate the CardDirectory
                for (int i = 0; i < cardDataContracts.Count; i++)
                {
                    CardDataContract cardDataContract = cardDataContracts[i];

                    CardData newCard = null;

                    if (cardDataContract.IsUnit)
                    {
                        newCard = UnitCard.Create(cardDataContract);
                    }
                    else if (cardDataContract.IsPower)
                    {
                        newCard = PowerCard.Create(cardDataContract);
                    }
                    else if (cardDataContract.IsItem)
                    {
                        newCard = ItemCard.Create(cardDataContract);
                    }
                    else
                    {
                        Debug.Log("CardData: " + newCard.Name + " has no Type");
                        continue;
                    }

                    string credit;
                    if (GameData.ArtistCredits.TryGetValue(newCard.ArtID, out credit))
                    {
                        newCard.ArtistCredit = credit;
                    }
                    if (0 == newCard.CardID)
                    {
                        Debug.Log("CardData: " + newCard.Name);
                    }
                    if (GameData.CardDictionary.ContainsKey(newCard.CardID) == false)
                    {
                        GameData.CardDictionary.Add(newCard.CardID, newCard);
                    }
                    else
                    {
                        Debug.LogError(newCard.Name + " with CardID: " + newCard.CardID + " already added to CardData as " + GameData.CardDictionary[newCard.CardID].Name);
                    }
                }

                Debug.Log(cardDataContracts.Count.ToString() + " added to CardCatalog");


                if (CardArtImage.PositionData == null)
                {
                    CardArtImage.PositionData = new Dictionary<ArtPositioningKey, ArtPositioningData>();
                }
                CardArtImage.PositionData[ArtPositioningKey.CardListItem] = new ArtPositioningData(ArtPositioningKey.CardListItem);
                var cardListDict = CardArtImage.PositionData[ArtPositioningKey.CardListItem].PositionsDictionary;
				if(cardFile.CardListItems != null)
				{
					foreach (var kvp in cardFile.CardListItems)
	                {
	                    cardListDict[kvp.Key] = new ArtPosition(kvp.Key, kvp.Value);
	                }
				}
                CardArtImage.PositionData[ArtPositioningKey.HeroPortrait] = new ArtPositioningData(ArtPositioningKey.HeroPortrait);
                var heroDict = CardArtImage.PositionData[ArtPositioningKey.HeroPortrait].PositionsDictionary;
				if(cardFile.HeroPortraitItems != null)
				{
	                foreach (var kvp in cardFile.HeroPortraitItems)
	                {
	                    heroDict[kvp.Key] = new ArtPosition(kvp.Key, kvp.Value);
	                }
				}

                this.PlayerProfile.isClientCardFileLoaded = true;
            }
            catch (Exception e)
            {
                Debug.LogError("Card File not readable. Exception: " + e.ToString());
                if (isFromFile)
                    throw e;
                else
                    TitleDataParseFailed(DisconnectCode.DataParseFailed);
            }
            this.Release();
        }

        private void ProcessDataFile(string dataFile, bool isFromFile)
        {
            try
            {
                Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(dataFile);
                string catalogVersionString;
                string globalChatShardString;
                string preconstructedDecksJSON;
                string aiDecksJSON;
                string ratingRulesJSON;
                string achievementDataJSON;
                string tutorialDataJSON;
                string tutorialSpecialAttackJSON;
                string avatarDataJSON;
                string cardBackDataJSON;
                string localizationFileCSV;
                //string eventTypesJSON;
                //string eventScheduleJSON;
                data.TryGetValue(catalogVersionKey, out catalogVersionString);
                data.TryGetValue(preconstructedDeckKey, out preconstructedDecksJSON);
                data.TryGetValue(aiDeckKey, out aiDecksJSON);
                data.TryGetValue(ratingRulesKey, out ratingRulesJSON);
                data.TryGetValue(achievementDataKey, out achievementDataJSON);
                data.TryGetValue(tutorialSpecialAttackKey, out tutorialSpecialAttackJSON);
                data.TryGetValue(tutorialDataKey, out tutorialDataJSON);
                data.TryGetValue(avatarDataKey, out avatarDataJSON);
                data.TryGetValue(cardBackDataKey, out cardBackDataJSON);
                data.TryGetValue(localizationDataKey, out localizationFileCSV);

                GameData.CatalogVersion = catalogVersionString;
                //this.LoadFullCatalogSignal.Dispatch(catalogVersionString);
                if(NovaIAPService.SteamController != null)
                {
                    NovaIAPService.SteamController.Initialize(GameData.CatalogVersion);
                }
                uint shards;
                if(data.TryGetValue(globalChatShardKey, out globalChatShardString) && uint.TryParse(globalChatShardString, out shards) && shards > 0)
                {
                    this.PlayerProfile.NumberOfGlobalChatShards = shards;
                }
                else
                {
                    this.PlayerProfile.NumberOfGlobalChatShards = 1;
                    Debug.LogFormat("global chat shards not known. Assuming one.");
                }
                //data.TryGetValue(eventTypesKey, out eventTypesJSON);
                //data.TryGetValue(eventScheduleKey, out eventScheduleJSON);

                ProcessPreconstructedDecks(preconstructedDecksJSON);
                ProcessAIDecks(aiDecksJSON);

                ProcessTutorialData(tutorialDataJSON, tutorialSpecialAttackJSON);

                ProcessRatingRules(ratingRulesJSON);
                ProcessAchievements(achievementDataJSON);

                ProcessAvatarData(avatarDataJSON);
                ProcessCardBackData(cardBackDataJSON);

                //ProcessTournamentTypes(eventTypesJSON);

                //ProcessEventSchedule(eventScheduleJSON);
                
                // process lock values for league & events
                string dataString;
                bool boolValue;
                int intValue;
                GameData.isLeagueUnlocked = data.TryGetValue(leagueUnlockedKey, out dataString) && bool.TryParse(dataString, out boolValue) && boolValue == true;
                GameData.isEventsUnlocked = data.TryGetValue(eventsUnlockedKey, out dataString) && bool.TryParse(dataString, out boolValue) && boolValue == true;
                GameData.leagueThreshold = data.TryGetValue(leagueRankKey, out dataString) && int.TryParse(dataString, out intValue) ? intValue : -100;
                GameData.eventsThreshold = data.TryGetValue(eventsRankKey, out dataString) && int.TryParse(dataString, out intValue) ? intValue : -100;
                
                this.PlayerProfile.isClientDataFileLoaded = true;
                this.TitleDataLoadedSignal.Dispatch();
            }
            catch (Exception e)
            {
                Debug.LogError("Data File not readable. Exception: " + e.ToString());
                if (isFromFile)
                    throw e;
                else
                    TitleDataParseFailed(DisconnectCode.DataParseFailed);
            }
            this.Release();
        }
        
        private void ProcessTextFile(string localizationFileCSV, bool isFromFile)
        {
            try
            {
                I2.Loc.LocalizationManager.Sources[0].Import_CSV(string.Empty, localizationFileCSV, I2.Loc.eSpreadsheetUpdateMode.Replace);
                I2.Loc.LocalizationManager.LocalizeAll();

                // Sets the keys that'll be used for localized keyword searches
                GameData.SetSearchKeys();

				foreach(var card in GameData.CardDictionary.Values)
				{
					card.SetSearchableCardText();
				}

                this.PlayerProfile.isClientTextFileLoaded = true;
                this.LanguageChangedSignal.Dispatch();
            }
            catch (Exception e)
            {
                Debug.LogError("Game Text File not readable. Exception: " + e.ToString());
                if (isFromFile)
                    throw e;
                else
                    TitleDataParseFailed(DisconnectCode.DataParseFailed);
            }
            this.Release();
        }

        private void ProcessAvatarData(string avatarDataJSON)
        {
            GameData.AvatarDictionary.Clear();
            GameData.AvatarByItemIdDictionary.Clear();
            Dictionary<string, List<AvatarDataContract>> avatarDataContracts = JsonConvert.DeserializeObject<Dictionary<string, List<AvatarDataContract>>>(avatarDataJSON);// DataUtils.GetObjectList<AvatarDataContract>(avatarResult, "Avatars");
            var avatarList = avatarDataContracts.Values.FirstOrDefault();
            foreach (var avatar in avatarList)
            {
                GameData.AvatarDictionary.Add((int)avatar.Id, avatar);
                if (avatar.IsFree)
                    GameData.FreeAvatars.Add(avatar);
                if (avatar.Id == 0)
                    GameData.DefaultAvatarArtId = avatar.ArtId;

                StringBuilder builder = new StringBuilder(NBEconomy.CATALOG_AVATAR_PREFIX);
                builder.Append((int)avatar.Id);
                string itemId = builder.ToString();
                GameData.AvatarByItemIdDictionary.Add(itemId, avatar);
            }
        }

        private void ProcessCardBackData(string cardBackDataJSON)
        {
            GameData.CardBackDictionary.Clear();
            GameData.CardBackByItemIdDictionary.Clear();
            if (cardBackDataJSON == null)
                return;
            Dictionary<string, List<CardBackDataContract>> cardBackDataContracts = JsonConvert.DeserializeObject<Dictionary<string, List<CardBackDataContract>>>(cardBackDataJSON);// DataUtils.GetObjectList<AvatarDataContract>(avatarResult, "Avatars");
            var cardBackList = cardBackDataContracts.Values.FirstOrDefault();
            foreach (var cardBack in cardBackList)
            {
                GameData.CardBackDictionary.Add((int)cardBack.Id, cardBack);
                if (cardBack.IsFree)
                    GameData.FreeCardBacks.Add(cardBack);
                if (cardBack.Id == 0)
                    GameData.DefaultCardBackArtId = cardBack.ArtId;

                StringBuilder builder = new StringBuilder(NBEconomy.CATALOG_CARDBACK_PREFIX);
                builder.Append((int)cardBack.Id);
                string itemId = builder.ToString();
                GameData.CardBackByItemIdDictionary.Add(itemId, cardBack);
            }
        }

        private void ProcessTutorialData(string tutorialDataJSON, string tutorialSpecialAttackJSON)
        {

            GameData.TutorialList.Clear();
            GameData.TutorialDictionary.Clear();
            GameData.TutorialCombatDictionary = new Dictionary<int, List<TutorialCombatData>>();

            if (tutorialDataJSON != null)
            {
                Dictionary<string, List<TutorialDataContract>> tutorialDataContracts = JsonConvert.DeserializeObject<Dictionary<string, List<TutorialDataContract>>>(tutorialDataJSON);

                foreach (var deck in tutorialDataContracts.Values)
                {
                    // create the actual deck
                }
                GameData.TutorialList = tutorialDataContracts.Values.FirstOrDefault();

                GameData.TutorialDictionary = GameData.TutorialList.ToDictionary(k => (int)k.TutorialId, k => k);

                if (!string.IsNullOrEmpty(tutorialSpecialAttackJSON))
                {
                    List<TutorialCombatData> specialAttackData = JsonConvert.DeserializeObject<List<TutorialCombatData>>(tutorialSpecialAttackJSON);
                    if (specialAttackData != null)
                    {
                        foreach (var attack in specialAttackData)
                        {
                            int id = attack.Game * 100 + attack.Turn;
                            List<TutorialCombatData> attackList;
                            if (!GameData.TutorialCombatDictionary.TryGetValue(id, out attackList))
                            {
                                attackList = new List<TutorialCombatData>();
                                GameData.TutorialCombatDictionary.Add(id, attackList);
                            }
                            attackList.Add(attack);
                        }
                    }
                }
            }
        }

        private void ProcessPreconstructedDecks(string preconstructedDecksJson)
        {
            DeckListDataContract deckList = JsonConvert.DeserializeObject<DeckListDataContract>(preconstructedDecksJson);// DataUtils.ParseValue<DeckListDataContract>(preconstructedDecksJson);
            this.GameData.PreconstructedDataContract = deckList;

        }

        private void ProcessAIDecks(string aiDeckJson)
        {
            Dictionary<int, string> aiDecks = JsonConvert.DeserializeObject<Dictionary<int, string>>(aiDeckJson);
            this.GameData.AIDecks.Clear();
            this.GameData.AIDeckStrings.Clear();

            foreach (var kvp in aiDecks)
            {
                var lower = kvp.Value.ToLower();
                var hashSet = new HashSet<CardAspect>();
                if (lower.Contains("a"))
                    hashSet.Add(CardAspect.Arcane);
                if (lower.Contains("t"))
                    hashSet.Add(CardAspect.Tech);
                if (lower.Contains("d"))
                    hashSet.Add(CardAspect.Divine);
                if (lower.Contains("n"))
                    hashSet.Add(CardAspect.Nature);
                if (lower.Contains("c"))
                    hashSet.Add(CardAspect.Chaos);
                if (hashSet.Count > 0)
                {
                    this.GameData.AIDecks[kvp.Key] = hashSet;
                    this.GameData.AIDeckStrings[kvp.Key] = kvp.Value;
                }
            }
        }

        private void ProcessRatingRules(string json)
        {
            RatingRuleDataContract rawResult = JsonConvert.DeserializeObject<RatingRuleDataContract>(json);

            this.GameData.RankThresholds = rawResult.rank_thresholds.ToArray();
            this.GameData.RankBrackets = EngineUtils.BracketsToDictionary(EngineUtils.GenerateBrackets(this.GameData.RankThresholds));
            this.PlayerProfile.RankBrackets = this.GameData.RankBrackets;
            /*
            object thresholds;
			if (rawResult.TryGetValue(rankThresholdsKey, out thresholds)) {
				var objArray = (object[]) thresholds;
				var intArray = objArray.Select(o => Convert.ToInt32(o)).ToArray();
				this.GameData.RankThresholds = intArray;
				this.GameData.RankBrackets = EngineUtils.BracketsToDictionary(EngineUtils.GenerateBrackets(intArray));

			}*/
        }

        private void ProcessAchievements(string json)
        {
            var rawResult = JsonConvert.DeserializeObject<Dictionary<string, AchievementDataContract>>(json);
            this.GameData.AchievementDataCache = rawResult.Select(dc => dc.Value.ToModel(dc.Key)).ToDictionary(k => k.Name, StringComparer.OrdinalIgnoreCase);
        }

        private const int MIN_EVENT_LENGTH = 120; // in minutes
        private const long MIN_EVENT_END_GAP = 5 * TimeSpan.TicksPerMinute;

        private void TitleDataParseFailed(DisconnectCode code)
        {
            this.ClientService.ForceDisconnect(code);
            this.Fail();
            this.Release();
        }
        

        public class CardFileDataContract
        {
            public List<KeywordDataContract> Keywords { get; set; }
            public List<CardDataContract> Cards { get; set; }
            public Dictionary<string, string> ArtCredits { get; set; }
            public string[] Subtypes { get; set; }
            public Dictionary<string, float[]> CardListItems { get; set; }
            public Dictionary<string, float[]> HeroPortraitItems { get; set; }
            //public string Version { get; set; }
        }
        public class RatingRuleDataContract
        {
            public Dictionary<GameFormat, int> rating_points_per_win { get; set; }
            public Dictionary<GameFormat, int> max_events { get; set; }
            public List<int> rank_thresholds { get; set; }
            //{"rating_points_per_win":{"Casual":5,"Draft":5,"Constructed":5,"Tournament":20,"Monthly":50},"max_events":{"Casual":250,"Draft":5,"Constructed":5,"Tournament":5,"Monthly":1},"rank_thresholds":[0,5,20,35,50,90,130,170,210,250,350,450,550,650,750,1000,1250,1500,1750,2000,2500,3000,3500,4000,4500,6000,7500,9000,10500,12000,15000]}
        }

        //MD FIXME This private class enables us to wait on the WWW get request for the card data
        // but there is probably an easier way to do this!
        internal class PrivateHttp : SingletonMonoBehaviour<PrivateHttp>
        {
            public static void DownloadBytes(string url, Action<byte[], string> callback)
            {
                PrivateHttp.instance.InstGetBytes(url, callback);
            }

            private void InstGetBytes(string url, Action<byte[], string> callback)
            {
                StartCoroutine(GetBytes(url, callback));
            }

            private IEnumerator GetBytes(string url, Action<byte[], string> callback)
            {
                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add("Accept", "*/*");

                WWW www = new WWW(url, null, headers);
                yield return www;

                if (!String.IsNullOrEmpty(www.error))
                {
                    Debug.LogError(www.error);
                    callback(null, www.error);
                }
                else
                {
                    byte[] response = www.bytes;
                    callback(response, null);
                }

            }
            public static void DownloadString(string url, Action<string, string> callback)
            {
                PrivateHttp.instance.InstGetString(url, callback);
            }

            private void InstGetString(string url, Action<string, string> callback)
            {
                StartCoroutine(GetString(url, callback));
            }

            private IEnumerator GetString(string url, Action<string, string> callback)
            {
                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add("Accept", "*/*");

                WWW www = new WWW(url, null, headers);
                yield return www;

                if (!String.IsNullOrEmpty(www.error))
                {
                    Debug.LogError(www.error);
                    callback(null, www.error);
                }
                else
                {
                    string response = www.text;
                    callback(response, null);
                }

            }
        }
    }
}
