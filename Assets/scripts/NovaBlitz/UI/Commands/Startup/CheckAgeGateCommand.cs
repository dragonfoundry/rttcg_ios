﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI;

namespace NovaBlitz.UI
{
    //public class AgeGatePassedSignal : Signal { }

    public class CheckAgeGateCommand : Command
    {
        //[Inject] public ChillingoService ChillingoService { get; set; }
        //[Inject] public AgeGatePassedSignal AgeGatePassedSignal { get; set; }
		[Inject] public PlayerProfile PlayerProfile {get;set;}

        public override void Execute()
        {
            Debug.Log ("Starting age gate checking");
            this.Retain();
            //this.AgeGatePassedSignal.AddListener(OnTermsComplete);

			// check for age gate
			PlaceholderAgeGateCheck();

            // run the chillingo Terms service to check age compliance
            //Debug.Log("#Chillingo# Mobile - Checking Age Gate");
            //this.ChillingoService.CheckTerms();

        }

		public void PlaceholderAgeGateCheck()
		{
			this.PlayerProfile.isAgeGatePassed = true;
			OnTermsComplete ();
		}

        public void OnTermsComplete()
        {
            //this.AgeGatePassedSignal.RemoveListener(OnTermsComplete);
            Debug.Log("#Chillingo# Mobile - Age Gate Passed");
            this.Release();
        }
    }
}
