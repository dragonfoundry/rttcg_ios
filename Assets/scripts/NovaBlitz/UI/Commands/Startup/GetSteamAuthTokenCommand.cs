﻿#if UNITY_STEAM
using System;
using UnityEngine;
using System.Collections;
using NovaBlitz.Game;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;
using Steamworks;

namespace NovaBlitz.UI
{
    public class GetSteamAuthTokenSignal : Signal { }
    public class GetSteamAuthTokenCommand : Command
    {
        [Inject] public NovaSteamService NovaSteamService { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public SteamAuthTicketSignal SteamAuthTicketSignal { get; set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        
        public override void Execute()
        {
            this.Retain();
            //Debug.Log("Getting Steam Auth Token");
            //Bind to steam callbacks,  FYI - I hate the way they do event binding as it is Abnormal.
            NovaSteamService._authSessionTicketCallback = Callback<GetAuthSessionTicketResponse_t>.Create(this.AuthCallback); //bind to the Steam GetAuthSessionTicketCallback
            GetAuthToken();
        }


        protected byte[] _SteamTicket;
        protected uint _pcbTicket;
        private int Tries = 0;

        public void GetAuthToken()
        {
            Tries++;
            _SteamTicket = new byte[1024];
            try
            {
                SteamUser.GetAuthSessionTicket(_SteamTicket, 1024, out _pcbTicket);
            }
            catch (Exception except)
            {
                Debug.LogErrorFormat("Steam exception: {0}", except.ToString());
                if (Tries < 3)
                    GetAuthToken();
                else
                {
                    NovaSteamService.lastErrorCode = "2";
                    Debug.LogErrorFormat("Steam Error on authentication. Error code: {0}", NovaSteamService.lastErrorCode);
                    this.ClientService.ForceDisconnect(Messages.DisconnectCode.Reason15);
                }
            }

        }

        public void AuthCallback(GetAuthSessionTicketResponse_t param)
        {
            if (param.m_eResult != EResult.k_EResultOK)
            {
                if (Tries < 3)
                {
                    Debug.Log("Retrying Steam Auth. Failed on callback");
                    GetAuthToken();
                }
                else
                {
                    NovaSteamService.lastErrorCode = param.m_eResult.ToString();
                    Debug.LogErrorFormat("Steam Error on authentication. Error code: {0}", NovaSteamService.lastErrorCode);
                    this.ClientService.ForceDisconnect(Messages.DisconnectCode.Reason15);
                }
                return;
            }
            //Debug.Log(param.m_eResult);
            //Debug.Log("Steam Auth Callback - Processing AuthTicket");
            byte[] actualTicket = new byte[_pcbTicket]; // create a new array that matches the m_pcbTicket length
            //Debug.Log("Steam Auth Callback - Processing AuthTicket - step 2");
            Array.Copy(_SteamTicket, actualTicket, _pcbTicket); // copy the first m_pcbTicket bytes
            //Debug.Log("Steam Auth Callback - Processing AuthTicket - step 3");
            string authStr = BitConverter.ToString(actualTicket).Replace("-", string.Empty); // convert the actual ticket to a hexadecimal string
            //Debug.LogFormat("Steam Callback Auth String Acquired: {0}", authStr);
            this.SteamAuthTicketSignal.Dispatch(authStr);
            
            if(ClientPrefs.SteamName == null)
                ClientPrefs.SteamName = SteamFriends.GetPersonaName();
            if(ClientPrefs.SteamId == null)
                ClientPrefs.SteamId = SteamUser.GetSteamID().ToString();

            /*_instance._SteamTicket = new byte[1024];
            //Make a request to get the Steam Auth Ticket.  This ensures the user can authenticate via steam and used to login to playfab via steam on callback.
            SteamUser.GetAuthSessionTicket(_instance._SteamTicket, 1024, out _instance._pcbTicket); // Make Request for the auth ticket.*/
            //Debug.Log(string.Format("User: {0} using steam Id: {1}", ClientPrefs.SteamName, ClientPrefs.SteamId));
            //this.PlayerProfile.MetaProfile.Name = ClientPrefs.SteamName;
        }
    }
}
#endif
