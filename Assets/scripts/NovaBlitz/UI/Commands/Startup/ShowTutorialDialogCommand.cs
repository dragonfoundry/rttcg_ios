﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;

namespace NovaBlitz.UI
{
	public class ShowTutorialDialogSignal : Signal<string,bool> {}
}
