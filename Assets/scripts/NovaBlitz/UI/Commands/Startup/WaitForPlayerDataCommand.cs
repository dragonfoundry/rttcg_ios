﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using PlayFab;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
	public class WaitForPlayerDataSignal : Signal {}
	public class WaitForPlayerDataCommand : Command
	{
		// Dependendy Injection
		[Inject] public ClientPrefs ClientPrefs {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
		[Inject] public PlayFabErrorRaisedSignal PlayFabErrorRaisedSignal {get;set;}
		[Inject] public ProgressScreenClosedSignal ProgressScreenClosedSignal {get;set;}
        [Inject] public LoginWithDeviceOrSteamSignal LoginWithDeviceOrSteamSignal { get; set; }
        [Inject] public UserLoggedInSignal UserLoggedInSignal { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        //[Inject] public ChillingoService ChillingoService { get; set; }
        [Inject] public ShowNewMessageDialogSignal ShowNewMessageDialogSignal{get;set;}
        [Inject] public LogoutUserSignal LogoutUserSignal { get; set; }
        [Inject] public CancelSignal CancelSignal { get; set; }

        private IEnumerator _WaitForChillingoCoroutine { get; set; }

        public override void Execute ()
        {
            Debug.LogFormat("WaitForPlayerData Started: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Retain();
            //this.CancelSignal.AddListener(OnCancel);
            this.LogoutUserSignal.AddListener(OnLogout);
            if (_WaitForChillingoCoroutine != null)
                Timer.Instance.StopCoroutine(_WaitForChillingoCoroutine);
            _WaitForChillingoCoroutine = WaitForChillingoCoroutine();
            Timer.Instance.StartCoroutine(_WaitForChillingoCoroutine);
        }

        private IEnumerator WaitForChillingoCoroutine()
        {
            yield return null;

            // no more than one login attempt per 5s, no matter what
            /*while(Time.realtimeSinceStartup < this.ClientPrefs.LastLoginTimestamp)
            {
                yield return null;
            }

            this.ClientPrefs.LastLoginTimestamp = Time.realtimeSinceStartup + 5.0f;*/

            //this.CloseAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));
            this.PlayFabErrorRaisedSignal.AddListener(OnPlayfabError);

            UserLoggedInSignal.RemoveListener(OnUserLoggedIn);
            UserLoggedInSignal.AddListener(OnUserLoggedIn);
            yield return null;
            this.LoginWithDeviceOrSteamSignal.Dispatch(false);
            _WaitForChillingoCoroutine = null;
        }

        private void OnUserLoggedIn(LoginResult result)
        {
            this.UserLoggedInSignal.RemoveListener(OnUserLoggedIn);
            //UserLoggedInSignal.RemoveListener(OnUserLoggedIn);

			Debug.LogFormat ("WaitForPlayerData: Player data loaded: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.PlayFabErrorRaisedSignal.RemoveListener(OnPlayfabError);
            
            this.UserLoggedInSignal.RemoveListener(OnUserLoggedIn);
            this.LogoutUserSignal.RemoveListener(OnLogout);
			this.Release();
		}

        private void OnLogout(ShouldUnlinkDevice unlinkDeviceId, ShouldAutoReconnect autoReconnect)
        {
            //this.CancelSignal.RemoveListener(OnCancel);
            this.LogoutUserSignal.RemoveListener(OnLogout);
            this.UserLoggedInSignal.RemoveListener(OnUserLoggedIn);
            this.PlayFabErrorRaisedSignal.RemoveListener(OnPlayfabError);
            if (_WaitForChillingoCoroutine != null)
                Timer.Instance.StopCoroutine(_WaitForChillingoCoroutine);
            this.Fail();
            this.Release();
        }
		
		private void OnPlayfabError(PlayFab.PlayFabError playFabError)
		{
			switch(playFabError.Error)
            {
                case PlayFabErrorCode.ServiceUnavailable:
                    break;
                case PlayFab.PlayFabErrorCode.AccountNotFound:
                case PlayFab.PlayFabErrorCode.InvalidSteamTicket:
				case PlayFab.PlayFabErrorCode.InvalidDeviceID:
				case PlayFab.PlayFabErrorCode.NotAuthenticated:
				case PlayFab.PlayFabErrorCode.InvalidEmailOrPassword:
				case PlayFab.PlayFabErrorCode.InvalidEmailAddress:
                case PlayFab.PlayFabErrorCode.InvalidParams:
                case PlayFab.PlayFabErrorCode.InvalidUsername:
                case PlayFabErrorCode.UsernameNotAvailable:
                case PlayFabErrorCode.DuplicateUsername:
                case PlayFab.PlayFabErrorCode.Success:
					// These tend to be produced by the Login flow, but if the flow isn't open it means we should probably get started on
                    // Note that "get started" actually means "open the panel that lets you choose what to do"
					LoginDialogNewFlowView loginDialog = (LoginDialogNewFlowView)this.ViewReferences.Get(typeof(LoginDialogNewFlowView));
				    if(loginDialog == null || loginDialog.IsOpen == false)
					{
						this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(FirstDialogView));
					}
				    break;

                case PlayFabErrorCode.DownstreamServiceUnavailable:
                    // we've already opened the login panel. pop a message.
                    LoginDialogNewFlowView loginDialog2 = (LoginDialogNewFlowView)this.ViewReferences.Get(typeof(LoginDialogNewFlowView));
                    if (loginDialog2 == null || loginDialog2.IsOpen == false)
                    {
                        this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(FirstDialogView));
                    }
                    this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams
                    {
                        Message = I2.Loc.ScriptLocalization.Stean_Auth_Down,
                        Title = I2.Loc.ScriptLocalization.Steam_Error
                    });

                    break;
                case PlayFabErrorCode.InternalServerError:
                default:
                    // If it's a server error, or some unknown error, bail... we're in trouble
                    //this.CancelSignal.RemoveListener(OnCancel);
                    this.LogoutUserSignal.RemoveListener(OnLogout);
                    this.UserLoggedInSignal.RemoveListener(OnUserLoggedIn);
                    this.PlayFabErrorRaisedSignal.RemoveListener(OnPlayfabError);
                    //this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect);
                    //this.ShowNewMessageDialogSignal.Dispatch(new NewMessageDialogParams(I2.Loc.ScriptLocalization.Error.CouldNotConnect, I2.Loc.ScriptLocalization.Error.CouldNotConnectMessage));
                    Debug.Log("Canceling WaitForPlayerDataCommand : PlayFabErrorCode is " + playFabError.Error + " error: " + playFabError.ErrorMessage);
                    if (playFabError.ErrorDetails != null)
                    {
                        foreach (var error in playFabError.ErrorDetails)
                        {
                            Debug.Log("details: " + error.Key + ": " + string.Join(", ", error.Value.ToArray()));
                        }
                    }
                    this.Fail();
                    this.Release();
                    this.LogoutUserSignal.Dispatch(ShouldUnlinkDevice.DontUnlinkAccount, ShouldAutoReconnect.AutomaticallyReconnect);// playFabError.Error == PlayFabErrorCode.ServiceUnavailable ? ShouldAutoReconnect.AutomaticallyReconnect : ShouldAutoReconnect.DontReconnect);
				break;
            }
            this.Release();
        }
	}
}
