﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using LinqTools;
using NovaBlitz.UI.DataContracts;
using Newtonsoft.Json;
using System;

namespace NovaBlitz.UI
{
    public class LoadCardArtPositionDataCommand : Command
    {
        [Inject] public AsyncProgressSignal AsyncProgressSignal {get;set;}
        public override void Execute()
        {
            if(CardArtImage.PositionData == null)
            {
                CardArtImage.PositionData = new Dictionary<ArtPositioningKey, ArtPositioningData>();
            }

            Debug.Log("Loading card art positions");
            // CardListItem Positioning Data
             IEnumerator cardListItemEnumerator = 
             this.LoadPositionsAsync(ArtPositioningKey.CardListItem,
                                                           0.0f, 0.5f, 
                                                           this.OnCardListItemsComplete,
                                                           this.OnProgress);
                                                           
             Timer.Instance.StartCoroutine(cardListItemEnumerator);
        }
        
        private void OnProgress(float progress)
        {
            Debug.LogFormat("Card Art Positions. Got progress: {0}", progress);
            this.AsyncProgressSignal.Dispatch(AsyncProgress.CardArtPositions, progress);
        }
        
        private void OnCardListItemsComplete(ArtPositioningDataContract contract)
        {
            if(contract != null)
            {
                ArtPositioningData positionData = contract.ToModel();
                CardArtImage.PositionData[positionData.Key] = positionData;
                Debug.Log("CardArtPositions: loaded positions for CardListItems");
            }
            else
            {
                Debug.LogWarning("CardArtPositions: unable to load positions for CardListItems");
            }
            
            IEnumerator heroPortraitEnumerator = 
            this.LoadPositionsAsync(ArtPositioningKey.HeroPortrait,
                                                           0.5f, 1.0f, 
                                                           this.OnHeroPortraitComplete,
                                                           this.OnProgress);
            Timer.Instance.StartCoroutine(heroPortraitEnumerator);
        }
        
        private void OnHeroPortraitComplete(ArtPositioningDataContract contract)
        {
            if(contract != null)
            {
                ArtPositioningData positionData = contract.ToModel();
                CardArtImage.PositionData[positionData.Key] = positionData;
                Debug.Log("CardArtPositions: loaded positions for HeroPortraits");
            }
            else
            {
                Debug.LogWarning("CardArtPositions: unable to load positions for HeroPortraits");
            }
        }
        
        private IEnumerator LoadPositionsAsync(ArtPositioningKey key, float startProgress, float endProgress, System.Action<ArtPositioningDataContract> onComplete, System.Action<float> onProgress)
        {
            yield return null;
            ArtPositioningDataContract contract = null;
            string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, key.ToString() + ".json");
            if(Application.platform != RuntimePlatform.Android)
            {
                filePath = "file://" + filePath;
            }
            Debug.LogFormat("trying to load art positions from file: {0}", filePath);
            WWW reader = new WWW(filePath);
            yield return reader;
            while(! reader.isDone) 
            {
                onProgress.Invoke(startProgress + reader.progress * (endProgress - startProgress));
                yield return reader;
            }
            yield return null;
            try
            {
                string json = reader.text;
                //Debug.Log(json);
                contract = JsonConvert.DeserializeObject<ArtPositioningDataContract>(json);
            }
            catch (Exception except)
            {
                string exception = except.Message;
                Debug.LogWarningFormat("Failed loading art positining data for: {0}. Exception: {1}", filePath, except.ToString());
            }
            onProgress.Invoke(endProgress);
            yield return null;
            onComplete.Invoke(contract);
            Debug.Log("Card items enumerator complete");
        }
    }

    public class ArtPositioningClass
    {
        public string Key { get; set; }
        ArtPositioningItem[] Positions { get; set; }
    }

    public class ArtPositioningItem
    {
        float[] Vals { get; set; }
        string Id { get; set; }
    }
}