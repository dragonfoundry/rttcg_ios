﻿using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using NovaBlitz.Economy;
using UnityEngine.Purchasing;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class PlayerDataResponseSignal : Signal<PlayerDataResponse> { }
    public class PlayerDataResponseCommand : Command
    {
        [Inject] public PlayerDataResponse PlayerDataResponse { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ProfileUpdatedSignal ProfileUpdatedSignal { get; set; }
        [Inject] public GameData GameData { get; set; }
		[Inject] public NovaIAPService NovaIAPService { get; set; }
		[Inject] public LogEventSignal LogEventSignal {get;set;}
        [Inject] public LoadDataFileSignal LoadDataFileSignal { get; set; }
        [Inject] public LoadFriendsListSignal LoadFriendsListSignal { get; set; }
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public MetricsTrackingService MetricsTrackingService { get; set; }
        [Inject] public AssetBundleManager AssetBundleManager { get; set; }

        private IEnumerator LoadDataFilesCoroutine()
        {
            yield return null;
            if(!string.IsNullOrEmpty(PlayerDataResponse.ClientCardFile))
            {
                this.LoadDataFileSignal.Dispatch(PlayerDataResponse.ClientCardFile, DataFileType.CardFile);
            }

            yield return null;
            if (!string.IsNullOrEmpty(PlayerDataResponse.ClientDataFile))
            {
                this.LoadDataFileSignal.Dispatch(PlayerDataResponse.ClientDataFile, DataFileType.DataFile);
            }

            yield return null;
            if (!string.IsNullOrEmpty(PlayerDataResponse.ClientTextFile))
            {
                this.LoadDataFileSignal.Dispatch(PlayerDataResponse.ClientTextFile, DataFileType.TextFile);
            }
            this.Release();
        }

        public override void Execute()
        {
            this.Retain();
            Timer.Instance.StartCoroutine(WaitForOutOfGameCoroutine());
        }
        private IEnumerator WaitForOutOfGameCoroutine()
        { 
            // If we receive a data response during game play, wait. But don't wait if the player's data isn't complete.
            while(this.PlayerProfile.IsAllDataLoaded && this.ClientService.IsInGame.HasValue && this.ClientService.IsInGame.Value == true)
            {
                yield return new WaitForSecondsRealtime(0.1f);
            }
            this.Release();
            bool isFriendsUpdated = false;
            bool isInventoryUpdated = false;
            bool isStatsUpdated = false;
            bool isTournamentDataUpdated = false;

            if (PlayerDataResponse.InitialPlayerDataFinished)
            {
                this.PlayerProfile.isPlayerDataLoaded = true;
            }

            // Check and update all the asset bundles
            if(PlayerDataResponse.AssetBundleList != null)
            {
                AssetBundleManager.UpdateAssetBundles(PlayerDataResponse.AssetBundleList);
            }

            if (!string.IsNullOrEmpty(PlayerDataResponse.ClientCardFile) || !string.IsNullOrEmpty(PlayerDataResponse.ClientDataFile) || !string.IsNullOrEmpty(PlayerDataResponse.ClientTextFile))
            {
                this.Retain();
                Timer.Instance.StartCoroutine(LoadDataFilesCoroutine());
            }

            if(PlayerDataResponse.NovaFriendList != null)
            {
                PlayerProfile.FriendListData.Clear();
                for(int i = 0; i<PlayerDataResponse.NovaFriendList.Count; i++)
                {

                    var friend = new NovaBlitzFriend(PlayerDataResponse.NovaFriendList[i], 0, GameData.DefaultAvatarArtId);
                    
                    PlayerProfile.FriendListData[PlayerDataResponse.NovaFriendList[i].PlayFabId] = friend;
                }
                Debug.Log("Nova FriendList updated");
                isFriendsUpdated = true;
                LoadFriendsListSignal.Dispatch();
            }
            if (isFriendsUpdated)
            {
            }
            if (PlayerDataResponse.BlockList != null)
            {
                this.PlayerProfile.BlockList.Clear();
                for(int i = 0; i <PlayerDataResponse.BlockList.Count; i++)
                {
                    this.PlayerProfile.BlockList.Add(PlayerDataResponse.BlockList[i]);
                }

                foreach (var friend in this.PlayerProfile.FriendListData)
                {
                    friend.Value.IsBlocked = this.PlayerProfile.BlockList.Contains(friend.Key);
                }

                Debug.Log("BlockList updated");
            }

                /*if (PlayerDataResponse.BlockList != null || PlayerDataResponse.FriendList != null || PlayerDataResponse.FriendRequestList != null)
                {
                    if (PlayerDataResponse.BlockList != null)
                    {
                        this.PlayerProfile.BlockList = new HashSet<string>(PlayerDataResponse.BlockList);
                        Debug.Log("BlockList updated");
                    }

                    if (PlayerDataResponse.FriendList != null)
                    {
                        this.PlayerProfile.FriendList = new HashSet<string>(PlayerDataResponse.FriendList);
                        Debug.Log("FriendList updated");
                    }

                    if (PlayerDataResponse.FriendRequestList != null)
                    {
                        PlayerProfile.PendingFriendRequests.Clear();
                        foreach (var request in PlayerDataResponse.FriendRequestList)
                        {
                            try
                            {
                                FriendRequestData data = JsonConvert.DeserializeObject<FriendRequestData>(request.Value);
                                PlayerProfile.PendingFriendRequests.Add(request.Key, data);
                            }
                            catch (Exception e)
                            {
                                Debug.LogErrorFormat("FriendRequest parse exception for {0}, value: {1}, {2}", request.Key, request.Value, e);
                            }
                        }
                        Debug.Log("FriendRequestList updated");
                    }
                    isFriendsUpdated = true;
                }*/

                if (PlayerDataResponse.CardInventory != null)
            {

                Dictionary<int, int> cards = new Dictionary<int, int>();

                foreach (var item in PlayerDataResponse.CardInventory)
                {
                    // This property works regardless of IsStackable.
                    int amount = (int)(item.Value.RemainingUses ?? 1);
                    // Increase quantity.
                    if (cards.ContainsKey(item.Key))
                        cards[item.Key] += amount;
                    else
                        cards.Add(item.Key, amount);
                }

                this.PlayerProfile.OwnedCards = cards;
                isInventoryUpdated = true;
                Debug.Log("PDR: Card Inventory");
            }

            if (PlayerDataResponse.Inventory != null)
            {
                HashSet<string> keys = new HashSet<string>();
                HashSet<string> avatars = new HashSet<string>();
                HashSet<string> cardBacks = new HashSet<string>();
                HashSet<string> playmats = new HashSet<string>();
                HashSet<string> storeItems = new HashSet<string>();
                HashSet<string> ownedItems = new HashSet<string>();

                foreach (var item in PlayerDataResponse.Inventory)
                {
                    ownedItems.Add(item.Value.ItemId);
                    switch (item.Value.ItemClass)
                    {
                        case NBEconomy.ITEMCLASS_CARD:
                            break;
                        case NBEconomy.ITEMCLASS_KEY:
                            keys.Add(item.Value.ItemId);
                            break;
                        case NBEconomy.ITEMCLASS_AVATAR:
                            avatars.Add(item.Value.ItemId);
                            break;
                        case NBEconomy.ITEMCLASS_CARDBACK:
                            cardBacks.Add(item.Value.ItemId);
                            break;
                        case NBEconomy.ITEMCLASS_PLAYMAT:
                            playmats.Add(item.Value.ItemId);
                            break;
                        case NBEconomy.ITEMCLASS_STORE_ITEM:
                            storeItems.Add(item.Value.ItemId);

                            if (item.Value.ItemId == NovaIAPService.ANNUITY_CREDIT)
                            {
                                PlayerProfile.CreditAnnuityExpiration = item.Value.Expiration;
                            }
                            else if (item.Value.ItemId == NovaIAPService.ANNUITY_NANOBOT)
                            {
                                PlayerProfile.NanoBotAnnuityExpiration = item.Value.Expiration;
                            }
                            else if (item.Value.ItemId == NovaIAPService.ANNUITY_SMARTPACK)
                            {
                                PlayerProfile.SmartPackAnnuityExpiration = item.Value.Expiration;
                            }
                            else if (item.Value.ItemId == NovaIAPService.ANNUITY_GEM)
							{
								PlayerProfile.GemAnnuityExpiration = item.Value.Expiration;
							}
                            break;
                        default:
                            if (item.Value.ItemId == NovaIAPService.ANNUITY_STEAM)
                            {
                                PlayerProfile.SteamAnnuityExpiration = item.Value.Expiration;
                            }
                            break;
                    }
                }

                this.PlayerProfile.OwnedKeys = keys;
                this.PlayerProfile.OwnedAvatars = avatars;
                this.PlayerProfile.OwnedCardBacks = cardBacks;
                this.PlayerProfile.OwnedPlaymats = playmats;
                this.PlayerProfile.BoughtStoreItems = storeItems;
                this.PlayerProfile.Inventory = PlayerDataResponse.Inventory.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                
                isInventoryUpdated = true;
                Debug.Log("PDR: Object Inventory");
            }

            if (PlayerDataResponse.Wallet != null)
            {
                foreach (var kvp in PlayerDataResponse.Wallet)
                {
                    this.PlayerProfile.Wallet.Currencies[kvp.Key] = kvp.Value;
                    Debug.LogFormat("PDR. Balance for {0} set to {1}", kvp.Key, kvp.Value);
					/*if (kvp.Value > 0)
					{
						this.LogEventSignal.Dispatch (new LogEventParams {
							EventName = LogEventParams.EVENT_CURRENCY_CHANGE,
							CurrencyType = kvp.Key,
							CurrencyBalance = kvp.Value,
						});
					}*/
                }
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Currency);
                Debug.Log("PDR: Wallet");
            }

            if (isInventoryUpdated)
            {
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Inventory);
            }

            if (PlayerDataResponse.PlayerStats != null)
            {
                this.PlayerProfile.UserStatistics = (Dictionary<string,int>)PlayerDataResponse.PlayerStats;
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Statistics);
                MetricsTrackingService.GA_SetRank();
                isStatsUpdated = true;
                Debug.Log("PDR: Player Stats");
            }
            if(isStatsUpdated)
            {
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.Statistics);
            }



            // Updates to tournament, onboarding, rating data

            if (PlayerDataResponse.OnboardingProgress != null)
            {
                PlayerProfile.OnboardingProgress = PlayerDataResponse.OnboardingProgress;
                isTournamentDataUpdated = true;
                //Debug.Log("PDR: Onboarding progress");
            }
            if (PlayerDataResponse.RatingBaseData != null)
            {
                PlayerProfile.RatingObject = PlayerDataResponse.RatingBaseData;
                isTournamentDataUpdated = true;
                //Debug.Log("PDR: Ratings");
            }
            if (PlayerDataResponse.DraftProgress != null)
            {
                PlayerProfile.DraftProgress = PlayerDataResponse.DraftProgress;

                isTournamentDataUpdated = true;
                //Debug.Log("PDR: Draft Progress");
            }
            if (PlayerDataResponse.DraftStatus != null)
            {
                PlayerProfile.DraftData = PlayerDataResponse.DraftStatus;
                isTournamentDataUpdated = true;
                //Debug.Log("PDR: Draft Status");
            }
            if (PlayerDataResponse.ConstructedStatus != null)
            {
                PlayerProfile.ConstructedData = PlayerDataResponse.ConstructedStatus;
                isTournamentDataUpdated = true;
                //Debug.Log("PDR: Constructed Staus");
            }
            if (PlayerDataResponse.TournamentStatus != null)
            {
                PlayerProfile.TournamentData = PlayerDataResponse.TournamentStatus;
                isTournamentDataUpdated = true;
                //Debug.Log("PDR: tournament staus");
            }
            if (PlayerDataResponse.TournamentSchedule != null || PlayerDataResponse.EventTypes != null)
            {
                if (PlayerDataResponse.TournamentSchedule != null)
                {
                    GameData.TournamentSchedule = (Dictionary<DateTime, TournamentEventData>)PlayerDataResponse.TournamentSchedule;
                }
                if (PlayerDataResponse.EventTypes != null)
                {
                    GameData.TournamentDataCache = (Dictionary<string, TournamentEventData>)PlayerDataResponse.EventTypes;
                }
                isTournamentDataUpdated = true;
                //Debug.Log("PDR: Tournament schedule");
            }
            if (isTournamentDataUpdated)
            {
                this.ProfileUpdatedSignal.Dispatch(ProfileSection.TournamentData);
            }
            // Base data

            if (PlayerDataResponse.CardData != null)
            {

                Debug.Log("PDR: IMPLEMENT THIS: CardJson updated");
            }


            if (PlayerDataResponse.LegalCardIDs != null && PlayerDataResponse.LegalCardIDs.Count > 0)
            {
                this.GameData.LegalCards.Clear();
                foreach (var id in PlayerDataResponse.LegalCardIDs)
                    this.GameData.LegalCards.Add(id);
            }

            // catalog data
            if (PlayerDataResponse.AllCatalogItems != null)
            {
                this.GameData.ProductCache = PlayerDataResponse.AllCatalogItems as Dictionary<string, NovaCatalogItem>;//.ToDictionary(prod => prod.Key, prod => prod.Value);

                int cardId;
                //List<string> toRemove = new List<string>();
                foreach (var kvp in GameData.ProductCache)
                {
                    if (kvp.Value.ItemClass == NBEconomy.ITEMCLASS_CARD)
                    {
                        if (int.TryParse(kvp.Key, out cardId) && this.GameData.LegalCards.Contains(cardId))
                        {
                            this.GameData.CardProductCache[cardId] = kvp.Value;
                        }
                        else
                            Debug.Log("card failed: " + kvp.Key);
                    }
                }
                this.GameData._catalogCached = true;

                this.PlayerProfile.isAllCatalogsLoaded = true;
                Debug.LogFormat("Full Catalog Loaded: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
                Debug.Log("CatalogJson updated");
            }

            // Store data
            if (PlayerDataResponse.AllStoreItems != null)
            {
                Debug.Log("Got store items");
                foreach (var store in PlayerDataResponse.AllStoreItems)
                {
                    var newStore = new Dictionary<string, StoreData>();
                    foreach(var storeItem in store.Value.StoreData)
                    {

                        NovaCatalogItem pdata; this.GameData.ProductCache.TryGetValue(storeItem.ItemId, out pdata);
                        Product UnityProduct = null;
                        if (NovaIAPService != null
                            && NovaIAPService.Controller != null
                            && NovaIAPService.Controller.products != null
                            && NovaIAPService.Controller.products.all.Count() > 0)
                        {
                            UnityProduct = NovaIAPService.Controller.products.WithID(storeItem.ItemId);
							Debug.LogFormat ("Product for item {0} is {1}", storeItem.ItemId, 
								UnityProduct == null ? "Null" : "id:" + UnityProduct.definition.id + "/storeId:" + UnityProduct.definition.storeSpecificId);
                        }
                        /*UnityIAPSettings.Products.Add(new UnityIAPProduct()
                        {
                            AppleStoreIdentifier = storeItem.ItemId,
                            GoogleStoreIdentifier = storeItem.ItemId,
                            ProductId = storeItem.ItemId,
                            ProductType = ProductType.Consumable
                        }); */

                        var data = new StoreData
                        {
                            StoreId = store.Key,
                            ItemId = storeItem.ItemId,
                            VirtualPrices = storeItem.VirtualCurrencyPrices.Where(vp => vp.Value > 0).ToDictionary(k => k.Key, v => v.Value), //ignore zero price
                            //ProductData = pdata,
                            UnityPurchasingProduct = UnityProduct,
                            ExpiryTime = storeItem.ExpiryTime,
                            tags = storeItem.Tag,
                        };
                        newStore[storeItem.ItemId] = data;
                    }
                    this.GameData.StoreCache[store.Key] = newStore;
                }
                Debug.Log("PDR: Store Data");
            }


            this.Release();
        }


    }
}
