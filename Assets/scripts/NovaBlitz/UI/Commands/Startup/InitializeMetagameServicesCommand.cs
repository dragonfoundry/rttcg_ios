﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class InitializeMetagameServicesSignal : Signal {}
	public class InitializeMetagameServicesCommand : Command 
	{
		[Inject] public MetagameServices MetagameServices {get;set;}
		[Inject] public MetagameSettings MetagameSettings {get;set;}

		public override void Execute ()
		{
			//Debug.Log ("Initializing Metagame Services");
			this.MetagameServices.Init(this.MetagameSettings.GameTitleId);
		}
	}
}

