using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.UI;
using NovaBlitz.Game;
using LinqTools;

public class StartSignal : Signal{}
public class StartCommand : Command 
{
	[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
	[Inject] public InitializeProgressScreenSignal InitializeProgressScreenSignal {get;set;} 
	[Inject] public ViewReferences ViewReferences {get;set;}
	[Inject] public GameData GameData { get; set;}
		[Inject] public INovaContext ContextView {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}

	public override void Execute ()
	{
		this.Retain();
		
		Dictionary<AsyncProgress,string> progressLabels = new Dictionary<AsyncProgress,string>();
		progressLabels[AsyncProgress.NoProgress] = I2.Loc.ScriptLocalization.AsyncProgress.StartingGame;
		progressLabels[AsyncProgress.CardArtPositions] = I2.Loc.ScriptLocalization.AsyncProgress.CardArtPositions;
		progressLabels[AsyncProgress.LoadingAudio] = I2.Loc.ScriptLocalization.AsyncProgress.LoadingAudio;
		progressLabels[AsyncProgress.Authenticating] = I2.Loc.ScriptLocalization.AsyncProgress.Authenticating;
        progressLabels[AsyncProgress.TitleData] = I2.Loc.ScriptLocalization.AsyncProgress.TitleData;
        //progressLabels[AsyncProgress.FriendsList] = I2.Loc.ScriptLocalization.AsyncProgress.FriendsList;
        
		this.InitializeProgressScreenSignal.Dispatch(progressLabels);

        //this.OpenAnimatedViewSignal.Dispatch(typeof(BackgroundMainMenuView));
        
        var versions = Application.version.Split(new char[] { '.' });
        this.GameData.VersionMajor = int.Parse(versions[0]);// gameConfig.VersionMajor;
		this.GameData.VersionMinor = int.Parse(versions[1]);// gameConfig.VersionMinor;
        this.GameData.VersionPatch = int.Parse(versions[2]);// gameConfig.VersionMicro;
        Debug.LogFormat("Version: {0}", this.GameData.VersionLabel);
        Timer.Instance.StartCoroutine(this.WaitForProgressScreen());
    }
	
	private IEnumerator WaitForProgressScreen()
	{
        yield return null;
		/*
        if (null == this.ViewReferences.Get(typeof(TutorialMenuView)))
            this.ViewReferences.Instantiate(typeof(TutorialMenuView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(TutorialMenuView));
        if (null == this.ViewReferences.Get(typeof(DeckListView)))
            this.ViewReferences.Instantiate(typeof(DeckListView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(DeckListView));
        if (null == this.ViewReferences.Get(typeof(TwoTouchOverlayView)))
            this.ViewReferences.Instantiate(typeof(TwoTouchOverlayView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
        if (null == this.ViewReferences.Get(typeof(CardBrowserView)))
            this.ViewReferences.Instantiate(typeof(CardBrowserView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(CardBrowserView));
        if (null == this.ViewReferences.Get(typeof(CardListView)))
            this.ViewReferences.Instantiate(typeof(CardListView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(CardListView));
        if (null == this.ViewReferences.Get(typeof(MainHeaderView)))
            this.ViewReferences.Instantiate(typeof(MainHeaderView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(MainHeaderView));
        if (null == this.ViewReferences.Get(typeof(HomeButtonsView)))
            this.ViewReferences.Instantiate(typeof(HomeButtonsView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(HomeButtonsView));
        if (null == this.ViewReferences.Get(typeof(DeckListDockView)))
            this.ViewReferences.Instantiate(typeof(DeckListDockView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(DeckListDockView));
        if (null == this.ViewReferences.Get(typeof(DraftPackBrowserView)))
            this.ViewReferences.Instantiate(typeof(DraftPackBrowserView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(DraftPackBrowserView));
        if (null == this.ViewReferences.Get(typeof(StoreView)))
            this.ViewReferences.Instantiate(typeof(StoreView), this.ContextView.MainCanvas.transform);
        yield return null;
        //this.CloseAnimatedViewSignal.Dispatch(typeof(StoreView));
        if (null == this.ViewReferences.Get(typeof(ProfileMenuView)))
            this.ViewReferences.Instantiate(typeof(ProfileMenuView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(ProfileMenuView));
        if (null == this.ViewReferences.Get(typeof(FriendListDockView)))
            this.ViewReferences.Instantiate(typeof(FriendListDockView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(FriendListDockView));
        if (null == this.ViewReferences.Get(typeof(DeckStatsView)))
            this.ViewReferences.Instantiate(typeof(DeckStatsView), this.ContextView.MainCanvas.transform);
        yield return null;
        this.CloseAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
        //if (null == this.ViewReferences.Get(typeof(PlayScreenColumnView)))
        //    this.ViewReferences.Instantiate(typeof(PlayScreenColumnView), this.ContextView.MainCanvas.transform);
        //if (null == this.ViewReferences.Get(typeof(ProgressLoadingScreenView)))
        //    this.ViewReferences.Instantiate(typeof(ProgressLoadingScreenView), this.ContextView.transform);*/

        while (this.ViewReferences.Get(typeof(ProgressLoadingScreenView)) == null)
        {
            yield return null;
        }
        this.Release();

    }
}
