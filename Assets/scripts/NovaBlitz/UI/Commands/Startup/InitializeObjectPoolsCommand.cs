using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using System.Collections.Generic;

namespace NovaBlitz.UI
{
	public class InitializeObjectPoolsCommand : Command
	{
		[Inject] public GameData GameData {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public UIConfiguration UIConfiguration {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }

        public override void Execute()
        {
            this.Retain();
            //Debug.Log("Initializing Object Pools");
            Timer.Instance.StartCoroutine(ObjectPoolCoroutine());
        }

        private IEnumerator ObjectPoolCoroutine()
        {
            Debug.LogFormat("Initializing Object Pools: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            yield return new WaitForSecondsRealtime(0.1f);
			//Timer.Instance.StartCoroutine(SpawnInstances());
			int cardPoolSize = this.UIConfiguration.CardPoolSize;
            int objectPoolSize = this.UIConfiguration.UIObjectPoolSize;

            DraggedCardView draggedCardPrefab = this.UIConfiguration.DraggedCardPrefab;
            ObjectPool.CreatePool<DraggedCardView>(draggedCardPrefab);
            ObjectPool.SetPoolSize<DraggedCardView>(draggedCardPrefab, 5);

            //yield return new WaitForSecondsRealtime(0.1f);
            DraggableCardView draggableCardPrefab = this.UIConfiguration.DraggableCardPrefab;
			ObjectPool.CreatePool<DraggableCardView>(draggableCardPrefab);
			ObjectPool.SetPoolSize<DraggableCardView>(draggableCardPrefab, cardPoolSize);

            yield return null;
            // DefaultCardView ObjectPool
            CardView cardViewPrefab = this.UIConfiguration.CardPrefab;
			ObjectPool.CreatePool<CardView>(cardViewPrefab);
            ObjectPool.SetPoolSizeAsync<CardView>(cardViewPrefab, objectPoolSize, 5);

            yield return null;
            yield return null;
            yield return null;
            yield return null;
            yield return null;
            //yield return new WaitForSecondsRealtime(0.1f);
            // AchievementView ObjectPool
            AchievementView achievementPrefab = this.UIConfiguration.AchievementPrefab;
            ObjectPool.CreatePool<AchievementView>(achievementPrefab);
            ObjectPool.SetPoolSizeAsync<AchievementView>(achievementPrefab, 5, 1);

            // QuestView ObjectPool
            QuestView questViewPrefab = this.UIConfiguration.QuestPrefab;
            ObjectPool.CreatePool<QuestView>(questViewPrefab);
            ObjectPool.SetPoolSizeAsync<QuestView>(questViewPrefab, 3, 1);

            //yield return null;
            // QuestView ObjectPool
            LeaderboardItemView leaderboardPrefab = this.UIConfiguration.LeaderboardPrefab;
            ObjectPool.CreatePool<LeaderboardItemView>(leaderboardPrefab);
            ObjectPool.SetPoolSizeAsync<LeaderboardItemView>(leaderboardPrefab, 3, 1);

            yield return null;
            // CardListItemView ObjectPool
            AvatarView avatarPrefab = this.UIConfiguration.AvatarItemPrefab;
            ObjectPool.CreatePool(avatarPrefab);
            ObjectPool.SetPoolSizeAsync(avatarPrefab, 5, 1);
            //Debug.Log ("Card List View Pool Initialized");


            StoreItemView storeItemViewPrefab = this.UIConfiguration.StoreItemPrefab;
            ObjectPool.CreatePool(storeItemViewPrefab);
            ObjectPool.SetPoolSizeAsync(storeItemViewPrefab, 10, 2);

            //yield return new WaitForSecondsRealtime(0.1f);
            // CardListItemView ObjectPool
            CardBackView cardBackViewPrefab = this.UIConfiguration.CardBackViewPrefab;
            ObjectPool.CreatePool(cardBackViewPrefab);
            ObjectPool.SetPoolSizeAsync(cardBackViewPrefab, 5, 1);
            //Debug.Log ("Card List View Pool Initialized");

            yield return null;
            yield return null;
            // CardListItemView ObjectPool
            CardListItemView cardListItemPrefab = this.UIConfiguration.CardListItemPrefab;
			ObjectPool.CreatePool(cardListItemPrefab);
            ObjectPool.SetPoolSizeAsync(cardListItemPrefab, objectPoolSize, 5);
            //Debug.Log ("Card List View Pool Initialized");

            //yield return new WaitForSecondsRealtime(0.1f);
            CardListItemProxyView cardListItemProxyPrefab = this.UIConfiguration.CardListItemProxyPrefab;
            ObjectPool.CreatePool(cardListItemProxyPrefab);
            ObjectPool.SetPoolSizeAsync(cardListItemProxyPrefab, objectPoolSize, 5);

            yield return null;
            yield return null;
            yield return null;
            yield return null;
            yield return null;
            UpdatedPrizeView prizePrefab = (Resources.Load("PrizePrefab") as GameObject).GetComponent<UpdatedPrizeView>();
            this.GameData.PrizePrefab = prizePrefab;
            ObjectPool.CreatePool(prizePrefab);
            ObjectPool.SetPoolSizeAsync(prizePrefab, 5, 5);

            //yield return new WaitForSecondsRealtime(0.1f);
            // Arena Card Pool
            CardFrontController arenaCardPrefab = (Resources.Load("Prefabs/ArenaCard") as GameObject).GetComponent<CardFrontController>();
            this.GameData.ArenaCardPrefab = arenaCardPrefab;
            arenaCardPrefab.CanvasGroup.alpha = 0;
            ObjectPool.CreatePool<CardFrontController>(this.GameData.ArenaCardPrefab);
            ObjectPool.SetPoolSizeAsync(arenaCardPrefab, cardPoolSize,5);

            yield return null;
            yield return null;
            yield return null;
            yield return null;
            yield return null;
            // CardBack Pool
            CardBackController cardBackPrefab = (Resources.Load("Prefabs/ArenaCardBack") as GameObject).GetComponent<CardBackController>();
            this.GameData.CardBackPrefab = cardBackPrefab;
            cardBackPrefab.CanvasGroup.alpha = 0;
            ObjectPool.CreatePool<CardBackController>(this.GameData.CardBackPrefab);
            ObjectPool.SetPoolSizeAsync(cardBackPrefab, cardPoolSize,5);

            //yield return new WaitForSecondsRealtime(0.1f);
            // FxTrigger Pool
            FxTrigger pooledFxPrefab = this.UIConfiguration.PooledFxPrefab;
			ObjectPool.CreatePool<FxTrigger>(pooledFxPrefab);
            FxTrailTrigger pooledTrailFxPrefab = this.UIConfiguration.PooledTrailFxPrefab;
            ObjectPool.CreatePool<FxTrailTrigger>(pooledTrailFxPrefab);
            
            // CardListItemView ObjectPool
            DeckListItem deckListPrefab = this.UIConfiguration.DeckListItemPrefab;
            ObjectPool.CreatePool(deckListPrefab);
            ObjectPool.SetPoolSizeAsync(deckListPrefab, 20, 5);
            //Debug.Log ("Card List View Pool Initialized");

            yield return null;
            yield return null;
            yield return null;
            yield return null;
            yield return null;
            //yield return new WaitForSecondsRealtime(0.1f);
            // CardListItemView ObjectPool
            FriendListItem friendListItemPrefab = this.UIConfiguration.FriendListItemPrefab;
            ObjectPool.CreatePool(friendListItemPrefab);
            ObjectPool.SetPoolSizeAsync(friendListItemPrefab, 5, 5);
            
            // CardListItemView ObjectPool
            ChatLogEntry globalChatItemPrefab = this.UIConfiguration.GlobalChatPrefab;
            ObjectPool.CreatePool(globalChatItemPrefab);
            // not adding any pool members here; we'll instantiate as needed
            //ObjectPool.SetPoolSizeAsync(globalChatItemPrefab, 100, 5);

            yield return null;
            yield return null;
            yield return null;
            yield return null;
            yield return null;
            this.Release();
            //ObjectPool.SetPoolSizeAsync(cardViewPrefab, GameData.CardDictionary.Count, 1);

            Debug.LogFormat("All object pools created: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            PlayerProfile.isAllObjectPoolsCreated = true;
        }
	}
}
