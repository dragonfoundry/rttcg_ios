﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using PlayFab.ClientModels;
using PlayFab;

namespace NovaBlitz.UI
{
    public class EmailListSignupSignal : Signal<EmailListParams> { }
    public class EmailListParams
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string PlayFabId { get; set; }
        public Platform Platform { get; set; }

        public EmailListParams(string email, string username, string playFabId, Platform platform)
        {
            Email = email;
            Username = username;
            PlayFabId = playFabId;
            Platform = platform;
        }
    }

    public enum Platform
    {
        NoPlatform = 0,
        Steam = 1,
        Windows = 2,
        IOS = 3,
        Android = 4
    }

    public class EmailListSignupCommand : Command
    {
        [Inject] public EmailListParams EmailListParams { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }

        public override void Execute()
        {
            if (string.IsNullOrEmpty(EmailListParams.Email))
                return;
            // Don't do this if we don't actually have email AND username.
            Timer.Instance.StartCoroutine(SendToMailChimp(EmailListParams));
        }

        private IEnumerator SendToMailChimp(EmailListParams emailParams)
        {
            emailParams.PlayFabId = this.PlayerProfile.MetaProfile.PlayFabID;
            int i = 0;
            while((!PlayerProfile.MetaProfile.IsNameSet || string.IsNullOrEmpty(this.PlayerProfile.MetaProfile.PlayFabID)) && i < 60)
            {
                yield return new WaitForSecondsRealtime(1);
                i++;
            }
            emailParams.Username = this.PlayerProfile.MetaProfile.Name;
            emailParams.PlayFabId = this.PlayerProfile.MetaProfile.PlayFabID;
            var form = new WWWForm();
            form.AddField("EMAIL", emailParams.Email);
            form.AddField("UNAME", emailParams.Username);
            form.AddField("PLAYFABID", emailParams.PlayFabId);
            switch (emailParams.Platform)
            {
                case Platform.Android:
                    form.AddField("ANDROID", 1);
                    break;
                case Platform.IOS:
                    form.AddField("IOS", 1);
                    break;
                case Platform.Steam:
                    form.AddField("STEAM", 1);
                    break;
                case Platform.Windows:
                    form.AddField("WIN", 1);
                    break;
            }
            form.AddField("b_8855252b454d05c2f913c1d2b_2ac022b22a", string.Empty);
            UnityWebRequest w = UnityWebRequest.Post("http://novablitz.us9.list-manage.com/subscribe/post?u=ee104d48f69d370c1738ff919&amp;id=b9e47e82e8", form);
            
            yield return w.Send();

            if (w.isNetworkError)
            {
                Debug.Log("Error sending the form: " + w.error);
            }
            else
            {
                Debug.Log("Form sent without errors");
            }
        }
    }
}
