﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using LinqTools;


namespace NovaBlitz.UI
{
	public class OpenBlurredAnimatedViewSignal : Signal<System.Type> {}
	public class OpenBlurredAnimatedViewCommand : Command
	{
		// Signal Parameters
		[Inject] public System.Type ViewType {get;set;}

		// Injected Dependencies
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public INovaContext NovaContext{get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public ViewStateStack ViewStateStack{get;set;}


		/// <summary>
		//  Blurs the main camera and opens the specified view on the overlay canvas
		/// </summary>
		public override void Execute ()
		{
			//Debug.LogFormat ("Blurred Opening {0}", ViewType);

			AnimatedView view = this.ViewReferences.Get(this.ViewType) as AnimatedView;
			if(view == null)
			{
				view = (AnimatedView)this.ViewReferences.Instantiate(this.ViewType, this.NovaContext.OverlayCanvas.transform);
			}

			// Check to see if the current viewState is already owned by this view
			ViewState currentState = this.ViewStateStack.GetCurrentState();
			if(currentState == null || view != currentState.Views.FirstOrDefault())
			{
				// Only add a new blurred viewState if thew view being opened is different that the one that blurred the current state
				this.ViewStateStack.PushViewState(view);
				this.MoveAnimatedViewToCanvasSignal.Dispatch(view,NovaCanvas.Overlay, 0.0f);

				if(currentState != null)
				{
					// If there is a viewstate already, move its views to the main canvas
					for(int i=0;i<currentState.Views.Count;i++)
					{
						AnimatedView stateView = (AnimatedView)currentState.Views[i];
						if(stateView != view)
						{
							this.MoveAnimatedViewToCanvasSignal.Dispatch(stateView,NovaCanvas.Main, 0.0f);
						}
					}
				}
			}

			this.OpenAnimatedViewSignal.Dispatch(this.ViewType);
            Timer.Instance.StartCoroutine(BlurMain(view));
		}

        private IEnumerator BlurMain(AnimatedView view)
        {
            yield return null;
            this.NovaContext.BlurMainCanvas(true, view);
        }
	}
}
