﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using DG.Tweening;

namespace NovaBlitz.UI
{
	public class CloseAnimatedViewSignal : Signal<System.Type> {}
	public class CloseAnimatedViewCommand : Command
	{
		// Signal Parameters
		[Inject] public System.Type ViewType {get;set;}
		
		// Injected Dependencies
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public ViewStateStack ViewStateStack {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}

		public override void Execute ()
        {
            Debug.LogFormat("Closing {0}", ViewType);
            AnimatedView view = this.ViewReferences.Get(this.ViewType) as AnimatedView;
			if (null != view && view.IsOpen)
			{
				view.IsOpen = false;

                ViewState states;
				// Does closing this view remove the current viewstate?
				if(this.ViewStateStack.TryToPopViewState(view, out states))
				{
                    if(states != null && states.Views != null)
                    {
                        foreach(var state in states.Views)
                        {
                            this.MoveAnimatedViewToCanvasSignal.Dispatch(state, NovaCanvas.Main, 1.0f);
                        }
                    }

					// Is the view state revealed by closing the current viewstate a blurred state?
					ViewState currentViewState = this.ViewStateStack.GetCurrentState();
					if(currentViewState != null && this.ViewStateStack.Count > 1)
					{
						// If so make all the views in that state be on the overlay layer
						for(int i=0;i<currentViewState.Views.Count;i++)
						{
							AnimatedView stateView = (AnimatedView)currentViewState.Views[i];
    						this.MoveAnimatedViewToCanvasSignal.Dispatch(stateView,NovaCanvas.Overlay, 0.0f);
						}
					}
				}

				if(this.ViewStateStack.Count <= 1)
				{
					// Only one viewstate remains and it cannot be blurred
					this.NovaContext.BlurMainCanvas(false,view);
				}
			}
            if (view != null)
            {
                view.DOKill();
                DOTween.To(x => 
					{ 
						if (x == 1.0f && view != null && !view.IsOpen) 
						{
							view.gameObject.SetActive(false);
							view.transform.SetParent(this.NovaContext.HoldingCanvas.transform);
						} 
					}, 0.0f, 1.0f, 2.0f);
            }
		}
	}
}