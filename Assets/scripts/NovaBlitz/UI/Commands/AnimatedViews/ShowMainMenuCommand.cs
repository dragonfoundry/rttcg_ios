﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using LinqTools;
using NovaBlitz.Game;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
	public class ShowMainMenuSignal : Signal<ShowMainMenuParams>{}
	public class ShowMainMenuCommand : Command 
	{
        // Signal Params
        [Inject] public ShowMainMenuParams ShowMainMenuParams { get; set; }

        // Injected Dependencies
        [Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get;set;}
        [Inject] public ShowPurchaseOffersSignal ShowPurchaseOffersSignal { get; set; }
        [Inject] public RateGameSignal RateGameSignal { get; set; }
        [Inject] public ScreenTransitionSignal ScreenTransitionSignal {get;set;}
		[Inject] public INovaContext ContextView {get;set;}
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public SelectDeckSignal SelectDeckSignal { get; set; }
        [Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public ClientService ClientService { get; set; }
        [Inject] public HomeScreen HomeScreen { get; set; }
        [Inject] public InitializeViewSignal InitializeViewSignal {get;set;}
        [Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public SaveChatHistorySignal SaveChatHistorySignal { get; set; }
		[Inject] public ConnectToPhotonChatSignal ConnectToPhotonChatSignal {get;set;}
        [Inject] public RejoinGameSignal RejoinGameSignal { get; set; }

        [Inject] public GameData GameData { get; set; }
        
        [Inject] public NovaAudio NovaAudio { get; set; }

        public override void Execute()
        {
            Debug.LogFormat("ShowMainMenu Start: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);

            Screen.sleepTimeout = (int)SleepTimeout.SystemSetting;
            if(ShowMainMenuParams.IsCancelMatchmaking)
            {
                // all we need to do here is close the progress loading screen.
                CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
                return;
            }


            this.Retain();
            this.OpenAnimatedViewSignal.Dispatch(typeof(BackgroundMainMenuView));
            if (!PlayerProfile.OnboardingProgress.TutorialsCompleted)
            {
                Timer.Instance.StartCoroutine(OpenMainMenuCoroutine());
            }
            else if(!PlayerProfile.MetaProfile.IsNameSet)
            {
                // Player is logged in, but doesn't have their email/username/password set
                Timer.Instance.StartCoroutine(OpenSignupCoroutine());
            }
            else if(PlayerProfile.GameToRejoin != Guid.Empty)
            {
                // We're in the middle of a running game. Rejoin it.
                Timer.Instance.StartCoroutine(RejoinGameCoroutine());
                return;
            }
            else
            {
				// Send a signal to show special prizes, or show the pack opener.
				Timer.Instance.StartCoroutine (OpenMainMenuCoroutine ());
            }
            Timer.Instance.StartCoroutine(PlayMainMenuMusicCoroutine());
        }

        public IEnumerator RejoinGameCoroutine()
        {
            while (!this.PlayerProfile.IsFinishedDataLoading || !AssetBundleManager.Instance.IsAllBundlesLoaded)
            {
                yield return null;
            }
            // need to:
            // Build an arena
            // Tell the server we're coming back
            // rejoin the game
            // get the arena state
            //
            if (PlayerProfile.GameToRejoin != Guid.Empty)
            {
                Timer.Instance.StartCoroutine(PlayThemeMusicCoroutine());
                ObjectPool.SetPoolSizeAsync(UIConfiguration.PooledFxPrefab, 6, 6);
                RejoinGameSignal.Dispatch(new CreateGameRequest { GameToRejoin = PlayerProfile.GameToRejoin } );
            }
            else
            {
                Timer.Instance.StartCoroutine(OpenMainMenuCoroutine());
                Timer.Instance.StartCoroutine(PlayMainMenuMusicCoroutine());
            }
        }
        

    public IEnumerator PlayThemeMusicCoroutine()
        {
            while (!AssetBundleManager.Instance.AssetBundleData.ContainsKey("novaaudio.unity3d"))
            {
                yield return null;
            }
            this.NovaAudio.CrossfadeToMusicTheme();
        }

        public IEnumerator PlayMainMenuMusicCoroutine()
        {
            while(!AssetBundleManager.Instance.AssetBundleData.ContainsKey("novaaudio.unity3d"))
            {
                yield return null;
            }
            this.NovaAudio.CrossfadeToMusicMenu();
        }

        public IEnumerator OpenSignupCoroutine()
        {
            Debug.Log("Initializing login");
            this.InitializeAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView), (v) => {
                ((LoginDialogNewFlowView)v).Initialize(new LoginDialogSettings
                {
                    IsAddUsernameAndPassword = true,
                    IsAgeGatePassed = this.PlayerProfile.isAgeGatePassed,
                    IsCancelable = false,
                    SteamName = this.ClientPrefs.SteamName,
                });
            });
            yield return null;
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));
            this.Release();
        }

        private void PreloadView(System.Type type)
        {
            if (null == this.ViewReferences.Get(type))
                this.ViewReferences.Instantiate(type, this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(type);
        }

        public IEnumerator OpenMainMenuCoroutine()
        {
            Debug.Log("Opening main menu");
            this.CloseAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));
            yield return null;
            PreloadView(typeof(MainHeaderView));
            yield return null;
            PreloadView(typeof(DeckListDockView));
            yield return null;
            PreloadView(typeof(DeckListView));
            yield return null;
            PreloadView(typeof(PlayScreenColumnView));
            yield return null;
            PreloadView(typeof(TwoTouchOverlayView));
            yield return null;
            PreloadView(typeof(StoreView));
            yield return null;
            PreloadView(typeof(SettingsView));
            yield return null;
            PreloadView(typeof(ProfileMenuView));
            yield return null;

            // Set "in game" to false - 
            this.ClientService.IsInGame = false;
            
            while (!this.PlayerProfile.IsFinishedDataLoading)
            {
                yield return null;
            }

            if (!PlayerProfile.OnboardingProgress.TutorialsCompleted)
            {
                while (!AssetBundleManager.Instance.IsPriotityZeroBundlesLoaded)
                {
                    yield return null;
                }
                // ensure we don't give the player an offer view when they complete the tutorials - they won't get offers for 45 minutes
                this.PlayerProfile.OffersLastSeen = Server.Time.AddMinutes(-40);
                this.PlayerProfile.PackOpenerLastSeen = Server.Time.AddYears(-40);

                this.ScreenTransitionSignal.Dispatch(new ScreenTransitionParams
                {
                    FromScreenType = null,
                    ToScreenType = typeof(PlayScreen),
                    Format = GameFormat.Tutorial
                });

                yield return new WaitForSecondsRealtime(1.5f);

                this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));
                //this.ClientService.IsInGame = false;

                // IMPORTANT - just early out here, as none of the other views are important in the tutorial.
                yield break;
            }
            else
			{
				Debug.LogFormat("Wait for asset bundle load: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
                // If we're not in the tutorial, we must wait for asset bundle loads to complete.
                while(!AssetBundleManager.Instance.IsAllBundlesLoaded)
                {
                    yield return null;
                }
				Debug.LogFormat("All asset bundles loaded: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            }

            PreloadView(typeof(CardBrowserView));
            yield return null;
            PreloadView(typeof(CardListView));
            yield return null;
            PreloadView(typeof(HomeButtonsView));
            yield return null;
            PreloadView(typeof(DraftPackBrowserView));
            yield return null;
            
            HomeScreen.RegisterChildren(); // need this so friends & header work, even on other screens.



            FriendListDockView friendListDockView = (FriendListDockView)this.ViewReferences.Get(typeof(FriendListDockView));
            if (friendListDockView == null)
            {
                //Debug.Log("Creating FriendListDock View");
                friendListDockView = (FriendListDockView)this.ViewReferences.Instantiate(typeof(FriendListDockView), this.ContextView.MainCanvas.transform);
            }
            // re-initialize the friend list
            //friendListDockView.Initialize(this.PlayerProfile.FriendListData, this.PlayerProfile.MetaProfile.ID, this.PlayerProfile.isAgeGatePassed);
            //yield return null;
            //friendListDockView.UpdateFriendList(this.PlayerProfile.FriendListData);
            this.CloseAnimatedViewSignal.Dispatch(typeof(FriendListDockView));

            // Select a deck, either the last-selected one, or the first in the list
            if (PlayerProfile.CurrentDeck != null)
                SelectDeckSignal.Dispatch(PlayerProfile.CurrentDeck);
            else if (PlayerProfile.DeckList != null && PlayerProfile.DeckList.Count > 0)
                SelectDeckSignal.Dispatch(PlayerProfile.DeckList[0]);

            this.ScreenTransitionSignal.Dispatch(new ScreenTransitionParams
            {
                FromScreenType = null,
                ToScreenType = PlayerProfile.LastMenuScreenViewed == null
                    || ShowMainMenuParams.Format == GameFormat.Challenge
                    || ShowMainMenuParams.IsForceHomeScreen
                        ? typeof(HomeScreen)
                        : PlayerProfile.LastMenuScreenViewed,
                Format = PlayerProfile.LastPlayScreenFormat// ShowMainMenuParams.Format
            });


            //yield return null;

            //this.InitializeViewSignal.Dispatch(typeof(DeckListView));
            yield return null;
            this.SaveChatHistorySignal.Dispatch();
            this.ConnectToPhotonChatSignal.Dispatch(string.Empty, new ChatMessage(), false);


			yield return null;
            if(ShowMainMenuParams.Format != GameFormat.NoGame)
            {
                yield return new WaitForSecondsRealtime(1.0f);
				if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
					yield return new WaitForSecondsRealtime(1.0f);
            }

			// Close the progress loading screen, if it's open
			this.CloseAnimatedViewSignal.Dispatch(typeof(ProgressLoadingScreenView));

			// Either open a blurred pack opener, or un-blur the screen.
			// If the end game view is open, let it do all this stuff - it's displaying onboarding prizes.
			var endgame = (EndGameDialogView)ViewReferences.Get(typeof(EndGameDialogView));
            if (!PlayerProfile.OnboardingProgress.TutorialsCompleted) { }
            else if (endgame == null || !endgame.IsOpen)
            {

                // check for a "rate game" flow. Priority: Events, win streak, Level Up.
                if (ShowMainMenuParams.HasCompletedKeyQuest)
                {
                    this.RateGameSignal.Dispatch(RateGamePoint.DraftQuest);
                }
                else if (ShowMainMenuParams.HasWon && ShowMainMenuParams.HasEndedEvent && ShowMainMenuParams.EventWins >= 5)
                {
                    this.RateGameSignal.Dispatch(RateGamePoint.EventEnd);
                }
                else if (ShowMainMenuParams.HasWon && ShowMainMenuParams.WinStreak >= 3)
                {
                    this.RateGameSignal.Dispatch(RateGamePoint.WinStreak);
                }
                else if (ShowMainMenuParams.HasWon && ShowMainMenuParams.HasLeveledUp && ShowMainMenuParams.Level == 18)
                {
                    this.RateGameSignal.Dispatch(RateGamePoint.LevelUp);
                }

                var msgView = (NewMessageDialogView)this.ViewReferences.Get(typeof(NewMessageDialogView));
                
                // Pop the pack opener, if there's no rating flow open
                if((msgView == null || !msgView.IsOpen) && ShowMainMenuParams.Format != GameFormat.Tutorial)
                {
                    bool showOffers = false;
                    foreach(var offer in this.PlayerProfile.GetOffers(3, true))
                    {
                        if(this.PlayerProfile.OffersLastSeen.AddMinutes(83) < Server.Time || (offer.StoreId == NBEconomy.STORE_OFFER && offer.ExpiryTime > Server.Time.AddYears(-5) && offer.ExpiryTime <= Server.Time))
                        {
                            showOffers = true;
                        }
                    }

                    if (showOffers)
                    {
                        this.PlayerProfile.OffersLastSeen = Server.Time;
                        this.ShowPurchaseOffersSignal.Dispatch(OfferType.SpecialOffer);
                    }
                    else if (ShowMainMenuParams.PacksToOpen > 0 && this.PlayerProfile.PackOpenerLastSeen.AddMinutes(45) < Server.Time)// !this.PlayerProfile.OffersViewedThisSession.Contains("packopener"))
                    {
                        //this.PlayerProfile.OffersViewedThisSession.Add("packopener");
                        this.PlayerProfile.PackOpenerLastSeen = Server.Time;
                        this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(PackOpenerConfirmView));
                    }
                }
            }

			this.Release ();
			Debug.LogFormat("ShowMainMenu Complete: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
		}
	}
    
    public class ShowMainMenuParams
    {
        public bool IsStartup = false;
        public int PacksToOpen = 0;
        public bool HasWon = false;
        public int WinStreak = 0;
        public int Level = 0;
        public bool HasLeveledUp = false;
        public bool HasEndedEvent = false;
        public bool HasCompletedKeyQuest = false;
        public int EventWins = 0;
        public bool IsForceHomeScreen = false;
        public GameFormat Format = GameFormat.NoGame;
        public bool IsCancelMatchmaking = false;
    }
}
