﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using Messages;
using LinqTools;
using NovaBlitz.Game;

namespace NovaBlitz.UI
{
    public class InitializeBaseViewsSignal : Signal { }
    public class InitializeBaseViewsCommand : Command
    {
        [Inject] public ViewReferences ViewReferences { get; set; }
		[Inject] public INovaContext ContextView {get;set;}
        [Inject] public HomeScreen HomeScreen { get; set; }
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
	    [Inject] public ClientService ClientService {get;set;}
        [Inject] public UnloadUnusedResourcesSignal UnloadUnusedResourcesSignal { get; set; }

        public override void Execute()
        {
            this.Retain();
            Timer.Instance.StartCoroutine(InitializeViewsCoroutine());
        }

        private IEnumerator InitializeViewsCoroutine()
        {
            yield return null;
            while (!ClientService.IsConnected)
            {
                yield return null;
            }
            yield return null;
            if (null == this.ViewReferences.Get(typeof(DeckListView)))
                this.ViewReferences.Instantiate(typeof(DeckListView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(DeckListView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(CardBrowserView)))
                this.ViewReferences.Instantiate(typeof(CardBrowserView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(CardBrowserView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(CardListView)))
                this.ViewReferences.Instantiate(typeof(CardListView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(CardListView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(MainHeaderView)))
                this.ViewReferences.Instantiate(typeof(MainHeaderView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(MainHeaderView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(HomeButtonsView)))
                this.ViewReferences.Instantiate(typeof(HomeButtonsView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(HomeButtonsView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(DeckListDockView)))
                this.ViewReferences.Instantiate(typeof(DeckListDockView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(DeckListDockView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(DeckListView)))
                this.ViewReferences.Instantiate(typeof(DeckListView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(DeckListView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(DraftPackBrowserView)))
                this.ViewReferences.Instantiate(typeof(DraftPackBrowserView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(DraftPackBrowserView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(PlayScreenColumnView)))
                this.ViewReferences.Instantiate(typeof(PlayScreenColumnView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(PlayScreenColumnView));
            yield return null;
            if (null == this.ViewReferences.Get(typeof(TwoTouchOverlayView)))
                this.ViewReferences.Instantiate(typeof(TwoTouchOverlayView), this.ContextView.MainCanvas.transform);
            this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
            yield return null;

            //HomeScreen.RegisterChildren(); // need this so friends & header work, even on other screens.



            FriendListDockView friendListDockView = (FriendListDockView)this.ViewReferences.Get(typeof(FriendListDockView));
            if (friendListDockView == null)
            {
                //Debug.Log("Creating FriendListDock View");
                friendListDockView = (FriendListDockView)this.ViewReferences.Instantiate(typeof(FriendListDockView), this.ContextView.MainCanvas.transform);
            }
            this.CloseAnimatedViewSignal.Dispatch(typeof(FriendListDockView));
            yield return null;
            this.UnloadUnusedResourcesSignal.Dispatch();

            Debug.LogFormat("Base Views Completed: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            this.Release();
        }
    }
}
