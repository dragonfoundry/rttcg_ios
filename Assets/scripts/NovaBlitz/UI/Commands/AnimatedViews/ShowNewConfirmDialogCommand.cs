﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using strange.extensions.context.impl;

namespace NovaBlitz.UI
{
	public delegate void DialogDismissedDelegate(bool isConfirmed);
	public struct NewConfirmDialogParams
	{
		public NewConfirmDialogParams(string title, string prompt, DialogDismissedDelegate handler, bool isWaitForResponse)
		{
			this.Title = title;
			this.Prompt = prompt;
			this.DialogDismissedHandler = handler;
            this.IsWaitForResponse = isWaitForResponse;
		}
		public string Title;
		public string Prompt;
		public DialogDismissedDelegate DialogDismissedHandler;
        public bool IsWaitForResponse;
	}
	
	public class NewConfirmDialogDismissedSignal : Signal<bool> {}
	public class ShowNewConfirmDialogSignal : Signal<NewConfirmDialogParams> {}
    public class ShowNewConfirmDialogCommand : Command
    {
		// Signal Parameters
		[Inject] public NewConfirmDialogParams Params {get;set;}
		
		// Injected Dependencies
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public INovaContext ContextView {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
		[Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal {get;set;}
		[Inject] public NewConfirmDialogDismissedSignal NewConfirmDialogDismissedSignal {get;set;}
        [Inject] public ServerResponseSignal ServerResponseSignal { get; set; }
		
		public override void Execute()
		{
			this.Retain();
			this.InitializeAnimatedViewSignal.Dispatch(typeof(NewConfirmDialogView), 
				(v)=>{
					((NewConfirmDialogView)v).Initialize(this.Params.Title, this.Params.Prompt, this.Params.IsWaitForResponse);
				});
			this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(NewConfirmDialogView));
			this.NewConfirmDialogDismissedSignal.AddOnce(OnConfirmDialogDismissed);
		}
		
		private void OnConfirmDialogDismissed(bool isConfirmed)
		{
			this.Params.DialogDismissedHandler.Invoke(isConfirmed);
            if (Params.IsWaitForResponse)
            {
                this.ServerResponseSignal.AddOnce(OnServerResponse);
            }
            else
                this.Release();

		}

        private void OnServerResponse(bool success)
        {
            this.Release();
        }
    }
}
