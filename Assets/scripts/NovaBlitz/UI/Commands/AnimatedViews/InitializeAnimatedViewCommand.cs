﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using strange.extensions.context.impl;

namespace NovaBlitz.UI
{
	public delegate void InitializeDelegate(AnimatedView view);
	public class InitializeAnimatedViewSignal : Signal<System.Type,InitializeDelegate> {}
	public class InitializeAnimatedViewCommand : Command
	{
		// Signal Parameters
		[Inject] public System.Type ViewType {get;set;}
		[Inject] public InitializeDelegate Initialize {get;set;}
		[Inject] public INovaContext NovaContext {get;set;}

		// Injected Dependencies
		[Inject] public ViewReferences ViewReferences {get;set;}

		public override void Execute ()
		{
			AnimatedView view = this.ViewReferences.Get(this.ViewType) as AnimatedView;
			if(view == null)
			{
				view = this.ViewReferences.Instantiate(this.ViewType, this.NovaContext.MainCanvas.transform) as AnimatedView;
			}

			// Pass thew view to the initialize method and let it do its thing
			this.Initialize(view);
		}
	}
}