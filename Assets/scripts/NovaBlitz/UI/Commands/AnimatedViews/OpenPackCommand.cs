using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
	public class PackOpenedSignal : Signal {}
	public class OpenPackSignal : Signal<OpenPackResponse> {}
	public class OpenPackCommand : Command
	{
        [Inject] public OpenPackResponse response { get; set; }

        [Inject] public GameData GameData { get; set; }
		[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public ViewReferences ViewReferences { get; set; }
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
		[Inject] public PackOpenedSignal PackOpenedSignal { get; set; }
        [Inject] public InventoryLoadedSignal InventoryLoadedSignal { get; set; }

		public override void Execute()
		{
			/*this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(OpenPackView));  
			var openPackView = this.ViewReferences.Get<OpenPackView>();
            if (openPackView == null || !openPackView.gameObject.activeSelf)
            {
                this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(OpenPackView));
                openPackView = this.ViewReferences.Get<OpenPackView>();
            }*/

            if (null == this.PlayerProfile.UnopenedPack) {
				Debug.LogWarning("No pack in local cache!");
				return;
			}

            // Populate the pack opener display
			var cards = this.PlayerProfile.UnopenedPack.CardIDs.Select(id => {
				CardData cdata;
				if (!GameData.CardDictionary.TryGetValue(id, out cdata))
					Debug.LogWarningFormat("OpenPack: purchased card id not found: {0}", id);
				return cdata;
			}).OfType<CardData>().ToList();

            //openPackView.Populate(cards, this.GameData.ArenaCardPrefab, this.PlayerProfile.MetaProfile.CardBackID);
            //openPackView.Play(this.PlayerProfile);

            List<Prize> prizes = new List<Prize>();
            // Add the cards to the player's inventory
            foreach (var cardId in this.PlayerProfile.UnopenedPack.CardIDs)
            {
                PlayerProfile.AddCard(cardId);
                prizes.Add(new Prize { PrizeId = cardId.ToString(), PrizeType = PrizeType.card, PrizeSource = PrizeSource.NoType, Quantity = 1 });
            }

            //var openPrizeView = this.ViewReferences.Get<OpenPrizeView>();
            //if (openPrizeView == null || !openPrizeView.gameObject.activeSelf)
            //{
                this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(OpenPrizeView));
                var openPrizeView = this.ViewReferences.Get<OpenPrizeView>();
            //}

            openPrizeView.Initialize();
            Timer.Instance.StartCoroutine(PopulateView(openPrizeView, prizes, cards));

            this.PlayerProfile.UnopenedPack = null;
			PackOpenedSignal.Dispatch();
            InventoryLoadedSignal.Dispatch();
        }

        private IEnumerator PopulateView(OpenPrizeView openPrizeView, List<Prize> prizes, List<CardData> cards)
        {
            PrizeViewType packType = PrizeViewType.BasicPack;
            switch(response.PackName)
            {
                case NBEconomy.ITEMID_BASIC_PACK:
                    packType = PrizeViewType.BasicPack;
                    break;
                case NBEconomy.ITEMID_SMART_PACK:
                    packType = PrizeViewType.SmartPack;
                    break;
                case NBEconomy.ITEMID_MEGA_PACK:
                    packType = PrizeViewType.MegaPack;
                    break;
            }
            yield return null;
            openPrizeView.Populate(prizes, cards, this.GameData.PrizePrefab, this.GameData.ArenaCardPrefab, this.PlayerProfile.MetaProfile.CardBackID,
                this.GameData, this.PlayerProfile, packType, null);
        }
    }
}
