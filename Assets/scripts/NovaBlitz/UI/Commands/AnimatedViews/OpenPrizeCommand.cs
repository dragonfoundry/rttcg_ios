﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Game;
using Messages;

namespace NovaBlitz.UI
{
    public enum PrizeViewType
    {
        NoType,
        DLC,
        DailyRewards,
        GameEnd,
        Achievement,
        TournamentEnd,
        SeasonEnd,
        BasicPack,
        SmartPack,
        MegaPack,
    }
    
    public class PrizeOpenedSignal : Signal { }
    public class OpenPrizeSignal : Signal<List<Prize>, PrizeViewType, GameOverMessage, ShowMainMenuParams> { }
    public class OpenPrizeCommand : Command
    {
        [Inject] public List<Prize> PrizeList { get; set; }
        [Inject] public PrizeViewType PrizeViewType { get; set; }
        [Inject] public GameOverMessage GameOverMessage { get; set; }
        [Inject] public ShowMainMenuParams ShowMainMenuParams { get; set; }

        [Inject] public GameData GameData { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ViewReferences ViewReferences { get; set; }
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public PrizeOpenedSignal PrizeOpenedSignal { get; set; }
        [Inject] public InventoryLoadedSignal InventoryLoadedSignal { get; set; }
        [Inject] public OpenPrizeSignal OpenPrizeSignal { get; set; }
        [Inject] public ShowMainMenuSignal ShowMainMenuSignal { get; set; }
        
        [Inject] public AsyncProgressSignal AsyncProgressSignal { get; set; }
        [Inject] public InitializeProgressScreenSignal InitializeProgressScreenSignal { get; set; }
		[Inject] public TeardownArenaSignal TeardownArenaSignal {get;set;}
        [Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal { get; set; }

        private PrizeViewType nextPrizeView;

        public override void Execute()
        {
            //Debug.LogFormat("current prize view: {0}", PrizeViewType.ToString());
            // take the right prizes from the prize list
            List<Prize> prizes = new List<Prize>();
            switch(PrizeViewType)
            {
                case PrizeViewType.GameEnd:
                    prizes = PrizeList.Where(p => p.PrizeSource == PrizeSource.casual 
                        || p.PrizeSource == PrizeSource.achievement || p.PrizeSource == PrizeSource.onboardingCard || p.PrizeSource == PrizeSource.quest).ToList();
                    if(prizes.Count > 6)
                    {
                        prizes = prizes.Take(6).ToList();
                        nextPrizeView = PrizeViewType.GameEnd;
                    }
                    else
                        nextPrizeView = PrizeViewType.DailyRewards;
                    break;
                case PrizeViewType.DailyRewards:
                    prizes = PrizeList.Where(p => p.PrizeSource == PrizeSource.annuity
                        || p.PrizeSource == PrizeSource.kickstarterReward).ToList();
                    if (prizes.Count > 6)
                    {
                        prizes = prizes.Take(6).ToList();
                        nextPrizeView = PrizeViewType.DailyRewards;
                    }
                    else
                        nextPrizeView = PrizeViewType.DLC;
                    break;
                case PrizeViewType.DLC:
                    prizes = PrizeList.Where(p => p.PrizeSource == PrizeSource.dailyReward || p.PrizeSource == PrizeSource.dlc).ToList();
                    if (prizes.Count > 6)
                    {
                        prizes = prizes.Take(6).ToList();
                        nextPrizeView = PrizeViewType.DLC;
                    }
                    else
                        nextPrizeView = PrizeViewType.TournamentEnd;
                    break;
                case PrizeViewType.TournamentEnd:
                    prizes = PrizeList.Where(p => p.PrizeSource == PrizeSource.tournament 
                        || p.PrizeSource == PrizeSource.lastWeeksCard 
                        || p.PrizeSource == PrizeSource.thisWeeksCard).ToList();
                    if (prizes.Count > 6)
                    {
                        prizes = prizes.Take(6).ToList();
                        nextPrizeView = PrizeViewType.TournamentEnd;
                    }
                    else
                        nextPrizeView = PrizeViewType.SeasonEnd;
                    break;
                case PrizeViewType.SeasonEnd:
                    prizes = PrizeList.Where(p => p.PrizeSource == PrizeSource.season).ToList();
                    if (prizes.Count > 6)
                    {
                        prizes = prizes.Take(6).ToList();
                        nextPrizeView = PrizeViewType.SeasonEnd;
                    }
                    else
                        nextPrizeView = PrizeViewType.NoType;
                    break;
                default:
                    ShowMainMenu();
                    return;
            }
            
            // Skip showing this step if there's no prizes of this type
            if(prizes == null || prizes.Count == 0)
            {
                //Debug.LogFormat("next prize view: {0}", nextPrizeView.ToString());
                Timer.Instance.StartCoroutine(NextPrizeCoroutine());
                return;
            }

            this.Retain();
            this.PrizeOpenedSignal.AddListener(OnClosePrizeView);

            //this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(OpenPrizeView));
            var openPrizeView = this.ViewReferences.Get<OpenPrizeView>();
            if (openPrizeView == null || !openPrizeView.gameObject.activeSelf)
            {
                this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(OpenPrizeView));
                openPrizeView = this.ViewReferences.Get<OpenPrizeView>();
            }
            
            List<CardData> cards = new List<CardData>();
            foreach(var prize in prizes)
            {
                PrizeList.Remove(prize); // Remove the prizes we're showing, in case we need to show more than 6 of a group.

                if(prize.PrizeSource == PrizeSource.quest && PlayerProfile.MostRecentCompletedQuest != null)
                {
                    if (PlayerProfile.MostRecentCompletedQuest.ID == -5)
                        ShowMainMenuParams.IsForceHomeScreen = true;
                    prize.AchievementName = string.Format(I2.Loc.ScriptLocalization.Get(string.Format("QuestDetails/{0}", PlayerProfile.MostRecentCompletedQuest.ID)), PlayerProfile.MostRecentCompletedQuest.TargetScore);
                    if (string.IsNullOrEmpty(prize.PrizeId))
                        prize.PrizeId = PlayerProfile.MostRecentCompletedQuest.ArtId;
                }

                int cardId;
                CardData cdata;
                if(int.TryParse(prize.PrizeId, out cardId) && GameData.CardDictionary.TryGetValue(cardId, out cdata))
                {
                    cards.Add(cdata);
                }
            }
            openPrizeView.Initialize();
            Timer.Instance.StartCoroutine(PopulateView(openPrizeView, prizes, cards));

        }

        private IEnumerator PopulateView(OpenPrizeView openPrizeView, List<Prize> prizes, List<CardData> cards)
        {
            yield return null;
            openPrizeView.Populate(prizes, cards, this.GameData.PrizePrefab, this.GameData.ArenaCardPrefab, this.PlayerProfile.MetaProfile.CardBackID,
                this.GameData, this.PlayerProfile, PrizeViewType, GameOverMessage);
        }

        private void ShowMainMenu()
        {

            this.CloseAnimatedViewSignal.Dispatch(typeof(OpenPrizeView));
            Timer.Instance.StartCoroutine(TeardownArena());
            //this.ShowMainMenuSignal.Dispatch(new ShowMainMenuParams());
        }


        private IEnumerator TeardownArena()
        {
            //Debug.Log("EndGameDialogClosed");
            
            // TODO: pretty sure this doesn't work, find a better solution -DMac
            // Create a temporary progress loading screen
            bool isJustRewards = GameOverMessage.IsJustRewards;
            if (!isJustRewards)
            {
                Dictionary<AsyncProgress, string> progressLabels = new Dictionary<AsyncProgress, string>();
                progressLabels[AsyncProgress.NoProgress] = "FOO...";
                progressLabels[AsyncProgress.ObjectPool] = "BAR...";

                this.InitializeProgressScreenSignal.Dispatch(progressLabels);
                this.AsyncProgressSignal.Dispatch(AsyncProgress.ObjectPool, 1.0f);
            }
            //yield return null;
            if(/*GameOverMessage.IsDailyRewards || */GameOverMessage.IsJustRewards)
            {
                yield return null;
            }
            else
            {
                yield return new WaitForSecondsRealtime(0.5f);

                this.TeardownArenaSignal.Dispatch();

                yield return new WaitForSecondsRealtime(0.5f);
            }
            //yield return null;
            //yield return null;
            if (this.ShowMainMenuParams == null)
                this.ShowMainMenuParams = new ShowMainMenuParams();

            ShowMainMenuParams.IsStartup = (GameOverMessage.IsDailyRewards || GameOverMessage.IsJustRewards);
  
            if (this.PlayerProfile.UiHintData.OpenPacks > 0)
                this.PlayerProfile.Wallet.Currencies.TryGetValue(Messages.CurrencyType.SP, out ShowMainMenuParams.PacksToOpen);

            this.ShowMainMenuSignal.Dispatch(ShowMainMenuParams);

            this.Release();
        }


        void OnClosePrizeView()
        {
            this.PrizeOpenedSignal.RemoveListener(OnClosePrizeView);
            //Debug.LogFormat("next prize view: {0}. Prizes: {1}", nextPrizeView.ToString(), PrizeList.Count);
            Timer.Instance.StartCoroutine(NextPrizeCoroutine());
            
        }

        private IEnumerator NextPrizeCoroutine()
        {
            yield return null;
            this.OpenPrizeSignal.Dispatch(PrizeList, nextPrizeView, GameOverMessage, ShowMainMenuParams);
            this.Release();
        }
    }
}
