﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	public enum NovaCanvas
	{
		Main,
		Overlay,
	}
    public class MoveAnimatedViewToCanvasSignal : Signal<AnimatedView, NovaCanvas, float> { }

    public class MoveAnimatedViewToCanvasCommand : Command
    {
		// Signal Parametrrs
		[Inject] public AnimatedView View {get;set;}
		[Inject] public NovaCanvas NovaCanvas {get;set;}
        [Inject] public float DelayTime { get; set; }
		[Inject] public ViewStateStack ViewStateStack {get;set;}
		
		// Injected Dependencies
		[Inject] public INovaContext NovaContext {get;set;}

		public override void Execute()
		{
			if (this.View == null)
				return;
			//Debug.Log("Moving " + this.View.GetType() + " to " + this.NovaCanvas);
			switch(this.NovaCanvas)
			{
				case NovaCanvas.Main:
                    this.View.isIntendedForOverlayCanvas = false;
                    if (DelayTime > 0)
                    {
                        this.Retain();
                        Timer.Instance.StartCoroutine(MoveToMainCoroutine(DelayTime));
                    }
                    else
                    {
                        this.View.transform.SetParent(this.NovaContext.MainCanvas.transform);
                    }
                    break;
				case NovaCanvas.Overlay:
                    this.View.isIntendedForOverlayCanvas = true;
                    this.View.transform.SetParent(this.NovaContext.OverlayCanvas.transform);
					this.View.transform.SetAsFirstSibling();
					this.ViewStateStack.AddViewToCurrentState(this.View);
				break;
			}

			this.View.transform.SetAsLastSibling();
		}

        public IEnumerator MoveToMainCoroutine(float delayTime)
        {
            yield return new WaitForSecondsRealtime(delayTime);
            if(this.View != null && !this.View.isIntendedForOverlayCanvas)
                this.View.transform.SetParent(this.NovaContext.MainCanvas.transform);
            this.Release();
        }

        public IEnumerator MoveToOverlayCoroutine()
        {
            yield return new WaitForSecondsRealtime(0.25f);
            if (this.View != null)
            {
                this.View.transform.SetParent(this.NovaContext.OverlayCanvas.transform);
                this.View.transform.SetAsFirstSibling();
                this.ViewStateStack.AddViewToCurrentState(this.View);
            }
            this.Release();
        }
    }
}
