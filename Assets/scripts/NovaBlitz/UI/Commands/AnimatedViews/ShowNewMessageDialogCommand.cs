﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;
using NovaBlitz.Economy;

namespace NovaBlitz.UI
{
	public struct NewMessageDialogParams
	{
		public NewMessageDialogParams(string title, string message, bool isWaitingForPurchase = false, StoreData storeData = null)
		{
			this.Title = title;
			this.Message = message;
            this.IsWaitingForPurchase = isWaitingForPurchase;
            StoreData = storeData;
		}
		public string Title;
		public string Message;
        public bool IsWaitingForPurchase;
        public StoreData StoreData;
	}
	public class ShowNewMessageDialogSignal : Signal<NewMessageDialogParams> {}
	
    public class ShowNewMessageDialogCommand : Command
    {
		// Signal Parameters
		[Inject] public NewMessageDialogParams Params {get;set;}
		
		// Injected Dependencies
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public INovaContext ContextView {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
		
		public override void Execute()
		{
			NewMessageDialogView messageDialog = (NewMessageDialogView)this.ViewReferences.Get(typeof(NewMessageDialogView));
			if(messageDialog == null)
			{
				messageDialog = (NewMessageDialogView)this.ViewReferences.Instantiate(typeof(NewMessageDialogView), this.ContextView.MainCanvas.transform );
			}

                messageDialog.Initialize(this.Params);
            
			//messageDialog.transform.localPosition = Vector3.zero;
			//messageDialog.transform.localScale = Vector3.one;
			this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(NewMessageDialogView));
		}
    }
}
