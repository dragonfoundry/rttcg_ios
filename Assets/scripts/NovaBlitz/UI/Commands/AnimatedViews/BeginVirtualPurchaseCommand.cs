﻿using System;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using strange.extensions.mediation.impl;
using strange.extensions.context.impl;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
    public class BeginVirtualPurchaseSignal : Signal<StoreData, PoolableMonoBehaviour> { }
    public class BeginVirtualPurchaseCommand : Command
    {
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }
        [Inject] public ViewReferences ViewReferences { get; set; }

        [Inject] public StoreData StoreData { get; set; }
        [Inject] public PoolableMonoBehaviour View { get; set; }
		[Inject] public INovaContext ContextView {get;set;}

        [Inject] public PlayerProfile PlayerProfile { get; set; }

        public override void Execute()
        {
            ConfirmVirtualPurchaseDialogView view = this.ViewReferences.Get(typeof(ConfirmVirtualPurchaseDialogView)) as ConfirmVirtualPurchaseDialogView; // we just opened it, so it's ok to do without a null check
            if(view == null)
            {
                view = this.ViewReferences.Instantiate(typeof(ConfirmVirtualPurchaseDialogView), this.ContextView.MainCanvas.transform) as ConfirmVirtualPurchaseDialogView;
            }

            int currentCredits;
            PlayerProfile.Wallet.Currencies.TryGetValue(CurrencyType.CR, out currentCredits);
            view.Initialize(StoreData, View, currentCredits);
            OpenBlurredAnimatedViewSignal.Dispatch(typeof(ConfirmVirtualPurchaseDialogView));
        }
    }
}
