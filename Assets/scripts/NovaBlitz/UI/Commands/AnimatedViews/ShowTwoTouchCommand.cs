﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using LinqTools;
using UnityEngine.UI;
using NovaBlitz.UI;

public class TwoTouchParams
{
	public int CardId;
	public RectTransform SourceTransform;
	public bool IsEditable;
	public DraggableCardView DraggableCardView;
	public bool IsFullScrim = false;
    public bool IsBlurred = true;
	public float DestCardScale = 2f;
	public CardData CardData { get { return GameData.CardDictionary[this.CardId];}}
}

public class ShowTwoTouchSignal : Signal<TwoTouchParams> {}
public class ShowTwoTouchCommand : Command
{
	// Signal Parameters
	[Inject] public TwoTouchParams TwoTouchParams {get;set;}

	// Injected Dependencies
	[Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal {get;set;}
	[Inject] public PlayerProfile PlayerProfile {get;set;}
	[Inject] public ViewReferences ViewReferences { get;set;}
    [Inject] public INovaContext ContextView {get;set;}

	public override void Execute()
	{
        var twoTouch = (TwoTouchOverlayView)ViewReferences.Get(typeof(TwoTouchOverlayView));
        if(twoTouch == null)
        {
            this.ViewReferences.Instantiate(typeof(TwoTouchOverlayView), this.ContextView.MainCanvas.transform);
            this.Retain();
            Timer.Instance.StartCoroutine(WaitForTwoTouchEnumerator());
        }
        else
        {
            InitializeTwoTouch();
        }
	}

    private IEnumerator WaitForTwoTouchEnumerator()
    {
        yield return null;
        InitializeTwoTouch();
        this.Release();
    }

    private void InitializeTwoTouch()
    {
        this.InitializeAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView), (v) => {
            TwoTouchOverlayView view = (TwoTouchOverlayView)v;
            view.InitializeAndOpenTwoTouch(this.PlayerProfile, this.TwoTouchParams);
        });
    }
}
