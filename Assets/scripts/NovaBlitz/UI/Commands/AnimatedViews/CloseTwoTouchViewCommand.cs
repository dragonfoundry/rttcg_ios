﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using LinqTools;
using UnityEngine.UI;
using NovaBlitz.UI;


public class CloseTwoTouchViewSignal : Signal {}

public class CloseTwoTouchViewCommand : Command 
{
	[Inject] public ViewReferences ViewReferences {get;set;}

	public override void Execute()
	{
		TwoTouchOverlayView twoTouchOverlayView = this.ViewReferences.Get<TwoTouchOverlayView>();
		if(twoTouchOverlayView != null)
		{
			twoTouchOverlayView.ClickScrim();
		}
	}
}
