﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using DG.Tweening;

namespace NovaBlitz.UI
{
    public class AnimatedViewOpenedSignal : Signal<System.Type> { }

    public class OpenAnimatedViewSignal : Signal<System.Type> {}
	public class OpenAnimatedViewCommand : Command
	{
		// Signal Parameters
		[Inject] public System.Type ViewType {get;set;}

		// Injected Dependencies
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public INovaContext ContextView{get;set;}
        [Inject] public AnimatedViewOpenedSignal AnimatedViewOpenedSignal { get; set; }

        /// <summary>
        /// Plays an animated menus Open animation.Instantiates the AnimatedMenu with 
        /// the given prefab path and stores a reference to it if neccessary.
        /// </summary>
        public override void Execute ()
		{
			Debug.LogFormat ("Opening {0}", ViewType);
			AnimatedView view = this.ViewReferences.Get(this.ViewType) as AnimatedView;
			if(view == null)
			{
				GameObject parent = this.ContextView.MainCanvas.gameObject;
				view = this.ViewReferences.Instantiate(this.ViewType, parent.transform) as AnimatedView;
            }
			if(view.transform.parent != this.ContextView.MainCanvas.transform 
				&& view.transform.parent != this.ContextView.OverlayCanvas.transform)
			{
				view.transform.SetParent(this.ContextView.MainCanvas.transform);
			}
            view.DOKill();
            view.IsShouldAwakeOpen = true;
            view.gameObject.SetActive(true);
            view.transform.localPosition = Vector3.zero;
			view.transform.localScale = Vector3.one;

			view.transform.SetAsLastSibling();
			if (!view.IsOpen)
				view.IsOpen = true;
            Timer.Instance.StartCoroutine(NotifyViews());
            this.Retain();
        }

        private IEnumerator NotifyViews()
        {
            yield return null;
            AnimatedViewOpenedSignal.Dispatch(ViewType);
            this.Release();
        }
	}
}
