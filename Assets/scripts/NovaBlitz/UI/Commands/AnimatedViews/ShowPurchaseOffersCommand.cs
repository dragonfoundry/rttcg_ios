﻿using System;
using System.Collections.Generic;
using LinqTools;
using System.Text;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using strange.extensions.mediation.impl;
using strange.extensions.context.impl;
using NovaBlitz.Economy;
using Messages;

namespace NovaBlitz.UI
{
    public enum OfferType
    {
        NoType,
        CreditsOffer,
        NanoBotsOffer,
        GemsOffer,
        SpecialOffer
    }

    public class ShowPurchaseOffersSignal : Signal<OfferType> { }
    public class ShowPurchaseOffersCommand : Command
    {
        [Inject] public OfferType OfferType { get; set; }

        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get;set;}

        public override void Execute()
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(OfferView), v => ((OfferView)v).InitializeView(OfferType));
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(OfferView));
        }
    }
}
