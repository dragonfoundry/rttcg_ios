﻿using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using NovaBlitz.Game;
using DragonFoundry.Fx;
using GameFormat = Messages.GameFormat;
using TournamentStatus = Messages.TournamentStatus;
using ChatUserStatus = ExitGames.Client.Photon.Chat.ChatUserStatus;

namespace NovaBlitz.UI
{
	public class PlayScreen : MenuScreen
	{
		[Inject] public DeckBuilderScreen DeckBuilderScreen {get;set;}
        
		[Inject] public NewDeckSignal NewDeckSignal {get;set;}
		[Inject] public DraftSessionLoadedSignal DraftSessionLoadedSignal {get;set;}
		[Inject] public FilterCardsSignal FilterCardsSignal {get;set;}
		[Inject] public CardsFilteredSignal CardsFilteredSignal {get;set;}
        [Inject] public DeckSelectedSignal DeckSelectedSignal { get; set; }
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
        [Inject] public ToFriendChallengeSignal ToFriendChallengeSignal { get; set; }
        [Inject] public SaveUiHintDataSignal SaveUiHintDataSignal { get; set; }
        [Inject] public INovaContext Context { get; set; }
        [Inject] public ShowTutorialDialogSignal ShowTutorialDialogSignal { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }
		[Inject] public CancelSignal CancelSignal {get;set;}

        public Type PreviousScreenType = null;

        protected GameFormat _Format = GameFormat.Casual;
		public GameFormat Format {
			get { return _Format; }
			set { _Format = value;
                PlayerProfile.LastPlayScreenFormat = value;
                var pscolumn = FindView<PlayScreenColumnView>();
                if (null != pscolumn)
                {
                    pscolumn.IsTutorial = value == GameFormat.Tutorial || !this.PlayerProfile.HasUnlockedAllDecks;
                    pscolumn.Format = value;
                }
				var deckDock = FindView<DeckListDockView>();
				if (null != deckDock)
					deckDock.Format = value;
                var mainHeader = FindView<MainHeaderView>();
                if (null != mainHeader)
                    mainHeader.IsTutorial = value == GameFormat.Tutorial;
                var deckListView = FindView<DeckListView>();
                if (null != deckListView)
                    // this ensures the deck list items don't show delete buttons if we're in the tutorial, or we still have onboarding decks to unlock
                    deckListView.IsTutorial = value == GameFormat.Tutorial;
            }
		}

		public override void Init()
		{
			this.ChildViews = new Dictionary<Type, bool>{
                {typeof(MainHeaderView), true},
                {typeof(PlayScreenColumnView), true},
                {typeof(FriendListDockView), false},
                {typeof(DeckListView), true},
				{typeof(DeckListDockView), true}
            };

			this.NewDeckSignal.AddListener(OnNewDeck);
			this.DraftSessionLoadedSignal.AddListener(OnDraftSessionLoaded);
            this.ToFriendChallengeSignal.AddListener(OnFriends_ChallengeClicked);
            this.MoveAnimatedViewToCanvasSignal.AddListener(OnMoveAnimatedViewToCanvas);
            this.CancelSignal.AddListener(OnCancelPlay);

            base.Init();
		}

        PlayScreenColumnView PlayScreenColumn;
        MainHeaderView MainHeader;
        DeckListDockView DeckListDock;
        DeckListView DeckListView;

        protected override void RegisterChild(AnimatedView view)
		{
			base.RegisterChild(view);
			var playScreenColumn = view as PlayScreenColumnView;
			if (null != playScreenColumn)
            {
                PlayScreenColumn = playScreenColumn;
                playScreenColumn.EventPlayScreenOpened += UpdateUiHints;
                playScreenColumn.EventBackClicked += OnBackClicked;
                playScreenColumn.LauncherWidget.EventPlayClicked += OnPlayClicked;
				playScreenColumn.LauncherWidget.EventPracticeClicked += OnPracticeClicked;
                playScreenColumn.LauncherWidget.EventFreePlayClicked += OnFreePlayClicked;
                playScreenColumn.EventOpenStore += OnOpenStore;
				//playScreenColumn.Format = this.Format;
                //playScreenColumn.PlayScreenOpened();
                return;
            }
            var mainHeader = view as MainHeaderView;
            if(null != mainHeader)
            {
                MainHeader = mainHeader;
                MainHeader.EventBackClicked += OnBackClicked;
            }
            //var friendScreen = view as FriendListDockView;
            //if (null != friendScreen)
            //    friendScreen.EventChallengeClicked += OnFriends_ChallengeClicked;
            var deckDock = view as DeckListDockView;
			if (null != deckDock) {
                DeckListDock = deckDock;
                deckDock.EventEditClicked += OnDeckEditClicked;
				deckDock.EventStatsClicked += OnDeckStatsClicked;
				//deckDock.Format = this.Format;
				return;
			}
            var deckListView = view as DeckListView;
            if(null != deckListView)
            {
                DeckListView = deckListView;
                deckListView.DeckAdd += this.OnDeckAddClicked;
            }
		}

		public override void Open()
		{
			Debug.Log("PLAY SCREEN OPENED!");
			bool hideCards = false;
			switch (this.Format) {
			case GameFormat.Draft:
				if (this.PlayerProfile.DraftData.event_status == TournamentStatus.not_playing)
					hideCards = true;
				this.ChildViews[typeof(DeckListView)] = false;
				break;
			case GameFormat.Constructed:
                this.ChildViews[typeof(DeckListView)] = this.PlayerProfile.ConstructedData.event_status != TournamentStatus.playing;
                break;
            case GameFormat.Tournament:
            case GameFormat.Monthly:
			case GameFormat.Annual:
				this.ChildViews[typeof(DeckListView)] = this.PlayerProfile.TournamentData.event_status != TournamentStatus.playing;
				break;
			default:
				this.ChildViews[typeof(DeckListView)] = true;
				break;
			}
			
			this.ChildViews[typeof(DeckListDockView)] = !hideCards;
			base.Open();

            var playScreenColumn = (PlayScreenColumnView)this.ViewReferences.Get(typeof(PlayScreenColumnView));
            playScreenColumn.PlayScreenOpened();
            var mainHeader = (MainHeaderView)this.ViewReferences.Get(typeof(MainHeaderView));
            mainHeader.BackButton.gameObject.SetActive(this.Format != GameFormat.Tutorial);
        }

		protected void ToHomeScreen() { TransitionTo(typeof(HomeScreen)); }
		protected void ToDraftScreen() { TransitionTo(typeof(DraftScreen)); }
		protected void ToDeckBuilder()
		{
			//this.DeckBuilderScreen.PreviousScreenType = this.GetType();
			//TransitionTo(typeof(DeckBuilderScreen));
			
			// Delay opening the deckbuilder until cards have been filtered
			// Go ahead and close the PlayScreen though
			this.CloseViews(this._ViewsRegistered, ()=>{
				// Use any random gameObject to start a coRoutine on...-DMac
				Timer.Instance.StartCoroutine(this.WaitToShowDeckBuilder());
			});
		}
		
		// Give the playScreen a frame or two to get out of the way...
		// then filter the DecBuilders card list...
		private IEnumerator WaitToShowDeckBuilder()
		{
			yield return null;
			yield return null;
			DeckData cDeck = this.PlayerProfile.CurrentDeck;
            DeckSelectedSignal.Dispatch(cDeck);
				CardAspect[] aspects = (null != cDeck && cDeck.Aspects.Count > 1) ? cDeck.Aspects.ToArray() : new CardAspect[0];
				this.CardsFilteredSignal.AddOnce(OnDeckbuilderCardsFiltered);
				this.FilterCardsSignal.Dispatch(new CardFilterParams{
					Aspects = aspects, CollectionType = this.PlayerProfile.NewCards.Count > 0 ? CardCollectionType.NewCards : CardCollectionType.MyCards
                });
		}

		// Once the deckbuilder has filtered cards... go ahead and open the sreen.
        private void OnDeckbuilderCardsFiltered(List<int> obj)
        {
			Debug.Log("Got filter in play screen");
            this.DeckBuilderScreen.PreviousScreenType = this.GetType();
            //PlayerProfile.LastMenuScreenViewed = this.GetType();

            //this.DeckBuilderScreen.Open();
            TransitionTo(typeof(DeckBuilderScreen));
        }

        private IEnumerator ShowTutorialPopup()
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView), v => ((TutorialPopUpView)v).GameState = null);
            yield return null;
            this.OpenAnimatedViewSignal.Dispatch(typeof(TutorialPopUpView));
            this.ShowTutorialDialogSignal.Dispatch(string.Format("Tutorial_{0}", this.PlayerProfile.OnboardingProgress.CurrentTutorial), false);
        }

        protected void UpdateUiHints()
        {
            if (this.PlayScreenColumn.IsOpen)
            {
                switch (this.PlayScreenColumn.Format)
                {
                    case GameFormat.Tutorial:
                        PlayFx(this.PlayScreenColumn.LauncherWidget.PlayButtonFx);
                        if(this.PlayerProfile.OnboardingProgress != null)
                        {
                            Timer.Instance.StartCoroutine(ShowTutorialPopup());
                        }    
                        break;
                    case GameFormat.Practice:
                    case GameFormat.Casual:
                        if (this.PlayerProfile.UiHintData.Practice == 0)
                        {
                            PlayFx(this.DeckListDock.PracticeButtonFx);
                            //SetGlow(this.PlayScreenColumn.LauncherWidget.PracticeCanvas);
                        }
                        else if (this.PlayerProfile.UiHintData.Play <= 1 && this.PlayScreenColumn.LauncherWidget.IsPlayEnabled)
                        {
                            PlayFx(this.PlayScreenColumn.LauncherWidget.PlayButtonFx);
                            //SetGlow(this.PlayScreenColumn.LauncherWidget.PlayCanvas);
                        }
                        else if (this.PlayerProfile.UiHintData.EditDeck == 0 && this.DeckListDock.EditButton.interactable)
                        {
                            PlayFx(this.DeckListDock.EditButtonFx);
                            //SetGlow(this.DeckListDock.EditGlowCanvas);
                            // highlight the "add deck" or "edit deck" button
                        }
                        else if (this.PlayerProfile.UiHintData.AddDeck == 0
                            && this.DeckListView.AddDeckButton.interactable
                            && this.PlayerProfile.OnboardingProgress != null
                            && (this.PlayerProfile.OnboardingProgress.CurrentQuest == null || this.PlayerProfile.OnboardingProgress.CurrentQuest.ID > 0))
                        {
                            PlayFx(this.DeckListView.AddDeckButtonFx);
                            //SetGlow(this.DeckListView.AddDeckGlowCanvas);
                        }
                        else
                        {
                            PlayFx(null);
                            //SetGlow(null);
                        }
                        break;
                    case GameFormat.Draft:
                        if (//this.PlayerProfile.UiHintData.Draft <= 1
                            //&& (
                            this.PlayerProfile.DraftData == null || this.PlayerProfile.DraftData.event_status == TournamentStatus.not_playing
                            && this.PlayScreenColumn.LauncherWidget.IsFreeEnabled)
                        {
                            PlayFx(this.PlayScreenColumn.LauncherWidget.FreeEntryButtonFx);
                            //SetGlow(this.PlayScreenColumn.LauncherWidget.FreeEntryCanvas);
                        }
                        else
                        {
                            PlayFx(null);
                            //SetGlow(null);
                        }
                        break;
                    case GameFormat.Constructed:
                        if (//this.PlayerProfile.UiHintData.League <= 1
                            this.PlayerProfile.ConstructedData == null || this.PlayerProfile.ConstructedData.event_status == TournamentStatus.not_playing
                            && this.PlayScreenColumn.LauncherWidget.IsFreeEnabled)
                        {
                            PlayFx(this.PlayScreenColumn.LauncherWidget.FreeEntryButtonFx);
                            //SetGlow(this.PlayScreenColumn.LauncherWidget.FreeEntryCanvas);
                        }
                        else
                        {
                            PlayFx(null);
                            //SetGlow(null);
                        }
                        break;
                    case GameFormat.Monthly:
                    case GameFormat.Annual:
                    case GameFormat.Tournament:
                        if (//this.PlayerProfile.UiHintData.Event <= 1
                            this.PlayerProfile.TournamentData == null || this.PlayerProfile.TournamentData.event_status == TournamentStatus.not_playing
                            && this.PlayScreenColumn.LauncherWidget.IsFreeEnabled)
                        {
                            PlayFx(this.PlayScreenColumn.LauncherWidget.FreeEntryButtonFx);
                            //SetGlow(this.PlayScreenColumn.LauncherWidget.FreeEntryCanvas);
                        }
                        else
                        {
                            PlayFx(null);
                            //SetGlow(null);
                        }
                        break;
                }
            }
        }

        private ParticleEffectsTrigger _PlayingTrigger;
        public void PlayFx(ParticleEffectsTrigger trigger)
        {
            if (_PlayFxCoroutine != null)
            {
                Timer.Instance.StopCoroutine(_PlayFxCoroutine);
            }
            _PlayFxCoroutine = PlayFxCoroutine(trigger);
            Timer.Instance.StartCoroutine(_PlayFxCoroutine);
        }
        private IEnumerator _PlayFxCoroutine;
        private IEnumerator PlayFxCoroutine(ParticleEffectsTrigger trigger)
        {
            yield return null;
            if (_PlayingTrigger != trigger)
            {
                if (_PlayingTrigger != null)
                {
                    Debug.LogFormat("StoppingFX");
                    _PlayingTrigger.EndEffect();
                    _PlayingTrigger.gameObject.SetActive(false);
                    _PlayingTrigger = null;
                }

                if (this.IsOpen && trigger != null)
                {
                    trigger.gameObject.SetActive(true);
                    _PlayingTrigger = trigger;
                    while (!trigger.gameObject.activeInHierarchy)
                    {
                        yield return null;
                    }
                    Debug.LogFormat("StartingFX");
                    trigger.StartEffect();
                    //trigger.gameObject.SetActive(false);
                    //trigger.gameObject.SetActive(true);
                }
            }
        }

        protected void OnCancelPlay()
        {
            Open();
        }

        private IEnumerator _WaitThenFixFx;
        protected void OnMoveAnimatedViewToCanvas(AnimatedView view, NovaCanvas canvas, float delayTime)
        {
            if(this.IsOpen && _WaitThenFixFx == null)
            {
                _WaitThenFixFx = WaitThenFixFx();
                Timer.Instance.StartCoroutine(_WaitThenFixFx);
            }

        }

        private IEnumerator WaitThenFixFx()
        {
            yield return null;
            if (this.IsOpen && _PlayingTrigger != null)
            {
                if (this.Context.IsAlreadyBlurred)
                {
                    _PlayingTrigger.EndEffect();
                    _PlayingTrigger.gameObject.SetActive(false);
                }
                else
                {
                    _PlayingTrigger.gameObject.SetActive(true);
                    _PlayingTrigger.StartEffect();
                }
            }
            _WaitThenFixFx = null;
        }

        /*public void SetGlow(CanvasGroup glowCanvas)
        {
            PlayScreenColumn.LauncherWidget.FreeEntryCanvas.DOFade(0.0f, 1.0f);
            PlayScreenColumn.LauncherWidget.PracticeCanvas.DOFade(0.0f, 1.0f);
            PlayScreenColumn.LauncherWidget.PlayCanvas.DOFade(0.0f, 1.0f);
            DeckListDock.EditGlowCanvas.DOFade(0.0f, 1.0f);
            DeckListView.AddDeckGlowCanvas.DOFade(0.0f, 1.0f);
            if (glowCanvas != null)
                glowCanvas.DOFade(1.0f, 1.0f);
        }*/

        #region Signal Listeners
        protected void OnNewDeck()
		{
			ToDeckBuilder();
		}

		protected void OnDraftSessionLoaded()
		{
			ToDraftScreen();
		}
		#endregion

		#region View Listeners
		protected void OnBackClicked()
        {
            if (this.PreviousScreenType != null)
            {
                TransitionTo(this.PreviousScreenType);
                this.PreviousScreenType = null;
            }
			this.PlayerProfile.SelectedChallengeID = null;
			ToHomeScreen();
            //var mainHeader = (MainHeaderView)this.ViewReferences.Get(typeof(MainHeaderView));
            MainHeader.BackButton.gameObject.SetActive(false);
        }

		protected void OnPlayClicked(GameFormat format)
		{
            //ToggleView<ProgressLoadingScreenView>(true);
            if (format == GameFormat.Casual && this.PlayerProfile.UiHintData.Play <= 1)
            {
                this.PlayerProfile.UiHintData.Play = 2;
                this.SaveUiHintDataSignal.Dispatch();
            }
        }

		protected void OnPracticeClicked()
        {
            if (this.PlayerProfile.UiHintData.Practice <= 1)
            {
                this.PlayerProfile.UiHintData.Practice = 2;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }
            //ToggleView<ProgressLoadingScreenView>(true);
        }

        protected void OnFreePlayClicked(GameFormat format)
        {
            //ToggleView<ProgressLoadingScreenView>(true);
            if (format == GameFormat.Draft && this.PlayerProfile.UiHintData.Draft <= 1)
            {
                this.PlayerProfile.UiHintData.Draft = 2;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }
            else if (format == GameFormat.Constructed && this.PlayerProfile.UiHintData.League <= 1)
            {
                this.PlayerProfile.UiHintData.League = 2;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }
            else if ((format == GameFormat.Tournament || format == GameFormat.Annual || format == GameFormat.Monthly) 
                && this.PlayerProfile.UiHintData.Event <= 1)
            {
                this.PlayerProfile.UiHintData.Event = 2;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }
        }

        protected void OnOpenStore()
        {
			var twoTouch = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			if (twoTouch != null)
            {
                this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
                //twoTouch.IsOpen = false;
			}

            ToggleView<StoreView>(true);
        }

        protected void OnDeckStatsClicked()
		{
            /*if (this.PlayerProfile.UiHintData.Practice == 0)
            {
                this.PlayerProfile.UiHintData.Practice = 1;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }*/
            var deckStats = ToggleView<DeckStatsView>(true);
			this.OpenBlurredAnimatedViewSignal.Dispatch(deckStats.GetType());

            bool isLeagueDeck = Format == GameFormat.Constructed && this.PlayerProfile.ConstructedData != null && this.PlayerProfile.ConstructedData.event_status == TournamentStatus.playing;

            deckStats.IsEditable = (Format == GameFormat.Casual || Format == GameFormat.Practice);
            if(Format == GameFormat.Draft)
                deckStats.DeckData = this.PlayerProfile.DraftDeck;
            else if(isLeagueDeck)
                deckStats.DeckData = this.PlayerProfile.LeagueDeck;
            else
                deckStats.DeckData = this.PlayerProfile.CurrentDeck;
			deckStats.Refresh(Format != GameFormat.Draft && !isLeagueDeck);
		}
        
		protected void OnDeckEditClicked()
        {
            if (this.PlayerProfile.UiHintData.EditDeck == 0)
            {
                this.PlayerProfile.UiHintData.EditDeck = 1;
                this.SaveUiHintDataSignal.Dispatch();
                UpdateUiHints();
            }
            ToDeckBuilder();
        }

        protected void OnDeckAddClicked()
        {
            if (this.PlayerProfile.UiHintData.AddDeck == 0)
            {
                this.PlayerProfile.UiHintData.AddDeck = 1;
                this.SaveUiHintDataSignal.Dispatch();
                PlayFx(null);
            }
        }

        public void OnFriends_ChallengeClicked(string playfabID)
        {
            if (this.IsOpen)
            {
                ToPlayScreen(GameFormat.Challenge);
            }
        }
        protected void ToPlayScreen(GameFormat format)
        {
            //FuseService.ShowAdInZone(FuseService.ZONE_PLAY_ENTER);
            this.Format = format;
            TransitionTo(typeof(PlayScreen));
            this.Open();
        }
        #endregion
    }
}
