﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;

namespace NovaBlitz.UI
{
	public abstract class MenuScreen
	{
		protected List<AnimatedView> _ViewsRegistered = new List<AnimatedView>();

		public bool IsOpen {get; protected set;}
		public virtual Dictionary<Type, bool> ChildViews {get; protected set;}
		public IEnumerable<AnimatedView> ChildInstances { get { return this.ChildViews.Keys.Select(t => this.ViewReferences.Get(t)).OfType<AnimatedView>(); } }

		[Inject] public INovaContext NovaContext {get;set;}
		[Inject] public ViewReferences ViewReferences {get;set;}
		[Inject] public OpenAnimatedViewSignal OpenAnimatedViewSignal {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
		[Inject] public ScreenTransitionSignal ScreenTransitionSignal {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }
		protected float _FallbackTimer = 0.55f;

        

		/// <summary>
		/// Called after construction and injection.
		/// </summary>
		[PostConstruct]
		public virtual void Init()
		{
            RegisterChildren();
		}

        public void RegisterChildren()
        {
            var existing = ChildInstances;
            foreach (var view in existing)
                RegisterChild(view);
        }

		/// <summary>
		/// Partially for shorthand, this ensures that views register on late creation.
		/// </summary>
		protected AnimatedView ToggleView(Type viewType, bool visible)
		{
			if (visible)
                this.OpenAnimatedViewSignal.Dispatch(viewType);
			else
                this.CloseAnimatedViewSignal.Dispatch(viewType);

			var viewInstance = this.FindView(viewType);
			if (null != viewInstance && !_ViewsRegistered.Contains (viewInstance))
			{
				RegisterChild (viewInstance);
				//if (visible) this.OpenAnimatedViewSignal.Dispatch(viewType);
				//else this.CloseAnimatedViewSignal.Dispatch(viewType);
			}
			return viewInstance;
		}
		protected T ToggleView<T>(bool visible) where T : AnimatedView { return ToggleView(typeof(T), visible) as T; }

		/// <summary>
		/// Shorthand.
		/// </summary>
		protected AnimatedView FindView(Type viewType) { return this.ViewReferences.Get(viewType) as AnimatedView; }
		protected T FindView<T>() where T : AnimatedView { return this.ViewReferences.Get(typeof(T)) as T; }

		/// <summary>
		/// Handle subscriptions and such here, since view instantiation times may vary.
		/// </summary>
		protected virtual void 
		RegisterChild(AnimatedView view)
		{
			_ViewsRegistered.Add(view);
		}

		/// <summary>
		/// Open all children flagged visible in dictionary.
		/// </summary>
		public virtual void Open()
		{
			this.IsOpen = true;
            foreach(var kvp in this.ChildViews)
            {
                ToggleView(kvp.Key, kvp.Value);
            }
			/*var toOpen = this.ChildViews.Where(kvp => kvp.Value).Select(kvp => kvp.Key);
			foreach (var viewType in toOpen)
				ToggleView(viewType, true);*/
		}

		/// <summary>
		/// Close this screen's child views.
		/// </summary>
		public virtual void Close(Action onClosed = null)
		{
			this.IsOpen = false;
			var openViews = this.ChildInstances.Where(v => v.IsOpen);
			CloseViews(openViews, onClosed);
		}

		/// <summary>
		/// Close this screen's child views, except those shared with the next screen.
		/// </summary>
		public virtual void Close(MenuScreen nextScreen, Action onClosed = null)
		{
			this.IsOpen = false;
			var openViews = this.ChildInstances.Where(v => v.isActiveAndEnabled && v.IsOpen);
			if (!openViews.Any()) {
				CloseViews(null, onClosed);
				return;
			}

			var toOpen = nextScreen.ChildViews.Where(kvp => kvp.Value).Select(kvp => kvp.Key);
			var toClose = openViews.Where(v => !toOpen.Contains(v.GetType()));
			CloseViews(toClose, onClosed);
		}

		/// <summary>
		/// Invoke a callback after all are closed.
		/// </summary>
		protected void CloseViews(IEnumerable<AnimatedView> views, Action onAllClosed = null)
		{
			this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
			if (null == views || !views.Any()) {
				if (null != onAllClosed)
					onAllClosed.Invoke();
				return;
			}

			// Wait for all to close before invoking callback.
			if (null != onAllClosed) {
				int remaining = views.Count();
				Action onViewClosed = ()=> {
					--remaining;
					if (remaining == 0)
						onAllClosed.Invoke();
				};

				foreach (var view in views) {
					var v = view;
					Timer.Instance.StartCoroutine(EngineUtils.ActionOnceTimeout(_FallbackTimer, onViewClosed, a => v.EventAnimClose += a, a => v.EventAnimClose -= a));
				}
			}
			foreach (var view in views)
				this.CloseAnimatedViewSignal.Dispatch(view.GetType());
		}

		/// <summary>
		/// Convenience. Wait for a view to close before opening another.
		/// </summary>
		protected void CloseThenOpenView(Type toOpen, Type toClose)
		{
			Action openNext = ()=> this.OpenAnimatedViewSignal.Dispatch(toOpen);
			var viewToClose = FindView(toClose);
			if (null != viewToClose && viewToClose.IsOpen) {
				EngineUtils.ActionOnce(openNext, a => viewToClose.EventAnimClose += a, a => viewToClose.EventAnimClose -= a);
				this.CloseAnimatedViewSignal.Dispatch(toClose);
			}
			else openNext.Invoke();
		}

		/// <summary>
		/// Convenience. Dispatch a transition from this screen to another.
		/// </summary>
		protected void TransitionTo(Type toScreenType)
		{
            if (this.IsOpen)
            {
                this.ScreenTransitionSignal.Dispatch(new ScreenTransitionParams
                {
                    FromScreenType = this.GetType(),
                    ToScreenType = toScreenType
                });
            }
		}
    }
}
