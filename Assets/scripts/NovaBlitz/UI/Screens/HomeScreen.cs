﻿using UnityEngine;
using System;
using NovaBlitz.Economy;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using GameFormat = Messages.GameFormat;
using ChatUserStatus = ExitGames.Client.Photon.Chat.ChatUserStatus;

namespace NovaBlitz.UI
{
	public class HomeScreen : MenuScreen
	{
		[Inject] public LogoutUserSignal LogoutUserSignal {get;set;}
        [Inject] public GameData GameData { get; set; }
		[Inject] public UIConfiguration UIConfiguration {get;set;}
		[Inject] public PlayScreen PlayScreen {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}
		[Inject] public ClientPrefs ClientPrefs {get;set;}
        [Inject] public ToFriendChallengeSignal ToFriendChallengeSignal { get; set; }
        [Inject] public ShowLeaderboardSignal ShowLeaderboardSignal { get; set; }
        [Inject] public SaveUiHintDataSignal SaveUiHintDataSignal { get; set; }
        [Inject] public InitializeAnimatedViewSignal InitializeAnimatedViewSignal { get; set; }

        public override void Init()
		{
			this.ChildViews = new Dictionary<Type, bool>{
				{typeof(MainHeaderView), true},
				{typeof(HomeButtonsView), true},
				{typeof(FriendListDockView), false},
				//{typeof(ProfileMenuView), false},
				//{typeof(AvatarSelectorView), false},
				//{typeof(CardBackSelectorView), false},
				//{typeof(StoreView), false}
			};

			this.LogoutUserSignal.AddListener(OnUserLogout);
            this.ToFriendChallengeSignal.AddListener(OnFriends_ChallengeClicked);

            base.Init();
		}

        public override void Open()
        {
            base.Open();
            var homeButtons = (HomeButtonsView)this.ViewReferences.Get(typeof(HomeButtonsView));
            homeButtons.HomeButtonsOpened();
        }

        private MainHeaderView MainHeader;
        private HomeButtonsView HomeButtons;

        protected override void RegisterChild(AnimatedView view)
		{
            if (_ViewsRegistered.Contains(view))
                return;
			base.RegisterChild(view);
			var mainHeader = view as MainHeaderView;
			if (null != mainHeader) {
                MainHeader = mainHeader;
                MainHeader.EventFriendsClicked += OnHome_FriendsClicked;
                return;
			}
			var homeButtons = view as HomeButtonsView;
			if (null != homeButtons) {
                HomeButtons = homeButtons;
				homeButtons.EventCasualButtonClicked += OnHome_CasualClicked;
                homeButtons.EventDraftButtonClicked += OnHome_DraftClicked;
                homeButtons.EventLeagueButtonClicked += OnHome_LeagueClicked;
                homeButtons.EventEventsButtonClicked += OnHome_EventsClicked;
                homeButtons.EventLeaderboardClicked += OnHome_LeaderboardClicked;
                homeButtons.EventFriendsClicked += OnHome_FriendsClicked;
                homeButtons.EventChatClicked += OnHome_ChatClicked;
                homeButtons.EventSettingsClicked += OnHome_SettingsClicked;
				homeButtons.EventAchievementsClicked += OnHome_AchievementsClicked;
                homeButtons.EventFeedbackClicked += OnHome_FeedbackClicked;
                homeButtons.EventStoreClicked += OnHome_StoreClicked;
				homeButtons.EventPacksClicked += OnHome_OpenPacksClicked;
                homeButtons.EventProfileClicked += OnHome_ProfileClicked;
                //homeButtons.HomeButtonsOpened();
                return;
			}
            //var friendScreen = view as FriendListDockView;
            //if (null != friendScreen)
            //    friendScreen.EventChallengeClicked += OnFriends_ChallengeClicked;
        }

		protected void ToLoginScreen() { TransitionTo(typeof(LoginScreen)); }

        protected void ToPlayScreen(GameFormat format)
        {
            /*switch(format)
            {
                case GameFormat.Draft:
                    FuseService.ShowAdInZone(FuseService.ZONE_DRAFT_ENTER);
                    break;
                case GameFormat.Constructed:
                    FuseService.ShowAdInZone(FuseService.ZONE_LEAGUE_ENTER);
                    break;
                case GameFormat.Tournament:
                case GameFormat.Monthly:
                case GameFormat.Annual:
                    FuseService.ShowAdInZone(FuseService.ZONE_EVENTS_ENTER);
                    break;
                default:
                    FuseService.ShowAdInZone(FuseService.ZONE_PLAY_ENTER);
                    break;
            }*/
            TransitionTo(typeof(PlayScreen));
            this.PlayScreen.Format = format;
        }
        protected void ToEventsScreen()
        {
            //FuseService.ShowAdInZone(FuseService.ZONE_EVENTS_ENTER);
            TransitionTo(typeof(EventsScreen));
        }
        protected void ToProfile()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ProfileMenuView));
            var profileMenu = (ProfileMenuView)ViewReferences.Get(typeof(ProfileMenuView));
            if (profileMenu != null)
                profileMenu.OpenPlayerTab();
        }

        protected void OpenStore()
        {
            var twoTouch = (TwoTouchOverlayView)this.ViewReferences.Get(typeof(TwoTouchOverlayView));
			if (twoTouch != null)
			{
                this.CloseAnimatedViewSignal.Dispatch(typeof(TwoTouchOverlayView));
			}
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(StoreView));

            var StoreView = ViewReferences.Get<StoreView>();
            if (StoreView != null)
            {
                    StoreView.ScrollRect.verticalNormalizedPosition = 1.0f;
            }
        }

        protected void OpenPackOpener()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(PackOpenerConfirmView));
        }

		#region Signal Listeners
		protected void OnUserLogout(ShouldUnlinkDevice shouldUnlink, ShouldAutoReconnect shouldReconnect)
		{
			ToLoginScreen();
		}
		#endregion

		#region View Listeners

        protected void OnHome_LeaderboardClicked()
        {
            this.ShowLeaderboardSignal.Dispatch(this.PlayerProfile.RatingMonth, LeaderboardType.Ranking);
        }

		protected void OnHome_FriendsClicked()
		{

			this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(FriendListDockView));
			//var friendList = ToggleView<FriendListDockView>(true);
        }
        
        protected void OnHome_ChatClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(GlobalChatView));
        }

        public void OnFriends_ChallengeClicked(string playfabID)
        {
            if (this.IsOpen)
            {
                ToPlayScreen(GameFormat.Challenge);
            }
        }

        protected void OnHome_SettingsClicked()
        {
            ToProfile(); // this really did open to the player profile tab. Misnamed???
		}

        protected void OnHome_FeedbackClicked()
        {
            this.InitializeAnimatedViewSignal.Dispatch(typeof(ReportBugView),
                    (v) => { ((ReportBugView)v).Initialize(ReportType.ReportBug, null, null); });
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ReportBugView));
        }

        protected void OnHome_AchievementsClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ProfileMenuView));
            var profileMenu = (ProfileMenuView)ViewReferences.Get(typeof(ProfileMenuView));
            if (profileMenu != null)
                profileMenu.OpenAchievementsTab();
		}

		protected void OnHome_StoreClicked()
		{
            if(this.PlayerProfile.UiHintData.Store == 0)
            {
                this.PlayerProfile.UiHintData.Store = 1;
                RefreshHomeHints();
            }
			OpenStore();
		}

		protected void OnHome_OpenPacksClicked()
        {
            if (this.PlayerProfile.UiHintData.OpenPacks == 0)
            {
                this.PlayerProfile.UiHintData.OpenPacks = 1;
                RefreshHomeHints();
            }
            OpenPackOpener();
            //OpenStore();
		}

        protected void OnHome_ProfileClicked()
        {
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(ProfileMenuView));
            var profileMenu = (ProfileMenuView)ViewReferences.Get(typeof(ProfileMenuView));
            if (profileMenu != null)
                profileMenu.OpenPlayerTab();
        }


        protected void OnHome_CasualClicked()
        {
            if (this.PlayerProfile.UiHintData.Play == 0)
            {
                this.PlayerProfile.UiHintData.Play = 1;
                RefreshHomeHints();
            }
            ToPlayScreen(GameFormat.Casual);
        }

        protected void OnHome_DraftClicked()
        {
            if (this.PlayerProfile.UiHintData.Draft == 0)
            {
                this.PlayerProfile.UiHintData.Draft = 1;
                RefreshHomeHints();
            }
            ToPlayScreen(GameFormat.Draft);
        }

        protected void OnHome_LeagueClicked()
        {
            if (this.PlayerProfile.UiHintData.League == 0)
            {
                this.PlayerProfile.UiHintData.League = 1;
                RefreshHomeHints();
            }
            ToPlayScreen(GameFormat.Constructed);
        }

        protected void OnHome_EventsClicked()
        {
            if (this.PlayerProfile.UiHintData.Event == 0)
            {
                this.PlayerProfile.UiHintData.Event = 1;
                RefreshHomeHints();
            }
            if (PlayerProfile.TournamentData.event_status == Messages.TournamentStatus.not_playing)
                ToEventsScreen();
            else
                ToPlayScreen(GameFormat.Tournament);
        }

        protected void RefreshHomeHints()
        {
            if(null != HomeButtons.EventUpdateHints)
            {
                HomeButtons.EventUpdateHints.Invoke();
            }
            this.SaveUiHintDataSignal.Dispatch();
        }
		#endregion
	}
}
