﻿using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using GameFormat = Messages.GameFormat;
using ChatUserStatus = ExitGames.Client.Photon.Chat.ChatUserStatus;
using TournamentStatus = Messages.TournamentStatus;
using Messages;

namespace NovaBlitz.UI
{
    public class EventsScreen : MenuScreen
    {
        [Inject] public PlayScreen PlayScreen { get; set; }

        [Inject] public NewDeckSignal NewDeckSignal { get; set; }
        [Inject] public DraftSessionLoadedSignal DraftSessionLoadedSignal { get; set; }
        [Inject] public ToFriendChallengeSignal ToFriendChallengeSignal { get; set; }


        protected GameFormat _Format = GameFormat.Casual;
        public GameFormat Format
        {
            get { return _Format; }
            set
            {
                _Format = value;
                var pscolumn = FindView<PlayScreenColumnView>();
                if (null != pscolumn)
                    pscolumn.Format = value;
                var deckDock = FindView<DeckListDockView>();
                if (null != deckDock)
                    deckDock.Format = value;
            }
        }

        public override void Init()
        {
            this.ChildViews = new Dictionary<Type, bool>{
                {typeof(MainHeaderView), true},
                {typeof(FriendListDockView), false},
                {typeof(EventScreenColumnView), true},
            };

            this.ToFriendChallengeSignal.AddListener(OnFriends_ChallengeClicked);
            base.Init();
        }

        protected override void RegisterChild(AnimatedView view)
        {
            base.RegisterChild(view);
            var eventScreenColumn = view as EventScreenColumnView;
            if (null != eventScreenColumn)
            {
                eventScreenColumn.EventBackClicked += OnBackClicked;
                eventScreenColumn.EventEnterClicked += OnEnterClicked;
                eventScreenColumn.EventTournamentClicked += OnTournamentClicked;
                eventScreenColumn.EventTournamentEntryClicked += OnEventEntryClicked;
                return;
            }
            //var friendScreen = view as FriendListDockView;
            //if (null != friendScreen)
            //    friendScreen.EventChallengeClicked += OnFriends_ChallengeClicked;
        }

        public override void Open()
        {
            /*bool hideCards = false;
            switch (this.Format)
            {
                case GameFormat.Draft:
                    if (this.PlayerProfile.DraftStatus == TournamentStatus.not_playing)
                        hideCards = true;
                    this.ChildViews[typeof(DeckListView)] = false;
                    break;
                case GameFormat.Constructed:
                    this.ChildViews[typeof(DeckListView)] = this.PlayerProfile.LeagueStatus != TournamentStatus.playing;
                    break;
                case GameFormat.Tournament:
                case GameFormat.Monthly:
                case GameFormat.Annual:
                    this.ChildViews[typeof(DeckListView)] = this.PlayerProfile.EventStatus != TournamentStatus.playing;
                    break;
                default:
                    this.ChildViews[typeof(DeckListView)] = true;
                    break;
            }

            this.ChildViews[typeof(DeckListDockView)] = !hideCards;*/
            base.Open();
        }

        protected void ToHomeScreen() { TransitionTo(typeof(HomeScreen)); }
        protected void ToPlayScreen(TournamentEventData eventData)
        {
            PlayScreenColumnView screen = (PlayScreenColumnView)ViewReferences.Get(typeof(PlayScreenColumnView));
            if (screen == null)
                screen = (PlayScreenColumnView)ViewReferences.Instantiate(typeof(PlayScreenColumnView));
            screen.TournamentEventData = eventData;
            this.PlayScreen.Format = GameFormat.Tournament;
            this.PlayScreen.PreviousScreenType = this.GetType();
            TransitionTo(typeof(PlayScreen));
        }

        #region Signal Listeners
        #endregion

        #region View Listeners
        protected void OnBackClicked()
        {
            //this.PlayerProfile.SelectedChallengeID = null;
            ToHomeScreen();
        }

        protected void OnEnterClicked(TournamentEventData eventData)
        {
            ToPlayScreen(eventData);
        }

        protected void OnEventEntryClicked(string eventId)
        {
            //var eventStats = ToggleView<EventStatsView>(true);
            //eventStats.Refresh();
        }

        protected void OnTournamentClicked(string eventId)
        {
            //var eventStats = ToggleView<EventStatsView>(true);
            //eventStats.Refresh();
        }

        public void OnFriends_ChallengeClicked(string playfabID)
        {
            if (this.IsOpen)
            {
                this.PlayScreen.Format = GameFormat.Challenge;
                TransitionTo(typeof(PlayScreen));
                this.PlayScreen.Open();
            }
        }

        #endregion
    }
}
