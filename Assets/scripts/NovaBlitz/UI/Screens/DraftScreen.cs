﻿using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using GameFormat = Messages.GameFormat;
using TournamentStatus = Messages.TournamentStatus;

namespace NovaBlitz.UI
{
	public class DraftScreen : MenuScreen
	{
        [Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal { get; set; }

        public override void Init()
		{
			this.ChildViews = new Dictionary<Type, bool>{
				{typeof(DraftPackBrowserView), true},
                {typeof(DraftCardListView), true},
                {typeof(DeckStatsView), false}
			};

			base.Init();
		}

		protected override void RegisterChild(AnimatedView view)
		{
			base.RegisterChild(view);
			var draftList = view as DraftCardListView;
			if (null != draftList) {
				draftList.EventDeckButtonClicked += OnCardList_DeckButtonClicked;
				draftList.EventDoneClicked += OnCardList_DoneClicked;
				return;
			}
		}

		protected void ToPlayScreen() { TransitionTo(typeof(PlayScreen)); }

		#region Signal Listeners
		#endregion

		#region View Listeners
		private void OnCardList_DeckButtonClicked()
		{
            this.OpenBlurredAnimatedViewSignal.Dispatch(typeof(DeckStatsView));
			var deckStats = ViewReferences.Get<DeckStatsView>();
			deckStats.IsEditable = false;
			deckStats.DeckData = this.PlayerProfile.DraftDeck;
			deckStats.Refresh(false);
		}

		public void OnCardList_DoneClicked()
		{
			ToPlayScreen();
		}
		#endregion
	}
}
