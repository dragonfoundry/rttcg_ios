﻿using UnityEngine;
using System;
using LinqTools;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class ScreenTransitionParams
	{
		public Type FromScreenType;
		public Type ToScreenType;
        public Messages.GameFormat Format;
	}

	public class ScreenTransitionSignal : Signal<ScreenTransitionParams> {}
	public class ScreenTransitionCommand : Command
	{
		[Inject] public ScreenTransitionParams SignalParams {get;set;}
        [Inject] public PlayerProfile PlayerProfile { get; set; }

        public override void Execute()
		{
            // only track last viewed after tutorials
            if (PlayerProfile != null && PlayerProfile.OnboardingProgress != null && PlayerProfile.OnboardingProgress.TutorialsCompleted)
                PlayerProfile.LastMenuScreenViewed = SignalParams.ToScreenType;

            var fromScreen = null == this.SignalParams.FromScreenType
				? null
				: this.injectionBinder.GetInstance(this.SignalParams.FromScreenType) as MenuScreen;
			var toScreen = this.injectionBinder.GetInstance(this.SignalParams.ToScreenType) as MenuScreen;
			if (toScreen == null)
				return;

            var playScreen = toScreen as PlayScreen;
            if (playScreen != null)
            {
                if (SignalParams.Format == Messages.GameFormat.Draft
                    || SignalParams.Format == Messages.GameFormat.Constructed
                    || SignalParams.Format == Messages.GameFormat.Tournament
                    || SignalParams.Format == Messages.GameFormat.Annual
                    || SignalParams.Format == Messages.GameFormat.Monthly
                    || SignalParams.Format == Messages.GameFormat.Tutorial)
                        playScreen.Format = SignalParams.Format;
            }
			
			if (null != fromScreen)
				fromScreen.Close(toScreen, toScreen.Open);
			else
                toScreen.Open();
		}
	}
}
