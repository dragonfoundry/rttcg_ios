﻿using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class LoginScreen : MenuScreen
	{
		public override void Init()
		{
			this.ChildViews = new Dictionary<Type, bool>{
				{typeof(LoginDialogNewFlowView), true}
			};
			base.Init();
		}
	}
}
