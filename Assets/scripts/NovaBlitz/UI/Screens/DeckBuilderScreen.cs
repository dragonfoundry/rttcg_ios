﻿using UnityEngine;
using System;
using LinqTools;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class DeckBuilderScreen : MenuScreen
	{
		public Type PreviousScreenType = typeof(HomeScreen);

		[Inject] public GameData GameData {get;set;}
		//[Inject] public PlayerProfile PlayerProfile {get;set;}
		[Inject] public DeckDeletedSignal DeckDeletedSignal {get;set;}
		[Inject] public FilterCardsSignal FilterCardsSignal {get;set;}
		[Inject] public MoveAnimatedViewToCanvasSignal MoveAnimatedViewToCanvasSignal {get;set;}
		[Inject] public OpenBlurredAnimatedViewSignal OpenBlurredAnimatedViewSignal {get;set;}

		public override void Init()
		{
			this.ChildViews = new Dictionary<Type, bool>{
				{typeof(CardBrowserView), true},
				{typeof(CardListView), true},
				//{typeof(CardFilterView), false},
				{typeof(DeckStatsView), false}
			};

			this.DeckDeletedSignal.AddListener(OnDeckDeleted);
			base.Init();
		}

		protected override void RegisterChild(AnimatedView view)
		{
			base.RegisterChild(view);
			var cardBrowser = view as CardBrowserView;
			if (null != cardBrowser) {
				cardBrowser.FilterBarWidget.EventResetClicked += OnCardBrowser_FilterResetClicked;
				return;
			}
			var cardList = view as CardListView;
			if (null != cardList) 
			{
				cardList.EventDeckButtonClicked += OnCardList_DeckButtonClicked;
				cardList.DoneDeckBuilding += OnCardList_DoneClicked;
				return;
			}
		}

		protected void ToPreviousScreen()
		{
			TransitionTo(this.PreviousScreenType);
		}

		#region Signal Listeners
		protected void OnDeckDeleted(int index)
		{
			ToPreviousScreen();
		}
		#endregion

		#region View Listeners

		private void OnCardBrowser_FilterResetClicked()
		{
			/*var cardFilter = FindView<CardFilterView>();
			if (null != cardFilter) {
				cardFilter.FilterWidget.ClearAll();
				cardFilter.FilterWidget.ClickApply();
			}*/
		}

		private void OnCardList_DeckButtonClicked()
		{
			// Because MenuScreens use this toggleView thing, it's difficult to open
			// the DeckStatsView using OpenBlurredAnimatedView without refactoring the MenuScreen stuff
			// so I'm just going to manually tell the stats screen to be on the overlay here -DMac
			var deckStats = ToggleView<DeckStatsView>(true);
			this.OpenBlurredAnimatedViewSignal.Dispatch(deckStats.GetType());

			deckStats.IsEditable = true;
			deckStats.DeckData = this.PlayerProfile.CurrentDeck;
			deckStats.Refresh(true);
		}

		protected void OnCardList_DoneClicked(DeckData deckData)
		{
            Debug.Log("To previous screen from deckbuilder");
			ToPreviousScreen();
		}
		#endregion
	}
}
