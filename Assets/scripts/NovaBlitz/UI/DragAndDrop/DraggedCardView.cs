﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using LinqTools;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Can switch disply between different card prefabs.
	/// </summary>
	public class DraggedCardView : BaseCardView, IRectTransform
	{
		public float Scale = 1f;
		public Dictionary<Type, BaseCardView> ChildViews = new Dictionary<Type, BaseCardView>();

		public RectTransform RectTransform {get; protected set;}
		public CanvasGroup CanvasGroup {get; protected set;}

        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.DraggedCardView; } }

        public override void RefreshCardData()
		{
			foreach (var child in this.ChildViews.Values)
			{
				child.CardData = _CardData;
			}
		}

        public void ScaleCardDisplay(Vector3 scale)
        {
            BaseCardView instance;
            if (this.ChildViews.TryGetValue(typeof(CardView), out instance))
            {
                (instance as CardView).Canvas.transform.localScale = scale / Scale;
            }
        }

        public void SwitchDisplay<T>(T viewPrefab) where T : BaseCardView
        {
            this.RectTransform.localScale = Vector3.one * this.Scale;
            Type prefabType = viewPrefab.GetType();
			BaseCardView instance;

			bool isProxyLoading = false;
			if (!this.ChildViews.TryGetValue(prefabType, out instance)) 
			{
				instance = UITools.PoolSpawnRect(viewPrefab, Vector3.zero, this.RectTransform);
				this.ChildViews[prefabType] = instance;
				if(instance is CardListItemProxyView)
				{
					isProxyLoading = true;
					instance.IsVisible = false;
				}
				var irx = (RectTransform)instance.transform;
				irx.anchorMin = Vector2.one * 0.5f;
				irx.anchorMax = Vector2.one * 0.5f;
				irx.localPosition = Vector3.zero;
				
				if (null != _CardData)
				{
					//Debug.Log("setting card art:" + _CardData.ArtID);
					instance.CardData = _CardData;
					Texture tex = AssetBundleManager.Instance.LoadAsset<Texture>(_CardData.ArtID);// Resources.Load(_CardData.ArtID) as Texture;
					instance.SetCardArt(tex);
				}
                ScaleCardDisplay(Vector3.one * 1.5f);
            }

			foreach (var child in ChildViews.Values.Where(v => v != instance))
				child.IsVisible = false;

			if(isProxyLoading == false)
			{
				instance.IsVisible = true;
			}
		}

		#region Mono
		protected virtual void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
			this.CanvasGroup = GetComponent<CanvasGroup>();
		}

		protected virtual void Start()
		{
			this.RectTransform.localScale = Vector3.one * this.Scale;
		}
        #endregion

        public void RecycleChildren()
        {
            foreach(var view in ChildViews)
            {
                if (view.Value != null)
                    view.Value.Recycle();
            }
            ChildViews.Clear();
        }

        protected void OnDestroy()
        {
            RecycleChildren();
        }

        public override void OnRecycle()
        {
            RecycleChildren();
            IsVisible = true;
        }
    }
}
