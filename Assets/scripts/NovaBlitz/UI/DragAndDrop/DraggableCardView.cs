﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using LinqTools;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Displays any card view as a child.
	/// Spawns a DraggedCard on drag or long press. This is parented under DragOverlay and follows pointer motion.
	/// IsLocked determines whether DraggedCard spawns.
	/// Requests a TwoTouch on click. IsEditable determines whether TwoTouch can edit deck.
	/// </summary>
	public class DraggableCardView : BaseCardView, IRectTransform
	{

		[SerializeField]
		protected DraggedCardView _DraggedCardPrefab;
        public bool IsBreak;
		public bool IsLocked;
		public bool IsEditable;
		public NovaAudioPlayer ClickAudio;
		public bool IsTwoTouchable = true;

		public RectTransform RectTransform {get; protected set;}
		public BaseCardView CardDisplay {get; protected set;}
		public DraggableItem DragItem {get; protected set;}
		public DraggedCardView DraggedCard {get; protected set;}

		public IDragOverlay DragOverlay {get;set;}
		public Action<DraggableCardView>CardDraggedCallback;
		public Action<TwoTouchParams> ShowTwoTouchCallback;
		public Action<DraggableCardView> CardHoveredCallback;
		public Action<DraggableCardView> CardUnhoveredCallback;

		public Action DismissTwoTouchCallback;
		
		private static DraggableCardView LastSelectedCard = null;

        // Pool state
        public override PooledObjectType PooledObjectType { get { return PooledObjectType.DraggableCardView; } }

        public override void RefreshCardData()
		{
			if (null != this.CardDisplay)
				this.CardDisplay.CardData = _CardData;
			if (null != _CardData)
				this.gameObject.name = string.Format("Draggable {0}", _CardData.Name);
		}

		public T InitCardDisplay<T>(T prefab, CardData cardData) where T : BaseCardView
		{
			DespawnCardDisplay();
            this.CardDisplay = UITools.PoolSpawnRect(prefab, Vector3.zero, this.RectTransform);
            (this.CardDisplay.transform as RectTransform).anchorMin = Vector2.one *0.5f;
            (this.CardDisplay.transform as RectTransform).anchorMax = Vector2.one * 0.5f;
            this.CardDisplay.transform.localPosition = Vector3.zero;

			this.CardData = cardData;
			return (T)this.CardDisplay;
		}
		
		public override void SelectCard(bool isSelected)
		{
            if(this.CardDisplay != null)
    			this.CardDisplay.SelectCard(isSelected);

			//Debug.Log("DraggableCardView: SelectCard: " + isSelected + " " + this);
			
			if(isSelected == true)
			{
				if(DraggableCardView.LastSelectedCard != null 
				&& DraggableCardView.LastSelectedCard != this)
				{
					DraggableCardView.LastSelectedCard.SelectCard(false);
				}
				
				DraggableCardView.LastSelectedCard = this;
			}
			else
			{
				if(DraggableCardView.LastSelectedCard == this)
				{
					DraggableCardView.LastSelectedCard = null;
				}
			}
		}

		public void DespawnCardDisplay()
		{
			if (null != this.CardDisplay) 
			{
				if (this.CardDisplay.transform.parent == this.RectTransform)
					this.CardDisplay.transform.SetParent(null);
				if (this.CardDisplay.gameObject.activeSelf)
					this.CardDisplay.Recycle();
				this.CardDisplay = null;
			}
		}

		protected void SpawnDraggedCard(Vector2 pos)
		{
			if (null == this.DraggedCard) {
                this.DraggedCard = UITools.PoolSpawnRect(_DraggedCardPrefab, Vector3.zero, this.RectTransform.parent);
				this.DragOverlay.Place(this.DraggedCard.RectTransform);
				this.DragOverlay.Move(this.DraggedCard.RectTransform, pos);
				this.DraggedCard.CardData = _CardData;
				this.DraggedCard.SwitchDisplay(this.CardDisplay);
				this.DraggedCard.CanvasGroup.blocksRaycasts = false;
                var cardView = this.CardDisplay as CardView;
                //var cardListItemView = this.CardDisplay as CardListItemView;
                if (cardView != null)
                {
                    this.DraggedCard.ScaleCardDisplay(cardView.Canvas.transform.localScale);
                }
                if(IsBreak)
                    Timer.Instance.StartCoroutine(DebugBreak());

                if (null != _CardData)
					this.DraggedCard.gameObject.name = string.Format("Dragged {0}", _CardData.Name);
                
			}
		}

        private IEnumerator DebugBreak()
        {
            yield return new WaitForSecondsRealtime(0.1f);
            Debug.Break();
        }

		protected void DespawnDraggedCard()
		{
			if (null != this.DraggedCard)
			{
				this.DraggedCard.Recycle ();
				this.DraggedCard = null;

				//GameObject.Destroy (this.DraggedCard.gameObject);
			}
		}
		
		

		#region Drag
		protected void OnDragItem_BeginDrag(Vector2 pos)
		{
			if (!this.IsLocked)
			{
				SpawnDraggedCard(pos);
                if(this.CardDraggedCallback != null)
    				this.CardDraggedCallback.Invoke(this);
			}
		}

		protected void OnDragItem_Drag(Vector2 pos)
		{
			if (null != this.DraggedCard)
				this.DragOverlay.Move(this.DraggedCard.RectTransform, pos);
		}

		protected void OnDragItem_EndDrag(Vector2 pos)
		{
			DespawnDraggedCard();
		}

		protected void OnDragItem_Click(Vector2 pos)
		{
			if(this.IsTwoTouchable == false) return;

			// TODO: Look up the actual collection total
			this.ClickAudio.Play();

			//Debug.Log("LastSelectedCard:" + DraggableCardView.LastSelectedCard);

			if( this.CardDisplay.IsSelected == false)
			{
				if(this.ShowTwoTouchCallback != null)
				{
                    TwoTouchParams twoTouchParams = new TwoTouchParams() {
                        CardId = _CardData.CardID,
                        DestCardScale = this.IsEditable ? 1.9f : 1.2f,
                        SourceTransform = this.CardDisplay.GetRectTransform(),
                        IsEditable = this.IsEditable, DraggableCardView = this,
                        IsBlurred = true
					};
						
					this.ShowTwoTouchCallback.Invoke(twoTouchParams);
				}
				//this.TwoTouchOverlay.PlaceTwoTouch(_CardData, this.IsEditable ? 1.9f : 1.2f, this.CardDisplay.GetRectTransform(), this.IsEditable, this);
				this.SelectCard(true);
			}
			else
			{
				this.SelectCard(false);
				if(this.DismissTwoTouchCallback != null)
				{
					this.DismissTwoTouchCallback.Invoke();
				}
			}
		}

		protected void OnDragItem_LongPress(Vector2 pos)
		{
			if (!this.IsLocked)
				SpawnDraggedCard(pos);
		}

		protected void OnDragItem_LongPressUp(Vector2 pos)
		{
			DespawnDraggedCard();
		}
		#endregion

		#region Mono
		protected virtual void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
			this.DragItem = GetComponent<DraggableItem>();
			this.DragItem.EventBeginDrag += OnDragItem_BeginDrag;
			this.DragItem.EventDrag += OnDragItem_Drag;
			this.DragItem.EventEndDrag += OnDragItem_EndDrag;
			this.DragItem.EventClick += OnDragItem_Click;
			this.DragItem.EventLongPress += OnDragItem_LongPress;
			this.DragItem.EventLongPressUp += OnDragItem_LongPressUp;
			this.DragItem.EventHover += OnDragItem_Hover;
			this.DragItem.EventUnhover += OnDragItem_Unhover;
		}

        private void OnDragItem_Unhover()
        {
			if(Input.GetMouseButton(0)== false)
			{
				if(this.CardUnhoveredCallback != null)
					this.CardUnhoveredCallback.Invoke(this);
			}
        }

        private void OnDragItem_Hover()
        {
			if(Input.GetMouseButton(0)== false)
			{
				if(this.CardHoveredCallback != null)
					this.CardHoveredCallback.Invoke(this);
			}
        }

        override public void OnRecycle()
        {
            if (!_IsAppQuitting)
            {
                DespawnCardDisplay();
                DespawnDraggedCard();
                ShowTwoTouchCallback = null;
                DismissTwoTouchCallback = null;
                CardDraggedCallback = null;
                CardHoveredCallback = null;
                CardUnhoveredCallback = null;
                CardData = null;
                IsVisible = true;
                IsLocked = false;
                IsEditable = false;
                DragItem.Constraint = DragConstraint.Horizontal;
            }
        }

		protected void OnDestroy()
		{
            if (!_IsAppQuitting)
            {
                DespawnCardDisplay();
                DespawnDraggedCard();
            }
        }

		protected bool _IsAppQuitting;
		protected void OnApplicationQuit() { _IsAppQuitting = true; }
		#endregion
	}
}
