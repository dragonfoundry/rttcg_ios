﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Detects drags with optional axis constraint.
	/// Detects clicks, canceling when dragged. Long press initiates drag.
	/// If PassThrough is enabled, drag events outside of constrained axis will propagate to parent (useful for ScrollView).
	/// Adjust drag threshold in the EventSystem instance.
	/// </summary>
	public class DraggableItem : MonoBehaviour, IRectTransform, IInitializePotentialDragHandler, IBeginDragHandler, 
		IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
	{
		public event Action<Vector2> EventBeginDrag;
		public event Action<Vector2> EventDrag;
		public event Action<Vector2> EventEndDrag;
		public event Action<Vector2> EventClick;
		public event Action<Vector2> EventLongPress;
		public event Action<Vector2> EventLongPressUp;
		public event Action EventHover;
		public event Action EventUnhover;

		public float LongPressDuration = 1f;
		public DragConstraint Constraint = DragConstraint.None;
		public bool PassThrough = true;
		public bool DragOverActivation = false;
		private bool isDraggedByDragOver = false;
		private static bool IS_DRAGGING = false;

		protected Vector2 _PressPosition;
		protected float _NextLongPressTime;
		protected bool _IsDetectingLongPress;
		protected bool _IsHoldingLongPress;

		public bool IsDragging {get; protected set;}
		public RectTransform RectTransform {get; protected set;}

		#region Notifiers
		protected void BeginValidDrag(Vector2 pos)
		{
			this.IsDragging = true;
			NotifyBeginDrag(pos);
		}

		protected void PassInitDrag(BaseEventData eventData) 	{ ExecuteEvents.ExecuteHierarchy(this.RectTransform.parent.gameObject, eventData, ExecuteEvents.initializePotentialDrag); }
		protected void PassBeginDrag(BaseEventData eventData) 	{ ExecuteEvents.ExecuteHierarchy(this.RectTransform.parent.gameObject, eventData, ExecuteEvents.beginDragHandler); }
		protected void PassDrag(BaseEventData eventData) 		{ ExecuteEvents.ExecuteHierarchy(this.RectTransform.parent.gameObject, eventData, ExecuteEvents.dragHandler); }
		protected void PassEndDrag(BaseEventData eventData) 	{ ExecuteEvents.ExecuteHierarchy(this.RectTransform.parent.gameObject, eventData, ExecuteEvents.endDragHandler); }

		protected void NotifyBeginDrag(Vector2 pos) { if (null != this.EventBeginDrag) this.EventBeginDrag.Invoke(pos); }
		protected void NotifyDrag(Vector2 pos) 		{ if (null != this.EventDrag) this.EventDrag.Invoke(pos); }
		protected void NotifyEndDrag(Vector2 pos) 	{ if (null != this.EventEndDrag) this.EventEndDrag.Invoke(pos); }
		protected void NotifyClick(Vector2 pos) 	{ if (null != this.EventClick) this.EventClick.Invoke(pos); }
		protected void NotifyLongPress(Vector2 pos) { if (null != this.EventLongPress) this.EventLongPress.Invoke(pos); }
		protected void NotifyLongPressUp(Vector2 pos) { if (null != this.EventLongPressUp) this.EventLongPressUp.Invoke(pos); }
		#endregion

		#region EventSystem
		public void OnInitializePotentialDrag(PointerEventData eventData)
		{
			if (this.PassThrough) PassInitDrag(eventData);
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			_IsDetectingLongPress = false;
			_IsHoldingLongPress = false;
			var dir = eventData.position - eventData.pressPosition;
			bool vertical = Mathf.Abs(dir.y) > Mathf.Abs(dir.x);
			
			DraggableItem.IS_DRAGGING = true;

			switch (this.Constraint) {
			case DragConstraint.None:
				BeginValidDrag(eventData.position);
				break;
			case DragConstraint.Horizontal:
				if (!vertical) BeginValidDrag(eventData.position);
				else if (this.PassThrough) PassBeginDrag(eventData);
				break;
			case DragConstraint.Vertical:
				if (vertical) BeginValidDrag(eventData.position);
				else if (this.PassThrough) PassBeginDrag(eventData);
				break;
			}
		}

		public void OnDrag(PointerEventData eventData)
		{
			if (this.IsDragging) NotifyDrag(eventData.position);
			else PassDrag(eventData);
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			if (this.IsDragging) NotifyEndDrag(eventData.position);
			else PassEndDrag(eventData);
			this.IsDragging = false;
			DraggableItem.IS_DRAGGING = false;
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
				return;

			_IsDetectingLongPress = true;
			_PressPosition = eventData.pressPosition;
			_NextLongPressTime = Time.time + this.LongPressDuration;
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
				return;

			_IsDetectingLongPress = false;
			if (this.IsDragging) {
				if (_IsHoldingLongPress) NotifyLongPressUp(eventData.position);
			}
			else if (!eventData.dragging && Time.time < _NextLongPressTime)
				NotifyClick(eventData.position);
			_IsHoldingLongPress = false;
		}
		#endregion

		#region Mono
		void Awake()
		{
			this.RectTransform = (RectTransform)this.transform;
		}

		void Update()
		{
			if (_IsDetectingLongPress) {
				if (Time.time >= _NextLongPressTime) {
					_IsDetectingLongPress = false;
					_IsHoldingLongPress = true;
					BeginValidDrag(_PressPosition);
					NotifyLongPress(_PressPosition);
				}
			}
			
			if(this.isDraggedByDragOver)
			{
				PointerEventData eventData = new PointerEventData(EventSystem.current);
				eventData.position = Input.mousePosition;
				
				if(Input.GetMouseButton(0)==true)
				{
					this.OnDrag(eventData);
				}
				else
				{
					this.isDraggedByDragOver = false;
					this.OnEndDrag(eventData);
				}
			}
		}

        public void OnPointerEnter(PointerEventData eventData)
        {
            if(this.DragOverActivation == true 
			&& Input.GetMouseButton(0) 
			&& DraggableItem.IS_DRAGGING == false)
			{
				this.isDraggedByDragOver = true;
				this.OnBeginDrag(eventData);
			}

			if(this.EventHover != null)
			{
				this.EventHover.Invoke();
			}
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if(this.EventUnhover != null)
			{
				this.EventUnhover.Invoke();
			}
        }

        #endregion
    }

	public enum DragConstraint
	{
		None,
		Horizontal,
		Vertical
	}
}