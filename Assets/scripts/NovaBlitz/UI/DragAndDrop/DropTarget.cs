using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace NovaBlitz.UI
{
	/// <summary>
	/// Detects drop and hover of DraggableItems. Dragged objects under pointer must not block raycasts.
	/// </summary>
	public class DropTarget : MonoBehaviour, IPointerEnterHandler, IDropHandler
	{
		public event Action<DraggableItem> EventDragItemHovered;
		public event Action<DraggableItem> EventDragItemDropped;

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (!eventData.dragging)
				return;

			var dragItem = eventData.pointerDrag.GetComponent<DraggableItem>();
			if (null != dragItem && dragItem.IsDragging) {
				NotifyHovered(dragItem);
			}
		}

		public void OnDrop(PointerEventData eventData)
		{
			var dragItem = eventData.pointerDrag.GetComponent<DraggableItem>();
			if (null != dragItem && dragItem.IsDragging)
				NotifyDropped(dragItem);
		}

		protected void NotifyHovered(DraggableItem dragItem) { if (null != this.EventDragItemHovered) this.EventDragItemHovered.Invoke(dragItem); }
		protected void NotifyDropped(DraggableItem dragItem) { if (null != this.EventDragItemDropped) this.EventDragItemDropped.Invoke(dragItem); }
	}
}
