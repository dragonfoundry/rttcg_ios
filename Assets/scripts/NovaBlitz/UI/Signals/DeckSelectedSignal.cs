﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using NovaBlitz.UI;

public class DeckSelectedSignal : Signal<DeckData>{} // DeckData of selected Deck
