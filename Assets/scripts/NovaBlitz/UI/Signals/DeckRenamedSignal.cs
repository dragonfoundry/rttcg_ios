using strange.extensions.signal.impl;
using NovaBlitz.UI;

public class DeckRenamedSignal : Signal<DeckData> {}