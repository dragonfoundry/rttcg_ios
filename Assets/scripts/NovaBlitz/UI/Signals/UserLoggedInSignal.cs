﻿using UnityEngine;
using NovaBlitz.Game;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using PlayFab.ClientModels;

namespace NovaBlitz.UI
{
    public class UserLoggedInSignal : Signal<LoginResult> { }   // PlayfabSessionTicket
    public class UserLoggedInCommand : Command
    {
        [Inject] public LoginResult result { get; set; }
        [Inject] public PlayerProfile PlayerProfile { get; set; }
        [Inject] public ClientPrefs ClientPrefs { get; set; }
        [Inject] public ClientService ClientService { get; set; }
		[Inject] public InitializeAnalyticsSignal InitializeAnalyticsSignal { get; set; }
		[Inject] public LoadAccountInfoSignal LoadAccountInfoSignal {get;set;}
		[Inject] public CloseAnimatedViewSignal CloseAnimatedViewSignal {get;set;}
        [Inject] public PlayFabChatClient PlayfabChatClient {get;set;}
        [Inject] public InitializeBaseViewsSignal InitializeBaseViewsSignal { get; set; }
        [Inject] public UnloadUnusedResourcesSignal UnloadUnusedResourcesSignal { get; set; }

        public override void Execute()
        {

            Debug.LogFormat("User Logged in: {0}:{1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Now.Millisecond);
            // Set the session ticket everywhere - this is too many places...

            // Clean up old data
            if(this.PlayerProfile.MetaProfile.PlayFabSessionToken != null)
            {

                if (PlayfabChatClient != null && PlayfabChatClient.chatClient != null)
                    this.PlayfabChatClient.chatClient.Disconnect();
                this.ClientPrefs.ResetClientPrefs();
                PlayerProfile.ResetProfile();
                this.UnloadUnusedResourcesSignal.Dispatch();
                this.ClientService.OnLogout();
            }

            this.PlayerProfile.MetaProfile.PlayFabSessionToken = result.SessionTicket;
            this.ClientPrefs.SessionTicket = result.SessionTicket;
            this.PlayerProfile.PlayFabId = result.PlayFabId;

            // Set the age gate
            string ageGate = PlayerPrefs.GetString("isAgeGatePassed", "false");
            Debug.LogFormat("player prefs age gate = {0}, profile age gate = {1}",
                ageGate,
                (PlayerProfile.isAgeGatePassed) != null ? PlayerProfile.isAgeGatePassed.ToString() : "null");
            if (ageGate == "true" || PlayerProfile.isAgeGatePassed == true)
            {
                PlayerPrefs.SetString("isAgeGatePassed", "true");
                PlayerProfile.isAgeGatePassed = true;
            }
            else
            {
                PlayerProfile.isAgeGatePassed = false;
            }

            this.CloseAnimatedViewSignal.Dispatch(typeof(LoginDialogNewFlowView));

            this.InitializeAnalyticsSignal.Dispatch();
            this.LoadAccountInfoSignal.Dispatch(this.ClientPrefs.SessionTicket);

            this.InitializeBaseViewsSignal.Dispatch();
        }
    }
}