﻿using UnityEngine;
using System.Collections;
using PlayFab;
using strange.extensions.signal.impl;

public class PlayFabErrorRaisedSignal : Signal<PlayFabError> {}