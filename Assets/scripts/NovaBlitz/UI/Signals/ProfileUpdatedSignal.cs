﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;

public enum ProfileSection
{
	DeckList,
	Currency,
	DraftDeck,
    Inventory,
	Statistics,
    Friends,
    TournamentData,
}
public class ProfileUpdatedSignal : Signal<ProfileSection>{}
