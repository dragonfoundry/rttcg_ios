﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using strange.extensions.command.impl;
using NovaBlitz.Game;

public class CancelSignal : Signal { }
public class CancelMatchmakingSignal : Signal { }

public class CancelCommand : Command
{
    [Inject] public ClientService ClientService { get; set; }
    public override void Execute()
    {
        Messages.ConcedeGameRequest request = new Messages.ConcedeGameRequest
        {
            GameGuid = this.ClientService.SessionGuid
        };
        this.ClientService.SendMessage(request);
    }
}
