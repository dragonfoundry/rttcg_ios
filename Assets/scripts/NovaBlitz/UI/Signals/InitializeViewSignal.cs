﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;

public class InitializeViewSignal : Signal<System.Type> {}