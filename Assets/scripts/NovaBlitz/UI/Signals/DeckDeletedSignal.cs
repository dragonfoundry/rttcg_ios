﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;

public class DeckDeletedSignal : Signal<int>{} // Index deck was deleted from
