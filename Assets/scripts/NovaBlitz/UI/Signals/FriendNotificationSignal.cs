﻿using System;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using LinqTools;
using System.Text;

namespace NovaBlitz.UI
{

    public struct FriendNotification
    {
        public int numberOfFriends;
        public int? numberOnlineFriends;
        public bool? isChallengeRequest;
        public bool? isFriendRequest;
        public bool? isChatMessage;
    }
    public class FriendNotificationSignal : Signal<FriendNotification> { }
}