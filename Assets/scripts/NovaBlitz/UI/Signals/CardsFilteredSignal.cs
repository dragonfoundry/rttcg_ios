﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using System.Collections.Generic;

// Signal indicating Card filter results are ready
public class CardsFilteredSignal : Signal<List<int>> {} //List of CardID's that match the filter criteria