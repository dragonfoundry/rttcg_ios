using System;
using System.Collections.Generic;
using strange.extensions.signal.impl;

/// <summary>
/// Designed to contain only one callback per ID. Use Prepare and Reset instead of standard API.
/// </summary>
public abstract class BaseIDActionSignal : Signal<string>
{
	protected Dictionary<string, Action> _Callbacks = new Dictionary<string, Action>();
	
	public virtual void Prepare(string id, Action callback) { _Callbacks[id] = callback ?? delegate {}; }
	public virtual void Reset(string id) { _Callbacks.Remove(id); }
	
	new public virtual void Dispatch(string id)
	{
		Action callback = delegate {};
		if (_Callbacks.ContainsKey(id))
			callback = _Callbacks[id];

		callback.Invoke();
		Reset(id);
		base.Dispatch(id);
	}
}
