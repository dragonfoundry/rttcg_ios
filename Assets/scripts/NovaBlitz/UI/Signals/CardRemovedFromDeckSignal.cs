using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using System.Collections.Generic;
using NovaBlitz.UI;

public class CardRemovedFromDeckSignal : Signal<CardData, DeckData>{} // CardData, reference to Deck 