﻿
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
    public class ToFriendChallengeSignal : Signal<string> { }
}
