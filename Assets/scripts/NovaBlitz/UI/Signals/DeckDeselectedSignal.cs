﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using NovaBlitz.UI;

public class DeckDeselectedSignal : Signal<DeckData>{} // DeckData of deselected Deck
