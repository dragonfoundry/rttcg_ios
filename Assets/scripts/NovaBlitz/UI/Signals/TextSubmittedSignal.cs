using System;
using System.Collections.Generic;
using strange.extensions.signal.impl;

namespace NovaBlitz.UI
{
	public class TextSubmittedSignal : Signal<string, string>
	{
		protected Dictionary<string, Action<string>> _Callbacks = new Dictionary<string, Action<string>>();
		
		public virtual void Prepare(string id, Action<string> callback) { _Callbacks[id] = callback ?? delegate {}; }
		public virtual void Reset(string id) { _Callbacks.Remove(id); }
		
		new public virtual void Dispatch(string id, string text)
		{
			Action<string> callback = delegate {};
			if (_Callbacks.ContainsKey(id))
				callback = _Callbacks[id];

			callback.Invoke(text);
			Reset(id);
			base.Dispatch(id, text);
		}
	}
}
