﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using NovaBlitz.UI;

public class DeckClonedSignal : Signal<DeckData, string>{} // SourceDeck, ID of deck Cloned
