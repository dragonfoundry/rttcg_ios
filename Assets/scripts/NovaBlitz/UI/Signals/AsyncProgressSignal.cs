﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;

public enum AsyncProgress
{
    NoProgress = 0,
    ObjectPool = 1,
	MatchmakeRequest = 2,
	CreateGameRequest = 3,
	JoinGameRequest = 4,
	ReadyToStartGameRequest = 5,
    PaidGameNewOrResumeRequest = 6,
	DropFromEvent = 7,
    ServerNotFound = 8,
    CancelMatchmaking = 9,
    CardArtPositions = 10,
    LoadingAudio = 12,
    Authenticating = 13,
    TitleData = 14,
    FriendsList = 15,
    Complete = 16,
}

public class AsyncProgressSignal : Signal<AsyncProgress, float> {} // ProgressKey, value between 0-1.0f