﻿using UnityEngine;

namespace TMPro {

	[ExecuteInEditMode]
	public abstract class MarginShaper : MonoBehaviour {

		protected RectTransform rectTransform;

		public virtual void Awake () {
			TMP_Text text = GetComponent<TMP_Text>();
			if (!text) {
				Debug.LogError("TextMesh Pro component required.", this);
			}
			if (text.marginShaper != this) {
				text.marginShaper = this;
			}
			rectTransform = transform as RectTransform;
		}

		public abstract Vector2 GetShapedMargins (float lineOffset);
	}
}