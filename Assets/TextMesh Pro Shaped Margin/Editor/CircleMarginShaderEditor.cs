﻿using UnityEditor;
using UnityEngine;

namespace TMPro.EditorUtilities {

	[CustomEditor(typeof(CircleMarginShaper))]
	public class CircleMarginShaderEditor : Editor {

		public override void OnInspectorGUI () {
			EditorGUI.BeginChangeCheck();
			DrawDefaultInspector();
			if (EditorGUI.EndChangeCheck()) {
				foreach (CircleMarginShaper c in targets) {
					c.GetComponent<TMP_Text>().ForceMeshUpdate();
				}
			}
		}

		void OnSceneGUI () {
			CircleMarginShaper shaper = target as CircleMarginShaper;
			Rect r = (shaper.transform as RectTransform).rect;
			Vector3 center = shaper.center;
			center.y += r.yMax;

			Handles.DrawWireDisc(
				shaper.transform.TransformPoint(center),
				shaper.transform.forward,
				shaper.radius
			);
		}
	}
}