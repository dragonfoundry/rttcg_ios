﻿using UnityEngine;

namespace TMPro
{

    public class CardRulesMarginShaper : MarginShaper
    {

        public float radius = 100f;
        public float basewidth = 160f;
        public float fullwidth = 190f;

        public Vector2 center;

        public override Vector2 GetShapedMargins(float lineOffset)
        {
            Rect r = rectTransform.rect;

            lineOffset += center.y;
            float extends = Mathf.Sqrt(radius * radius - lineOffset * lineOffset);

            Vector2 margins;
            margins.x = r.width * 0.5f - extends + center.x;
            margins.y = r.width * 0.5f - extends - center.x;
            return margins;
        }
    }
}