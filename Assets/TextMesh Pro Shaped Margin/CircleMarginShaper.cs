﻿using UnityEngine;

namespace TMPro {

	public class CircleMarginShaper : MarginShaper {

		public float radius = 100f;

		public Vector2 center;

		public override Vector2 GetShapedMargins (float lineOffset)
        {
			Rect r = rectTransform.rect;

			lineOffset += center.y;
			float extends = Mathf.Sqrt(radius * radius - lineOffset * lineOffset);

			Vector2 margins;
			margins.x = r.width * 0.5f - extends + center.x;
			margins.y = r.width * 0.5f - extends - center.x;
			return margins;
		}
	}
}