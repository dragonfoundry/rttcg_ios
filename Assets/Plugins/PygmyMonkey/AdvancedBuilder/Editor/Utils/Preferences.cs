﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace PygmyMonkey.AdvancedBuilder
{
	public class Preferences
	{
		private static string m_PackagePath;
		public static string packagePath
		{
			get
			{
				Init();
				return m_PackagePath;
			}
		}

		private static bool m_HasInit = false;
		private static string m_SavedPackagePath;
		private static bool m_DoesFolderExists;

		[PreferenceItem("Adv. Builder")]
		public static void PreferencesGUI()
		{
			// Load the preferences
			Init();

			// Preferences GUI
			EditorGUILayout.LabelField("Package path:");
			m_PackagePath = EditorGUILayout.TextField(m_PackagePath);

			if (!m_DoesFolderExists)
			{
				EditorGUILayout.HelpBox("Sorry but this path does not exist, can't save this!", MessageType.Error);
			}

			GUI.enabled = m_DoesFolderExists && !m_PackagePath.Equals(m_SavedPackagePath);
			if (GUILayout.Button("Save"))
			{
				EditorPrefs.SetString("PREF_ADVANCED_BUILDER_PACKAGE_PATH", m_PackagePath);
				m_SavedPackagePath = m_PackagePath;
			}
			GUI.enabled = true;

			if (GUI.changed)
			{
				m_DoesFolderExists = Directory.Exists(Application.dataPath.Replace("Assets", "") + m_PackagePath);
			}
		}

		private static void Init()
		{
			if (!m_HasInit)
			{
				string path = EditorPrefs.GetString("PREF_ADVANCED_BUILDER_PACKAGE_PATH", "Assets/PygmyMonkey/");

				m_PackagePath = path;
				m_SavedPackagePath = path;
				m_HasInit = true;
			}
		}

		public static AdvancedBuilder GetAdvancedBuilder()
		{
			Init();
			return (AdvancedBuilder)AssetDatabase.LoadAssetAtPath(m_PackagePath + "AdvancedBuilder/Editor/AdvancedBuilder.asset", typeof(AdvancedBuilder));
		}
	}
}