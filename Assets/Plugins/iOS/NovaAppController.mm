#import "UnityAppController.h"

@interface NovaAppController : UnityAppController {}
@end

@implementation NovaAppController
- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application {
    printf_console("WARNING NOVA APP CONTROLLER -> applicationDidReceiveMemoryWarning()\n");
    UnitySendMessage("MainContext", "ReceivedMemoryWarning","");
}
@end

IMPL_APP_CONTROLLER_SUBCLASS(NovaAppController)
