﻿using UnityEngine;
using UnityEditor;
using Threeyes.AssetManager;

[CustomEditor(typeof(AssetBundleManager), false)]
public class AssetBundleManagerInspector : AssetManagerBaseInspector<AssetBundleManager, AssetBundle>
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
