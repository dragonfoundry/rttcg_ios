﻿using UnityEngine;
using System.Collections;

public class CardDealerDefaultBehavior : StateMachineBehaviour
{
	public CardDealer CardDealer;
	
	/// <summary>
	/// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	/// </summary>
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
	{
		/*
		animator.SetBool("IsDealing", false);
       // Debug.Log("On State enter Default:" + this.GetInstanceID());
        this.CardDealer = animator.gameObject.GetComponent<CardDealer>();
		*/
	}

    /// <summary>
    /// OnStateUpdate is called on each Update frame between OnStateEnter 
    /// and OnStateExit callbacks. It is only called for the state
    /// the behavior script is attached to, in this case the Default State
    /// </summary>
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
	{
		/*
    	if(this.CardDealer.NumCardsToDeal > 0 
			&& animator.GetBool("IsDealing") == false)
		{
			animator.SetBool("IsDealing", true);
			this.CardDealer.CardDealt();
		}
		*/
    }
}
