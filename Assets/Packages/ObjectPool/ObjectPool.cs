﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NovaBlitz.UI;

public sealed class ObjectPool : MonoBehaviour
{
	Dictionary<PooledObjectType, List<PoolableMonoBehaviour>> objectLookup = new Dictionary<PooledObjectType, List<PoolableMonoBehaviour>>();
	Dictionary<PooledObjectType, PoolableMonoBehaviour> prefabLookup = new Dictionary<PooledObjectType, PoolableMonoBehaviour>();
	private bool IsQuitting = false;

    public static void Clear()
	{
        if (Instance != null)
        {
            Instance.objectLookup.Clear();
            Instance.prefabLookup.Clear();
        }
	}

	public static bool PoolExists(PooledObjectType type) { return null != Instance && Instance.prefabLookup.ContainsKey(type) && Instance.objectLookup.ContainsKey(type); }


    public static T SpawnPrefab<T>(PooledObjectType type, Vector3? pos = null, Transform parent = null) where T : PoolableMonoBehaviour
    {
        T prefab = GetPrefab<T>(type);
        return Spawn(prefab, pos ?? Vector3.zero, Quaternion.identity, parent);
    }

    public static T GetPrefab<T>(PooledObjectType type) where T : PoolableMonoBehaviour
    {
        PoolableMonoBehaviour mb;
        Instance.prefabLookup.TryGetValue(type, out mb);
        return mb as T;
    }

	public static void CreatePool<T>(T prefab) where T : PoolableMonoBehaviour
    {

        if (Instance == null)
        {
            Debug.Log("Creating Object Pool");
            var obj = new GameObject("_ObjectPool");
            obj.transform.localPosition = Vector3.zero;
            Instance = obj.AddComponent<ObjectPool>();
            GameObject uiRoot = GameObject.Find("UI Root");
            if (uiRoot != null)
            {
                Instance.transform.SetParent(uiRoot.transform);
            }
        }
        if (!Instance.objectLookup.ContainsKey (prefab.PooledObjectType))
			Instance.objectLookup.Add (prefab.PooledObjectType, new List<PoolableMonoBehaviour> ());
		

		if (!Instance.prefabLookup.ContainsKey (prefab.PooledObjectType))
			Instance.prefabLookup.Add (prefab.PooledObjectType, prefab);
		
	}

    public static void LimitPoolSize<T>(T prefab, int size) where T : PoolableMonoBehaviour
    {
        if (Instance != null && Instance.objectLookup.ContainsKey(prefab.PooledObjectType))
        {
            T obj = null;
            var list = Instance.objectLookup[prefab.PooledObjectType];
            //Debug.LogFormat("Limiting pool size: from {0} to {1} for {2}", list.Count, size, prefab.GetType().Name);

            if (list.Count > size)
            {
                for (int i = list.Count - 1; i >= size; i--)
                {
                    obj = list[i] as T;
                    if (null != obj) Destroy(obj.gameObject);
                    else Debug.LogWarningFormat("ObjectPool tried to destroy null object!");
                    obj = null; // PB added
                    list.RemoveAt(i);
                }
            }
        }
        else
        {
            Debug.LogErrorFormat("ObjectPool for {0} does not exist!", prefab.GetType().Name);
        }

    }


    public static void SetPoolSize<T>(T prefab, int size) where T : PoolableMonoBehaviour
    {
		if (Instance != null && Instance.objectLookup.ContainsKey(prefab.PooledObjectType))
		{
			T obj = null;
			var list = Instance.objectLookup[prefab.PooledObjectType];
            Debug.LogFormat("Setting pool size: from {0} to {1} for {2}", list.Count, size, prefab.GetType().Name);

            if (list.Count > size)
			{
				for(int i=list.Count-1; i>=size; i--)
				{ 
					obj = list[i] as T;
					if (null != obj) Destroy(obj.gameObject);
					else Debug.LogWarningFormat("ObjectPool tried to destroy null object!");
					obj = null; // PB added
					list.RemoveAt(i);
				}
			}
			else if(list.Count < size)
			{
				for(int i=list.Count; i<size;i++)
				{
					obj = (T)Object.Instantiate(prefab, Vector3.zero, Quaternion.identity);
					obj.transform.SetParent(Instance.transform);
					obj.gameObject.SetActive (false);
					list.Add (obj);
					//instance.prefabLookup.Add(obj, prefab);
				}
			}
		}
		else
		{
			Debug.LogErrorFormat("ObjectPool for {0} does not exist!", prefab.GetType().Name);
		}
	}
	
	public static void SetPoolSizeAsync<T>(T prefab, int size, int chunks, System.Action onFinished = null, System.Action<float> onProgress = null) where T : PoolableMonoBehaviour
    {
        if(Instance != null)
           Instance.StartCoroutine(Instance.PoolSizeCoRoutine<T>(prefab, size, chunks, onFinished, onProgress));
	}
	
	private IEnumerator PoolSizeCoRoutine<T>(T prefab, int size, int chunks, System.Action onFinished, System.Action<float>onProgress) where T : PoolableMonoBehaviour
    {
		float total = 0;
		float current = 0;
		
		if (Instance != null && Instance.objectLookup.ContainsKey(prefab.PooledObjectType))
		{
			T obj = null;
			var list = Instance.objectLookup[prefab.PooledObjectType];
			//Debug.LogFormat("in Poolsize coroutine for {0} list.Count:{1} size:{2} chunks:{3}", prefab.GetType(), list.Count, size, chunks);
			
			total = list.Count - size;
			
			if(list.Count > size)
			{
				//Debug.Log("Reducing down from " + list.Count + " to " + size);
				for(int i=list.Count-1; i>size; i--)
				{ 
					if(i< 0 || i > list.Count-1) continue;
					obj = list[i] as T;
					Destroy(obj.gameObject);
					list.RemoveAt(i);
					current++;
					obj = null; // PB added
                    // Do NOT destroy in chunks; iterating over a collection across multiple frames isn't particularly safe!
					/*if(i % chunks == 0)
					{
						if(onProgress != null) { onProgress.Invoke(current/total);}
						yield return null;
					}*/
				}
			}
			else if(list.Count < size)
			{
                var count = list.Count;
				total = size - count;
				//Debug.Log("Increasing up to" + list.Count + " from " + size);
				
				for(int i= count; i<size;i++)
				{
					obj = (T)Object.Instantiate(prefab, Vector3.zero, Quaternion.identity);
                    obj.Recycle();
					//obj.transform.SetParent(instance.transform);
					//obj.gameObject.SetActive (false);
					//list.Add(obj);
					current++;
					if(i % chunks == 0)
					{
                        count = list.Count;
                        if (onProgress != null) { onProgress.Invoke(current/total);}
						yield return null;
                        // Handle other objects adding to the pool during pool size adjustments
                        if(count < list.Count)
                        {
                            Debug.LogErrorFormat("Object pool {0} changed from {1} to {2} during pool resize", prefab.name, count, list.Count);
                            i += list.Count - count;
                        }
					}
				}
            }
            //Debug.LogFormat("Poolsize coroutine complete for {0} list.Count:{1} size:{2} chunks:{3}", prefab.GetType(), list.Count, size, chunks);
        }
		else
		{
			Debug.LogError("ObjectPool for " + prefab.GetType() + " does not exist!");
		}
		
		if(onProgress != null) { onProgress.Invoke(1.0f); }
		
		if(onFinished != null) { onFinished.Invoke(); }

		if(0 % chunks == 0)
			yield return null;
	}
	
	public static T Spawn<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent = null) where T : PoolableMonoBehaviour
    {
        T obj = null;
        if (Instance != null && Instance.objectLookup.ContainsKey(prefab.PooledObjectType))
		{
			var list = Instance.objectLookup[prefab.PooledObjectType];
            if (list.Count > 0)
            {
                while (obj == null && list.Count > 0)
                {
                    obj = list[0] as T;
                    list.RemoveAt(0);
                }
                if (obj != null)
                {
                    if(parent != null)
                        obj.transform.SetParent(parent);
                    obj.transform.localPosition = position;
                    obj.transform.localRotation = rotation;
                    obj.gameObject.SetActive(true);
                    //instance.prefabLookup.Add(obj, prefab);
                    return (T)obj;
                }
            }
            if(obj == null)
            {
                obj = (T)Object.Instantiate(prefab, position, rotation);
            }
		}
		else
            obj = (T)Object.Instantiate(prefab, position, rotation);

        if (parent != null)
            obj.transform.SetParent(parent);
        obj.transform.localPosition = position;
        obj.transform.localRotation = rotation;
        obj.gameObject.SetActive(true);
        //instance.prefabLookup.Add(obj, prefab);
        return (T)obj;
    }
    /*public static T Spawn<T>(T prefab, Vector3 position) where T : PoolableMonoBehaviour
	{
		return Spawn(prefab, position, Quaternion.identity);
	}*/
    public static T Spawn<T>(T prefab) where T : PoolableMonoBehaviour
    {
		return Spawn(prefab, Vector3.zero, Quaternion.identity);
	}
	
	/*public static void Recycle<T>(T obj) where T : PoolableMonoBehaviour
    {
		Recycle(obj);
	}*/
    
	public static void Recycle(PoolableMonoBehaviour obj)
	{
        if (Instance == null || Instance.IsQuitting)
            return;
        if (obj == null)
        {
            Debug.LogErrorFormat("Recycled Card is NULL");
            return;
        }
        var type = obj.PooledObjectType;
		//if (null == type) type = obj.GetType();

        if (Instance.prefabLookup.ContainsKey(type) && Instance.objectLookup.ContainsKey(type))//instance.prefabLookup.ContainsKey(obj))
        {
            var prefab = Instance.prefabLookup[type];
            var pool = Instance.objectLookup[type];
            if (!pool.Contains(obj))
            {
                pool.Add(obj);
                obj.OnRecycle();
                obj.gameObject.SetActive(false);
                obj.transform.SetParent(Instance.transform);
                //Debug.Log("Recycling object of type "  + obj.GetType() +  " and setting parent to:" + instance, obj.gameObject);
            }
            else
            {
                var cardFront = obj as CardFrontController;
                var cardBack = obj as CardBackController;
                if (cardFront != null)
                    Debug.LogFormat("#ObjectPool# Attempting to Re-Recycle card {0}", cardFront.CardIdAndName);
                else if (cardBack != null)
                    Debug.LogFormat("#ObjectPool# Attempting to Re-Recycle cardBack {0}", cardBack.CardIdAndName);
                else
                    Debug.LogFormat("#ObjectPool# Attempting to Re-Recycle object");
            }
        }
        else
        {
            Debug.LogErrorFormat("#ObjectPool# DESTROYING {0}", type);
            Object.Destroy(obj.gameObject);
        }
	}

    public void OnApplicationQuit()
    {
        Debug.Log("Quitting Object pool");
        this.IsQuitting = true;
    }

    public static int Count<T>(T prefab) where T : PoolableMonoBehaviour
    {
		if (Instance != null && Instance.objectLookup.ContainsKey(prefab.PooledObjectType))
			return Instance.objectLookup[prefab.PooledObjectType].Count;
		else
			return 0;
	}
	
	public static ObjectPool Instance { get; set; }

    /*private static System.Type GetPooledObjectType(PooledObjectType typeEnum)
    {
        switch(typeEnum)
        {
            case PooledObjectType.CardFrontController:
                return typeof(CardFrontController);
            case PooledObjectType.CardBackController:
                return typeof(CardBackController);
            case PooledObjectType.FxTrigger:
                return typeof(FxTrigger);
            case PooledObjectType.FxTrailTrigger:
                return typeof(FxTrailTrigger);
            case PooledObjectType.CardView:
                return typeof(CardView);
            case PooledObjectType.CardListItemView:
                return typeof(CardListItemView);
            case PooledObjectType.DraggableCardView:
                return typeof(DraggableCardView);
            case PooledObjectType.DraggedCardView:
                return typeof(DraggedCardView);
            case PooledObjectType.CardTwoTouchView:
                return typeof(CardTwoTouchView);
            case PooledObjectType.FriendListItem:
                return typeof(FriendListItem);
            case PooledObjectType.UpdatedPrizeView:
                return typeof(UpdatedPrizeView);
            default:
                return null;
        }
    }*/
}

public enum PooledObjectType
{
    NoType,
    CardFrontController,
    CardBackController,
    FxTrigger,
    FxTrailTrigger,
    CardView,
    CardListItemView,
    CardListItemProxy,
    DraggableCardView,
    DraggedCardView,
    CardTwoTouchView,
    FriendListItem,
    UpdatedPrizeView,
    QuestView,
    AchievementView,
    LeaderboardItemView,
    DeckListItem,
    GlobalChatItem,
    AvatarView,
    CardBackView,
    StoreItem,
}

public class PoolableMonoBehaviour : MonoBehaviour
{
    // override to return the correct type for efficient pool lookup
    public virtual PooledObjectType PooledObjectType { get; private set; }

    public virtual void OnRecycle()
    {
        // override to reset data values when pooled
    }
}

public static class ObjectPoolExtensions
{
	public static void CreatePool<T>(this T prefab) where T : PoolableMonoBehaviour
    {
		ObjectPool.CreatePool(prefab);
	}

    /*public static T Spawn<T>(this T prefab, Vector3 position, Quaternion rotation) where T : PoolableMonoBehaviour
	{
		return ObjectPool.Spawn(prefab, position, rotation);
	}
	public static T Spawn<T>(this T prefab, Vector3 position) where T : PoolableMonoBehaviour
	{
		return ObjectPool.Spawn(prefab, position, Quaternion.identity);
	}*/
    public static T Spawn<T>(this T prefab) where T : PoolableMonoBehaviour
    {
		return ObjectPool.Spawn(prefab, Vector3.zero, Quaternion.identity);
	}
    public static void Recycle<T>(this T obj) where T : PoolableMonoBehaviour
    {
		ObjectPool.Recycle(obj);
	}
	
	public static int Count<T>(T prefab) where T : PoolableMonoBehaviour
    {
		return ObjectPool.Count(prefab);
	}
}

public interface IPoolView
{
	void Recycle();
}