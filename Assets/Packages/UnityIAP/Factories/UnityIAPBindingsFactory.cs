﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.api;
using strange.extensions.injector.api;
using strange.extensions.mediation.api;

public class UnityIAPBindingsFactory {

    public static void Create(ICommandBinder commandBinder, IInjectionBinder injectionBinder,
        IMediationBinder mediationBinder)
    {
        var iapSettings = Object.FindObjectOfType<UnityIAPSettings>();
        injectionBinder.Bind<UnityIAPSettings>().To(iapSettings).ToSingleton().CrossContext();

        mediationBinder.Bind<IAPStoreListenerView>().To<IAPStoreListenerMediator>();

        commandBinder.Bind<InitializePurchasingSignal>();
        commandBinder.Bind<UnityIapBuyProductSignal>();
        commandBinder.Bind<UnityIapRestorePurchaseSignal>();
        commandBinder.Bind<UnityIapPurchaseProcessSignal>();
        commandBinder.Bind<UnityIapCompletePurchaseSignal>();
        //commandBinder.Bind<UnityIapPurchaseProcessSignal>().To<UnityIapPurchaseProcessCommand>();

    }
}
