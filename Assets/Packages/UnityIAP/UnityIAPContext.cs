﻿using UnityEngine;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;

public class UnityIAPContext : MVCSContext
{
    public UnityIAPContextMediator mainCtx;
    public UnityIAPContext(MonoBehaviour ctxView, bool autoMap)
        : base(ctxView, autoMap)
    {
        mainCtx = (UnityIAPContextMediator)ctxView;
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
    }

    protected override void mapBindings()
    {

    }
}

