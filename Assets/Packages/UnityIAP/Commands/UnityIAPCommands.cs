﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

public class UnityIapPurchaseProcessSignal : Signal<UnityEngine.Purchasing.Product, bool, string> { } 
public class UnityIapBuyProductSignal : Signal<UnityIAPProduct> { }
public class UnityIapRestorePurchaseSignal : Signal { }
public class InitializePurchasingSignal : Signal<List<UnityIAPProduct>> { }
public class UnityIapCompletePurchaseSignal : Signal<bool,string> { }

public class UnityIapPurchaseProcessCommand : Command
{
    [Inject] public UnityEngine.Purchasing.Product Product { get; set; }

    public override void Execute()
    {
        Debug.LogFormat("Product Purchase Proccessed for: {0}", Product.definition.id);
        Debug.LogFormat("Product Type: {0}", Product.definition.type);
        Debug.LogFormat("Product Store Id: {0}", Product.definition.storeSpecificId);
        Debug.LogFormat("Product TransactionId: {0}", Product.transactionID);
        Debug.LogFormat("HasReceipt: {0}", Product.hasReceipt);
        if (Product.hasReceipt)
        {
            Debug.LogFormat("Reciept:{0}", Product.receipt);
        }

    }
}

