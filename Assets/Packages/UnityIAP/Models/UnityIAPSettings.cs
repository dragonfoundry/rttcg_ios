﻿using System;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.Purchasing;


[Serializable]
public class UnityIAPProduct
{
    public ProductType ProductType;
    public string ProductId;
    public string AppleStoreIdentifier;
    public string GoogleStoreIdentifier;
}

public class UnityIAPSettings : View
{
    public List<UnityIAPProduct> Products = new List<UnityIAPProduct>();
}
