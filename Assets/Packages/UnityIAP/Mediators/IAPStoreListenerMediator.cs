﻿using System;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine.Purchasing;
using UnityEngine;

public class IAPStoreListenerMediator : Mediator, IStoreListener  {
    [Inject] public UnityIAPSettings UnityIapSettings { get; set; }
    [Inject] public UnityIapPurchaseProcessSignal ProcessSignal { get; set; }
    [Inject] public UnityIapBuyProductSignal BuyProductListener { get; set; }
    [Inject] public UnityIapRestorePurchaseSignal RestorePurchaseSignal { get; set; }
    [Inject] public InitializePurchasingSignal InitPurchasingSignal { get; set; }


    private static IStoreController _StoreController;           // Reference to the Purchasing system.
    private static IExtensionProvider _StoreExtensionProvider;  // Reference to store-specific Purchasing subsystems.

    public override void OnRegister()
    {
        InitPurchasingSignal.AddOnce(OnInitializePurchasing);
        BuyProductListener.AddListener(OnBuyProductSignal);
        RestorePurchaseSignal.AddListener(OnRestorePurchaseSignal);
    }

    private void OnInitializePurchasing(List<UnityIAPProduct> products)
    {
        if (UnityIapSettings.Products == null)
        {
            UnityIapSettings.Products = new List<UnityIAPProduct>();
        }

        foreach (var product in products)
        {
            UnityIapSettings.Products.Add(product);
        }
        
        InitializePurchasing();
    }

    public override void OnRemove()
    {
        BuyProductListener.RemoveListener(OnBuyProductSignal);
        RestorePurchaseSignal.RemoveListener(OnRestorePurchaseSignal);
    }

    private void InitializePurchasing()
    {
        if (IsInitialized())
        {
            //Already Initialized.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        foreach (var product in UnityIapSettings.Products)
        {
            builder.AddProduct(product.ProductId, product.ProductType,
                new IDs()
            {
                {product.AppleStoreIdentifier, AppleAppStore.Name},
                {product.GoogleStoreIdentifier, GooglePlay.Name},
            });
        }

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and 
        //this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);

    }

    private void OnBuyProductSignal(UnityIAPProduct iapProduct)
    {
        try
        {
            if (IsInitialized())
            {
                Product product = _StoreController.products.WithID(iapProduct.ProductId);

                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
                    _StoreController.InitiatePurchase(product);
                }
                else
                {
                    throw new Exception("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                throw new Exception("Buy Product Failure. Not Initialized.");
            }
        }
        catch (Exception e)
        {
            Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
        }
    }

    private void OnRestorePurchaseSignal()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = _StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return _StoreController != null && _StoreExtensionProvider != null;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Overall Purchasing system, configured with products for this application.
        _StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        _StoreExtensionProvider = extensions;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing this reason with the user.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",product.definition.storeSpecificId, failureReason));
        ProcessSignal.Dispatch(product, false, failureReason.ToString());
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        //Dispatch to listeners.
        ProcessSignal.Dispatch(args.purchasedProduct, true, null);

        //TODO: I don't know how to handle the below. I think this is going to have to be something developers are responsible for, not the plugin.
        
        // Return a flag indicating wither this product has completely been received, or if the application needs 
        //to be reminded of this purchase at next app launch. Is useful when saving purchased products to the cloud, 
        //and when that save is delayed.
        return PurchaseProcessingResult.Complete;        
    }
} 
