﻿using strange.extensions.context.impl;

public class UnityIAPContextMediator : ContextView {

	void Awake () {
        context = new UnityIAPContext(this,true);
	    context.Start();
	}

}
