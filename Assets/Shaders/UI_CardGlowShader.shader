// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UI/CardGlowShader"
{
	Properties
	{
		_PanTex ("Pan Texture", 2D) = "white" {}
		_MainTex ("Alpha Mask", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		_SpeedX ("SpeedX", Float) = 0.05
		_SpeedY ("SpeedY", Float) = -0.1
		_Speed2X ("Speed2X", Float) = 0.05
		_Speed2Y ("Speed2Y", Float) = 0.1

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha One
		ColorMask [_ColorMask]

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				half2 texcoord2 : TEXCOORD2;
				half2 texcoord3 : TEXCOORD3;
			};
			
			fixed4 _Color;
			fixed4 _TextureSampleAdd;
			float4 _ClipRect;
			float _SpeedX;
			float _SpeedY;
			float _Speed2X;
			float _Speed2Y;
			float _TimeDelta;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.worldPosition = IN.vertex;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				OUT.texcoord2 = IN.texcoord;
				
				#ifdef UNITY_HALF_TEXEL_OFFSET
				OUT.vertex.xy += (_ScreenParams.zw-1.0)*float2(-1,1);
				#endif
				
				OUT.color = IN.color * _Color;

				float offset = 0;

				OUT.texcoord.xy =  IN.texcoord.xy + frac((_Time.y + offset) * float2(_SpeedX, _SpeedY));
				OUT.texcoord3 = IN.texcoord.xy + frac((_Time.y + offset) * float2(_Speed2X, _Speed2Y));
				
				

				return OUT;
			}

			sampler2D _PanTex;
			sampler2D _MainTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				half4 color = (tex2D(_PanTex, IN.texcoord));
				half4 color3 = (tex2D(_PanTex, IN.texcoord3));
				half4 color4 = color * color3;

				half4 color2 = (tex2D(_MainTex, IN.texcoord2 - color4.rg *0.025));
				
				color = _Color;

				half4 color5 = color2;
				color5.a -= (1.0f - color2.a)*2.0f;
				color5.a *= pow(color2.a, 1.2f);
				color.a = color5.a;
				color.rgb = (IN.color*_Color).rgb;
				color.a *= (IN.color*_Color).a;


				color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
}
