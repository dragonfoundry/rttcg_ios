﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class JenkinsBuildScript : MonoBehaviour {

    static string[] SCENES = FindEnabledEditorScenes();
    static string TARGET_DIR = "Builds";

    [MenuItem("Custom/CI/Build iOS")]
    static void PerformIOSBuild()
    {
        string target_dir = "xcode";
        GenericBuild(SCENES, TARGET_DIR + "/" + target_dir, BuildTarget.iOS, BuildTargetGroup.iOS, BuildOptions.None);
    }

    private static string[] FindEnabledEditorScenes()
    {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled) continue;
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildTargetGroup target_group, BuildOptions build_options)
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(target_group, build_target);
        Debug.Log(scenes);
        string res = BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
        if (res.Length > 0)
        {
            throw new Exception("BuildPlayer failure: " + res);
        }
    }

}
