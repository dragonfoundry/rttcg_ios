﻿#if UNITY_IOS

using System;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;

public class IosPostProcess: ScriptableObject
{
	public DefaultAsset m_entitlementsFile;

	[PostProcessBuild]
	public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject) 
	{
		if (buildTarget == BuildTarget.iOS) 
		{
			// PROJECT
			// =======
			string projectPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
			PBXProject project = new PBXProject();
			project.ReadFromString(File.ReadAllText(projectPath));

			// This is the project name that Unity generates for iOS, isn't editable until after post processing
			var unityTarget = PBXProject.GetUnityTargetName();
			string target = project.TargetGuidByName(unityTarget);

			//project.AddFrameworkToProject(target, "AdSupport.framework", false);
			//project.AddFrameworkToProject(target, "StoreKit.framework", false);
			//project.AddFrameworkToProject(target, "Security.framework", false);
			//project.AddFrameworkToProject(target, "SystemConfiguration.framework", false);
			//project.AddFrameworkToProject(target, "MessagesUI.framework", false);

			// Add all Frameworks required
			/*project.AddFrameworkToProject(target, "AudioToolbox.framework", true);
			project.AddFrameworkToProject(target, "AVFoundation.framework", true);
			project.AddFrameworkToProject(target, "CFNetwork.framework", true);
			project.AddFrameworkToProject(target, "CoreGraphics.framework", true);
			project.AddFrameworkToProject(target, "CoreLocation.framework", true);
			project.AddFrameworkToProject(target, "CoreMedia.framework", true);
			project.AddFrameworkToProject(target, "CoreMotion.framework", false);
			project.AddFrameworkToProject(target, "CoreVideo.framework", true);
			project.AddFrameworkToProject(target, "Foundation.framework", true);
			project.AddFrameworkToProject(target, "iAd.framework", false);
			project.AddFrameworkToProject(target, "MediaPlayer.framework", true);
			project.AddFrameworkToProject(target, "OpenAL.framework", true);
			project.AddFrameworkToProject(target, "OpenGLES.framework", true);
			project.AddFrameworkToProject(target, "QuartzCore.framework", true);


			project.AddFrameworkToProject(target, "CoreText.framework", true);
			project.AddFrameworkToProject(target, "Accounts.framework", true);
			project.AddFrameworkToProject(target, "CoreData.framework", true);
			project.AddFrameworkToProject(target, "MobileCoreServices.framework", true);
			project.AddFrameworkToProject(target, "GLKit.framework", true);
			project.AddFrameworkToProject(target, "libz.tbd", true);
			project.AddFrameworkToProject(target, "libsqlite3.0.tbd", true);*/


			// CAPABILITIES
			// ============
			project.AddCapability(target, PBXCapabilityType.GameCenter);
			project.AddCapability(target, PBXCapabilityType.InAppPurchase);
			project.AddCapability(target, PBXCapabilityType.PushNotifications);

			// LINKER FLAGS
			// ============
			project.SetBuildProperty (target, "FRAMEWORK_SEARCH_PATHS", "$(inherited)");
			project.AddBuildProperty (target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Frameworks");
			project.AddBuildProperty (target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Frameworks/FacebookSDK/Plugins/iOS");
			project.AddBuildProperty (target, "OTHER_LDFLAGS", "-ObjC");
			project.AddBuildProperty (target, "OTHER_LDFLAGS", "-fobjc-arc");

			// ENTITLEMENTS
			// ============
			string entitlementFileName = "novablitz.entitlements";
			string entitlementPath = Application.dataPath + "/Plugins/iOS/" + entitlementFileName;
			string entitlementDestination = unityTarget + "/" + entitlementFileName;
			FileUtil.ReplaceFile(entitlementPath, pathToBuiltProject + "/" + entitlementDestination);
			project.AddFile(entitlementDestination, entitlementFileName);
			project.AddBuildProperty(target, "CODE_SIGN_ENTITLEMENTS", entitlementDestination);

			// EXPORT PROJECT FILE
			// ===================
			File.WriteAllText(projectPath, project.WriteToString());

			// PLIST
			// =====
			string plistPath = pathToBuiltProject + "/Info.plist";
			PlistDocument plist = new PlistDocument();
			plist.ReadFromString(File.ReadAllText(plistPath));

			PlistElementDict rootDict = plist.root;

			// Change value of CFBundleVersion in Xcode plist
			var cameraKey = "NSCameraUsageDescription";
			rootDict.SetString(cameraKey,"You can use the camera to record video while streaming through replayKit.");
			var calendarKey = "NSCalendarsUsageDescription";
			rootDict.SetString(calendarKey,"You can save upcoming events to your calendar.");
			rootDict ["UIRequiredDeviceCapabilities"].AsArray ().AddString ("gamekit");

			// Write to file
			File.WriteAllText(plistPath, plist.WriteToString());
			Debug.Log ("IOS info.plist updated with 2 items");

			// XIB file
			//FileUtil.ReplaceFile(Application.dataPath + "/iOSsplashScreen/16x9banner.jpg", pathToBuiltProject + "/Unity-iPhone/Images.xcassets/16x9banner.jpg");
			FileUtil.ReplaceFile(Application.dataPath + "/iOSsplashScreen/16x9banner.jpg", pathToBuiltProject + "/16x9banner.jpg");
			FileUtil.ReplaceFile(Application.dataPath + "/iOSsplashScreen/nbview.xib", pathToBuiltProject + "/LaunchScreen-iPad.xib");
			FileUtil.ReplaceFile(Application.dataPath + "/iOSsplashScreen/nbview.xib", pathToBuiltProject + "/LaunchScreen-iPhone.xib");

			Debug.LogError ("IOS Postbuild completed successfully");
		}
	}
}
#endif