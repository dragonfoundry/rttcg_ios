#!/usr/bin/python

import sys
import os

from mod_pbxproj import XcodeProject

# List of error code constants (although nothing is really constant in Python!)
# These match the EX_xxx codes in the 'os' module but to ensure they are available on all platforms we define our own here
CHILLINGO_EXIT_CODE_OK = 0; # See os.EX_OK
CHILLINGO_EXIT_CODE_NOINPUT = 66;  # See os.EX_NOINPUT

def executeCommand(cmd):
	print "Executing OS Command: ", cmd
	os.system(cmd)
	return

def addFrameworks(project):
	print "Adding frameworks to the project"
	project.add_file_if_doesnt_exist('System/Library/Frameworks/Accounts.framework', tree='SDKROOT', weak=True)
	# For some reason the AdSupport.framework doesn't like being added to the project via the add_file_if_doesnt_exist mechanism 
	# so this will be added to the 'other linker flags' instead.
	#project.add_file_if_doesnt_exist('System/Library/Frameworks/AdSupport.framework', tree='SDKROOT', weak=True)
	project.add_file_if_doesnt_exist('System/Library/Frameworks/AVFoundation.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/CFNetwork.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/CoreData.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/CoreGraphics.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/CoreText.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/MobileCoreServices.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/OpenGLES.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/QuartzCore.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/Security.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/Social.framework', tree='SDKROOT', weak=True)
    project.add_file_if_doesnt_exist('System/Library/Frameworks/StoreKit.framework', tree='SDKROOT')
        project.add_file_if_doesnt_exist('System/Library/Frameworks/GameKit.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/SystemConfiguration.framework', tree='SDKROOT')
	project.add_file_if_doesnt_exist('System/Library/Frameworks/Twitter.framework', tree='SDKROOT', weak=True)
	return


def addDynamicLibraries(project):
	print "Adding dynamic libraries to the project"
    #project.add_file_if_doesnt_exist('usr/lib/libsqlite3.dylib', tree='SDKROOT')
	return


def addLinkerFlags(project):
	print "Adding linker flags to project"
    #project.add_other_ldflags('-ObjC')
    #project.add_other_ldflags('-all_load')
    #project.add_other_ldflags('-loffers')
    #project.add_other_ldflags('-lz')

	# Add any problematic frameworks 
	project.add_other_ldflags('-weak_framework AdSupport')
	return


def backupAndSave(project):
	# Backup and save if necessary
	if project.modified:
		print "Backing up and saving project"
		project.backup()
		project.saveFormat3_2()
	return

def buildiOSProject(project, xcodeprojDirPath, assetsDir):
	addFrameworks(project)
	addDynamicLibraries(project)
	addLinkerFlags(project)
	backupAndSave(project)
	return CHILLINGO_EXIT_CODE_OK


def postProcessAndroid():
	print "Processing Android"
	androidSdkPath = sys.argv[3]
	return CHILLINGO_EXIT_CODE_OK

def postProcessIOS():
	print "Post processing iOS project"
	xcodeprojDirPath = sys.argv[1]
	xcodeprojFilePath = xcodeprojDirPath + '/Unity-iPhone.xcodeproj'
	project = XcodeProject.Load(xcodeprojFilePath + '/project.pbxproj')

	if not project:
		print "Failed to load project file"
		return CHILLINGO_EXIT_CODE_NOINPUT

	assetsDir = sys.argv[0].rpartition("Editor/PostProcessBuildPlayer_Offers")[0]
	return buildiOSProject(project, xcodeprojDirPath, assetsDir)


def main():
	print "Running PostProcessBuildPlayer script with the following arguments:" 
	# Arguments are as follows;
	# 0: Full path to this file
	# 1: Full path to deployed project (iOS -> folder containing xcodeproj, Android -> path to APK)
	# 2: Platform name (android or iPhone)
	# 3: Path to Android SDK (if set in Unity)
	# 4: Keystore name (path)
	# 5: Keystore password
	# 6: Key alias name
	# 7: Key alias password
		
	i = 0
	for args in sys.argv:
	    print str(i) +": " + args
	    i += 1
	     
	result = CHILLINGO_EXIT_CODE_OK
	if sys.argv[2] == "iPhone":
		result = postProcessIOS()

	if sys.argv[2] == "android":
		result = postProcessAndroid()

	return result 

if __name__ == '__main__':
    exitCode = main()
    sys.exit(exitCode)


