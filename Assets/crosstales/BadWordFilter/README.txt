# Bad Word Filter PRO 2.6.3

Thank you for buying our asset "Bad Word Filter PRO"! 
If you have any questions about this asset, send us an email at [assets@crosstales.com](mailto:assets@crosstales.com). 
Please don't forget to rate it or even better write a little review – it would be very much appreciated.



## Description
The "Bad Word Filter" (aka profanity or obscenity filter) is exactly what the title suggests: a tool to filter swearwords and other "bad sentences".

There are multiple uses for the "Bad Word Filter" in your projects, but the three most obvious would be user names (e.g. for high-scores), in a chat within the game and character names. If you don't want some wannabe-funny-guy to use the user name “a55-face”, “S+alin” or any other word you don't approve of, just enable the "Bad Word Filter" and instead of the swearword something like this comes out: #$@&%*!

In our library included are the following 23 languages (bad words as regular expressions which matches ten thousands of word variations): 
Arabic, Chinese, Czech, Danish, Dutch, English, Finnish, French, German, Hindi, Hungarian, Italian, Japanese, Korean, Norwegian, Persian, Polish, Portuguese, Russian, Spanish, Swedish, Thai and Turkish. 
Furthermore, you can add (or remove) any word and language you'd like!

We also included the following special filters: domains (URLs/emails), reserved names (from games, movies, comics etc.), global bad words, emojis (miscellaneous symbols), excessive capitalization and punctuation.

The "Bad Word Filter" works with any language and writing system. It is easily customizable, runs on all Unity platforms and the source code (including all bad words provided) is also contained within the package.

Please read the "BadWordFilter-doc.pdf" or "BadWordFilter-api.pdf" for more details.



## Upgrade to new version
Follow this steps to upgrade your version of "Bad Word Filter PRO":

1. Update "Bad Word Filter PRO" to the latest version from the "Asset Store" => https://www.assetstore.unity3d.com/en/#!/content/26255
2. Inside your project in Unity, go to menu "File" => "New Scene"
3. Delete the "crosstales\BadWordFilter" folder from the Project-view
4. Import the latest version from the "Asset Store"



## Release notes

### 2.6.3
* Managers improved
* Editor integration improved
* Domains updated

### 2.6.2
* Test-Drive added to the custom editors and configuration window
* Menu improved
* Rename of "MultiManager" to "BWFManager"
* Ensure object name "BWF"
* Domains updated
* Minor code improvements
* Documentation improved

### 2.6.1
* Configuration window and "Unity Preferences" added

### 2.6.0
* Automatically adds the neccessary RTVoice-prefabs to the current scene
* Update-checker added
* Playmaker actions improved
* Demo for 'Adventure Creator' added (see folder '3rd party')
* en, fr, ja, de and domains updated
* Code clean-up
* Minimal Unity version is now 5.1.1

### 2.5.2
* Check for Internet connection added
* Domains updated
* Code clean-up

### 2.5.1
* Playmaker actions updated
* Domains updated

### 2.5.0
* Playmaker actions added
* Domains updated

### 2.4.1
* Demo scene improved
* Proxy manager added
* Internet availability check implemented
* Domains updated

### 2.4.0
* Filter for Hindi, Persian and Emoji (miscellaneous symbols) added
* Arabic, Czech, Danish, French, Swedish, domains, global and names updated
* Minor code improvements
* Documentation updated

### 2.3.0
* Swedish updated
* Domains updated
* Code clean-up
* API documentation added

### 2.2.1
* Warnings on unsupported platforms removed
* Domains updated

### 2.2.0
* PRO edition created
* Filters are now always ready after a scene starts

### 2.1.0
* Better Editor integration
* Domains updated

### 2.0.8
* Minor changes

### 2.0.7
* Fix for the Mac-platform (thx to Alan I'Anson!)

### 2.0.6
* Debug-mode for bad words and domains (enable/disable it via 'Constants.cs')

### 2.0.5
* Minor code improvements

### 2.0.3
* Customer request: version for U4.6
* Version for U4.6 doesn't contain the demo scene (see 'Test' instead)!

### 2.0.2
* Documentation updated
* Minor improved detection in English

### 2.0.1
* Code clean-up
* Improved detection in English

### 2.0.0
* Developed for Unity5
* Filters are decoupled from MonoBehaviour
* All filters are now changeable during run-time
* Separate usable and configurable filters/managers
* The data is now injected via providers with 1-n sources. We implemented three different providers.
* Meta data for sources, like a description and icon
* New functions "Mark", "Unmark" and "Replace"
* New Unity UI-based demo scene
* Improved performance (5-10%)
* More and better bad word matching (more than 100 changes)

### 1.5.0
* Thai added

### 1.5.0
* Thai added

### 1.4.2
* Minor detection improvements in English and German

### 1.4.0
* Turkish added
* Refined word matching
* Domain check implemented
* Check for excessive capitalization and punctuation added

### 1.3.0
* Czech, Hungarian, Polish and Portuguese added
* Fuzzy mode improved and re-added
* Support for regular expressions

### 1.2.0
* Danish, Finnish, Norwegian and Swedish added
* More than 630 additional words for the existing languages added
* Speed improvements
* Demo scenes improved
* GetFirst() and Fuzzy removed

### 1.1.0
* Arabic, Chinese, Japanese, Korean, Russian and Dutch added
* More than 800 additional words for the existing languages added
* Code improvements

### 1.0.0
* Production release



## Credits

"Bad Word Filter" contains some words from the following sources:

* [Wikipedia](http://en.wikipedia.org/wiki/Category:Profanity_by_language)
* [Shutterstock](https://github.com/shutterstock/List-of-Dirty-Naughty-Obscene-and-Otherwise-Bad-Words)

Some icons are from [OpenIconLibrary](http://sourceforge.net/projects/openiconlibrary/)


## Contact

crosstales LLC
Weberstrasse 21
CH-8004 Zürich

[Homepage](http://www.crosstales.com/en/assets/badwordfilter/)
[Email](mailto:assets@crosstales.com)
[AssetStore](https://www.assetstore.unity3d.com/en/#!/content/26255)
[Forum](http://forum.unity3d.com/threads/bad-word-filter-aka-profanity-or-obscenity-filter.289960/)
[Documentation](http://www.crosstales.com/en/assets/badwordfilter/BadWordFilter-doc.pdf)
[API](http://www.crosstales.com/en/assets/badwordfilter/api)
[WebGL](http://www.crosstales.com/en/assets/badwordfilter/run-webgl.html)

`Version: 25.07.2016 14:27`