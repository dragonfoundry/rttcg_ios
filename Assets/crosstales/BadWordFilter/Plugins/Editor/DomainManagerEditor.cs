﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace Crosstales.BadWord.EditorExt {
   /// <summary>Custom editor for the 'DomainManager'-class.</summary>
   [CustomEditor(typeof(DomainManager))]
   public class DomainManagerEditor : Editor {

#region Variables

      private string inputText = "Write me an email bwfuser@mypage.com!";
      private string outputText;

#endregion

#region Editor methods

      void OnEnable() {
         DomainManager.Load();
      }

      public override void OnInspectorGUI() {
         DrawDefaultInspector();

         EditorHelper.SeparatorUI();
         
         if (DomainManager.isReady) {
            GUILayout.Label("Test-Drive", EditorStyles.boldLabel);

            if (Helper.isEditorMode) {
               inputText = EditorGUILayout.TextField(new GUIContent("Input Text", "Text to check."), inputText);

               EditorHelper.ReadOnlyTextField("Output Text", outputText);

               GUILayout.Space(8);

               GUILayout.BeginHorizontal();
               if (GUILayout.Button(new GUIContent("Contains", "Contains any domains?"))) {
                  DomainManager.Load();
                  outputText = DomainManager.Contains(inputText).ToString();
               }

               if (GUILayout.Button(new GUIContent("Get", "Get all domains."))) {
                  DomainManager.Load();
                  outputText = string.Join(", ", DomainManager.GetAll(inputText).ToArray());
               }

               if (GUILayout.Button(new GUIContent("Replace", "Check and replace all domains."))) {
                  DomainManager.Load();
                  outputText = DomainManager.ReplaceAll(inputText);
               }

               if (GUILayout.Button(new GUIContent("Mark", "Mark all domains."))) {
                  DomainManager.Load();
                  outputText = DomainManager.Mark(inputText, BWFManager.GetAll(inputText));
               }
               GUILayout.EndHorizontal();
            } else {
               GUILayout.Label("Disabled in Play-mode!");
            }
         }
      }

      public override bool RequiresConstantRepaint() {
         return true;
      }

#endregion

   }
}
// Copyright 2016 www.crosstales.com