﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace Crosstales.BadWord.EditorExt {
   /// <summary>Custom editor for the 'CapitalizationManager'-class.</summary>
   [CustomEditor(typeof(CapitalizationManager))]
   public class CapitalizationManagerEditor : Editor {

#region Variables

      private string inputText = "COME ON, TEST ME User!";
      private string outputText;

#endregion

#region Editor methods

      public override void OnInspectorGUI() {
         DrawDefaultInspector();

         EditorHelper.SeparatorUI();
         
         if (CapitalizationManager.isReady) {
            GUILayout.Label("Test-Drive", EditorStyles.boldLabel);

            if (Helper.isEditorMode) {
               inputText = EditorGUILayout.TextField(new GUIContent("Input Text", "Text to check."), inputText);

               EditorHelper.ReadOnlyTextField("Output Text", outputText);

               GUILayout.Space(8);

               GUILayout.BeginHorizontal();
               if (GUILayout.Button(new GUIContent("Contains", "Contains any extensive capitalizations?"))) {
                  CapitalizationManager.Load();
                  outputText = CapitalizationManager.Contains(inputText).ToString();
               }

               if (GUILayout.Button(new GUIContent("Get", "Get all extensive capitalizations."))) {
                  CapitalizationManager.Load();
                  outputText = string.Join(", ", CapitalizationManager.GetAll(inputText).ToArray());
               }

               if (GUILayout.Button(new GUIContent("Replace", "Check and replace all extensive capitalizations."))) {
                  CapitalizationManager.Load();
                  outputText = CapitalizationManager.ReplaceAll(inputText);
               }

               if (GUILayout.Button(new GUIContent("Mark", "Mark all extensive capitalizations."))) {
                  CapitalizationManager.Load();
                  outputText = CapitalizationManager.Mark(inputText, BWFManager.GetAll(inputText));
               }
               GUILayout.EndHorizontal();
            } else {
               GUILayout.Label("Disabled in Play-mode!");
            }
         }
      }

      public override bool RequiresConstantRepaint() {
         return true;
      }

#endregion

   }
}
// Copyright 2016 www.crosstales.com