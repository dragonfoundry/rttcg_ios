﻿using System.Collections.Generic;
using System;
using System.Text;

namespace Crosstales.BadWord {
   /// <summary>Model for a source of domains.</summary>
   [Serializable]
   public class Domains {
      
#region Variables
      /// <summary>Source-object.</summary>
      public Source Source;

      /// <summary>List of all domains (RegEx).</summary>
      public List<string> DomainList;

#endregion

#region Constructor

      public Domains(Source source, List<string> domainList) {
         Source = source;
         DomainList = domainList;
      }

#endregion

#region Overridden methods

      public override string ToString() {
         StringBuilder result = new StringBuilder();

         result.Append(GetType().Name);
         result.Append(Constants.TEXT_TOSTRING_START);

         result.Append("Source='");
         result.Append(Source);
         result.Append(Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("DomainList='");
         result.Append(DomainList);
         result.Append(Constants.TEXT_TOSTRING_DELIMITER_END);

         result.Append(Constants.TEXT_TOSTRING_END);

         return result.ToString();   
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com