﻿using UnityEngine;
using System;
using System.Text;

namespace Crosstales.BadWord {
   /// <summary>Base class for sources.</summary>
   [Serializable]
   public class Source {
      
#region Variables

      [Header("Information")]
      /// <summary>Name of the source.</summary>
      [Tooltip("Name of the source.")]
      public string Name = string.Empty;

      /// <summary>Description for the source (optional).</summary>
      [Tooltip("Description for the source (optional).")]
      public string Description = string.Empty;

      /// <summary>Icon to represent the source (e.g. country flag, optional)</summary>
      [Tooltip("Icon to represent the source (e.g. country flag, optional)")]
      public Sprite Icon;

      [Header("Settings")]
      /// <summary>URL of a text file containing all regular expressions for this source. Add also the protocol-type ('http://', 'file://' etc.).</summary>
      [Tooltip("URL of a text file containing all regular expressions for this source. Add also the protocol-type ('http://', 'file://' etc.).")]
      public string URL = string.Empty;

      /// <summary>Text file containing all regular expressions for this source.</summary>
      [Tooltip("Text file containing all regular expressions for this source.")]
      public TextAsset Resource;

#endregion

#region Overridden methods

      public override string ToString() {
         StringBuilder result = new StringBuilder();

         result.Append(GetType().Name);
         result.Append(Constants.TEXT_TOSTRING_START);

         result.Append("Name='");
         result.Append(Name);
         result.Append(Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Description='");
         result.Append(Description);
         result.Append(Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Icon='");
         result.Append(Icon);
         result.Append(Constants.TEXT_TOSTRING_DELIMITER_END);

         result.Append(Constants.TEXT_TOSTRING_END);

         return result.ToString();   
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com