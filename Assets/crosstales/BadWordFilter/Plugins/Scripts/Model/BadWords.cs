﻿using System.Collections.Generic;
using System;
using System.Text;

namespace Crosstales.BadWord {
   /// <summary>Model for a source of bad words.</summary>
   [Serializable]
   public class BadWords {
      
#region Variables
      /// <summary>Source-object.</summary>
      public Source Source;

      /// <summary>List of all bad words (RegEx).</summary>
      public List<string> BadWordList;

#endregion
      
#region Constructor

      public BadWords(Source source, List<string> badWordList) {
         Source = source;
         BadWordList = badWordList;
      }

#endregion

#region Overridden methods

      public override string ToString() {
         StringBuilder result = new StringBuilder();

         result.Append(GetType().Name);
         result.Append(Constants.TEXT_TOSTRING_START);

         result.Append("Source='");
         result.Append(Source);
         result.Append(Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("BadWordList='");
         result.Append(BadWordList);
         result.Append(Constants.TEXT_TOSTRING_DELIMITER_END);

         result.Append(Constants.TEXT_TOSTRING_END);

         return result.ToString();   
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com