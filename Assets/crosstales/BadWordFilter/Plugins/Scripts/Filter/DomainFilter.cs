using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LinqTools;

namespace Crosstales.BadWord {
   /// <summary>Filter for domains. The class can also replace all domains inside a string.</summary>
   public class DomainFilter : Filter {
      
#region Variables
      /// <summary>Replace characters for domains.</summary>
      public string ReplaceCharacters;

      private List<DomainProvider> domainProvider = new List<DomainProvider>();
            
      private readonly List<DomainProvider> tempDomainProvider;
      private readonly Dictionary<string, Regex> domainsRegex = new Dictionary<string, Regex>();
      private readonly Dictionary<string, List<Regex>> debugDomainsRegex = new Dictionary<string, List<Regex>>();
      private bool ready = false;
      private bool readyFirstime = false;

#endregion

#region Properties

      /// <summary>List of all domain providers.</summary>
      /// <returns>All domain providers.</returns>
      public List<DomainProvider> DomainProvider {
         get{ return domainProvider;}
         set {
            domainProvider = value;
            if (domainProvider != null && domainProvider.Count > 0) {
               foreach (DomainProvider dp in domainProvider) {
                  if (dp != null) {
                     if (Constants.DEBUG_DOMAINS) {
                        debugDomainsRegex.CTAddRange(dp.DebugDomainsRegex);
                     } else {
                        domainsRegex.CTAddRange(dp.DomainsRegex);
                     }
                  } else {
                     if (!Helper.isEditorMode) {
                        Debug.LogError("DomainProvider is null!");
                     }
                  }
               }
            } else {
               Debug.LogWarning("No 'DomainProvider' added!" + Environment.NewLine + "If you want to use this functionality, please add your desired 'DomainProvider' in the editor or script.");
            }
         }
      }

      public override bool isReady {
         get {
            bool result = true;
            
			if (!ready) {
               if (tempDomainProvider != null) {
                  foreach (DomainProvider dp in tempDomainProvider) {
                     if (dp != null && !dp.Ready) {
                        result = false;
                        break;
                     }
                  }
               }

               if (!readyFirstime && result) {
                  DomainProvider = tempDomainProvider;

                  if (DomainProvider != null) {
                     foreach (DomainProvider dp in DomainProvider) {
                        if (dp != null) {
                           foreach (Source src in dp.Sources) {
                              if (!sources.ContainsKey(src.Name)) {
                                 sources.Add(src.Name, src);
                              }
                           }
                        }
                     }
                  }
                  
                  readyFirstime = true;
               }
            }
            
            ready = result;
            
            return result;
         }
      }

#endregion

#region Constructor

      public DomainFilter(List<DomainProvider> domainProvider, string replaceCharacters, string markPrefix, string markPostfix) {
         tempDomainProvider = domainProvider;
         ReplaceCharacters = replaceCharacters;

         MarkPrefix = markPrefix;
         MarkPostfix = markPostfix;
      }

#endregion

#region Implemented methods

      public override bool Contains(string testString, params string[] sources) {
         bool result = false;

         if(isReady) {
            if (string.IsNullOrEmpty(testString)) {
               logContains();
            } else {
               
#region DEBUG

               if (Constants.DEBUG_DOMAINS) {
                  if (sources == null || sources.Length == 0) {
                     foreach (List<Regex> domainRegexes in debugDomainsRegex.Values) {
                        foreach (Regex domainRegex in domainRegexes) {
                           Match match = domainRegex.Match(testString);
                           if (match.Success) {
                              Debug.Log("Test string contains a domain: '" + match.Value + "' detected by regex '" + domainRegex + "'");
                              result = true;
                              break;
                           }
                        }
                     }
                  } else {
                     List<Regex> domainRegexes;
                     
                     foreach (string domainResource in sources) {
                        if (debugDomainsRegex.TryGetValue(domainResource, out domainRegexes)) {
                           foreach (Regex domainRegex in domainRegexes) {
                              Match match = domainRegex.Match(testString);
                              if (match.Success) {
                                 Debug.Log("Test string contains a domain: '" + match.Value + "' detected by regex '" + domainRegex + "'" + "' from source '" + domainResource + "'");
                                 result = true;
                                 break;
                              }
                           }
                        } else {
                           logResourceNotFound(domainResource);
                        }
                     }
                  }

#endregion

               } else {
                  if (sources == null || sources.Length == 0) {
                     foreach (Regex domainRegex in domainsRegex.Values) {
                        if (domainRegex.Match(testString).Success) {
                           result = true;
                           break;
                        }
                     }
                  } else {
                     Regex domainRegex;

                     foreach (string domainResource in sources) {
                        if (domainsRegex.TryGetValue(domainResource, out domainRegex)) {
                           Match match = domainRegex.Match(testString);
                           if (match.Success) {
                              result = true;
                              break;
                           }
                        } else {
                           logResourceNotFound(domainResource);
                        }
                     }
                  }
               }
            }
         } else {
            logFilterNotReady();
         }

         return result;
      }

      public override List<string> GetAll(string testString, params string[] sources) {
         List<string> result = new List<string>();

         if(isReady) {
            if (string.IsNullOrEmpty(testString)) {
               logGetAll();
            } else {
               
#region DEBUG

               if (Constants.DEBUG_DOMAINS) {
                  if (sources == null || sources.Length == 0) {
                     foreach (List<Regex> domainRegexes in debugDomainsRegex.Values) {
                        foreach (Regex domainRegex in domainRegexes) {
                           MatchCollection matches = domainRegex.Matches(testString);
                           
                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 Debug.Log("Test string contains a domain: '" + capture.Value + "' detected by regex '" + domainRegex + "'");
                                 
                                 if (!result.Contains(capture.Value)) {
                                    result.Add(capture.Value);
                                 }
                              }
                           }
                        }
                     }
                  } else {
                     List<Regex> domainRegexes;
                     
                     foreach (string domainResource in sources) {
                        if (debugDomainsRegex.TryGetValue(domainResource, out domainRegexes)) {
                           foreach (Regex domainRegex in domainRegexes) {
                              MatchCollection matches = domainRegex.Matches(testString);
                              
                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    Debug.Log("Test string contains a domain: '" + capture.Value + "' detected by regex '" + domainRegex + "'" + "' from source '" + domainResource + "'");

                                    if (!result.Contains(capture.Value)) {
                                       result.Add(capture.Value);
                                    }
                                 }
                              }
                           }
                        } else {
                           logResourceNotFound(domainResource);
                        }
                     }
                  }

#endregion

               } else {
                  if (sources == null || sources.Length == 0) {
                     foreach (Regex domainRegex in domainsRegex.Values) {
                        MatchCollection matches = domainRegex.Matches(testString);
                        
                        foreach (Match match in matches) {
                           foreach (Capture capture in match.Captures) {
                              if (!result.Contains(capture.Value)) {
                                 result.Add(capture.Value);
                              }
                           }
                        }
                     }
                  } else {
                     Regex domainRegex;
                     
                     foreach (string domainResource in sources) {
                        if (domainsRegex.TryGetValue(domainResource, out domainRegex)) {
                           MatchCollection matches = domainRegex.Matches(testString);
                           
                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 if (!result.Contains(capture.Value)) {
                                    result.Add(capture.Value);
                                 }
                              }
                           }

                        } else {
                           logResourceNotFound(domainResource);
                        }
                     }
                  }
               }
            }
         } else {
            logFilterNotReady();
         }

         return result.Distinct().OrderBy(x => x).ToList();
      }

      public override string ReplaceAll(string testString, params string[] sources) {
         string result = testString;

         if(isReady) {
            if (string.IsNullOrEmpty(testString)) {
               logReplaceAll();

               result = string.Empty;
            } else {
               
#region DEBUG

               if (Constants.DEBUG_DOMAINS) {
                  if (sources == null || sources.Length == 0) {
                     foreach (List<Regex> domainRegexes in debugDomainsRegex.Values) {
                        foreach (Regex domainRegex in domainRegexes) {
                           MatchCollection matches = domainRegex.Matches(testString);
                           
                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 Debug.Log("Test string contains a domain: '" + capture.Value + "' detected by regex '" + domainRegex + "'");

                                 result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                              }
                           }
                        }
                     }
                  } else {
                     List<Regex> domainRegexes;
                     
                     foreach (string domainResource in sources) {
                        if (debugDomainsRegex.TryGetValue(domainResource, out domainRegexes)) {
                           foreach (Regex domainRegex in domainRegexes) {
                              MatchCollection matches = domainRegex.Matches(testString);
                              
                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    Debug.Log("Test string contains a domain: '" + capture.Value + "' detected by regex '" + domainRegex + "'" + "' from source '" + domainResource + "'");

                                    result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                                 }
                              }
                           }
                        } else {
                           logResourceNotFound(domainResource);
                        }
                     }
                  }

#endregion

               } else {
                  if (sources == null || sources.Length == 0) {
                     foreach (Regex domainRegex in domainsRegex.Values) {
                        MatchCollection matches = domainRegex.Matches(testString);
                        
                        foreach (Match match in matches) {
                           foreach (Capture capture in match.Captures) {
                              result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                           }
                        }
                     }
                  } else {
                     Regex domainRegex;
                     
                     foreach (string domainResource in sources) {
                        if (domainsRegex.TryGetValue(domainResource, out domainRegex)) {
                           MatchCollection matches = domainRegex.Matches(testString);
                           
                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                              }
                           }
                        } else {
                           logResourceNotFound(domainResource);
                        }
                     }
                  }
               }
            }
         } else {
            logFilterNotReady();
         }

         return result;
      }

      public override string Replace(string text, List<string> domains) {
         string result = text;
         
         if (string.IsNullOrEmpty(text)) {
            logReplace();
            
            result = string.Empty;
         } else {
            if (domains == null || domains.Count == 0) {
               Debug.LogWarning("Parameter 'badWords' is null or empty!" + Environment.NewLine + "=> 'Replace()' will return the original string.");
            } else {
               foreach(string badword in domains) {
                  result = result.Replace(badword, Helper.CreateReplaceString(ReplaceCharacters, badword.Length));
               }
            }
         }
         
         return result;
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com