using UnityEngine;
using System.Collections.Generic;
using LinqTools;

namespace Crosstales.BadWord {
   /// <summary>Base class for all filters.</summary>
   public abstract class Filter {
      
#region Variables

      /// <summary>Prefix for every found bad word.</summary>
      public string MarkPrefix = "<color=red>";

      /// <summary>Postfix for every found bad word.</summary>
      public string MarkPostfix = "</color>";

      protected Dictionary<string, Source> sources = new Dictionary<string, Source>();

#endregion

#region Properties

      /// <summary>All sources of the current filter.</summary>
      /// <returns>List with all sources for the current filter</returns>
      public virtual List<Source> Sources {
         get {
            List<Source> result = new List<Source>();

            if (isReady) {
               result = sources.OrderBy(x => x.Key).Select(y => y.Value).ToList();
            } else {
               logFilterNotReady();
            }

            return result;
         }
      }

#endregion

#region Abstract methods

      /// <summary>Checks the readiness status of the current filter.</summary>
      /// <returns>True if the filter is ready.</returns>
      public abstract bool isReady {
         get;
      }

      /// <summary>Searches for bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "en")</param>
      /// <returns>True if a match was found</returns>
      public abstract bool Contains(string testString, params string[] sources);

      /// <summary>Searches for bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "en")</param>
      /// <returns>List with all the matches</returns>
      public abstract List<string> GetAll(string testString, params string[] sources);

      /// <summary>Searches and replaces all bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "en")</param>
      /// <returns>Clean text</returns>
      public abstract string ReplaceAll(string testString, params string[] sources);

      /// <summary>
      /// Replaces all bad words in a text.
      /// Use this method if you already have a list of bad words (e.g. from the 'GetAll()' method).
      /// </summary>
      /// <param name="text">Text containig bad words</param>
      /// <param name="badWords">Bad words to replace</param>
      /// <returns>Clean text</returns>
      public abstract string Replace(string text, List<string> badWords);

#endregion

#region Public methods

      /// <summary>Marks the text with a prefix and postfix from a list of words.</summary>
      /// <param name="text">Text containig bad words</param>
      /// <param name="badWords">Bad words to mark</param>
      /// <param name="prefix">Prefix for every found bad word (optional)</param>
      /// <param name="postfix">Postfix for every found bad word (optional)</param>
      /// <returns>Text with marked bad words</returns>
      public virtual string Mark(string text, List<string> badWords, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;

         string _prefix = prefix;
         string _postfix = postfix;
         
         if (string.IsNullOrEmpty(text)) {
            //Debug.LogWarning("Parameter 'text' is null or empty!" + Environment.NewLine + "=> 'Mark()' will return an empty string.");
            
            result = string.Empty;
         } else {
            if (string.IsNullOrEmpty(prefix)) {
               //Debug.LogWarning("Parameter 'prefix' is null!" + Environment.NewLine + "=> Using an empty string as prefix.");
               _prefix = MarkPrefix ?? string.Empty;
            }

            if (string.IsNullOrEmpty(postfix)) {
               //Debug.LogWarning("Parameter 'postfix' is null!" + Environment.NewLine + "=> Using an empty string as postfix.");
               _postfix = MarkPostfix ?? string.Empty;
            }

            if (badWords == null || badWords.Count == 0) {
               //Debug.LogWarning("Parameter 'badWords' is null or empty!" + Environment.NewLine + "=> 'Mark()' will return the original string.");
            } else {
               foreach (string badword in badWords) {
                  result = result.Replace(badword, _prefix + badword + _postfix);
               }
            }
         }
         
         return result;
      }

      /// <summary>Unmarks the text with a prefix and postfix.</summary>
      /// <param name="text">Text with marked bad words</param>
      /// <param name="prefix">Prefix for every found bad word (optional)</param>
      /// <param name="postfix">Postfix for every found bad word (optional)</param>
      /// <returns>Text with marked bad words</returns>
      public virtual string Unmark(string text, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;

         string _prefix = prefix;
         string _postfix = postfix;

         if (string.IsNullOrEmpty(text)) {
            //Debug.LogWarning("Parameter 'text' is null or empty!" + Environment.NewLine + "=> 'Unmark()' will return an empty string.");

            result = string.Empty;
         } else {
            if (string.IsNullOrEmpty(prefix)) {
               //Debug.LogWarning("Parameter 'prefix' is null!" + Environment.NewLine + "=> Using an empty string as prefix.");
               _prefix = MarkPrefix ?? string.Empty;
            }

            if (string.IsNullOrEmpty(postfix)) {
               //Debug.LogWarning("Parameter 'postfix' is null!" + Environment.NewLine + "=> Using an empty string as postfix.");
               _postfix = MarkPostfix ?? string.Empty;
            }

            result = result.Replace(_prefix, string.Empty);
            result = result.Replace(_postfix, string.Empty);
         }

         return result;
      }

#endregion

#region Protected methods

      protected void logFilterNotReady() {
         Debug.LogWarning("Filter is not ready - please wait until 'isReady' returns true.");
      }

      protected void logResourceNotFound(string res) {
         //Debug.LogWarning("Resource not found: '" + res + "'" + Environment.NewLine + "Did you call the method with the correct resource name?");
      }

      protected void logContains() {
         //Debug.LogWarning("Parameter 'testString' is null or empty!" + Environment.NewLine + "=> 'Contains()' will return 'false'.");
      }

      protected void logGetAll() {
         //Debug.LogWarning("Parameter 'testString' is null or empty!" + Environment.NewLine + "=> 'GetAll()' will return an empty list.");
      }

      protected void logReplaceAll() {
         //Debug.LogWarning("Parameter 'testString' is null or empty!" + Environment.NewLine + "=> 'ReplaceAll()' will return an empty string.");
      }

      protected void logReplace() {
         //Debug.LogWarning("Parameter 'text' is null or empty!" + Environment.NewLine + "=> 'Replace()' will return an empty string.");
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com