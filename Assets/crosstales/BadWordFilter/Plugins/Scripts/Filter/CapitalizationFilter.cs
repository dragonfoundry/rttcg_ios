using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LinqTools;

namespace Crosstales.BadWord {
   /// <summary>Filter for excessive capitalization. The class can also replace all capitalizations inside a string.</summary>
   public class CapitalizationFilter : Filter {
      
#region Variables

      private int characterNumber;

#endregion

#region Properties

      /// <summary>RegEx to find excessive capitalization.</summary>
      public Regex RegularExpression { get; private set; }

      /// <summary>Defines the number of allowed capital letters in a row.</summary>
      public int CharacterNumber {
         get{ return characterNumber;}
         set {
            if (value < 1) {
               characterNumber = 1;
            } else {
               characterNumber = value;
            }

            RegularExpression = new Regex(@"\b\w*[A-ZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ]{" + characterNumber + @",}\w*\b", RegexOptions.CultureInvariant);
         }
      }

      public override bool isReady {
         get {
            return true; //is always ready
         }
      }

#endregion

#region Constructor

      public CapitalizationFilter(int capitalizationCharsNumber, string markPrefix, string markPostfix) {
         CharacterNumber = capitalizationCharsNumber;

         MarkPrefix = markPrefix;
         MarkPostfix = markPostfix;
      }

#endregion

#region Implemented methods

      public override bool Contains(string testString, params string[] sources) {
         bool result = false;

         if (string.IsNullOrEmpty(testString)) {
            logContains();
         } else {
            result = RegularExpression.Match(testString).Success;
         }

         return result;
      }

      public override List<string> GetAll(string testString, params string[] sources) {
         List<string> result = new List<string>();

         if (string.IsNullOrEmpty(testString)) {
            logGetAll();
         } else {
            MatchCollection matches = RegularExpression.Matches(testString);
            
            foreach (Match match in matches) {
               foreach (Capture capture in match.Captures) {
                  //Debug.Log("Test string contains an excessive capital word: '" + capture.Value + "'");
                  
                  if (!result.Contains(capture.Value)) {
                     result.Add(capture.Value);
                  }
               }
            }
         }

         return result.Distinct().OrderBy(x => x).ToList();
      }

      public override string ReplaceAll(string testString, params string[] sources) {
         string result = testString;

         if (string.IsNullOrEmpty(testString)) {
            logReplaceAll();

            result = string.Empty;
         } else {
            MatchCollection matches = RegularExpression.Matches(testString);
            
            foreach (Match match in matches) {
               foreach (Capture capture in match.Captures) {
                  //Debug.Log("Test string contains an excessive capital word: '" + capture.Value + "'");

                  //if (characterNumber == 1) {
                     //result = result.Replace(capture.Value.Substring(characterNumber), capture.Value.Substring(characterNumber).ToLowerInvariant());
                  //} else {
                     result = result.Replace(capture.Value, capture.Value.ToLowerInvariant());
  //                }
               }
            }
         }

         return result;
      }
      
      public override string Replace(string text, List<string> badWords) {
         string result = text;
         
         if (string.IsNullOrEmpty(text)) {
            logReplace();
            
            result = string.Empty;
         } else {
            if (badWords == null || badWords.Count == 0) {
               Debug.LogWarning("Parameter 'badWords' is null or empty!" + Environment.NewLine + "=> 'Replace()' will return the original string.");
            } else {
               foreach(string badword in badWords) {
                  result = result.Replace(badword, badword.ToLowerInvariant());
               }
            }
         }
         
         return result;
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com