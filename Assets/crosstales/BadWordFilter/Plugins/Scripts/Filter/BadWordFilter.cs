using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LinqTools;

namespace Crosstales.BadWord {
   /// <summary>Filter for bad words. The class can also replace all bad words inside a string.</summary>
   public class BadWordFilter : Filter {
      
#region Variables
      /// <summary>Replace characters for bad words.</summary>
      public string ReplaceCharacters;

      /// <summary>Defines how exact the match will be. Without fuzziness, only exact matches are detected. Important: “Fuzzy” is much more performance consuming – so be careful!</summary>
      public bool isFuzzy;

      private readonly List<BadWordProvider> tempBadWordProviderLTR; //left-to-right
      private readonly List<BadWordProvider> tempBadWordProviderRTL; //right-to-left
      private readonly Dictionary<string, Regex> exactBadwordsRegex = new Dictionary<string, Regex>(30);
      private readonly Dictionary<string, Regex> fuzzyBadwordsRegex = new Dictionary<string, Regex>(30);
      private readonly Dictionary<string, List<Regex>> debugExactBadwordsRegex = new Dictionary<string, List<Regex>>(30);
      private readonly Dictionary<string, List<Regex>> debugFuzzyBadwordsRegex = new Dictionary<string, List<Regex>>(30);
      private bool ready = false;
      private bool readyFirstime = false;

      private List<BadWordProvider> badWordProviderLTR = new List<BadWordProvider>(); //left-to-right
      private List<BadWordProvider> badWordProviderRTL = new List<BadWordProvider>(); //right-to-left
#endregion

#region Properties

      /// <summary>List of all left-to-right providers.</summary>
      /// <returns>All left-to-right providers.</returns>
      public List<BadWordProvider> BadWordProviderLTR {
         get{ return badWordProviderLTR;}
         set {
            badWordProviderLTR = value;
            if (badWordProviderLTR != null && badWordProviderLTR.Count > 0) {
               foreach (BadWordProvider bp in badWordProviderLTR) {
                  if (bp != null) {
                     if (Constants.DEBUG_BADWORDS) {
                        debugExactBadwordsRegex.CTAddRange(bp.DebugExactBadwordsRegex);
                        debugFuzzyBadwordsRegex.CTAddRange(bp.DebugFuzzyBadwordsRegex);
                     } else {
                        exactBadwordsRegex.CTAddRange(bp.ExactBadwordsRegex);
                        fuzzyBadwordsRegex.CTAddRange(bp.FuzzyBadwordsRegex);
                     }
                  } else {
                     if (!Helper.isEditorMode) {
                        Debug.LogError("A LTR-BadWordProvider is null!");
                     }
                  }
               }
            } else {
               badWordProviderLTR = new List<BadWordProvider>();
               Debug.LogWarning("No 'BadWordProviderLTR' added!" + Environment.NewLine + "If you want to use this functionality, please add your desired 'BadWordProviderLTR' in the editor or script.");
            }
         }
      }

      /// <summary>List of all right-to-left providers.</summary>
      /// <returns>All right-to-left providers.</returns>
      public List<BadWordProvider> BadWordProviderRTL {
         get{ return badWordProviderRTL;}
         set {
            badWordProviderRTL = value;
            if (badWordProviderRTL != null && badWordProviderRTL.Count > 0) {
               foreach (BadWordProvider bp in badWordProviderRTL) {
                  if (bp != null) {
                     if (Constants.DEBUG_BADWORDS) {
                        debugExactBadwordsRegex.CTAddRange(bp.DebugExactBadwordsRegex);
                        debugFuzzyBadwordsRegex.CTAddRange(bp.DebugFuzzyBadwordsRegex);
                     } else {
                        exactBadwordsRegex.CTAddRange(bp.ExactBadwordsRegex);
                        fuzzyBadwordsRegex.CTAddRange(bp.FuzzyBadwordsRegex);
                     }
                  } else {
                     Debug.LogError("A RTL-BadWordProvider is null!");
                  }
               }
            } else {
               badWordProviderRTL = new List<BadWordProvider>();
               Debug.LogWarning("No 'BadWordProviderRTL' added!" + Environment.NewLine + "If you want to use this functionality, please add your desired 'BadWordProviderRTL' in the editor or script.");
            }
         }
      }

      public override bool isReady {
         get {
            bool result = true;
            
            if (!ready) {
               if (tempBadWordProviderLTR != null) {
                  foreach (BadWordProvider bp in tempBadWordProviderLTR) {
                     if (bp != null && !bp.Ready) {
                        result = false;
                        break;
                     }
                  }
               }

               if (result) {
                  if (tempBadWordProviderRTL != null) {
                     foreach (BadWordProvider bp in tempBadWordProviderRTL) {
                        if (bp != null && !bp.Ready) {
                           result = false;
                           break;
                        }
                     }
                  }
               }
               
               if (!readyFirstime && result) {
                  BadWordProviderLTR = tempBadWordProviderLTR;
				      BadWordProviderRTL = tempBadWordProviderRTL;

						if (BadWordProviderLTR != null) {
							foreach (BadWordProvider bpLTR in BadWordProviderLTR) {
								if (bpLTR != null) {
									foreach (Source src in bpLTR.Sources) {
										if (!sources.ContainsKey (src.Name)) {
											sources.Add (src.Name, src);
										}
									}
								}
							}
						}

						if (BadWordProviderRTL != null) {
                  foreach (BadWordProvider bpRTL in BadWordProviderRTL) {
                     if (bpRTL != null) {
                        foreach (Source src in bpRTL.Sources) {
                           if (!sources.ContainsKey(src.Name)) {
                              sources.Add(src.Name, src);
                           }
                        }
                     }
							}
                  }
                  
                  readyFirstime = true;
               }
            }
            
            ready = result;
            
            return result;
         }
      }

#endregion

#region Constructor

      public BadWordFilter(List<BadWordProvider> badWordProviderLTR, List<BadWordProvider> badWordProviderRTL, string replaceCharacters, bool isFuzzy, string markPrefix, string markPostfix) {

         tempBadWordProviderLTR = badWordProviderLTR;
         tempBadWordProviderRTL = badWordProviderRTL;

         ReplaceCharacters = replaceCharacters;
         this.isFuzzy = isFuzzy;

         MarkPrefix = markPrefix;
         MarkPostfix = markPostfix;
      }

#endregion

#region Implemented methods

      public override bool Contains(string testString, params string[] sources) {
         bool result = false;

         if (isReady) {
            if (string.IsNullOrEmpty(testString)) {
               logContains();
            } else {
               
#region DEBUG

               if (Constants.DEBUG_BADWORDS) {
                  if (sources == null || sources.Length == 0) {
                     if (isFuzzy) {
                        //Debug.Log("FUZZY enabled");
                        foreach (List<Regex> badWordRegexes in debugFuzzyBadwordsRegex.Values) {
                           foreach (Regex badWordRegex in badWordRegexes) {
                              Match match = badWordRegex.Match(testString);
                              if (match.Success) {
                                 Debug.Log("Test string contains a bad word: '" + match.Value + "' detected by regex '" + badWordRegex + "'");
                                 result = true;
                                 break;
                              }
                           }
                        }
                     } else {
                        //Debug.Log("FUZZY disabled");
                        foreach (List<Regex> badWordRegexes in debugExactBadwordsRegex.Values) {
                           foreach (Regex badWordRegex in badWordRegexes) {
                              Match match = badWordRegex.Match(testString);
                              if (match.Success) {
                                 Debug.Log("Test string contains a bad word: '" + match.Value + "' detected by regex '" + badWordRegex + "'");
                                 result = true;
                                 break;
                              }
                           }
                        }
                     }
                  } else {
                     List<Regex> badWordRegexes;
                     foreach (string badWordsResource in sources) {
                        
                        if (isFuzzy) {
                           //Debug.Log("FUZZY enabled");
                           if (debugFuzzyBadwordsRegex.TryGetValue(badWordsResource, out badWordRegexes)) {
                              foreach (Regex badWordRegex in badWordRegexes) {
                                 Match match = badWordRegex.Match(testString);
                                 if (match.Success) {
                                    Debug.Log("Test string contains a bad word: '" + match.Value + "' detected by regex '" + badWordRegex + "'" + "' from source '" + badWordsResource + "'");
                                    result = true;
                                    break;
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        } else {
                           //Debug.Log("FUZZY disabled");
                           if (debugExactBadwordsRegex.TryGetValue(badWordsResource, out badWordRegexes)) {
                              foreach (Regex badWordRegex in badWordRegexes) {
                                 Match match = badWordRegex.Match(testString);
                                 if (match.Success) {
                                    Debug.Log("Test string contains a bad word: '" + match.Value + "' detected by regex '" + badWordRegex + "'" + "' from source '" + badWordsResource + "'");
                                    result = true;
                                    break;
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        }
                     }
                  }

#endregion

               } else {
                  if (sources == null || sources.Length == 0) {
                     if (isFuzzy) {
                        foreach (Regex badWordRegex in fuzzyBadwordsRegex.Values) {
                           if (badWordRegex.Match(testString).Success) {
                              result = true;
                              break;
                           }
                        }
                     } else {
                        foreach (Regex badWordRegex in exactBadwordsRegex.Values) {
                           if (badWordRegex.Match(testString).Success) {
                              result = true;
                              break;
                           }
                        }
                     }

                  } else {
                     Regex badWordRegex;
                     foreach (string badWordsResource in sources) {

                        if (isFuzzy) {
                           if (fuzzyBadwordsRegex.TryGetValue(badWordsResource, out badWordRegex)) {
                              Match match = badWordRegex.Match(testString);
                              if (match.Success) {
                                 result = true;
                                 break;
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        } else {
                           if (exactBadwordsRegex.TryGetValue(badWordsResource, out badWordRegex)) {
                              Match match = badWordRegex.Match(testString);
                              if (match.Success) {
                                 result = true;
                                 break;
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        }
                     }
                  }
               }
            }
         } else {
            logFilterNotReady();
         }

         return result;
      }

      public override List<string> GetAll(string testString, params string[] sources) {
         List<string> result = new List<string>();

         if (isReady) {
            if (string.IsNullOrEmpty(testString)) {
               logGetAll();
            } else {
               
#region DEBUG

               if (Constants.DEBUG_BADWORDS) {
                  if (sources == null || sources.Length == 0) {
                     if (isFuzzy) {
                        //Debug.Log("FUZZY enabled");
                        foreach (List<Regex> badWordsResources in debugFuzzyBadwordsRegex.Values) {
                           foreach(Regex badWordsResource in badWordsResources) {
                              MatchCollection matches = badWordsResource.Matches(testString);
                              
                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordsResource + "'");

                                    if (!result.Contains(capture.Value)) {
                                       result.Add(capture.Value);
                                    }
                                 }
                              }
                           }
                        }
                     } else {
                        //Debug.Log("FUZZY disabled");
                        foreach (List<Regex> badWordsResources in debugExactBadwordsRegex.Values) {
                           foreach(Regex badWordsResource in badWordsResources) {
                              MatchCollection matches = badWordsResource.Matches(testString);
                              
                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordsResource + "'");

                                    if (!result.Contains(capture.Value)) {
                                       result.Add(capture.Value);
                                    }
                                 }
                              }
                           }
                        }
                     }
                  } else {
                     List<Regex> badWordRegexes;
                     
                     foreach (string badWordsResource in sources) {
                        if (isFuzzy) {
                           //Debug.Log("FUZZY enabled");
                           if (debugFuzzyBadwordsRegex.TryGetValue(badWordsResource, out badWordRegexes)) {
                              foreach(Regex badWordRegex in badWordRegexes) {
                                 MatchCollection matches = badWordRegex.Matches(testString);
                                 
                                 foreach (Match match in matches) {
                                    foreach (Capture capture in match.Captures) {
                                       Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordRegex + "'" + "' from source '" + badWordsResource + "'");

                                       if (!result.Contains(capture.Value)) {
                                          result.Add(capture.Value);
                                       }
                                    }
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        } else {
                           //Debug.Log("FUZZY disabled");
                           if (debugExactBadwordsRegex.TryGetValue(badWordsResource, out badWordRegexes)) {
                              foreach(Regex badWordRegex in badWordRegexes) {
                                 MatchCollection matches = badWordRegex.Matches(testString);
                                 
                                 foreach (Match match in matches) {
                                    foreach (Capture capture in match.Captures) {
                                       Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordRegex + "'" + "' from source '" + badWordsResource + "'");

                                       if (!result.Contains(capture.Value)) {
                                          result.Add(capture.Value);
                                       }
                                    }
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        }
                     }
                  }

#endregion

               } else {
                  if (sources == null || sources.Length == 0) {
                     if (isFuzzy) {
                        foreach (Regex badWordsResource in fuzzyBadwordsRegex.Values) {
                           MatchCollection matches = badWordsResource.Matches(testString);

                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 if (!result.Contains(capture.Value)) {
                                    result.Add(capture.Value);
                                 }
                              }
                           }
                        }
                     } else {
                        foreach (Regex badWordsResource in exactBadwordsRegex.Values) {
                           MatchCollection matches = badWordsResource.Matches(testString);

                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 if (!result.Contains(capture.Value)) {
                                    result.Add(capture.Value);
                                 }
                              }
                           }
                        }
                     }
                  } else {
                     Regex badWordRegex;

                     foreach (string badWordsResource in sources) {
                        if (isFuzzy) {
                           if (fuzzyBadwordsRegex.TryGetValue(badWordsResource, out badWordRegex)) {
                              MatchCollection matches = badWordRegex.Matches(testString);

                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    if (!result.Contains(capture.Value)) {
                                       result.Add(capture.Value);
                                    }
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        } else {
                           if (exactBadwordsRegex.TryGetValue(badWordsResource, out badWordRegex)) {
                              MatchCollection matches = badWordRegex.Matches(testString);

                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    if (!result.Contains(capture.Value)) {
                                       result.Add(capture.Value);
                                    }
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        }
                     }
                  }
               }
            }
         } else {
            logFilterNotReady();
         }

         return result.Distinct().OrderBy(x => x).ToList();
      }

      public override string ReplaceAll(string testString, params string[] sources) {
         string result = testString;

         if (isReady) {
            if (string.IsNullOrEmpty(testString)) {
               logReplaceAll();

               result = string.Empty;
            } else {
               
#region DEBUG

               if (Constants.DEBUG_BADWORDS) {
                  if (sources == null || sources.Length == 0) {
                     if (isFuzzy) {
                        //Debug.Log("FUZZY enabled");
                        foreach (List<Regex> badWordsResources in debugFuzzyBadwordsRegex.Values) {
                           foreach(Regex badWordsResource in badWordsResources) {
                              MatchCollection matches = badWordsResource.Matches(testString);
                              
                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordsResource + "'");

                                    result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                                 }
                              }
                           }
                        }
                     } else {
                        //Debug.Log("FUZZY disabled");
                        foreach (List<Regex> badWordsResources in debugExactBadwordsRegex.Values) {
                           foreach(Regex badWordsResource in badWordsResources) {
                              MatchCollection matches = badWordsResource.Matches(testString);
                              
                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordsResource + "'");

                                    result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                                 }
                              }
                           }
                        }
                     }
                  } else {
                     List<Regex> badWordRegexes;
                     
                     foreach (string badWordsResource in sources) {
                        if (isFuzzy) {
                           if (debugFuzzyBadwordsRegex.TryGetValue(badWordsResource, out badWordRegexes)) {
                              foreach(Regex badWordRegex in badWordRegexes) {
                                 MatchCollection matches = badWordRegex.Matches(testString);
                                 
                                 foreach (Match match in matches) {
                                    foreach (Capture capture in match.Captures) {
                                       Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordRegex + "'" + "' from source '" + badWordsResource + "'");

                                       result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                                    }
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        } else {
                           if (debugExactBadwordsRegex.TryGetValue(badWordsResource, out badWordRegexes)) {
                              foreach(Regex badWordRegex in badWordRegexes) {
                                 MatchCollection matches = badWordRegex.Matches(testString);
                                 
                                 foreach (Match match in matches) {
                                    foreach (Capture capture in match.Captures) {
                                       Debug.Log("Test string contains a bad word: '" + capture.Value + "' detected by regex '" + badWordRegex + "'" + "' from source '" + badWordsResource + "'");

                                       result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                                    }
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        }
                     }
                  }

#endregion

               } else {
                  if (sources == null || sources.Length == 0) {
                     if (isFuzzy) {
                        foreach (Regex badWordsResource in fuzzyBadwordsRegex.Values) {
                           MatchCollection matches = badWordsResource.Matches(testString);

                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                              }
                           }
                        }
                     } else {
                        foreach (Regex badWordsResource in exactBadwordsRegex.Values) {
                           MatchCollection matches = badWordsResource.Matches(testString);

                           foreach (Match match in matches) {
                              foreach (Capture capture in match.Captures) {
                                 result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                              }
                           }
                        }
                     }
                  } else {
                     Regex badWordRegex;

                     foreach (string badWordsResource in sources) {


                        if (isFuzzy) {
                           if (fuzzyBadwordsRegex.TryGetValue(badWordsResource, out badWordRegex)) {
                              MatchCollection matches = badWordRegex.Matches(testString);

                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        } else {
                           if (exactBadwordsRegex.TryGetValue(badWordsResource, out badWordRegex)) {
                              MatchCollection matches = badWordRegex.Matches(testString);

                              foreach (Match match in matches) {
                                 foreach (Capture capture in match.Captures) {
                                    result = result.Replace(capture.Value, Helper.CreateReplaceString(ReplaceCharacters, capture.Value.Length));
                                 }
                              }
                           } else {
                              logResourceNotFound(badWordsResource);
                           }
                        }
                     }
                  }
               }
            }
         } else {
            logFilterNotReady();
         }

         return result;
      }

      public override string Replace(string text, List<string> badWords) {
         string result = text;
         
         if (string.IsNullOrEmpty(text)) {
            logReplace();
            
            result = string.Empty;
         } else {
            if (badWords == null || badWords.Count == 0) {
               Debug.LogWarning("Parameter 'badWords' is null or empty!" + Environment.NewLine + "=> 'Replace()' will return the original string.");
            } else {
               foreach (string badword in badWords) {
                  result = result.Replace(badword, Helper.CreateReplaceString(ReplaceCharacters, badword.Length));
               }
            }
         }
         
         return result;
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com