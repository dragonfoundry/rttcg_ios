﻿using System;

namespace Crosstales.BadWord {
   /// <summary>Collected constants of very general utility for the asset.</summary>
   public static class Constants {

#region Constant variables

      /// <summary>Name of the asset.</summary>
      public const string ASSET_NAME = "BWF PRO";
      //PRO
      //public const string ASSET_NAME = "BWF"; //DLL

      /// <summary>Version of the asset.</summary>
      public const string ASSET_VERSION = "2.6.3";

      /// <summary>Build number of the asset.</summary>
      public const int ASSET_BUILD = 263;

      /// <summary>Create date of the asset (YYYY, MM, DD).</summary>
      public static readonly DateTime ASSET_CREATED = new DateTime(2015, 1, 3);

      /// <summary>Change date of the asset (YYYY, MM, DD).</summary>
      public static readonly DateTime ASSET_CHANGED = new DateTime(2016, 7, 25);

      /// <summary>Author of the asset.</summary>
      public const string ASSET_AUTHOR = "crosstales LLC";

      /// <summary>URL of the asset author.</summary>
      public const string ASSET_AUTHOR_URL = "http://www.crosstales.com";

      /// <summary>URL of the asset in UAS.</summary>
      public const string ASSET_URL = "https://www.assetstore.unity3d.com/en/#!/content/26255";
      //PRO
      //public const string ASSET_URL = "https://www.assetstore.unity3d.com/#!/content/48397"; //DLL

      /// <summary>URL for update-checks of the asset</summary>
      public const string ASSET_UPDATE_CHECK_URL = "http://www.crosstales.com/media/assets/bwf_versions.txt";

      /// <summary>Contact to the owner of the asset.</summary>
      public const string ASSET_CONTACT = "assets@crosstales.com";

      /// <summary>UID of the asset.</summary>
      public static readonly Guid ASSET_UID = new Guid("b11eebc0-525a-4d58-b33d-c0a9a728f3a9");
      //PRO
      //public static readonly Guid ASSET_UID = new Guid("61a28d26-56a8-4109-8131-45161614ee9d"); //DLL

      /// <summary>URL of the asset manual.</summary>
      //public const string ASSET_MANUAL_URL = "http://goo.gl/pbgZLx";
      public const string ASSET_MANUAL_URL = "http://www.crosstales.com/en/assets/badwordfilter/BadWordFilter-doc.pdf";
	  
      /// <summary>URL of the asset API.</summary>
      public const string ASSET_API_URL = "http://goo.gl/QkE2sN";
      //public const string ASSET_API_URL = "http://www.crosstales.com/en/assets/badwordfilter/api";
	  
      /// <summary>URL of the asset forum.</summary>
      public const string ASSET_FORUM_URL = "http://goo.gl/Mj9XpS";
      //public const string ASSET_FORUM_URL = "http://forum.unity3d.com/threads/bad-word-filter-solution-against-profanity-obscenity.289960/";
	  
      /// <summary>URL of the asset in crosstales.</summary>
      public const string ASSET_CT_URL = "http://www.crosstales.com/en/assets/badwordfilter/";
	  
      /// <summary>Name of the BWF scene object.</summary>
      public const string MANAGER_SCENE_OBJECT_NAME = "BWF";
	  
      // Keys for the configuration of the asset
      private const string KEY_PREFIX = "BWF_CFG_";
      public const string KEY_DEBUG = KEY_PREFIX + "DEBUG";
      public const string KEY_DEBUG_BADWORDS = KEY_PREFIX + "DEBUG_BADWORDS";
      public const string KEY_DEBUG_DOMAINS = KEY_PREFIX + "DEBUG_DOMAINS";
      public const string KEY_UPDATE_CHECK = KEY_PREFIX + "UPDATE_CHECK";
      public const string KEY_UPDATE_OPEN_UAS = KEY_PREFIX + "UPDATE_OPEN_UAS";
      public const string KEY_PREFAB_PATH = KEY_PREFIX + "PREFAB_PATH";
      public const string KEY_PREFAB_AUTOLOAD = KEY_PREFIX + "PREFAB_AUTOLOAD";
      public const string KEY_UPDATE_DATE = KEY_PREFIX + "UPDATE_DATE";

#endregion

#region Changable variables

      /// <summary>Enable or disable debug logging for the asset.</summary>
      public static bool DEBUG = false;

      /// <summary>Enable or disable debug logging for BadWords (Attention: slow!).</summary>
      public static bool DEBUG_BADWORDS = false;

      /// <summary>Enable or disable debug logging for Domains (Attention: VERY SLOOOOOOOOWWWW!).</summary>
      public static bool DEBUG_DOMAINS = false;

      /// <summaryEnable or disable update-checks for the asset.</summary>
      public static bool UPDATE_CHECK = true;

      /// <summaryOpen the UAS-site when an update is found.</summary>
      public static bool UPDATE_OPEN_UAS = false;

      /// <summary>Path of all prefabs.</summary>
      public static string PREFAB_PATH = "Assets/crosstales/BadWordFilter/Prefabs/";

      /// <summary>Automatically load and add the prefabs to the scene.</summary>
      public static bool PREFAB_AUTOLOAD = true;

      // Text fragments for the asset
      public static string TEXT_TOSTRING_START = " {";
      public static string TEXT_TOSTRING_END = "}";
      public static string TEXT_TOSTRING_DELIMITER = "', ";
      public static string TEXT_TOSTRING_DELIMITER_END = "'";

#endregion

   }
}
// Copyright 2015-2016 www.crosstales.com