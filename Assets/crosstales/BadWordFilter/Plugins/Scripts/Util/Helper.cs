using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Crosstales.BadWord {
   /// <summary>Various helper functions.</summary>
   public static class Helper {
      
#region Variables

      private static readonly Regex lineEndingsRegex = new Regex(@"\r\n|\r|\n");
      private static readonly System.Random rd = new System.Random();

      private const string WINDOWS_PATH_DELIMITER = @"\";
      private const string UNIX_PATH_DELIMITER = "/";

#endregion

#region Static properties

      /// <summary>Checks if a Internet connection is available.</summary>
      /// <returns>True if a Internet connection is available.</returns>
      public static bool isInternetAvailable {
         get {
            return Application.internetReachability != NetworkReachability.NotReachable;
         }
      }

      /// <summary>Checks if the current platform is Windows.</summary>
      /// <returns>True if the current platform is Windows.</returns>
      public static bool isWindowsPlatform {
         get {
            return Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor;
         }
      }

      /// <summary>Checks if the current platform is OSX.</summary>
      /// <returns>True if the current platform is OSX.</returns>
      public static bool isMacOSPlatform {
         get {
            return Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor;
         }
      }

      /// <summary>Checks if the current platform is Linux.</summary>
      /// <returns>True if the current platform is Linux.</returns>
      public static bool isLinuxPlatform {
         get {
            return Application.platform == RuntimePlatform.LinuxPlayer; //TODO add the editor as soon it's released
         }
      }

      /// <summary>Checks if the we are in Editor mode.</summary>
      /// <returns>True if in Editor mode.</returns>
      public static bool isEditorMode {
         get {
            return (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor) && !Application.isPlaying;
         }
      }

      /// <summary>Checks if the current platform is supported.</summary>
      /// <returns>True if the current platform is supported.</returns>
      public static bool isSupportedPlatform {
         get {
            return isWindowsPlatform || isMacOSPlatform || isLinuxPlatform;
         }
      }

#endregion

#region Static methods

      /// <summary>Validates a given path and add missing slash.</summary>
      /// <param name="path">Path to validate</param>
      /// <returns>Valid path</returns>
      public static string ValidatePath(string path) {
         string result = null;

         if (isWindowsPlatform) {
            result = path.Replace('/', '\\');

            if (!result.EndsWith(WINDOWS_PATH_DELIMITER)) {
               result += WINDOWS_PATH_DELIMITER;
            }
         } else {
            result = path.Replace('\\', '/');

            if (!result.EndsWith(UNIX_PATH_DELIMITER)) {
               result += UNIX_PATH_DELIMITER;
            }
         }

         return result;
      }

//      static int count = 0;
//      static int itter = 0;

      /// <summary>Split the given text to lines and return it as list.</summary>
      /// <param name="text">Complete text fragment</param>
      /// <param name="skipHeaderLines">Number of skipped header lines (default: 0, optional)</param>
      /// <param name="skipFooterLines">Number of skipped footer lines (default: 0, optional)</param>
      /// <param name="splitChar">Split character for the lines (default: #, optional)</param>
      /// <returns>Splitted lines as array</returns>
      public static List<string> SplitStringToLines(string text, int skipHeaderLines = 0, int skipFooterLines = 0, char splitChar = '#') {
         List<string> result = new List<string>(200);
         
         if (string.IsNullOrEmpty(text)) {
            Debug.LogWarning("Parameter 'text' is null or empty!" + Environment.NewLine + "=> 'ReadTextLines()' will return an empty string for the given resource.");
         } else {
            string[] lines = lineEndingsRegex.Split(text);

            for (int ii = 0; ii < lines.Length; ii++) {
               if (ii + 1 > skipHeaderLines && ii < lines.Length - skipFooterLines) {
                  //if (!lines[ii].StartsWith("#", StringComparison.InvariantCulture)) { //disabled line? Windows Store!
                  if (!String.IsNullOrEmpty(lines [ii]) && !lines [ii].StartsWith("#", StringComparison.OrdinalIgnoreCase)) { //valid and not disabled line?
                     result.Add(lines [ii].Split(splitChar) [0]);
                  }
               }
            }
         }

//         count += result.Count;
//         itter++;
//
//         Debug.Log(result.Count);
//         Debug.Log(count/itter);
         return result;
      }

      /// <summary>Creates a string of characters with a given length.</summary>
      /// <param name="replaceChars">Valid character set for the replace string</param>
      /// <param name="stringLength">Length of the generated replace string</param>
      /// <returns>Generated replace string</returns>
      public static string CreateReplaceString(string replaceChars, int stringLength) {
         if (replaceChars.Length > 1) {
            char[] chars = new char[stringLength];
            
            for (int i = 0; i < stringLength; i++) {
               chars [i] = replaceChars [rd.Next(0, replaceChars.Length)];
            }
            
            return new string(chars);
         } else if (replaceChars.Length == 1) {
            return new string(replaceChars [0], stringLength);        
         }
         
         return string.Empty;
      }

      /// <summary>
      /// Generate nice HSV colors.
      /// Based on https://gist.github.com/rje/6206099
      /// </summary>
      /// <param name="h">Hue</param>
      /// <param name="s">Saturation</param>
      /// <param name="v">Value</param>
      /// <param name="a">Alpha (optional)</param>
      /// <returns>True if the current platform is supported.</returns>
      public static Color HSVToRGB(float h, float s, float v, float a = 1f) {
         if (s == 0f) {
            return new Color(v, v, v, a);
         }

         h /= 60f;
         var sector = Mathf.FloorToInt(h);
         var fact = h - sector;
         var p = v * (1f - s);
         var q = v * (1f - s * fact);
         var t = v * (1 - s * (1 - fact));

         switch (sector) {
            case 0:
               return new Color(v, t, p, a);
            case 1:
               return new Color(q, v, p, a);
            case 2:
               return new Color(p, v, t, a);
            case 3:
               return new Color(p, q, v, a);
            case 4:
               return new Color(t, p, v, a);
            default:
               return new Color(v, p, q, a);
         }
      }

#endregion

#region Extension methods

      /// <summary>
      /// Extension method for dictionaries.
      /// Adds a dictionary to an existing one.
      /// </summary>
      public static void CTAddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection) {
         if (collection == null) {
            throw new ArgumentNullException("collection");
         }

         foreach (var item in collection) {
            if (!source.ContainsKey(item.Key)) { 
               source.Add(item.Key, item.Value);
            } else {
               // handle duplicate key issue here
               Debug.LogWarning("Duplicate key found: " + item.Key);
            }  
         } 
      }

      /// <summary>
      /// Extension method for Arrays.
      /// Dumps an array to a string.
      /// </summary>
      /// <param name="array">Array to dump.</param>
      /// <returns>String with lines for all array entries.</returns>
      public static string CTDump<T>(this T[] array) {
         if (array == null || array.Length <= 0)
            throw new ArgumentNullException("array");

         StringBuilder sb = new StringBuilder();

         foreach (T element in array) {
            if (0 < sb.Length) {
               sb.Append(Environment.NewLine);
            }
            sb.Append(element.ToString());
         }

         return sb.ToString();
      }

      /// <summary>
      /// Extension method for Lists.
      /// Dumps a list to a string.
      /// </summary>
      /// <param name="list">List to dump.</param>
      /// <returns>String with lines for all list entries.</returns>
      public static string CTDump<T>(this List<T> list) {
         if (list == null)
            throw new ArgumentNullException("list");

         StringBuilder sb = new StringBuilder();

         foreach (T element in list) {
            if (0 < sb.Length) {
               sb.Append(Environment.NewLine);
            }
            sb.Append(element.ToString());
         }

         return sb.ToString();
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com