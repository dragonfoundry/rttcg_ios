using UnityEngine;

namespace Crosstales.BadWord {
   /// <summary>Base class for all managers.</summary>
   [ExecuteInEditMode]
   public abstract class Manager : MonoBehaviour {

#region Variables

      [Header("Marker")]
      /// <summary>Mark prefix for bad words (default: bold and color).</summary>
      [Tooltip("Mark prefix for bad words (default: bold and color).")]
      public string MarkPrefix = "<b><color=red>";

      /// <summary>Mark postfix for bad words (default: bold and color).</summary>
      [Tooltip("Mark postfix for bad words (default: bold and color).")]
      public string MarkPostfix = "</color></b>";

      #endregion
   }
}
// Copyright 2015-2016 www.crosstales.com