using UnityEngine;
using System;
using System.Collections.Generic;

namespace Crosstales.BadWord {
   /// <summary>Manager for excessive capitalization.</summary>
	[DisallowMultipleComponent]
	[HelpURL("http://www.crosstales.com/en/assets/badwordfilter/api/class_crosstales_1_1_bad_word_1_1_capitalization_manager.html")]
   public class CapitalizationManager : Manager {
      
#region Variables

      [Header("Settings")]
      /// <summary>Defines the number of allowed capital letters in a row. (default: 1).</summary>
      [Tooltip("Defines the number of allowed capital letters in a row. (default: 3).")]
      public int CapitalizationCharsNumber = 3;

      private static bool initalized = false;
      private static CapitalizationFilter filter;
      private static CapitalizationManager manager;
      private static bool loggedFilterIsNull = false;
      private static bool loggedOnlyOneInstance = false;

      private const string clazz = "CapitalizationManager";

#endregion

#region MonoBehaviour methods

      void OnEnable() {
         if (Helper.isEditorMode || !initalized) {

            manager = this;

            Load();
            
            if (!Helper.isEditorMode) {
               DontDestroyOnLoad(transform.root.gameObject);
               initalized = true;
            }         
         } else {
            if (!Helper.isEditorMode) {
               if (!loggedOnlyOneInstance) {
                  loggedOnlyOneInstance = true;

                  Debug.LogWarning("Only one active instance of 'CapitalizationManager' allowed in all scenes!" + Environment.NewLine + "This object will now be destroyed.");
               }
               
               Destroy(transform.root.gameObject, 0.2f);
            }
         }
      }

      void OnValidate() {
         if (CapitalizationCharsNumber < 2) {
            CapitalizationCharsNumber = 2;
         }
      }

#endregion

#region Static properties

      /// <summary>Returns the filter of the manager.</summary>
      /// <returns>Filter for the manager</returns>
      public static CapitalizationFilter Filter {
         get {
            return filter;
         }
      }

      /// <summary>Checks the readiness status of the manager.</summary>
      /// <returns>True if the manager is ready.</returns>
      public static bool isReady {
         get {
            return filter.isReady;
         }
      }

#endregion

#region Static methods

      /// <summary>Loads the current filter with all settings from this object.</summary>
      public static void Load() {
         if (manager != null) {
            filter = new CapitalizationFilter(manager.CapitalizationCharsNumber, manager.MarkPrefix, manager.MarkPostfix);
         }
      }

      /// <summary>Searches for excessive capitalizations in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <returns>True if a match was found</returns>
      public static bool Contains(string testString) {
         bool result = false;
         
         if (filter != null) {
            result = filter.Contains(testString);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Searches for excessive capitalizations in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <returns>List with all the matches</returns>
      public static List<string> GetAll(string testString) {
         List<string> result = new List<string>();
         
         if (filter != null) {
            result = filter.GetAll(testString);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Searches and replaces all excessive capitalizations in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <returns>Clean text</returns>
      public static string ReplaceAll(string testString) {
         string result = testString;
         
         if (filter != null) {
            result = filter.ReplaceAll(testString);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>
      /// Replaces all excessive capitalizations in a text.
      /// Use this method if you already have a list of excessive capitalizations (e.g. from the 'GetAll()' method).
      /// </summary>
      /// <param name="text">Text containig excessive capitalizations</param>
      /// <param name="capitalWords">Capital words to replace</param>
      /// <returns>Clean text</returns>
      public static string Replace(string text, List<string> capitalWords) {
         string result = text;
         
         if (filter != null) {
            result = filter.Replace(text, capitalWords);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Marks the text with a prefix and postfix from a list of words.</summary>
      /// <param name="text">Text containig excessive capitalizations</param>
      /// <param name="capitalWords">Excessive capitalizations to mark</param>
      /// <param name="prefix">Prefix for every found capitalization (default: bold and red, optional)</param>
      /// <param name="postfix">Postfix for every found capitalization (default: bold and red, optional)</param>
      /// <returns>Text with marked excessive capitalizations</returns>
      public static string Mark(string text, List<string> capitalWords, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;
         
         if (filter != null) {
            result = filter.Mark(text, capitalWords, prefix, postfix);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Unmarks the text with a prefix and postfix.</summary>
      /// <param name="text">Text with marked excessive capitalizations</param>
      /// <param name="prefix">Prefix for every found capitalization (default: bold and red, optional)</param>
      /// <param name="postfix">Postfix for every found capitalization (default: bold and red, optional)</param>
      /// <returns>Text with unmarked excessive capitalizations</returns>
      public static string Unmark(string text, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;
         
         if (filter != null) {
            result = filter.Unmark(text, prefix, postfix);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      private static void logFilterIsNull(string clazz) {
         if (!loggedFilterIsNull) {
            Debug.LogWarning("'filter' is null!" + Environment.NewLine + "Did you add the '" + clazz + "' to the current scene?");
            loggedFilterIsNull = true;
         }
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com