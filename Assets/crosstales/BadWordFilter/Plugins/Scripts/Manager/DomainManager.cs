using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

namespace Crosstales.BadWord {
   /// <summary>Manager for domains.</summary>
	[DisallowMultipleComponent]
   [HelpURL("http://www.crosstales.com/en/assets/badwordfilter/api/class_crosstales_1_1_bad_word_1_1_domain_manager.html")]
   public class DomainManager : Manager {
      
#region Variables

      [Header("Domain Provider")]
      /// <summary>List of all domain providers.</summary>
      [Tooltip("List of all domain providers.")]
      public List<DomainProvider> DomainProvider;

      [Header("Settings")]
      /// <summary>Replace characters for domains (default: *).</summary>
      [Tooltip("Replace characters for domains (default: *).")]
      public string ReplaceChars = "*"; //e.g. "?#@*&%!$^~+-/<>:;=()[]{}"

      private static bool initalized = false;
      private static DomainFilter filter;
      private static DomainManager manager;
      private static bool loggedFilterIsNull = false;
      private static bool loggedOnlyOneInstance = false;

      private const string clazz = "DomainManager";

#endregion

#region MonoBehaviour methods

      void OnEnable() {
         if (Helper.isEditorMode || !initalized) {
            //StartCoroutine(initalize());

            manager = this;

            Load();

            if (!Helper.isEditorMode) {
               DontDestroyOnLoad(transform.root.gameObject);
               initalized = true;
            }
         } else {
            if (!Helper.isEditorMode) {
               if (!loggedOnlyOneInstance) {
                  loggedOnlyOneInstance = true;

                  Debug.LogWarning("Only one active instance of 'DomainManager' allowed in all scenes!" + Environment.NewLine + "This object will now be destroyed.");
               }

               Destroy(transform.root.gameObject, 0.2f);
            }
         }
      }

#endregion

//#region Private methods
//
//      private IEnumerator initalize() {
//         filter = new DomainFilter(DomainProvider, ReplaceChars, MarkPrefix, MarkPostfix);
//         
//         while (!isReady) {
//            yield return null;
//         }
//         
//         if (!Helper.isEditorMode) {
//            DontDestroyOnLoad(transform.root.gameObject);
//            initalized = true; 
//         }        
//      }
//
//#endregion

#region Static properties

      /// <summary>Returns the filter of the manager.</summary>
      /// <returns>Filter for the manager</returns>
      public static DomainFilter Filter {
         get {
            return filter;
         }
      }

      /// <summary>Checks the readiness status of the manager.</summary>
      /// <returns>True if the manager is ready.</returns>
      public static bool isReady {
         get {
            bool result = false;
         
            if (filter != null) {
               result = filter.isReady;
            } else {
               logFilterIsNull(clazz);
            }
         
            return result;
         }
      }

      /// <summary>Returns all sources for the manager.</summary>
      /// <returns>List with all sources for the manager</returns>
      public static List<Source> Sources {
         get {
            List<Source> result = new List<Source>();
            
            if (filter != null) {
               result = filter.Sources;
            } else {
               logFilterIsNull(clazz);
            }
            
            return result;
         }
      }

#endregion

#region Static methods

      /// <summary>Loads the current filter with all settings from this object.</summary>
      public static void Load() {
         if (manager != null) {
            filter = new DomainFilter(manager.DomainProvider, manager.ReplaceChars, manager.MarkPrefix, manager.MarkPostfix);
         }
      }

      /// <summary>Searches for domains in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "iana")</param>
      /// <returns>True if a match was found</returns>
      public static bool Contains(string testString, params string[] sources) {
         bool result = false;
         
         if (filter != null) {
            result = filter.Contains(testString, sources);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Searches for domains in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "iana")</param>
      /// <returns>List with all the matches</returns>
      public static List<string> GetAll(string testString, params string[] sources) {
         List<string> result = new List<string>();
         
         if (filter != null) {
            result = filter.GetAll(testString, sources);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Searches and replaces all domains in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "iana")</param>
      /// <returns>Clean text</returns>
      public static string ReplaceAll(string testString, params string[] sources) {
         string result = testString;
         
         if (filter != null) {
            result = filter.ReplaceAll(testString, sources);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>
      /// Replaces all domains in a text.
      /// Use this method if you already have a list of domains (e.g. from the 'GetAll()' method).
      /// </summary>
      /// <param name="text">Text containig domains</param>
      /// <param name="domains">Domains to replace</param>
      /// <returns>Clean text</returns>
      public static string Replace(string text, List<string> domains) {
         string result = text;
         
         if (filter != null) {
            result = filter.Replace(text, domains);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Marks the text with a prefix and postfix from a list of words.</summary>
      /// <param name="text">Text containig domains</param>
      /// <param name="domains">Domains to mark</param>
      /// <param name="prefix">Prefix for every found doamin (default: bold and red, optional)</param>
      /// <param name="postfix">Postfix for every found doamin (default: bold and red, optional)</param>
      /// <returns>Text with marked domains</returns>
      public static string Mark(string text, List<string> domains, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;
         
         if (filter != null) {
            result = filter.Mark(text, domains, prefix, postfix);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Unmarks the text with a prefix and postfix.</summary>
      /// <param name="text">Text with marked domains</param>
      /// <param name="prefix">Prefix for every found doamin (default: bold and red, optional)</param>
      /// <param name="postfix">Postfix for every found doamin (default: bold and red, optional)</param>
      /// <returns>Text with unmarked domains</returns>
      public static string Unmark(string text, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;
         
         if (filter != null) {
            result = filter.Unmark(text, prefix, postfix);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      private static void logFilterIsNull(string clazz) {
         if (!loggedFilterIsNull) {
            Debug.LogWarning("'filter' is null!" + Environment.NewLine + "Did you add the '" + clazz + "' to the current scene?");
            loggedFilterIsNull = true;
         }
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com