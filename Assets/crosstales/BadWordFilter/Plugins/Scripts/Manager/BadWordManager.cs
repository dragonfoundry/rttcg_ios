using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

namespace Crosstales.BadWord {
   /// <summary>Manager for for bad words.</summary>
	[DisallowMultipleComponent]
	[HelpURL("http://www.crosstales.com/en/assets/badwordfilter/api/class_crosstales_1_1_bad_word_1_1_bad_word_manager.html")]
   public class BadWordManager : Manager {
      
#region Variables

      [Header("Bad Word Provider")]

      /// <summary>List of all left-to-right providers.</summary>
      [Tooltip("List of all left-to-right providers.")]
      public List<BadWordProvider> BadWordProviderLTR;

      /// <summary>List of all right-to-left providers.</summary>
      [Tooltip("List of all right-to-left providers.")]
      public List<BadWordProvider> BadWordProviderRTL;

      [Header("Settings")]
      /// <summary>Replace characters for bad words (default: *).</summary>
      [Tooltip("Replace characters for bad words (default: *).")]
      public string ReplaceChars = "*"; //e.g. "?#@*&%!$^~+-/<>:;=()[]{}"

      /// <summary>Defines how exact the match will be. Without fuzziness, only exact matches are detected. Important: “Fuzzy” is much more performance consuming – so be careful (default: off).</summary>
      [Tooltip("Defines how exact the match will be. Without fuzziness, only exact matches are detected. Important: “Fuzzy” is much more performance consuming – so be careful (default: off).")]
      public bool Fuzzy = false;

      private GameObject root;

      private static bool initalized = false;
      private static BadWordFilter filter;
      private static BadWordManager manager;
      private static bool loggedFilterIsNull = false;
      private static bool loggedOnlyOneInstance = false;

      private const string clazz = "BadWordManager";

#endregion

#region MonoBehaviour methods

      void OnEnable() {
         if (Helper.isEditorMode || !initalized) {

            manager = this;

            Load();

            root = transform.root.gameObject;

            if (!Helper.isEditorMode) {
               DontDestroyOnLoad(root);
               initalized = true;
            }
         } else {
            if (!Helper.isEditorMode) {
               if (!loggedOnlyOneInstance) {
                  loggedOnlyOneInstance = true;

                  Debug.LogWarning("Only one active instance of 'BadWordManager' allowed in all scenes!" + Environment.NewLine + "This object will now be destroyed.");
               }

               Destroy(root, 0.2f);
            }
         }
      }

//      void Update() {
//         if (Helper.isEditorMode) {
//            if (root != null) {
//               root.name = Constants.MANAGER_SCENE_OBJECT_NAME; //ensure name
//            }
//         }
//      }

#endregion

//#region Private methods
//
//      private IEnumerator initalize() {
//         filter = new BadWordFilter(BadWordProviderLTR, BadWordProviderRTL, ReplaceChars, Fuzzy, MarkPrefix, MarkPostfix);
//
//         while (!isReady) {
//            yield return null;
//         }
//
//         if (!Helper.isEditorMode) {
//            DontDestroyOnLoad(transform.root.gameObject);
//            initalized = true;
//         }
//      }
//
//#endregion

#region Static properties

      /// <summary>Returns the filter of the manager.</summary>
      /// <returns>Filter for the manager</returns>
      public static BadWordFilter Filter {
         get {
            return filter;
         }
      }

      /// <summary>Checks the readiness status of the manager.</summary>
      /// <returns>True if the manager is ready.</returns>
      public static bool isReady {
         get {
            bool result = false;

            if (filter != null) {
               result = filter.isReady;
            } else {
               logFilterIsNull(clazz);
            }

            return result;
         }
      }

      /// <summary>Returns all sources for the manager.</summary>
      /// <returns>List with all sources for the manager</returns>
      public static List<Source> Sources {
         get {
            List<Source> result = new List<Source>();
            
            if (filter != null) {
               result = filter.Sources;
            } else {
               logFilterIsNull(clazz);
            }
            
            return result;
         }
      }
      
#endregion

#region Static methods

      /// <summary>Loads the current filter with all settings from this object.</summary>
      public static void Load() {
         if (manager != null) {
            filter = new BadWordFilter(manager.BadWordProviderLTR, manager.BadWordProviderRTL, manager.ReplaceChars, manager.Fuzzy, manager.MarkPrefix, manager.MarkPostfix);
         }
      }

      /// <summary>Searches for bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "english")</param>
      /// <returns>True if a match was found</returns>
      public static bool Contains(string testString, params string[] sources) {
         bool result = false;
         
         if (filter != null) {
            result = filter.Contains(testString, sources);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Searches for bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "english")</param>
      /// <returns>List with all the matches</returns>
      public static List<string> GetAll(string testString, params string[] sources) {
         List<string> result = new List<string>();
         
         if (filter != null) {
            result = filter.GetAll(testString, sources);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Searches and replaces all bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="sources">Relevant sources (e.g. "english")</param>
      /// <returns>Clean text</returns>
      public static string ReplaceAll(string testString, params string[] sources) {
         string result = testString;
         
         if (filter != null) {
            result = filter.ReplaceAll(testString, sources);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>
      /// Replaces all bad words in a text.
      /// Use this method if you already have a list of bad words (e.g. from the 'GetAll()' method).
      /// </summary>
      /// <param name="text">Text containig bad words</param>
      /// <param name="badWords">Bad words to replace</param>
      /// <returns>Clean text</returns>
      public static string Replace(string text, List<string> badWords) {
         string result = text;
         
         if (filter != null) {
            result = filter.Replace(text, badWords);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Marks the text with a prefix and postfix from a list of words.</summary>
      /// <param name="text">Text containig bad words</param>
      /// <param name="badWords">Bad words to mark</param>
      /// <param name="prefix">Prefix for every found bad word (default: bold and red, optional)</param>
      /// <param name="postfix">Postfix for every found bad word (default: bold and red, optional)</param>
      /// <returns>Text with marked bad words</returns>
      public static string Mark(string text, List<string> badWords, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;
         
         if (filter != null) {
            result = filter.Mark(text, badWords, prefix, postfix);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      /// <summary>Unmarks the text with a prefix and postfix.</summary>
      /// <param name="text">Text with marked bad words</param>
      /// <param name="prefix">Prefix for every found bad word (default: bold and red, optional)</param>
      /// <param name="postfix">Postfix for every found bad word (default: bold and red, optional)</param>
      /// <returns>Text with unmarked bad words</returns>
      public static string Unmark(string text, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text;
         
         if (filter != null) {
            result = filter.Unmark(text, prefix, postfix);
         } else {
            logFilterIsNull(clazz);
         }
         
         return result;
      }

      private static void logFilterIsNull(string clazz) {
         if (!loggedFilterIsNull) {
            Debug.LogWarning("'filter' is null!" + Environment.NewLine + "Did you add the '" + clazz + "' to the current scene?");
            loggedFilterIsNull = true;
         }
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com