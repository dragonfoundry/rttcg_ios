using UnityEngine;
using System.Collections.Generic;
using LinqTools;

namespace Crosstales.BadWord {
   /// <summary>BWF is a multi-manager for all available managers.</summary>
   [ExecuteInEditMode]
   public class BWFManager : MonoBehaviour {
      
		private GameObject root;

#region Static properties

      /// <summary>Checks the readiness status of all managers.</summary>
      /// <returns>True if all managers are ready.</returns>
      public static bool isReady {
         get {
            return BadWordManager.isReady && DomainManager.isReady && CapitalizationManager.isReady && PunctuationManager.isReady;
         }
      }

#endregion
		void OnEnable() {
			root = transform.root.gameObject;
		}

		void Update() {
			if (Helper.isEditorMode) {
				if (root != null) {
					root.name = Constants.MANAGER_SCENE_OBJECT_NAME; //ensure name
				}
			}
		}

#region Static methods

      /// <summary>Loads the filter of a manager.</summary>
      /// <param name="mask">Active manager (default: ManagerMask.All, optional)</param>
      public static void Load(ManagerMask mask = ManagerMask.All) {
         if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All) {
            BadWordManager.Load();
         }

         if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All) {
            DomainManager.Load();
         }

         if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization || (mask & ManagerMask.All) == ManagerMask.All) {
            CapitalizationManager.Load();
         }

         if ((mask & ManagerMask.Punctuation) == ManagerMask.Punctuation || (mask & ManagerMask.All) == ManagerMask.All) {
            PunctuationManager.Load();
         }
      }

      /// <summary>Returns the filter of a manager.</summary>
      /// <param name="mask">Active manager (default: ManagerMask.BadWord, optional)</param>
      /// <returns>Filter for the selected manager</returns>
      public static Filter Filter(ManagerMask mask = ManagerMask.BadWord) {
         Filter result = BadWordManager.Filter;

         if ((mask & ManagerMask.Domain) == ManagerMask.Domain) {
            result = DomainManager.Filter;
         }

         if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization) {
            result = CapitalizationManager.Filter;
         }

         if ((mask & ManagerMask.Punctuation) == ManagerMask.Punctuation) {
            result = PunctuationManager.Filter;
         }

         return result; 
      }

      /// <summary>Returns all sources for a manager.</summary>
      /// <param name="mask">Active manager (default: ManagerMask.All, optional)</param>
      /// <returns>List with all sources for the selected manager</returns>
      public static List<Source> Sources(ManagerMask mask = ManagerMask.All) {
         List<Source> result = new List<Source>(30);
         
         if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All) {
            result.AddRange(BadWordManager.Sources);
         }
         
         if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All) {
            result.AddRange(DomainManager.Sources);
         }
         
//         if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization || (mask & ManagerMask.All) == ManagerMask.All) {
//            result.AddRange(CapitalizationManager.Sources);
//         }
         
//         if ((mask & ManagerMask.Punctuation) == ManagerMask.Punctuation || (mask & ManagerMask.All) == ManagerMask.All) {
//            result.AddRange(PunctuationManager.Sources);
//         }
         
         return result.OrderBy(x => x.Name).ToList();
      }

      /// <summary>Searches for bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="mask">Active manager (default: ManagerMask.All, optional)</param>
      /// <param name="sources">Relevant sources (e.g. "english")</param>
      /// <returns>True if a match was found</returns>
      public static bool Contains(string testString, ManagerMask mask = ManagerMask.All, params string[] sources) {
         return (((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All) && BadWordManager.Contains(testString, sources)) ||
            (((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All) && DomainManager.Contains(testString, sources)) ||
            (((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization || (mask & ManagerMask.All) == ManagerMask.All) && CapitalizationManager.Contains(testString)) ||
            (((mask & ManagerMask.Punctuation) == ManagerMask.Punctuation || (mask & ManagerMask.All) == ManagerMask.All) && PunctuationManager.Contains(testString));
      }

      /// <summary>Searches for bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="mask">Active manager (default: ManagerMask.All, optional)</param>
      /// <param name="sources">Relevant sources (e.g. "english")</param>
      /// <returns>List with all the matches</returns>
      public static List<string> GetAll(string testString, ManagerMask mask = ManagerMask.All, params string[] sources) {
         List<string> result = new List<string>();

         if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All) {
            result.AddRange(BadWordManager.GetAll(testString, sources));
         }

         if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All) {
            result.AddRange(DomainManager.GetAll(testString, sources));
         }

         if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization || (mask & ManagerMask.All) == ManagerMask.All) {
            //result.AddRange(CapitalizationManager.GetAll(testString, sources));
            result.AddRange(CapitalizationManager.GetAll(testString));
         }

         if ((mask & ManagerMask.Punctuation) == ManagerMask.Punctuation || (mask & ManagerMask.All) == ManagerMask.All) {
            //result.AddRange(PunctuationManager.GetAll(testString, sources));
            result.AddRange(PunctuationManager.GetAll(testString));
         }

         return result.Distinct().OrderBy(x => x).ToList();
      }

      /// <summary>Searches and replaces all bad words in a text.</summary>
      /// <param name="testString">Text to check</param>
      /// <param name="mask">Active manager (default: ManagerMask.All, optional)</param>
      /// <param name="sources">Relevant sources (e.g. "english")</param>
      /// <returns>Clean text</returns>
      public static string ReplaceAll(string testString, ManagerMask mask = ManagerMask.All, params string[] sources) {
         string result = testString ?? string.Empty;

         if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All) {
            result = BadWordManager.ReplaceAll(result, sources);
         }
         
         if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All) {
            result = DomainManager.ReplaceAll(result, sources);
         }
         
         if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization || (mask & ManagerMask.All) == ManagerMask.All) {
            //result = CapitalizationManager.ReplaceAll(result, sources);
            result = CapitalizationManager.ReplaceAll(result);
         }
         
         if ((mask & ManagerMask.Punctuation) == ManagerMask.Punctuation || (mask & ManagerMask.All) == ManagerMask.All) {
            //result = PunctuationManager.ReplaceAll(result, sources);
            result = PunctuationManager.ReplaceAll(result);
         }

         return result;
      }

      /// <summary>
      /// Replaces all bad words in a text.
      /// Use this method if you already have a list of bad words (e.g. from the 'GetAll()' method).
      /// </summary>
      /// <param name="text">Text containig bad words</param>
      /// <param name="mask">Active manager (default: ManagerMask.All, optional)</param>
      /// <param name="badWords">Bad words to replace</param>
      /// <returns>Clean text</returns>
      public static string Replace(string text, List<string> badWords, ManagerMask mask = ManagerMask.All) {
         string result = text ?? string.Empty;
         
         if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All) {
            result = BadWordManager.Replace(result, badWords);
         }
         
         if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All) {
            result = DomainManager.Replace(result, badWords);
         }
         
         if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization || (mask & ManagerMask.All) == ManagerMask.All) {
            result = CapitalizationManager.Replace(result, badWords);
         }
         
         if ((mask & ManagerMask.Punctuation) == ManagerMask.Punctuation || (mask & ManagerMask.All) == ManagerMask.All) {
            result = PunctuationManager.Replace(result, badWords);
         }
         
         return result;
      }

      /// <summary>Marks the text with a prefix and postfix from a list of words.</summary>
      /// <param name="text">Text containig bad words</param>
      /// <param name="badWords">Bad words to mark</param>
      /// <param name="prefix">Prefix for every found bad word (optional)</param>
      /// <param name="postfix">Postfix for every found bad word (optional)</param>
      /// <returns>Text with marked bad words</returns>
      //public static string Mark(string text, List<string> badWords, ManagerMask mask = ManagerMask.BadWord, string prefix = "", string postfix = "") {
      public static string Mark(string text, List<string> badWords, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text ?? string.Empty;

         //The different mangers all do the same.
         result = BadWordManager.Mark(result, badWords, prefix, postfix);

         return result;
      }

      /// <summary>Unmarks the text with a prefix and postfix.</summary>
      /// <param name="text">Text with marked bad words</param>
      /// <param name="prefix">Prefix for every found bad word (optional)</param>
      /// <param name="postfix">Postfix for every found bad word (optional)</param>
      /// <returns>Text with marked bad words</returns>
      public static string Unmark(string text, string prefix = "<b><color=red>", string postfix = "</color></b>") {
         string result = text ?? string.Empty;

         //The different mangers all do the same.
         result = BadWordManager.Unmark(result, prefix, postfix);

         return result;
      }
#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com