using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Crosstales.BadWord {
   /// <summary>Text-file based bad word provider.</summary>
   [HelpURL("http://www.crosstales.com/en/assets/badwordfilter/api/class_crosstales_1_1_bad_word_1_1_bad_word_provider.html")]
   public class BadWordProviderText : BadWordProvider {

#region Implemented methods

      public override void Load() {
         base.Load();

         if (Sources != null) {
            loading = true;

            if (Helper.isEditorMode) {
#if UNITY_EDITOR
               foreach (Source source in Sources) {
                  if (source.Resource != null) {
                     loadResourceInEditor(source);
                  }

                  if (!string.IsNullOrEmpty(source.URL)) {
                     loadWebInEditor(source);
                  }
               }

               init();
#endif
            } else {
               foreach (Source source in Sources) {
                  if (source.Resource != null) {
                     StartCoroutine(loadResource(source));
                  }

                  if (!string.IsNullOrEmpty(source.URL)) {
                     StartCoroutine(loadWeb(source));
                  }
               }
            }
         }
      }

      public override void Save() {
         Debug.LogWarning("Save not implemented!");
      }

#endregion

#region Private methods

      private IEnumerator loadWeb(Source src) {
         Guid uid = Guid.NewGuid();
         coRoutines.Add(uid);

         if (!string.IsNullOrEmpty(src.URL)) {
            WWW www = new WWW(src.URL.Trim());
            yield return www;

            while (!www.isDone) {
               yield return null;
            }

            if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.text)) {
               List<string> list = Helper.SplitStringToLines(www.text);

               yield return null;

               if (list.Count > 0) {
                  badwords.Add(new BadWords(src, list));
               } else {
                  //Debug.LogWarning("Source: '" + src.URL + "' does not contain any active bad words!");
               }
            } else {
               Debug.LogWarning("Could not load source: '" + src.URL + "'" + Environment.NewLine + www.error + Environment.NewLine + "Did you set the correct 'URL'?");
            }

            www.Dispose();
         } else {
            Debug.LogWarning("'URL' is null or empty!" + Environment.NewLine + "Please add a valid URL.");
         }

         coRoutines.Remove(uid);

         if (loading && coRoutines.Count == 0) {
            loading = false;
            init();
         }
      }

      private IEnumerator loadResource(Source src)
        {
            yield return new WaitForSeconds(0.1f);
         Guid uid = Guid.NewGuid();
         coRoutines.Add(uid);

         if (src.Resource != null) {
            //List<string> list = Helper.SplitStringToLines(Resource.text, SkipHeaderLines, SkipFooterLines);
            List<string> list = Helper.SplitStringToLines(src.Resource.text);

            yield return null;

            if (list.Count > 0) {
               badwords.Add(new BadWords(src, list));
            } else {
               //Debug.LogWarning("Resource: '" + src.Resource + "' does not contain any active bad words!");
            }
         } else {
            Debug.LogWarning("Resource field 'Source' is null or empty!" + Environment.NewLine + "Please add a valid resource.");
         }

         coRoutines.Remove(uid);

            yield return null;
         if (loading && coRoutines.Count == 0) {
            loading = false;
            init();
         }
      }

#endregion

#region Editor-only methods

#if UNITY_EDITOR

      private void loadWebInEditor(Source src) {
         if (!string.IsNullOrEmpty(src.URL)) {
            WWW www = new WWW(src.URL.Trim());

            float startTime = Time.realtimeSinceStartup; //time in seconds
            float timeOut = 3f;
            bool isTimeout = false;

            while (!www.isDone) {
               if (Time.realtimeSinceStartup > startTime + timeOut) {
                  isTimeout = true;
                  break;
               }
            }

            if (!isTimeout && string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.text)) {
               List<string> list = Helper.SplitStringToLines(www.text);

               if (list.Count > 0) {
                  badwords.Add(new BadWords(src, list));
               } else {
                  Debug.LogWarning("Source: '" + src.URL + "' does not contain any active bad words!");
               }
            } else {
               Debug.LogWarning("Could not load source: '" + src.URL + "'" + Environment.NewLine + www.error + Environment.NewLine + "Did you set the correct 'URL'?");
            }

            www.Dispose();
         } else {
            Debug.LogWarning("'URL' is null or empty!" + Environment.NewLine + "Please add a valid URL.");
         }
      }

      private void loadResourceInEditor(Source src) {
         if (src.Resource != null) {
            List<string> list = Helper.SplitStringToLines(src.Resource.text);

            if (list.Count > 0) {
               badwords.Add(new BadWords(src, list));
            } else {
               //Debug.LogWarning("Resource: '" + src.Resource + "' does not contain any active bad words!");
            }
         } else {
            Debug.LogWarning("Resource field 'Source' is null or empty!" + Environment.NewLine + "Please add a valid resource.");
         }
      }

#endif

#endregion
   }
}
// Copyright 2016 www.crosstales.com