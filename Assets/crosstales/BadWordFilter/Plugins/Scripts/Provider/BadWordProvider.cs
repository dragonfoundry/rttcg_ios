using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Crosstales.BadWord {
   /// <summary>Base class for bad word providers.</summary>
   public abstract class BadWordProvider : Provider {
      
#region Variables

      protected List<BadWords> badwords = new List<BadWords>();

      private const string exactRegexStart = @"(?<![\w\d])";
      private const string exactRegexEnd = @"s?(?![\w\d])";
      private const string fuzzyRegexStart = @"\b\w*";
      private const string fuzzyRegexEnd = @"\w*\b";

      private Dictionary<string, Regex> exactBadwordsRegex = new Dictionary<string, Regex>();
      private Dictionary<string, Regex> fuzzyBadwordsRegex = new Dictionary<string, Regex>();
      private Dictionary<string, List<Regex>> debugExactBadwordsRegex = new Dictionary<string, List<Regex>>();
      private Dictionary<string, List<Regex>> debugFuzzyBadwordsRegex = new Dictionary<string, List<Regex>>();

#endregion

#region Properties
      /// <summary>Exact RegEx for bad words.</summary>
      public Dictionary<string, Regex> ExactBadwordsRegex {
         get{ return exactBadwordsRegex;}
         protected set { exactBadwordsRegex = value;}
      }

      /// <summary>Fuzzy RegEx for bad words.</summary>
      public Dictionary<string, Regex> FuzzyBadwordsRegex {
         get{ return fuzzyBadwordsRegex;}
         protected set { fuzzyBadwordsRegex = value;}
      }

      /// <summary>Debug-version of "Exact RegEx for bad words".</summary>
      public Dictionary<string, List<Regex>> DebugExactBadwordsRegex {
         get{ return debugExactBadwordsRegex;}
         protected set { debugExactBadwordsRegex = value;}
      }

      /// <summary>Debug-version of "Fuzzy RegEx for bad words".</summary>
      public Dictionary<string, List<Regex>> DebugFuzzyBadwordsRegex {
         get{ return debugFuzzyBadwordsRegex;}
         protected set { debugFuzzyBadwordsRegex = value;}
      }

#endregion

#region Implemented methods

      public override void Load() {
         if (ClearOnLoad) {
            badwords.Clear();
         }
      }

      protected override void init() {
         ExactBadwordsRegex.Clear();
         FuzzyBadwordsRegex.Clear();
         //Sources.Clear();
          
         if (Constants.DEBUG_BADWORDS) 
            Debug.Log("++ BadWordProvider '" + Name + "' started in debug-mode ++");
         
         foreach (BadWords badWord in badwords) {
            if (Constants.DEBUG_BADWORDS) {
               List<Regex> exactRegexes = new List<Regex>(badWord.BadWordList.Count);
               List<Regex> fuzzyRegexes = new List<Regex>(badWord.BadWordList.Count);

               foreach (string line in badWord.BadWordList) {
                  exactRegexes.Add(new Regex(exactRegexStart + line + exactRegexEnd, RegexOption1 | RegexOption2 | RegexOption3 | RegexOption4 | RegexOption5));
                  fuzzyRegexes.Add(new Regex(fuzzyRegexStart + line + fuzzyRegexEnd, RegexOption1 | RegexOption2 | RegexOption3 | RegexOption4 | RegexOption5));
               }

               if (!DebugExactBadwordsRegex.ContainsKey(badWord.Source.Name)) {
                  DebugExactBadwordsRegex.Add(badWord.Source.Name, exactRegexes);
               }

               if (!DebugExactBadwordsRegex.ContainsKey(badWord.Source.Name)) {
                  DebugExactBadwordsRegex.Add(badWord.Source.Name, fuzzyRegexes);
               }

            } else {
               if (!ExactBadwordsRegex.ContainsKey(badWord.Source.Name)) {
                  ExactBadwordsRegex.Add(badWord.Source.Name, new Regex(exactRegexStart + "(" + string.Join("|", badWord.BadWordList.ToArray()) + ")" + exactRegexEnd, RegexOption1 | RegexOption2 | RegexOption3 | RegexOption4 | RegexOption5));
               }

               if (!FuzzyBadwordsRegex.ContainsKey(badWord.Source.Name)) {
                  FuzzyBadwordsRegex.Add(badWord.Source.Name, new Regex(fuzzyRegexStart + "(" + string.Join("|", badWord.BadWordList.ToArray()) + ")" + fuzzyRegexEnd, RegexOption1 | RegexOption2 | RegexOption3 | RegexOption4 | RegexOption5));
               }
            }

            //Sources.Add(badWord.Source);

            if (Constants.DEBUG_BADWORDS) 
               Debug.Log("Bad word resource '" + badWord.Source.Name + "' loaded and " + badWord.BadWordList.Count + " entries found.");
            
         }

         Ready = true;
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com