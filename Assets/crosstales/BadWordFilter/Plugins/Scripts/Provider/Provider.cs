﻿using UnityEngine;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;

namespace Crosstales.BadWord {
   /// <summary>Base class for all providers.</summary>
   [ExecuteInEditMode]
   public abstract class Provider : MonoBehaviour {
      
#region Variables

      /// <summary>Name to identify the provider.</summary>
      [Tooltip("Name to identify the provider.")]
      public string Name = string.Empty; //to identify the provider

      [Header("Regex Options")]
      /// <summary>Option1 (default: RegexOptions.IgnoreCase).</summary>
      [Tooltip("Option1 (default: RegexOptions.IgnoreCase).")]
      public RegexOptions RegexOption1 = RegexOptions.IgnoreCase; //DEFAULT

      /// <summary>Option2 (default: RegexOptions.CultureInvariant).</summary>
      [Tooltip("Option2 (default: RegexOptions.CultureInvariant).")]
      public RegexOptions RegexOption2 = RegexOptions.CultureInvariant; //DEFAULT

      /// <summary>Option3 (default: RegexOptions.None).</summary>
      [Tooltip("Option3 (default: RegexOptions.None).")]
      public RegexOptions RegexOption3 = RegexOptions.None;

      /// <summary>Option4 (default: RegexOptions.None).</summary>
      [Tooltip("Option4 (default: RegexOptions.None).")]
      public RegexOptions RegexOption4 = RegexOptions.None;

      /// <summary>Option5 (default: RegexOptions.None).</summary>
      [Tooltip("Option5 (default: RegexOptions.None).")]
      public RegexOptions RegexOption5 = RegexOptions.None;

      [Header("Sources")]
      /// <summary>All sources for this provider.</summary>
      [Tooltip("All sources for this provider.")]
      public Source[] Sources;

      [Header("Load behaviour")]
      /// <summary>Clears all existing bad words on 'Load' (default: on).</summary>
      [Tooltip("Clears all existing bad words on 'Load' (default: on).")]
      public bool ClearOnLoad = true;

      protected List<Guid> coRoutines = new List<Guid>();

      protected static bool loggedUnsupportedPlatform = false;
      protected bool loading = false;

#endregion

#region Properties

      private bool ready = false;
      public bool Ready {
         get{ return ready;}
         protected set{ ready = value;}
      }

//      private List<Source> sources = new List<Source>();
//      public List<Source> Sources {
//         get { return sources;}
//      }

#endregion

#region Abstract methods

      /// <summary>Loads all sources.</summary>
      public abstract void Load();

      /// <summary>Saves all sources.</summary>
      public abstract void Save();

      /// <summary>Intialize the provider.</summary>
      protected abstract void init();

#endregion

#region MonoBehaviour methods

      public void Awake() {
         Load();
      }

#endregion

#region Protected methods

      protected void logNoResourcesAdded() {
         Debug.LogWarning("No 'Resources' for " + name + " added!" + Environment.NewLine + "If you want to use this functionality, please add your desired 'Resources'.");
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com