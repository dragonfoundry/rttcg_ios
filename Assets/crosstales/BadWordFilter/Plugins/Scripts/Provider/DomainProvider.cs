using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Crosstales.BadWord {
   /// <summary>Base class for domain providers.</summary>
   public abstract class DomainProvider : Provider {
      
#region Variables

      protected List<Domains> domains = new List<Domains>();

      private const string domainRegexStart = @"\b{0,1}((ht|f)tp(s?)\:\/\/)?[\w\-\.\@]*[\.]";
      //private const string domainRegexEnd = @"(:\d{1,5})?(\/|\b)([\a-zA-Z0-9\-\.\?\!\,\=\'\/\&\%#_]*)?\b";
      private const string domainRegexEnd = @"(:\d{1,5})?(\/|\b)";

      private Dictionary<string, Regex> domainsRegex = new Dictionary<string, Regex>();
      private Dictionary<string, List<Regex>> debugDomainsRegex = new Dictionary<string, List<Regex>>();
      
#endregion

#region Properties

      /// <summary>RegEx for domains.</summary>
      public Dictionary<string, Regex> DomainsRegex {
         get{ return domainsRegex;}
         protected set { domainsRegex = value;}
      }

      /// <summary>Debug-version of "RegEx for domains".</summary>
      public Dictionary<string, List<Regex>> DebugDomainsRegex {
         get{ return debugDomainsRegex;}
         protected set { debugDomainsRegex = value;}
      }

#endregion

#region Implemented methods

      public override void Load() {
         if (ClearOnLoad) {
            domains.Clear();
         }
      }

      protected override void init() {
         DomainsRegex.Clear();
         //Sources.Clear();
            
         if (Constants.DEBUG_DOMAINS)
            Debug.Log("++ DomainProvider '" + Name + "' started in debug-mode ++");

         foreach (Domains domain in domains) {
            if (Constants.DEBUG_DOMAINS) {
               List<Regex> domainRegexes = new List<Regex>(domain.DomainList.Count);

               foreach (string line in domain.DomainList) {
                  domainRegexes.Add(new Regex(domainRegexStart + line + domainRegexEnd, RegexOption1 | RegexOption2 | RegexOption3 | RegexOption4 | RegexOption5));
               }

               if (!DebugDomainsRegex.ContainsKey(domain.Source.Name)) {
                  DebugDomainsRegex.Add(domain.Source.Name, domainRegexes);
               }
            } else {
               if (!DomainsRegex.ContainsKey(domain.Source.Name)) {
                  DomainsRegex.Add(domain.Source.Name, new Regex(domainRegexStart + "(" + string.Join("|", domain.DomainList.ToArray()) + ")" + domainRegexEnd, RegexOption1 | RegexOption2 | RegexOption3 | RegexOption4 | RegexOption5));
               }
            }

            //Sources.Add(domain.Source);

            if (Constants.DEBUG_DOMAINS) 
               Debug.Log("Domain resource '" + domain.Source + "' loaded and " + domain.DomainList.Count + " entries found.");
            
         }
         Ready = true;
      }

#endregion
   }
}
// Copyright 2015-2016 www.crosstales.com