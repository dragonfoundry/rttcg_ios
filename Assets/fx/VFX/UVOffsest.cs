﻿using UnityEngine;
using System.Collections;

public class UVOffsest : MonoBehaviour
{
    public float scrollSpeed = 0.5F;
    public Renderer rend;
    float offset = -1f;

    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.material.mainTextureOffset = new Vector2(-1, 0);

    }
    void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        rend.material.mainTextureOffset = new Vector2(offset, 0);
    }


}
