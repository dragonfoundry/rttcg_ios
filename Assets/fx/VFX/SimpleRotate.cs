﻿using UnityEngine;
using System.Collections;

public class SimpleRotate : MonoBehaviour
{
    public float rotationSpeed;
    public float damping;
    public bool xAxis = false;
    public bool yAxis = false;
    public bool zAxis = false;
    private int caseSwitch = 0;
    private Vector3 rot = Vector3.zero;

    // **************** only one axis can be true, if multiple axis are enable, only x will be used

	void Awake()
    {
        if (damping == 0f)
            damping = 1f;

        if (xAxis)
        {
            caseSwitch = 1;
            yAxis = false;
            zAxis = false;
        }
        else if (yAxis)
        {
            caseSwitch = 2;
            xAxis = false;
            zAxis = false;
        }
        else if (zAxis)
        {
            caseSwitch = 3;
            xAxis = false;
            yAxis = false;
        }

        switch (caseSwitch)
        {
            case 1:
                rot = new Vector3(rotationSpeed, 0f, 0f);
                break;
            case 2:
                rot = new Vector3(0f, rotationSpeed, 0f);
                break;
            case 3:
                rot = new Vector3(0f, 0f, rotationSpeed);
                break;
            default:
                break;
        }

    }

	void Update () {
        transform.Rotate(rot / damping);
	}
}
