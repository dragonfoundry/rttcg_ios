﻿using UnityEngine;
using System.Collections;

public class StaticUVOffsest : MonoBehaviour
{
    public Renderer rend;
    public float offset = -1f;

    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.material.mainTextureOffset = new Vector2(offset, 0);

    }
}
