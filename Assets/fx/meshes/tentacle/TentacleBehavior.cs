﻿using UnityEngine;
using System.Collections;

public class TentacleBehavior : MonoBehaviour
{
    public bool anim01, anim02, anim03 = false;
    private Animator anim;

	void Start ()
    {
        anim = GetComponent<Animator>();

        if (anim01)
            anim.SetBool("anim01", true);
        if (anim02)
            anim.SetBool("anim02", true);
    }
	

}
