﻿using UnityEngine;
using System.Collections;

public class CardCrumble : MonoBehaviour
{
    private Vector3 startPos;
    //private Quaternion startRot;


    void Start ()
    {
        startPos = transform.position;
        //startRot = transform.rotation;
    }



    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == "crumbler")
        {
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            gameObject.GetComponentInChildren<ParticleSystem>().Play(true);
        }
    }

    public void OnBegin()
    {
        foreach (GameObject card in GameObject.FindGameObjectsWithTag("crumbledCard"))
        {
            card.GetComponent<Rigidbody>().useGravity = false;
            card.GetComponent<Rigidbody>().mass = 200.0f;
            card.GetComponent<Rigidbody>().angularDrag = 0.05f;
            card.GetComponent<Rigidbody>().drag = 0.01f;
            card.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    public void OnReset()
    {
        foreach(GameObject card in GameObject.FindGameObjectsWithTag("crumbledCard"))
        {
            card.GetComponent<MeshRenderer>().enabled = false;
            card.GetComponent<Rigidbody>().mass = 0.0f;
            card.GetComponent<Rigidbody>().angularDrag = 100.0f;
            card.GetComponent<Rigidbody>().useGravity = false;
            card.GetComponent<Rigidbody>().mass = 0.0f;
            card.GetComponent<Rigidbody>().angularDrag = 100.0f;
            card.GetComponent<Rigidbody>().drag = 100.0f;
            card.transform.position = card.GetComponent<CardCrumble>().startPos;
        }
    }
}
