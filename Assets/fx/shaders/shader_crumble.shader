// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Shader created with Shader Forge Beta 0.34 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.34;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:31864,y:32854|diff-2-RGB,clip-414-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33164,y:32524,ptlb:diffuse,ptin:_diffuse,tex:1e2f861f111ab7347a6bbd8d2d792117,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:35,x:33141,y:33088|A-137-OUT,B-321-A,T-313-OUT;n:type:ShaderForge.SFN_Time,id:36,x:34119,y:33119;n:type:ShaderForge.SFN_Tex2d,id:136,x:33756,y:32521,ptlb:node_136,ptin:_node_136,tex:45970b3b088940a4ca634c647c2b0312,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Clamp01,id:137,x:33276,y:32717|IN-136-A;n:type:ShaderForge.SFN_Sin,id:313,x:33715,y:33195|IN-1192-OUT;n:type:ShaderForge.SFN_Color,id:321,x:33413,y:32896,ptlb:cardOpacity,ptin:_cardOpacity,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Panner,id:358,x:33396,y:33384,spu:-0.75,spv:0.2|UVIN-359-UVOUT,DIST-836-OUT;n:type:ShaderForge.SFN_TexCoord,id:359,x:33726,y:33383,uv:0;n:type:ShaderForge.SFN_Multiply,id:361,x:32925,y:33206|A-35-OUT,B-531-OUT;n:type:ShaderForge.SFN_Clamp01,id:414,x:32766,y:33206|IN-361-OUT;n:type:ShaderForge.SFN_RemapRange,id:528,x:33334,y:33622,frmn:0,frmx:1,tomn:-1,tomx:1|IN-358-UVOUT;n:type:ShaderForge.SFN_Multiply,id:529,x:33150,y:33650|A-528-OUT,B-528-OUT;n:type:ShaderForge.SFN_ComponentMask,id:530,x:33086,y:33512,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-529-OUT;n:type:ShaderForge.SFN_Add,id:531,x:32828,y:33493|A-530-R,B-530-G;n:type:ShaderForge.SFN_Clamp01,id:836,x:33542,y:33253|IN-313-OUT;n:type:ShaderForge.SFN_TexCoord,id:887,x:33715,y:33682,uv:0;n:type:ShaderForge.SFN_TexCoord,id:1156,x:33041,y:32930,uv:0;n:type:ShaderForge.SFN_Multiply,id:1192,x:33865,y:33017|A-1193-OUT,B-36-T;n:type:ShaderForge.SFN_Vector1,id:1193,x:34021,y:32951,v1:0.5;proporder:2-321-136;pass:END;sub:END;*/

Shader "Custom/shader_crumble" {
    Properties {
        _diffuse ("diffuse", 2D) = "white" {}
        _cardOpacity ("cardOpacity", Color) = (0.5,0.5,0.5,1)
        _node_136 ("node_136", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _diffuse; uniform float4 _diffuse_ST;
            uniform sampler2D _node_136; uniform float4 _node_136_ST;
            uniform float4 _cardOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), unity_WorldToObject).xyz;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float2 node_1201 = i.uv0;
                float4 node_36 = _Time + _TimeEditor;
                float node_313 = sin((0.5*node_36.g));
                float2 node_528 = ((i.uv0.rg+saturate(node_313)*float2(-0.75,0.2))*2.0+-1.0);
                float2 node_530 = (node_528*node_528).rg;
                clip(saturate((lerp(saturate(tex2D(_node_136,TRANSFORM_TEX(node_1201.rg, _node_136)).a),_cardOpacity.a,node_313)*(node_530.r+node_530.g))) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.rgb;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * tex2D(_diffuse,TRANSFORM_TEX(node_1201.rg, _diffuse)).rgb;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _diffuse; uniform float4 _diffuse_ST;
            uniform sampler2D _node_136; uniform float4 _node_136_ST;
            uniform float4 _cardOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), unity_WorldToObject).xyz;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float2 node_1202 = i.uv0;
                float4 node_36 = _Time + _TimeEditor;
                float node_313 = sin((0.5*node_36.g));
                float2 node_528 = ((i.uv0.rg+saturate(node_313)*float2(-0.75,0.2))*2.0+-1.0);
                float2 node_530 = (node_528*node_528).rg;
                clip(saturate((lerp(saturate(tex2D(_node_136,TRANSFORM_TEX(node_1202.rg, _node_136)).a),_cardOpacity.a,node_313)*(node_530.r+node_530.g))) - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * tex2D(_diffuse,TRANSFORM_TEX(node_1202.rg, _diffuse)).rgb;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCollector"
            Tags {
                "LightMode"="ShadowCollector"
            }
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCOLLECTOR
            #define SHADOW_COLLECTOR_PASS
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcollector
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_136; uniform float4 _node_136_ST;
            uniform float4 _cardOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_COLLECTOR;
                float2 uv0 : TEXCOORD5;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_SHADOW_COLLECTOR(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float2 node_1203 = i.uv0;
                float4 node_36 = _Time + _TimeEditor;
                float node_313 = sin((0.5*node_36.g));
                float2 node_528 = ((i.uv0.rg+saturate(node_313)*float2(-0.75,0.2))*2.0+-1.0);
                float2 node_530 = (node_528*node_528).rg;
                clip(saturate((lerp(saturate(tex2D(_node_136,TRANSFORM_TEX(node_1203.rg, _node_136)).a),_cardOpacity.a,node_313)*(node_530.r+node_530.g))) - 0.5);
                SHADOW_COLLECTOR_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Cull Off
            Offset 1, 1
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_136; uniform float4 _node_136_ST;
            uniform float4 _cardOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float2 node_1204 = i.uv0;
                float4 node_36 = _Time + _TimeEditor;
                float node_313 = sin((0.5*node_36.g));
                float2 node_528 = ((i.uv0.rg+saturate(node_313)*float2(-0.75,0.2))*2.0+-1.0);
                float2 node_530 = (node_528*node_528).rg;
                clip(saturate((lerp(saturate(tex2D(_node_136,TRANSFORM_TEX(node_1204.rg, _node_136)).a),_cardOpacity.a,node_313)*(node_530.r+node_530.g))) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
