// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'

// Shader created with Shader Forge Beta 0.34 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.34;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:True,enco:True,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,blpr:3,bsrc:0,bdst:6,culm:2,dpts:2,wrdp:False,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:31826,y:32759|diff-1094-OUT,emission-802-OUT,transm-2-RGB,alpha-1327-OUT,voffset-913-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33365,y:32451,ptlb:Diffuse Texture,ptin:_DiffuseTexture,tex:1a5d8cd24a805cd44b00c7f1e1e4d287,ntxv:2,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:9,x:35170,y:33137,uv:0;n:type:ShaderForge.SFN_Time,id:106,x:35165,y:33456;n:type:ShaderForge.SFN_Panner,id:115,x:33931,y:33420,spu:0.25,spv:0.1|UVIN-1765-OUT,DIST-2238-OUT;n:type:ShaderForge.SFN_Tex2d,id:601,x:33423,y:33612,ptlb:displacementMa,ptin:_displacementMa,tex:1a5d8cd24a805cd44b00c7f1e1e4d287,ntxv:0,isnm:False|UVIN-644-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:602,x:33072,y:33572,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1736-OUT;n:type:ShaderForge.SFN_Rotator,id:644,x:33628,y:33612|UVIN-115-UVOUT,PIV-2013-UVOUT,ANG-1895-OUT,SPD-919-OUT;n:type:ShaderForge.SFN_Color,id:795,x:33535,y:32960,ptlb:Glow Colour,ptin:_GlowColour,glob:False,c1:0.1442474,c2:0.8529412,c3:0.3837371,c4:1;n:type:ShaderForge.SFN_Tex2d,id:801,x:33935,y:32618,ptlb:Glow Tex,ptin:_GlowTex,tex:820a38890d8f1f742b289b29927ffb3b,ntxv:0,isnm:False|UVIN-1026-UVOUT;n:type:ShaderForge.SFN_Multiply,id:802,x:33281,y:32875|A-1397-OUT,B-795-RGB;n:type:ShaderForge.SFN_Time,id:890,x:35144,y:34225;n:type:ShaderForge.SFN_Slider,id:906,x:34361,y:34195,ptlb:speed,ptin:_speed,min:0,cur:1.160648,max:10;n:type:ShaderForge.SFN_Slider,id:912,x:33227,y:33842,ptlb:intensity,ptin:_intensity,min:0,cur:1.262136,max:5;n:type:ShaderForge.SFN_Multiply,id:913,x:32813,y:33525|A-602-OUT,B-912-OUT,C-2254-OUT;n:type:ShaderForge.SFN_Multiply,id:919,x:33824,y:33890|A-906-OUT,B-920-OUT,C-2156-OUT;n:type:ShaderForge.SFN_Vector1,id:920,x:34040,y:34169,v1:0.01;n:type:ShaderForge.SFN_Lerp,id:960,x:33224,y:32113|A-1095-RGB,B-1107-RGB,T-1370-OUT;n:type:ShaderForge.SFN_Time,id:961,x:35406,y:32836;n:type:ShaderForge.SFN_ConstantClamp,id:974,x:33048,y:32188,min:0,max:1|IN-960-OUT;n:type:ShaderForge.SFN_Panner,id:1026,x:34255,y:32658,spu:0.1,spv:0.1|UVIN-1765-OUT,DIST-1386-OUT;n:type:ShaderForge.SFN_Multiply,id:1094,x:32804,y:32351|A-974-OUT,B-2-RGB;n:type:ShaderForge.SFN_Color,id:1095,x:33570,y:31906,ptlb:Colour 2,ptin:_Colour2,glob:False,c1:0.2658543,c2:0.8455882,c3:0.03730535,c4:1;n:type:ShaderForge.SFN_Color,id:1107,x:33538,y:32119,ptlb:Colour 1,ptin:_Colour1,glob:False,c1:0.06541956,c2:0.8088235,c3:0.8088235,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1202,x:33350,y:33125,ptlb:Alpha Tex,ptin:_AlphaTex,tex:6e77d387966489d6cb6bb6ba39e09c45,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:1203,x:33092,y:33126|A-2-A,B-1202-A,T-1205-OUT;n:type:ShaderForge.SFN_Time,id:1204,x:34027,y:33190;n:type:ShaderForge.SFN_Sin,id:1205,x:33382,y:33311|IN-2201-OUT;n:type:ShaderForge.SFN_ComponentMask,id:1327,x:32711,y:32920,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1575-OUT;n:type:ShaderForge.SFN_Sin,id:1370,x:33403,y:32272|IN-2180-OUT;n:type:ShaderForge.SFN_Relay,id:1382,x:34966,y:33054|IN-961-T;n:type:ShaderForge.SFN_Tex2d,id:1383,x:33926,y:32970,ptlb:Glow Tex 2,ptin:_GlowTex2,tex:0a70c16c7b47ab048bc9e03b588c24f6,ntxv:0,isnm:False|UVIN-1026-UVOUT;n:type:ShaderForge.SFN_Lerp,id:1384,x:33723,y:32807|A-801-RGB,B-1383-RGB,T-1386-OUT;n:type:ShaderForge.SFN_Cos,id:1386,x:34454,y:33064|IN-1382-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:1397,x:33470,y:32755,min:0,max:1|IN-1384-OUT;n:type:ShaderForge.SFN_Clamp01,id:1575,x:32881,y:33000|IN-1203-OUT;n:type:ShaderForge.SFN_Clamp01,id:1736,x:33249,y:33582|IN-601-RGB;n:type:ShaderForge.SFN_ComponentMask,id:1753,x:32926,y:33147,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-802-OUT;n:type:ShaderForge.SFN_Relay,id:1765,x:35018,y:33324|IN-9-UVOUT;n:type:ShaderForge.SFN_Vector1,id:1875,x:34446,y:33878,v1:0;n:type:ShaderForge.SFN_Vector1,id:1894,x:34426,y:33823,v1:6.28;n:type:ShaderForge.SFN_Lerp,id:1895,x:34265,y:33878|A-1894-OUT,B-1875-OUT,T-1943-OUT;n:type:ShaderForge.SFN_Multiply,id:1943,x:34609,y:34017|A-2195-OUT,B-1944-OUT;n:type:ShaderForge.SFN_Vector1,id:1944,x:34775,y:34085,v1:2;n:type:ShaderForge.SFN_Panner,id:2013,x:33974,y:33587,spu:0.25,spv:0.1|UVIN-1765-OUT,DIST-2024-OUT;n:type:ShaderForge.SFN_Multiply,id:2024,x:34202,y:33669|A-106-T,B-2025-OUT,C-906-OUT;n:type:ShaderForge.SFN_Vector1,id:2025,x:34556,y:33791,v1:0.05;n:type:ShaderForge.SFN_Vector1,id:2156,x:33858,y:34230,v1:0.01;n:type:ShaderForge.SFN_Add,id:2180,x:34638,y:32490|A-961-T,B-2181-OUT;n:type:ShaderForge.SFN_Vector1,id:2181,x:35071,y:32786,v1:10;n:type:ShaderForge.SFN_Add,id:2187,x:34929,y:33703|A-106-T,B-2189-OUT;n:type:ShaderForge.SFN_Vector1,id:2188,x:35144,y:34016,v1:500;n:type:ShaderForge.SFN_Slider,id:2189,x:35318,y:33638,ptlb:Seed,ptin:_Seed,min:10,cur:10,max:1000;n:type:ShaderForge.SFN_Add,id:2195,x:34891,y:33943|A-2187-OUT,B-2188-OUT,C-890-TSL;n:type:ShaderForge.SFN_Add,id:2201,x:33718,y:33241|A-2189-OUT,B-1204-T,C-2202-OUT;n:type:ShaderForge.SFN_Vector1,id:2202,x:33918,y:33355,v1:800;n:type:ShaderForge.SFN_Cos,id:2238,x:34321,y:33454|IN-2187-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2254,x:33090,y:33981,ptlb:int. power,ptin:_intpower,glob:False,v1:1;proporder:2-601-795-801-906-912-1095-1107-1202-1383-2189-2254;pass:END;sub:END;*/

Shader "Custom/highlight" {
    Properties {
        _DiffuseTexture ("Diffuse Texture", 2D) = "black" {}
        _displacementMa ("displacementMa", 2D) = "white" {}
        _GlowColour ("Glow Colour", Color) = (0.1442474,0.8529412,0.3837371,1)
        _GlowTex ("Glow Tex", 2D) = "white" {}
        _speed ("speed", Range(0, 10)) = 1.160648
        _intensity ("intensity", Range(0, 5)) = 1.262136
        _Colour2 ("Colour 2", Color) = (0.2658543,0.8455882,0.03730535,1)
        _Colour1 ("Colour 1", Color) = (0.06541956,0.8088235,0.8088235,1)
        _AlphaTex ("Alpha Tex", 2D) = "white" {}
        _GlowTex2 ("Glow Tex 2", 2D) = "white" {}
        _Seed ("Seed", Range(10, 1000)) = 10
        _intpower ("int. power", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcColor
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers d3d11 xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _DiffuseTexture; uniform float4 _DiffuseTexture_ST;
            uniform sampler2D _displacementMa; uniform float4 _displacementMa_ST;
            uniform float4 _GlowColour;
            uniform sampler2D _GlowTex; uniform float4 _GlowTex_ST;
            uniform float _speed;
            uniform float _intensity;
            uniform float4 _Colour2;
            uniform float4 _Colour1;
            uniform sampler2D _AlphaTex; uniform float4 _AlphaTex_ST;
            uniform sampler2D _GlowTex2; uniform float4 _GlowTex2_ST;
            uniform float _Seed;
            uniform float _intpower;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 shLight : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.shLight = ShadeSH9(float4(mul(unity_ObjectToWorld, float4(v.normal,0)).xyz * 1.0,1)) * 0.5;
                o.normalDir = mul(float4(v.normal,0), unity_WorldToObject).xyz;
                float4 node_106 = _Time + _TimeEditor;
                float node_2187 = (node_106.g+_Seed);
                float4 node_890 = _Time + _TimeEditor;
                float2 node_1765 = o.uv0.rg;
                float node_644_ang = lerp(6.28,0.0,((node_2187+500.0+node_890.r)*2.0));
                float node_644_spd = (_speed*0.01*0.01);
                float node_644_cos = cos(node_644_spd*node_644_ang);
                float node_644_sin = sin(node_644_spd*node_644_ang);
                float2 node_644_piv = (node_1765+(node_106.g*0.05*_speed)*float2(0.25,0.1));
                float2 node_644 = (mul((node_1765+cos(node_2187)*float2(0.25,0.1))-node_644_piv,float2x2( node_644_cos, -node_644_sin, node_644_sin, node_644_cos))+node_644_piv);
                float node_913 = (saturate(tex2Dlod(_displacementMa,float4(TRANSFORM_TEX(node_644, _displacementMa),0.0,0)).rgb).r*_intensity*_intpower);
                v.vertex.xyz += float3(node_913,node_913,node_913);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                
                float nSign = sign( dot( viewDirection, i.normalDir ) ); // Reverse normal if this is a backface
                i.normalDir *= nSign;
                normalDirection *= nSign;
                
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 forwardLight = max(0.0, NdotL );
                float2 node_2280 = i.uv0;
                float4 node_2 = tex2D(_DiffuseTexture,TRANSFORM_TEX(node_2280.rg, _DiffuseTexture));
                float3 backLight = max(0.0, -NdotL ) * node_2.rgb;
                float3 diffuse = (forwardLight+backLight)*InvPi * attenColor;
////// Emissive:
                float4 node_961 = _Time + _TimeEditor;
                float node_1386 = cos(node_961.g);
                float2 node_1765 = i.uv0.rg;
                float2 node_1026 = (node_1765+node_1386*float2(0.1,0.1));
                float3 node_802 = (clamp(lerp(tex2D(_GlowTex,TRANSFORM_TEX(node_1026, _GlowTex)).rgb,tex2D(_GlowTex2,TRANSFORM_TEX(node_1026, _GlowTex2)).rgb,node_1386),0,1)*_GlowColour.rgb);
                float3 emissive = node_802;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                diffuseLight += i.shLight; // Per-Vertex Light Probes / Spherical harmonics
                finalColor += diffuseLight * (clamp(lerp(_Colour2.rgb,_Colour1.rgb,sin((node_961.g+10.0))),0,1)*node_2.rgb);
                finalColor += emissive;
                float4 node_1204 = _Time + _TimeEditor;
/// Final Color:
                return fixed4(finalColor,saturate(lerp(node_2.a,tex2D(_AlphaTex,TRANSFORM_TEX(node_2280.rg, _AlphaTex)).a,sin((_Seed+node_1204.g+800.0)))).r);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers d3d11 xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _DiffuseTexture; uniform float4 _DiffuseTexture_ST;
            uniform sampler2D _displacementMa; uniform float4 _displacementMa_ST;
            uniform float4 _GlowColour;
            uniform sampler2D _GlowTex; uniform float4 _GlowTex_ST;
            uniform float _speed;
            uniform float _intensity;
            uniform float4 _Colour2;
            uniform float4 _Colour1;
            uniform sampler2D _AlphaTex; uniform float4 _AlphaTex_ST;
            uniform sampler2D _GlowTex2; uniform float4 _GlowTex2_ST;
            uniform float _Seed;
            uniform float _intpower;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), unity_WorldToObject).xyz;
                float4 node_106 = _Time + _TimeEditor;
                float node_2187 = (node_106.g+_Seed);
                float4 node_890 = _Time + _TimeEditor;
                float2 node_1765 = o.uv0.rg;
                float node_644_ang = lerp(6.28,0.0,((node_2187+500.0+node_890.r)*2.0));
                float node_644_spd = (_speed*0.01*0.01);
                float node_644_cos = cos(node_644_spd*node_644_ang);
                float node_644_sin = sin(node_644_spd*node_644_ang);
                float2 node_644_piv = (node_1765+(node_106.g*0.05*_speed)*float2(0.25,0.1));
                float2 node_644 = (mul((node_1765+cos(node_2187)*float2(0.25,0.1))-node_644_piv,float2x2( node_644_cos, -node_644_sin, node_644_sin, node_644_cos))+node_644_piv);
                float node_913 = (saturate(tex2Dlod(_displacementMa,float4(TRANSFORM_TEX(node_644, _displacementMa),0.0,0)).rgb).r*_intensity*_intpower);
                v.vertex.xyz += float3(node_913,node_913,node_913);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                
                float nSign = sign( dot( viewDirection, i.normalDir ) ); // Reverse normal if this is a backface
                i.normalDir *= nSign;
                normalDirection *= nSign;
                
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 forwardLight = max(0.0, NdotL );
                float2 node_2281 = i.uv0;
                float4 node_2 = tex2D(_DiffuseTexture,TRANSFORM_TEX(node_2281.rg, _DiffuseTexture));
                float3 backLight = max(0.0, -NdotL ) * node_2.rgb;
                float3 diffuse = (forwardLight+backLight)*InvPi * attenColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_961 = _Time + _TimeEditor;
                finalColor += diffuseLight * (clamp(lerp(_Colour2.rgb,_Colour1.rgb,sin((node_961.g+10.0))),0,1)*node_2.rgb);
                float4 node_1204 = _Time + _TimeEditor;
/// Final Color:
                return fixed4(finalColor * saturate(lerp(node_2.a,tex2D(_AlphaTex,TRANSFORM_TEX(node_2281.rg, _AlphaTex)).a,sin((_Seed+node_1204.g+800.0)))).r,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCollector"
            Tags {
                "LightMode"="ShadowCollector"
            }
            Cull Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCOLLECTOR
            #define SHADOW_COLLECTOR_PASS
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcollector
            #pragma exclude_renderers d3d11 xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform sampler2D _displacementMa; uniform float4 _displacementMa_ST;
            uniform float _speed;
            uniform float _intensity;
            uniform float _Seed;
            uniform float _intpower;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_COLLECTOR;
                float2 uv0 : TEXCOORD5;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                float4 node_106 = _Time + _TimeEditor;
                float node_2187 = (node_106.g+_Seed);
                float4 node_890 = _Time + _TimeEditor;
                float2 node_1765 = o.uv0.rg;
                float node_644_ang = lerp(6.28,0.0,((node_2187+500.0+node_890.r)*2.0));
                float node_644_spd = (_speed*0.01*0.01);
                float node_644_cos = cos(node_644_spd*node_644_ang);
                float node_644_sin = sin(node_644_spd*node_644_ang);
                float2 node_644_piv = (node_1765+(node_106.g*0.05*_speed)*float2(0.25,0.1));
                float2 node_644 = (mul((node_1765+cos(node_2187)*float2(0.25,0.1))-node_644_piv,float2x2( node_644_cos, -node_644_sin, node_644_sin, node_644_cos))+node_644_piv);
                float node_913 = (saturate(tex2Dlod(_displacementMa,float4(TRANSFORM_TEX(node_644, _displacementMa),0.0,0)).rgb).r*_intensity*_intpower);
                v.vertex.xyz += float3(node_913,node_913,node_913);
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_SHADOW_COLLECTOR(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                SHADOW_COLLECTOR_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Cull Off
            Offset 1, 1
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers d3d11 xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform sampler2D _displacementMa; uniform float4 _displacementMa_ST;
            uniform float _speed;
            uniform float _intensity;
            uniform float _Seed;
            uniform float _intpower;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                float4 node_106 = _Time + _TimeEditor;
                float node_2187 = (node_106.g+_Seed);
                float4 node_890 = _Time + _TimeEditor;
                float2 node_1765 = o.uv0.rg;
                float node_644_ang = lerp(6.28,0.0,((node_2187+500.0+node_890.r)*2.0));
                float node_644_spd = (_speed*0.01*0.01);
                float node_644_cos = cos(node_644_spd*node_644_ang);
                float node_644_sin = sin(node_644_spd*node_644_ang);
                float2 node_644_piv = (node_1765+(node_106.g*0.05*_speed)*float2(0.25,0.1));
                float2 node_644 = (mul((node_1765+cos(node_2187)*float2(0.25,0.1))-node_644_piv,float2x2( node_644_cos, -node_644_sin, node_644_sin, node_644_cos))+node_644_piv);
                float node_913 = (saturate(tex2Dlod(_displacementMa,float4(TRANSFORM_TEX(node_644, _displacementMa),0.0,0)).rgb).r*_intensity*_intpower);
                v.vertex.xyz += float3(node_913,node_913,node_913);
                o.pos = UnityObjectToClipPos(v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
