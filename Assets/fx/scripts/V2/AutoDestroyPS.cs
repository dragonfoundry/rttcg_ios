﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AutoDestroyPS : MonoBehaviour
{
	
	void Update ()
    {
        if (!this.GetComponent<ParticleSystem>().IsAlive())
        {
            Destroy(gameObject);
        }

    }

}
