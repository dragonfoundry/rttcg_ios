﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArcaneBurstV2 : MonoBehaviour
{
    public GameObject opposingPlayerBurstSource; // location in 3d space to spawn projectiles from
    public GameObject player01BurstSource; // location in 3d space to spawn projectiles from
    public GameObject arcaneBurstParticle;
    public GameObject arcaneBurstProjectilePFB; // "arcaneBurstProjectile" within FX Prefabs folder
    public int numOfArcaneBursts = 7;

    public static int playerWhoseAttacking = 1 ; // to be controlled by you
    public static Transform attackSpawnPoint; // used within the projectile itself

    private GameObject[] targetToDamage;
    private List<GameObject> projectiles = new List<GameObject>();
    int a = 0;
    float delay = 0.15f;

    void Awake()
    {
        switch (playerWhoseAttacking) // determine which side of board to spawn projectiles from
        {
            case 1:
                attackSpawnPoint = player01BurstSource.transform;
                break;
            default:
                attackSpawnPoint = opposingPlayerBurstSource.transform;
                break;
        }

        targetToDamage = GameObject.FindGameObjectsWithTag("targetToDamage"); // where projectiles will hit the card recieving damage
        StartCoroutine(SpawnProjectile(delay));

    }

    void Update()
    {


    }

    IEnumerator SpawnProjectile(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GameObject projectile = Instantiate(arcaneBurstProjectilePFB, attackSpawnPoint.position, Quaternion.identity) as GameObject;
        a++;
        int x = Random.Range(0, targetToDamage.Length);
        Transform projectileTarget = targetToDamage[x].transform;
        projectile.GetComponent<ArcaneBurstProjectileTarget>().target = projectileTarget;
        projectile.GetComponent<ArcaneBurstProjectileTarget>().startMarker = attackSpawnPoint.transform;
        if (a < numOfArcaneBursts)
            StartCoroutine(SpawnProjectile(delay));
    }

}
