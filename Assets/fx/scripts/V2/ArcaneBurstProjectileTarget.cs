﻿using UnityEngine;
using System.Collections;

public class ArcaneBurstProjectileTarget : MonoBehaviour
{
    public Transform target;

    public Transform startMarker;
    public Transform endMarker;
    float speed = 0.33f;
    float speedPower;
    public float startTime;
    public float journeyLength;
    private float offset;

    void Start ()
    {
        speedPower = Random.Range(2f, 3.0f);
        offset = Random.Range(-2f, 2f);
        endMarker = target;
        startTime = Time.time;
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
    }
	
	void Update ()
    {
        Vector3 center = (startMarker.position + endMarker.position) * 2F;
        center -= new Vector3(0, 1, 0);
        Vector3 riseRelCenter = startMarker.position - center + new Vector3(offset,0f,offset);
        Vector3 setRelCenter = endMarker.position - center;
        float fracJourney = (Time.time - startTime) * speed * speedPower;
        transform.position = Vector3.Slerp(riseRelCenter, setRelCenter, fracJourney);
        transform.position += center;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "targetToDamage")
        {
            GetComponent<TrailRenderer>().enabled = false;
        }
    }
}

