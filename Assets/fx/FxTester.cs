﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using DragonFoundry.Fx;

public class FxTester : MonoBehaviour
{
    public GameObject Anchor;
    public FxTrailTrigger TrailFx;
    public TextMeshProUGUI Header;
    public RectTransform Holder;
    public TextMeshProUGUI Title;
    public CanvasGroup CanvasGroup;

    private ParticleEffectsTrigger[] FX;
    
	void Start ()
    {
        var TrailFx2 = GameObject.Instantiate(TrailFx);
        //TrailFx2.transform.AttachToParent(Title.transform,0);
        FX = TrailFx2.GetComponentsInChildren<ParticleEffectsTrigger>();
        foreach (ParticleEffectsTrigger trigger in FX)
        {
            var header = GameObject.Instantiate(Header);
            var holder = GameObject.Instantiate(Holder);
            //var t2 = GameObject.Instantiate(trigger);
            trigger.gameObject.SetActive(false);
            trigger.transform.AttachToParent(holder.transform, 0);
            trigger.transform.localScale = Vector3.one;
            trigger.transform.localPosition = new Vector3(0, -100, 0);
            trigger.transform.localRotation = Quaternion.Euler(0, 0, 90);
            if (trigger.name == "trail_lightning")
                trigger.transform.localPosition = new Vector3(100, -100, 0);

            holder.transform.AttachToParent(Anchor.transform, 0);
            holder.transform.localScale = Vector3.one;
            holder.transform.localPosition = new Vector3(0, 0, 0);
            holder.name = trigger.name.Substring(6);

            header.transform.AttachToParent(this.transform, 0);
            header.transform.localScale = Vector3.one;
            header.transform.localPosition = new Vector3(0, 0, 0);
            header.text = trigger.name.Substring(6);
            header.name = trigger.name.Substring(6);

            var systems = trigger.GetComponentsInChildren<ParticleSystem>();
            foreach (var system in systems)
            {
                var renderer = system.GetComponent<Renderer>();
                renderer.sortingLayerName = "Overlay";
                renderer.sortingOrder = 20000;
            }
            var trails = trigger.GetComponentsInChildren<TrailRenderer>();
            foreach (var trail in trails)
            {
                trail.sortingLayerName = "Overlay";
                trail.sortingOrder = 20000;
            }
        }
        GameObject.Destroy(TrailFx2);

        Title.text = string.Format("FX Trail Test. {0} FX", FX.Length);
        Header.gameObject.SetActive(false);
        Holder.gameObject.SetActive(false);

        Anchor.transform.localPosition = Vector3.zero;

        Sequence seq = DOTween.Sequence();
        seq.SetLoops(-1);
        seq.AppendCallback(()=> {
            Anchor.gameObject.SetActive(false);
            foreach(var trigger in FX)
            {
                trigger.EndEffect();
            }
        });
        seq.Insert(0.1f, Anchor.transform.DOLocalMoveY(-600,0));
        seq.Insert(0.3f, CanvasGroup.DOFade(1.0f, 0.0f));
        seq.InsertCallback(0.4f, () =>
        {
            Anchor.gameObject.SetActive(true);
            /*firingNumber++;
            if (firingNumber * 3 > FX.Length || firingNumber < 1)
                firingNumber = 1;
            for(int i = 0; i<FX.Length; i++)
            {
                if (FX.Length % firingNumber == 0)
                {
                    FX[i].gameObject.SetActive(true);
                    FX[i].StartEffect();
                }

            }*/

            foreach (var trigger in FX)
            {
                trigger.StartEffect();
            }
        });
        seq.Append(Anchor.transform.DOLocalMoveY(450, 1.5f));
        seq.AppendInterval(0.1f);
        seq.Append(CanvasGroup.DOFade(0.0f, 0.2f));
        foreach (var trigger in FX)
        {
            trigger.gameObject.SetActive(true);
        }
    }
}
